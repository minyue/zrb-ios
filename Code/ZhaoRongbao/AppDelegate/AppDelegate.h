//
//  AppDelegate.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI/BMapKit.h>
#import <AMapNaviKit/AMapNaviKit.h>
#import "ABCIntroView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,BMKGeneralDelegate,ABCIntroViewDelegate>
{
    BMKMapManager *_mapManager;
    int _alertCount;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ABCIntroView *introView;

//- (void)autoLogin;
//-(void)share;
//过期，未登录
- (void)showUnvalidity;
//服务器异常
- (void)showServiceException;
//被T
- (void)showForcedoffline;
@end

