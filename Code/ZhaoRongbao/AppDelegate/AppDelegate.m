//
//  AppDelegate.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//
#import "SSGlobalConfig.h"
#import "AppDelegate.h"
#import "StoryBoardUtilities.h"
//#import "ProjectHomeViewController.h"
#import "SearchProjectHomeViewController.h"
#import "MineHomeViewController.h"
#import "MineContainerViewController.h"
#import "RegisterService.h"
#import "UMSocial.h"
#import "AFNetworkActivityIndicatorManager.h"

#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialSinaSSOHandler.h"
#import "IQKeyboardManager.h"
#import "UMSocial.h"
#import "LoadingViewController.h" 
#import "UMSocialSinaHandler.h"
#import "MineLoginViewcController.h"

@interface AppDelegate ()<UMSocialUIDelegate,LoadingViewControllerDelegate,UIAlertViewDelegate>
@property (nonatomic,strong) UITabBarController *tabController;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    [self setupUMConfig];
    
    //IQKeyboardManager虚拟键盘弹出遮挡输入框处理
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    manager.shouldToolbarUsesTextFieldTintColor = YES;
    manager.enableAutoToolbar = NO;
    
    
    
    // 要使用百度地图，请先启动BaiduMapManager
    _mapManager = [[BMKMapManager alloc]init];
    BOOL ret = [_mapManager start:BaiduMapAppkey generalDelegate:self];
    
    if (!ret) {
        LogInfo(@"baiduMapManager start failed!");
    }
    //初始化导航SDK
//    [BNCoreServices_Instance initServices:BaiduMapAppkey];
//    [BNCoreServices_Instance startServicesAsyn:nil fail:nil];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self autoLogin];
//    });
    //初始化高德地图SDK
    [AMapNaviServices sharedServices].apiKey = GaodeMapAppkey;
    [MAMapServices sharedServices].apiKey = GaodeMapAppkey;

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.tabController = [self setupMainViewController];
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:19],
                                                           NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],
                                                           NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [self.window setRootViewController:self.tabController];
     [self.window makeKeyAndVisible];
    id isFirstLaunch = [[SSGlobalConfig defaultConfig] getParam:@"LaunchViewController"];
    
    if(isFirstLaunch == nil){
        self.introView = [[ABCIntroView alloc] initWithFrame:self.tabController.view.frame];
        self.introView.delegate = self;
        self.introView.backgroundColor = [UIColor whiteColor];
        [self.tabController.view addSubview:self.introView];
    }else{
        // 启动界面
        LoadingViewController *loadingViewController = [[LoadingViewController    alloc] initWithNibName:@"LoadingViewController" bundle:[NSBundle   mainBundle]];
        loadingViewController.delegate = self;
        loadingViewController.animate = YES;
        [self.tabController presentViewController:loadingViewController animated:NO completion:nil];
        [self.window setRootViewController:loadingViewController];
    }

    [self setupUMConfig];
    
    return YES;
}



#pragma mark delegate of ABCIntroView

- (void)resgisterButtonPressed{
    
    [[SSGlobalConfig defaultConfig] setParam:@"LaunchViewController" value:@1];
    
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.introView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.introView removeFromSuperview];
    }];
    
    self.tabController.selectedIndex = 3;
    // 显示注册
    for(ZRB_UINavigationController* controller in self.tabController.viewControllers){
        if([[controller childViewControllers][0] isKindOfClass:[MineContainerViewController class]]){
            MineContainerViewController *mine = [controller childViewControllers][0];
            if(mine.mineVC){
                [mine.mineVC pushToRegister];
            }
        }
    }

}

- (void)loginButtonPressed{
    
    [[SSGlobalConfig defaultConfig] setParam:@"LaunchViewController" value:@1];
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.introView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.introView removeFromSuperview];
    }];
    
    self.tabController.selectedIndex = 3;
    //登录
    for(ZRB_UINavigationController* controller in self.tabController.viewControllers){
        if([[controller childViewControllers][0] isKindOfClass:[MineContainerViewController class]]){
            MineContainerViewController *mine = [controller childViewControllers][0];
            if(mine.mineVC){
                [mine.mineVC pushToLogin];
            }
        }
    }
}

- (void)experienceButtonPressed{
    
    [[SSGlobalConfig defaultConfig] setParam:@"LaunchViewController" value:@1];
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.introView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.introView removeFromSuperview];
    }];
}

#pragma mark -
#pragma mark Delegate

- (void)loadingControllerShouldDismiss:(LoadingViewController *)controller withAnimate:(BOOL)animate
{
    [controller dismissViewControllerAnimated:animate completion:nil];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return  [UMSocialSnsService handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return  [UMSocialSnsService handleOpenURL:url];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark - 私有方法
//配置友盟相关属性
- (void)setupUMConfig
{
    [UMSocialData setAppKey:UmengAppkey];
    
    //新浪微博
    [UMSocialSinaSSOHandler openNewSinaSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    //设置分享到QQ空间的应用Id，和分享url 链接
    [UMSocialQQHandler setQQWithAppId:QQAppId appKey:QQAppKey url:@"http://www.umeng.com/social"];
    //设置微信AppId，设置分享url，默认使用友盟的网址
    [UMSocialWechatHandler setWXAppId:WXChatAppId appSecret:WXChatAppSecret url:@"http://www.umeng.com/social"];
   
    
    [UMSocialConfig hiddenNotInstallPlatforms:@[UMShareToQQ,UMShareToQzone,UMShareToWechatTimeline,UMShareToWechatSession]];
}

- (UITabBarController *)setupMainViewController
{

    UITabBarController  *tabBarController = [[UITabBarController alloc] init];
    
    ZRB_UINavigationController  *findNav = (ZRB_UINavigationController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Find" class:kFindStoryboardId];
    findNav.tabBarItem = [self tabBarItemWithType:ZRBFineItemType];

    ZRB_UINavigationController  *projectNav = [[ZRB_UINavigationController alloc]initWithRootViewController:[[SearchProjectHomeViewController alloc]init]];
    projectNav.tabBarItem = [self tabBarItemWithType:ZRBProjectItemType];
    
    ZRB_UINavigationController  *moneyNav = (ZRB_UINavigationController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Money" class:kMoneyStoryboardId];
    moneyNav.tabBarItem = [self tabBarItemWithType:ZRBMoneyItemType];
    
    MineContainerViewController *mineVC = [[MineContainerViewController alloc] init];
    ZRB_UINavigationController  *mineNav = [[ZRB_UINavigationController alloc] initWithRootViewController:mineVC];
    mineNav.tabBarItem = [self tabBarItemWithType:ZRBMineItemType];
    
    NSArray  *array = @[findNav,projectNav,moneyNav,mineNav];
    tabBarController.viewControllers =array;
    return tabBarController;

}


- (UITabBarItem *)tabBarItemWithType:(ZRBTabItemType)type
{

    UIImage *image = nil;
    
    UIImage *selectImage = nil;
    
    NSString *title = nil;
    
    switch (type)
    {
        case ZRBFineItemType:
        {
            image = [UIImage imageNamed:@"discovery_normal"];
            selectImage = [UIImage imageNamed:@"discovery_highlight"];
            title = @"发现";
            break;
        }
        case ZRBProjectItemType:
        {
            image = [UIImage imageNamed:@"project_normal"];
            selectImage = [UIImage imageNamed:@"project_highlight"];
            title = @"找项目";

            break;
        }
        case ZRBMoneyItemType:
        {
            image = [UIImage imageNamed:@"asset_normal"];
            selectImage = [UIImage imageNamed:@"asset_highlight"];
            title = @"找资金";
            
            break;
        }
            
        case ZRBMineItemType:
        {
            image = [UIImage imageNamed:@"me_normal"];
            selectImage = [UIImage imageNamed:@"me_highlight"];
            title = @"我的";

            break;
        }
        default:
            break;
    }
    
    if (CURRENT_VERSION >=8.0)
    {
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        selectImage = [selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    UITabBarItem * barItem = [[UITabBarItem alloc] initWithTitle:title image:image selectedImage:selectImage];
    barItem.tag = type;
    [barItem setTitlePositionAdjustment:UIOffsetMake(0, -5)];
    return barItem;
}

- (void)refreshLogin{
    //请求自动登录
    //取出已存在的账号密码
    NSString *tel = (NSString*)[[SSGlobalConfig defaultConfig] getParam:@"PHONENUMBER"];
    NSString *pwd = [(NSString*)[[SSGlobalConfig defaultConfig] getParam:@"PASSWORD"] decodeBase64];
    if(!tel || !pwd || [pwd isEqualToString:@""] || [tel isEqualToString:@""]){
        return;
    }
    RegisterService *_service = [[RegisterService alloc] init];
    [_service loginWithName:tel passWord:pwd success:^(id responseObject) {
        LoginModel *logmodel = responseObject;
        if (logmodel.res == ZRBHttpSuccssType) {
            logmodel.phoneNum = tel;
            logmodel.userPwd = pwd;
            //登录成功
            [ZRB_UserManager loginSuccssWithModel:logmodel withBlock:nil];
        }
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
    [SVProgressHUD show];

}

#pragma mark - 强制被T，未登录过期
//过期，未登录
- (void)showUnvalidity{
    //请求自动登录
    [self refreshLogin];
}

//未启用
//- (void)showServiceException{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"服务器异常\n请您稍后重新登陆" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//    if(_alertCount == 0){
//        [alert show];
//        _alertCount++;
//    }
//}

//被T
- (void)showForcedoffline{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"您的帐号在其他位置登录\n请您重新登陆" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    if(_alertCount == 0){
        [alert show];
        _alertCount++;
    }
}

- (void)pushLoginController{
    self.tabController.selectedIndex = 3;
    //登录
    for(ZRB_UINavigationController* controller in self.tabController.viewControllers){
        if([[controller childViewControllers][0] isKindOfClass:[MineContainerViewController class]]){
            MineContainerViewController *mine = [controller childViewControllers][0];
            if(mine.mineVC){
                [mine.mineVC pushToLogin];
            }
        }
    }
}

#pragma mark - UIAlertDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    _alertCount = 0;
    if(buttonIndex == 0){
        exit(0);
    }else{
        //跳转登录
        [self pushLoginController];
    }
}

#pragma mark-
#pragma mark- 其他回调

#pragma mark - umeng share
-(BOOL)isDirectShareInIconActionSheet
{
    return YES;
}

-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}

@end
