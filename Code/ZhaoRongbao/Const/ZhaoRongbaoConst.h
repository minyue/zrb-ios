//
//  ZhaoRongbaoConst.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//


#ifndef __ZhaoRongbaoConst__H__
#define __ZhaoRongbaoConst__H__

#define ShareMenuHeight     180

//友盟sdk AppKey
#define UmengAppkey         @"5629a690e0f55a886b001d64"

//微信AppId
#define WXChatAppId         @"wx9773f4410609a9d2"
//微信AppSecret
#define WXChatAppSecret     @"76218ace8fef18b63cea0d45feba4849"
//QQ互联AppId
#define QQAppId             @"1104814343"
//QQ互联AppKey
#define QQAppKey            @"OuwjSfXtmF5n0NOf"

//招融宝主页地址 for share
#define ZRBINDEXSHAREURL    @"http://www.zhaorongbao.cn:81/"

//百度地图sdk
#define BaiduMapAppkey @"05bI7cjDumwjhukGxSKvIGkG"
//高德地图sdk
#define GaodeMapAppkey @"03ea66fddb090546282aed7ecba6a4e2"
//飞讯语音APPID
#define IflyAppID      @"55dd772c"


//系统版本
#define CURRENT_VERSION [[UIDevice currentDevice].systemVersion floatValue]


#define GetOriginX(view)                (view.frame.origin.x + view.frame.size.width)
#define GetOriginY(view)                (view.frame.origin.y + view.frame.size.height)

//当前设备屏幕高度
#define UIScreenHeight ([[UIScreen mainScreen] bounds].size.height)
#define UIScreenWidth  ([[UIScreen mainScreen] bounds].size.width)

//等比例计算当前宽度在各个分辨率的大小  （width:是375宽度下得，然后换算）

#define ZRBScaleAspectFit(width)      ((width * UIScreenWidth) / 375)


#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

//导航栏返回字体大小
#define ZRB_BACK_ITEM_SIZE   17

//页面背景颜色
#define ZRB_BACKGROUNDCORLOR @"eff1f6"
//navigationBar颜色
#define ZRB_NAVIGATIONBGCOLOR @"007de3"

//咨询页面上的滚动菜单的button的宽度
#define MENU_BUTTON_WIDTH  56

//通知标示
#define PROJECT_PROVINCE_CHANGE     @"Project_Province_Changed"
#define REGISTER_INFO_CHANGE        @"Register_Info_Change"

#define INTENT_PUSH                 @"Inetnt_Push"
#define INTENT_MODIFY               @"Intent_Modify"
#define INTENT_DELETE               @"Intent_Delete"

#define PROJECT_FILTER_WITH_INDUSTRY                @"Project_Filter_With_Industry"
#define PROJECT_FILTER_WITH_AREA                    @"Project_Filter_With_Area"
#define MONEY_FILTER_WITH_INDUSTRY                  @"Money_Filter_With_Industry"
#define MONEY_FILTER_WITH_AREA                      @"Money_Filter_With_Area"

extern NSString  *const kHttpFailError;

extern NSString  *const kHttpNonData;

//storyboard的标志值
extern NSString  *const kFindStoryboardId;
extern NSString  *const kMoneyStoryboardId;
extern NSString  *const kProjectStoryboardId;
extern NSString  *const kChatStoryboardId;
extern NSString  *const kMineStoryboardId;


//sotryboard的名字
extern NSString  *const kNewsStoryboardName;
extern NSString  *const kProjectStoryboardName;
extern NSString  *const kChatStoryboardName;
extern NSString  *const kMineStoryboardName;
extern NSString  *const kPersonalStoryboardName;

//分享

extern NSString *const kShareToWechat;          //微信
extern NSString *const kShareToWechatTimeline; //微信圈子
extern NSString *const kShareToQQ;             //QQ
extern NSString *const kShareToQzone;          //QQ空间
extern NSString *const kShareToSinaBlog;        //新浪微博
extern NSString *const kShareToTencentBlog;     //腾讯微博
extern NSString *const kShareToMail;            //邮件
extern NSString *const kShareToCopy;            //复制链接


typedef NS_ENUM(NSInteger, ShareType){
    /**
     *  QQ
     */
    ShareToQQ = 0,
    /**
     *  QQ空间
     */
    ShareToQQZone,
    /**
     *  微信
     */
    ShareToWechat,
    /**
     *  微信朋友圈
     */
    ShareToWechatFriend,
    /**
     *  新浪微博
     */
    ShareToSinaWeibo,
    /**
     *  复制链接
     */
    ShareToCopy,
    
};

typedef NS_ENUM(NSInteger, ZRBProvinceLastCtrlFlag){
    /**
     *  个人中心
     */
    ZRBPersonalCenterViewController = 0,
    /**
     *  首页
     */
    ZRBProjectHomeViewCtroller,
    /**
     *  注册-详细信息
     */
    ZRBRegisterUserInfoViewCtroller,
   
};


typedef NS_ENUM(NSInteger, ZRBTabItemType){
    /**
     *  发现
     */
    ZRBFineItemType=0,
    /**
     *  找资金
     */
    ZRBMoneyItemType,
    /**
     *  找项目
     */
    ZRBProjectItemType,
    /**
     *  我的
     */
    ZRBMineItemType
};


typedef NS_ENUM(NSInteger, ProjectMenuItemType) {
    /**
     *  分享
     */
    ProjectShareType = 0,
    /**
     *  收藏
     */
    ProjectCollectType,
    /**
     *  评论
     */
    ProjectCommentType,
};

typedef NS_ENUM(NSInteger, ProjectBottomMenuType) {
    /**
     *  咨询
     */
    ConsultingMenuType = 0,
    /**
     *  对话
     */
    ComunicationMenuType,
    /**
     *  勘察
     */
    InvestigationMenuType,
    
};

typedef NS_ENUM(NSInteger, MapType) {
    /**
     *  苹果地图
     */
    AppleMap = 0,
    /**
     *  百度地图
     */
    BaiduMap,
    /**
     *  高德地图
     */
    GaodeMap,
};

typedef NS_ENUM(NSInteger, ZRBHttpCode) {
    
    ZRBHttpSuccssType  = 1,     //成功
    
    ZRBHttpFailType   = 0,      //失败
    
    ZRBHttpNoLoginType  = -1,    //未登录,超时
    
    ZRBHttpServiceException = -5,   //服务器系统异常
    
    ZRBHttpForceOffline = -11    //被T
};



/**
 *  分享的类型
 */
typedef NS_ENUM(NSInteger, ZRBSocialSnsType){
    /**
     *  无
     */
    ZRBSocialSnsTypeNone = 0,
    /**
     *  微信
     */
    ZRBSocialSnsTypeWeChat,
    /**
     *  朋友圈
     */
    ZRBSocialSnsTypeWeChatTimeLine,
    /**
     *  QQ
     */
    ZRBSocialSnsTypeQQ,
    /**
     *  QQ空间
     */
    ZRBSocialSnsTypeQzone,
    /**
     *  新浪
     */
    ZRBSocialSnsTypeSinaBlog,
    /**
     *  腾讯微博
     */
    ZRBSocialSnsTypeTencentBlog,
    /**
     *  邮件
     */
    ZRBSocialSnsTypeMail,
    /**
     *  拷贝地址
     */
    ZRBSocialSnsTypeCopy
};


/**
 *  身份选择
 */
typedef NS_ENUM(NSInteger, IdentityType){
    /**
     *  政府
     */
    IdentityTypeServants = 2,
    /**
     *  企业
     */
    IdentityTypeInvestor = 1,
    /**
     *  其他
     */
    IdentityTypeOther   = 0
};


/**
 *  用户信息-是否认证
 */
typedef NS_ENUM(NSInteger, OrgStatus){
    /**
     * 已认证
     */
    OrgStatus_ed = 12,
    /**
     *  未认证
     */
    OrgStatus_un = 11,
    /**
     *  其他
     */
    OrgStatus_Other = 0
};

/**
 *  意向类型
 */
typedef NS_ENUM(NSInteger, IntentType) {
    /**
     *  资金
     */
    Intent_Project = 101,
    /**
     *  项目
     */
    Intent_Money = 102
};

//关注类型
typedef NS_ENUM(NSInteger, FcousType) {
    /**
     *  项目关注
     */
    FcousProject = 701,
    /**
     *  资金关注
     */
    FcousMoney = 702,
    /**
     *  意向关注
     */
    FcousIntent = 703,
    /**
     *  咨询项目
     */
    ConsultProject = 704,
    /**
     *  咨询资金
     */
    ConsultMoney = 705,
    /**
     *  咨询意向
     */
    ConsultIntent = 708,
    /**
     *  分享项目
     */
    ShareProject = 706,
    /**
     *  分享资金
     */
    ShareMoney = 707,
    /**
     *  分享意向
     */
    ShareIntent = 709,
};

/**
 *  意向绑定
 */
typedef NS_ENUM(NSInteger, IntentWithType) {
    /**
     *  意向绑定项目
     */
    IntentWithProject = 101,
    /**
     *  意向绑定资金
     */
    IntentWithMoney = 102
};

/**
 *  筛选类型
 */
typedef NS_ENUM(NSInteger,FilterType){
    /**
     *  项目筛选
     */
    ProjectFilter,
    /**
     *  资金筛选
     */
    MoneyFilter
};


/**
 *  认证类型
 */
typedef NS_ENUM(NSInteger, ApproveType) {
    /**
     *  实地认证
     */
    OnSideApprove = 502,
    /**
     *  资料认证
     */
    OnDataApprove = 503
};

#endif
