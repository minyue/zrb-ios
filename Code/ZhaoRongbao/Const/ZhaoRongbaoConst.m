//
//  ZhaoRongbaoConst.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//


#ifndef __ZhaoRongbaoConst__M__
#define __ZhaoRongbaoConst__M__


NSString  *const kHttpFailError     = @"请求失败，请稍后再试";

NSString  *const kHttpNonData       = @"暂无数据";

//storyboard的标志值
NSString  *const kFindStoryboardId   = @"FindViewController";
NSString  *const kMoneyStoryboardId      = @"MoneyHomeViewController";
NSString  *const kProjectStoryboardId   = @"ProjectHomeViewController";
//NSString  *const kChatStoryboardId      = @"ChatHomeViewController";
NSString  *const kMineStoryboardId      = @"MineHomeViewController";



NSString  *const kFindStoryboardName        = @"Find";
NSString  *const kProjectStoryboardName     = @"Projects";
NSString  *const kMoneyStoryboardName        = @"Money";
NSString  *const kMineStoryboardName        = @"Mine";
NSString  *const kPersonalStoryboardName    = @"Personal";




//分享
NSString *const kShareToWechat          = @"微信";
NSString *const kShareToWechatTimeline  = @"微信朋友圈";
NSString *const kShareToQQ              = @"QQ";
NSString *const kShareToQzone           = @"QQ空间";
NSString *const kShareToSinaBlog        = @"新浪微博";
NSString *const kShareToTencentBlog     = @"腾讯微博";
NSString *const kShareToMail            = @"邮件";
NSString *const kShareToCopy            = @"复制链接";

#endif

