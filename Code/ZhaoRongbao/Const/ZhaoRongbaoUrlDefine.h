//
//  ZhaoRongbaoUrlDefine.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#ifndef ZhaoRongbao_ZhaoRongbaoUrlDefine_h
#define ZhaoRongbao_ZhaoRongbaoUrlDefine_h

//客服电话
#define SERVICE_TEL         @"400-027-5955"

//测试
#define SERVER_URL                  @"http://172.17.32.253:8091"
#define IMAGE_SERVER_URL            @"http://172.17.32.253/"

//生产环境
//#define SERVER_URL              @"http://mobile.zhaorongbao.cn:81"
//#define IMAGE_SERVER_URL        @"http://file.zhaorongbao.cn:81"


//登录之后刷新首页列表
#define FIND_HOME_REFRESH           @"FindHomeRefresh"

//返现首页关注计数通知
#define LIST_FCOUS                  @"ListFcous"

//发现首页留言计数+1通知
#define LIST_MESSAGE_ADD            @"ListMessageAdd"

//发现详情留言计数+1通知
#define DETAIL_MESSAGE_ADD           @"DetailMessageAdd"


//查询某资讯或列表是否被关注过（项目、意向和资金通用）
#define HTTP_IFTAKEATTENTION_QUERY_URL             [NSString stringWithFormat:@"%@/userStatisticDetail/isCollected", SERVER_URL]

/********************************发现模块***********************************/
//查询名片用户信息
#define HTTP_PERSONAL_USERINFO           [NSString stringWithFormat:@"%@/businessCard/myCard",SERVER_URL]

//查询名片意向列表
#define HTTP_PERSONAL_INTENT_LIST       [NSString stringWithFormat:@"%@/intention/listByUser",SERVER_URL]

//查询名片资金列表
//#define HTTP_PERSONAL_FINDLIST           [NSString stringWithFormat:@"%@/businessCard/queryFundList",SERVER_URL]
#define HTTP_PERSONAL_FINDLIST           [NSString stringWithFormat:@"%@/intention/listByUser",SERVER_URL]




//发现首页列表
#define HTTP_FIND_HOME              [NSString stringWithFormat:@"%@/intention/list",SERVER_URL]

//发布项目、资金
#define HTTP_PUSH_INTENT            [NSString stringWithFormat:@"%@/intention/add",SERVER_URL]

//修改意向
#define HTTP_EDIT_INTENT            [NSString stringWithFormat:@"%@/intention/edit",SERVER_URL]

//删除意向
#define HTTP_DELETE_INTENT            [NSString stringWithFormat:@"%@/intention/delete",SERVER_URL]

//投资行业
#define HTTP_ALL_INDUSTRY           [NSString stringWithFormat:@"%@/industryApi/findAllIndustry",SERVER_URL]

//留言
#define HTTP_INTENT_MESSAGE         [NSString stringWithFormat:@"%@/intentionMessage/add",SERVER_URL]

//意向详情页面
#define HTTP_INTENT_DETAIL          [NSString stringWithFormat:@"%@/intention/details",SERVER_URL]

//意向详情Json
#define HTTP_DATA_INTENT_DETAIL          [NSString stringWithFormat:@"%@/intention/toEdit",SERVER_URL]

//关注
#define HTTP_FCOUS_URL              [NSString stringWithFormat:@"%@/userStatisticDetail/doAdd",                       SERVER_URL]

//取消关注
#define HTTP_CANCEL_FCOUS_URL              [NSString stringWithFormat:@"%@/userStatisticDetail/doCancel",                       SERVER_URL]


/********************************找资金模块***********************************/

//找资金首页列表
#define HTTP_MONEY_HOME_LIST            [NSString stringWithFormat:@"%@/fund/searchList",SERVER_URL]

/********************************找项目模块***********************************/
//找项目列表

#define HTTP_PROJECT_HOME_LIST      [NSString stringWithFormat:@"%@/projectApi/searchProList",SERVER_URL]


/********************************咨询模块***********************************/
//1、咨询列表：
#define HTTP_NEWS_QUERY_URL             [NSString stringWithFormat:@"%@/news/query",                       SERVER_URL]

//新闻咨询搜素
#define HTTP_NEWS_SEARCH_URL            [NSString stringWithFormat:@"%@/news/search",                      SERVER_URL]

//查询常用标签
#define HTTP_NEWS_QUERYSEARCHLABEL_URL  [NSString stringWithFormat:@"%@/news/querySearchLabel",            SERVER_URL]

//查询所有的咨询类别
#define HTTP_NEWS_QUERYCATEGORY_URL     [NSString stringWithFormat:@"%@/news/queryCategory",               SERVER_URL]

//随机获取热榜新闻
#define HTTP_HOTPOULAR_QUERYHOTPOULARAPI_URL  [NSString stringWithFormat:@"%@/hotPoular/querHotPoularAPI", SERVER_URL]

//咨询详情，html页面
#define HTTP_NEWSAPP_NEWSDETAIL_URL     [NSString stringWithFormat:@"%@/newsapp/newsDetails",              SERVER_URL]


/********************************项目模块***********************************/
//项目列表：
#define HTTP_PROJECT_QUERY_URL             [NSString stringWithFormat:@"%@/projectApi/queryProjectApiList", SERVER_URL]

//关注（项目和资讯通用）
#define HTTP_COLLECTION_URL             [NSString stringWithFormat:@"%@/user/interest/news/doAdd",          SERVER_URL]

//取消关注（项目和资讯通用）
#define HTTP_CANCEL_COLLECTION_URL  [NSString stringWithFormat:@"%@/user/interest/news/doCancel",           SERVER_URL]

//项目类型
#define HTTP_PROJECT_TYPE_URL           [NSString stringWithFormat:@"%@/projectComment/getCommentTypes",    SERVER_URL]

//提交项目评论
#define HTTP_SEND_PROJECT_COMMENT_URL    [NSString stringWithFormat:@"%@/projectComment/createNormal",      SERVER_URL]

//获取项目所有评论
#define HTTP_PROJECT_ALL_COMMENT_URL      [NSString stringWithFormat:@"%@/projectComment/query",            SERVER_URL]

//删除评论
#define HTTP_PROJECT_COMMENT_DELETE   [NSString stringWithFormat:@"%@/projectComment/remove",               SERVER_URL]

//项目勘察申请
#define HTTP_INVESTIGATION_APPLY    [NSString stringWithFormat:@"%@/inspect/userInspectApply",              SERVER_URL]

//索要更多资料
#define HTTP_ASKFOR_MORE_INFO       [NSString stringWithFormat:@"%@/proEmailApi/insert", SERVER_URL]

//城市详情
#define HTTP_CITY_DETAIL_INFO       [NSString stringWithFormat:@"%@/projectApi/getCityInfo", SERVER_URL]

//城市详情经济状况（城市详情页，弹出菜单的数据）
#define HTTP_CITY_ECONOMIC          [NSString stringWithFormat:@"%@/projectApi/getCityEconomic", SERVER_URL]

//产业详情
#define HTTP_CITY_INDUSTRY           [NSString stringWithFormat:@"%@/projectApi/getCityIndustry", SERVER_URL]

///查询项目图片
#define HTTP_PROJECTPHOTOLIST_QUERY_URL             [NSString stringWithFormat:@"%@/projectApi/getPhotoList", SERVER_URL]

/********************************个人中心***********************************/
//发送短信验证码
#define HTTP_SMS_URL                   [NSString stringWithFormat:@"%@/register/sendSMSRegister", SERVER_URL]

//更换手机号发送短信验证码
#define HTTP_UPDATEUMOBILESMS_URL                   [NSString stringWithFormat:@"%@/user/sendModifyMobileAuthCode", SERVER_URL]

//找回密码短信验证码发送
#define HTTP_FINDPWDSMS_URL                   [NSString stringWithFormat:@"%@/register/sendFindPasswordAuthCode", SERVER_URL]

//检查手机号码是否已经注册
#define HTTP_CHECKMOBILE_URL            [NSString stringWithFormat:@"%@/register/checkMobileRegister", SERVER_URL]

//检查短信验证码是否有效
#define HTTP_CHECKSMS_URL                [NSString stringWithFormat:@"%@/register/checkSMSRegister", SERVER_URL]

//验证用户名是否存在
#define HTTP_CHECKUSER_URL              [NSString stringWithFormat:@"%@/register/checkUnameRegister", SERVER_URL]

//保存用户注册信息 1 _V0.3
#define HTTP_SAVEAPPUSER_URL            [NSString stringWithFormat:@"%@/register/saveAppUser", SERVER_URL]
//修改用户信息 注册入口 2 _V1.0
#define HTTP_SAVEAPPUSERINFO_URL         [NSString stringWithFormat:@"%@/register/saveUserInfoApi", SERVER_URL]

//找回密码短信发送
#define HTTP_SMSFINDPWD_URL             [NSString stringWithFormat:@"%@/register/sendSMSFindPW", SERVER_URL]

//修改密码
#define HTTP_CHANGEPWD_URL              [NSString stringWithFormat:@"%@/register/changePWord", SERVER_URL]

//查询企业信息
#define HTTP_SEARCHCOMPANY_URL              [NSString stringWithFormat:@"%@/register/serchCompany", SERVER_URL]

//登录
#define HTTP_LOGIN_URL                  [NSString stringWithFormat:@"%@/dologin", SERVER_URL]

//用户图像上传
#define HTTP_UPLOADIMAGE_URL                 [NSString stringWithFormat:@"%@/avatarApi/fileUpload", SERVER_URL]

//获取当前用户所有信息
#define HTTP_GETUSERINFO_URL                 [NSString stringWithFormat:@"%@/user/doUserInfo", SERVER_URL]

//获取当前用户关注次数及被关注次数等信息
#define HTTP_GETUSERNUMSTATISTIC_URL                 [NSString stringWithFormat:@"%@/userStatisticDetail/getStatisticNumber", SERVER_URL]

//修改用户信息
#define HTTP_UPDATEAPPUSER_URL            [NSString stringWithFormat:@"%@/user/updateAppUserInfo", SERVER_URL]



//更换手机号
#define HTTP_UPDATEAPPUSERMOBILE_URL            [NSString stringWithFormat:@"%@/user/updateAppUserPhone", SERVER_URL]

//验证用户密码
#define HTTP_UPDATEAPPUSERPWDCHECK_URL            [NSString stringWithFormat:@"%@/user/checkPWord", SERVER_URL]

//修改用户密码
#define HTTP_UPDATEAPPUSERPWDCHANGE_URL            [NSString stringWithFormat:@"%@/user/updatePWord", SERVER_URL]

//获取所有省份信息
#define HTTP_GETAREAPROVINCEINFO_URL                 [NSString stringWithFormat:@"%@/areaApi/findAllProvince", SERVER_URL]

//获取所有市区信息
#define HTTP_GETAREACITYBYPROVINCEINFO_URL                 [NSString stringWithFormat:@"%@/areaApi/findCityByProvince", SERVER_URL]

//保存定位到的地区信息
#define HTTP_SAVELOCATIONATINFO_URL                 [NSString stringWithFormat:@"%@/user/updateUserLocation", SERVER_URL]

//行业列表
#define HTTP_ALLINDUSTRY_QUERY_URL             [NSString stringWithFormat:@"%@/industryApi/findAllIndustry", SERVER_URL]
/********************************个人设置***********************************/
//关于
#define HTTP_ABOUTZRB_URL                 [NSString stringWithFormat:@"%@/page/aboutZRB.html", SERVER_URL]
//意见反馈
#define HTTP_FEEDBACKZRB_URL                 [NSString stringWithFormat:@"%@/feedbackApi/insertDate", SERVER_URL]

/******************************我的主页**********************************/
//会员服务
#define HTTP_MEMBERSERVICEZRB_URL                 [NSString stringWithFormat:@"%@/page/member/index.html", SERVER_URL]
//认证介绍
#define HTTP_MEMBERCERTIFICATIONZRB_URL                 [NSString stringWithFormat:@"%@/page/member/certificationDescription.html", SERVER_URL]

//我关注的资讯列表
#define HTTP_MYATTENTIONNEWS_QUERY_URL             [NSString stringWithFormat:@"%@/news/queryCollections", SERVER_URL]
//我关注的项目列表
#define HTTP_MYATTENTIONPROJECTS_QUERY_URL             [NSString stringWithFormat:@"%@/collection/query/project", SERVER_URL]
//我的项目列表
#define HTTP_MYPROJECTS_QUERY_URL               [NSString stringWithFormat:@"%@/projectApi/myProject", SERVER_URL]
//我的项目详情
#define HTTP_MYPROJECTS_DETAIL_URL             [NSString stringWithFormat:@"%@/projectApi/myProjectDetail", SERVER_URL]
//我的勘察列表
#define HTTP_MYPROSPECT_QUERY_URL               [NSString stringWithFormat:@"%@/inspect/queryUserInspectPage", SERVER_URL]
//删除取消勘察的信息
#define HTTP_CANCELMYPROSPECTDELETE_QUERY_URL             [NSString stringWithFormat:@"%@/inspect/deleteByInspectId", SERVER_URL]
//勘察详情
#define HTTP_MYPROSPECTDETAIL_QUERY_URL               [NSString stringWithFormat:@"%@/inspect/queryUserInspectDetail", SERVER_URL]
//取消勘察
#define HTTP_CANCELMYPROSPECT_QUERY_URL             [NSString stringWithFormat:@"%@/inspect/userInspectCancel", SERVER_URL]
//我的发布列表
#define HTTP_MYPUBLISH_QUERY_URL               [NSString stringWithFormat:@"%@/ucenter/findPublish", SERVER_URL]
//我的关注发现列表
#define HTTP_MYATTENTIONFIND_QUERY_URL               [NSString stringWithFormat:@"%@/userStatisticDetail/getCollectIntentions", SERVER_URL]
//我的关注项目列表
#define HTTP_MYATTENTIONPROJECT_QUERY_URL               [NSString stringWithFormat:@"%@/userStatisticDetail/getCollectProjects", SERVER_URL]
//我的关注资金列表
#define HTTP_MYATTENTIONMONEY_QUERY_URL               [NSString stringWithFormat:@"%@/userStatisticDetail/getCollectFunds", SERVER_URL]
//我的项目列表
#define HTTP_MINEPROJECT_QUERY_URL             [NSString stringWithFormat:@"%@/userStatisticDetail/getMyProjects", SERVER_URL]
//我的资金列表
#define HTTP_MINEMONEY_QUERY_URL             [NSString stringWithFormat:@"%@/userStatisticDetail/getMyFunds", SERVER_URL]
//我的消息列表
#define HTTP_CHATSYSTEMNOTYLIST_QUERY_URL             [NSString stringWithFormat:@"%@/message/queryMessage", SERVER_URL]
/******************************聊天模块***********************************/


//读取消息
#define HTTP_READCHATSYSTEMNOTYLIST_QUERY_URL             [NSString stringWithFormat:@"%@/message/readMessage", SERVER_URL]

#endif
