//
//  AreaModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  区域 -model

#import "ZRB_BaseModel.h"

@interface AreaModel : ZRB_BaseModel

@property (nonatomic,copy) NSString *uid;

@property (nonatomic,copy) NSString *cityName;

@property (nonatomic,copy) NSString *postcode;

@property (nonatomic,copy) NSString *areacode;

@property (nonatomic,copy) NSString *cityLevel;

@property (nonatomic,copy) NSString *topCityId;

@property (nonatomic,copy) NSString *isRecommend;

@property (nonatomic,copy) NSString *img;

@property (nonatomic,weak) NSString *showName;
@end
