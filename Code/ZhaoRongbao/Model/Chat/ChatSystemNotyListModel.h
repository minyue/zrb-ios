//
//  ChatSystemNotyListModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/10.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  聊天-系统通知List model

#import "ZRB_BaseModel.h"

@class MessageInfo;

@interface ChatSystemNotyListModel : NSObject

@property (nonatomic,assign) int uid;
@property (nonatomic,assign) int status;
@property (nonatomic,copy) NSString *receiverUid;
@property (nonatomic,assign) int read;
@property (nonatomic,assign) int messageInfoId;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,assign) long long int createTime;
@property (nonatomic,strong) MessageInfo *messageInfo;

@end

@interface MessageInfo : NSObject
@property (nonatomic,copy) NSString *bizKey;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *bizType;
@property (nonatomic,copy) NSString *typeName;
@property (nonatomic,copy) NSString *senderUname;
@end
