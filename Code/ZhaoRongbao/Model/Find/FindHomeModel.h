//
//  FindHomeModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_BaseModel.h"
@class FindHomeItemModel;
@interface FindHomeModel : ZRB_BaseModel

@property (nonatomic,strong) NSMutableArray *data;

@property (nonatomic,strong) FindHomeItemModel *model;//add by zl
@end


@interface FindHomeItemModel : NSObject

@property (nonatomic,assign) NSInteger index;
@property (nonatomic,assign) BOOL isShowTimeFlag;

@property (nonatomic,copy) NSString *userName;
@property (nonatomic,copy) NSString *industryName;
@property (nonatomic,copy) NSString *userIndustryName;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *org;
@property (nonatomic,copy) NSString *messageAmount;
@property (nonatomic,copy) NSString *modeName;
@property (nonatomic,copy) NSString *attentionTimes;
@property (nonatomic,copy) NSString *attention;
@property (nonatomic,copy) NSString *districtName;
@property (nonatomic,copy) NSString *typeName;
@property (nonatomic,copy) NSString *position;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *intro;
@property (nonatomic,copy) NSString *industry;
@property (nonatomic,copy) NSString *note;
@property (nonatomic,copy) NSString *amount;
@property (nonatomic,copy) NSString *handlePerson;
@property (nonatomic,copy) NSString *pushTime;
@property (nonatomic,assign) int type;
@property (nonatomic,copy) NSString *handleTime;
@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *mode;
@property (nonatomic,copy) NSString *projectId;
@property (nonatomic,copy) NSString *district;
@property (nonatomic,copy) NSString *isValid;
@property (nonatomic,copy) NSString *status;
@property (nonatomic,copy) NSString *content;

@property (nonatomic,copy) NSString *authName;
@property (nonatomic,copy) NSString *auth;
@end