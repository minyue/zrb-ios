//
//  IndustryModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface FindIndustryModel : ZRB_BaseModel

@property (nonatomic,copy) NSMutableArray *data;

@end


@interface FindIndustryItemModel : NSObject

@property (nonatomic,copy) NSString *updateTime;

@property (nonatomic,copy) NSString *uid;

@property (nonatomic,copy) NSString *color;

@property (nonatomic,copy) NSString *industryLevel;

@property (nonatomic,copy) NSString *topIndustryId;

@property (nonatomic,copy) NSString *industryName;


@end