//
//  FindPersonalModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//  发现 个人名片 model

#import <Foundation/Foundation.h>
@class FindPersonalUserInfoModel;

@interface FindPersonalModel : NSObject
/***********用户资金列表*******************/
@property (nonatomic,copy) NSString *queryNum;
@property (nonatomic,copy) NSString *mid;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *pushTime;
@property (nonatomic,copy) NSString *collectNum;
@property (nonatomic,copy) NSString *cityInfo;
@property (nonatomic,copy) NSString *industry;
@property (nonatomic,copy) NSString *total;
@property (nonatomic,copy) NSString *auth;//502实地认证 503资料认证
@end

@interface FindPersonalUserInfoModel : NSObject
/***********用户信息*************/
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *Uid;
@property (nonatomic,copy) NSString *realName;
@property (nonatomic,copy) NSString *industryName;
@property (nonatomic,copy) NSString *industryId;
@property (nonatomic,copy) NSString *org;
@property (nonatomic,copy) NSString *position;
@property (nonatomic,copy) NSString *intentionCount;
@property (nonatomic,copy) NSString *umobile;
@property (nonatomic,copy) NSString *projectCount;
@property (nonatomic,copy) NSString *fundCount;

@end
