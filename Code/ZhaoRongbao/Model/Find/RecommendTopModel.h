//
//  RecommendTopModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/12.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecommendTopModel : NSObject

@property (nonatomic,copy) NSString *imageName;

@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSString *titleColor;

@end
