//
//  IndustryModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  行业Model

#import <Foundation/Foundation.h>

@interface IndustryModel : NSObject

@property (nonatomic,assign) int iid;

@property (nonatomic,copy) NSString *industryName;

@property (nonatomic,assign) int industryLevel;

@property (nonatomic,assign)long long updateTime;

@property (nonatomic,copy) NSString *color;
@end
