//
//  AttentionModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/23.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  关注Model

#import <Foundation/Foundation.h>

@interface AttentionModel : NSObject

@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSString *aid;

@property (nonatomic,copy) NSString *createTime;

@property (nonatomic,copy) NSString *typeName;

@property (nonatomic,assign) int type;

@property (nonatomic,copy) NSString *userName;

@property (nonatomic,copy) NSString *province;//地区

@property (nonatomic,copy) NSString *total;//融资金额

@property (nonatomic,copy) NSString *collectNum;//关注数

@property (nonatomic,copy) NSString *messageNum;//留言数

@end
