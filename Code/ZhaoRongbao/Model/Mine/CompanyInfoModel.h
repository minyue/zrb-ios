//
//  CompanyInfoModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/14.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  企业信息

#import <Foundation/Foundation.h>

@interface CompanyInfoModel : ZRB_BaseModel

@property (nonatomic,copy) NSString *district;

@property (nonatomic,copy) NSString *uid;

@property (nonatomic,copy) NSString *province;

@property (nonatomic,copy) NSString *disCode;

@property (nonatomic,copy) NSString *name;

@property (nonatomic,copy) NSString *city;

@end
