//
//  MineMoneyListModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的资金列表 model

#import <Foundation/Foundation.h>

@interface MineMoneyListModel : NSObject
@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSString *mid;

@property (nonatomic,copy) NSString *pushTime;

@property (nonatomic,copy) NSString *pic;

@property (nonatomic,assign) int collectNum;

@property (nonatomic,assign) int queryNum;
@end
