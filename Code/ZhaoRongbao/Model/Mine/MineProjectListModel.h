//
//  MineProjectListModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的项目cell


@interface MineProjectListModel : NSObject

@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSString *pid;

@property (nonatomic,copy) NSString *pushTime;

@property (nonatomic,copy) NSString *pic;

@property (nonatomic,assign) int collectNum;

@property (nonatomic,assign) int queryNum;

@end
