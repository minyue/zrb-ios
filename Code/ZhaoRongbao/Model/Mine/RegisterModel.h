//
//  RegisterModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/16.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_BaseModel.h"


/**
 *  SMS信息注册
 */
@interface RegisterNormalModel : ZRB_BaseModel

@property (nonatomic,copy) NSString *data;

@property (nonatomic,copy) NSString *bannerData;
@end



