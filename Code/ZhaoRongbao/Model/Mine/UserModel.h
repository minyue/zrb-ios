//
//  UserModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/21.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_BaseModel.h"


@interface ZRB_UserModel : NSObject

@property (nonatomic,copy) NSString *zrb_front_ut;

@property (nonatomic,copy) NSString *uid;

//@property (nonatomic,copy) NSString *org;//集团或政府

@property (nonatomic,copy) NSString *avatar;

@property (nonatomic,copy) NSString *uEmail;

@property (nonatomic,copy) NSString *orgId;

@property (nonatomic,copy) NSString *uname;

@property (nonatomic,assign) BOOL auth;

@property (nonatomic,copy) NSString *userTypeName;

@property (nonatomic,assign) IdentityType  userType; 

//add by zouli
@property (nonatomic,assign) OrgStatus status;

@property (nonatomic,strong) NSString *uMobile;

@property (nonatomic,strong) NSString *statusName;

@property (nonatomic,strong) NSString *IDCARD_PROVINCE;//省

@property (nonatomic,strong) NSString *IDCARD_CITY;//市

@property (nonatomic,strong) NSString *realname;

@property (nonatomic,strong) NSString *INFO;//个人说明

@property (nonatomic,strong) NSString *idCard;//身份证

@property (nonatomic,strong) NSString *LOCATIONNAME;

@property (nonatomic,strong) NSString *org;//公司名称

@property (nonatomic,strong) NSString *position;//担任职务

@property (nonatomic,strong) NSString *industry;//所属行业

@property (nonatomic,assign) int industryId;

@property (nonatomic,assign) int collectedNumber;//被关注数

//@property (nonatomic,assign) int intcollectProject;//关注项目数
@property (nonatomic,assign) int collectProject;//关注项目数
@property (nonatomic,assign) int collectFund;//关注资金数
@end


@interface LoginModel : ZRB_BaseModel

@property (nonatomic,strong) ZRB_UserModel  *model;

@property (nonatomic,copy)  NSString   *bannerData;

@property (nonatomic,copy)  NSString   *phoneNum;

@property (nonatomic,copy)  NSString   *userPwd;
@end





