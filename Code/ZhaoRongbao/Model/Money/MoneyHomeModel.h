//
//  MoneyHomeModel.h
//  ZhaoRongbao
//
//  找资金首页模型
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface MoneyHomeModel : ZRB_BaseModel

@property (nonatomic,strong) NSMutableArray *data;

@end


@interface MoneyHomeItemModel : NSObject

@property (nonatomic,copy) NSString *period;

@property (nonatomic,copy) NSString *uid;

@property (nonatomic,copy) NSString *industryName;

@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSString *pic;

@property (nonatomic,copy) NSString *areas;

@property (nonatomic,copy) NSString *authName;

@property (nonatomic,copy) NSString *total;

@property (nonatomic,copy) NSString *collectNum;

@property (nonatomic,copy) NSString *investMode;

@property (nonatomic,copy) NSString *pushTime;

@end