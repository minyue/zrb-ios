//
//  AllCommentModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/18.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface AllCommentModel : ZRB_BaseModel

@property (nonatomic,strong)NSMutableArray *data;

@end

@interface AllCommentItemModel : NSObject

@property (nonatomic,assign) int cid;
//"id": 8,
@property (nonatomic,copy) NSString *pid;

@property (nonatomic,copy) NSString *uid;

@property (nonatomic,copy) NSString *uname;

@property (nonatomic,copy) NSString *content;

@property (nonatomic,assign) int *targetId;

@property (nonatomic,copy) NSString *targetType;

@property (nonatomic,copy) NSString *targetName;

@property (nonatomic,assign) int status;

@property (nonatomic,copy) NSString *ctime;

@property (nonatomic,copy) NSString *uptime;

@property (nonatomic,copy) NSString *detime;

@property (nonatomic,assign) int *questionType;

@property (nonatomic,copy)NSString *picUrl;
@end