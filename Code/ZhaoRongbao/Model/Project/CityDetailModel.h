//
//  CityDetailModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface CityDetailModel : ZRB_BaseModel

@property (nonatomic,copy)NSString *province;
@property (nonatomic,copy)NSString *city;
@property (nonatomic,copy)NSString *keyWords;
@property (nonatomic,copy)NSString *gdp;
@property (nonatomic,copy)NSString *population;
@property (nonatomic,copy)NSString *pcdi;
@property (nonatomic,copy)NSString *urbanRate;
@property (nonatomic,copy)NSString *intro;
@property (nonatomic,copy)NSString *primaryIndustry;
@property (nonatomic,copy)NSString *secondIndustry;
@property (nonatomic,copy)NSString *tertiaryIndustry;
@property (nonatomic,copy)NSMutableArray *cityStatistics;

@end


@interface CityStatisticsModel : NSObject

@property (nonatomic,copy)NSString *date;
@property (nonatomic,copy)NSString *gdp;
@property (nonatomic,copy)NSString *gdpRate;

@end