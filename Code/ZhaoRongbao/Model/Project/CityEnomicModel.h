//
//  CityEnomicModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface CityEnomicModel : ZRB_BaseModel

@property (nonatomic,copy)NSMutableArray *data;

@end

@interface CityEnomicItemModel : NSObject

@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *value;

@end
