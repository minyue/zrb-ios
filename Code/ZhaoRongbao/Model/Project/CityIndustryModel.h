//
//  CityIndustryModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface CityIndustryModel : ZRB_BaseModel

@property (nonatomic,strong)NSMutableArray *data;

@end


@interface CityIndustryItemModel : NSObject

@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *value;
@property (nonatomic,strong)NSMutableArray *company;

@end