//
//  ProjectImageModel.h
//  ZhaoRongbao
//
//  Created by zouli on 15/10/23.
//  Copyright © 2015年 zouli. All rights reserved.
//  项目图片Model

#import <Foundation/Foundation.h>

@interface ProjectImageModel : NSObject

@property (nonatomic,copy) NSString *bizzid;

@property (nonatomic,copy) NSString *iid;//id

@property (nonatomic,copy) NSString *addtime;

@property (nonatomic,copy) NSString *name;//URL

@property (nonatomic,copy) NSString *url;//URL

@property (nonatomic,copy) NSString *remark;//备注

@end
