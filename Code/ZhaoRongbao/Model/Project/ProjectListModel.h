//
//  ProjectListModel.h
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/12.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_BaseModel.h"


/**
 *  项目列表
 */
@interface ProjectListModel : ZRB_BaseModel

@property (nonatomic,strong) NSMutableArray   *projectList;
@property (nonatomic,strong) NSMutableArray   *bannerList;

@end


//@interface ProjectDataModel : ZRB_BaseModel
//
//
//
//@end

/**
 *  项目实体
 */
@interface ProjectItemModel : NSObject

@property (nonatomic,copy) NSString  *autoId;

@property (nonatomic,copy) NSString  *projectId;

@property (nonatomic,copy) NSString  *pppType;

@property (nonatomic,copy) NSString  *pppTypeZH;

@property (nonatomic,copy) NSString  *projectName;

@property (nonatomic,assign) int projectType;

@property (nonatomic,copy) NSString  *projectTypeZH;

@property (nonatomic,copy) NSString  *industryId;

@property (nonatomic,copy) NSString  *provincePostcode;

@property (nonatomic,copy) NSString  *province;

@property (nonatomic,copy) NSString  *cityPostcode;

@property (nonatomic,copy) NSString  *city;

@property (nonatomic,copy) NSString  *countyPostcode;

@property (nonatomic,copy) NSString  *county;

@property (nonatomic,copy) NSString  *insertTime;

@property (nonatomic,copy) NSString  *updateTime;

@property (nonatomic,copy) NSString  *pushTime;

@property (nonatomic,copy) NSString  *pic;

@property (nonatomic,copy) NSString  *bigPic;

@property (nonatomic,copy) NSString  *intro;

@property (nonatomic,copy) NSString *investmentStr;

@property (nonatomic,copy) NSString *investmentStatus;

@property (nonatomic,copy) NSString  *keywords;

@property (nonatomic,copy) NSString  *hotValue;

@property (nonatomic,assign) int isValidate;

@property (nonatomic,copy) NSString  *isValidateZH;

@property (nonatomic,copy) NSString  *nameMd5;

@property (nonatomic,assign) int  projectStatus;

@property (nonatomic,copy) NSString  *projectStatusZH;

@property (nonatomic,assign) int isCase;

@property (nonatomic,assign) int area;

@property (nonatomic,assign) long long tradeDate;

@property (nonatomic,assign) int isRecommend;

@property (nonatomic,assign) int source;

@property (nonatomic,copy) NSString  *shortName;

@property (nonatomic,copy) NSString  *assetType;

@property (nonatomic,copy) NSString  *assetTypeZH;

@property (nonatomic,copy) NSString *purpose;

@property (nonatomic,copy) NSString *purposeZH;

@property (nonatomic,copy) NSString *collectionTime;

/**********************for 我的项目-Cell的Model***********************/

@property (nonatomic,assign) int inspectNum;
@property (nonatomic,assign) int commentNum;
@property (nonatomic,assign) int collectionNum;

/**********************for 我的项目详情的Model***********************/
@property (nonatomic,copy)  NSString *period;//招商时长
@property (nonatomic,assign) int viewNum;
@property (nonatomic,copy)  NSString *investment;//项目总额
@property (nonatomic,assign) int inviteNum;
@property (nonatomic,copy)  NSArray *shareStatistic;
@property (nonatomic,copy)  NSArray *inspectStatistics;
@property (nonatomic,copy)  NSArray *commentStatistics;
@property (nonatomic,copy)  NSArray *collectionStatistics;
@end






