//
//  ProjectTypeModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/18.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface ProjectTypeModel : ZRB_BaseModel

@property (nonatomic,copy) NSMutableArray *data;

@end


/**
 *  项目类型实体类
 */
@interface ProjectTypeItemModel : NSObject

@property (nonatomic,copy)NSString *pid;

@property (nonatomic,copy)NSString *itemName;

@property (nonatomic,copy)NSString *itemCode;

@property (nonatomic,copy)NSString *topItemCode;

@property (nonatomic,copy)NSString *status;

@property (nonatomic,copy)NSString *weight;

@property (nonatomic,copy)NSString *parentItem;

@end