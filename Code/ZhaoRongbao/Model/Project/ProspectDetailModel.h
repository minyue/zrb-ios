//
//  ProspectDetailModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察详情页面

#import "ProspectModel.h"
#import "ProjectListModel.h"

@interface ProspectDetailModel : ProspectModel
@property (nonatomic,copy) NSString *cancelReason;
@property (nonatomic,copy) NSString *reasonTypeZH;
@property (nonatomic,copy) NSMutableArray *projectInfoVo;//ProjectListModel
@property (nonatomic,copy) NSMutableArray *event;//ProspectEventModel
@property (nonatomic,copy) NSString *geoCoor;//经度，纬度
@end

@interface ProspectEventModel : NSObject
@property (nonatomic,copy) NSString *type;
@property (nonatomic,assign) int id;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,assign) int inspectId;
@property (nonatomic,assign) long long ctime;
@end