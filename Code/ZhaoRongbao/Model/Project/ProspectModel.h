//
//  ProspectModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/2.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  勘察 Model

#import "ZRB_BaseModel.h"

@interface ProspectModel : ZRB_BaseModel
@property (nonatomic,assign) int autoId;
@property (nonatomic,assign) int userId;
@property (nonatomic,assign) int projectId;
@property (nonatomic,copy) NSString *projectName;
@property (nonatomic,assign) long long inspectTime;
@property (nonatomic,assign) long long updateTime;
@property (nonatomic,copy) NSString *contact;
@property (nonatomic,assign) int personNum;
@property (nonatomic,copy) NSString *telphone;
@property (nonatomic,copy) NSString *companyName;
@property (nonatomic,assign) long long insertTime;
@property (nonatomic,assign) int inspectStatus;
@property (nonatomic,copy) NSString *projectPicUrl;

/***********************for UI*************************/
@property (nonatomic,assign) int selectIndex;
@end
