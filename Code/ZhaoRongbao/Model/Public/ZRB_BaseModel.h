//
//  ZRB_BaseModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZRB_BaseModel : NSObject

@property (nonatomic,assign) int res;

@property (nonatomic,copy) NSString *msg;

@end
