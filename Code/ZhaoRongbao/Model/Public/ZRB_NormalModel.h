//
//  ZRB_NormalModel.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/30.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface ZRB_NormalModel : ZRB_BaseModel

@property (nonatomic,assign) id   data;

@end
