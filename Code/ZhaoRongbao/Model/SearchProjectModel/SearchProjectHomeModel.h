//
//  SearchProjectHomeModel.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_BaseModel.h"

@interface SearchProjectHomeModel : ZRB_BaseModel

@property (nonatomic,strong) NSMutableArray *data;

@end


@interface SearchProjectHomeItemModel : NSObject


@property (nonatomic,copy) NSString *userId;

@property (nonatomic,copy) NSString *projectNo;

@property (nonatomic,copy) NSString *source;

@property (nonatomic,copy) NSString *province;

@property (nonatomic,copy) NSString *total;

@property (nonatomic,copy) NSString *projectStatus;

@property (nonatomic,copy) NSString *updateTime;

@property (nonatomic,copy) NSString *qryEndDate;

@property (nonatomic,copy) NSString *qryKeyWord;

@property (nonatomic,copy) NSString *modeStr;

@property (nonatomic,copy) NSString *industryStr;

@property (nonatomic,copy) NSString *provincePostcode;

@property (nonatomic,copy) NSString *city;

@property (nonatomic,copy) NSString *qryStartDate;

@property (nonatomic,copy) NSString *projectName;

@property (nonatomic,copy) NSString *uid;

@property (nonatomic,copy) NSString *insertTime;

@property (nonatomic,copy) NSString *pic;

@property (nonatomic,copy) NSString *auth;

@property (nonatomic,copy) NSString *intentionId;

@property (nonatomic,copy) NSString *intentionStr;

@property (nonatomic,copy) NSString *industryColor;

@property (nonatomic,copy) NSString *authStr;

@property (nonatomic,copy) NSString *phone;

@property (nonatomic,copy) NSString *pushtimeStr;

@property (nonatomic,copy) NSString *pusher;

@property (nonatomic,copy) NSString *insertTimeStr;

@property (nonatomic,copy) NSString *cityPostcode;

@property (nonatomic,copy) NSString *userName;

@property (nonatomic,copy) NSString *pushtime;

@property (nonatomic,copy) NSString *mode;

@property (nonatomic,copy) NSString *projectStatusStr;

@property (nonatomic,copy) NSString *industryId;



@end