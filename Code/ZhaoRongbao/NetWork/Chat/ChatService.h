//
//  ChatService.h
//  ZhaoRongbao
//
//  Created by zouli on 15/9/10.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "AFBaseService.h"
@class ChatSystemNotyQueryListRequest;

@interface ChatService : AFBaseService
/**
 *  系统通知列表
 *
 *  @param request ChatSystemNotyQueryListRequest
 *  @param success NSMUtableArray
 *  @param fail    nil
 */
- (void)chatSystemNotyQueryListWithRequest:(ChatSystemNotyQueryListRequest *)request
                                   success:(void (^)(id responseObject))success
                                   failure:(void (^)(NSError *error))fail;

//读取系统消息
- (void)readSystemNotyMessageWithRequest:(NSString*)mId
                                 success:(void(^)(BOOL back))success
                                 failure:(void(^)(NSError *error))fail;
@end

/**
 *  系统通知列表请求
 */
@interface ChatSystemNotyQueryListRequest : NSObject

@property (nonatomic,assign) int        maxId;

@property (nonatomic,assign) int      limit;    //最多一批返回多少


@end