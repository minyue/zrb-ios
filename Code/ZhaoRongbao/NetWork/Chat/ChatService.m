//
//  ChatService.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/10.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ChatService.h"
#import "ChatSystemNotyListModel.h"

@implementation ChatService

/**
 *  系统通知列表
 *
 *  @param request ChatSystemNotyQueryListRequest
 *  @param success NSMUtableArray
 *  @param fail    nil
 */
- (void)chatSystemNotyQueryListWithRequest:(ChatSystemNotyQueryListRequest *)request
                                    success:(void (^)(id responseObject))success
                                    failure:(void (^)(NSError *error))fail
{
    if(!request)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@(request.limit),@"limit",nil];
    if (request.maxId)
    {
        [dic setObject:@(request.maxId) forKey:@"maxId"];
    }
    
    [[HttpClient sharedClient] POST:HTTP_CHATSYSTEMNOTYLIST_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"%@",[dic dictionaryToJson]);
        NSString    *res = (NSString*)[dic valueForKey:@"res"];
        if([res longLongValue] == 1){
            NSMutableArray *array = [dic valueForKey:@"data"];
            NSMutableArray  *returnArray = [[NSMutableArray alloc] init];
            if(array){
                for(NSDictionary *dic1 in array){
                    ChatSystemNotyListModel *model = [ChatSystemNotyListModel objectWithKeyValues:dic1];
                    model.uid = [[dic1 valueForKey:@"id"] intValue];
                    model.messageInfo = [MessageInfo objectWithKeyValues:(NSDictionary*)[dic1 valueForKey:@"messageInfo"]];
                    [returnArray addObject:model];
                }
            }
            success(returnArray);
        }else{
            success(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];

}

//读取系统消息
- (void)readSystemNotyMessageWithRequest:(NSString*)mId
                            success:(void(^)(BOOL back))success
                            failure:(void(^)(NSError *error))fail{
    if(!mId)
        return;
    
    NSMutableDictionary *mdic = [[NSMutableDictionary    alloc]initWithObjectsAndKeys:[[ZRB_UserManager shareUserManager] userData].uid,@"receiverUid",mId,@"id", nil];

    [[HttpClient sharedClient] POST:HTTP_READCHATSYSTEMNOTYLIST_QUERY_URL
                                       parameters:mdic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           NSDictionary  *dic = responseObject;
                                           NSLog(@"读取信息---%@",[dic dictionaryToJson]);
                                           if ([[dic objectForKey:@"res"] intValue] == 1) {
                                               success(YES);
                                           }else{
                                               success(NO);
                                           }
                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                           if (fail) {
                                               fail(error);
                                           }
                                       }];
}

@end


@implementation ChatSystemNotyQueryListRequest



@end