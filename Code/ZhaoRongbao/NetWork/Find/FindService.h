//
//  FindService.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "AFBaseService.h"

@class FindHomeRequest;
@class PushProjectOrSumRequest;
@class IndustryRequest;
@class MessageRequest;
@class FindDetailRequest;
@class PersonalMoneyListRequest;
@class PersonalIntentListRequest;

@interface FindService : AFBaseService
/**
 *  会员名片用户信息
 */
- (void)findPersonalUserInfoWithId:(NSString *)uid
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))failure;

/**
 *  会员名片意向列表
 */
- (void)findPersonalIntentWithRequest:(PersonalIntentListRequest *)request
                              success:(void (^)(id responseObject))success
                              failure:(void (^)(NSError *error))failure;

/**
 *  会员名片资金列表
 */
- (void)findPersonalMoneyListWithRequest:(PersonalMoneyListRequest *)request
                                 success:(void (^)(id responseObject))success
                                 failure:(void (^)(NSError *error))failure;

/**
 *  发现首页
 *
 *  @param request  请求类
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 *
 */
- (void)findHomeWithRequest:(FindHomeRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  发布意向（项目和资金）
 *
 *  @param request  请求类
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 *
 */
- (void)pushProjectOrSumWithRequest:(PushProjectOrSumRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  修改意向（项目和资金）
 *
 *  @param rid 意向ID
 */
- (void)editProjectOrSumWithRequest:(PushProjectOrSumRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  删除意向（项目和资金）
 *
 *  @param rid 意向ID
 */
- (void)deleteProjectOrSumWithId:(NSString *)rid success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  投资行业（项目和资金）
 *
 *  @param request  请求类
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 *
 */
- (void)industryWithRequest:(IndustryRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  留言
 *
 *  @param request  请求类
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 *
 */
- (void)messageWithRequest:(MessageRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  意向详情
 *
 *  @param request  请求类
 *  @param success  Web页面
 *  @param failure  请求失败回调
 *
 */
- (void)intentDetailWithRequest:(FindDetailRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  意向详情
 *
 *  @param request  请求类
 *  @param success  Json
 *  @param failure  请求失败回调
 *
 */
- (void)intentDetailDataWithRequest:(FindDetailRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

@end

/**
 *  会员名片意向列表请求类
 */
@interface PersonalIntentListRequest : NSObject
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *lastPushTime;//时间戳
@property (nonatomic,copy) NSString *limit;
@property (nonatomic,copy) NSString *maxId;

@end


/**
 *  会员名片信息资金列表请求类
 */
@interface PersonalMoneyListRequest : NSObject
@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *pushTime;//时间戳
@property (nonatomic,copy) NSString *limit;
@property (nonatomic,copy) NSString *maxId;

@end

/**
 *  发现首页列表请求类
 */
@interface FindHomeRequest : NSObject

@property (nonatomic,copy) NSString *maxId;
@property (nonatomic,copy) NSString *limit;

@end


/**
 *  发布项目/资金请求类
 */
@interface PushProjectOrSumRequest : NSObject

@property (nonatomic,strong) NSString *intro;         //简介

@property (nonatomic,copy) NSString *industry;        //投资行业行业ID,多个用#隔开，例如：@“32#34#89”

@property (nonatomic,copy) NSString *district;        //投资区域

@property (nonatomic,copy) NSString *amount;          //投资总额

@property (nonatomic,copy) NSString *mode;            //发布类型，项目/资金

@property (nonatomic,copy) NSString *type;            //投资方式

@property (nonatomic,copy) NSString *content;         //详情

@property (nonatomic,copy) NSString *uid;             //意向id

@end


/**
 *  投资行业
 */
@interface IndustryRequest : NSObject

@property (nonatomic,copy) NSString *type;

@end

/**
 *  留言
 */
@interface MessageRequest : NSObject

@property (nonatomic,strong) NSString *intentionId;
@property (nonatomic,strong) NSString *content;

@end

/**
 *  更新意向
 */


@interface FindDetailRequest : NSObject

@property (nonatomic,strong) NSString *intentId;

@end



