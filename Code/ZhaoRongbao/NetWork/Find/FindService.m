//
//  FindService.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindService.h"
#import "FindIndustryModel.h"
#import "FindHomeModel.h"
#import "PushModel.h"
#import "FindMessageModel.h"
#import "FindPersonalModel.h"
#import "ZRB_NormalModel.h"
//#define HTTP_URL_FIND                @"http://172.17.32.88:8080"



@implementation FindService

//名片用户信息
- (void)findPersonalUserInfoWithId:(NSString *)uid success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    if(!uid)
        return;
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:uid forKey:@"userId"];
    
    LogInfo(@"名片信息参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_PERSONAL_USERINFO parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"名片信息列表：%@",[dic dictionaryToJson]);
        FindPersonalUserInfoModel *itemModel = [FindPersonalUserInfoModel objectWithKeyValues:[dic objectForKey:@"data"]];
        
        if(success){
            success(itemModel);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

/**
 *  会员名片意向列表
 */
- (void)findPersonalIntentWithRequest:(PersonalIntentListRequest *)request
                              success:(void (^)(id responseObject))success
                              failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.uid forKey:@"uid"];
    [params setObject:request.limit forKey:@"limit"];
    if (request.maxId.length > 0) {
        [params setObject:request.maxId forKey:@"maxId"];
    }
   
    [[HttpClient sharedClient] POST:HTTP_PERSONAL_INTENT_LIST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"名片意向列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            [FindHomeItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"uid":@"id"};
            }];
            FindHomeItemModel *itemModel = [FindHomeItemModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        
        FindHomeModel *model = [[FindHomeModel alloc]init];
        model.data = array;
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

////名片资金列表
//- (void)findPersonalMoneyListWithRequest:(PersonalMoneyListRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
//    
//    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
//    [params setObject:request.userId forKey:@"userId"];
//    [params setObject:request.limit forKey:@"limit"];
////    [params setObject:request.pushTime forKey:@"pushTime"];
//    [params setObject:request.maxId forKey:@"maxId"];
//    
//    LogInfo(@"名片资金列表参数：%@",[params dictionaryToJson]);
//    
//    [[HttpClient sharedClient] POST:HTTP_PERSONAL_FINDLIST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSDictionary *dic = responseObject;
//        LogInfo(@"列表：%@",[dic dictionaryToJson]);
//        NSMutableArray *array = [[NSMutableArray alloc]init];
//        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
//            
//            [FindPersonalModel setupReplacedKeyFromPropertyName:^NSDictionary *{
//                return @{@"mid":@"id"};
//            }];
//            
//            FindPersonalModel *itemModel = [FindPersonalModel objectWithKeyValues:modelDic];
//            [array addObject:itemModel];
//        }
//        
//        FindHomeModel *model = [[FindHomeModel alloc]init];
//        model.res = [[dic objectForKey:@"res"] intValue];
//        model.data = array;
//        
//        if(success){
//            success(model);
//        }
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        if (failure) {
//            failure(error);
//        }
//    }];
//    
//}

//名片资金列表
- (void)findPersonalMoneyListWithRequest:(PersonalMoneyListRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    LogInfo(@"名片资金列表");
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.userId forKey:@"userId"];
    [params setObject:request.limit forKey:@"limit"];
    //    [params setObject:request.pushTime forKey:@"pushTime"];
    [params setObject:request.maxId forKey:@"maxId"];
    
    LogInfo(@"名片资金列表参数：%@",[params dictionaryToJson]);
    [[HttpClient sharedClient] POST:HTTP_PERSONAL_FINDLIST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"名片资金列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            
            [FindHomeItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"uid":@"id"};
            }];
            
            FindHomeItemModel *itemModel = [FindHomeItemModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        
        FindHomeModel *model = [[FindHomeModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        model.data = array;
        
        if(success){
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

//    [[HttpClient sharedClient] POST:HTTP_PERSONAL_FINDLIST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSDictionary *dic = responseObject;
//        LogInfo(@"列表：%@",[dic dictionaryToJson]);
//        NSMutableArray *array = [[NSMutableArray alloc]init];
//        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
//            
//            [FindPersonalModel setupReplacedKeyFromPropertyName:^NSDictionary *{
//                return @{@"mid":@"id"};
//            }];
//            
//            FindPersonalModel *itemModel = [FindPersonalModel objectWithKeyValues:modelDic];
//            [array addObject:itemModel];
//        }
//        
//        FindHomeModel *model = [[FindHomeModel alloc]init];
//        model.res = [[dic objectForKey:@"res"] intValue];
//        model.data = array;
//        
//        if(success){
//            success(model);
//        }
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        if (failure) {
//            failure(error);
//        }
//    }];
    
}


- (void)findHomeWithRequest:(FindHomeRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.maxId forKey:@"maxId"];
    [params setObject:request.limit forKey:@"limit"];
    
    LogInfo(@"发现列表参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_FIND_HOME parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"发现列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            
            [FindHomeItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"uid":@"id"};
            }];
            
            FindHomeItemModel *itemModel = [FindHomeItemModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        
        FindHomeModel *model = [[FindHomeModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        model.data = array;
        
        if(success){
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

- (void)pushProjectOrSumWithRequest:(PushProjectOrSumRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.intro forKey:@"intro"];
    [params setObject:request.industry forKey:@"industry"];
    [params setObject:request.district forKey:@"district"];
    [params setObject:request.amount forKey:@"amount"];
    [params setObject:request.mode forKey:@"mode"];
    [params setObject:request.type forKey:@"type"];
    [params setObject:request.content forKey:@"content"];
    
    LogInfo(@"发布意向参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_PUSH_INTENT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"意向返回：%@",[dic dictionaryToJson]);
        
        PushModel *model = [[PushModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if(success){
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

//修改意向
- (void)editProjectOrSumWithRequest:(PushProjectOrSumRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.intro forKey:@"intro"];
    [params setObject:request.industry forKey:@"industry"];
    [params setObject:request.district forKey:@"district"];
    [params setObject:request.amount forKey:@"amount"];
    [params setObject:request.mode forKey:@"mode"];
    [params setObject:request.type forKey:@"type"];
    [params setObject:request.content forKey:@"content"];
    [params setObject:request.uid forKey:@"id"];
    
    LogInfo(@"修改意向参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_EDIT_INTENT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"意向返回：%@",[dic dictionaryToJson]);
        
        PushModel *model = [[PushModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if(success){
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

//删除意向
- (void)deleteProjectOrSumWithId:(NSString *)rid success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:rid forKey:@"id"];
    
    LogInfo(@"删除意向参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_DELETE_INTENT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"意向返回：%@",[dic dictionaryToJson]);
        
        PushModel *model = [[PushModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if(success){
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (void)industryWithRequest:(IndustryRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    
    if (request.type.length > 1) {
        [params setObject:request.type forKey:@"type"];
    }
    LogInfo(@"行业参数：%@",[params  dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_ALL_INDUSTRY parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"行业：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            
            [FindIndustryItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"uid":@"id"};
            }];
            FindIndustryItemModel *itemModel = [FindIndustryItemModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        
        FindIndustryModel *model = [[FindIndustryModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        model.data = array;
        
        if(success){
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

- (void)messageWithRequest:(MessageRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.intentionId forKey:@"intentionId"];
    [params setObject:request.content forKey:@"content"];
    
    LogInfo(@"留言参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_INTENT_MESSAGE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"留言：%@",[dic dictionaryToJson]);
    
        FindMessageModel *model = [[FindMessageModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}


- (void)intentDetailWithRequest:(FindDetailRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.intentId forKey:@"id"];
    
    LogInfo(@"详情参数：%@",params);
    LogInfo(@"%@?id=%@",[NSString stringWithFormat:@"%@/intention/details",SERVER_URL],request.intentId);
    [[HttpClient sharedClient] POST:HTTP_INTENT_DETAIL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"意向详情：%@",[dic dictionaryToJson]);
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

- (void)intentDetailDataWithRequest:(FindDetailRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.intentId forKey:@"id"];
    
    LogInfo(@"详情参数：%@",params);
    LogInfo(@"%@?id=%@",[NSString stringWithFormat:@"%@/intention/details",SERVER_URL],request.intentId);
    [[HttpClient sharedClient] POST:HTTP_DATA_INTENT_DETAIL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = responseObject;
        [FindHomeItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{@"uid":@"id"};
        }];
        
        FindHomeItemModel *itemModel = [FindHomeItemModel objectWithKeyValues:[dic objectForKey:@"data"]];
        
        
        FindHomeModel *model = [[FindHomeModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        model.model = itemModel;
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}



@end

@implementation PersonalMoneyListRequest

@end

@implementation PersonalIntentListRequest


@end

@implementation FindHomeRequest


@end

@implementation PushProjectOrSumRequest


@end

@implementation IndustryRequest


@end

@implementation MessageRequest


@end

@implementation FindDetailRequest

@end