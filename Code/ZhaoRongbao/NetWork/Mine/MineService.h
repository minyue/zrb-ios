//
//  MineService.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/30.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "AFBaseService.h"
@class PublishRequest;
@class AttentionFindRequest;
@class AttentionProjectRequest;
@class AttentionMoneyRequest;
@class MineProjectListRequest;
@class MineMoneyListRequest;

@interface MineService : AFBaseService

/**
 *  上传用户图像
 *
 *  @param file         图像
 *  @param success      成功回调
 *  @param fail         失败回调
 */
- (void)fileUpload:(NSString *)file
           success:(void (^)(id responseObject))success
           failure:(void (^)(NSError *error))fail;


/**
 *  获取当前用户所有信息
 *
 *  return ZRB_UserModel
 */
-(void)getCurrentUserInfoWithsuccess:(void (^)(id responseObject))success
                        failure:(void (^)(void))fail;

/**
 *  获取当前用户主页的关注次数及被关注次数等
 *
 *  return ZRB_UserModel
 */
-(void)getCurrentUserNumStatisticWithsuccess:(void (^)(id responseObject))success
                                failure:(void (^)(void))fail;

/**
 *  修改用户基本信息
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)updateAppUserWithRequest:(NSDictionary*)dic
                         success:(void (^)(id responseObject))success
                         failure:(void (^)(NSError *error))fail;

/**
 *  更换用户手机号
 *
 *  @param dic     手机号 + 验证码
 *  @param success BOOL 成功
 *  @param fail    失败
 */
- (void)changeUserMobileWithRequest:(NSDictionary*)dic
                            success:(void(^)(BOOL back))success
                            failure:(void(^)(NSError *error))fail;

/**
 *  发送修改手机号短信验证码
 *
 *  @param tel     电话
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)sendUpdateUmobileSMSRegisterWithTel:(NSString *)tel
                                    success:(void (^)(id responseObject))success
                                    failure:(void (^)(NSError *error))fail;

/**
 *  校验密码是否一致
 *
 *  @param dic     用户密码
 *  @param success BOOL 成功
 *  @param fail    失败
 */
- (void)checkUserPWDWithRequest:(NSString*)pwd
                        success:(void(^)(id responseObject))success
                        failure:(void(^)(NSError *error))fail;


/**
 *  修改密码
 *
 *  @param dic     用户密码
 *  @param success BOOL 成功
 *  @param fail    失败
 */
- (void)changeUserPWDWithRequest:(NSString*)pwd
                         success:(void(^)(id responseObject))success
                         failure:(void(^)(NSError *error))fail;
/**
 *  获取所有省份信息
 *
 *  return AreaModel
 */
-(void)getAllProvinceInfoWithSuccess:(void (^)(id responseObject))success
                             failure:(void (^)(void))fail;

/**
 *  获取所有市区信息
 *
 *  return AreaModel List
 */
-(void)getAllCitynfoWithProvinceCode:(NSString*)code
                             success:(void (^)(id responseObject))success
                             failure:(void (^)(void))fail;

/**
 *  保存定位到的地区信息
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调 location
 */
- (void)updateAppUserWithLocationAtRequest:(NSDictionary*)dic
                                   success:(void (^)(id responseObject))success
                                   failure:(void (^)(NSError *error))fail;



/**
 *  用户意见反馈
 */
- (void)userFeedbackZRBRequest:(NSString*)content
                    andContact:(NSString*)contact
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail;

/**
 *  我的发布列表
 *
 *  @param request PublishRequest
 *  @param success
 *  @param fail    nil
 */
- (void)minePublishQueryWithRequest:(PublishRequest *)request
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail;

/**
 *  关注发现列表
 *
 *  @param request PublishRequest
 *  @param success
 *  @param fail    nil
 */
- (void)mineAttentionFindQueryWithRequest:(AttentionFindRequest *)request
                                  success:(void (^)(id responseObject))success
                                  failure:(void (^)(NSError *error))fail;

/**
 *  关注项目列表
 *
 *  @param request PublishRequest
 *  @param success
 *  @param fail    nil
 */
- (void)mineAttentionProjectQueryWithRequest:(AttentionProjectRequest *)request
                                     success:(void (^)(id responseObject))success
                                     failure:(void (^)(NSError *error))fail;

/**
 *  关注资金列表
 *
 *  @param request MoneyRequest
 *  @param success
 *  @param fail    nil
 */
- (void)mineAttentionMoneyQueryWithRequest:(AttentionMoneyRequest *)request
                                   success:(void (^)(id responseObject))success
                                   failure:(void (^)(NSError *error))fail;

/**
 *  我的项目列表
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)mineProjectQueryListWithRequest:(MineProjectListRequest *)request
                                success:(void (^)(id responseObject))success
                                failure:(void (^)(NSError *error))fail;

/**
 *  我的资金列表
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)mineMoneyQueryListWithRequest:(MineMoneyListRequest *)request
                              success:(void (^)(id responseObject))success
                              failure:(void (^)(NSError *error))fail;
@end

@interface PublishRequest : NSObject

@property (nonatomic,assign) int limit;

@property (nonatomic,assign) long long int maxId;

@end

@interface AttentionFindRequest : NSObject

@property (nonatomic,assign) int limit;

@property (nonatomic,copy) NSString *createTime;

@end

@interface AttentionProjectRequest : NSObject

@property (nonatomic,assign) int limit;

@property (nonatomic,copy) NSString *createTime;

@end

@interface AttentionMoneyRequest : NSObject

@property (nonatomic,assign) int limit;

@property (nonatomic,copy) NSString *createTime;

@end

@interface MineProjectListRequest : NSObject

@property (nonatomic,assign) int limit;

@property (nonatomic,copy) NSString *pushTime;

@end

@interface MineMoneyListRequest : NSObject

@property (nonatomic,assign) int limit;

@property (nonatomic,copy) NSString *pushTime;

@end

