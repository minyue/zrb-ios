//
//  MineService.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/30.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineService.h"
#import "ZRB_NormalModel.h"
#import "AreaModel.h"
#import "RegisterModel.h"
#import "FindHomeModel.h"
#import "AttentionModel.h"
#import "MineProjectListModel.h"
#import "MineMoneyListModel.h"

@implementation MineService


/**
 *  上传用户图像
 *
 *  @param file         图像
 *  @param success      成功回调
 *  @param fail         失败回调
 */
- (void)fileUpload:(NSString *)file
           success:(void (^)(id responseObject))success
           failure:(void (^)(NSError *error))fail
{
    [[HttpClient sharedClient] POST:HTTP_UPLOADIMAGE_URL
                                       parameters:nil
                        constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:file] name:@"uploadExcel" error:nil];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"upload image Response:\n%@",[dic dictionaryToJson]);

        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
        if (success) {
            success(model);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];

}

/**
 *  获取当前用户所有信息
 *
 *  return ZRB_UserModel
 */
-(void)getCurrentUserInfoWithsuccess:(void (^)(id responseObject))success
                        failure:(void (^)(void))fail{
    [[HttpClient sharedClient] POST:HTTP_GETUSERINFO_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"CurrentUser:   \n%@",[dic dictionaryToJson]);
        ZRB_UserModel  *model = [ZRB_UserModel objectWithKeyValues:[dic objectForKey:@"data"]];
        if (success) {
            success(model);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail();
        }
    }];
}

/**
 *  获取当前用户主页的关注次数及被关注次数等
 *
 *  return ZRB_UserModel
 */
-(void)getCurrentUserNumStatisticWithsuccess:(void (^)(id responseObject))success
                        failure:(void (^)(void))fail{
    [[HttpClient sharedClient] POST:HTTP_GETUSERNUMSTATISTIC_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"CurrentNumStatistic:   \n%@",[dic dictionaryToJson]);
        ZRB_UserModel  *model = [ZRB_UserModel objectWithKeyValues:[dic objectForKey:@"data"]];
        if (success) {
            success(model);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail();
        }
    }];
}

/**
 *  修改用户基本信息
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)updateAppUserWithRequest:(NSDictionary*)dic
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail
{
    if(!dic)
        return;
    
    NSMutableDictionary *mdic = [[NSMutableDictionary    alloc]initWithDictionary:dic];
    
    [[HttpClient sharedClient] POST:HTTP_UPDATEAPPUSER_URL
                                       parameters:mdic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           NSDictionary  *dic = responseObject;
                                           NSLog(@"修改用户信息----%@",[dic dictionaryToJson]);
                                           
                                           ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
                                           
                                           if (success) {
                                               success(model);
                                           }
                                           
                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                           if (fail) {
                                               fail(error);
                                           }
                                       }];
}

/**
 *  更换用户手机号
 *
 *  @param dic     手机号 + 验证码
 *  @param success BOOL 成功
 *  @param fail    失败
 */
- (void)changeUserMobileWithRequest:(NSDictionary*)dic
                            success:(void(^)(BOOL back))success
                            failure:(void(^)(NSError *error))fail{
    if(!dic)
        return;
    
    NSMutableDictionary *mdic = [[NSMutableDictionary    alloc]initWithDictionary:dic];
    [[HttpClient sharedClient] POST:HTTP_UPDATEAPPUSERMOBILE_URL
                                       parameters:mdic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           NSDictionary  *dic = responseObject;
                                           NSLog(@"修改用户手机号---%@",[dic dictionaryToJson]);
                                           if ([[dic objectForKey:@"res"] intValue] == 1) {
                                               success(YES);
                                           }else{
                                               success(NO);
                                           }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         if (fail) {
             fail(error);
         }
    }];
}

/**
 *  发送修改手机号短信验证码
 *
 *  @param tel     电话
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)sendUpdateUmobileSMSRegisterWithTel:(NSString *)tel
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail
{
    
    NSDictionary *dic = @{@"mobile":@(tel.longLongValue)};
    if(!tel)
        return;
    
    NSMutableDictionary *mdic = [[NSMutableDictionary    alloc]initWithDictionary:dic];
    [[HttpClient sharedClient] POST:HTTP_UPDATEUMOBILESMS_URL
                                       parameters:mdic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              
                                              NSDictionary  *dic = responseObject;
                                              NSLog(@"%@",[dic dictionaryToJson]);
                                              
                                              
                                              RegisterNormalModel *sms = [RegisterNormalModel objectWithKeyValues:dic];
                                              
                                              if (success) {
                                                  success(sms);
                                              }
                                              
                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              if (fail) {
                                                  fail(error);
                                              }
                                          }];
    
}

/**
 *  校验密码是否一致
 *
 *  @param dic     用户密码
 *  @param success BOOL 成功
 *  @param fail    失败
 */
- (void)checkUserPWDWithRequest:(NSString*)pwd
                            success:(void(^)(id responseObject))success
                            failure:(void(^)(NSError *error))fail{
    if(!pwd)
        return;
    
    NSDictionary *dic = @{@"passWord":pwd};
    [[HttpClient sharedClient] POST:HTTP_UPDATEAPPUSERPWDCHECK_URL
                                       parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           NSDictionary  *dic = responseObject;
                                           NSLog(@"验证用户密码----%@",[dic dictionaryToJson]);
                                           
                                           ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
                                           
                                           if (success) {
                                               success(model);
                                           }

                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                           if (fail) {
                                               fail(error);
                                           }
                                       }];
}


/**
 *  修改密码
 *
 *  @param dic     用户密码
 *  @param success BOOL 成功
 *  @param fail    失败
 */
- (void)changeUserPWDWithRequest:(NSString*)pwd
                        success:(void(^)(id responseObject))success
                        failure:(void(^)(NSError *error))fail{
    if(!pwd)
        return;
    
    NSDictionary *dic = @{@"passWord":pwd};
    [[HttpClient sharedClient] POST:HTTP_UPDATEAPPUSERPWDCHANGE_URL
                                       parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           NSDictionary  *dic = responseObject;
                                           NSLog(@"修改用户密码----%@",[dic dictionaryToJson]);
                                           
                                           ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
                                           
                                           if (success) {
                                               success(model);
                                           }
                                           
                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                           if (fail) {
                                               fail(error);
                                           }
                                       }];
}


/**
 *  获取所有省份信息
 *
 *  return AreaModel List
 */
-(void)getAllProvinceInfoWithSuccess:(void (^)(id responseObject))success
                             failure:(void (^)(void))fail{
   
    [[HttpClient sharedClient] POST:HTTP_GETAREAPROVINCEINFO_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"CurrentAllProvince:   \n%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for(NSDictionary *model in [dic valueForKey:@"data"]){
            
            [AreaModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"uid":@"id"};
            }];
             AreaModel  *area = [AreaModel objectWithKeyValues:model];
            [array addObject:area];
        }
       
        if (success) {
            success(array);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail();
        }
    }];
  
}

/**
 *  获取所有市区信息
 *
 *  return AreaModel List
 */
-(void)getAllCitynfoWithProvinceCode:(NSString*)code
                             success:(void (^)(id responseObject))success
                             failure:(void (^)(void))fail{
    NSDictionary *dic = @{@"code":code};
    [[HttpClient sharedClient] POST:HTTP_GETAREACITYBYPROVINCEINFO_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"CurrentAllCityByProvince:   \n%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for(NSDictionary *model in [dic valueForKey:@"data"]){
            AreaModel  *area = [AreaModel objectWithKeyValues:model];
            [array addObject:area];
        }
        
        if (success) {
            success(array);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail();
        }
    }];
    
}

/**
 *  保存定位到的地区信息
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调 location
 */
- (void)updateAppUserWithLocationAtRequest:(NSDictionary*)dic
                                   success:(void (^)(id responseObject))success
                                   failure:(void (^)(NSError *error))fail
{
    if(!dic)
        return;
    
    NSMutableDictionary *mdic = [[NSMutableDictionary    alloc]initWithDictionary:dic];
    
    [[HttpClient sharedClient] POST:HTTP_SAVELOCATIONATINFO_URL
                                       parameters:mdic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           
                                           NSDictionary  *dic = responseObject;
                                           NSLog(@"保存定位到的地区信息----%@",[dic dictionaryToJson]);
                                           
                                           ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
                                           
                                           if (success) {
                                               success(model);
                                           }
                                           
                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                           if (fail) {
                                               fail(error);
                                           }
                                       }];
}


/**
 *  用户意见反馈
 */
- (void)userFeedbackZRBRequest:(NSString*)content
                    andContact:(NSString*)contact
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail
{
    if(!content)
        return;
    
    
    NSMutableDictionary *mdic = [[NSMutableDictionary    alloc]init];
    [mdic setObject:content forKey:@"contents"];
    
    if(!contact){
       [mdic setObject:[[ZRB_UserManager shareUserManager] userData].zrb_front_ut forKey:@"contact"];
    }else{
        [mdic setObject:contact forKey:@"contact"];
    }
    
    [[HttpClient sharedClient] POST:HTTP_FEEDBACKZRB_URL
                                       parameters:mdic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              
                                              NSDictionary  *dic = responseObject;
                                              NSLog(@"用户意见反馈----%@",[dic dictionaryToJson]);
                                              
                                              ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
                                              
                                              if (success) {
                                                  success(model);
                                              }
                                              
                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              if (fail) {
                                                  fail(error);
                                              }
                                          }];
}

/**
 *  我的发布列表
 *
 *  @param request PublishRequest
 *  @param success
 *  @param fail    nil
 */
- (void)minePublishQueryWithRequest:(PublishRequest *)request
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail{
    if(!request)
        return;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(request.limit) forKey:@"limit"];
    
    if(request.maxId){
        [dic setObject:@(request.maxId) forKey:@"maxId"];
    }
    
    LogInfo(@"我的发布列表参数：%@",[dic dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_MYPUBLISH_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        LogInfo(@"发布列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            
            [FindHomeItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"uid":@"id"};
            }];
            
            FindHomeItemModel *itemModel = [FindHomeItemModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        
        if(array.count>0){
            success(array);
        }else{
            success(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}


/**
 *  关注发现列表
 *
 *  @param request PublishRequest
 *  @param success
 *  @param fail    nil
 */
- (void)mineAttentionFindQueryWithRequest:(AttentionFindRequest *)request
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail{
    if(!request)
        return;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(request.limit) forKey:@"limit"];
    
    if(request.createTime){
        [dic setObject:request.createTime forKey:@"createTime"];
    }
//    NSLog(@"%@?%@&%@",HTTP_MYATTENTIONFIND_QUERY_URL,@(request.limit),request.createTime);
    LogInfo(@"关注发现列表参数：%@",[dic dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_MYATTENTIONFIND_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *dic = responseObject;
        LogInfo(@"关注发现列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            [AttentionModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"aid":@"id"};
            }];
            
            AttentionModel *itemModel = [AttentionModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        if(success){
            if(array.count>0){
                success(array);
            }else{
                success(nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  关注项目列表
 *
 *  @param request PublishRequest
 *  @param success
 *  @param fail    nil
 */
- (void)mineAttentionProjectQueryWithRequest:(AttentionProjectRequest *)request
                                  success:(void (^)(id responseObject))success
                                  failure:(void (^)(NSError *error))fail{
    if(!request)
        return;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(request.limit) forKey:@"limit"];
    if(request.createTime){
        [dic setObject:request.createTime forKey:@"createTime"];
    }
    
    LogInfo(@"关注项目列表参数：%@",[dic dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_MYATTENTIONPROJECT_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *dic = responseObject;
        LogInfo(@"关注项目列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            [AttentionModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"aid":@"id"};
            }];
            
            AttentionModel *itemModel = [AttentionModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        if(success){
            if(array.count>0){
                success(array);
            }else{
                success(nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  关注资金列表
 *
 *  @param request MoneyRequest
 *  @param success
 *  @param fail    nil
 */
- (void)mineAttentionMoneyQueryWithRequest:(AttentionMoneyRequest *)request
                                     success:(void (^)(id responseObject))success
                                     failure:(void (^)(NSError *error))fail{
    if(!request)
        return;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(request.limit) forKey:@"limit"];
    
    if(request.createTime){
        [dic setObject:request.createTime forKey:@"createTime"];
    }
    
    LogInfo(@"关注资金列表参数：%@",[dic dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_MYATTENTIONMONEY_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *dic = responseObject;
        LogInfo(@"关注资金列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            [AttentionModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"aid":@"id"};
            }];
            
            AttentionModel *itemModel = [AttentionModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        if(success){
            if(array.count>0){
                success(array);
            }else{
                success(nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  我的项目列表
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)mineProjectQueryListWithRequest:(MineProjectListRequest *)request
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(request.limit) forKey:@"limit"];
    
    if (request.pushTime.length > 1) {
        [dic setObject:request.pushTime forKey:@"pushTime"];
    }
    
    LogInfo(@"项目列表参数：%@",[dic dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_MINEPROJECT_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        LogInfo(@"项目列表：%@",[dic dictionaryToJson]);
        
        NSMutableArray *array = [[NSMutableArray alloc]init];;
        for (NSDictionary *mo in [dic valueForKey:@"data"]) {
            [MineProjectListModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"pid":@"id"};
            }];
            MineProjectListModel *model = [MineProjectListModel objectWithKeyValues:mo];
            [array addObject:model];
        }
        if(array.count>0){
            success(array);
        }else{
            success(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  我的资金列表
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)mineMoneyQueryListWithRequest:(MineMoneyListRequest *)request
                                success:(void (^)(id responseObject))success
                                failure:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(request.limit) forKey:@"limit"];
    
    if (request.pushTime.length > 1) {
        [dic setObject:request.pushTime forKey:@"pushTime"];
    }
    
    LogInfo(@"资金列表参数：%@",[dic dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_MINEMONEY_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        LogInfo(@"资金列表：%@",[dic dictionaryToJson]);
        
        NSMutableArray *array = [[NSMutableArray alloc]init];;
        for (NSDictionary *mo in [dic valueForKey:@"data"]) {
            [MineMoneyListModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"mid":@"id"};
            }];
            MineMoneyListModel *model = [MineMoneyListModel objectWithKeyValues:mo];
            [array addObject:model];
        }
        if(array.count>0){
            success(array);
        }else{
            success(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

@end

@implementation PublishRequest
@end

@implementation AttentionFindRequest
@end

@implementation AttentionProjectRequest
@end

@implementation AttentionMoneyRequest
@end

@implementation MineProjectListRequest
@end

@implementation MineMoneyListRequest
@end