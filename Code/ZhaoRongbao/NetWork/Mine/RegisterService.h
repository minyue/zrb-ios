//
//  RegisterService.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/16.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "AFBaseService.h"
@class  RegisterRequest;
@interface RegisterService : AFBaseService

/**
 *  发送短信验证码
 *
 *  @param tel     电话
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)sendSMSRegisterWithTel:(NSString *)tel
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail;


/**
 *  检查验证码是否有效
 *
 *  @param tel     电话号码
 *  @param code    验证码
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)checkSMSCodeWithTel:(NSString *)tel
                       code:(NSString *)code
                    success:(void (^)(id responseObject))success
                    failure:(void (^)(NSError *error))fail;


/**
 *  检查手机号码是否已经注册
 *
 *  @param tel     手机号码
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)checkMobileRegisterWithTel:(NSString *)tel
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))fail;


/**
 *  校验用户名是否已经注册
 *
 *  @param userName 用户名
 *  @param success  成功回调
 *  @param fail     失败回调
 */
- (void)checkUnameRegisterWithUserName:(NSString *)userName
                               success:(void (^)(id responseObject))success
                               failure:(void (^)(NSError *error))fail;


/**
 *  保存用户信息
 *  V0.3
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)saveAppUserWithRequest:(RegisterRequest *)request
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail;
//V1.0
- (void)saveAppUserInfoWithRequest:(NSDictionary *)dic
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))fail;
/**
 *  找回密码短信发送
 *
 *  @param mobile  手机号码
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)sendSMSFindPWWithMobile:(NSString *)mobile
                        success:(void (^)(id responseObject))success
                        failure:(void (^)(NSError *error))fail;



/**
 *  修改密码
 *
 *  @param mobile  手机号码
 *  @param pwd     密码
 *  @param smsCode 手机校验码
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)changePWordWithMobile:(NSString *)mobile
                          pwd:(NSString *)pwd
                      smsCode:(NSString *)smsCode
                      success:(void (^)(id responseObject))success
                      failure:(void (^)(NSError *error))fail;


/**
 *  用户登录
 *
 *  @param name     用户名
 *  @param password 用户密码
 *  @param success  成功回调
 *  @param fail     失败回调
 */
- (void)loginWithName:(NSString *)name
             passWord:(NSString *)password
              success:(void (^)(id responseObject))success
              failure:(void (^)(NSError *error))fail;



/**
 *  查询企业信息
 *
 *  @return <#return value description#>
 */
- (void)checkEnterpriseInfoWithKey:(NSString*)string
                           success:(void (^)(NSMutableArray *infolist))success
                           failure:(void (^)(NSError *error))fail;
@end



@interface RegisterRequest : NSObject


+ (instancetype)shareInstance;

@property (nonatomic,copy) NSString  *mobile;

@property (nonatomic,assign) NSInteger userType;

@property (nonatomic,copy) NSString *smsCode;

@property (nonatomic,copy) NSString *passWord;

@property (nonatomic,copy) NSString *uname;

@property (nonatomic,copy) NSString *org;

@end





