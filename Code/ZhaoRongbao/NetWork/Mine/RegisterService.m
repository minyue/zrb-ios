//
//  RegisterService.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/16.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "RegisterService.h"
#import "RegisterModel.h"
#import "UserModel.h"
#import "CompanyInfoModel.h"

@implementation RegisterService


/**
 *  发送短信验证码
 *
 *  @param tel     电话
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)sendSMSRegisterWithTel:(NSString *)tel
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail
{
 
    NSDictionary *dic = @{@"mobile":@(tel.longLongValue)};
    [[HttpClient sharedClient] POST:HTTP_SMS_URL
                                       parameters:dic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
         NSLog(@"%@",[dic dictionaryToJson]);
        
        
        RegisterNormalModel *sms = [RegisterNormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(sms);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];

}

/**
 *  发送找回密码短信验证码
 *
 *  @param tel     电话
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)sendSMSFindPwdWithTel:(NSString *)tel
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail
{
    
    NSDictionary *dic = @{@"mobile":@(tel.longLongValue)};
    [[HttpClient sharedClient] POST:HTTP_FINDPWDSMS_URL
                                       parameters:dic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              
                                              NSDictionary  *dic = responseObject;
                                              NSLog(@"%@",[dic dictionaryToJson]);
                                              
                                              
                                              RegisterNormalModel *sms = [RegisterNormalModel objectWithKeyValues:dic];
                                              
                                              if (success) {
                                                  success(sms);
                                              }
                                              
                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              if (fail) {
                                                  fail(error);
                                              }
                                          }];
    
}


/**
 *  检查验证码是否有效
 *
 *  @param tel     电话号码
 *  @param code    验证码
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)checkSMSCodeWithTel:(NSString *)tel
                       code:(NSString *)code
                    success:(void (^)(id responseObject))success
                    failure:(void (^)(NSError *error))fail
{

    NSDictionary *dic = @{@"mobile":@(tel.longLongValue),
                          @"smsCode":code};
    [[HttpClient sharedClient] POST:HTTP_CHECKSMS_URL
                                       parameters:dic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"%@",[dic dictionaryToJson]);
        
        
        RegisterNormalModel *model = [RegisterNormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];

}

/**
 *  查询企业信息
 *
 */
- (void)checkEnterpriseInfoWithKey:(NSString*)string
                           success:(void (^)(NSMutableArray *infolist))success
                           failure:(void (^)(NSError *error))fail{
    NSDictionary *dic = @{@"companyName":string};
    [[HttpClient sharedClient] POST:HTTP_SEARCHCOMPANY_URL
                                       parameters:dic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSMutableArray *array = [[NSMutableArray alloc] init];
         NSDictionary  *dic = responseObject;
         NSLog(@"拉取企业信息----%@",[dic dictionaryToJson]);
         
         if([[dic valueForKey:@"data"] isEqual:[NSNull null]]){
             if (success) {
                 success(nil);
             }
         }else{
             for(NSDictionary *mo in [dic valueForKey:@"data"]){
                 [CompanyInfoModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                     return @{@"uid":@"id"};
                 }];
                 CompanyInfoModel *model = [CompanyInfoModel objectWithKeyValues:mo];
                 [array addObject:model];
             }
             
             if (success) {
                 success(array);
             }
         }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (fail) {
                fail(error);
            }
     }];
}

/**
 *  检查手机号码是否已经注册
 *
 *  @param tel     手机号码
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)checkMobileRegisterWithTel:(NSString *)tel
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))fail
{

    NSDictionary *dic = @{@"mobile":@(tel.longLongValue)};
    [[HttpClient sharedClient] POST:HTTP_CHECKMOBILE_URL
                                       parameters:dic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"检查手机号码是否已经注册----%@",[dic dictionaryToJson]);
        RegisterNormalModel *model = [RegisterNormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];


}



/**
 *  校验用户名是否已经注册
 *
 *  @param userName 用户名
 *  @param success  成功回调
 *  @param fail     失败回调
 */
- (void)checkUnameRegisterWithUserName:(NSString *)userName
                               success:(void (^)(id responseObject))success
                               failure:(void (^)(NSError *error))fail
{
    
    NSDictionary *dic = @{@"uname":userName};
    [[HttpClient sharedClient] POST:HTTP_CHECKUSER_URL
                                       parameters:dic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"校验用户名是否已经注册----%@",[dic dictionaryToJson]);
        RegisterNormalModel *model = [RegisterNormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];

    
}



/**
 *  保存用户信息 V0.3
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)saveAppUserWithRequest:(RegisterRequest *)request
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail
{
    NSDictionary *dic = @{@"mobile":request.mobile,
                          @"userType":@(request.userType),
                          @"smsCode":request.smsCode,
                          @"passWord":request.passWord};
    
    NSMutableDictionary *mdic = [[NSMutableDictionary    alloc] initWithDictionary:dic];
    
    if (request.uname) {
        [mdic setObject:request.uname forKey:@"uname"];
    }
    
    if (request.org) {
        [mdic setObject:request.org forKey:@"org"];
    }
    
    [[HttpClient sharedClient] POST:HTTP_SAVEAPPUSER_URL
                                       parameters:mdic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"保存用户信息----%@",[dic dictionaryToJson]);
        RegisterNormalModel *model = [RegisterNormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  保存注册用户信息 V1.0
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)saveAppUserInfoWithRequest:(NSDictionary *)dic
                       success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail
{
    if(!dic)
        return;

    [[HttpClient sharedClient] POST:HTTP_SAVEAPPUSERINFO_URL
                                       parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           
                                           NSDictionary  *dic = responseObject;
                                           NSLog(@"保存注册用户信息----%@",[dic dictionaryToJson]);
                                           ZRB_BaseModel *model = [ZRB_BaseModel objectWithKeyValues:dic];
                                           
                                           if (success) {
                                               success(model);
                                           }
                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                           if (fail) {
                                               fail(error);
                                           }
                                       }];
}



/**
 *  找回密码短信发送
 *
 *  @param mobile  手机号码
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)sendSMSFindPWWithMobile:(NSString *)mobile
                        success:(void (^)(id responseObject))success
                        failure:(void (^)(NSError *error))fail
{
    
    NSDictionary *dic = @{@"mobile":mobile};
    [[HttpClient sharedClient] POST:HTTP_SMSFINDPWD_URL
                                       parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"找回密码短信发送----%@",[dic dictionaryToJson]);
        RegisterNormalModel *model = [RegisterNormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    

    
}



/**
 *  修改密码
 *
 *  @param mobile  手机号码
 *  @param pwd     密码
 *  @param smsCode 手机校验码
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)changePWordWithMobile:(NSString *)mobile
                          pwd:(NSString *)pwd
                      smsCode:(NSString *)smsCode
                      success:(void (^)(id responseObject))success
                      failure:(void (^)(NSError *error))fail
{
    NSDictionary *dic = @{@"mobile":mobile,
                          @"passWord":pwd,
                          @"smsCode":smsCode};
    [[HttpClient sharedClient] POST:HTTP_CHANGEPWD_URL
                                       parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"修改密码----%@",[dic dictionaryToJson]);
        RegisterNormalModel *model = [RegisterNormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];

}


/**
 *  用户登录
 *
 *  @param name     用户名
 *  @param password 用户密码
 *  @param success  成功回调
 *  @param fail     失败回调
 */
- (void)loginWithName:(NSString *)name
             passWord:(NSString *)password
              success:(void (^)(id responseObject))success
              failure:(void (^)(NSError *error))fail
{
    NSLog(@"%@?name=%@&password=%@",HTTP_LOGIN_URL,name,password);
    NSDictionary *dic = @{@"name":name,
                          @"password":password};
    [[HttpClient sharedClient] POST:HTTP_LOGIN_URL
                                       parameters:dic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"%@",[dic dictionaryToJson]);
        
        [LoginModel setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{@"model":@"data"};
        }];
        
        LoginModel *model = [LoginModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];


}



@end



@implementation RegisterRequest

+ (instancetype)shareInstance
{
    static  RegisterRequest *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[RegisterRequest alloc] init];
    });
    return instance;
}


@end