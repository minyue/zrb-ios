//
//  MoneyService.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "AFBaseService.h"


@class MoneyHomeListRequest;

@interface MoneyService : AFBaseService


/**
 *  找资金首页列表
 *
 *  @param request  请求类
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 *
 */
- (void)moneyHomeListWithRequest:(MoneyHomeListRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;



@end



@interface MoneyHomeListRequest : NSObject

@property (nonatomic,strong) NSString *pushTime;
@property (nonatomic,strong) NSString *limit;
@property (nonatomic,strong) NSString *citys;            //城市邮编，多个用“,”隔开
@property (nonatomic,strong) NSString *industrys;           //行业，多个用“,”隔开


@end