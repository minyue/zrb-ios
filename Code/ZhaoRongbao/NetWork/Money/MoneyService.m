//
//  MoneyService.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MoneyService.h"
#import "MoneyHomeModel.h"

@implementation MoneyService

- (void)moneyHomeListWithRequest:(MoneyHomeListRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.pushTime forKey:@"pushTime"];
    [params setObject:request.limit forKey:@"limit"];
    
    if (request.citys.length > 0) {
        [params setObject:request.citys forKey:@"citys"];
    }
    
    if (request.industrys.length > 0) {
        [params setObject:request.industrys forKey:@"industrys"];
    }
    
    LogInfo(@"找资金列表参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_MONEY_HOME_LIST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"找资金首页列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            
            [MoneyHomeItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return  @{@"uid":@"id"};
            }];
            MoneyHomeItemModel *itemModel = [MoneyHomeItemModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
            
        }
        
        MoneyHomeModel *model = [[MoneyHomeModel alloc]init];
        model.data = array;
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

@end


@implementation MoneyHomeListRequest



@end