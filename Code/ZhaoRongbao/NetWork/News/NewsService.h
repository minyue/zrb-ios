//
//  NewsService.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "AFBaseService.h"

@class NewsQueryListRequest;

@class NewsSearchListRequst;

@interface NewsService : AFBaseService

/**
 *  查询咨询类别
 *
 */
- (void)newsQueryCategoryWithSuccess:(void (^)(id responseObject))success
                             failure:(void (^)(NSError *error))fail;



/**
 *  查询新闻列表
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)newsQueryListWithRequest:(NewsQueryListRequest *)request
                         success:(void (^)(id responseObject))success
                         failure:(void (^)(NSError *error))fail;

/**
 *  查询常用标签
 *
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)querySearchLabelWithSuccess:(void (^)(id responseObject))success
                             failure:(void (^)(NSError *error))fail;

/**
 *  随机获取热榜新闻
 *
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)querHotPoularAPIWithSuccess:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail;


/**
 *  查询搜索记录
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)querySearchListWithRequest:(NewsSearchListRequst *)request
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))fail;


/**
 *  查询我关注的咨讯列表
 *  news/queryCollections?zrb_front_ut=d4504fcb5ee1a28efa7c89a242f66626&createTime=2015-08-14%2011:28:10
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */

- (void)myAttentionNewsQueryListWithRequest:(NewsQueryListRequest *)request
                                    success:(void (^)(id responseObject))success
                                    failure:(void (^)(NSError *error))fail;


@end




/**
 *  新闻列表请求
 */
@interface NewsQueryListRequest : NSObject


@property (nonatomic,strong) NSNumber  *id;

@property (nonatomic,assign) BOOL     forword;  // YES 升序  NO，降序

@property (nonatomic,strong) NSNumber      *category; //咨询类别

@property (nonatomic,assign) int      limit;    //最多一批返回多少

@property (nonatomic,strong) NSString *pushTime;       // 最后一条数据的时间标记

@end


/**
 *  搜索列表查询
 */
@interface NewsSearchListRequst : NSObject

@property (nonatomic,copy) NSString *searchStr;

@property (nonatomic,assign) int searchType;

@property (nonatomic,assign) int pageSize;

@property (nonatomic,assign) int pageNum;

@end
