//
//  NewsService.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsService.h"
#import "NewsCategory.h"
#import "NewsListModel.h"
#import "NewsSearchModel.h"
#import "ZRB_NormalModel.h"

@implementation NewsService


/**
 *  查询咨询类别
 *
 */

- (void)newsQueryCategoryWithSuccess:(void (^)(id))success failure:(void (^)(NSError *))fail
{

    [[AFHTTPRequestOperationManager manager] POST:HTTP_NEWS_QUERYCATEGORY_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
       // NSLog(@"%@",[dic dictionaryToJson]);
        
        [NewsCategory setupObjectClassInArray:^NSDictionary *{
            return @{@"data":[CategoryItem class]};
        }];
        
        NewsCategory *category = [NewsCategory objectWithKeyValues:dic];
        
        if (success) {
            success(category);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}


/**
 *  查询新闻列表
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */

- (void)newsQueryListWithRequest:(NewsQueryListRequest *)request
                         success:(void (^)(id responseObject))success
                         failure:(void (^)(NSError *error))fail
{
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:request.forword],@"forword",
                          @(request.limit),@"limit",nil];
    if (request.id ) {
        [dic setObject:request.id forKey:@"id"];
    }
    
    if (request.category)
    {
        [dic setObject:request.category forKey:@"category"];
    }
    
    [[AFHTTPRequestOperationManager manager] POST:HTTP_NEWS_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"%@",[dic dictionaryToJson]);
        
        [NewsListModel setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"data" : [NewsItemModel class]
                     };
        }];
        
        [NewsItemModel setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"tagsInfoList" : [NewsTagModel class]
                     };

        }];
        
        NewsListModel *model = [NewsListModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    
    

}



/**
 *  查询常用标签
 *
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)querySearchLabelWithSuccess:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail
{

    [[AFHTTPRequestOperationManager manager] POST:HTTP_NEWS_QUERYSEARCHLABEL_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
         NSLog(@"%@",[dic dictionaryToJson]);
        [NewsTagList   setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"data" : [NewsTagModel class]
                     };
        }];
        
        NewsTagList *list = [NewsTagList objectWithKeyValues:dic];
        if (success) {
            success(list);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}


/**
 *  随机获取热榜新闻
 *
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)querHotPoularAPIWithSuccess:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail
{

    [[AFHTTPRequestOperationManager manager] POST:HTTP_HOTPOULAR_QUERYHOTPOULARAPI_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        [NewsRandomItemList   setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"data" : [NewsRandomItem class]
                     };
        }];
        
        NewsRandomItemList *list = [NewsRandomItemList objectWithKeyValues:dic];
        if (success) {
            success(list);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];

}


/**
 *  查询搜索记录
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)querySearchListWithRequest:(NewsSearchListRequst *)request
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))fail
{
    
    
    NSDictionary *dic = @{@"searchStr":request.searchStr,
                          @"searchType":@(request.searchType),
                          @"pageSize":@(request.pageSize),
                          @"pageNum":@(request.pageNum)};
    [[AFHTTPRequestOperationManager manager] POST:HTTP_NEWS_SEARCH_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        
        NSDictionary  *tempDic = [dic objectForKey:@"data"];
        
        [NewsSearchModel   setupObjectClassInArray:^NSDictionary *{
            return @{@"list" : [NewsItemModel class]};
        }];
        
        [NewsItemModel setupObjectClassInArray:^NSDictionary *{
            return @{@"tagsInfoList" : [NewsTagModel class]};
        
        }];
        
        NewsSearchModel *model = [NewsSearchModel objectWithKeyValues:tempDic];
        
        NewsSearchListModel *returnModel = [[NewsSearchListModel alloc] init];
        returnModel.res = [[dic objectForKey:@"res"] intValue];
        returnModel.msg = [dic objectForKey:@"msg"];
        returnModel.data = model;
        
        if (success) {
            success(returnModel);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    
    

}





/**
 *  查询我关注的咨讯列表
 *  news/queryCollections?zrb_front_ut=d4504fcb5ee1a28efa7c89a242f66626&createTime=2015-08-14%2011:28:10
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */

- (void)myAttentionNewsQueryListWithRequest:(NewsQueryListRequest *)request
                                    success:(void (^)(id responseObject))success
                                    failure:(void (^)(NSError *error))fail
{
    NSString *str = [[ZRB_UserManager shareUserManager] userData].zrb_front_ut;
    
    if(!request  || !str)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:request.pushTime,@"createTime",str,@"zrb_front_ut",nil];
    
    if (request.limit)
    {
        [dic setObject:@(request.limit) forKey:@"limit"];
    }
    
    [[AFHTTPRequestOperationManager manager] POST:HTTP_MYATTENTIONNEWS_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"%@",[dic dictionaryToJson]);
        
        [NewsListModel setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"data" : [NewsItemModel class]
                     };
        }];
        
        [NewsItemModel setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"tagsInfoList" : [NewsTagModel class]
                     };
            
        }];
        
        NewsListModel *model = [NewsListModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    
    
    
}


@end







/**
 *  新闻列表请求
 */

@implementation NewsQueryListRequest



@end

/**
 *  搜索列表查询
 */
@implementation NewsSearchListRequst



@end
