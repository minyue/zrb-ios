//
//  ProjectService.h
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/12.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "AFBaseService.h"

@class ProjectQueryListRequest;
@class ProjectCommentRequest;
@class ProjectTypeRequest;
@class ProjectAllCommentRequest;
@class DeleteProjectCommentRequest;
@class CollectionProjectRequest;
@class CancelCollectionProjectRequest;
@class InvestigationApplyRequest;
@class AskForMoreInfoRequest;
@class CityDetailInfoRequest;
@class CityEconomicInfoRequest;
@class CityIndustryInfoRequest;
@class ProspectsRequest;

@interface ProjectService : AFBaseService

/**
 *  查询项目列表
 *
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)projectQueryListWithRequest:(ProjectQueryListRequest *)request
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail;

/**
 *  获取项目类型
 *
 *  @param request 请求体
 */
- (void)projectTypeWithRequest:(ProjectTypeRequest *)request success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail;

/**
 *  提交项目评论
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)sendProjectCommentWithRequest:(ProjectCommentRequest *)request success:(void (^)(id responseObject))sucess failure:(void (^)(NSError *error)) fail;


/**
 *  获取项目所有评论
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)projectAllCommnetWithRequest:(ProjectAllCommentRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error)) fail;


/**
 *  删除项目评论
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)deleteProjectCommentWithRequest:(DeleteProjectCommentRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error)) fail;

/**
 *  关注项目
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)collectionPorjectWithRequest:(CollectionProjectRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error)) fail;

/**
 *  取消关注项目
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)cancelCollectionProjectWithRequest:(CancelCollectionProjectRequest *)request success:(void (^)(id responseObject)) success failture:(void (^)(NSError *error))fail;


/**
 *  查询某一资讯或项目是否被关注过
 *  0是资讯 1是项目
 */
//- (void)checkIfAttentionNewOrProjectWithRequest:(NSString *)cid
//                                        andType:(int)type
//                                        success:(void (^)(id responseObject))success
//                                        failure:(void (^)(NSError *error))fail;

/**
 *  项目勘察申请
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)investigationApplyWithRequest:(InvestigationApplyRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail;

/**
 *  索要更多资料
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)askForMoreInfoWithRequest:(AskForMoreInfoRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail;

/**
 *  城市详情数据
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)cityDetailInfoWithRequest:(CityDetailInfoRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail;

/**
 *  城市经济情况
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)cityEconomicInfoWithRequest:(CityEconomicInfoRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail;

/**
 *  产业详情
 *
 *  @param request 请求类
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)cityIndustryInfoWithRequest:(CityIndustryInfoRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail;


/** add by zouli
 *  查询我关注的项目列表
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */

- (void)myAttentionProjectsQueryListWithRequest:(ProjectQueryListRequest *)request
                                    success:(void (^)(id responseObject))success
                                        failure:(void (^)(NSError *error))fail;

/** (政府)
 *  查询我的项目列表
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)myProjectQueryListWithRequest:(ProjectQueryListRequest *)request
                              success:(void (^)(id responseObject))success
                              failure:(void (^)(NSError *error))fail;

/**
 *  查询我的项目详情
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)myProjectDetailWithRequest:(NSString *)prjID
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))fail;

/**
 *  请求我的勘察列表
 */
- (void)requestProspectListWithRequest:(ProspectsRequest *)request
                               success:(void (^)(id responseObject))success
                               failure:(void (^)(NSError *error))fail;

/**
 *  请求取消勘察
 */
- (void)cancelProspectDeleteWithRequest:(NSString *)pid
                          success:(void (^)(id responseObject))success
                          failure:(void (^)(NSError *error))fail;

/**
 *  获取勘察详情
 *
 */
- (void)getProspectDetailWithRequest:(NSString *)iid
                             success:(void (^)(id responseObject))success
                             failure:(void (^)(NSError *error))fail;

/**
 *  取消勘察
 */
- (void)cancelProspectWithRequest:(NSDictionary *)dic
                          success:(void (^)(id responseObject))success
                          failure:(void (^)(NSError *error))fail;


/**查询项目图片列表
 */
- (void)myProjectImagesWithRequest:(NSString *)prjID
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))fail;
@end


/**
 *  项目列表请求
 */
@interface ProjectQueryListRequest : NSObject

@property (nonatomic,strong) NSNumber  *uid;
@property (nonatomic,assign) BOOL     forword;          //YES 升序  NO，降序
@property (nonatomic,strong) NSString *pushTime;        //最后一条数据的时间标记
@property (nonatomic,assign) int      limit;            //最多一批返回多少
@property (nonatomic,strong) NSString *postCode;        //城市邮政编码，获取全国项目不传该参数

@end


/**
 *  获取项目类型请求
 */
@interface ProjectTypeRequest : NSObject


@end

/**
 *  发送项目评论请求
 */
@interface ProjectCommentRequest : NSObject

@property (nonatomic,strong) NSString *pid;             //项目ID
@property (nonatomic,strong) NSString *content;         //评论内容
@property (nonatomic,strong) NSString *questionType;          //项目问题类型

@end

/**
 * 项目所有评论请求
 */
@interface ProjectAllCommentRequest : NSObject

@property (nonatomic,strong) NSString *pid;             //项目ID
@property (nonatomic,assign) int limit;                 //一次请求获取的数量
@property (nonatomic,assign) int maxCommentId;          //最大评论id

@end

/**
 * 删除评论请求
 */
@interface DeleteProjectCommentRequest : NSObject

@property (nonatomic,assign) int cid;                   //评论ID
@property (nonatomic,strong) NSString *pid;                   //项目ID

@end

/**
 *  关注项目
 */
@interface CollectionProjectRequest : NSObject

@property (nonatomic,strong)NSString *projectID;              //项目ID
@property (nonatomic,assign)int collectionType;               //0=资讯；1=项目

@end

/**
 *  取消关注项目
 */
@interface CancelCollectionProjectRequest : NSObject

@property (nonatomic,strong)NSString *projectID;              //项目ID
@property (nonatomic,assign)int collectionType;               //0=资讯；1=项目

@end

/**
 *  项目勘察申请
 */
@interface InvestigationApplyRequest : NSObject

@property (nonatomic,strong)NSString *inspectTime;      //勘察时间
@property (nonatomic,strong)NSString *projectID;        //项目ID
@property (nonatomic,strong)NSString *personNum;              //勘察人数
@property (nonatomic,strong)NSString *telphone;         //电话
@property (nonatomic,strong)NSString *contact;          //联系人
@property (nonatomic,strong)NSString *companyName;      //公司名称

@end

/**
 *  索要更多资料
 */
@interface AskForMoreInfoRequest : NSObject

@property (nonatomic,strong)NSString *projectID;        //项目ID
@property (nonatomic,strong)NSString *projectName;      //项目名称
@property (nonatomic,strong)NSString *email;            //邮件地址
@end

/**
 *  勘察申请
 */
@interface ProspectsRequest : NSObject
@property (nonatomic,assign) int      pageSize;    //最多一批返回多少
@property (nonatomic,assign) int      maxInspectId;//最大勘察ID

@end

/**
 *  城市详情数据
 */
@interface CityDetailInfoRequest : NSObject

@property (nonatomic,strong)NSString *postCode;         //邮政编码

@end

/**
 *  城市经济情况查询
 */
@interface CityEconomicInfoRequest : NSObject

@property (nonatomic,strong)NSString *postCode;         //邮政编码

@end

/**
 *  产业详情
 */
@interface CityIndustryInfoRequest : NSObject

@property (nonatomic,strong)NSString *postCode;

@end





