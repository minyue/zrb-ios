//
//  ProjectService.m
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/12.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ProjectService.h"
#import "ProjectListModel.h"
#import "ProjectTypeModel.h"
#import "ProjectCommentModel.h"
#import "AllCommentModel.h"
#import "DeleteCommentModel.h"
#import "CollectionModel.h"
#import "InvestigationApplyModel.h"
#import "AskForMoreInfoModel.h"
#import "CityDetailModel.h"
#import "CityEnomicModel.h"
#import "CityIndustryModel.h"
#import "ZRB_NormalModel.h"
#import "ProspectModel.h"
#import "ProspectDetailModel.h"
#import "ProjectImageModel.h"

@implementation ProjectService

- (void)projectQueryListWithRequest:(ProjectQueryListRequest *)request
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail{

    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(request.limit) forKey:@"limit"];
    if (request.pushTime.length > 1) {
        [dic setObject:request.pushTime forKey:@"tradeDates"];
    }
    
    if(request.postCode.length > 1){
        [dic setObject:request.postCode forKey:@"provincePostcode"];
    }
    
    LogInfo(@"项目列表参数：%@",[dic dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_PROJECT_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        LogInfo(@"项目列表：%@",[dic dictionaryToJson]);
        
        NSMutableArray *projectArray = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [[dic valueForKey:@"data"] objectForKey:@"projectList"]) {
            ProjectItemModel *model = [ProjectItemModel objectWithKeyValues:modelDic];
            [projectArray addObject:model];
        }
        
        NSMutableArray *bannerArray = [[NSMutableArray alloc]init];;
        for (NSDictionary *mo in [[dic valueForKey:@"data"] objectForKey:@"bannerList"]) {
            ProjectItemModel *model = [ProjectItemModel objectWithKeyValues:mo];
            [bannerArray addObject:model];
        }
        
        ProjectListModel *dataModel = [[ProjectListModel alloc]init];
        dataModel.res = [[dic objectForKey:@"res"] intValue];
        
        dataModel.projectList = projectArray;
        dataModel.bannerList = bannerArray;
        
        if (success) {
            success(dataModel);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  获取项目类型
 *
 */
- (void)projectTypeWithRequest:(ProjectTypeRequest *)request success:(void (^)(id responseObject))success
                       failure:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    LogInfo(@"%@",params);
    
    [[HttpClient sharedClient] POST:HTTP_PROJECT_TYPE_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        NSMutableArray *typeArray = [[NSMutableArray alloc]init];
        for (NSDictionary *mo in [dic objectForKey:@"data"]) {
            ProjectTypeItemModel *model = [ProjectTypeItemModel objectWithKeyValues:mo];
            [typeArray addObject:model];
        }
        
        ProjectTypeModel *model = [[ProjectTypeModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        model.data = typeArray;
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}


/**
 *  提交项目评论
 */
- (void)sendProjectCommentWithRequest:(ProjectCommentRequest *)request success:(void (^)(id responseObject))sucess failure:(void (^)(NSError *error)) fail{
    //参数封装
    NSMutableDictionary *parmas = [[NSMutableDictionary alloc]init];
    [parmas setObject:request.pid forKey:@"pid"];
    [parmas setObject:request.questionType forKey:@"questionType"];
    [parmas setObject:request.content forKey:@"content"];
    
    [[HttpClient sharedClient] POST:HTTP_SEND_PROJECT_COMMENT_URL parameters:parmas success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        ProjectCommentModel *model = [ProjectCommentModel objectWithKeyValues:dic];
        
        if (sucess) {
            sucess(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  项目所有评论
 */
- (void)projectAllCommnetWithRequest:(ProjectAllCommentRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error)) fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.pid forKey:@"pid"];
    [params setObject:@(request.limit) forKey:@"limit"];
    
    if(request.maxCommentId != -1){
        [params setObject:@(request.maxCommentId) forKey:@"maxCommentId"];
    }
    
    [[HttpClient sharedClient] POST:HTTP_PROJECT_ALL_COMMENT_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        NSMutableArray *commentArray = [[NSMutableArray alloc]init];
        for (NSDictionary *mo in [dic objectForKey:@"data"]) {
            [AllCommentItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"cid":@"id"};
            }];
            
            AllCommentItemModel *model = [AllCommentItemModel objectWithKeyValues:mo];
            [commentArray addObject:model];
        }
        
        AllCommentModel *model = [[AllCommentModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        model.data = commentArray;
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  删除评论
 */
- (void)deleteProjectCommentWithRequest:(DeleteProjectCommentRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error)) fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:@(request.cid) forKey:@"id"];
    [params setObject:request.pid forKey:@"pid"];
    
    [[HttpClient sharedClient] POST:HTTP_PROJECT_COMMENT_DELETE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        DeleteCommentModel *model = [[DeleteCommentModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}


- (void)collectionPorjectWithRequest:(CollectionProjectRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error)) fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.projectID forKeyedSubscript:@"id"];
    [params setObject:@(request.collectionType) forKeyedSubscript:@"type"];
    
    [[HttpClient sharedClient] POST:HTTP_COLLECTION_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        CollectionModel *model = [[CollectionModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)cancelCollectionProjectWithRequest:(CancelCollectionProjectRequest *)request success:(void (^)(id responseObject)) success failture:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.projectID forKeyedSubscript:@"id"];
    [params setObject:@(request.collectionType) forKeyedSubscript:@"type"];
    
    [[HttpClient sharedClient] POST:HTTP_CANCEL_COLLECTION_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        CollectionModel *model = [[CollectionModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)investigationApplyWithRequest:(InvestigationApplyRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.projectID forKey:@"projectId"];
    [params setObject:request.inspectTime forKey:@"inspectTime"];
    [params setObject:request.telphone forKey:@"telphone"];
    [params setObject:request.contact forKey:@"contact"];
    [params setObject:request.personNum forKey:@"personNum"];
    [params setObject:request.companyName forKey:@"companyName"];
    
    [[HttpClient sharedClient] POST:HTTP_INVESTIGATION_APPLY parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo([dic dictionaryToJson]);
        InvestigationApplyModel *model = [[InvestigationApplyModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if(success){
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)askForMoreInfoWithRequest:(AskForMoreInfoRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.projectName forKey:@"projectName"];
    [params setObject:request.projectID forKey:@"projectId"];
    [params setObject:request.email forKey:@"email"];
    
    [[HttpClient sharedClient] POST:HTTP_ASKFOR_MORE_INFO parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        AskForMoreInfoModel *model = [[AskForMoreInfoModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)cityDetailInfoWithRequest:(CityDetailInfoRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.postCode forKey:@"postCode"];
    
    [[HttpClient sharedClient] POST:HTTP_CITY_DETAIL_INFO parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        
        CityDetailModel *model = [CityDetailModel objectWithKeyValues:[dic objectForKey:@"data"]];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [[dic objectForKey:@"data"] objectForKey:@"cityStatistics"]) {
            
            CityStatisticsModel *itemModel = [CityStatisticsModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        model.cityStatistics = [NSMutableArray arrayWithArray:array];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

- (void)cityEconomicInfoWithRequest:(CityEconomicInfoRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.postCode forKey:@"postCode"];
    
    [[HttpClient sharedClient] POST:HTTP_CITY_ECONOMIC parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            CityEnomicItemModel *model = [CityEnomicItemModel objectWithKeyValues:modelDic];
            [array addObject:model];
        }
        
        CityEnomicModel *model = [[CityEnomicModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        model.data = [NSMutableArray arrayWithArray:array];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(fail){
            fail(error);
        }
        
    }];
}

- (void)cityIndustryInfoWithRequest:(CityIndustryInfoRequest *)request success:(void (^)(id responseObject))success failture:(void (^)(NSError *error))fail{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.postCode forKey:@"postCode"];
    
    [[HttpClient sharedClient] POST:HTTP_CITY_INDUSTRY parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            CityIndustryItemModel *itemModel = [CityIndustryItemModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        
        CityIndustryModel *model = [[CityIndustryModel alloc]init];
        model.data = [NSMutableArray arrayWithArray:array];
        model.res = [[dic objectForKey:@"res"] intValue];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(fail){
            fail(error);
        }
        
    }];
}


/**
 *  查询我关注的项目列表
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)myAttentionProjectsQueryListWithRequest:(ProjectQueryListRequest *)request
                                        success:(void (^)(id responseObject))success
                                        failure:(void (^)(NSError *error))fail
{
    if(!request)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:request.pushTime,@"createTime",nil];
    
    if (request.limit)
    {
        [dic setObject:@(request.limit) forKey:@"limit"];
    }
    
    [[HttpClient sharedClient] POST:HTTP_MYATTENTIONPROJECTS_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        LogInfo(@"项目列表：%@",[dic dictionaryToJson]);
        
        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
        
        NSMutableArray *projectArray = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in model.data) {
            ProjectItemModel *model = [ProjectItemModel objectWithKeyValues:modelDic];
            [projectArray addObject:model];
        }
        
        
        if (projectArray.count>0) {
            success(projectArray);
        }else{
            success(nil);
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  查询我的项目列表
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)myProjectQueryListWithRequest:(ProjectQueryListRequest *)request
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail{
    if(!request)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:request.pushTime,@"insertTime",nil];
    
    if (request.limit)
    {
        [dic setObject:@(request.limit) forKey:@"limit"];
    }
    
    [[HttpClient sharedClient] POST:HTTP_MYPROJECTS_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        LogInfo(@"我的项目列表：%@",[dic dictionaryToJson]);
        
        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
        
        NSMutableArray *projectArray = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in model.data) {
            ProjectItemModel *model = [ProjectItemModel objectWithKeyValues:modelDic];
            [projectArray addObject:model];
        }
        
        
        if (projectArray.count>0) {
            success(projectArray);
        }else{
            success(nil);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  查询我的项目详情
 *  @param request 请求体
 *  @param success 成功回调
 *  @param fail    失败回调
 */
- (void)myProjectDetailWithRequest:(NSString *)prjID
                              success:(void (^)(id responseObject))success
                              failure:(void (^)(NSError *error))fail{
    if(!prjID)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:prjID,@"projectId",nil];
    [[HttpClient sharedClient] POST:HTTP_MYPROJECTS_DETAIL_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        LogInfo(@"我的项目详情：%@",[dic dictionaryToJson]);
        ProjectItemModel *item = [ProjectItemModel objectWithKeyValues:[dic valueForKey:@"data"]];
        if(success){
            success(item);
        }
//        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
//
//        NSMutableArray *projectArray = [[NSMutableArray alloc]init];
//        for (NSDictionary *modelDic in model.data) {
//            ProjectItemModel *model = [ProjectItemModel objectWithKeyValues:modelDic];
//            [projectArray addObject:model];
//        }
//        
//        
//        if (projectArray.count>0) {
//            success(projectArray);
//        }else{
//            success(nil);
//        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}

/**
 *  查询某一资讯或项目是否被关注过
 *  0是资讯 1是项目
 */
//- (void)checkIfAttentionNewOrProjectWithRequest:(NSString *)cid
//                                        andType:(int)type
//                                        success:(void (^)(id responseObject))success
//                                        failure:(void (^)(NSError *error))fail
//{
//    NSString *str = [[ZRB_UserManager shareUserManager] userData].zrb_front_ut;
//    
//    if(!cid  || !str)
//        return;
//    
//    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:cid,@"id",str,@"zrb_front_ut",@(type),@"type",nil];
//    
//    
//    [[HttpClient sharedClient] POST:HTTP_IFTAKEATTENTION_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSDictionary  *dic = responseObject;
//        NSLog(@"%@",[dic dictionaryToJson]);
//        
//        
//        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
//        
//        if (success) {
//            success(model);
//        }
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        if (fail) {
//            fail(error);
//        }
//    }];
//    
//}



/**************************我的勘察*****************************/
/**
 *  请求我的勘察列表
 */
- (void)requestProspectListWithRequest:(ProspectsRequest *)request
                                        success:(void (^)(id responseObject))success
                                        failure:(void (^)(NSError *error))fail
{
    
    if(!request)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@(request.pageSize),@"pageSize",nil];
    
    if(request.maxInspectId){
        [dic setObject:@(request.maxInspectId) forKey:@"maxInspectId"];
    }
//    NSLog(@"%@?pageSize=%@&maxInspectId=%@",HTTP_MYPROSPECT_QUERY_URL,@(request.pageSize),@(request.maxInspectId));
    [[HttpClient sharedClient] POST:HTTP_MYPROSPECT_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"我的勘察列表：%@",[dic dictionaryToJson]);
        
//        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for(NSDictionary *dica in [dic valueForKey:@"data"]){
            ProspectModel *model = [ProspectModel objectWithKeyValues:dica];
            [array addObject:model];
        }

        if (array.count>0) {
            success(array);
        }else{
            success(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    
}

/**
 *  请求删除取消的取消勘察
 */
- (void)cancelProspectDeleteWithRequest:(NSString *)pid
                        success:(void (^)(id responseObject))success
                        failure:(void (^)(NSError *error))fail
{
    if(!pid)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:pid,@"inspectId",nil];
    
    
    [[HttpClient sharedClient] POST:HTTP_CANCELMYPROSPECTDELETE_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"%@",[dic dictionaryToJson]);
        
        
        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    
}

/**
 *  获取勘察详情
 *
 */
- (void)getProspectDetailWithRequest:(NSString *)iid
                            success:(void (^)(id responseObject))success
                            failure:(void (^)(NSError *error))fail
{
    if(!iid)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:iid,@"inspectId",nil];
    
    [[HttpClient sharedClient] POST:HTTP_MYPROSPECTDETAIL_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"%@",[dic dictionaryToJson]);
        
        
        ProspectDetailModel *model = [ProspectDetailModel objectWithKeyValues:[dic valueForKey:@"data"]];
        if(model){
            if(model.projectInfoVo){
                NSMutableArray  *arr = [[NSMutableArray alloc] init];
                for(NSDictionary    *dicInfo in model.projectInfoVo){
                    ProjectItemModel *item =  [ProjectItemModel objectWithKeyValues:dicInfo];
                    [arr addObject:item];
                }
                if(arr)
                    model.projectInfoVo = arr;
            }
            if(model.event){
                NSMutableArray  *arr = [[NSMutableArray alloc] init];
                for(NSDictionary    *dicInfo in model.event){
                    ProspectEventModel *item =  [ProspectEventModel objectWithKeyValues:dicInfo];
                    [arr addObject:item];
                }
                if(arr)
                    model.event = arr;
            }
        }
        if (success) {
            success(model);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    
}

/**
 *  取消勘察
 */
- (void)cancelProspectWithRequest:(NSDictionary *)dic
                                success:(void (^)(id responseObject))success
                                failure:(void (^)(NSError *error))fail
{
    if(!dic)
        return;
    
    NSMutableDictionary  *mdic = [NSMutableDictionary dictionaryWithDictionary:dic];

    [[HttpClient sharedClient] POST:HTTP_CANCELMYPROSPECT_QUERY_URL parameters:mdic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        NSLog(@"%@",[dic dictionaryToJson]);
        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
    
}
 
/**查询项目图片列表
 */
- (void)myProjectImagesWithRequest:(NSString *)prjID
                           success:(void (^)(id responseObject))success
                           failure:(void (^)(NSError *error))fail{
    if(!prjID)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:prjID,@"id",nil];
    [[HttpClient sharedClient] POST:HTTP_PROJECTPHOTOLIST_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;//[dic valueForKey:@"data"]
//        LogInfo(@"我的项目图片详情：%@",[dic dictionaryToJson]);
        NSMutableArray *iarray = [[NSMutableArray alloc] init];
        for(id data in dic){
            ProjectImageModel *item = [ProjectImageModel objectWithKeyValues:data];
            [iarray addObject:item];
        }
        if (iarray.count>0) {
            success(iarray);
        }else{
            success(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (fail) {
            fail(error);
        }
    }];
}
@end


/**
 * 项目列表请求类
 */
@implementation ProjectQueryListRequest

@end

/**
 *  获取项目类型请求类
 */
@implementation ProjectTypeRequest

@end

/**
 *  发送项目评论请求
 */
@implementation ProjectCommentRequest

@end


/**
 *  项目所有评论请求
 */
@implementation ProjectAllCommentRequest

@end

/**
 *  删除项目评论
 */
@implementation DeleteProjectCommentRequest


@end

/**
 *  关注项目
 */
@implementation CollectionProjectRequest


@end

/**
 *  取消关注
 */
@implementation CancelCollectionProjectRequest


@end

/**
 *  项目勘察
 */
@implementation InvestigationApplyRequest


@end

/**
 *  索要更多资料
 */
@implementation AskForMoreInfoRequest

@end
/**
 *  勘察申请
 */
@implementation ProspectsRequest
@end

/**
 *  城市详情数据
 */
@implementation CityDetailInfoRequest

@end

/**
 *  城市经济情况
 */
@implementation CityEconomicInfoRequest


@end

/**
 *  产业详情
 */
@implementation CityIndustryInfoRequest


@end

