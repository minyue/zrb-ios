//
//  AFBaseService.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFBaseService.h"
#import "Log.h"
#import "HttpClient.h"

@class FcousRequest;
@class CancelFcousRequest;

@interface AFBaseService : NSObject



- (void)serviceDemo:(NSString *)inputString
            success:(void (^)(id  responseOjb))success
               fail:(void (^)(NSError  *error))fail;


/**
 *  关注对象
 *
 *  @param request  请求类
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 *
 */
- (void)fcousObjectWithRequest:(FcousRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  取消关注对象
 *
 *  @param request  请求类
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 *
 */
- (void)cancelFcousObjectWithRequest:(FcousRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  判断是否被关注
 *
 *  @param cid      对象ID
 *  @param type     对象类型
 *
 */
- (void)checkIfAttentionNewOrProjectWithRequest:(NSString *)cid
                                        andType:(int)type
                                        success:(void (^)(id responseObject))success
                                        failure:(void (^)(NSError *error))fail;

@end

/**
 *  关注请求类
 */
@interface FcousRequest : NSObject

@property (nonatomic,strong) NSString *fcousObjectId;   //关注对象ID
@property (nonatomic,assign) int fcousType;             //关注类型

@end

/**
 *  取消关注请求类
 */
@interface CancelFcousRequest : NSObject

@property (nonatomic,strong) NSString *fcousObjectId;   //关注对象ID
@property (nonatomic,assign) int fcousType;             //关注类型

@end




