//
//  AFBaseService.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "AFBaseService.h"
#import "FcousModel.h"
#import "ZRB_NormalModel.h"

@implementation AFBaseService


- (void)serviceDemo:(NSString *)inputString
            success:(void (^)(id  responseOjb))success
               fail:(void (^)(NSError  *error))fail
{

    // 发送POST请求
    [[HttpClient sharedClient] POST:@"www.baidu.com" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        /*******成功返回处理逻辑**********/
        
        ZRB_BaseModel * demoModel = [ZRB_BaseModel objectWithKeyValues:responseObject];
        
        
        // 如果接口返回数据不加密，responseObject为NSDictionary类型，直接通过JSONModel方法反射出实体类
        
        
        // 如果有需要，此处添加其他业务逻辑，注意进行线程处理，不要影响UI主线程
        // 如果有需要，此处添加其他业务逻辑，注意进行线程处理，不要影响UI主线程
        // 如果有需要，此处添加其他业务逻辑，注意进行线程处理，不要影响UI主线程
        // 如果有需要，此处添加其他业务逻辑，注意进行线程处理，不要影响UI主线程
        
        
        // 调用success块
        if (success) {
            success(demoModel);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        /*******失败返回处理逻辑**********/
        
        if (fail) {
            fail(error);
        }
    }];

}


- (void)fcousObjectWithRequest:(FcousRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params  = [[NSMutableDictionary alloc]init];
    [params setObject:request.fcousObjectId forKey:@"id"];
    [params setObject:@(request.fcousType) forKey:@"type"];
    
    
    LogInfo(@"关注参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_FCOUS_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"关注：%@",[dic dictionaryToJson]);
        FcousModel *model = [FcousModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

- (void)cancelFcousObjectWithRequest:(CancelFcousRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
    NSMutableDictionary *params  = [[NSMutableDictionary alloc]init];
    [params setObject:request.fcousObjectId forKey:@"id"];
    [params setObject:@(request.fcousType) forKey:@"type"];
    
    
    LogInfo(@"取消关注参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_CANCEL_FCOUS_URL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"取消关注：%@",[dic dictionaryToJson]);
        FcousModel *model = [FcousModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];

    
}

/**
 *  查询某一资讯或项目是否被关注过
 *  0是资讯 1是项目
 */
- (void)checkIfAttentionNewOrProjectWithRequest:(NSString *)cid
                                        andType:(int)type
                                        success:(void (^)(id responseObject))success
                                        failure:(void (^)(NSError *error))fail
{
    if(!cid)
        return;
    
    NSMutableDictionary  *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:cid,@"id",@(type),@"type",nil];
    LogInfo(@"是否关注参数：%@",[dic dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_IFTAKEATTENTION_QUERY_URL parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *dic = responseObject;
        LogInfo(@"是否关注，%@",[dic dictionaryToJson]);
        
        
        ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
        
        if (success) {
            success(model);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LogInfo(@"是否关注error，%@",error);
        
        if (fail) {
            fail(error);
        }
    }];
    
}



@end


@implementation FcousRequest


@end

@implementation CancelFcousRequest


@end

