//
//  HttpClient.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface HttpClient : AFHTTPRequestOperationManager 

+ (instancetype)sharedClient;
- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
       constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
