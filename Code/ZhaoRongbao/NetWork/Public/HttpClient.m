//
//  HttpClient.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "HttpClient.h"
#import "ZRRequestManager.h"
#import "MineLoginViewcController.h"
#import "AppDelegate.h"

static NSString * const AFAppDotNetAPIBaseURLString = SERVER_URL;

@implementation HttpClient

+ (instancetype)sharedClient {
    static HttpClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[HttpClient alloc] initWithBaseURL:[NSURL URLWithString:AFAppDotNetAPIBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}

- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    //封装Token
    parameters = [ZRRequestManager reqestUrlAppentToken:parameters];
    return [super POST:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject){
            NSDictionary  *dic = responseObject;
//            NSLog(@"返回信息----%@",[dic dictionaryToJson]);
            ZRB_BaseModel *model = [ZRB_BaseModel objectWithKeyValues:dic];
            if(model.res){
                if(model.res ==ZRBHttpNoLoginType){
                    //过期，未登录
                    ZRB_UserManager *manager = [ZRB_UserManager  shareUserManager];
                    manager.userData = nil;
                    //自动登录
                    [(AppDelegate *)[UIApplication sharedApplication].delegate showUnvalidity];
                    success(operation,responseObject);
                }
                else if(model.res == ZRBHttpForceOffline){
                    //被T
                    [(AppDelegate *)[UIApplication sharedApplication].delegate showForcedoffline];
                    [ZRB_UserManager logOut];
                    success(operation,responseObject);
                }
//                else if(model.res == ZRBHttpServiceException){
//                    //服务器异常
//                    [(AppDelegate *)[UIApplication sharedApplication].delegate showServiceException];
//                    [ZRB_UserManager logOut];
//                    success(operation,nil);
//                }
                else{
                    success(operation,responseObject);
                }
            
            }else{
                success(operation,responseObject);
            }
        }
    } failure:failure];
}

- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
       constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    //封装Token
    parameters = [ZRRequestManager reqestUrlAppentToken:parameters];
    return [super POST:URLString parameters:parameters constructingBodyWithBlock:block success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject){
            NSDictionary  *dic = responseObject;
//            NSLog(@"返回信息----%@",[dic dictionaryToJson]);
            ZRB_BaseModel *model = [ZRB_BaseModel objectWithKeyValues:dic];
            if(model.res){
                if(model.res ==ZRBHttpNoLoginType){
                    //过期，未登录
                    ZRB_UserManager *manager = [ZRB_UserManager  shareUserManager];
                    manager.userData = nil;
                    //自动登录
                    [(AppDelegate *)[UIApplication sharedApplication].delegate showUnvalidity];
                    success(operation,nil);
                }
                else if(model.res == ZRBHttpForceOffline){
                    //被T
                    [(AppDelegate *)[UIApplication sharedApplication].delegate showForcedoffline];
                    [ZRB_UserManager logOut];
                    success(nil,nil);
                }
//                else if(model.res == ZRBHttpServiceException){
//                    //服务器异常
//                    [(AppDelegate *)[UIApplication sharedApplication].delegate showServiceException];
//                    [ZRB_UserManager logOut];
//                    success(nil,nil);
//                }
                else{
                    success(operation,responseObject);
                }
                
            }else{
                success(operation,responseObject);
            }
        }
    } failure:failure];
}


@end
