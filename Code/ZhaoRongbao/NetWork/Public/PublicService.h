//
//  PublicService.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  公共请求

#import "AFBaseService.h"

@interface PublicService : AFBaseService
/**
 *  获取所有行业
 *
 *  @param success NSMutableArray
 *  @param fail    nil
 */
- (void)getAllIndustryRequestWithsuccess:(void (^)(id responseObject))success
                                 failure:(void (^)(NSError *error))fail;
@end
