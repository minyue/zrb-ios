//
//  PublicService.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "PublicService.h"
#import "ZRB_NormalModel.h"
#import "IndustryModel.h"

@implementation PublicService
/**
 *  获取所有行业
 *
 *  @param success NSMutableArray
 *  @param fail    nil
 */
- (void)getAllIndustryRequestWithsuccess:(void (^)(id responseObject))success
                                 failure:(void (^)(NSError *error))fail{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(101) forKey:@"type"];
    
    [[HttpClient sharedClient] POST:HTTP_ALLINDUSTRY_QUERY_URL
                                       parameters:dic
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              
                                              NSDictionary  *dic = responseObject;
                                              NSLog(@"所有行业----%@",[dic dictionaryToJson]);
                                              
                                              ZRB_NormalModel  *model = [ZRB_NormalModel objectWithKeyValues:dic];
                                              if(model.res == 0){
                                                  success(nil);
                                              }else{
                                                  NSMutableArray  *array = [[NSMutableArray alloc] init];
                                                  for (NSDictionary *dic in (NSMutableArray*)model.data) {
                                                      IndustryModel *model = (IndustryModel*)[IndustryModel objectWithKeyValues:dic];
                                                      model.iid = [[dic valueForKey:@"id"] intValue];
                                                      [array addObject:model];
                                                  }
                                                  
                                                  if (success) {
                                                      success(array);
                                                  }
                                              }

                                              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              if (fail) {
                                                  fail(error);
                                              }
          }];

}
@end
