//
//  SearchProjectService.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "AFBaseService.h"

@class ProjectListRequest;

@interface SearchProjectService : AFBaseService

/**
 *  找项目首页列表
 *
 *  @param request  请求类
 *  @param success  请求成功回调
 *  @param failure  请求失败回调
 *
 */
- (void)projectHomeListWithRequest:(ProjectListRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;


@end



@interface ProjectListRequest : NSObject

@property (nonatomic,assign) NSString *lastPushTime;
@property (nonatomic,strong) NSString *rows;
@property (nonatomic,strong) NSString *postcode;            //城市邮编，多个用“,”隔开
@property (nonatomic,strong) NSString *industrys;           //行业，多个用“,”隔开

@end