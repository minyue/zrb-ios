//
//  SearchProjectService.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "SearchProjectService.h"
#import "SearchProjectHomeModel.h"

//#define PROJECT_URL    @"http://172.17.32.19:8080"


@implementation SearchProjectService


- (void)projectHomeListWithRequest:(ProjectListRequest *)request success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure{
    
   
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:request.lastPushTime forKey:@"lastPushTime"];
    [params setObject:request.rows forKey:@"rows"];
    
    if(request.postcode.length > 0){
        [params setObject:request.postcode forKey:@"postcode"];
    }
    
    if (request.industrys.length > 0) {
        [params setObject:request.industrys forKey:@"industrys"];
    }
    
     LogInfo(@"找项目首页参数：%@",[params dictionaryToJson]);
    
    [[HttpClient sharedClient] POST:HTTP_PROJECT_HOME_LIST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dic = responseObject;
        LogInfo(@"找项目首页列表：%@",[dic dictionaryToJson]);
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSDictionary *modelDic in [dic objectForKey:@"data"]) {
            
            [SearchProjectHomeItemModel setupReplacedKeyFromPropertyName:^NSDictionary *{
                return @{@"uid":@"id"};
            }];
            
            SearchProjectHomeItemModel *itemModel = [SearchProjectHomeItemModel objectWithKeyValues:modelDic];
            [array addObject:itemModel];
        }
        
        SearchProjectHomeModel *model = [[SearchProjectHomeModel alloc]init];
        model.res = [[dic objectForKey:@"res"] intValue];
        model.data = array;
        
        if (success) {
            success(model);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

@end




@implementation ProjectListRequest


@end