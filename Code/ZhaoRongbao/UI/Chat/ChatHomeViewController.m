//
//  ChatHomeViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/27.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ChatHomeViewController.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
//#import "ProjectLocationViewController.h"
//#import "CityDetailViewController.h"
#import "MineViewController.h"
#import "ChatListViewController.h"
#import "ChatSystemNotificationViewController.h"

@interface ChatHomeViewController ()
{
    IBOutlet UIScrollView   *_scrollView;
//    IBOutlet UISegmentedControl *_segmentController;
    MineViewController *_mineVC;
    ZRB_ViewController       *_currentVC;
    ChatListViewController  *_chatlistVC;
}
@end

@implementation ChatHomeViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [self setup];
    
//    if(_scrollView){
//        [_scrollView setContentOffset:CGPointMake(0, _scrollView.contentOffset.y) animated:NO];
//    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    CustomBarItem * _rightItem =  [self.navigationItem setItemWithTitle:@"发送" textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_rightItem setOffset:-2];
    [_rightItem addTarget:self selector:@selector(sendComment:) event:UIControlEventTouchUpInside];
    
}

- (void)setup{
    if ([ZRB_UserManager isLogin] ){
        if(_mineVC){
            [_mineVC.view removeFromSuperview];
            [_mineVC removeFromParentViewController];
            _mineVC = nil;
        }
        [self loadUI];
    }else{
        if(!_mineVC)
        {
            _mineVC = (MineViewController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineViewController class ]];
            _mineVC.imgName= @"message_login";
            _mineVC.loginSuccssBlock = ^{
                
            };
            [self addChildViewController:_mineVC];
            
            [_mineVC.view removeFromSuperview];
            _mineVC.view.frame = self.view.bounds;
            [self.view addSubview:_mineVC.view];
            [_mineVC didMoveToParentViewController:self];
        }
    }
}

- (void)loadUI{
    if(!_chatlistVC)
    {
        _chatlistVC = [[ChatListViewController alloc] init];
        [self addChildViewController:_chatlistVC];
        [_scrollView addSubview:_chatlistVC.view];
        
        UIView  *view = _chatlistVC.view;
        view.frameSize = _scrollView.frameSize;
        view.frameX = 0;
        view.frameY = 0;
        _scrollView.contentSize = CGSizeMake(_scrollView.frameWidth, self.view.frameHeight - self.navigationController.navigationBar.frameHeight -  self.tabBarController.tabBar.frameHeight);
        
        ChatSystemNotificationViewController *sysNotylistVC = (ChatSystemNotificationViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Chat" class:[ChatSystemNotificationViewController class]];
        [self addChildViewController:sysNotylistVC];
        [_scrollView addSubview:sysNotylistVC.view];
        
        UIView  *view1 = sysNotylistVC.view;
        view1.frameSize = _scrollView.frameSize;
        view1.frameX = _scrollView.frameWidth;
        view1.frameY = 0;
        _scrollView.contentSize = CGSizeMake(_scrollView.frameWidth * 2,self.view.frameHeight - self.navigationController.navigationBar.frameHeight -  self.tabBarController.tabBar.frameHeight);// _scrollView.contentSize.height
    }
}

- (void)showMineViewController
{
    [self transitionFromViewController:_currentVC toViewController:_mineVC duration:0.4 options:UIViewAnimationOptionLayoutSubviews animations:^{
        
    } completion:^(BOOL finished) {
        _currentVC = _mineVC;
    }];
    
}

- (void)sendComment:(id)sender{
//    ProjectLocationViewController *locationCtrl = [[ProjectLocationViewController alloc]init];
//    locationCtrl.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:locationCtrl animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)select:(id)sender{
    UISegmentedControl  *seg = (UISegmentedControl*)sender;
   
    [_scrollView setContentOffset:CGPointMake(_scrollView.frameWidth * seg.selectedSegmentIndex, _scrollView.contentOffset.y) animated:YES];
}


@end
