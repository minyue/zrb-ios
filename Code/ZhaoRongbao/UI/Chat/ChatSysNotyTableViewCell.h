//
//  ChatSysNotyTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/10.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatSystemNotyListModel.h"

static NSString *const chatSysNotyTableViewCellIdentifier = @"ChatSysNotyTableViewCellIdentifier";

@interface ChatSysNotyTableViewCell : UITableViewCell

- (void)initWithModel:(ChatSystemNotyListModel*)model;

- (void)setImageShow:(BOOL)set;
@end
