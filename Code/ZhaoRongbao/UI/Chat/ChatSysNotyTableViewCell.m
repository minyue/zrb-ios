//
//  ChatSysNotyTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/10.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ChatSysNotyTableViewCell.h"
#import "NSDate+Addition.h"

@interface ChatSysNotyTableViewCell(){
    IBOutlet UILabel    *_lblTitle;
    IBOutlet UILabel    *_lblContent;
    IBOutlet UILabel    *_lblTime;
    IBOutlet UIImageView    *_imgStatue;
}
@end

@implementation ChatSysNotyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithModel:(ChatSystemNotyListModel*)model{
    if(model.read == 0){
        _imgStatue.image = [UIImage imageNamed:@"sysmsg_pin1"];
    }else{
        _imgStatue.image = [UIImage imageNamed:@"sysmsg_pin2"];
    }
    [ZRBUtilities setSysNotyShow:_lblContent and:_lblTitle andModel:model];
    _lblTime.text = [NSDate timeFormatWithTimeStamp:[NSString stringWithFormat:@"%lld",model.createTime] format:@"MM/dd"];
}

- (void)setImageShow:(BOOL)set{
    if(set){
        _imgStatue.image = [UIImage imageNamed:@"sysmsg_pin1"];
    }else{
        _imgStatue.image = [UIImage imageNamed:@"sysmsg_pin2"];
    }
}

@end
