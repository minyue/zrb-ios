//
//  ChatSystemNotificationDetailController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/11.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  系统通知详情页面

#import "ZRB_ViewController.h"
#import "ChatSystemNotyListModel.h"

@interface ChatSystemNotificationDetailController : ZRB_ViewControllerWithBackButton

@property (nonatomic,strong) ChatSystemNotyListModel *currentModel;
@end
