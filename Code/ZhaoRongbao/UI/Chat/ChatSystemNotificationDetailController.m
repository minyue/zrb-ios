//
//  ChatSystemNotificationDetailController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/11.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ChatSystemNotificationDetailController.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "NSDate+Addition.h"
//#import "AllCommentViewController.h"
#import "MineEnterpriseCerViewController.h"

@interface ChatSystemNotificationDetailController ()
{
    IBOutlet UILabel    *_lblTitle;
    IBOutlet UILabel    *_lblContent;
    IBOutlet UILabel    *_lblTime;
    
    CustomBarItem *_item;
    
    NSString* _rightBtnName;
}
@end

@implementation ChatSystemNotificationDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    [ZRBUtilities setSysNotyShow:_lblContent and:_lblTitle andModel:_currentModel];
    _lblTime.text = [NSDate timeFormatWithTimeStamp:[NSString stringWithFormat:@"%lld",_currentModel.createTime] format:@"MM-dd"];
    
    [self setRightName];
}

- (void)setRightName{
    if([_currentModel.messageInfo.bizType  isEqualToString: @"investor_regiester_succes_without_org"]){
        //身份认证
        [self setRightItem:@"认证"];
    }else if([_currentModel.messageInfo.bizType  isEqualToString: @"proj_prog_new"]){
        //发布成功
        [self setRightItem:@"详情"];
    }else if([_currentModel.messageInfo.bizType  isEqualToString: @"proj_cment_relay"] || [_currentModel.messageInfo.bizType  isEqualToString: @"proj_cment_new"]){
        //回复评论
        [self setRightItem:@"查看"];
    }
}

- (void)setRightItem:(NSString*)rightBtnName
{
    NSString *title = rightBtnName;
    _rightBtnName = rightBtnName;
    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_item setOffset:-2];
    [_item addTarget:self selector:@selector(approve:) event:UIControlEventTouchUpInside];
}

- (void)approve:(id)sender{
    if([_rightBtnName isEqualToString:@"认证"]){
        //跳转认证
        MineEnterpriseCerViewController *eVC = (MineEnterpriseCerViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineEnterpriseCerViewController class]];
        eVC.thePageType = MineEnterpriceCerTypePersonal;
        [self.navigationController pushViewController:eVC animated:YES];
        return;

    }else if ([_rightBtnName isEqualToString:@"详情"]){
        //跳转项目详情
    }else if ([_rightBtnName isEqualToString:@"查看"]){
        //跳转评论
        NSArray *array = [_currentModel.messageInfo.bizKey componentsSeparatedByString:@":"];
        if(array.count==4){
//            AllCommentViewController *allCommentCtrl = [[AllCommentViewController alloc]init];
//            allCommentCtrl.projectID = array[0];
//            allCommentCtrl.currentCommentId = [array[1] intValue];
//            allCommentCtrl.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:allCommentCtrl animated:YES];
            return;
        }
    }
}


@end
