//
//  ChatSystemNotificationViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/10.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ChatSystemNotificationViewController.h"
#import "ChatSysNotyTableViewCell.h"
#import "MJRefresh.h"
#import "ChatService.h"
#import "ChatSystemNotyListModel.h"
#import "ChatSystemNotificationDetailController.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineProspectsDetailViewController.h"
#import "MinePersonalZRBAlertView.h"
#import "AppDelegate.h"
#import "SearchProjectDetailViewController.h"
#import "MoneyDetailViewController.h"
#import "PersonalCardViewController.h"

@interface ChatSystemNotificationViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    CustomBarItem *_item;
    MinePersonalZRBAlertView    *_alertView;
    NSMutableArray  *_listArray;
    IBOutlet    UITableView *_tableView;
    ChatService *_service;
    BOOL       _isRequst;
}
@end

@implementation ChatSystemNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    _listArray = [NSMutableArray array];
    _service = [[ChatService alloc] init];
    [self addHeaderView];
    [self addFooterView];
    [self refreshData];
    [self setRightItem];
}

- (void)setRightItem
{
//    NSString *title = @"咨询";
//    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
//    [_item setOffset:-2];
//    [_item addTarget:self selector:@selector(clickCall:) event:UIControlEventTouchUpInside];
    
    _item =  [self.navigationItem setItemWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_call"]] itemType:right];
    [_item addTarget:self selector:@selector(clickCall:) event:UIControlEventTouchUpInside];
    [_item setOffset:-2];
}

//咨询
-(IBAction)clickCall:(id)sender{
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString: @"点击呼叫“400-2525-888”\n若有任何疑问可联系我们人工客服\n"];
    if(_alertView != nil){
        [_alertView removeFromSuperview];
        _alertView = nil;
    }
    _alertView = [[MinePersonalZRBAlertView alloc] initWithText:attString];
    [_alertView showOnView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}

/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}


#pragma mark -
#pragma mark -网络请求



/**
 *  请求我关注的项目列表
 */
- (void)queryListData
{
    _isRequst = NO;
    ChatSystemNotyQueryListRequest *request = [[ChatSystemNotyQueryListRequest alloc] init];
    request.limit = 10;
    
    [self startWithReq:request];
    
}

/**
 *  加载更多
 */
- (void)loadMoreData
{
    if(_listArray.count > 0){
        ChatSystemNotyListModel *lastModel = (ChatSystemNotyListModel*)[_listArray lastObject];
//      LogInfo([NSString stringWithFormat:@"%d",lastModel.id]);
        ChatSystemNotyQueryListRequest *request = [[ChatSystemNotyQueryListRequest alloc]init];
        request.limit = 10;
        request.maxId = lastModel.uid;
        
        [self loadMoreWithReq:request];
    }
}


/**
 *  下拉加载
 *
 *  @param req 请求体
 */

- (void)startWithReq:(ChatSystemNotyQueryListRequest *)req
{
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_service chatSystemNotyQueryListWithRequest:req success:^(id responseObject) {
        [bself chatSysNotyListQueryListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(ChatSystemNotyQueryListRequest   *)req
{
    WS(bself);
    [_service chatSystemNotyQueryListWithRequest:req success:^(id responseObject) {
        [bself loadMoreListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
    
}




- (void)chatSysNotyListQueryListCallBackWithObject:(NSMutableArray *)arr
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    if(arr){
        _listArray = arr;
        [_tableView reloadData];
    }
    
    [_tableView.header endRefreshing];
    
}


- (void)loadMoreListCallBackWithObject:(NSMutableArray *)arr
{
    [_tableView.header endRefreshing];
    
    [_tableView.footer endRefreshing];
    if(arr.count>0){
        [_listArray addObjectsFromArray:arr];
        [_tableView reloadData];
    }else{
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
}


#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatSysNotyTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:chatSysNotyTableViewCellIdentifier forIndexPath:indexPath];
    [cell initWithModel:(ChatSystemNotyListModel*)[_listArray objectAtIndex:indexPath.row]];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatSystemNotyListModel *model = (ChatSystemNotyListModel*)_listArray[indexPath.row];
    //切换已读状态
    [_service readSystemNotyMessageWithRequest:[NSString stringWithFormat:@"%d",model.uid] success:^(BOOL back) {
        if(back){
            ChatSysNotyTableViewCell *cell = (ChatSysNotyTableViewCell*) [_tableView cellForRowAtIndexPath:indexPath];
            if(cell){
                [cell setImageShow:NO];
               ChatSystemNotyListModel* model = (ChatSystemNotyListModel*)[_listArray objectAtIndex:indexPath.row];
                model.read = 0;
                [_listArray replaceObjectAtIndex:indexPath.row withObject:model];
            }
        }
    } failure:^(NSError *error) {
        
    }];
    
    if([model.messageInfo.bizType isEqualToString:@"pro_fund_pulished"] || [model.messageInfo.bizType isEqualToString:@"pro_fund_collected"] || [model.messageInfo.bizType isEqualToString:@"pro_fund_changed"]){
        //* 1.项目/资金发布成功通知   //2.项目或资金关注成功  //3. 项目/资金关注意向变更通知
        if(model.messageInfo.bizKey)
        {
            NSArray *arr = [model.messageInfo.bizKey componentsSeparatedByString:@":"];
            if([arr count]>1){
                if([arr[1] isEqualToString:@"101"]){
                    //跳转到项目详情页
                    SearchProjectDetailViewController *detailCtrl = [[SearchProjectDetailViewController alloc]init];
                    detailCtrl.projectId = arr[0];
                    detailCtrl.hidesBottomBarWhenPushed = YES;
                    
                    [self.navigationController pushViewController:detailCtrl animated:YES];
                }else if([arr[1] isEqualToString:@"102"]){
                    //跳转到资金详情页
                    MoneyDetailViewController *detailCtrl = [[MoneyDetailViewController alloc]init];
                    detailCtrl.moneyId = arr[0];
                    detailCtrl.hidesBottomBarWhenPushed = YES;
                    
                    [self.navigationController pushViewController:detailCtrl animated:YES];
                }
            }else{
                if([model.messageInfo.bizKey isKindOfClass:[NSString class]]){
                    //查看名片功能 model.messageInfo.bizKey为关注人ID --
                    PersonalCardViewController *personalCardViewController = (PersonalCardViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Find" class:[PersonalCardViewController class]];
                    personalCardViewController.userId = model.messageInfo.bizKey;
                    personalCardViewController.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:personalCardViewController animated:YES];
                }
                //跳转到详情页面
                //发布详情
//                FindHomeItemModel *model = [_listArray objectAtIndex:indexPath.row];
//                if(model.type == 101 && model.projectId != nil){
//                    //项目详情
//                    SearchProjectDetailViewController *detailCtrl = [[SearchProjectDetailViewController alloc]init];
//                    detailCtrl.projectId = model.projectId;
//                    detailCtrl.hidesBottomBarWhenPushed = YES;
//                    
//                    [self.navigationController pushViewController:detailCtrl animated:YES];
//                }else if (model.type == 102 && model.projectId != nil){
//                    //资金详情
//                    MoneyDetailViewController *detailCtrl = [[MoneyDetailViewController alloc]init];
//                    detailCtrl.moneyId = model.projectId;
//                    detailCtrl.hidesBottomBarWhenPushed = YES;
//                    
//                    [self.navigationController pushViewController:detailCtrl animated:YES];
//                }else{
//                    //发现详情
//                    FindDetailViewController *detailCtrl = [[FindDetailViewController alloc]init];
//                    detailCtrl.hidesBottomBarWhenPushed = YES;
//                    detailCtrl.model = model;
//                    
//                    [self.navigationController pushViewController:detailCtrl animated:YES];
//                }

            }
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
