//
//  FIndAreaViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZRB_ViewController.h"
#import "MineService.h"
#import "AreaModel.h"

@interface AreaViewController : ZRB_ViewControllerWithBackButton<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    MineService *_mineService;
    
    UITableView *_mTableView;
}

@property (nonatomic,strong) NSMutableArray *statusArray;
@property (nonatomic,strong) NSMutableArray *areaModelArray;

@property (nonatomic,strong) NSMutableDictionary *selectedCityNameDic;              //已选的城市名称
@property (nonatomic,strong) NSMutableDictionary *selectedCityPostCodeDic;          //已选的城市邮编

@end
