//
//  FIndAreaViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "AreaViewController.h"
#import "FindAreaCell.h"

#define CHECK_KEY           @"checked"

@interface AreaViewController ()

@end

@implementation AreaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    [self getAllProvice];
}

- (void)viewDidDisappear:(BOOL)animated{
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:self.selectedCityPostCodeDic,self.selectedCityNameDic, nil];
    LogInfo(@"已选城市名称和ID：%@",array);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectCity" object:array];
}

- (void)setup{
    
    self.title = @"地区";
    
    _mineService = [[MineService alloc]init];
    _areaModelArray = [[NSMutableArray alloc]init];
    _statusArray = [[NSMutableArray alloc]init];
    
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
    
}


#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _areaModelArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *indentifierStr = @"FindAreaCell";
   FindAreaCell *cell = (FindAreaCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[FindAreaCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifierStr];
    }
    
    AreaModel *model = [_areaModelArray objectAtIndex:indexPath.row];
    cell.name.text = model.cityName;
    
    NSMutableDictionary *dic = [_statusArray objectAtIndex:indexPath.row];

    if ([[dic objectForKey:CHECK_KEY] isEqualToString:@"YES"]) {
        
        [dic setObject:@"YES" forKey:CHECK_KEY];
        [cell setChecked:YES];

    }else {
        
        [dic setObject:@"NO" forKey:CHECK_KEY];
        [cell setChecked:NO];

    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [self cellClicked:indexPath];
}

//cell点击响应事件
- (void)cellClicked:(NSIndexPath *)indexPath{
    
    FindAreaCell *cell = (FindAreaCell*)[_mTableView cellForRowAtIndexPath:indexPath];
    AreaModel *itemModel = _areaModelArray[indexPath.row];
    
    NSString *key = [NSString stringWithFormat:@"%ld",indexPath.row];
    NSUInteger row = [indexPath row];
    NSMutableDictionary *dic = [_statusArray objectAtIndex:row];
    if ([[dic objectForKey:CHECK_KEY] isEqualToString:@"NO"]) {
        
        [dic setObject:@"YES" forKey:CHECK_KEY];
        [cell setChecked:YES];
        
        [self.selectedCityNameDic setObject:itemModel.cityName forKey:key];
        [self.selectedCityPostCodeDic setObject:itemModel.postcode forKey:key];
        
    }else {
        
        [dic setObject:@"NO" forKey:CHECK_KEY];
        [cell setChecked:NO];
        
        [self.selectedCityNameDic removeObjectForKey:key];
        [self.selectedCityPostCodeDic removeObjectForKey:key];
    }
    
    LogInfo(@"selectedCityPostCodeDic :%@",self.selectedCityPostCodeDic);
    if ([self.selectedCityPostCodeDic allValues].count > 5 && indexPath.row > 0) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"超过5个城市默认为全国" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag = indexPath.row;
        [alert show];
        
    }
    
    
    if(indexPath.row == 0){
        for (int i = 1; i < _statusArray.count; i++) {
            NSString *key1 = [NSString stringWithFormat:@"%d",i];
            FindAreaCell *cell1 = (FindAreaCell*)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            [cell1 setChecked:NO];
            
            [[_statusArray objectAtIndex:i] setObject:@"NO" forKey:CHECK_KEY];
            [self.selectedCityNameDic removeObjectForKey:key1];
            [self.selectedCityPostCodeDic removeObjectForKey:key1];
        }
        
    }else{
        FindAreaCell *topCell = (FindAreaCell*)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [topCell setChecked:NO];
        
        [[_statusArray objectAtIndex:0] setObject:@"NO" forKey:CHECK_KEY];
        [self.selectedCityNameDic removeObjectForKey:@"0"];
        [self.selectedCityPostCodeDic removeObjectForKey:@"0"];
    }
}

#pragma mark delegate of UIAlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        FindAreaCell *cell = (FindAreaCell *)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:alertView.tag inSection:0]];
        [cell setChecked:NO];
        
        NSString *key = [NSString stringWithFormat:@"%ld",alertView.tag];
        [self.selectedCityNameDic removeObjectForKey:key];
        [self.selectedCityPostCodeDic removeObjectForKey:key];
        
        [[_statusArray objectAtIndex:alertView.tag] setObject:@"NO" forKey:CHECK_KEY];
    }else if (buttonIndex == 1){
        
        [self cellClicked:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    
}


#pragma mark 网络请求
- (void)getAllProvice{
    
    [SVProgressHUD show];
    WS(bself);
    [_mineService getAllProvinceInfoWithSuccess:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        
        //手动添加
        AreaModel *model = [[AreaModel alloc]init];
        model.postcode = @"0";
        model.cityName = @"全国";
        [_areaModelArray addObject:model];
        
        [_areaModelArray addObjectsFromArray:responseObject];
        
        for (int i = 0; i < _areaModelArray.count; i++) {
            
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            AreaModel *itemModel = _areaModelArray[i];
            
            if ([[self.selectedCityPostCodeDic allValues] containsObject:itemModel.postcode]) {
                [dic setValue:@"YES" forKey:CHECK_KEY];
                LogInfo(@"已选");
            }else{
                [dic setValue:@"NO" forKey:CHECK_KEY];
            }
            
            [_statusArray addObject:dic];
        }
        
        [_mTableView reloadData];
    } failure:^{
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:kHttpFailError];
        
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
