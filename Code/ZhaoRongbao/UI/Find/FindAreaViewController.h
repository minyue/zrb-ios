//
//  FindAreaViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "MineService.h"
#import "AreaModel.h"

@interface FindAreaViewController : ZRB_ViewControllerWithBackButton<UITableViewDataSource,UITableViewDelegate>{
    
    MineService *_mineService;
    UITableView *_mTableView;
}


@property (nonatomic,strong) NSString *selectedCityName;
@property (nonatomic,strong) NSString *selectedCityPostcode;

@property (nonatomic,strong) NSMutableArray *areaModelArray;

@end
