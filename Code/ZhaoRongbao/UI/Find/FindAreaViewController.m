//
//  FindAreaViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindAreaViewController.h"
#import "FindAreaCell.h"


@interface FindAreaViewController ()

@end

@implementation FindAreaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    [self getAllProvice];
}

- (void)setup{
    
    self.title = @"地区";
    
    _mineService = [[MineService alloc]init];
    _areaModelArray = [[NSMutableArray alloc]init];
    
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
    
}


#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _areaModelArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifierStr = @"FindAreaCell";
    FindAreaCell *cell = (FindAreaCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[FindAreaCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifierStr];
    }
    
    AreaModel *model = [_areaModelArray objectAtIndex:indexPath.row];
    cell.name.text = model.cityName;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AreaModel *model = [_areaModelArray objectAtIndex:indexPath.row];
    self.selectedCityName = model.cityName;
    self.selectedCityPostcode = model.postcode;
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:self.selectedCityPostcode,self.selectedCityName, nil];
    LogInfo(@"已选行业字典名称和ID：%@",array);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectSingleCity" object:array];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 网络请求
- (void)getAllProvice{
    
    [SVProgressHUD show];
    WS(bself);
    [_mineService getAllProvinceInfoWithSuccess:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        
        //手动添加
        AreaModel *model = [[AreaModel alloc]init];
        model.postcode = @"0";
        model.cityName = @"全国";
        [_areaModelArray addObject:model];
        
        [_areaModelArray addObjectsFromArray:responseObject];
        [_mTableView reloadData];
    } failure:^{
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:kHttpFailError];
        
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
