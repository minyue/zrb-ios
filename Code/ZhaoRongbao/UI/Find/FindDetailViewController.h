//
//  FindDetailViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "FindService.h"
#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"
#import "FindMenuTagView.h"
#import "FindHomeModel.h"

#import "UMSocialShakeService.h"
#import "ShareMenuView.h"
#import "ShareManager.h"

@interface FindDetailViewController : ZRB_ViewControllerWithBackButton<UIWebViewDelegate, NJKWebViewProgressDelegate,UMSocialUIDelegate,UMSocialShakeDelegate>
{
    FindService *_findService;
    
    UIWebView  *_webView;
    
    UIButton *_focusButton;
    UIButton *_messageButton;
    UIButton *_shareButton;
    
    NJKWebViewProgressView *_progressView;
    NJKWebViewProgress *_progressProxy;
    
    ShareMenuView *_shareMenu;
}

@property (nonatomic,assign) int index;
@property (nonatomic,strong) FindHomeItemModel *model;

@property (nonatomic,strong) ShareMenuView *shareMenu;
@property (nonatomic,strong) FindMenuTagView *focusTag;
@property (nonatomic,strong) FindMenuTagView *messageTag;

@property (nonatomic,copy) NSMutableString *shareContent;
@property (nonatomic,copy) NSString *shareUrl;

@end
