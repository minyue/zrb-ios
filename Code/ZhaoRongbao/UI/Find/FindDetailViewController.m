//
//  FindDetailViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindDetailViewController.h"
#import "MessageViewController.h"
#import "MineLoginViewcController.h"
#import "FcousModel.h"

@interface FindDetailViewController ()

@end

@implementation FindDetailViewController

@synthesize shareMenu = _shareMenu;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    
    [self loadDetailWithId:_model.uid];
    
    //留言计数+1 通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(messageAdd:) name:LIST_MESSAGE_ADD object:nil];
}

- (void)messageAdd:(id)sender{
    
    LogInfo(@"详情页面留言：%d",[_model.messageAmount intValue]);
    _model.messageAmount = [NSString stringWithFormat:@"%d",[_model.messageAmount intValue] + 1];
     [_messageTag initHeadTagView:@"comment" title:[NSString stringWithFormat:@"%d人留言",[_model.messageAmount intValue]] titleColor:@"a6a6a6"];
}

- (void)setup{
    
    self.title = @"详情";
    _findService = [[FindService alloc]init];
    
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -64)];
    _webView.autoresizesSubviews = YES;
    _webView.scalesPageToFit = YES;
    _webView.delegate = _progressProxy;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_webView];
    
    [self initBottomMenuView];
    
    [self initShareMenu];
}

- (void)initBottomMenuView{
    
    UIView *bottomMenuView = [[UIView alloc]init];
    bottomMenuView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomMenuView];
    
    [bottomMenuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.height.equalTo(@50);
    }];
    
    
    //关注
    _focusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _focusButton.backgroundColor = [UIColor clearColor];
    [_focusButton addTarget:self action:@selector(fcousButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomMenuView addSubview:_focusButton];
    
     _focusTag = [[FindMenuTagView alloc]init];
    
    if ([_model.attention isEqualToString:@"0"] == YES) {
      [_focusTag initHeadTagView:@"like" title:[NSString stringWithFormat:@"%@人关注",_model.attentionTimes] titleColor:@"a6a6a6"];
    }else{
        [_focusTag initHeadTagView:@"like_hl" title:[NSString stringWithFormat:@"%@人关注",_model.attentionTimes] titleColor:@"a6a6a6"];
    }
    
    [_focusButton addSubview:_focusTag];
    
    [_focusTag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_focusButton.mas_centerY);
        make.centerX.equalTo(_focusButton.mas_centerX);
    }];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"dedede"];
    [bottomMenuView addSubview:line];
    
    //留言
    _messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _messageButton.backgroundColor = [UIColor clearColor];
    [_messageButton addTarget:self action:@selector(messageButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomMenuView addSubview:_messageButton];
    
     _messageTag = [[FindMenuTagView alloc]init];
    [_messageTag initHeadTagView:@"comment" title:[NSString stringWithFormat:@"%@人留言",_model.messageAmount] titleColor:@"a6a6a6"];
    [_messageButton addSubview:_messageTag];
    
    [_messageTag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_messageButton.mas_centerY);
        make.centerX.equalTo(_messageButton.mas_centerX);
    }];
    
    UIView *line1 = [[UIView alloc]init];
    line1.backgroundColor = [UIColor colorWithHexString:@"dedede"];
    [bottomMenuView addSubview:line1];
    
    //分享
    _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _shareButton.backgroundColor = [UIColor clearColor];
    [_shareButton addTarget:self action:@selector(sharedFind:) forControlEvents:UIControlEventTouchUpInside];
    [bottomMenuView addSubview:_shareButton];
    
    FindMenuTagView *_shareTag = [[FindMenuTagView alloc]init];
    [_shareTag initHeadTagView:@"share" title:@"分享" titleColor:@"a6a6a6"];
    [_shareButton addSubview:_shareTag];
    
    [_shareTag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_shareButton.mas_centerY);
        make.centerX.equalTo(_shareButton.mas_centerX);
    }];
    
    
    [_focusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomMenuView.mas_left);
        make.top.bottom.equalTo(bottomMenuView);
        make.width.equalTo(SCREEN_WIDTH*1/3);
    }];

    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_focusButton.mas_right);
        make.top.bottom.equalTo(bottomMenuView);
        make.width.equalTo(@0.5);
    }];

    [_messageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(line.mas_right);
        make.bottom.top.equalTo(bottomMenuView);
        make.width.equalTo(SCREEN_WIDTH*1/3);
    }];
    
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_messageButton.mas_right);
        make.bottom.top.equalTo(bottomMenuView);
        make.width.equalTo(@0.5);
    }];
    
    [_shareButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(line1.mas_right);
        make.bottom.top.equalTo(bottomMenuView);
        make.width.equalTo(SCREEN_WIDTH*1/3);
    }];

}

//关注按钮
- (void)fcousButtonClicked:(id)sender{
    
    if ([ZRB_UserManager isLogin]) {
        
        if ([_model.attention isEqualToString:@"0"]) {
            
            [self fcousIntent:_model];
        }else if([_model.attention isEqualToString:@"1"]){
            
            [self cancelFcousIntent:_model];
        }
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.hidesBottomBarWhenPushed = YES;
        loginCtrl.isFindHomeRefresh = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
    
}

//留言按钮点击
- (void)messageButtonClicked:(id)sender{
    if ([ZRB_UserManager isLogin]) {
        MessageViewController *messageCtrl = [[MessageViewController alloc]init];
        messageCtrl.hidesBottomBarWhenPushed = YES;
        messageCtrl.intentId = _model.uid;
        
        [self.navigationController pushViewController:messageCtrl animated:YES];
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
}

//分享按钮点击
- (void)sharedFind:(id)sender{
    if(!_model.uid)
        return;
   _shareUrl = [NSString stringWithFormat:@"%@/intention/details?id=%@",SERVER_URL,_model.uid];
    _shareContent = [[NSMutableString alloc]initWithString:@"向您推荐 "];
    if(_model.intro)
        [_shareContent appendString:_model.intro];
    
    LogInfo(_shareUrl);
    
    [_shareMenu show];
    
}


//初始化分享菜单
- (void)initShareMenu{

    _shareMenu = [[ShareMenuView alloc]init];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_shareMenu];
    
    WS(bself);
    _shareMenu.shareButtonClickBlock = ^(NSInteger index){
        LogInfo(@"%ld",index);
        
        switch (index) {
            case ShareToQQ:
                [[ShareManager ShareManager] QQShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToQQZone:
                [[ShareManager ShareManager] QQZoneShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToWechat:
                [[ShareManager ShareManager] WXChatShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToWechatFriend:
                [[ShareManager ShareManager] WXFriendShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToSinaWeibo:
                [[ShareManager ShareManager] SinaWeiboShareWithViewControll:bself.navigationController shareContent:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToCopy:
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = bself.shareUrl;
                
                [bself.shareMenu hide];
                break;
            }
            default:
                break;
        }
    };
}




/**
 *  加载详情页面
 *
 *  @param intentId 意向ID
 */
- (void)loadDetailWithId:(NSString *)intentId
{
    NSString  *url = [NSString stringWithFormat:@"%@/intention/details?id=%@",SERVER_URL,intentId];
    LogInfo(url);
    NSURL *webUrl = [NSURL URLWithString:url];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:webUrl];
    [_webView loadRequest:req];
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
//    self.title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}



- (void)webViewDidFinishLoad:(UIWebView *)webView{
}

#pragma mark 网络请求

/**
 *  意向关注
 */
- (void)fcousIntent:(FindHomeItemModel *)itemModel{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousIntent;
    request.fcousObjectId =  itemModel.uid;
    
    [SVProgressHUD show];
    
    AFBaseService *baseService = [[AFBaseService alloc]init];
    [baseService fcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"关注成功" onView:self.view];
                    
                    itemModel.attentionTimes = [NSString stringWithFormat:@"%d",[itemModel.attentionTimes intValue]+1];
                    itemModel.attention = @"1";
                    
                    //关注计数+1
                    [_focusTag initHeadTagView:@"like_hl" title:[NSString stringWithFormat:@"%@人关注",_model.attentionTimes] titleColor:@"a6a6a6"];
                    
                    NSArray *array = [[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"%d",self.index],@"1", nil];
                    //通知主页面
                    [[NSNotificationCenter defaultCenter] postNotificationName:LIST_FCOUS object:array];
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"已关注"];
                }
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"关注失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
    }];
}

/**
 *  取消关注
 */
- (void)cancelFcousIntent:(FindHomeItemModel *)itemModel{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousIntent;
    request.fcousObjectId =  itemModel.uid;
    
    [SVProgressHUD show];
    
    AFBaseService *baseService = [[AFBaseService alloc]init];
    [baseService fcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"已取消关注" onView:self.view];
                    
                    itemModel.attentionTimes = [NSString stringWithFormat:@"%d",[itemModel.attentionTimes intValue]- 1];
                    itemModel.attention = @"0";
                    
                    //关注计数+1
                    [_focusTag initHeadTagView:@"like" title:[NSString stringWithFormat:@"%@人关注",_model.attentionTimes] titleColor:@"a6a6a6"];
                    
                    NSArray *array = [[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"%d",self.index],@"0", nil];
                    //通知主页面
                    [[NSNotificationCenter defaultCenter] postNotificationName:LIST_FCOUS object:array];
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"取消关注失败"];
                }
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"取消关注失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
    }];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
