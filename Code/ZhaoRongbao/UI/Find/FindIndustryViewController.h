//
//  FindIndustryViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "FindService.h"

@interface FindIndustryViewController : ZRB_ViewControllerWithBackButton<UITableViewDataSource,UITableViewDelegate>
{
    FindService *_findService;
    UITableView *_mTableView;
}

@property (nonatomic,strong) NSMutableArray *industryItemModelArray;

@property (nonatomic,strong) NSString *selectedIndustryName;      //已选的行业名称
@property (nonatomic,strong) NSString *selectedIndustryId;        //已选的行业ID

@property (nonatomic,assign) BOOL isHasNolimit;

@end
