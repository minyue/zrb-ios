//
//  FindIndustryViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindIndustryViewController.h"
#import "FindIndustryCell.h"
#import "FindIndustryModel.h"

@interface FindIndustryViewController ()

@end

@implementation FindIndustryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    [self getAllIndustry];
}

- (void)setup{
    
    self.title = @"行业";
    
    _findService = [[FindService alloc]init];
    
    self.industryItemModelArray = [[NSMutableArray alloc]init];
    
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _industryItemModelArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *indentifierStr = @"FindIndustry";
    FindIndustryCell *cell = (FindIndustryCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[FindIndustryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifierStr];
    }
    
    FindIndustryItemModel *model = [_industryItemModelArray objectAtIndex:indexPath.row];
    [cell initWithModel:model];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    FindIndustryItemModel *model = [_industryItemModelArray objectAtIndex:indexPath.row];
    self.selectedIndustryId = model.uid;
    self.selectedIndustryName = model.industryName;
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:self.selectedIndustryId,self.selectedIndustryName, nil];
    LogInfo(@"已选行业字典名称和ID：%@",array);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectIndustry" object:array];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 网络请求

//获取所有行业
- (void)getAllIndustry{
    
    //请求指示器
    [HUDManager showLoadNetWorkHUDInView:self.view];
    //开始请求
    IndustryRequest *request = [[IndustryRequest alloc]init];
    if (_isHasNolimit) {
        request.type = @"101";
    }else{
        request.type = @"";
    }
    
    [_findService industryWithRequest:request success:^(id responseObject) {
        
        [HUDManager removeHUDFromView:self.view];
        
        FindIndustryModel *model = responseObject;
        
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                _industryItemModelArray = model.data;
                
                [_mTableView reloadData];
                break;
            }
            case ZRBHttpFailType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        LogInfo(@"error:%@",error);
        [HUDManager removeHUDFromView:self.view];
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            
        }];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
