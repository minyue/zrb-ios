//
//  FindViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "MineLoginViewcController.h"
#import "FindService.h"
#import "MJRefresh.h"
#import "PersonalCardViewController.h"
#import "UMSocialShakeService.h"
#import "ShareMenuView.h"
#import "ShareManager.h"

@interface FindViewController : ZRB_ViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UMSocialUIDelegate,UMSocialShakeDelegate>
{
    FindService *_findService;
    UITableView *_mTableView;
    
    BOOL  _isRequst;            //是否请求过
    
    ShareMenuView *_shareMenu;
}

@property (strong, nonatomic) NSMutableDictionary *offscreenCells;

@property (nonatomic,copy)ShareMenuView *shareMenu;

@property (nonatomic,strong) NSMutableArray *modelArray;

@property (nonatomic,copy) NSMutableString *shareContent;

@property (nonatomic,copy) NSString *shareUrl;

@property (nonatomic,assign) int modifyIndex;           //编辑意向的序列

@end
