//
//  FindViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindViewController.h"
#import "PushViewController.h"
#import "MessageViewController.h"
#import "FindDetailViewController.h"
#import "SearchProjectDetailViewController.h"
#import "MoneyDetailViewController.h"
#import "HeadTagView.h"
#import "FindHomeCell.h"
#import "FindHomeModel.h"
#import "FcousModel.h"

#import "RecommendViewController.h"


#define HEAD_HEIGHT             50

#define CellIdentifierFirst                 @"FindHomeFirstCell"
#define CellIdentifier                      @"FindHomeCell"

#define CellIdentifierWithOutTime   @"FindHomeCellWithOutTime"

@interface FindViewController ()

@end



@implementation FindViewController

@synthesize shareMenu = _shareMenu;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    [self refreshData];
    
    [self initShareMenu];
}

- (void)setup{
    
    self.title = @"发现";
    
    _isRequst = NO;
    _findService = [[FindService alloc]init];
    _modelArray = [[NSMutableArray alloc]init];
    _offscreenCells = [[NSMutableDictionary alloc]init];
    
    [self initHeadView];

     _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, HEAD_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 64- 49 - HEAD_HEIGHT) style:UITableViewStylePlain];
     _mTableView.backgroundColor = [UIColor clearColor];
     _mTableView.delegate = self;
     _mTableView.dataSource = self;
     _mTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
    
    [self addFooterView];
    [self addHeaderView];
    
    //登录后刷新首页
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAfterLogin:) name:FIND_HOME_REFRESH object:nil];
    
    //留言计数+1 通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(messageAdd:) name:LIST_MESSAGE_ADD object:nil];
    
    //分享计数+1 通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fcousChanged:) name:LIST_FCOUS object:nil];
    
    //意向发布后刷新数据
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAfterPush:) name:INTENT_PUSH object:nil];
    
    //意向修改后更新数据
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAfterModify:) name:INTENT_MODIFY object:nil];
    
    //意向删除后更新列表
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAfterDelete:) name:INTENT_DELETE object:nil];
}

- (void)initShareMenu{

    _shareMenu = [[ShareMenuView alloc]init];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_shareMenu];
    
    WS(bself);
    _shareMenu.shareButtonClickBlock = ^(NSInteger index){
        LogInfo(@"%ld",index);
        
        switch (index) {
            case ShareToQQ:
                [[ShareManager ShareManager] QQShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToQQZone:
                [[ShareManager ShareManager] QQZoneShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToWechat:
                [[ShareManager ShareManager] WXChatShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToWechatFriend:
                [[ShareManager ShareManager] WXFriendShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToSinaWeibo:
                [bself.shareMenu hide];
                [[ShareManager ShareManager] SinaWeiboShareWithViewControll:bself shareContent:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToCopy:
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = bself.shareUrl;
                
                [bself.shareMenu hide];
                
                break;
            }
            default:
                break;
        }
    };
}


- (void)refreshAfterLogin:(id)sender{
    LogInfo(@"刷新列表");
    [_mTableView.header beginRefreshing];
}

//接收留言成功通知
- (void)messageAdd:(id)sender{
   
    NSNotification *notify = (NSNotification *)sender;
    int index =[notify.object intValue];
    
    LogInfo(@"增加留言 index:%d",index);
    FindHomeItemModel *itemModel = [_modelArray objectAtIndex:index];
    itemModel.messageAmount = [NSString stringWithFormat:@"%d",[itemModel.messageAmount intValue]+1];
    
    [_mTableView reloadData];
    
}

//接收意向详情页面关注状态改变通知
- (void)fcousChanged:(id)sender{
    
    NSNotification *notify = (NSNotification *)sender;
    NSArray *array = notify.object;
    
    int index = [[array objectAtIndex:0] intValue];
    int flag = [[array objectAtIndex:1] intValue];
    
    LogInfo(@"关注数量改变：%@",[array objectAtIndex:1]);
    
    FindHomeItemModel *itemModel = _modelArray[index];
    
    if (flag == 1) {
       itemModel.attention = @"1";
       itemModel.attentionTimes = [NSString stringWithFormat:@"%d",[itemModel.attentionTimes intValue] + 1];
       
    }else if(flag == 0){
        itemModel.attention = @"0";
        itemModel.attentionTimes = [NSString stringWithFormat:@"%d",[itemModel.attentionTimes intValue] - 1];
        
    }
    
    [_mTableView reloadData];
}

- (void)refreshAfterPush:(id)sender{
    
    [_mTableView.header beginRefreshing];
}

- (void)refreshAfterModify:(id)sender{
    
    LogInfo(@"刷新被修改的cell %d",_modifyIndex);
    //更新数据源
    NSNotification *notify = (NSNotification *)sender;
    FindHomeItemModel *itemModel = (FindHomeItemModel *)notify.object;
    _modelArray[_modifyIndex] = itemModel;
    
    //刷新被修改的cell
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_modifyIndex inSection:0];
//    [_mTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];

    [_mTableView reloadData];
}

- (void)refreshAfterDelete:(id)sender{
    
    [_modelArray removeObjectAtIndex:_modifyIndex];
    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_modifyIndex inSection:0];
//    [_mTableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationLeft];
    
    [_mTableView reloadData];
}


/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        [_mTableView.header beginRefreshing];
    }
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _mTableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
    
}


/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _mTableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}


- (void)initHeadView{
    
    NSArray *titleArray = @[@"我要发布项目",@"我要发布资金"];
    NSArray *iconArray = @[@"icon_xm",@"icon_zj"];
    NSArray *colorArray = @[@"303a4a",@"303a4a"];
    
    UIImageView *headView = [[UIImageView alloc]init];
    headView.image = [UIImage imageNamed:@"found_bar_follow_bg"];
    headView.userInteractionEnabled = YES;
    [self.view addSubview:headView];
    
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.width.equalTo(self.view.mas_width);
        make.top.equalTo(self.view.mas_top);
        make.height.equalTo(@HEAD_HEIGHT);
    }];
    
    UIView *helpLine = [[UIView alloc]init];
    helpLine.backgroundColor= [UIColor colorWithHexString:@"e5e6eb"];
    [headView addSubview:helpLine];
    
    UIView *middleLine = [[UIView alloc]init];
    middleLine.backgroundColor= [UIColor colorWithHexString:@"dedede"];
    [headView addSubview:middleLine];
    
    for (int i = 0; i < titleArray.count; i++) {
        
        UIButton *itemView = [UIButton buttonWithType:UIButtonTypeCustom];
        itemView.backgroundColor =  [UIColor clearColor];
        itemView.tag = i;
//        itemView.layer.masksToBounds = YES;
//        itemView.layer.cornerRadius = 13.0f;
//        itemView.layer.borderWidth = 0.5f;
//        itemView.layer.borderColor = [UIColor colorWithHexString:@"0077d9"].CGColor;
//        [itemView setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"eaf4ff"]] forState:UIControlStateHighlighted];
//        [itemView setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
        [itemView addTarget:self action:@selector(findProjectOrMoney:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:itemView];
        
        
        HeadTagView *tagView = [[HeadTagView alloc]init];
        tagView.backgroundColor = [UIColor clearColor];
        tagView.userInteractionEnabled = NO;
        [tagView initHeadTagView:iconArray[i] title:titleArray[i] titleColor:colorArray[i]];
        [itemView addSubview:tagView];
        
        [itemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(headView.mas_left).offset((SCREEN_WIDTH*i/2+15));
            make.width.equalTo(SCREEN_WIDTH/2 - 15*2);
            make.top.equalTo(headView.mas_top);
            make.bottom.equalTo(headView.mas_bottom);
        }];
        
        [tagView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(itemView.mas_centerX);
            make.centerY.equalTo(itemView.mas_centerY);
        }];
    }
    
    [middleLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(headView.mas_height);
        make.centerX.equalTo(headView.mas_centerX);
        make.top.equalTo(headView.mas_top);
        make.width.equalTo(@0.5);
    }];
    
    [helpLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headView.mas_left);
        make.width.equalTo(headView.mas_width);
        make.height.equalTo(@0.5);
        make.bottom.equalTo(headView.mas_bottom);
    }];
    
}

//发布资金和项目
- (void)findProjectOrMoney:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    
    if ([ZRB_UserManager isLogin]) {
        
        PushViewController *pushCtrl = [[PushViewController alloc]init];
        pushCtrl.hidesBottomBarWhenPushed = YES;
        pushCtrl.intentType = button.tag;
        
        if(button.tag == 0){
            pushCtrl.navTitle = @"发布项目";
        }else if(button.tag == 1){
            pushCtrl.navTitle = @"发布资金";
        }
        
        [self.navigationController pushViewController:pushCtrl animated:YES];
    }else{
        
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
//    RecommendViewController *recommendCtrl = [[RecommendViewController alloc]init];
//    [self.navigationController pushViewController:recommendCtrl animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _modelArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float height = 0.0f;
    
    if (indexPath.row == 0) {
        
        height = 230.0f;
        
    }else{
        
        FindHomeItemModel *lastModel = _modelArray[indexPath.row - 1];
        FindHomeItemModel *model = _modelArray[indexPath.row];
        
        NSString *lastTimeStr = [ZRBUtilities stringToData:@"yyyy/MM/dd" interval:lastModel.pushTime];
        NSString *timeStr = [ZRBUtilities stringToData:@"yyyy/MM/dd" interval:model.pushTime];
        
        if ([timeStr isEqualToString:lastTimeStr]) {
            
            height = 200.0f;
        }else{
            
            height = 230.0f;
        }
        
    }

    return height;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FindHomeItemModel *model = [_modelArray objectAtIndex:indexPath.row];
    model.index = indexPath.row;
    
    FindHomeCell *cell = [FindHomeCell cellForTableView:tableView];
    cell.tag = indexPath.row;
    
    if (indexPath.row == 0) {
        
        model.isShowTimeFlag = YES;
        
    }else {
        FindHomeItemModel *lastModel = _modelArray[indexPath.row - 1];
        NSString *lastTimeStr = [ZRBUtilities stringToData:@"yyyy/MM/dd" interval:lastModel.pushTime];
        NSString *timeStr = [ZRBUtilities stringToData:@"yyyy/MM/dd" interval:model.pushTime];
        
        if ([timeStr isEqualToString:lastTimeStr]) {
            model.isShowTimeFlag = NO;
        }else {
            model.isShowTimeFlag = YES;
        }
    }
    
    cell.model = model;
    
    //头像点击
    cell.userAvatarBtn.tag = indexPath.row;
    cell.messageTag.tag = indexPath.row;
    cell.focusTag.tag = indexPath.row;
    cell.shareTag.tag = indexPath.row;
    cell.editView.tag = indexPath.row;

    [cell.userAvatarBtn addTarget:self action:@selector(userAvatarClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.messageTag addTarget:self action:@selector(messageButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.focusTag addTarget:self action:@selector(focusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.shareTag addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.editView addTarget:self action:@selector(editClicked:) forControlEvents:UIControlEventTouchUpInside];

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    FindHomeItemModel *model = [_modelArray objectAtIndex:indexPath.row];
    
    //未认证意向不跳转
    if ([model.auth intValue] == OnSideApprove || [model.auth intValue] == OnDataApprove) {
        //意向未绑定，跳转到意向详情页，绑定项目跳转到项目详情页，绑定资金跳转到资金详情页
        if([ZRBUtilities isBlankString:model.projectId]){
            
            FindDetailViewController *detailCtrl = [[FindDetailViewController alloc]init];
            detailCtrl.hidesBottomBarWhenPushed = YES;
            detailCtrl.model = model;
            detailCtrl.index = (int)indexPath.row;
            
            [self.navigationController pushViewController:detailCtrl animated:YES];
        }else{
            LogInfo(@"意向已绑定");
            if (model.type == IntentWithProject) {
                
                SearchProjectDetailViewController *projectDetailCtrl = [[SearchProjectDetailViewController alloc]init];
                projectDetailCtrl.projectId = model.projectId;
                projectDetailCtrl.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:projectDetailCtrl animated:YES];
                
            }else if(model.type == IntentWithMoney){
                
                MoneyDetailViewController *moneyDetailCtrl = [[MoneyDetailViewController alloc]init];
                moneyDetailCtrl.hidesBottomBarWhenPushed = YES;
                moneyDetailCtrl.moneyId = model.projectId;
                [self.navigationController pushViewController:moneyDetailCtrl animated:YES];
            }
            
        }
    }else {
        
        
    }
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (_mTableView.visibleCells.count > 0) {
        
        if(scrollView.contentOffset.y > 80){
            FindHomeCell *cell =  [_mTableView.visibleCells objectAtIndex:0];
            
            FindHomeItemModel *model = [_modelArray objectAtIndex:cell.tag];
            self.navigationItem.title = [ZRBUtilities stringToData:@"yyyy-MM-dd" interval:model.pushTime];
        }else{
            
            self.navigationItem.title = @"发现";
        }
    }
    
}


//点击头像
- (void)userAvatarClicked:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    FindHomeItemModel *model = _modelArray[button.tag];
    //跳转个人名片
    if([ZRB_UserManager isLogin]){
        PersonalCardViewController *personalCardViewController = [[PersonalCardViewController alloc]init];
        personalCardViewController.userId = model.userId;
        personalCardViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:personalCardViewController animated:YES];
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.hidesBottomBarWhenPushed = YES;
        loginCtrl.isFindHomeRefresh = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
}

//关注
- (void)focusButtonClicked:(id)sender{
    
    if([ZRB_UserManager isLogin]){
        
        UIButton *button= (UIButton *)sender;
        FindHomeItemModel *model = _modelArray[button.tag];
        
        if([model.attention isEqualToString:@"1"]){
            LogInfo(@"取消关注");
            [self cancelFcousIntent:model];
        }else if([model.attention isEqualToString:@"0"]){
            LogInfo(@"关注");
            [self fcousIntent:model];
        }
        
       
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.hidesBottomBarWhenPushed = YES;
        loginCtrl.isFindHomeRefresh = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
}

//留言
- (void)messageButtonClicked:(id)sender{
    
    if ([ZRB_UserManager isLogin]) {
        UIButton *button = (UIButton *)sender;
        FindHomeItemModel *model = _modelArray[button.tag];
        
        MessageViewController *messageCtrl = [[MessageViewController alloc]init];
        messageCtrl.hidesBottomBarWhenPushed = YES;
        messageCtrl.intentId = model.uid;
        messageCtrl.index = (int)button.tag;
        
        [self.navigationController pushViewController:messageCtrl animated:YES];
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
}

//分享
- (void)shareButtonClicked:(id)sender{
    
    NSInteger i = ((UIButton*)sender).tag;
   
    _shareContent = [[NSMutableString alloc]initWithString:@"向您推荐 "];

    FindHomeItemModel *model = [_modelArray objectAtIndex:i];
    if(model.intro)
        [_shareContent appendString:model.intro];
   
    //设置分享到QQ空间的应用Id，和分享url 链接
     _shareUrl = [NSString stringWithFormat:@"%@/intention/details?id=%ld",SERVER_URL,(long)i];
    LogInfo(_shareUrl);
    [_shareMenu show];
    
}

//编辑
- (void)editClicked:(id)sender{
    
    UIButton *button = (id)sender;
    _modifyIndex = (int)button.tag;
    
    FindHomeItemModel *model = [_modelArray objectAtIndex:button.tag];
    
    [self requestFindDetail:model.uid];
    
    
//    PushViewController *editCtrl = [[PushViewController alloc]init];
//    editCtrl.isEdit = YES;
//    editCtrl.dataModel = model;
//    editCtrl.hidesBottomBarWhenPushed = YES;
//    
//    if([model.typeName isEqualToString:@"资金方"]){
//        
//        editCtrl.navTitle = @"编辑资金";
//        editCtrl.intentType = 1;
//        
//    }else if([model.typeName isEqualToString:@"项目方"]){
//        
//        editCtrl.navTitle = @"编辑项目";
//        editCtrl.intentType = 0;
//    }
//    
//    [self.navigationController pushViewController:editCtrl animated:YES];
    
}

/**
 *  请求发现列表
 */
-(void)queryListData{
    
    _isRequst = NO;

    FindHomeRequest *request = [[FindHomeRequest alloc]init];
    request.maxId = @"";
    request.limit = @"10";
    
    WS(bself);
    [_findService findHomeWithRequest:request success:^(id responseObject) {
        
        [HUDManager removeHUDFromView:self.view];
        
        FindHomeModel *model= responseObject;
        [bself findQueryListCallBackWithObject:model];
        
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
    }];
}

/**
 *  加载更多
 */
-(void)loadMoreData{
    
    FindHomeItemModel *itemModel = [_modelArray lastObject];
    FindHomeRequest *request = [[FindHomeRequest alloc]init];
    request.maxId = itemModel.uid;
    request.limit = @"10";

    WS(bself);
    [_findService findHomeWithRequest:request success:^(id responseObject) {
        
        FindHomeModel *model = responseObject;
        [bself loadMoreListCallBackWithObject:model];
        
    } failure:^(NSError *error) {
        
    }];
}


/**
 * 下拉刷新请求回调
 */
- (void)findQueryListCallBackWithObject:(FindHomeModel *)model
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            _modelArray = model.data;

            if(_modelArray.count < 10){
                
                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                _mTableView.footer.hidden = YES;
            }else{
                _mTableView.footer.hidden = NO;
            }
            
            [_mTableView reloadData];
            
            break;
        }
        case ZRBHttpFailType:
        {
            LogInfo(@"请求失败");
            [_mTableView.header endRefreshing];
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
        default:
            break;
    }
    
    [_mTableView.header endRefreshing];
    
}

/**
 *  加载更多请求回调
 */
- (void)loadMoreListCallBackWithObject:(FindHomeModel *)model
{
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            LogInfo(@"加载更多回调");
            [_modelArray addObjectsFromArray:model.data];
            [_mTableView reloadData];
            
            if (model.data.count < 10) {
                LogInfo(@"已经加载完毕");
                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                _mTableView.footer.hidden = YES;
            }else{
                _mTableView.footer.hidden = NO;
            }
            
            [_mTableView.footer endRefreshing];
            break;
        }
        case ZRBHttpFailType:
        {
            [_mTableView.header endRefreshing];
            
            break;
        }
        case ZRBHttpNoLoginType:
        {
            [_mTableView.header endRefreshing];
            
            break;
        }
        default:
            break;
    }
}

#pragma mark 分享回调
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    LogInfo(@"didFinishGetUMSocialDataInViewController with response is %@",response);
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        LogInfo(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}


#pragma mark 网络请求

//意向列表
- (void)findList:(FindHomeRequest *)request{
    
    [_findService findHomeWithRequest:request success:^(id responseObject) {
        
        FindHomeModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                _modelArray = model.data;
                if (_modelArray.count < 10) {
                    
                }
                
                [_mTableView reloadData];
                
                break;
            }
            case ZRBHttpFailType:
                
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
        [_mTableView.header endRefreshing];
        
    } failure:^(NSError *error) {
         LogInfo(@"error：%@",error);
         [_mTableView.header endRefreshing];
    }];
    
}

/**
 *  意向关注
 */
- (void)fcousIntent:(FindHomeItemModel *)itemModel{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousIntent;
    request.fcousObjectId =  itemModel.uid;
    
    [SVProgressHUD show];
    AFBaseService *baseService = [[AFBaseService alloc]init];
    
    [baseService fcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"关注成功" onView:self.view];
                    
                    itemModel.attentionTimes = [NSString stringWithFormat:@"%d",[itemModel.attentionTimes intValue]+1];
                    itemModel.attention = @"1";
                    
                    [_mTableView reloadData];
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"已关注"];
                }
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"关注失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
    }];
}

/**
 *  取消关注
 */
- (void)cancelFcousIntent:(FindHomeItemModel *)itemModel{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousIntent;
    request.fcousObjectId =  itemModel.uid;
    
    [SVProgressHUD show];
    AFBaseService *baseService = [[AFBaseService alloc]init];
    
    [baseService cancelFcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"已取消关注" onView:self.view];
                    
                    itemModel.attentionTimes = [NSString stringWithFormat:@"%d",[itemModel.attentionTimes intValue]- 1];
                    itemModel.attention = @"0";
                    
                    [_mTableView reloadData];
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"取消关注失败"];
                }
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"取消关注失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
    }];

}

/**
 * 编辑意向请求
 */
- (void)requestFindDetail:(NSString*)fid{
    //封装参数
    FindDetailRequest *request = [[FindDetailRequest alloc]init];
    request.intentId = fid;
    
    [SVProgressHUD show];
    
    [_findService intentDetailDataWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FindHomeModel *model = (FindHomeModel *)responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.model) {
                    FindHomeItemModel *imodel = (FindHomeItemModel*)model.model;
                    //跳转编辑
                    PushViewController *editCtrl = [[PushViewController alloc]init];
                    editCtrl.hidesBottomBarWhenPushed = YES;
                    editCtrl.isEdit = YES;
                    editCtrl.dataModel = imodel;
                    
                    if([imodel.typeName isEqualToString:@"资金方"]){
                        
                        editCtrl.navTitle = @"编辑资金";
                        editCtrl.intentType = 1;
                        
                    }else if([imodel.typeName isEqualToString:@"项目方"]){
                        
                        editCtrl.navTitle = @"编辑项目";
                        editCtrl.intentType = 0;
                    }
                    
                    [self.navigationController pushViewController:editCtrl animated:YES];
                }else{
                    [self showTipViewWithMsg:@"无法操作，信息已失效，请刷新再试"];
                }
                break;
            case ZRBHttpFailType:
                if(model.msg)
                    [self showTipViewWithMsg:model.msg];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"请求意向信息失败，请稍后再试"];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
