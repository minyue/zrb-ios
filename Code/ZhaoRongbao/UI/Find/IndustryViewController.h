//
//  IndustryViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "FindService.h"

@interface IndustryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    FindService *_findService;
    UITableView *_mTableView;
}

@property (nonatomic,strong) NSMutableDictionary *selectedIndustryNameDic;      //已选的行业名称
@property (nonatomic,strong) NSMutableDictionary *selectedIndustryIdDic;        //已选的行业ID

@property (nonatomic,strong) NSMutableArray *industryItemModelArray;
@property (nonatomic,strong) NSMutableArray *statusArray;


@end
