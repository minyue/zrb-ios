//
//  IndustryViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "IndustryViewController.h"
#import "FindIndustryCell.h"
#import "FindIndustryModel.h"
#import "UINavigationItem+CustomItem.h"


#define CHECK_KEY       @"check"

@interface IndustryViewController ()

@end

@implementation IndustryViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setup];
    [self getAllIndustry];
}

- (void)setup{
    
    self.title = @"行业";
    
    _findService = [[FindService alloc]init];
    
    self.industryItemModelArray = [[NSMutableArray alloc]init];
    self.statusArray = [[NSMutableArray alloc]init];
    
    NSString *title = @"返回";
    CustomBarItem *backItem = [self.navigationItem setItemWithTitle:title   textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:left];
    [backItem addTarget:self selector:@selector(back:) event:UIControlEventTouchUpInside];
    [backItem setOffset:2];
    
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
}

- (void)back:(id)sender{
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:self.selectedIndustryIdDic,self.selectedIndustryNameDic, nil];
    LogInfo(@"已选行业字典名称和ID：%@",array);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectIndustry" object:array];

    [self.navigationController popViewControllerAnimated:YES];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _industryItemModelArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0f;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *indentifierStr = @"DiscoveryCell";
    FindIndustryCell *cell = (FindIndustryCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[FindIndustryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifierStr];
    }
    
    FindIndustryItemModel *model = [_industryItemModelArray objectAtIndex:indexPath.row];
    [cell initWithModel:model];

  
    NSMutableDictionary *dic = [_statusArray objectAtIndex:indexPath.row];
    
    if ([[dic objectForKey:CHECK_KEY] isEqualToString:@"NO"]) {
        [dic setObject:@"NO" forKey:CHECK_KEY];
        [cell setChecked:NO];
 
    }else {
        [dic setObject:@"YES" forKey:CHECK_KEY];
        [cell setChecked:YES];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    FindIndustryCell *cell = (FindIndustryCell*)[tableView cellForRowAtIndexPath:indexPath];
    FindIndustryItemModel *itemModel = _industryItemModelArray[indexPath.row];
    
    
    NSString *key = [NSString stringWithFormat:@"%ld",indexPath.row];
    NSUInteger row = [indexPath row];
    NSMutableDictionary *dic = [_statusArray objectAtIndex:row];
    if ([[dic objectForKey:CHECK_KEY] isEqualToString:@"NO"]) {
        
        [dic setObject:@"YES" forKey:CHECK_KEY];
        [cell setChecked:YES];
        
        [self.selectedIndustryNameDic setObject:itemModel.industryName forKey:key];
        [self.selectedIndustryIdDic setObject:itemModel.uid forKey:key];
        
    }else {
        
        [dic setObject:@"NO" forKey:CHECK_KEY];
        [cell setChecked:NO];
        
        [self.selectedIndustryNameDic removeObjectForKey:key];
        [self.selectedIndustryIdDic removeObjectForKey:key];
    }
}

#pragma mark 网络请求

//获取所有行业
- (void)getAllIndustry{
    
    //请求指示器
    [HUDManager showLoadNetWorkHUDInView:self.view];
    //开始请求
    IndustryRequest *request = [[IndustryRequest alloc]init];
    request.type = @"101";
    
    [_findService industryWithRequest:request success:^(id responseObject) {
        
        [HUDManager removeHUDFromView:self.view];
        
        FindIndustryModel *model = responseObject;
        
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                _industryItemModelArray = model.data;
                
                for (int i = 0; i < _industryItemModelArray.count; i++) {
                    
                     NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                    FindIndustryItemModel *itemModel = _industryItemModelArray[i];
                    
                    if ([[self.selectedIndustryIdDic allValues] containsObject:itemModel.uid]) {
                         [dic setValue:@"YES" forKey:CHECK_KEY];
                        LogInfo(@"已选");
                    }else{
                        [dic setValue:@"NO" forKey:CHECK_KEY];
                    }
                    
                    [_statusArray addObject:dic];
                }
                
                [_mTableView reloadData];
                break;
            }
            case ZRBHttpFailType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        LogInfo(@"error:%@",error);
        [HUDManager removeHUDFromView:self.view];
         [HUDManager showNonNetWorkHUDInView:self.view event:^{
             
         }];
        
    }];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
