//
//  MessageViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "FindService.h"

@interface MessageViewController : ZRB_ViewControllerWithBackButton<UITextViewDelegate>
{
    UITextView *_mTextView;
    FindService *_findService;
    
    UILabel *_statusLabel;
}

@property (nonatomic,assign) int index;
@property (nonatomic,strong) NSString *intentId;


@end
