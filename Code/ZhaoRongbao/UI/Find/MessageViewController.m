//
//  MessageViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MessageViewController.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "FindMessageModel.h"

#define MAX_COUNT           200

@interface MessageViewController ()

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setup];
}


- (void)setup{
    
    self.title = @"留言";
    _findService = [[FindService alloc]init];
    
    CustomBarItem * _rightItem =  [self.navigationItem setItemWithTitle:@"发布" textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_rightItem setOffset:-2];
    [_rightItem addTarget:self selector:@selector(commiteMessage:) event:UIControlEventTouchUpInside];
    
    _mTextView = [[UITextView alloc]init];
    _mTextView.backgroundColor = [UIColor whiteColor];
    _mTextView.textColor = [UIColor colorWithHexString:@"8c8c8c"];
    _mTextView.font = [UIFont systemFontOfSize:15.0f];
    _mTextView.delegate = self;
    [self.view addSubview:_mTextView];
    
    
    //字数限制显示
    _statusLabel = [[UILabel alloc]init];
    _statusLabel.text = [NSString stringWithFormat:@"0/%d",MAX_COUNT];
    _statusLabel.backgroundColor = [UIColor clearColor];
    _statusLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    _statusLabel.textColor = [UIColor colorWithRed:144.0/255.0 green:144.0/255.0 blue:144.0/255.0 alpha:1.0];
    [_statusLabel sizeToFit];
    [self.view addSubview:_statusLabel];
    
    
    [_mTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@180);
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_mTextView.mas_right).offset(-5);
        make.bottom.equalTo(_mTextView.mas_bottom).offset(-5);
    }];
    
}

- (void)commiteMessage:(id)sender{
    
    if([ZRBUtilities isBlankString:_mTextView.text]){
        
        [MBProgressHUD showHUDTitle:@"内容不能为空" onView:self.view];
        return ;
    }
    
    [self commiteFindMessage:_mTextView.text];
}


#pragma delegate of UITextView

- (void)textViewDidChange:(UITextView *)textView {
    
    NSInteger number = [textView.text length];
    if (number > MAX_COUNT) {
        
        NSString *alertStr = [NSString stringWithFormat:@"字符个数不能超过%d",MAX_COUNT];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:alertStr delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        textView.text = [textView.text substringToIndex:MAX_COUNT];
        number = MAX_COUNT;
    }
    _statusLabel.text = [NSString stringWithFormat:@"%ld/%d",(long)number,MAX_COUNT];
}


#pragma mark 网络请求

- (void)commiteFindMessage:(NSString *)content{
    
    //封装参数
    MessageRequest *request = [[MessageRequest alloc]init];
    request.content = content;
    request.intentionId = self.intentId;
    
    [SVProgressHUD show];
    
    [_findService messageWithRequest:request success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        FindMessageModel *model = responseObject;
        
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:LIST_MESSAGE_ADD object:[NSString stringWithFormat:@"%d",self.index]];
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
            case ZRBHttpFailType:
                
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"提交失败"];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
