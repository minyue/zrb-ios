//
//  MoreRecommendInfoCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/12.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreRecommendInfoCell : UITableViewCell

@property (nonatomic,copy) UILabel *infoLB;
@property (nonatomic,copy) UIButton *moreBtn;

+ (instancetype)cellForTableView:(UITableView *)tableView;

@end
