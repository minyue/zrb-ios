//
//  MoreRecommendInfoCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/12.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import "MoreRecommendInfoCell.h"

@implementation MoreRecommendInfoCell

+ (instancetype)cellForTableView:(UITableView *)tableView{
    
    static NSString *ID = @"MoreInfoCell";
    MoreRecommendInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[MoreRecommendInfoCell alloc]init];
        [cell setup];
    }
    
    return  cell;
}

- (void)setup{
    
    UIView *view = [[UIView alloc]init];
    [self.contentView addSubview:view];
    
    _infoLB = [[UILabel alloc]init];
    _infoLB.textColor = [UIColor colorWithHexString:@"68686d"];
    _infoLB.font = [UIFont systemFontOfSize:12.0f];
    _infoLB.text = @"没有满意的？";
    [view addSubview:_infoLB];
    
    _moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _moreBtn.backgroundColor = [UIColor clearColor];
    [_moreBtn setTitle:@"点击查看更多相关信息>" forState:UIControlStateNormal];
    [_moreBtn setTitleColor:[UIColor colorWithHexString:@"007de3"] forState:UIControlStateNormal];
    _moreBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [view addSubview:_moreBtn];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.centerX.equalTo(self.contentView.mas_centerX);
    }];
    
    
    [_infoLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left);
        make.top.bottom.equalTo(view);
    }];
    
    [_moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_infoLB.mas_right);
        make.right.equalTo(view.mas_right);
        make.top.bottom.equalTo(view);
    }];
}

@end
