//
//  PersonalCardInfoCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/10.
//  Copyright © 2015年 songmk. All rights reserved.
//  个人名片cell

#import <UIKit/UIKit.h>
#import "FindMenuTagView.h"
#import "FindHomeModel.h"

@interface PersonalCardInfoCell : UITableViewCell

@property (nonatomic,copy) UILabel *titleLB;              //标题
@property (nonatomic,copy) UIImageView *approveIcon;      //认证类型标识

@property (nonatomic,copy) UIView *contentBG;             //内容背景
@property (nonatomic,copy) UILabel *industryLB;           //投资行业
@property (nonatomic,copy) UILabel *areaLB;               //投资区域
@property (nonatomic,copy) UILabel *sumLB;                //投资金额

@property (nonatomic,copy) FindMenuTagView *fcousBtn;      //关注
@property (nonatomic,copy) FindMenuTagView *messageBtn;    //留言
@property (nonatomic,copy) FindMenuTagView *shareBtn;      //分享

@property (nonatomic,retain) FindHomeItemModel *model;      //数据模型

+(instancetype)cellForTableView:(UITableView *)tableView;

@end
