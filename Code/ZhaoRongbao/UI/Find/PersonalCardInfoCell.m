//
//  PersonalCardInfoCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/10.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "PersonalCardInfoCell.h"


@implementation PersonalCardInfoCell

+(instancetype)cellForTableView:(UITableView *)tableView{
    
    static NSString *ID = @"PersonalCardInfoCell";
    PersonalCardInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[PersonalCardInfoCell alloc]init];
    }
    
    return cell;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    
    self.backgroundColor = [UIColor clearColor];
    UIView *superView = self.contentView;
    
    UIView *cardView = [[UIView alloc]init];
    cardView.backgroundColor = [UIColor whiteColor];
    cardView.layer.masksToBounds = YES;
    cardView.layer.cornerRadius = 3.0f;
    cardView.layer.borderColor = [UIColor colorWithHexString:@"e5e6eb"].CGColor;
    cardView.layer.borderWidth = 0.5f;
    [superView addSubview:cardView];
    
    //标题
    _titleLB = [[UILabel alloc]init];
    _titleLB.opaque = YES;
    _titleLB.numberOfLines = 1;
    _titleLB.font = [UIFont boldSystemFontOfSize:14.0f];
    [_titleLB sizeToFit];
    _titleLB.textColor = [UIColor colorWithHexString:@"5f5f5f"];
    [cardView addSubview:_titleLB];
    
    //认证类型标识
    _approveIcon = [[UIImageView alloc]init];
    [cardView addSubview:_approveIcon];
    
    //内容背景
    _contentBG = [[UIView alloc]init];
    _contentBG.layer.masksToBounds = YES;
    _contentBG.layer.cornerRadius = 3.0f;
    [cardView addSubview:_contentBG];
    
    //投资行业
    _industryLB = [[UILabel alloc]init];
    _industryLB.textColor = [UIColor colorWithHexString:@"68686d"];
    _industryLB.font = [UIFont systemFontOfSize:12.0f];
    [_industryLB sizeToFit];
    [_contentBG addSubview:_industryLB];
    
    //投资区域
    _areaLB = [[UILabel alloc]init];
    _areaLB.textColor = [UIColor colorWithHexString:@"68686d"];
    _areaLB.font = [UIFont systemFontOfSize:12.0f];
    [_areaLB sizeToFit];
    [_contentBG addSubview:_areaLB];
    
    //投资金额
    _sumLB = [[UILabel alloc]init];
    _sumLB.textColor = [UIColor colorWithHexString:@"68686d"];
    _sumLB.font = [UIFont systemFontOfSize:12.0f];
    [_sumLB sizeToFit];
    [_contentBG addSubview:_sumLB];
    
    
    //关注
    _fcousBtn = [[FindMenuTagView alloc]init];
    [cardView addSubview:_fcousBtn];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"e6e8ec"];
    [cardView addSubview:line];
    
    //留言
    _messageBtn = [[FindMenuTagView alloc]init];
    [cardView addSubview:_messageBtn];
    
    UIView *line1 = [[UIView alloc]init];
    line1.backgroundColor = [UIColor colorWithHexString:@"e6e8ec"];
    [cardView addSubview:line1];
    
    //分享
    _shareBtn = [[FindMenuTagView alloc]init];
    [cardView addSubview:_shareBtn];
    
    /* ----------------------------- 子视图约束 ----------------------------- */
    
    [cardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top).offset(6);
        make.left.equalTo(superView.mas_left).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.height.equalTo(@138);
        make.bottom.equalTo(superView.mas_bottom).offset(-6);
    }];
    
    [_approveIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@15);
        make.height.equalTo(@26);
        make.top.equalTo(cardView.mas_top);
        make.right.equalTo(cardView.mas_right).offset(-13.5);
    }];
    
    [_titleLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cardView.mas_top).offset(10);
        make.left.equalTo(cardView.mas_left).offset(10);
        make.right.equalTo(_approveIcon.mas_right).offset(-40);
        
    }];
    
    [_contentBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(cardView.mas_left).offset(10);
        make.right.equalTo(cardView.mas_right).offset(-10);
        make.top.equalTo(_titleLB.mas_bottom).offset(10);
        make.height.equalTo(@66);
    }];
    
    [_industryLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_contentBG.mas_top).offset(10);
        make.left.equalTo(_contentBG.mas_left).offset(10);
        make.right.equalTo(_contentBG.mas_right).offset(-10);
        make.bottom.equalTo(_areaLB.mas_top).offset(-7);
    }];
    
    [_areaLB mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_industryLB.mas_bottom).offset(7);
        make.centerY.equalTo(_contentBG.mas_centerY);
        make.left.equalTo(_contentBG.mas_left).offset(10);
        make.right.equalTo(_contentBG.mas_right).offset(-10);
    }];
    
    [_sumLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_areaLB.mas_bottom).offset(7);
        make.left.equalTo(_contentBG.mas_left).offset(10);
        make.right.equalTo(_contentBG.mas_right).offset(-10);
        make.bottom.equalTo(_contentBG.mas_bottom).offset(-10);
    }];
    
    [_fcousBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_contentBG.mas_bottom);
        make.height.equalTo(@37);
        make.bottom.equalTo(cardView.mas_bottom);
        make.left.equalTo(cardView.mas_left);
        make.width.equalTo(@((SCREEN_WIDTH-20) * 1/3));
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@0.5);
        make.left.equalTo(_fcousBtn.mas_right);
        make.top.equalTo(_fcousBtn.mas_top).offset(5);
        make.bottom.equalTo(_fcousBtn.mas_bottom).offset(-5);
    }];
    
    [_messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_contentBG.mas_bottom);
//        make.height.equalTo(@37);
//        make.bottom.equalTo(cardView.mas_bottom);
        make.centerY.equalTo(_fcousBtn.mas_centerY);
        make.left.equalTo(_fcousBtn.mas_right);
        make.width.equalTo(@((SCREEN_WIDTH-20) * 1/3));
    }];
    
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@0.5);
        make.left.equalTo(_messageBtn.mas_right);
        make.top.equalTo(_fcousBtn.mas_top).offset(5);
        make.bottom.equalTo(_fcousBtn.mas_bottom).offset(-5);
    }];

    [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_contentBG.mas_bottom);
//        make.height.equalTo(@37);
//        make.bottom.equalTo(cardView.mas_bottom);
        make.centerY.equalTo(_fcousBtn.mas_centerY);
        make.left.equalTo(line1.mas_right);
        make.right.equalTo(cardView.mas_right);
    }];
    
}

- (void)setModel:(FindHomeItemModel *)model{
    
    if (model) {
        _titleLB.text = model.intro;
        
        //认证标签
        if ([model.auth intValue] == OnSideApprove) {
            _approveIcon.hidden = NO;
            _approveIcon.image = [UIImage imageNamed:@"list_sdrz"];
            
        }else if([model.auth intValue] == OnDataApprove){
            _approveIcon.hidden = NO;
            _approveIcon.image = [UIImage imageNamed:@"list_zlrz"];
        }else{
            _approveIcon.hidden = YES;
        }

        if([model.typeName isEqualToString:@"项目方"]){
            _contentBG.backgroundColor = [UIColor colorWithHexString:@"fef8f8"];
        }else if([model.typeName isEqualToString:@"资金方"]){
            _contentBG.backgroundColor = [UIColor colorWithHexString:@"f1f5fb"];
        }
        
        _industryLB.text = [NSString stringWithFormat:@"所属行业：%@",model.industryName];
        _areaLB.text = [NSString stringWithFormat:@"投资区域：%@",model.districtName];
        _sumLB.text = [NSString stringWithFormat:@"投资金额：%@万元整",model.amount];
        
        if ([model.attention isEqualToString:@"0"]) {
            [_fcousBtn initHeadTagView:@"like" title:[NSString stringWithFormat:@"%@",model.attentionTimes] titleColor:@"a6a6a6"];
        }else if([model.attention isEqualToString:@"1"]){
            [_fcousBtn initHeadTagView:@"like_hl" title:[NSString stringWithFormat:@"%@",model.attentionTimes] titleColor:@"a6a6a6"];
        }
        
        
        [_messageBtn initHeadTagView:@"comment" title:[NSString stringWithFormat:@"%@",model.messageAmount] titleColor:@"a6a6a6"];
        [_shareBtn initHeadTagView:@"share" title:@"" titleColor:@"a6a6a6"];
    }
}

@end
