//
//  PersonalCardViewController.h
//  ZhaoRongbao
//
//  Created by zouli on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//  个人名片

#import <UIKit/UIKit.h>
#import "ZRB_ViewController.h"
#import "PersonalIntentCountButton.h"

#import "FindService.h"
#import "FindPersonalModel.h"

@interface PersonalCardViewController : ZRB_ViewControllerWithBackButton<UIScrollViewDelegate>
{
    UIScrollView *_mScrollerView;
    FindService  *_service;
    
    FindPersonalUserInfoModel *_currentTopPersonalData;
}


@property (nonatomic, strong) NSString *userId;

@property (nonatomic,copy) UIImageView *userIcon;                   //会员头像
@property (nonatomic,copy) UILabel *realName;                       //真名
@property (nonatomic,copy) UILabel *industryAndCompany;             //行业、公司、职能

@property (nonatomic,copy) PersonalIntentCountButton *pushCountBtn;         //发布数量
@property (nonatomic,copy) PersonalIntentCountButton *projectCountBtn;      //关联项目数量
@property (nonatomic,copy) PersonalIntentCountButton *moneyCountBtn;        //关联资金数量

@property (nonatomic,copy) UIView *scrollLine;                              //选中横线

@end
