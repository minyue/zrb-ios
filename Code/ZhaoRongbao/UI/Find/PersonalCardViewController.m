//
//  PersonalCardViewController.m
//  ZhaoRongbao
//
//  Created by zouli on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "PersonalCardViewController.h"

#import "PushCountViewController.h"
#import "ProjectCountViewController.h"
#import "MoneyCountViewController.h"


@interface PersonalCardViewController()


@end

@implementation PersonalCardViewController


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup
{
    _service = [[FindService alloc] init];
    [self getUserData];

    [self initTopView];
    [self setupScrollerView];
}

/**
 *  初始化顶部视图
 */
- (void)initTopView{
    
    //顶部视图背景
    UIImageView *topView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"person_top_bg"]];
    topView.userInteractionEnabled = YES;
    [self.view addSubview:topView];
    
    //底部内容滑动容器
    _mScrollerView = [[UIScrollView alloc]init];
    [self.view addSubview:_mScrollerView];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(click_popViewController:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:backBtn];
    
    //头像背景
    UIView *userIconBG = [[UIView alloc]init];
    userIconBG.backgroundColor = [UIColor clearColor];
    userIconBG.layer.cornerRadius = 23;
    userIconBG.layer.masksToBounds = YES;
    userIconBG.layer.borderColor = [UIColor colorWithHexString:@"ffffff"].CGColor;
    userIconBG.layer.borderWidth = 1.0f;
    userIconBG.alpha = 0.4;
    [topView addSubview:userIconBG];
    //头像
    _userIcon = [[UIImageView alloc]init];
    _userIcon.layer.masksToBounds = YES;
    _userIcon.layer.cornerRadius = 21;
    _userIcon.layer.borderColor = [UIColor colorWithHexString:@"ffffff"].CGColor;
    _userIcon.layer.borderWidth = 1.0f;
    [userIconBG addSubview:_userIcon];
    
    //职位
    _realName = [[UILabel alloc]init];
    _realName.textColor = [UIColor colorWithHexString:@"ffffff"];
    _realName.font = [UIFont systemFontOfSize:14.0f];
    [topView addSubview:_realName];
    
    //行业、公司、职能
    _industryAndCompany = [[UILabel alloc]init];
    _industryAndCompany.font = [UIFont systemFontOfSize:12.0f];
    _industryAndCompany.textColor = [UIColor colorWithHexString:@"ffffff"];
    [topView addSubview:_industryAndCompany];
    
    //发布数量
    _pushCountBtn = [[PersonalIntentCountButton alloc]init];
    _pushCountBtn.selected = YES;
    _pushCountBtn.tag = 0;
    [_pushCountBtn addTarget:self action:@selector(changeViewWithIndex:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:_pushCountBtn];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"38a6ff"];
    [topView addSubview:line];
    
    //关联项目数量
    _projectCountBtn = [[PersonalIntentCountButton alloc]init];
    _projectCountBtn.selected = NO;
    _projectCountBtn.tag = 1;
    [_projectCountBtn addTarget:self action:@selector(changeViewWithIndex:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:_projectCountBtn];
    
    //底部选中横线
    _scrollLine = [[UIView alloc]init];
    _scrollLine.backgroundColor = [UIColor colorWithHexString:@"7ddffc"];
    [topView addSubview:_scrollLine];
    
    UIView *line1 = [[UIView alloc]init];
    line1.backgroundColor = [UIColor colorWithHexString:@"38a6ff"];
    [topView addSubview:line1];
    
    //关联资金数量
    _moneyCountBtn = [[PersonalIntentCountButton alloc]init];
    _moneyCountBtn.selected = NO;
    _moneyCountBtn.tag = 2;
    [_moneyCountBtn addTarget:self action:@selector(changeViewWithIndex:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:_moneyCountBtn];
    
    
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@174);
    }];

        
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@10);
        make.width.equalTo(@25);
        make.left.equalTo(topView.mas_left).offset(10);
        make.top.equalTo(topView.mas_top).offset(32);
    }];
    
    [_mScrollerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom);
        make.bottom.equalTo(self.view.mas_bottom);
        make.left.right.equalTo(self.view);
    }];
    
    [userIconBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@46);
        make.left.equalTo(topView.mas_left).offset(40);
        make.centerY.equalTo(topView.mas_centerY);
    }];
    
    [_userIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@42);
        make.center.equalTo(userIconBG);
    }];
    
    [_realName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userIconBG.mas_right).offset(10);
        make.bottom.equalTo(userIconBG.mas_centerY).offset(-5);
    }];
    
    [_industryAndCompany mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userIconBG.mas_right).offset(10);
        make.top.equalTo(userIconBG.mas_centerY).offset(5);
    }];
    
    [_pushCountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@32);
        make.width.equalTo(@(SCREEN_WIDTH*1/3));
        make.bottom.equalTo(topView.mas_bottom).offset(-1);
        make.left.equalTo(topView.mas_left);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@30);
        make.width.equalTo(@1);
        make.left.equalTo(_pushCountBtn.mas_right);
        make.centerY.equalTo(_pushCountBtn.mas_centerY);
    }];
    
    [_projectCountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@32);
        make.width.equalTo(@(SCREEN_WIDTH*1/3));
        make.bottom.equalTo(topView.mas_bottom).offset(-1);
        make.left.equalTo(topView.mas_left).offset(SCREEN_WIDTH*1/3);
    }];
    
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@30);
        make.width.equalTo(@1);
        make.left.equalTo(_projectCountBtn.mas_right);
        make.centerY.equalTo(_pushCountBtn.mas_centerY);
    }];
    
    [_moneyCountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@32);
        make.width.equalTo(@(SCREEN_WIDTH*1/3));
        make.bottom.equalTo(topView.mas_bottom).offset(-1);
        make.left.equalTo(topView.mas_left).offset(SCREEN_WIDTH*2/3);
    }];
    
    [_scrollLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(SCREEN_WIDTH *1/3));
        make.height.equalTo(@2);
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(topView.mas_bottom);
    }];

}

     

/**
 *  初始化底部内容滑动视图 _mScrollerView
 */
- (void)setupScrollerView{
    
    _mScrollerView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    _mScrollerView.delegate = self;
    _mScrollerView.showsHorizontalScrollIndicator = NO;
    _mScrollerView.showsVerticalScrollIndicator = NO;
    _mScrollerView.scrollEnabled = NO;
    
    
    PushCountViewController *pushCountCtrl = [[PushCountViewController alloc]init];
    pushCountCtrl.userId = self.userId;
    [self addChildViewController:pushCountCtrl];
     pushCountCtrl.view.frameX = SCREEN_WIDTH * 0;
     pushCountCtrl.view.frameY = _mScrollerView.frameY;
     pushCountCtrl.view.frameWidth = SCREEN_WIDTH;
     pushCountCtrl.view.frameHeight = _mScrollerView.frameSize.height;
    
    [_mScrollerView addSubview:pushCountCtrl.view];
    _mScrollerView.contentSize = CGSizeMake(SCREEN_WIDTH*1, _mScrollerView.contentSize.height);
    
    ProjectCountViewController *projectCountCtrl = [[ProjectCountViewController alloc]init];
    projectCountCtrl.userId = self.userId;
    [self addChildViewController:projectCountCtrl];
    projectCountCtrl.view.frameX = SCREEN_WIDTH * 1;
    projectCountCtrl.view.frameY = _mScrollerView.frameY;
    projectCountCtrl.view.frameWidth = SCREEN_WIDTH;
    projectCountCtrl.view.frameHeight = _mScrollerView.frameSize.height;
    [_mScrollerView addSubview:projectCountCtrl.view];
    _mScrollerView.contentSize = CGSizeMake(SCREEN_WIDTH*2, _mScrollerView.contentSize.height);

    MoneyCountViewController *moneyCountCtrl = [[MoneyCountViewController alloc]init];
    [self addChildViewController:moneyCountCtrl];
    moneyCountCtrl.view.frameX = SCREEN_WIDTH*2;
    moneyCountCtrl.view.frameY = _mScrollerView.frameY;
    moneyCountCtrl.view.frameWidth = SCREEN_WIDTH;
    moneyCountCtrl.view.frameHeight = _mScrollerView.frameSize.height;
    [_mScrollerView addSubview:moneyCountCtrl.view];
    _mScrollerView.contentSize = CGSizeMake(SCREEN_WIDTH * 3, _mScrollerView.contentSize.height);
    
}



-(void)changeViewWithIndex:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    CGRect frame = button.frame;
    
    [UIView animateWithDuration:0.3 animations:^{
        _scrollLine.frame = CGRectMake(frame.origin.x, _scrollLine.frame.origin.y, _scrollLine.frame.size.width, _scrollLine.frame.size.height);
    }];
    
    [_mScrollerView setContentOffset:CGPointMake(SCREEN_WIDTH * button.tag, _mScrollerView.contentOffset.y) animated:YES];
    
    switch (button.tag) {
        case 0://发布
        {
            _pushCountBtn.selected = YES;
            _projectCountBtn.selected = NO;
            _moneyCountBtn.selected = NO;
            break;
        }
        case 1://项目
        {
            _pushCountBtn.selected = NO;
            _projectCountBtn.selected = YES;
            _moneyCountBtn.selected = NO;
            break;
        }
        case 2://资金
        {
            _pushCountBtn.selected = NO;
            _projectCountBtn.selected = NO;
            _moneyCountBtn.selected = YES;
            break;
        }
        default:
            break;
    }
}

//用户信息 数据适配
- (void)refreshUserInfo{
    if(!_currentTopPersonalData)
        return;
    
    FindPersonalUserInfoModel *model = (FindPersonalUserInfoModel*)_currentTopPersonalData;
    if(model.avatar){
        [_userIcon sd_setImageWithURL:[ZRBUtilities urlWithIconPath:model.avatar] placeholderImage:[ZRBUtilities ZRB_UserDefaultImage] options:SDWebImageRetryFailed];
    }else{
        [_userIcon setImage:[ZRBUtilities ZRB_UserDefaultImage]];
    }
   
    if(model.realName && ![model.realName isEqualToString:@""]){
        _realName.text = model.realName;
    }
    
    NSMutableString *mstr = [[NSMutableString alloc] init];
    if(model.industryName){
        [mstr appendString:[NSString stringWithFormat:@"%@·",model.industryName]];
    }
    if(model.org){
        [mstr appendString:[NSString stringWithFormat:@"%@",model.org]];
    }
    if(model.position){
        [mstr appendString:[NSString stringWithFormat:@"·%@",model.position]];
    }
    if(mstr){
        _industryAndCompany.text = mstr;
    }else{
        _industryAndCompany.text = @"";
    }
    
    [_pushCountBtn initWithName:@"发布" objectCount:model.intentionCount];
    [_projectCountBtn initWithName:@"项目" objectCount:model.projectCount];
    [_moneyCountBtn initWithName:@"资金" objectCount:model.fundCount];

}

#pragma mark - 网络请求

/**
 * 获取名片用户信息
 */
- (void)getUserData{
    [_service findPersonalUserInfoWithId:_userId success:^(id responseObject) {
        if(responseObject){
            _currentTopPersonalData = (FindPersonalUserInfoModel*)responseObject;
            [self refreshUserInfo];
        }
    } failure:^(NSError *error) {
        [self showTipViewWithMsg:@"请求获取用户名片信息失败，请稍后重试"];
    }];
}


- (void )click_popViewController:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
