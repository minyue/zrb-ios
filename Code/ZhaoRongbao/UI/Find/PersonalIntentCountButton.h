//
//  PersonalIntentCountButton.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/13.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalIntentCountButton : UIButton

@property (nonatomic,copy) UILabel *name;
@property (nonatomic,copy) UILabel *count;

- (void)initWithName:(NSString *)name objectCount:(NSString *)count;


@end
