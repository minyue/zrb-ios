//
//  PersonalIntentCountButton.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/13.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import "PersonalIntentCountButton.h"

@implementation PersonalIntentCountButton

- (instancetype)init{
    
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    
    _name = [[UILabel alloc]init];
    _name.font = [UIFont systemFontOfSize:14.0f];
    [self addSubview:_name];
    
    _count = [[UILabel alloc]init];
    _count.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:_count];
    
    
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.bottom.equalTo(self.mas_centerY);
    }];
    
    [_count mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_centerY);
    }];
}

- (void)initWithName:(NSString *)name objectCount:(NSString *)count{
    _name.text = name;
    _count.text = count;
}

- (void)setSelected:(BOOL)selected{
    
    if (selected) {
        _name.textColor = [UIColor colorWithHexString:@"ffffff"];
        _count.textColor = [UIColor colorWithHexString:@"ffffff"];
    }else{
        _name.textColor = [UIColor colorWithHexString:@"99ccff"];
        _count.textColor = [UIColor colorWithHexString:@"99ccff"];
    }
    
}

@end
