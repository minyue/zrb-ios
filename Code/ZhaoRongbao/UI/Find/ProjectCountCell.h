//
//  ProjectCountCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/16.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTLabel.h"
#import "SearchProjectHomeModel.h"

@interface ProjectCountCell : UITableViewCell

@property (nonatomic,copy)UIImageView *bigPic;              //左边大图
@property (nonatomic,copy)UIImageView *defaultImage;        //占位图
//@property (nonatomic,strong)IndustryTagView *typePic;             //行业类型图片
@property (nonatomic,copy)BTLabel *title;                   //项目标题
@property (nonatomic,copy)UIImageView *certificationPic;    //认证图标
@property (nonatomic,copy)UILabel *certificationLB;         //认证类型
@property (nonatomic,copy)UILabel *industry;                //投资行业
@property (nonatomic,copy)UILabel *areaLB;                  //面积
@property (nonatomic,copy)UIView *line;
@property (nonatomic,copy)UILabel *price;                   //估值
@property (nonatomic,copy)UILabel *location;                //地址
@property (nonatomic,copy)UILabel *date;                    //时间

@property (nonatomic,retain) SearchProjectHomeItemModel *model;

+(instancetype) cellForTableView:(UITableView *)tableView;

@end
