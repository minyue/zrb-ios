//
//  PushCountViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/13.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "FindService.h"
#import "ShareMenuView.h"
#import "ShareManager.h"

@interface ProjectCountViewController : ZRB_ViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    BOOL _isRequst;
    
    FindService    *_service;
}

@property (nonatomic, strong) NSString *userId;

@property (nonatomic,strong) NSMutableArray *listArray;


@property (nonatomic,strong) ShareMenuView *shareMenu;              //分享弹出菜单
@property (nonatomic,copy) NSMutableString *shareContent;           //分享内容
@property (nonatomic,copy) NSString *shareUrl;                      //分享链接

@end
