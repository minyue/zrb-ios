//
//  PushCountViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/13.
//  Copyright © 2015年 songmk. All rights reserved.
//
#import "MJRefresh.h"

#import "ProjectCountViewController.h"
#import "MessageViewController.h"
#import "MineLoginViewcController.h"

#import "PersonalCardInfoCell.h"

#import "FcousModel.h"
#import "FindHomeModel.h"

@interface ProjectCountViewController ()

@end

@implementation ProjectCountViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setup];

    [self refreshData];
}

- (void)setup{
    
    _service = [[FindService alloc]init];
    _listArray = [[NSMutableArray alloc]init];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 175) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    [_tableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.view addSubview:_tableView];
    
    [self initShareMenu];
    [self addFooterView];
    [self addHeaderView];
}


/**
 *  初始化分享菜单
 */
- (void)initShareMenu{
    
    _shareMenu = [[ShareMenuView alloc]init];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_shareMenu];
    
    WS(bself);
    _shareMenu.shareButtonClickBlock = ^(NSInteger index){
        LogInfo(@"%ld",index);
        
        switch (index) {
            case ShareToQQ:
                [[ShareManager ShareManager] QQShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToQQZone:
                [[ShareManager ShareManager] QQZoneShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToWechat:
                [[ShareManager ShareManager] WXChatShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToWechatFriend:
                [[ShareManager ShareManager] WXFriendShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToSinaWeibo:
                [bself.shareMenu hide];
                [[ShareManager ShareManager] SinaWeiboShareWithViewControll:bself shareContent:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToCopy:
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = bself.shareUrl;
                
                [bself.shareMenu hide];
                
                break;
            }
            default:
                break;
        }
    };
}


/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}


/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}

#pragma mark -
#pragma mark - 列表相关方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PersonalCardInfoCell *cell = [PersonalCardInfoCell cellForTableView:tableView];
    cell.model = _listArray[indexPath.row];
    
    cell.fcousBtn.tag = indexPath.row;
    cell.messageBtn.tag = indexPath.row;
    cell.shareBtn.tag = indexPath.row;
    
//    [cell.fcousBtn addTarget:self action:@selector(focusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.messageBtn addTarget:self action:@selector(messageButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.shareBtn addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark -
#pragma mark -网络请求

/**
 *  请求我关注的项目列表
 */
- (void)queryListData
{
    _isRequst = NO;
    
    LogInfo(@"请求我关注的项目列表");
    
    PersonalMoneyListRequest *request = [[PersonalMoneyListRequest alloc]init];
    request.userId = _userId;
    request.limit = @"10";
    request.maxId = @"";
    [self startWithReq:request];
    
}


/**
 *  加载更多
 */
- (void)loadMoreData
{
    if(_listArray.count > 0){
        
        FindHomeItemModel *itemModel = [_listArray lastObject];
        PersonalMoneyListRequest *request = [[PersonalMoneyListRequest alloc]init];
        request.userId = _userId;
        request.limit = @"10";
        request.maxId = itemModel.uid;
        
        WS(bself);
        [_service findPersonalMoneyListWithRequest:request success:^(id responseObject) {
            
            FindHomeModel *model = responseObject;
            [bself loadMoreListCallBackWithObject:model];
            
        } failure:^(NSError *error) {
            
        }];
    }else{
        [_tableView.header endRefreshing];
        
        [_tableView.footer endRefreshing];
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
}


/**
 *  下拉加载
 *
 *  @param req 请求体
 */
- (void)startWithReq:(PersonalMoneyListRequest *)req
{
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_service findPersonalMoneyListWithRequest:req success:^(id responseObject) {
        
        [HUDManager removeHUDFromView:self.view];
        
        FindHomeModel *model= responseObject;
        [bself findQueryListCallBackWithObject:model];
        
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
    }];
}

/**
 * 下拉刷新请求回调
 */
- (void)findQueryListCallBackWithObject:(FindHomeModel *)model
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            _listArray = model.data;
            
            if(_listArray.count < 10){
                
                //                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                _tableView.footer.hidden = YES;
            }else{
                _tableView.footer.hidden = NO;
            }
            
            [_tableView reloadData];
            break;
        }
        case ZRBHttpFailType:
        {
            LogInfo(@"请求失败");
            [_tableView.header endRefreshing];
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
        default:
            break;
    }
    
    [_tableView.header endRefreshing];
    
}


/**
 *  加载更多请求回调
 */
- (void)loadMoreListCallBackWithObject:(FindHomeModel *)model
{
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            LogInfo(@"加载更多回调");
            [_listArray addObjectsFromArray:model.data];
            [_tableView reloadData];
            
            if (model.data.count < 10) {
                LogInfo(@"已经加载完毕");
//                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                _tableView.footer.hidden = YES;
            }else{
                _tableView.footer.hidden = NO;
            }
            
            [_tableView.footer endRefreshing];
            break;
        }
        case ZRBHttpFailType:
        {
            [_tableView.header endRefreshing];
            
            break;
        }
        case ZRBHttpNoLoginType:
        {
            [_tableView.header endRefreshing];
            
            break;
        }
        default:
            break;
    }
}

#pragma mark - 私人请求
//关注
- (void)focusButtonClicked:(id)sender{
    
    if([ZRB_UserManager isLogin]){
        
        UIButton *button= (UIButton *)sender;
        FindHomeItemModel *model = _listArray[button.tag];
        
        if([model.attention isEqualToString:@"1"]){
            LogInfo(@"取消关注");
            [self cancelFcousIntent:model andIndex:button.tag];
        }else if([model.attention isEqualToString:@"0"]){
            LogInfo(@"关注");
            [self fcousIntent:model andIndex:button.tag];
        }
        
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.hidesBottomBarWhenPushed = YES;
        loginCtrl.isFindHomeRefresh = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
}

//留言
- (void)messageButtonClicked:(id)sender{
    
    if ([ZRB_UserManager isLogin]) {
        UIButton *button = (UIButton *)sender;
        FindHomeItemModel *model = _listArray[button.tag];
        
        MessageViewController *messageCtrl = [[MessageViewController alloc]init];
        messageCtrl.hidesBottomBarWhenPushed = YES;
        messageCtrl.intentId = model.uid;
        messageCtrl.index = (int)button.tag;
        
        [self.navigationController pushViewController:messageCtrl animated:YES];
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
}

//分享
- (void)shareButtonClicked:(id)sender{
    
    NSInteger i = ((UIButton*)sender).tag;
    
    _shareContent = [[NSMutableString alloc]initWithString:@"向您推荐 "];
    
    FindHomeItemModel *model = [_listArray objectAtIndex:i];
    if(model.intro)
        [_shareContent appendString:model.intro];
    
    //设置分享到QQ空间的应用Id，和分享url 链接
    _shareUrl = [NSString stringWithFormat:@"%@/intention/details?id=%ld",SERVER_URL,(long)i];
    LogInfo(_shareUrl);
    [_shareMenu show];
    
}

#pragma mark - 网络请求
#pragma mark 网络请求

/**
 *  意向关注
 */
- (void)fcousIntent:(FindHomeItemModel *)itemModel andIndex:(NSInteger)index{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousIntent;
    request.fcousObjectId =  itemModel.uid;
    
    [SVProgressHUD show];
    
    AFBaseService *baseService = [[AFBaseService alloc]init];
    [baseService fcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"关注成功" onView:self.view];
                    FindHomeItemModel *selectModel = _listArray[index];
                    if(selectModel){
                        selectModel.attention = @"1";
                        selectModel.attentionTimes = [NSString stringWithFormat:@"%d",[selectModel.attentionTimes intValue] + 1];
                    }
                    [_tableView reloadData];
                    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:NO];
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"已关注"];
                }
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"关注失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
    }];
}

/**
 *  取消关注
 */
- (void)cancelFcousIntent:(FindHomeItemModel *)itemModel andIndex:(NSInteger)index{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousIntent;
    request.fcousObjectId =  itemModel.uid;
    
    [SVProgressHUD show];
    
    AFBaseService *baseService = [[AFBaseService alloc]init];
    [baseService cancelFcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"已取消关注" onView:self.view];
                    FindHomeItemModel *selectModel = _listArray[index];
                    if(selectModel){
                        selectModel.attention = @"0";
                        selectModel.attentionTimes = [NSString stringWithFormat:@"%d",[selectModel.attentionTimes intValue] - 1];
                    }
                    [_tableView reloadData];
                    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:NO];
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"取消关注失败"];
                }
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"取消关注失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
    }];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
