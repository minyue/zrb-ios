//
//  ProjectCountViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/13.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "ProjectCountCell.h"
#import "FindService.h"
#import "FindHomeModel.h"
#import "MJRefresh.h"


@interface PushCountViewController : ZRB_ViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    BOOL _isRequst;
    
    FindService *_service;
}

@property (nonatomic,strong)    NSString *userId;
@property (nonatomic,copy)      NSMutableArray *intentArray;

@end
