//
//  ProjectCountViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/13.
//  Copyright © 2015年 songmk. All rights reserved.
//  个人中心，意向模块子视图
//
//  *未完成，等待产品更新

#import "PushCountViewController.h"


@interface PushCountViewController ()

@end

@implementation PushCountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
}


- (void)setup{
    
    _service = [[FindService alloc]init];
    _intentArray = [[NSMutableArray alloc]init];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 175) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    [_tableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.view addSubview:_tableView];
    
    [self addFooterView];
    [self addHeaderView];
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
//        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        [bself loadMoreData];
    }];
}

/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}

#pragma mark -
#pragma mark - delegate of UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _intentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProjectCountCell *cell = [ProjectCountCell cellForTableView:tableView];
    cell.model = _intentArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark -
#pragma mark - 网络请求

/**
 *  请求我关注的项目列表
 */
- (void)refreshDataList
{
    _isRequst = NO;
    
    PersonalIntentListRequest *request = [[PersonalIntentListRequest alloc]init];
    [_service findPersonalIntentWithRequest:request success:^(id responseObject) {
       
        FindHomeModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                
                break;
            }
            case ZRBHttpFailType:
            {
                break;
            }
                
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
