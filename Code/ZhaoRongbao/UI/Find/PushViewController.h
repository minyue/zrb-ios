//
//  PushViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "QRadioButton.h"
#import "FindService.h"
#import "FindHomeModel.h"

@interface PushViewController : ZRB_ViewControllerWithBackButton<UIScrollViewDelegate,QRadioButtonDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    FindService *_findService;
    
    CGPoint _mScrollViewPoint;
    UIScrollView *_mScrollView;
    UILabel *_statusLabel;
}

@property (nonatomic,strong) FindHomeItemModel *dataModel;
@property (nonatomic,assign) BOOL isEdit;

@property (nonatomic,strong) NSString *navTitle;
@property (nonatomic,assign) NSInteger intentType;

@property (nonatomic,strong) UITextField *introductionTF;                   //简介
@property (nonatomic,strong) UILabel *industryNameLB;                       //投资行业
@property (nonatomic,strong) UILabel *areaNameLB;                           //投资区域
@property (nonatomic,strong) UITextField *sumTF;                            //投资金额
@property (nonatomic,strong) UITextView *sumDetailTV;                       //资金详情

@property (nonatomic,strong) NSString *industryName;
@property (nonatomic,strong) NSString *industryId;

@property (nonatomic,strong) NSMutableDictionary *cityNameDic;          //多选地区名称
@property (nonatomic,strong) NSMutableDictionary *cityPostCodeDic;      //多选地区编码

@property (nonatomic,strong) NSString *cityName;                //单选城市名称
@property (nonatomic,strong) NSString *cityPostcode;            //单选城市编码

@property (nonatomic,strong) NSString *investType;

@property (nonatomic,copy) NSString *textViewPlaceHolder;
@end
