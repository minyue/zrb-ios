//
//  PushViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "PushViewController.h"

#import "AreaViewController.h"
#import "IndustryViewController.h"

#import "FindIndustryViewController.h"
#import "FindAreaViewController.h"
#import "RecommendViewController.h"

#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "PushModel.h"

#define LEFT_TEXT_SIZE      
//#define TEXTVIEW_PLACEHOLDER   @"请详描述您的资金来源、用途等"
#define MAX_COUNT           120

@interface PushViewController ()

@end

@implementation PushViewController

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //行业
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getIndustry:) name:@"SelectIndustry" object:nil];
    //城市多选
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCity:) name:@"SelectCity" object:nil];
    //城市单选
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSingleCity:) name:@"SelectSingleCity" object:nil];
    [self setup];
}

- (void)setup{
    
    if (self.intentType == 1) {
        _textViewPlaceHolder = @"描述资金详情";
    }else{
        _textViewPlaceHolder = @"描述项目详情";
    }
    
    self.title = self.navTitle;
    
    _findService = [[FindService alloc]init];

    self.cityPostCodeDic = [[NSMutableDictionary alloc]init];
    self.cityNameDic = [[NSMutableDictionary alloc]init];
    
    NSString *rightItemTitle = @"";
    if (_isEdit) {
        rightItemTitle = @"完成";
    }else{
        rightItemTitle = @"发布";
    }
    CustomBarItem * _rightItem =  [self.navigationItem setItemWithTitle:rightItemTitle textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_rightItem setOffset:-2];
    [_rightItem addTarget:self selector:@selector(sendPush:) event:UIControlEventTouchUpInside];
    
    
    _mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
    _mScrollView.delegate = self;
    _mScrollView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    _mScrollView.showsVerticalScrollIndicator = YES; //垂直方向的滚动指示
    _mScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;//滚动指示的风格
    _mScrollView.bounces = NO;
    
    [self.view addSubview:_mScrollView];
    
    [self initHeadView];
    [self initInvestmentType];
    [self initSumDetail];
}

//接收行业选择通知
- (void)getIndustry:(id)sender{
    
    NSNotification *notify = (NSNotification *)sender;
    NSMutableArray *resultArray = notify.object;
 
    self.industryId = resultArray[0];
    self.industryName = resultArray[1];
    self.industryNameLB.text = [NSString stringWithFormat:@"%@",resultArray[1]];
}

//接收城市选择通知（多选）
- (void)getCity:(id)sender{
    
    NSNotification *notify = (NSNotification *)sender;
    NSMutableArray *resultArray = notify.object;
    
    self.cityPostCodeDic = resultArray[0];
    self.cityNameDic = resultArray[1];
    
    //城市名称
    NSString *resultStr = @"";
    for (int i = 0; i < [self.cityNameDic allValues].count; i ++) {
        
        if (i == 0) {
            resultStr = [NSString stringWithFormat:@"%@",[[self.cityNameDic allValues] objectAtIndex:0]];
        }else{
            resultStr = [NSString stringWithFormat:@"%@、%@",resultStr,[[self.cityNameDic allValues] objectAtIndex:i]];
        }
    }
    
    self.areaNameLB.text = resultStr;
    
    
    //城市编码
    NSString *cityPostCodeResultStr = @"";
    for (int i = 0; i < [self.cityNameDic allValues].count; i ++) {
        
        if (i == 0) {
            cityPostCodeResultStr = [NSString stringWithFormat:@"%@",[[self.cityPostCodeDic allValues] objectAtIndex:0]];
        }else{
            cityPostCodeResultStr = [NSString stringWithFormat:@"%@,%@",resultStr,[[self.cityPostCodeDic allValues] objectAtIndex:i]];
        }
    }
    self.cityPostcode = cityPostCodeResultStr;
    
    
}

//城市单选
- (void)getSingleCity:(id)sender{
    
    NSNotification *notify = (NSNotification *)sender;
    NSMutableArray *resultArray = notify.object;
    
    self.cityPostcode = resultArray[0];
    self.cityName = resultArray[1];
    
    self.areaNameLB.text = resultArray[1];;
    
}

//发布
- (void)sendPush:(id)sender{
    
    //简介不能为空
    if ([ZRBUtilities isBlankString:self.introductionTF.text]) {
        [self showTipViewWithMsg:@"简介不能为空"];
        return;
    }
    
    if ([ZRBUtilities isBlankString:self.industryNameLB.text]) {
        [self showTipViewWithMsg:@"请选择行业"];
        return;
    }

    if ([ZRBUtilities isBlankString:self.areaNameLB.text]) {
        [self showTipViewWithMsg:@"请选择区域"];
        return;
    }
  
    if ([ZRBUtilities isBlankString:self.sumTF.text]) {
        [self showTipViewWithMsg:@"请输入金额"];
        return;
    }

    
    if(_investType.length < 2){
        [self showTipViewWithMsg:@"请选择投资方式"];
        return;
    }
    
    if(_isEdit){
        [self modifyIntent];
    }else{
        [self sendIntent];
    }
    
}

//选择投资行业
- (void)chooseIndustry:(id)sender{
   
    FindIndustryViewController *industryCtrl = [[FindIndustryViewController alloc]init];
    industryCtrl.selectedIndustryId = self.industryId;
    industryCtrl.selectedIndustryName = self.industryName;
    
    if (self.intentType == 1) {
        industryCtrl.isHasNolimit = NO;
        
    }else{
         industryCtrl.isHasNolimit = YES;
    }
    
    [self.navigationController pushViewController:industryCtrl animated:YES];
    
}

//选择投资区域
- (void)chooseArea:(id)sender{
    
    if (self.intentType == 1) {
        
        AreaViewController *areaCtrl = [[AreaViewController alloc]init];
        areaCtrl.selectedCityNameDic = self.cityNameDic;
        areaCtrl.selectedCityPostCodeDic = self.cityPostCodeDic;
        
        [self.navigationController pushViewController:areaCtrl animated:YES];
        
    }else{
        FindAreaViewController *singleAreaCtrl = [[FindAreaViewController alloc]init];
        singleAreaCtrl.selectedCityPostcode = self.cityPostcode;
        singleAreaCtrl.selectedCityName = self.cityName;
        
        [self.navigationController pushViewController:singleAreaCtrl animated:YES];
    }
   
}

/**
 *   投资简介、行业、区域、金额
 */
- (void)initHeadView{
    /* ------------------------- 投资简介 -------------------------  */
    UIView *introductionView = [[UIView alloc]init];
    introductionView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:introductionView];
   
    UILabel *introduction = [[UILabel alloc]init];
    if (self.intentType == 1) {
         introduction.text = @"资金简介";
    }else{
         introduction.text = @"项目简介";
    }
        
   
    introduction.font = [UIFont systemFontOfSize:16.0f];
    introduction.backgroundColor = [UIColor clearColor];
    introduction.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    [introductionView addSubview:introduction];
    
    _introductionTF = [[UITextField alloc]init];
    _introductionTF.placeholder = @"输入20字内的投资需求";
    _introductionTF.delegate = self;
    _introductionTF.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    _introductionTF.textAlignment = NSTextAlignmentRight;
    
    [introductionView addSubview:_introductionTF];
    
    UIView *introductionLine = [[UIView alloc]init];
    introductionLine.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [introductionView addSubview:introductionLine];
    
    /* ------------------------- 投资行业 -------------------------  */
    UIButton *industryView = [UIButton buttonWithType:UIButtonTypeCustom];
    industryView.backgroundColor = [UIColor whiteColor];
    [industryView addTarget:self action:@selector(chooseIndustry:) forControlEvents:UIControlEventTouchUpInside];
    [_mScrollView addSubview:industryView];
    
    UILabel *industry = [[UILabel alloc]init];
    if (self.intentType == 1) {
       industry.text = @"投资行业";
    }else{
       industry.text = @"所属行业";
    }
    
    industry.font = [UIFont systemFontOfSize:16.0f];
    industry.backgroundColor = [UIColor clearColor];
    industry.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    [industryView addSubview:industry];
    
    _industryNameLB = [[UILabel alloc]init];
    _industryNameLB.font = [UIFont systemFontOfSize:16.0f];
    _industryNameLB.backgroundColor = [UIColor clearColor];
    _industryNameLB.textAlignment = NSTextAlignmentRight;
    _industryNameLB.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    [industryView addSubview:_industryNameLB];

    UIImageView *industryArrow = [[UIImageView alloc]init];
    industryArrow.image = [UIImage imageNamed:@"my_center_arrow"];
    [industryView addSubview:industryArrow];
    
    UIView *industryLine = [[UIView alloc]init];
    industryLine.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [industryView addSubview:industryLine];
    
    /* ------------------------- 投资区域 -------------------------  */
    UIButton *areaView =[UIButton buttonWithType:UIButtonTypeCustom];
    areaView.backgroundColor = [UIColor whiteColor];
    [areaView addTarget:self action:@selector(chooseArea:) forControlEvents:UIControlEventTouchUpInside];
    [_mScrollView addSubview:areaView];
    
    UILabel *area = [[UILabel alloc]init];
    
    if (self.intentType == 1) {
        area.text = @"投资区域";
    }else{
        area.text = @"所在区域";
    }
    area.font = [UIFont systemFontOfSize:16.0f];
    area.backgroundColor = [UIColor clearColor];
    area.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    [areaView addSubview:area];
    
    _areaNameLB = [[UILabel alloc]init];
    _areaNameLB.font = [UIFont systemFontOfSize:16.0f];
    _areaNameLB.backgroundColor = [UIColor clearColor];
    _areaNameLB.textAlignment = NSTextAlignmentRight;
    _areaNameLB.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    [areaView addSubview:_areaNameLB];
    
    UIImageView *areaArrow = [[UIImageView alloc]init];
    areaArrow.image = [UIImage imageNamed:@"my_center_arrow"];
    [areaView addSubview:areaArrow];
    
    UIView *areaLine = [[UIView alloc]init];
    areaLine.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [areaView addSubview:areaLine];

    /* ------------------------- 投资区域 -------------------------  */
    UIButton *sumView =[UIButton buttonWithType:UIButtonTypeCustom];
    sumView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:sumView];
    
    UILabel *sum = [[UILabel alloc]init];
    
    if (self.intentType == 1) {
        sum.text = @"投资金额";
    }else{
        sum.text = @"融资金额";
    }
    sum.font = [UIFont systemFontOfSize:16.0f];
    sum.backgroundColor = [UIColor clearColor];
    sum.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    [sumView addSubview:sum];
    
    _sumTF = [[UITextField alloc]init];
    _sumTF.delegate = self;
    _sumTF.textAlignment = NSTextAlignmentRight;
    _sumTF.keyboardType = UIKeyboardTypeNumberPad;
    _sumTF.textColor = [UIColor colorWithHexString:@"5c5c5c"];
//    _sumTF.placeholder = @"单位（万）";
    [sumView addSubview:_sumTF];
    
    UILabel *unitLB = [[UILabel alloc]init];
    unitLB.backgroundColor = [UIColor clearColor];
    unitLB.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    unitLB.font = [UIFont systemFontOfSize:16.0f];
    unitLB.text = @"万";
    [sumView addSubview:unitLB];
    
    
    UIView *sumLine = [[UIView alloc]init];
    sumLine.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [sumView addSubview:sumLine];

    //编辑状态
    if(_isEdit){
        
        _introductionTF.text = _dataModel.intro;
        _industryNameLB.text = _dataModel.industryName;
        _industryId = _dataModel.industry;
        _areaNameLB.text = _dataModel.districtName;
        _sumTF.text = _dataModel.amount;
        self.cityPostcode = _dataModel.district;
    }
    
    [introductionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_mScrollView.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(_mScrollView.mas_top);
        make.height.equalTo(@45);
    }];
    
    [introduction mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(introductionView.mas_left).offset(10);
        make.top.equalTo(introductionView.mas_top);
        make.height.equalTo(introductionView.mas_height);
        make.width.equalTo(@70);
    }];
    
    [_introductionTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(introduction.mas_right).offset(10);
        make.right.equalTo(introductionView.mas_right).offset(-10);
        make.bottom.top.equalTo(introductionView);
    }];
    
    [introductionLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(introductionView.mas_left).offset(10);
        make.right.equalTo(introductionView.mas_right);
        make.bottom.equalTo(introductionView.mas_bottom);
        make.height.equalTo(@0.5);
    }];
    
    [industryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_mScrollView.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(introductionView.mas_bottom);
        make.height.equalTo(@45);
    }];
    
    [industry mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(industryView.mas_left).offset(10);
        make.top.equalTo(industryView.mas_top);
        make.height.equalTo(industryView.mas_height);
        make.width.equalTo(@70);
    }];
    
    [industryArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(industryView.mas_right).offset(-10);
        make.width.equalTo(@6);
        make.height.equalTo(@11);
        make.centerY.equalTo(industryView.mas_centerY);
    }];
    
    [_industryNameLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(industryView.mas_left).offset(10);
        make.right.equalTo(industryArrow.mas_left).offset(-10);
        make.top.bottom.equalTo(industryView);
        
    }];
    
    [industryLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(industryView.mas_left).offset(10);
        make.right.equalTo(industryView.mas_right);
        make.bottom.equalTo(industryView.mas_bottom);
        make.height.equalTo(@0.5);
    }];
    
    [areaView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_mScrollView.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(industryView.mas_bottom);
        make.height.equalTo(@45);
    }];
    
    [area mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaView.mas_left).offset(10);
        make.top.equalTo(areaView.mas_top);
        make.height.equalTo(areaView.mas_height);
        make.width.equalTo(@70);
    }];
    
    [areaArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(areaView.mas_right).offset(-10);
        make.width.equalTo(@6);
        make.height.equalTo(@11);
        make.centerY.equalTo(areaView.mas_centerY);
    }];
    
    [_areaNameLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(area.mas_right).offset(10);
        make.right.equalTo(areaArrow.mas_left).offset(-5);
        make.top.bottom.equalTo(areaView);
    }];
    
    [areaLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaView.mas_left).offset(10);
        make.right.bottom.equalTo(areaView);
        make.height.equalTo(@0.5);
    }];
    
    [sumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_mScrollView.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(areaView.mas_bottom);
        make.height.equalTo(@45);
    }];
    
    [sum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(sumView.mas_left).offset(10);
        make.top.height.equalTo(sumView);
        make.width.equalTo(@70);
    }];
    
    [_sumTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(sum.mas_right).offset(10);
        make.top.bottom.equalTo(sumView);
        make.right.equalTo(unitLB.mas_left).offset(-5);
    }];

    [unitLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@20);
        make.top.bottom.equalTo(sumView);
        make.right.equalTo(sumView.mas_right).offset(-10);
    }];
}

/**
 *   方式
 */
- (void)initInvestmentType{
    
    NSString *titleStr = @"";

    if(self.intentType == 1){
        titleStr = @"投资方式";
    }else{
        titleStr = @"融资方式";
    }
    
//    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
//    //设置“投资方式”文字颜色
//    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5c5c5c"] range: NSMakeRange(0,4)];
//    //设置“（选填）”文字颜色
//    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"c6c6c6"] range: NSMakeRange(4,4)];
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:titleView];
    
    UILabel *typeTitle = [[UILabel alloc]init];
    typeTitle.backgroundColor = [UIColor whiteColor];
    typeTitle.font = [UIFont systemFontOfSize:16.0f];
    typeTitle.text = titleStr;
    typeTitle.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    [titleView addSubview:typeTitle];
   
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [titleView addSubview:line];
    
    UIView *selectTypeView = [[UIView alloc]init];
    selectTypeView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:selectTypeView];
    
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_sumTF.mas_bottom).offset(10);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@45);
    }];

    [typeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleView.mas_left).offset(10);
        make.top.bottom.right.equalTo(titleView);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.equalTo(titleView);
        make.left.equalTo(titleView.mas_left).offset(10);
        make.height.equalTo(@0.5);
    }];
    
    [selectTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@45);
    }];
    
    NSArray *typeArray = nil;
    if(self.intentType == 1){
        typeArray = @[@"股权投资",@"债权投资",@"不限"];
    }else{
        typeArray = @[@"股权融资",@"债权融资",@"不限"];
    }
    
    for (int i = 0; i < typeArray.count; i ++) {
        
        QRadioButton *radioBtn = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId"];
        [radioBtn setImage:[UIImage imageNamed:@"radio_discover_unsel"] forState:UIControlStateNormal];
        [radioBtn setImage:[UIImage imageNamed:@"radio_discover_sel"] forState:UIControlStateSelected];
        radioBtn.tag = i;
        radioBtn.delegate = self;
        [radioBtn setTitleColor:[UIColor colorWithHexString:@"5c5c5c"] forState:UIControlStateNormal];
        [radioBtn setTitle:typeArray[i] forState:UIControlStateNormal];
        [radioBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [selectTypeView addSubview:radioBtn];
        
        [radioBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@100);
            make.left.equalTo(selectTypeView.mas_left).offset(100*i+10);
            make.bottom.top.equalTo(selectTypeView);
        }];
        
        if (_isEdit && [[typeArray objectAtIndex:i] hasString:_dataModel.modeName]) {
            
            radioBtn.checked = YES;
            [radioBtn.delegate didSelectedRadioButton:radioBtn groupId:nil];
        }
        
    }
    
}

/**
 *   详情
 */
- (void)initSumDetail{
    
    NSString *titleStr = @"";
    
    if(self.intentType == 1){
        titleStr = @"资金详情";
    }else{
        titleStr = @"项目详情";
    }
    
//    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
//    //设置“投资方式”文字颜色
//    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5c5c5c"] range: NSMakeRange(0,4)];
//    //设置“（选填）”文字颜色
//    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"c6c6c6"] range: NSMakeRange(4,4)];
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:titleView];
    
    UILabel *typeTitle = [[UILabel alloc]init];
    typeTitle.backgroundColor = [UIColor whiteColor];
    typeTitle.font = [UIFont systemFontOfSize:16.0f];
    typeTitle.text = titleStr;
    typeTitle.textColor = [UIColor colorWithHexString:@"5c5c5c"];
    [titleView addSubview:typeTitle];
    
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [titleView addSubview:line];

    UIView *detailView = [[UIView alloc]init];
    detailView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:detailView];
    
    _sumDetailTV = [[UITextView alloc]init];
    _sumDetailTV.delegate = self;
    _sumDetailTV.backgroundColor = [UIColor whiteColor];
    _sumDetailTV.font = [UIFont systemFontOfSize:15.0f];
    _sumDetailTV.textColor = [UIColor lightGrayColor];
    _sumDetailTV.textAlignment = NSTextAlignmentLeft;
    _sumDetailTV.text = _textViewPlaceHolder;
    [detailView addSubview:_sumDetailTV];
    
    //字数限制显示
    _statusLabel = [[UILabel alloc]init];
    _statusLabel.text = [NSString stringWithFormat:@"0/%d",MAX_COUNT];
    _statusLabel.backgroundColor = [UIColor clearColor];
    _statusLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    _statusLabel.textColor = [UIColor colorWithRed:144.0/255.0 green:144.0/255.0 blue:144.0/255.0 alpha:1.0];
    [_statusLabel sizeToFit];
    [detailView addSubview:_statusLabel];
    
    
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_sumTF.mas_bottom).offset(110);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@45);
    }];
    
    [typeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleView.mas_left).offset(10);
        make.top.bottom.right.equalTo(titleView);
    }];

    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.equalTo(titleView);
        make.left.equalTo(titleView.mas_left).offset(10);
        make.height.equalTo(@0.5);
    }];
    
    [detailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(titleView.mas_bottom);
        make.height.equalTo(@100);
        if (!_isEdit) {
             make.bottom.equalTo(_mScrollView.mas_bottom);
        }
    }];
    
    [_sumDetailTV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(detailView).offset(10);
        make.bottom.right.equalTo(detailView).offset(-10);
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(detailView.mas_right).offset(-5);
        make.bottom.equalTo(detailView.mas_bottom).offset(-5);
    }];
    
    if(_isEdit){
        
        _sumDetailTV.text = _dataModel.content;
        _statusLabel.text = [NSString stringWithFormat:@"%ld/%d",_dataModel.content.length,MAX_COUNT];
        
        UIButton *deleteBtn = [[UIButton alloc]init];
        deleteBtn.layer.masksToBounds = YES;
        deleteBtn.layer.cornerRadius = 3.0f;
        deleteBtn.backgroundColor = [UIColor colorWithHexString:@"f2463e"];
        [deleteBtn addTarget:self action:@selector(deleteIntent:) forControlEvents:UIControlEventTouchUpInside];
        [_mScrollView addSubview:deleteBtn];
        
        UIImageView *deleteIcon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"find_delete"]];
        [deleteBtn addSubview:deleteIcon];
        
        UILabel *deleteText = [[UILabel alloc]init];
        deleteText.backgroundColor = [UIColor clearColor];
        deleteText.text = @"删除";
        deleteText.font = [UIFont systemFontOfSize:16.0f];
        deleteText.textColor = [UIColor colorWithHexString:@"ffffff"];
        [deleteBtn addSubview:deleteText];
        
        [deleteIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@20);
            make.centerY.equalTo(deleteBtn.mas_centerY);
            make.right.equalTo(deleteBtn.mas_centerX).offset(-6);
        }];
        
        [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(detailView.mas_bottom).offset(24);
            make.left.equalTo(self.view.mas_left).offset(24);
            make.right.equalTo(self.view.mas_right).offset(-24);
            make.height.equalTo(@44);
            make.bottom.equalTo(_mScrollView.mas_bottom).offset(-10);
        }];
        
        [deleteText mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(deleteBtn.mas_centerY);
            make.left.equalTo(deleteBtn.mas_centerX).offset(6);
        }];
        
    }
}

- (void)deleteIntent:(id)sender{
    
    [self deleteIntent];
    
}

#pragma delegate of RadioButton
- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId{
    
    switch (radio.tag) {
        case 0:
            _investType = @"301";       //股权投资
            break;
        case 1:
             _investType = @"302";      //债权投资
            break;
        case 2:
             _investType = @"399";      //不限
            break;
        default:
            break;
    }
}


#pragma delegate of UITextView

- (void)textViewDidChange:(UITextView *)textView {
    
    NSInteger number = [textView.text length];
    if (number > MAX_COUNT) {
        
        NSString *alertStr = [NSString stringWithFormat:@"字符个数不能超过%d",MAX_COUNT];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:alertStr delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        textView.text = [textView.text substringToIndex:MAX_COUNT];
        number = MAX_COUNT;
    }
    _statusLabel.text = [NSString stringWithFormat:@"%ld/%d",(long)number,MAX_COUNT];
}


- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:_textViewPlaceHolder]) {
        textView.text = @"";
    }
  
}


- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (textView.text.length<1) {
        
        textView.text = _textViewPlaceHolder;
        
    }
}

#pragma delegate of UITextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    //限制简介内容20个字以内
    if (textField == _introductionTF) {
        if (textField.text.length > 19) {
            textField.text = [textField.text substringToIndex:19];
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField*)theTextField{
    
    [theTextField resignFirstResponder];
    [_mScrollView setContentOffset:_mScrollViewPoint animated:YES];
    
    return YES;
    
}

#pragma mark 网络请求

/**
 * 发布意向
 */
- (void)sendIntent{
    
    //请求指示器
    [SVProgressHUD show];
    
    PushProjectOrSumRequest *request = [[PushProjectOrSumRequest alloc]init];
    request.intro = _introductionTF.text;
    request.industry = self.industryId;
    request.amount = _sumTF.text;
    request.content = _sumDetailTV.text;
    request.mode = _investType;
    
    if (self.intentType == 1) {
        request.type = [NSString stringWithFormat:@"%ld",Intent_Money];
        
        NSString *resultStr = @"";
        for (int i = 0; i < [self.cityNameDic allValues].count; i ++) {
            
            if (i == 0) {
                resultStr = [NSString stringWithFormat:@"%@",[[self.cityPostCodeDic allValues] objectAtIndex:0]];
            }else{
                resultStr = [NSString stringWithFormat:@"%@,%@",resultStr,[[self.cityPostCodeDic allValues] objectAtIndex:i]];
            }
        }
        request.district = resultStr;
        
    }else{
        request.type =  [NSString stringWithFormat:@"%d",Intent_Project];
        request.district = self.cityPostcode;
    }
   
    [_findService pushProjectOrSumWithRequest:request success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        PushModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                [MBProgressHUD showHUDTitle:@"发布成功" onView:self.view];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:INTENT_PUSH object:nil];
                
//                [self.navigationController popViewControllerAnimated:YES];
                
                RecommendViewController *recommendCtrl = [[RecommendViewController alloc]init];
                [self.navigationController pushViewController:recommendCtrl animated:YES];
                
                break;
            }
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"发布失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        LogInfo(@"error:%@",error);
        [self showTipViewWithMsg:@"发布失败"];
    }];
}

/**
 * 修改意向
 */
- (void)modifyIntent{
    
    //请求指示器
    [SVProgressHUD show];
    
    PushProjectOrSumRequest *request = [[PushProjectOrSumRequest alloc]init];
    request.intro = _introductionTF.text;
    request.industry = self.industryId;
    request.amount = _sumTF.text;
    request.content = _sumDetailTV.text;
    request.mode = _investType;
    request.district = self.cityPostcode;
    request.uid = _dataModel.uid;
    
    if (self.intentType == 1) {
        request.type = [NSString stringWithFormat:@"%ld",Intent_Money];
        
    }else{
        request.type =  [NSString stringWithFormat:@"%ld",Intent_Project];
        
    }
    
    [_findService editProjectOrSumWithRequest:request success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        PushModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                [MBProgressHUD showHUDTitle:@"修改成功" onView:self.view];
                
                //修改后的数据传给上级页面
                _dataModel.intro = request.intro;
                _dataModel.industry = request.industry;
                _dataModel.amount = request.amount;
                _dataModel.content = request.content;
                _dataModel.mode = request.mode;
                _dataModel.district = request.district;
                _dataModel.type = [request.type intValue];
                [[NSNotificationCenter defaultCenter] postNotificationName:INTENT_MODIFY object:_dataModel];
                
                
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"修改失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        LogInfo(@"error:%@",error);
        [self showTipViewWithMsg:@"发布失败"];
    }];

}

/**
 *  删除意向
 */
-(void)deleteIntent{
    
    [SVProgressHUD show];
    
    [_findService deleteProjectOrSumWithId:_dataModel.uid success:^(id responseObject) {
        if(responseObject){
            
            [SVProgressHUD dismiss];
            PushModel *model = (PushModel*)responseObject;
            if(model.res == 1){
                
                [[NSNotificationCenter defaultCenter] postNotificationName:INTENT_DELETE object:nil];
               
                [self.navigationController popViewControllerAnimated:YES];
                return ;
            }
        }
        [self showTipViewWithMsg:@"删除意向失败，请稍后再试！"];
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"删除意向失败，请稍后再试！"];
    }];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
