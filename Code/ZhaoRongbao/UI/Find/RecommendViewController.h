//
//  RecommendViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/11.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import "ZRB_ViewController.h"

@interface RecommendViewController : ZRB_ViewController<UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *_topView;
    UITableView *_mTableView;
}

@property (nonatomic,copy) NSMutableArray *intentModelArray;

@property (nonatomic,copy) UILabel *infoLB;

@end
