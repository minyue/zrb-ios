//
//  RecommendViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/11.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import "RecommendViewController.h"
#import "PushViewController.h"
#import "RecommendTopButton.h"
#import "RecommendTopModel.h"
#import "SearchProjectHomeCell.h"
#import "MoreRecommendInfoCell.h"

@interface RecommendViewController ()

@end

@implementation RecommendViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    [self.navigationItem setHidesBackButton:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
}


- (void)setup{
    
    [self initTopView];
    
    _intentModelArray = [[NSMutableArray alloc]init];
    
    _mTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _mTableView.backgroundColor = [UIColor clearColor];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    
    [self.view addSubview:_mTableView];
    
    [_mTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    
}

- (void)initTopView{
    
    _topView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"recommend_top_bg"]];
    _topView.userInteractionEnabled = YES;
    [self.view addSubview:_topView];
    
    //火箭图标
    UIImageView *flagBg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"success_bj"]];
    [_topView addSubview:flagBg];
    
    //发布成功提示
    UIView *successView = [[UIView alloc]init];
    [_topView addSubview:successView];
    
    UIImageView *successIcon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"success_icon_right"]];
    [successView addSubview:successIcon];
    
    UILabel *successLB = [[UILabel alloc]init];
    successLB.text = @"恭喜您，已经发布成功!";
    successLB.font = [UIFont boldSystemFontOfSize:19.0f];
    successLB.textColor = [UIColor colorWithHexString:@"303a4a"];
    [successView addSubview:successLB];
    
    UILabel *tipLB = [[UILabel alloc]init];
    tipLB.font = [UIFont systemFontOfSize:14.0f];
    tipLB.textColor = [UIColor colorWithHexString:@"303a4a"];
    tipLB.text = @"根据您的意向，我们为您提供以下信息";
    [_topView addSubview:tipLB];
    
    //前往发现首页按钮
    RecommendTopModel *findModel = [[RecommendTopModel alloc]init];
    findModel.imageName = @"recommend_find";
    findModel.title = @"前往发现页面";
    findModel.titleColor = @"007de3";
    
    RecommendTopButton *findBtn = [[RecommendTopButton alloc]init];
    findBtn.layer.borderColor = [UIColor colorWithHexString:@"007de3"].CGColor;
    findBtn.layer.borderWidth = 1.0f;
    findBtn.model = findModel;
    [findBtn addTarget:self action:@selector(goFindHome:) forControlEvents:UIControlEventTouchUpInside];
   
    [_topView addSubview:findBtn];
    
    //继续发布按钮
    RecommendTopModel *pushModel = [[RecommendTopModel alloc]init];
    pushModel.imageName = @"recommend_push";
    pushModel.title = @"继续发布资金";
    pushModel.titleColor = @"ffffff";
    
    RecommendTopButton *pushBtn = [[RecommendTopButton alloc]init];
    pushBtn.backgroundColor = [UIColor colorWithHexString:@"007de3"];
    pushBtn.model = pushModel;
    [pushBtn addTarget:self action:@selector(pushIntent:) forControlEvents:UIControlEventTouchUpInside];
   
    [_topView addSubview:pushBtn];
    
    
    [flagBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@95);
        make.height.equalTo(@82.5);
        make.right.equalTo(_topView.mas_right);
        make.top.equalTo(_topView.mas_top);
    }];
    
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(@194);
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
    }];

    [successView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topView.mas_top).offset(@54);
        make.centerX.equalTo(_topView.mas_centerX);
    }];
    
    [successIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(@20);
        make.left.equalTo(successView.mas_left);
        make.top.bottom.equalTo(successView);
    }];
    
    [successLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(successIcon.mas_right).offset(12);
        make.right.equalTo(successView.mas_right);
        make.top.bottom.equalTo(successView);
    }];
    
    [tipLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(successView.mas_bottom).offset(12);
        make.centerX.equalTo(_topView.mas_centerX);
    }];
    
    [findBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_topView.mas_left).offset(32);
        make.bottom.equalTo(_topView.mas_bottom).offset(-35);
        make.width.equalTo(@138);
        make.height.equalTo(@32);
    }];
    
    [pushBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_topView.mas_right).offset(-32);
        make.bottom.equalTo(_topView.mas_bottom).offset(-35);
        make.width.equalTo(@138);
        make.height.equalTo(@32);
    }];
}

//跳转到发现首页
- (void)goFindHome:(id)sender{
    LogInfo(@"");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//发布意向
- (void)pushIntent:(id)sender{
    
    PushViewController *pushCtrl = [[PushViewController alloc]init];
    [self.navigationController pushViewController:pushCtrl animated:YES];
}


#pragma mark delegate of UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < 3) {
        return 101.0f;
    }else{
        return 80;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < 3) {
        SearchProjectHomeCell *cell = [SearchProjectHomeCell cellForTabvleView:tableView];
        //    cell.model = [_intentModelArray objectAtIndex:indexPath.row];
        
        return cell;
    }else{
        MoreRecommendInfoCell *cell = [MoreRecommendInfoCell cellForTableView:tableView];
        
        return cell;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
