//
//  ApproverView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/29.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApproverView : UIView

@property (nonatomic,strong) UIImageView *icon;
@property (nonatomic,strong) UILabel *title;

@end
