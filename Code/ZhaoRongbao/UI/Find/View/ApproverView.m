//
//  ApproverView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/29.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ApproverView.h"

@implementation ApproverView

- (instancetype)init{
    
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    self.backgroundColor = [UIColor clearColor];

    _icon = [[UIImageView alloc]init];
    _icon.image = [UIImage imageNamed:@"approve"];
    [self addSubview:_icon];
    
    _title = [[UILabel alloc]init];
    _title.backgroundColor = [UIColor clearColor];
    _title.textColor = [UIColor colorWithHexString:@"4997ec"];
    _title.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:_title];
    
    [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@10);
        make.left.equalTo(self.mas_left).offset(5);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_icon.mas_right).offset(5);
//        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-5);
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
    }];
}




@end
