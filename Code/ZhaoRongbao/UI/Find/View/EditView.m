//
//  EditView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "EditView.h"

@implementation EditView

- (instancetype)init{
    
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    self.backgroundColor = [UIColor colorWithHexString:@"f3f5f7"];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 10.0f;
    self.layer.borderColor = [UIColor colorWithHexString:@"e5e6eb"].CGColor;
    self.layer.borderWidth = 0.5f;
    
    UIView *view = [[UIView alloc]init];
    [self addSubview:view];
    
    _icon = [[UIImageView alloc]init];
    _icon.image = [UIImage imageNamed:@"list_bj"];
    [view addSubview:_icon];
    
    _time = [[UILabel alloc]init];
    _time.backgroundColor = [UIColor  clearColor];
    _time.font = [UIFont systemFontOfSize:11.0f];
    _time.text = @"编辑";
    _time.textColor = [UIColor colorWithHexString:@"68686d"];
    [view addSubview:_time];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@12);
        make.left.equalTo(view.mas_left);
        make.centerY.equalTo(view.mas_centerY);
    }];
    
    [_time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view.mas_centerY);
        make.left.equalTo(_icon.mas_right).offset(5);
        make.right.equalTo(view.mas_right);
    }];
}



@end
