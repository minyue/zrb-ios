//
//  FindAreaCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindAreaCell : UITableViewCell
{
    BOOL mChecked;
}

- (void)setChecked:(BOOL)checked;

@property (nonatomic,strong) UILabel *name;
@property (nonatomic,strong) UIImageView *status;

@end
