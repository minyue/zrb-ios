//
//  FindAreaCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindAreaCell.h"


@implementation FindAreaCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{

    UIView *superView = self.contentView;
    
    _name = [[UILabel alloc]init];
    _name.backgroundColor = [UIColor clearColor];
    _name.textColor = [UIColor colorWithHexString:@"161e35"];
    _name.font = [UIFont systemFontOfSize:15.0f];
    [superView addSubview:_name];
    
    _status = [[UIImageView alloc]init];
    _status.backgroundColor = [UIColor clearColor];
    [superView addSubview:_status];
    
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(15);
        make.top.bottom.equalTo(superView);
    }];
    
    [_status mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView.mas_right).offset(-10);
        make.centerY.equalTo(superView.mas_centerY);
        make.width.height.equalTo(@14);
    }];

}



- (void)setChecked:(BOOL)checked{
    
    if (checked){
        _status.image = [UIImage imageNamed:@"checkbox_sel"];
    }else{
        _status.image = [UIImage imageNamed:@"checkbox_unsel"];
    }
    
    mChecked = checked;
}
@end
