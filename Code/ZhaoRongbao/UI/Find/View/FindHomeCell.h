//
//  FineHomeCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindHomeModel.h"
#import "FindMenuTagView.h"
#import "TimeTagView.h"
//#import "ApproverView.h"
#import "EditView.h"

@interface FindHomeCell : UITableViewCell

//@property (nonatomic,copy)  UIView *superView;

@property (nonatomic,copy) TimeTagView *tagTime;               //发布时间标签

@property (nonatomic,copy) UIView *point;                       //时间标签圆点

@property (nonatomic,copy) UIView *leftTimeLine;                //时间轴竖线

@property (nonatomic,copy) UIView *detailView;

@property (nonatomic,copy) UIImageView *flagIcon;             //项目或资金的标识图标

@property (nonatomic,copy) UIButton *userAvatarBtn;             
@property (nonatomic,copy) UIImageView *userAvatar;             //头像
@property (nonatomic,copy) UILabel *name;                       //职位
@property (nonatomic,copy) UILabel *companyType;                //资金方 or 项目方
//@property (nonatomic,copy) UILabel *companyName;                //单位名称

//@property (nonatomic,copy) UIImageView *approveTypeIcon;        //认证种类图标
//@property (nonatomic,copy) UILabel *approveType;                //认证种类
//@property (nonatomic,copy) ApproverView *approveView;

@property (nonatomic,copy) UIImageView *approveIcon;            //认证标签

@property (nonatomic,copy) UIImageView *content;            //内容背景
@property (nonatomic,copy) UILabel *contentTitle;               //内容标题
@property (nonatomic,copy) UILabel *industry;                   //投资行业
@property (nonatomic,copy) UILabel *area;                       //投资区域
@property (nonatomic,copy) UILabel *sum;                        //投资金额


@property (nonatomic,copy) FindMenuTagView *focusTag;               //关注
@property (nonatomic,copy) FindMenuTagView *messageTag;             //留言
@property (nonatomic,copy) FindMenuTagView *shareTag;               //分享
@property (nonatomic,copy) EditView *editView;                      //编辑

@property (nonatomic,retain) FindHomeItemModel *model;


+ (instancetype)cellForTableView:(UITableView *)tableView;


@end
