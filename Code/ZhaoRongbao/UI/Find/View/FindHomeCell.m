//
//  FineHomeCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindHomeCell.h"


#define CONTENT_TEXT_SIZE       12.0f


@implementation FindHomeCell

+ (instancetype)cellForTableView:(UITableView *)tableView{
    
    static NSString *ID = @"FindHomeCell";
    FindHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        
        cell = [[FindHomeCell alloc]init];
    }
 
    return cell;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}


//- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
//    
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        [self setup];
//    }
//    
//    return self;
//}

- (void)setup{

    self.backgroundColor = [UIColor clearColor];
    UIView *superView = self.contentView;
    
    //时间轴圆点
    _point = [[UIView alloc]init];
    _point.opaque = YES;
    _point.layer.cornerRadius = 2.0;
    _point.layer.masksToBounds = YES;
    _point.backgroundColor = [UIColor colorWithHexString:@"dfe0e4"];
    [superView addSubview:_point];
    
    //时间轴竖线
    _leftTimeLine = [[UIView alloc]init];
    _leftTimeLine.opaque = YES;
    _leftTimeLine.backgroundColor = [UIColor colorWithHexString:@"dfe0e4"];
    [superView addSubview:_leftTimeLine];
    
    
   //发布日期
    _tagTime = [[TimeTagView alloc]init];
    _tagTime.translatesAutoresizingMaskIntoConstraints= NO;
    [superView addSubview:_tagTime];
    
    //详情视图
    _detailView = [[UIView alloc]init];
    _detailView.opaque = YES;
    _detailView.backgroundColor = [UIColor whiteColor];
    _detailView.layer.masksToBounds = YES;
    _detailView.layer.borderWidth = 0.5f;
    _detailView.layer.borderColor = [UIColor colorWithHexString:@"e5e6eb"].CGColor;
    _detailView.layer.cornerRadius = 3.0;
    [superView addSubview:_detailView];
    
    //项目和资金的标识图标
    _flagIcon = [[UIImageView alloc]init];
    [superView addSubview:_flagIcon];
    
    _userAvatarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _userAvatarBtn.layer.cornerRadius = 18;
    _userAvatarBtn.layer.masksToBounds = YES;
    _userAvatarBtn.layer.borderWidth = 0.5f;
    _userAvatarBtn.layer.borderColor = [UIColor colorWithHexString:@"e5e6eb"].CGColor;
//    _userAvatarBtn.backgroundColor = [UIColor clearColor];
    _userAvatarBtn.opaque = YES;
    [_detailView addSubview:_userAvatarBtn];
    
    //头像
    _userAvatar = [[UIImageView alloc]init];
    _userAvatar.layer.cornerRadius = 18;
    _userAvatar.layer.masksToBounds = YES;
    _userAvatar.layer.borderWidth = 0.5f;
    _userAvatar.opaque = YES;
    _userAvatar.layer.borderColor = [UIColor colorWithHexString:@"e5e6eb"].CGColor;
    [_userAvatarBtn addSubview:_userAvatar];
    
    
    //名称
    _name = [[UILabel alloc]init];
//    _name.backgroundColor = [UIColor clearColor];
//    _name.text = @"刘经理";
    _name.textColor = [UIColor colorWithHexString:@"303a4a"];
    _name.font = [UIFont systemFontOfSize:14.0f];
    [_detailView addSubview:_name];
    
    //类型
    _companyType = [[UILabel alloc]init];
    _companyType.backgroundColor = [UIColor clearColor];
    _companyType.textColor = [UIColor colorWithHexString:@"68686d"];
    _companyType.font = [UIFont systemFontOfSize:11.0f];
    [_detailView addSubview:_companyType];
    
    //公司名称
//    _companyName = [[UILabel alloc]init];
//    _companyName.backgroundColor = [UIColor clearColor];
//    _companyName.textColor = [UIColor colorWithHexString:@"68686d"];
//    _companyName.font = [UIFont systemFontOfSize:11.0f];
//    [_detailView addSubview:_companyName];
//    
    
    _approveIcon = [[UIImageView alloc]init];
    [_detailView addSubview:_approveIcon];
    
    /*----------------------------- 内容 -----------------------------*/
    _content = [[UIImageView alloc]init];
    _content.backgroundColor = [UIColor clearColor];
    _content.userInteractionEnabled = NO;
//    _content.image = [UIImage imageNamed:@"projectBubble"];
    [_detailView addSubview:_content];
    
    //标题
    _contentTitle = [[UILabel alloc]init];
    _contentTitle.backgroundColor = [UIColor clearColor];
    _contentTitle.textColor = [UIColor colorWithHexString:@"5f5f5f"];
    _contentTitle.lineBreakMode = UILineBreakModeTailTruncation;
//    _contentTitle.text = @"企业资金1000万-5亿资金寻寻求政府平台和国企线下合作项目";
    _contentTitle.numberOfLines = 1;
    _contentTitle.font = [UIFont boldSystemFontOfSize:14.0f];
    [_content addSubview:_contentTitle];
    
    //投资行业
    _industry = [[UILabel alloc]init];
    _industry.backgroundColor = [UIColor clearColor];
    _industry.numberOfLines = 1;
    _industry.textColor = [UIColor colorWithHexString:@"68686d"];
    _industry.lineBreakMode = UILineBreakModeTailTruncation;
//    _industry.text = @"投资行业：计算机硬件/电子信息/高薪技术";
    _industry.font = [UIFont systemFontOfSize:CONTENT_TEXT_SIZE];
    [_content addSubview:_industry];
    
    //投资区域
    _area = [[UILabel alloc]init];
    _area.backgroundColor = [UIColor clearColor];
    _area.numberOfLines = 1;
    _area.lineBreakMode = UILineBreakModeTailTruncation;
    _area.textColor = [UIColor colorWithHexString:@"68686d"];
//    _area.text = @"投资区域：湖北 湖南";
    _area.font = [UIFont systemFontOfSize:CONTENT_TEXT_SIZE];
    [_content addSubview:_area];

    //投资金额
    _sum = [[UILabel alloc]init];
    _sum.backgroundColor = [UIColor clearColor];
    _sum.numberOfLines = 1;
    _sum.lineBreakMode = UILineBreakModeTailTruncation;
    _sum.textColor = [UIColor colorWithHexString:@"68686d"];
//    _sum.text = @"投资金额：1000万整";
    _sum.font = [UIFont systemFontOfSize:CONTENT_TEXT_SIZE];
    [_content addSubview:_sum];
    
    /*----------------------------- 内容 -----------------------------*/

    //关注
    _focusTag = [[FindMenuTagView alloc]init];
    [_detailView addSubview:_focusTag];
    
    //留言
    _messageTag = [[FindMenuTagView alloc]init];
    [_detailView addSubview:_messageTag];
    
    //分享
    _shareTag = [[FindMenuTagView alloc]init];
    [_detailView addSubview:_shareTag];
    
    //编辑
    _editView = [[EditView alloc]init];
    [_detailView addSubview:_editView];
    
}


/**
 *   根据数据模型动态约束
 */
- (void)setSubViewLayoutWithModel:(FindHomeItemModel *)model{
    
    UIView *superView = self.contentView;
    
    [_userAvatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_userAvatarBtn);
        make.top.bottom.equalTo(_userAvatarBtn);
    }];
  
    
    if (model.index == 0) {
        
        _tagTime.hidden = NO;
        
        [_leftTimeLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@1);
            make.top.equalTo(_point.mas_bottom);
            make.bottom.equalTo(superView.mas_bottom);
            make.centerX.equalTo(_flagIcon.mas_centerX);
        }];

        
        [_point mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@4);
            make.centerY.equalTo(_tagTime.mas_centerY);
            make.centerX.equalTo(_flagIcon.mas_centerX);
        }];
        
        [_tagTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(superView.mas_top).offset(12);
            make.left.equalTo(_detailView.mas_left);
        }];

        [_detailView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_tagTime.mas_bottom).offset(12);
            make.right.equalTo(superView.mas_right).offset(-10);
            make.bottom.equalTo(superView.mas_bottom);
        }];

        
    }else{
        
        [_leftTimeLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@1);
            make.top.equalTo(superView.mas_top);
            make.bottom.equalTo(superView.mas_bottom);
            make.centerX.equalTo(_flagIcon.mas_centerX);
        }];
        
        if (model.isShowTimeFlag) {
            
            _tagTime.hidden = NO;
            _point.hidden = NO;
            
            [_point mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.equalTo(@4);
                make.centerY.equalTo(_tagTime.mas_centerY);
                make.centerX.equalTo(_flagIcon.mas_centerX);
            }];
            
            [_tagTime mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(superView.mas_top).offset(12);
                make.left.equalTo(_detailView.mas_left);
            }];
            
            [_detailView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(_tagTime.mas_bottom).offset(12);
                make.right.equalTo(superView.mas_right).offset(-10);
                make.bottom.equalTo(superView.mas_bottom);
            }];
            
        }else {
            _tagTime.hidden = YES;
            _point.hidden = YES;
            
            [_detailView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(superView.mas_top).offset(12);
                make.right.equalTo(superView.mas_right).offset(-10);
                make.bottom.equalTo(superView.mas_bottom);
            }];
        }
    }
    
    [_approveIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@15);
        make.height.equalTo(@26);
        make.centerY.equalTo(_name.mas_centerY);
        make.right.equalTo(_detailView.mas_right).offset(-12);
    }];
    
    [_flagIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@30);
        make.height.equalTo(@19);
        make.left.equalTo(superView.mas_left).offset(12);
//        make.top.equalTo(_detailView.mas_top).offset(7);
        make.centerX.equalTo(_detailView.mas_left).offset(-9);
        make.centerY.equalTo(_name.mas_centerY);
    }];
    
    [_userAvatarBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_detailView.mas_left).offset(12);
        make.top.equalTo(_detailView.mas_top).offset(10);
        make.width.equalTo(@36);
        make.height.equalTo(@36);
    }];
    
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_userAvatar.mas_right).offset(10);
        make.top.equalTo(_userAvatar.mas_top).offset(5);
        make.centerY.equalTo(_userAvatar.mas_centerY).offset(-9);
    }];
    
    
    [_companyType mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_userAvatar.mas_right).offset(10);
        make.right.equalTo(_detailView.mas_right).offset(-10);
        make.centerY.equalTo(_userAvatar.mas_centerY).offset(9);
    }];
    
//    [_companyName mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_companyType.mas_right).offset(10);
//        make.centerY.equalTo(_companyType.mas_centerY);
//    }];
    
    [_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_detailView.mas_left).offset(10);
        make.right.equalTo(_detailView.mas_right).offset(-10);
        make.top.equalTo(_userAvatar.mas_bottom).offset(5);
        
    }];
    
    [_contentTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_content.mas_left).offset(10);
        make.right.equalTo(_content.mas_right).offset(-10);
        make.top.equalTo(_content.mas_top).offset(13);
    }];
    
    
    [_industry mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_content.mas_left).offset(10);
        make.right.equalTo(_content.mas_right).offset(-10);
        make.top.equalTo(_contentTitle.mas_bottom).offset(7);
    }];
    
    
    [_area mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_content.mas_left).offset(10);
        make.right.equalTo(_content.mas_right).offset(-10);
        make.top.equalTo(_industry.mas_bottom).offset(7);
    }];
    
    [_sum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_content.mas_left).offset(10);
        make.right.equalTo(_content.mas_right).offset(-10);
        make.top.equalTo(_area.mas_bottom).offset(7);
        make.bottom.equalTo(_content.mas_bottom).offset(-10);
    }];
    
    [_focusTag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_detailView.mas_left).offset(12);
        make.top.equalTo(_content.mas_bottom);
        make.bottom.equalTo(_detailView.mas_bottom);
        make.height.equalTo(@37);
    }];
    
    [_messageTag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_focusTag.mas_right).offset(40);
        make.centerY.equalTo(_focusTag.mas_centerY);
    }];
    
    [_shareTag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_messageTag.mas_right).offset(40);
        make.centerY.equalTo(_focusTag.mas_centerY);
    }];
    
    
    [_editView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@57);
        make.height.equalTo(@21);
        make.right.equalTo(_detailView.mas_right).offset(-10);
        make.centerY.equalTo(_focusTag.mas_centerY);
    }];
}


- (void)setModel:(FindHomeItemModel *)model{
    
    if (model) {
        
//        NSLog(@"----------> %@",model.isShowTimeFlag?@"YES":@"NO");
        
        [self setSubViewLayoutWithModel:model];
        
        _tagTime.time.text = [ZRBUtilities stringToData:@"yyyy/MM/dd" interval:model.pushTime];
        
        [_userAvatar sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",IMAGE_SERVER_URL,model.avatar]] placeholderImage:[UIImage imageNamed:@"portrait"] options:SDWebImageRefreshCached];
        
        _name.text = model.userName;
        
        _companyType.text = [NSString stringWithFormat:@"%@·%@·%@",model.userIndustryName,model.org,model.position];
        
        if ([model.typeName isEqualToString:@"项目方"]) {
            
            _flagIcon.image = [UIImage imageNamed:@"list_xm"];
            _content.image = [UIImage imageNamed:@"projectBubble"];
            
        }else if ([model.typeName isEqualToString:@"资金方"]){
            _flagIcon.image = [UIImage imageNamed:@"list_zj"];
            _content.image = [UIImage imageNamed:@"cashBubble"];
        }
        
//        _companyName.text = model.org;
        
        //认证标签
        if ([model.auth intValue] == OnSideApprove) {
            _approveIcon.hidden = NO;
            _approveIcon.image = [UIImage imageNamed:@"list_sdrz"];
            
        }else if([model.auth intValue] == OnDataApprove){
            _approveIcon.hidden = NO;
            _approveIcon.image = [UIImage imageNamed:@"list_zlrz"];
        }else{
            _approveIcon.hidden = YES;
        }
        
        _contentTitle.text = model.intro;
        
        _industry.text = [NSString stringWithFormat:@"所属行业：%@",model.industryName];
        
        _area.text = [NSString stringWithFormat:@"所在区域：%@",model.districtName];
        
        _sum.text = [NSString stringWithFormat:@"融资金额：%@万元整",model.amount];
        
        if ([model.attention isEqualToString:@"0"]) {
            [_focusTag initHeadTagView:@"like" title:[NSString stringWithFormat:@"%@",model.attentionTimes] titleColor:@"a6a6a6"];
        }else if([model.attention isEqualToString:@"1"]){
            [_focusTag initHeadTagView:@"like_hl" title:[NSString stringWithFormat:@"%@",model.attentionTimes] titleColor:@"a6a6a6"];
        }
        
        
        [_messageTag initHeadTagView:@"comment" title:[NSString stringWithFormat:@"%@",model.messageAmount] titleColor:@"a6a6a6"];
        [_shareTag initHeadTagView:@"share" title:@"" titleColor:@"a6a6a6"];
        
        if ([ZRB_UserManager isLogin]) {
            
            if ([[ZRB_UserManager shareUserManager].userData.uid isEqualToString:model.userId]) {
                
                _editView.hidden = NO;
            }else{
                _editView.hidden = YES;
            }
            
        }else{
            _editView.hidden = YES;
        }

        
    }
    
}

@end
