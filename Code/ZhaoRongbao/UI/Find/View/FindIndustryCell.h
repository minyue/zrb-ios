//
//  IndustryCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindIndustryModel.h"
#import "IndustryTagView.h"

@interface FindIndustryCell : UITableViewCell
{
    BOOL	m_checked;
}
@property (nonatomic,strong) IndustryTagView *icon;
@property (nonatomic,strong) UILabel *name;
@property (nonatomic,strong) UIImageView *status;               //选中状态

- (void)initWithModel:(FindIndustryItemModel *)model;
- (void)setChecked:(BOOL)checked;
@end
