//
//  IndustryCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindIndustryCell.h"


@implementation FindIndustryCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    UIView *superView = self.contentView;
    
    //图标
    _icon = [[IndustryTagView alloc]init];
    [superView addSubview:_icon];
    
    //行业名称
    _name = [[UILabel alloc]init];
    _name.backgroundColor = [UIColor clearColor];
    _name.font = [UIFont systemFontOfSize:15.0f];
    _name.textColor = [UIColor colorWithHexString:@"515151"];
    [superView addSubview:_name];
    
    //选择状态图标
    _status = [[UIImageView alloc]init];
    [superView addSubview:_status];
    
    [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(superView.mas_centerX);
        make.width.height.equalTo(@20);
        make.centerY.equalTo(superView.mas_centerY);
        make.left.equalTo(superView.mas_left).offset(15);
    }];
    
    [_status mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@30);
        make.centerY.equalTo(superView.mas_centerY);
        make.right.equalTo(superView.mas_right).offset(-10);
    }];
    
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_icon.mas_right).offset(10);
        make.centerY.equalTo(_icon.mas_centerY);
        make.right.equalTo(_status.mas_left);
    }];
    
}

- (void)initWithModel:(FindIndustryItemModel *)model{
    
    _name.text = model.industryName;
    
    [_icon initWithTitle:[model.industryName substringToIndex:1] textColor:[model.color substringFromIndex:1]];
    
}

- (void)setChecked:(BOOL)checked{
    
    if (checked){
        _status.image = [UIImage imageNamed:@"selected.png"];
    }else{
        _status.image = nil;
    }
    
    m_checked = checked;
    
}

@end
