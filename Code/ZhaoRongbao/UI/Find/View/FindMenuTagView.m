//
//  FindMenuTagView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "FindMenuTagView.h"

@implementation FindMenuTagView

- (instancetype)init{
    self= [super init];
    if(self){
        [self setup];
    }
    return self;
}

- (void)setup{
    
    
    
    UIView *view = [[UIView alloc]init];
     view.userInteractionEnabled = NO;
    [self addSubview:view];
    
    _icon = [[UIImageView alloc]init];
    [view addSubview:_icon];
    
    _title = [[UILabel alloc]init];
    _title.backgroundColor = [UIColor clearColor];
    _title.textColor = [UIColor colorWithHexString:@"5f5f5f"];
    _title.font = [UIFont systemFontOfSize:12.0f];
    [view addSubview:_title];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@15);
        make.height.equalTo(@15);
        make.left.equalTo(view.mas_left);
        make.top.equalTo(view.mas_top);
        make.bottom.equalTo(view.mas_bottom);
    }];
    
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_icon.mas_right).offset(5);
        make.centerY.equalTo(_icon.mas_centerY);
        make.right.equalTo(view.mas_right);
    }];
}

- (void)initHeadTagView:(NSString *)iconName title:(NSString *)title titleColor:(NSString *)colorStr{
    
    _icon.image = [UIImage imageNamed:iconName];
    _title.text = title;
    _title.textColor = [UIColor colorWithHexString:colorStr];
}

@end
