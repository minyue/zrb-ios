//
//  HeadTagView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeadTagView : UIView

@property (nonatomic,copy)UIImageView *icon;
@property (nonatomic,copy)UILabel *title;

- (void)initHeadTagView:(NSString *)iconName title:(NSString *)title titleColor:(NSString *)colorStr;

@end
