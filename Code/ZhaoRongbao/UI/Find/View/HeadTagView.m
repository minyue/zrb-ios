//
//  HeadTagView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "HeadTagView.h"

@implementation HeadTagView

- (instancetype)init{
    self= [super init];
    if(self){
        [self setup];
    }
    return self;
}

- (void)setup{
    
    _icon = [[UIImageView alloc]init];
    [self addSubview:_icon];
    
    _title = [[UILabel alloc]init];
    _title.backgroundColor = [UIColor clearColor];
    _title.font = [UIFont systemFontOfSize:16.0f];
    _title.textColor = [UIColor colorWithHexString:@"303a4a"];
    [self addSubview:_title];
    
    [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@20);
        make.height.equalTo(@20);
        make.left.equalTo(self.mas_left);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_icon.mas_right).offset(10);
        make.centerY.equalTo(_icon.mas_centerY);
        make.right.equalTo(self.mas_right);
    }];
}

- (void)initHeadTagView:(NSString *)iconName title:(NSString *)title titleColor:(NSString *)colorStr{
    
    _icon.image = [UIImage imageNamed:iconName];
    _title.text = title;
    _title.textColor = [UIColor colorWithHexString:colorStr];
}

@end
