//
//  IndustryTagView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndustryTagView : UIView

- (void)initWithTitle:(NSString *)title textColor:(NSString *)color;

@property (nonatomic,copy) UILabel *name;

@end
