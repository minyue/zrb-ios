//
//  IndustryTagView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "IndustryTagView.h"

@implementation IndustryTagView

-(instancetype)init{
    
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 2;
    
    _name = [[UILabel alloc]init];
    _name.backgroundColor = [UIColor clearColor];
    _name.textColor = [UIColor whiteColor];
    _name.font = [UIFont systemFontOfSize:12.0f];
    
    [self addSubview:_name];
    
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
}

- (void)initWithTitle:(NSString *)title textColor:(NSString *)color{
    
    _name.text = title;
    self.backgroundColor = [UIColor colorWithHexString:color];
    
}

@end
