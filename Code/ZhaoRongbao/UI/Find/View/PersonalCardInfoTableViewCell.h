//
//  PersonalCardInfoTableViewCell.h
//  ZhaoRongbao
//
//  Created by zouli on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//  个人名片-信息Cell

#import <UIKit/UIKit.h>
#import "FindHomeModel.h"

static NSString *const personalCardInfoTableViewCellIdentifier = @"PersonalCardInfoTableViewCellIdentifier";

@interface PersonalCardInfoTableViewCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UIButton *btnLove;
@property (nonatomic,strong) IBOutlet UIButton *btnMessage;
@property (nonatomic,strong) IBOutlet UIButton *btnShare;
- (void)initWithData:(FindHomeItemModel*)model;

@end
