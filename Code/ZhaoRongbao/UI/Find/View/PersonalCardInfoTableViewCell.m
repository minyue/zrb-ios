//
//  PersonalCardInfoTableViewCell.m
//  ZhaoRongbao
//
//  Created by zouli on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "PersonalCardInfoTableViewCell.h"
@interface PersonalCardInfoTableViewCell()

@property (nonatomic,weak) IBOutlet UIView *conView;
@property (nonatomic,weak) IBOutlet UIView *infoView;
@property (nonatomic,weak) IBOutlet UILabel *lblTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblIndustry;//行业
@property (nonatomic,weak) IBOutlet UILabel *lblArea;//区域
@property (nonatomic,weak) IBOutlet UILabel *lblMoney;//金额
@property (nonatomic,weak) IBOutlet UIImageView *imgLove;
@property (nonatomic,weak) IBOutlet UIImageView *imgLeaveMessage;
@property (nonatomic,weak) IBOutlet UIImageView *imgShare;
@property (nonatomic,weak) IBOutlet UILabel *lblLoveCount;
@property (nonatomic,weak) IBOutlet UILabel *lblLeaveMessageCount;
@property (nonatomic,weak) IBOutlet UIImageView *imgStatue;

@end
@implementation PersonalCardInfoTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self setup];
}

- (void)setup
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _conView.layer.cornerRadius = 3.0f;
    _conView.layer.masksToBounds = YES;
    _conView.layer.borderWidth = 1.0f;
    _conView.layer.borderColor = [UIColor colorWithHexString:@"e5e6eb"].CGColor;
    
    _infoView.layer.cornerRadius = 5.0f;
    _infoView.layer.masksToBounds = YES;
}

- (void)initWithData:(FindHomeItemModel*)model{
    if(model.intro){
        _lblTitle.text = model.intro;
    }else{
        _lblTitle.text = @"";
    }
    //行业
    if(model.industryName){
        _lblIndustry.text = [NSString stringWithFormat:@"所属行业：%@",model.industryName];
    }else{
        _lblIndustry.text = @"所属行业：";
    }
    //区域
    if(model.districtName){
        _lblArea.text = [NSString stringWithFormat:@"所在区域：%@",model.districtName];
    }else{
        _lblArea.text = @"所在区域：";
    }
    //金额
    if(model.amount){
        _lblMoney.text = [NSString stringWithFormat:@"融资金额：%@万元整",model.amount];
    }else{
        _lblMoney.text = @"融资金额：0";
    }
    if(model.attentionTimes){
        _lblLoveCount.text = model.attentionTimes;
    }else{
        _lblLoveCount.text = @"0";
    }
    if ([model.attention isEqualToString:@"0"]) {
        _imgLove.image = [UIImage imageNamed:@"like"];
    }else if([model.attention isEqualToString:@"1"]){
        _imgLove.image = [UIImage imageNamed:@"like_hl"];
    }
    if(model.messageAmount){
        _lblLeaveMessageCount.text = model.messageAmount;
    }else{
        _lblLeaveMessageCount.text = @"0";
    }

    _imgStatue.hidden = NO;
    //认证标签
    if ([model.auth intValue] == OnSideApprove) {
        //实地认证
        _imgStatue.image = [UIImage imageNamed:@"list_sdrz"];
    }else if([model.auth intValue] == OnDataApprove){
        //资料认证
        _imgStatue.image = [UIImage imageNamed:@"list_zlrz"];
    }else{
        _imgStatue.hidden = YES;
    }
    
    if (model.type == 101) {
        //项目
        _infoView.backgroundColor = [UIColor colorWithHexString:@"f1f5fb"];//蓝色
    }else if (model.type == 102){
        //资金
        _infoView.backgroundColor = [UIColor colorWithHexString:@"fef8f8"];//红色
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
