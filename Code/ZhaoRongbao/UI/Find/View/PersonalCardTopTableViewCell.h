//
//  PersonalCardTopTableViewCell.h
//  ZhaoRongbao
//
//  Created by zouli on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//  个人名片-Top Cell

#import <UIKit/UIKit.h>
#import "FindPersonalModel.h"

static NSString *const personalCardTopTableViewCellIdentifier = @"PersonalCardTopTableViewCellIdentifier";

@interface PersonalCardTopTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIButton  *btnBack;
- (void)initWithData:(FindPersonalUserInfoModel*)model;
@end
