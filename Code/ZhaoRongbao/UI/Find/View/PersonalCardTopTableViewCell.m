//
//  PersonalCardTopTableViewCell.m
//  ZhaoRongbao
//
//  Created by zouli on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "PersonalCardTopTableViewCell.h"

@interface PersonalCardTopTableViewCell()
@property (nonatomic,weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic,weak) IBOutlet UILabel *lblName;
@property (nonatomic,weak) IBOutlet UILabel *lblPerInfo;

@end

@implementation PersonalCardTopTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self setup];
}

- (void)setup
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _imgIcon.layer.cornerRadius = _imgIcon.frameWidth/2;
    _imgIcon.layer.masksToBounds = YES;
    _imgIcon.layer.borderWidth = 2.0f;
    _imgIcon.layer.borderColor = [UIColor colorWithHexString:@"83c0f6"].CGColor;

}

- (void)initWithData:(FindPersonalUserInfoModel*)model{
//    if(model.Avatar){
//        [self.imgIcon sd_setImageWithURL:[ZRBUtilities urlWithIconPath:model.Avatar] placeholderImage:[ZRBUtilities ZRB_UserDefaultImage] options:SDWebImageRetryFailed];
//    }else{
//        [self.imgIcon setImage:[ZRBUtilities ZRB_UserDefaultImage]];
//    }
//    if(model.Umobile){
//        _lblName.text = model.Umobile;
//    }else{
//        _lblName.text = @"";
//    }
//    NSMutableString *mstr = [[NSMutableString alloc] init];
//    if(model.Industry){
//        [mstr appendString:[NSString stringWithFormat:@"%@·",model.Industry]];
//    }
//    if(model.Org){
//        [mstr appendString:[NSString stringWithFormat:@"%@·",model.Org]];
//    }
//    if(model.position){
//        [mstr appendString:[NSString stringWithFormat:@"%@",model.position]];
//    }
//    if(mstr){
//        _lblPerInfo.text = mstr;
//    }else{
//        _lblPerInfo.text = @"";
//    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
