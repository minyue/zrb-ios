//
//  RecommendTopButton.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/12.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecommendTopModel.h"

@interface RecommendTopButton : UIButton

@property (nonatomic,copy) UIImageView *icon;
@property (nonatomic,copy) UILabel *title;

@property (nonatomic,retain) RecommendTopModel *model;

//- (void)initWithImageName:(NSString *)imageName title:(NSString *)title color:(NSString *)colorStr;

@end
