//
//  RecommendTopButton.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/12.
//  Copyright © 2015年 songmk. All rights reserved.
//  推荐页面顶部按钮

#import "RecommendTopButton.h"

@implementation RecommendTopButton

- (instancetype)init{
    self = [super init];
    if (self) {
        
        [self setup];
    }
    return  self;
}

- (void)setup{
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 15.0f;
    
    
    UIView *view = [[UIView alloc]init];
    view.userInteractionEnabled = NO;
    [self addSubview:view];
    
    _icon = [[UIImageView alloc]init];
    [view addSubview:_icon];
    
    _title = [[UILabel alloc]init];
    _title.font = [UIFont systemFontOfSize:12.0f];
    [view addSubview:_title];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left);
        make.top.equalTo(view.mas_top);
        make.bottom.equalTo(view.mas_bottom);
    }];
    
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_icon.mas_right).offset(5);
        make.top.equalTo(view.mas_top);
        make.bottom.equalTo(view.mas_bottom);
        make.right.equalTo(view.mas_right);
    }];
}

- (void)setModel:(RecommendTopModel *)model
{
    _icon.image = [UIImage imageNamed:model.imageName];
    _title.text = model.title;
    _title.textColor = [UIColor colorWithHexString:model.titleColor];
}

@end
