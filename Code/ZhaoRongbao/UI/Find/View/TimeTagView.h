//
//  TimeTagView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeTagView : UIView

@property (nonatomic,copy) UIImageView *icon;

@property (nonatomic,copy) UILabel *time;

@end
