//
//  TimeTagView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/10/29.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "TimeTagView.h"

@implementation TimeTagView

- (instancetype)init{
    
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    self.backgroundColor = [UIColor whiteColor];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 3.0f;
    self.layer.borderColor = [UIColor colorWithHexString:@"e5e6eb"].CGColor;
    self.layer.borderWidth = 0.5f;
    
    _icon = [[UIImageView alloc]init];
    _icon.image = [UIImage imageNamed:@"time"];
    [self addSubview:_icon];
    
    _time = [[UILabel alloc]init];
    _time.backgroundColor = [UIColor  clearColor];
    _time.font = [UIFont systemFontOfSize:11.0f];
    _time.textColor = [UIColor colorWithHexString:@"007de3"];
    [self addSubview:_time];
    
    [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@12);
        make.left.equalTo(self.mas_left).offset(5);
        make.top.equalTo(self.mas_top).offset(3);
        make.bottom.equalTo(self.mas_bottom).offset(-3);
    }];
    
    [_time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_icon.mas_centerY);
        make.left.equalTo(_icon.mas_right).offset(5);
        make.right.equalTo(self.mas_right).offset(-5);
    }];
}


@end
