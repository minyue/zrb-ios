//
//  IntroView.m
//  DrawPad
//
//  Created by Adam Cooper on 2/4/15.
//  Copyright (c) 2015 Adam Cooper. All rights reserved.
//

#import "ABCIntroView.h"

@interface ABCIntroView () <UIScrollViewDelegate>
@property (strong, nonatomic)  UIScrollView *scrollView;
@property (strong, nonatomic)  UIPageControl *pageControl;
@property UIView *holeView;
@property UIView *circleView;
@property UIButton *experienceBtn;
@property UIButton *regisiterBtn;
@property UIButton *loginBtn;

@end

@implementation ABCIntroView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        
        [self setup];
    }
    return self;
}

- (void)setup{
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    backgroundImageView.image = [UIImage imageNamed:@"Intro_Screen_Background"];
    [self addSubview:backgroundImageView];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.frame];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.scrollView];
    
    
    NSArray *backViewArray = @[@"board1",@"board2",@"board3",@"board4"];
    NSArray *titleViewArray = @[@"zi1",@"zi2",@"zi3",@"zi4"];
    
    for (int i = 0; i < backViewArray.count; i++) {
        
        UIView *view = [[UIView alloc] init];
        self.scrollView.delegate = self;
        [self.scrollView addSubview:view];
        
        UIImageView *imageview = [[UIImageView alloc] init];
        imageview.image = [UIImage imageNamed:backViewArray[i]];
        [view addSubview:imageview];
        
        
        UIImageView *titleView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:titleViewArray[i]]];
        [view addSubview:titleView];
        
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(SCREEN_WIDTH));
            make.height.equalTo(@(SCREEN_HEIGHT));
            make.top.equalTo(self.scrollView.mas_top);
            make.left.equalTo(self.scrollView.mas_left).offset(SCREEN_WIDTH*i);
            if (i == backViewArray.count - 1) {
                make.right.equalTo(self.scrollView.mas_right);
            }
        }];
        
        [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@260);
            make.height.equalTo(@256);
            make.centerX.equalTo(view.mas_centerX);
            make.top.equalTo(view.mas_top).offset(@70);
        }];
        
        [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@266);
            make.height.equalTo(@53);
            make.centerX.equalTo(view.mas_centerX);
            make.top.equalTo(imageview.mas_bottom).offset(@25);
        }];
    }
    
    
    self.experienceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.experienceBtn setTitle:@"立即体验" forState:UIControlStateNormal];
    [self.experienceBtn setTitleColor:[UIColor colorWithHexString:@"4ea0eb"] forState:UIControlStateNormal];
    [self.experienceBtn addTarget:self action:@selector(onExperienceButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.experienceBtn.titleLabel.font = [UIFont systemFontOfSize:17.0f];
    [self addSubview:self.experienceBtn];
    
    [self.experienceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
    }];
    
    
    self.regisiterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.regisiterBtn setTitle:@"注册" forState:UIControlStateNormal];
    [self.regisiterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.regisiterBtn setBackgroundImage:[UIImage imageNamed:@"lunchBtnBGBlue"] forState:UIControlStateNormal];
    [self.regisiterBtn addTarget:self action:@selector(onRegisiterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.regisiterBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [self addSubview:self.regisiterBtn];
    
    [self.regisiterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@125);
        make.height.equalTo(@44);
        make.left.equalTo(self.mas_left).offset(@15);
        make.bottom.equalTo(self.experienceBtn.mas_top).offset(@(-5));
    }];
    
    
     self.loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    [self.loginBtn setTitleColor:[UIColor colorWithHexString:@"4ea0eb"] forState:UIControlStateNormal];
    [self.loginBtn setBackgroundImage:[UIImage imageNamed:@"lunchBtnBGWhite"] forState:UIControlStateNormal];
    [self.loginBtn addTarget:self action:@selector(onLoginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.loginBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [self addSubview:self.loginBtn];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@125);
        make.height.equalTo(@44);
        make.right.equalTo(self.mas_right).offset(@(-15));
        make.bottom.equalTo(self.experienceBtn.mas_top).offset(@(-5));
    }];
    
    
    self.pageControl = [[UIPageControl alloc] init];
    self.pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"e6e6e6"];
    self.pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"6bcdfd"];
    [self addSubview:self.pageControl];
    
    [self.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.mas_width);
        make.left.equalTo(self.mas_left);
        make.height.equalTo(@2);
        make.bottom.equalTo(self.loginBtn.mas_top).offset(@(-15));
    }];
    
    self.pageControl.numberOfPages = 4;
    self.scrollView.contentSize = CGSizeMake(self.frame.size.width*4, self.scrollView.frame.size.height);
    
    //This is the starting point of the ScrollView
    CGPoint scrollPoint = CGPointMake(0, 0);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
}

- (void)onExperienceButtonPressed:(id)sender{
    
    [self.delegate experienceButtonPressed];
}

- (void)onRegisiterButtonPressed:(id)sender{
    
    [self.delegate resgisterButtonPressed];
}

- (void)onLoginButtonPressed:(id)sender{
    
    [self.delegate loginButtonPressed];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = CGRectGetWidth(self.bounds);
    CGFloat pageFraction = self.scrollView.contentOffset.x / pageWidth;
    self.pageControl.currentPage = roundf(pageFraction);
    
}


@end// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com