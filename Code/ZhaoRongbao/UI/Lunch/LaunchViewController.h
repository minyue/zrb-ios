//
//  LaunchViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/28.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  欢迎页

#import <UIKit/UIKit.h>
@class LaunchViewController;
@protocol LaunchViewControllerDelegate <NSObject>

- (void)launchViewControllerShouldDismiss:(LaunchViewController*)controller andBtnClick:(int)i;

@end

@interface LaunchViewController : UIViewController

@property (nonatomic,assign) id<LaunchViewControllerDelegate>   delegate;


@end
