//
//  LaunchViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/28.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "LaunchViewController.h"
#import "LazyFadeInView.h"

@interface LaunchViewController ()<UIScrollViewDelegate>
@property (nonatomic,strong) IBOutlet UIScrollView * scrollView;
@property (nonatomic,strong) IBOutlet UIView * view1;
@property (nonatomic,strong) IBOutlet UIView * view2;
@property (nonatomic,strong) IBOutlet UIView * view3;
@property (nonatomic,strong) IBOutlet UIView * view4;
@property (nonatomic,strong) IBOutlet UIImageView * imgZi1;
@property (nonatomic,strong) IBOutlet UIImageView * imgZi2;
@property (nonatomic,strong) IBOutlet UIImageView * imgZi3;
@property (nonatomic,strong) IBOutlet UIImageView * imgZi4;

@property (strong, nonatomic) IBOutlet LazyFadeInView *fadeInViewBigTitle1;
@property (strong, nonatomic) IBOutlet LazyFadeInView *fadeInViewSmallTitle1;
@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setup];
}

- (void)setup{

//    NSLog(@"SCREEN_HEIGHT:%.0f",SCREEN_HEIGHT);
    if(SCREEN_HEIGHT < 569){
        int offset = 0;
        if(SCREEN_HEIGHT == 480)
            offset = -165;
        if(SCREEN_HEIGHT == 568)
            offset = -200;
        //5S 以及更短的屏
        [_imgZi1 makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_view1.mas_bottom).offset(offset);
        }];
        [_imgZi2 makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_view2.mas_bottom).offset(offset);
        }];
        [_imgZi3 makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_view3.mas_bottom).offset(offset);
        }];
        [_imgZi4 makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_view4.mas_bottom).offset(offset);
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)toRgister:(id)sender{
    if([self.delegate respondsToSelector:@selector(launchViewControllerShouldDismiss:andBtnClick:)]){
        [self.delegate launchViewControllerShouldDismiss:self andBtnClick:0];
    }
}

-(IBAction)toLogin:(id)sender{
    if([self.delegate respondsToSelector:@selector(launchViewControllerShouldDismiss:andBtnClick:)]){
        [self.delegate launchViewControllerShouldDismiss:self andBtnClick:1];
    }
}

-(IBAction)toDismiss:(id)sender{
    if([self.delegate respondsToSelector:@selector(launchViewControllerShouldDismiss:andBtnClick:)]){
        [self.delegate launchViewControllerShouldDismiss:self andBtnClick:2];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
