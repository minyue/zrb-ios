//
//  LoadingViewController.h
//  TChat
//
//  Created by ZouLi on 14-9-25.
//  Copyright (c) 2014年 Sinosun Technology Co., Ltd. All rights reserved.
//  启动页

#import <UIKit/UIKit.h>

@class LoadingViewController;
@protocol LoadingViewControllerDelegate <NSObject>

- (void)loadingControllerShouldDismiss:(LoadingViewController *)controller withAnimate:(BOOL)animate;

@end

@interface LoadingViewController : UIViewController

@property (nonatomic,assign) id<LoadingViewControllerDelegate>  delegate;
@property (nonatomic,assign) BOOL animate;
@end
