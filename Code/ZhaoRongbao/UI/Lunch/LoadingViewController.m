//
//  LoadingViewController.m
//  TChat
//
//  Created by ZouLi on 14-9-25.
//  Copyright (c) 2014年 Sinosun Technology Co., Ltd. All rights reserved.
//

#import "LoadingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SSGlobalConfig.h"
#import "RegisterService.h"

@interface LoadingViewController ()
{

}
@property (nonatomic, strong) NSTimer               *timer;
@end

@implementation LoadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
    }

    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_timer invalidate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self autoLogin];
}

-(void)loadingController
{
    [self.delegate loadingControllerShouldDismiss:self withAnimate:_animate];
}

//判断是否自动登录
- (void)autoLogin{
    //取出已存在的账号密码
    NSString *tel = (NSString*)[[SSGlobalConfig defaultConfig] getParam:@"PHONENUMBER"];
    NSString *pwd = [(NSString*)[[SSGlobalConfig defaultConfig] getParam:@"PASSWORD"] decodeBase64];
    if(!tel || !pwd || [pwd isEqualToString:@""] || [tel isEqualToString:@""]){
         _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(loadingController) userInfo:nil repeats:NO];
        return;
    }
    RegisterService *_service = [[RegisterService alloc] init];
    [_service loginWithName:tel passWord:pwd success:^(id responseObject) {
        LoginModel *logmodel = responseObject;
        if (logmodel.res == ZRBHttpSuccssType) {
            logmodel.phoneNum = tel;
            logmodel.userPwd = pwd;
            //登录成功
            [ZRB_UserManager loginSuccssWithModel:logmodel withBlock:nil];
        }
        [self loadingController];
    } failure:^(NSError *error) {
        [self loadingController];
    }];
}

@end