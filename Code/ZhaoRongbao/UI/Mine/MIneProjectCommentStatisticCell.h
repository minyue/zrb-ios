//
//  MIneProjectCommentStatisticCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的项目详情-回复统计

#import <UIKit/UIKit.h>
#import "ProjectListCell.h"

static NSString *const mIneProjectCommentStatisticCell = @"MIneProjectCommentStatisticCellIdentifier";

@interface MIneProjectCommentStatisticCell : UITableViewCell


-(void)initWithModel:(ProjectItemModel*)model;
@end
