//
//  MIneProjectCommentStatisticCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MIneProjectCommentStatisticCell.h"

@interface MIneProjectCommentStatisticCell(){
    IBOutlet UILabel *_lblCommentNum;
}
@end

@implementation MIneProjectCommentStatisticCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initWithModel:(ProjectItemModel*)model{
    _lblCommentNum.text = [NSString stringWithFormat:@"回复统计：%d条",model.commentNum];
}

@end
