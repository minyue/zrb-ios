//
//  MineAttentionNewsViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/26.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAttentionNewsViewController.h"
#import "MJRefresh.h"
//#import "NewsService.h"
//#import "NewsListModel.h"
#import "NSDate+Addition.h"
//#import "NewsHomeViewCell.h"
//#import "NewsWebViewController.h"

static NSString *const kNewsHomeViewCellIdetifier = @"NewsHomeViewCellIdetifier";

@interface MineAttentionNewsViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *_tableView;
    
//    NewsService         *_myNewsService;
    
    NSMutableArray      *_listArray;
    
    BOOL       _isRequst;            //是否请求过
}
@end

@implementation MineAttentionNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup
{
    _isRequst = NO;
//    _myNewsService = [[NewsService alloc] init];
    _listArray = [NSMutableArray new];
    [self addHeaderView];
    [self addFooterView];
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
    
        [_tableView.header beginRefreshing];
        
    }
}


#pragma mark -
#pragma mark -网络请求



/**
 *  请求我关注的资讯列表
 */
- (void)queryListData
{
    _isRequst = NO;
    
//    NewsQueryListRequest *req = [[NewsQueryListRequest alloc] init];
//    req.pushTime = [NSDate dateNowFormatString:NSDATE_FORMAT_NORMAL];
//    req.limit = 10;
//    [self startWithReq:req];
    
    
}


/**
 *  加载更多
 */
- (void)loadMoreData
{
//    if(_listArray.count > 0){
//    NewsItemModel *lastObj = [_listArray lastObject];
//    NewsQueryListRequest *req = [[NewsQueryListRequest alloc] init];
//    req.limit = 10;
//    double ttime = [lastObj.collectionTime doubleValue]/1000;
//    req.pushTime = [NSDate dateNowFormatString:NSDATE_FORMAT_NORMAL  andDate:ttime];
//    [self loadMoreWithReq:req];
//    }
}



/**
 *  下拉加载
 *
 *  @param req 请求体
 */

//- (void)startWithReq:(NewsQueryListRequest *)req
//{

//    if (_isRequst) {
//        return;
//    }
//    
//    WS(bself);
//    [_myNewsService myAttentionNewsQueryListWithRequest:req success:^(id responseObject) {
//
//        
//        [bself newsQueryListCallBackWithObject:(NewsListModel *)responseObject];
//        
//    } failure:^(NSError *error)
//     {
//         
//     }];
    
//}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
//- (void)loadMoreWithReq:(NewsQueryListRequest   *)req
//{

//    WS(bself);
//    [_myNewsService myAttentionNewsQueryListWithRequest:req success:^(id responseObject) {
//        
//        [bself loadMoreListCallBackWithObject:(NewsListModel *)responseObject];
//        
//    } failure:^(NSError *error)
//     {
//     }];
    
//}




//- (void)newsQueryListCallBackWithObject:(NewsListModel *)model
//{
//    //请求完毕的标志
//    _isRequst = YES;
//    
//    switch (model.res)
//    {
//        case ZRBHttpSuccssType:
//        {
//            _listArray = model.data;
//            [_tableView reloadData];
//            break;
//        }
//        case ZRBHttpFailType:
//        {
//            
//            break;
//        }
//        case ZRBHttpNoLoginType:
//        {
//            break;
//        }
//        default:
//            break;
//    }
//    
//    [_tableView.header endRefreshing];
    
    
//}
//
//
//
//- (void)loadMoreListCallBackWithObject:(NewsListModel *)model
//{
//    switch (model.res)
//    {
//        case ZRBHttpSuccssType:
//        {
//            [_listArray addObjectsFromArray:model.data];
//            [_tableView reloadData];
//            
//            if (model.data.count == 0) {
//                [_tableView.footer setState:MJRefreshStateNoMoreData];
//            }else
//            {
//                [_tableView.footer endRefreshing];
//                
//            }
//            break;
//        }
//        case ZRBHttpFailType:
//        {
//            [_tableView.header endRefreshing];
//            
//            break;
//        }
//        case ZRBHttpNoLoginType:
//        {
//            [_tableView.header endRefreshing];
//            
//            break;
//        }
//        default:
//            break;
//    }
//}

#pragma mark -
#pragma mark - 列表相关方法


//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return _listArray.count;
//    
//}
//
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
////    NewsHomeViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kNewsHomeViewCellIdetifier forIndexPath:indexPath];
////    
////    NewsItemModel *model = _listArray[indexPath.row];
////    [cell initWithModel:model andIfNeedBtn:NO];
////    cell.tagsView.hidden = YES;
////    return cell;
//}
//
//
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    return 110;
//}
//
//
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
////    NewsItemModel *model = _listArray[indexPath.row];
////    NewsWebViewController *webViewController = (NewsWebViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"News" class:[NewsWebViewController class]];
////    webViewController.newsID = model.id ;
////    webViewController.hidesBottomBarWhenPushed = YES;
////    [self.navigationController pushViewController:webViewController animated:YES];
//}
//


@end
