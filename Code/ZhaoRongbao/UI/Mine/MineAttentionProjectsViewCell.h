//
//  MineAttentionProjectsViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/27.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  关注项目 - 列表 - cell / 我的项目 - 列表 - cell

#import <UIKit/UIKit.h>
#import "ProjectListModel.h"
#import "MineProjectListModel.h"

static NSString *const kAttentionProjectViewCellIdetifier = @"attentionProjectViewCellIdetifier";

@interface MineAttentionProjectsViewCell : UITableViewCell

//关注项目
- (void)initWithModel:(ProjectItemModel*)model;

//我的项目
- (void)initMineWithModel:(ProjectItemModel*)model;

//我的项目列表
- (void)initMineProjectWithModel:(MineProjectListModel*)model;
@end
