//
//  MineAttentionProjectsViewCell.m
//  ZhaoRongbao
//
//  Created by  on 15/8/27.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAttentionProjectsViewCell.h"
#import "ZRB_LineView.h"

@interface MineAttentionProjectsViewCell(){
    IBOutlet  UIImageView    *_imageView;
    
    IBOutlet  UILabel        *_titleLabel;
    
    IBOutlet  UILabel        *_secondTitleLabel;
    
    IBOutlet UILabel         *_lblTime;
    
    IBOutlet UILabel         *_lblTypeAndInvestment;

    ZRB_LineView            *_lineView;
}

@end

@implementation MineAttentionProjectsViewCell

- (void)awakeFromNib {
    // Initialization code
    [self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setup
{
//    self.backgroundColor  = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _imageView.contentScaleFactor = [[UIScreen mainScreen] scale];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    _imageView.backgroundColor = [UIColor colorWithHexString:@"e4e4e4"];
    
    _lineView = [[ZRB_LineView alloc] init];
    [self.contentView addSubview:_lineView];
    
    UIView *superView = self.contentView;
    [_lineView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.leading.equalTo(superView.mas_leading).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.height.equalTo(@0.5);
    }];
}

- (void)initWithModel:(ProjectItemModel *)model{
    //大图和资产类型图
    [_imageView sd_setImageWithURL:[ZRBUtilities urlWithPath:model.pic] placeholderImage:[ZRBUtilities ZRB_DefaultImage] options:SDWebImageRetryFailed];
    
    //标题
    [_titleLabel setText:[NSString stringWithFormat:@"%@",model.projectName]];
    if(model.intro)
        [_secondTitleLabel setText:model.intro];
    
    //类型 //估值
    NSDictionary *dic = @{NSForegroundColorAttributeName:RGB(83, 158, 223)};
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"估值:%@万",model.investmentStr] attributes:dic];
    NSAttributedString *itype = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ | ",[self strWithProjectType:model.projectType]]];
    [aStr insertAttributedString:itype atIndex:0];
    _lblTypeAndInvestment.attributedText = aStr;

    //日期
    _lblTime.text = [ZRBUtilities stringToData:@"MM/dd" interval:[NSString stringWithFormat:@"%@",model.pushTime]];
    
}



//资产类型
- (NSString *)strWithProjectType:(int)projectType
{
    switch (projectType) {
        case 101:
            
            return @"土地";
            break;
        case 102:
            return @"PPP";
            break;
        case 103:
            return @"资产";
            break;
        default:
            return nil;
            break;
    }
}

- (void)initMineWithModel:(ProjectItemModel*)model{
    //大图和资产类型图
    [_imageView sd_setImageWithURL:[ZRBUtilities urlWithPath:model.pic] placeholderImage:[ZRBUtilities ZRB_DefaultImage] options:SDWebImageRetryFailed];
    
    //标题
    [_titleLabel setText:[NSString stringWithFormat:@"%@",model.projectName]];
    if(model.intro)
        [_secondTitleLabel setText:model.intro];
    //日期
    _lblTime.text = [ZRBUtilities stringToData:@"MM/dd" interval:[NSString stringWithFormat:@"%@",model.pushTime]];
    
    //右下角内容 - 关注 评论 勘察
    _lblTypeAndInvestment.text = [NSString stringWithFormat:@"关注%d人 评论%d人 勘察%d人",model.collectionNum,model.commentNum,model.inspectNum];

}


- (void)initMineProjectWithModel:(MineProjectListModel*)model{
    //大图和资产类型图
    [_imageView sd_setImageWithURL:[ZRBUtilities urlWithPath:model.pic] placeholderImage:[ZRBUtilities ZRB_DefaultImage] options:SDWebImageRetryFailed];
    
    //标题
    [_titleLabel setText:[NSString stringWithFormat:@"%@",model.title]];
//    //副标题
//    if(model.intro)
//        [_secondTitleLabel setText:model.intro];
    //日期
    _lblTime.text = [ZRBUtilities stringToData:@"MM/dd" interval:[NSString stringWithFormat:@"%@",model.pushTime]];
    
    //右下角内容 - 关注 咨询
    _lblTypeAndInvestment.text = [NSString stringWithFormat:@"关注%d人 咨询%d人",model.collectNum,model.queryNum];
}
@end
