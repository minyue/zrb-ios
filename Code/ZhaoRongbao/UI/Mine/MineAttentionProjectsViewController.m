//
//  MineAttentionProjectsViewController.m
//  ZhaoRongbao
//
//  Created by  on 15/8/27.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAttentionProjectsViewController.h"
#import "MJRefresh.h"
#import "ProjectService.h"
#import "MineAttentionProjectsViewCell.h"
#import "ProjectDetailViewController.h"


@interface MineAttentionProjectsViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *_tableView;
    
    ProjectService         *_projectService;
    
    NSMutableArray      *_listArray;
    
    BOOL       _isRequst;            //是否请求过
}

@end

@implementation MineAttentionProjectsViewController





- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup
{
    _isRequst = NO;
    _projectService = [[ProjectService alloc] init];
    _listArray = [NSMutableArray new];
    [self addHeaderView];
    [self addFooterView];
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}


#pragma mark -
#pragma mark -网络请求



/**
 *  请求我关注的项目列表
 */
- (void)queryListData
{
    _isRequst = NO;
    
    ProjectQueryListRequest *request = [[ProjectQueryListRequest alloc]init];
    request.limit = 10;
    request.pushTime = @"";
    
    [self startWithReq:request];
    
}


/**
 *  加载更多
 */
- (void)loadMoreData
{
    if(_listArray.count > 0){
        
        ProjectItemModel *lastModel = [_listArray lastObject];
        LogInfo([NSString stringWithFormat:@"%@",lastModel.pushTime]);
        ProjectQueryListRequest *request = [[ProjectQueryListRequest alloc]init];
        request.limit = 10;
        request.pushTime = [ZRBUtilities stringToData:@"yyyy-MM-dd HH:mm:ss" interval:lastModel.collectionTime];
        LogInfo(request.pushTime);
        
        [self loadMoreWithReq:request];
    }
}


/**
 *  下拉加载
 *
 *  @param req 请求体
 */

- (void)startWithReq:(ProjectQueryListRequest *)req
{
    
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_projectService myAttentionProjectsQueryListWithRequest:req success:^(id responseObject) {
        [bself projectQueryListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
    
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(ProjectQueryListRequest   *)req
{
    
    WS(bself);
    [_projectService myAttentionProjectsQueryListWithRequest:req success:^(id responseObject) {
        [bself loadMoreListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
    
}




- (void)projectQueryListCallBackWithObject:(NSMutableArray *)modelArray
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    if(modelArray){
        _listArray = modelArray;
        [_tableView reloadData];
    }
    
    [_tableView.header endRefreshing];
    
}


- (void)loadMoreListCallBackWithObject:(NSMutableArray *)modelArray
{
    [_tableView.header endRefreshing];
    
    [_tableView.footer endRefreshing];
    if(modelArray){
        [_listArray addObjectsFromArray:modelArray];
        [_tableView reloadData];
    }else{
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }

}

#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineAttentionProjectsViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kAttentionProjectViewCellIdetifier forIndexPath:indexPath];
    ProjectItemModel *model = _listArray[indexPath.row];
    [cell initWithModel:model];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 110;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProjectItemModel *model = [[ProjectItemModel alloc]init];
    model = _listArray[indexPath.row];
        
    ProjectDetailViewController *detailCtrl = [[ProjectDetailViewController alloc]init];
    detailCtrl.projectID = model.autoId;
    detailCtrl.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailCtrl animated:YES];
}


@end
