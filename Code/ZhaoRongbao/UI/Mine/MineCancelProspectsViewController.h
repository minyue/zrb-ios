//
//  MineCancelProspectsViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/8.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  取消勘察页面

#import "ZRB_ViewController.h"

@interface MineCancelProspectsViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic,copy) NSString *prospectId;
@end
