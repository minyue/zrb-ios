//
//  MineCancelProspectsViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/8.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineCancelProspectsViewController.h"
#import "QRadioButton.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "ProjectService.h"
#import "ZRB_NormalModel.h"
#import "AppDelegate.h"

#define TextMaxLength 30
#define textPlaceHodel @"说点什么..."
@interface MineCancelProspectsViewController ()<QRadioButtonDelegate>
{
    CustomBarItem *_item;
    int _selectType;

    IBOutlet UIView *_countDownLabel;//倒计数栏
    IBOutlet UILabel *_lblCountDown;//到计数
    IBOutlet UITextView  *_txtFeed;//意见
    
    IBOutlet UIView *_radioView;
    IBOutlet QRadioButton *_btn0;
    IBOutlet QRadioButton *_btn1;
    IBOutlet QRadioButton *_btn2;
    IBOutlet QRadioButton *_btn3;
    
    ProjectService *_service;
}
@end

@implementation MineCancelProspectsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)setup{
    [self setRightItem];
    
    _service = [[ProjectService alloc] init];
    _selectType = -1;
    
    _countDownLabel.layer.cornerRadius = 10;
    _countDownLabel.layer.masksToBounds = YES;
    _lblCountDown.text = @"200";
    
    _btn0 = [[QRadioButton alloc] initWithDelegate:self groupId:@"cancel"];
    _btn0.tag = 0;
    _btn0.delegate = self;
    [_btn0 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];//[UIColor colorWithHexString:@"0077d9"]
    [_btn0 setTitle:@"我不想看了" forState:UIControlStateNormal];
    [_btn0.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [_radioView addSubview:_btn0];
    [_btn0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@95);
        make.height.equalTo(@15);
        make.left.equalTo(_radioView.mas_left).offset(@15);
        make.top.equalTo(_radioView.mas_top).offset(@20);
    }];
    
    
    _btn1 = [[QRadioButton alloc] initWithDelegate:self groupId:@"cancel"];
    _btn1.tag = 1;
    _btn1.delegate = self;
    [_btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];//[UIColor colorWithHexString:@"0077d9"]
    [_btn1 setTitle:@"行程有变" forState:UIControlStateNormal];
    [_btn1.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [_radioView addSubview:_btn1];
    [_btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@75);
        make.height.equalTo(@15);
//        make.left.equalTo(_btn0.mas_right).offset(@2);
        make.top.equalTo(_radioView.mas_top).offset(@20);
    }];
    
    
    _btn2 = [[QRadioButton alloc] initWithDelegate:self groupId:@"cancel"];
    _btn2.tag = 2;
    _btn2.delegate = self;
    [_btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];//[UIColor colorWithHexString:@"0077d9"]
    [_btn2 setTitle:@"信息错误重新提交" forState:UIControlStateNormal];
    [_btn2.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [_radioView addSubview:_btn2];
    [_btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@135);
        make.height.equalTo(@15);
        make.right.equalTo(_radioView.mas_right).offset(@-8);
        make.top.equalTo(_radioView.mas_top).offset(@20);
    }];
    [_btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_btn2.mas_left).offset(-2);
        int i = ( _btn2.frameX - (_btn0.frameX + _btn0.frameWidth))/2;
        make.left.equalTo(_btn0.mas_right).offset(i);
    }];
    
    _btn3 = [[QRadioButton alloc] initWithDelegate:self groupId:@"cancel"];
    _btn3.tag = 3;
    _btn3.delegate = self;
    [_btn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];//[UIColor colorWithHexString:@"0077d9"]
    [_btn3 setTitle:@"其他" forState:UIControlStateNormal];
    [_btn3.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [_radioView addSubview:_btn3];
    [_btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@65);
        make.height.equalTo(@15);
        make.left.equalTo(_radioView.mas_left).offset(@15);
        make.bottom.equalTo(_radioView.mas_bottom).offset(@-15);
    }];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textViewEditChanged:) name:@"UITextViewTextDidChangeNotification"
                                              object:_txtFeed];
    
    _txtFeed.text = textPlaceHodel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setRightItem
{
    NSString *title = @"提交";
    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_item setOffset:-2];
    [_item addTarget:self selector:@selector(submit) event:UIControlEventTouchUpInside];
    _item.contentBarItem.enabled = NO;
}

- (void)submit{
    //提交取消勘察
    if(!_prospectId || _selectType == -1)
        return;
     WS(bself);
    NSMutableDictionary *md = [NSMutableDictionary dictionaryWithObjectsAndKeys:_prospectId,@"inspectId",[self getType],@"reasonType", nil];
    if(_txtFeed.text.length > 0){
        [md setObject:_txtFeed.text forKey:@"reason"];
    }
    [_service cancelProspectWithRequest:md success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [MBProgressHUD showHUDTitle:@"取消勘察成功" onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
            dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 1.5 *NSEC_PER_SEC);
            dispatch_after(afterTime, dispatch_get_main_queue(), ^{
//                [bself.navigationController popViewControllerAnimated:YES];
                [bself.navigationController popToRootViewControllerAnimated:YES];
            });
        }else{
            [self showTipViewWithMsg:@"取消勘察失败"];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"取消勘察失败，请稍后提交"];
    }];
}

- (NSString*)getType{
    switch (_selectType) {
        case 0:
            return @"101"; //我不想看了
            break;
         case 1:
            return @"102"; //行程有变
            break;
        case 2:
            return @"103"; //信息错误重新提交
            break;
        case 3:
            return @"104"; //其它
            break;
        default:
            break;
    }
    return nil;
}

#pragma delegate of RadioButton
- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId{
    if(_item.contentBarItem.enabled == NO)
        _item.contentBarItem.enabled = YES;
    _selectType = (int)radio.tag;
}

-(void)textViewDidChange:(UITextView *)textView
{
    _txtFeed.text =  textView.text;
    
}
#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if([[textView text] isEqualToString:textPlaceHodel]){
        textView.text = @"";
    }
    _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [textView text].length)];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    NSString *new = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(new.length > TextMaxLength){
        
        if (![text isEqualToString:@""]) {
            
            return NO;
        }
    }
    return YES;
}

-(void)textViewEditChanged:(NSNotification *)obj{
    
    UITextField *textField = (UITextField *)obj.object;
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > TextMaxLength) {
                _txtFeed.text = [toBeString substringToIndex:TextMaxLength-1];
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtFeed text].length)];
            }else{
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtFeed text].length)];
            }
        }
        else{
            if(toBeString.length <= TextMaxLength){
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtFeed text].length)];
            }else{
                _txtFeed.text = [toBeString substringToIndex:TextMaxLength-1];
                _lblCountDown.text = @"0";
            }
            
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > TextMaxLength) {
            _txtFeed.text = [toBeString substringToIndex:TextMaxLength];
        }
        _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtFeed text].length)];
    }
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:@"UITextViewTextDidChangeNotification"
                                                 object:_txtFeed];
}
@end
