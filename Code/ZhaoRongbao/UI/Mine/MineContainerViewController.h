//
//  MineContainerViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/21.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  我的主页面容器

#import "ZRB_ViewController.h"
#import "MineHomeViewController.h"
#import "MineViewController.h"

@interface MineContainerViewController : ZRB_ViewController
@property (nonatomic,copy) void (^loginOutBlock)();


@property (nonatomic, strong) MineHomeViewController   *homeVC;

@property (nonatomic, strong) MineViewController       *mineVC;
@property (nonatomic, strong) ZRB_ViewController       *currentVC;

-(void)initv;
/**
 *  展示我的页面（未登录）
 */
- (void)showMineViewController;

/**
 *  展示我的主页(已登录)
 */
- (void)showHomeViewController;


- (void)pushToLogin;

- (void)pushToRegister;
@end
