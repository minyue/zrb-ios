//
//  MineContainerViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/21.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineContainerViewController.h"


#import "UINavigationItem+CustomItem.h"
#import "MineSettingViewController.h"
#import "MineLoginViewcController.h"
@interface MineContainerViewController()
{
    
    
    
}

@end

@implementation MineContainerViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    [self setupRightItem];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setFirstVC];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (void)setup
{
    WS(bself);
    self.navigationItem.title = @"我的主页";
    _mineVC = (MineViewController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineViewController class ]];
    _mineVC.loginSuccssBlock = ^{
        if(_currentVC == _mineVC)
            [bself showHomeViewController];
    };
    [self addChildViewController:_mineVC];
    
    _homeVC = (MineHomeViewController *)[StoryBoardUtilities viewControllerForStoryboardName:kPersonalStoryboardName class:[MineHomeViewController class]];
    [self addChildViewController:_homeVC];
    
}


- (void)setupRightItem
{
    CustomBarItem *item =  [self.navigationItem setItemWithImage:@"registered_setup" size:CGSizeMake(20, 20) itemType:right];
    [item setOffset:-5];
    [item addTarget:self selector:@selector(gotoPersonalCenter:) event:UIControlEventTouchUpInside];
}

- (void)setFirstVC
{
     //modified by zouli 20150827
    if (!_currentVC)
    {
        if ([ZRB_UserManager isLogin]){
            _homeVC.view.frame = self.view.bounds;
            [self.view addSubview:_homeVC.view];
            [_homeVC didMoveToParentViewController:self];
            _currentVC = _homeVC;
        }else{
            _mineVC.view.frame = self.view.bounds;
            [self.view addSubview:_mineVC.view];
            [_mineVC didMoveToParentViewController:self];
            _currentVC = _mineVC;
        }
    }else{
        if ([ZRB_UserManager isLogin] ){
            if(_currentVC == _mineVC){
                [self showHomeViewController];
            }
        }else{
            if(_currentVC == _homeVC){
                [self showMineViewController];
            }
        }
    }
}

/**
 *  展示我的页面（未登录）
 */
- (void)showMineViewController
{
    [self transitionFromViewController:_currentVC toViewController:_mineVC duration:0.4 options:UIViewAnimationOptionLayoutSubviews animations:^{
        
    } completion:^(BOOL finished) {
        _currentVC = _mineVC;
    }];

}

/**
 *  展示我的主页(已登录)
 */
- (void)showHomeViewController
{
    [self transitionFromViewController:_currentVC toViewController:_homeVC duration:0.4 options:UIViewAnimationOptionLayoutSubviews animations:^{
        
    } completion:^(BOOL finished) {
        _currentVC = _homeVC;
    }];
}

/**
 *  进入个人中心
 *
 *  @param sender
 */
- (void)gotoPersonalCenter:(id)sender
{
    WS(bself);
    MineSettingViewController *settingVC = (MineSettingViewController *)[StoryBoardUtilities viewControllerForStoryboardName:kPersonalStoryboardName class:[MineSettingViewController class]];
    settingVC.hidesBottomBarWhenPushed = YES;
    settingVC.logOutBlock = ^{
        if(_currentVC == _homeVC)
            [bself showMineViewController];
    };
    [bself.navigationController pushViewController:settingVC animated:YES];
}

- (void)pushToLogin{

}

- (void)pushToRegister{

}

@end
