//
//  MineEnterpriceCerInfoCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/14.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  查询企业 - Cell

#import <UIKit/UIKit.h>

@interface MineEnterpriceCerInfoCell : UITableViewCell

- (void)initWithData:(NSString*)str;

@end
