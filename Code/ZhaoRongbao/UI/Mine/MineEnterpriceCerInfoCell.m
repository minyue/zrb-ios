//
//  MineEnterpriceCerInfoCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/14.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineEnterpriceCerInfoCell.h"
@interface MineEnterpriceCerInfoCell()
{
    IBOutlet UILabel *_lblName;
}
@end
@implementation MineEnterpriceCerInfoCell

- (void)awakeFromNib {
    // Initialization code
  
    self.layer.borderColor = [UIColor colorWithHexString:@"4997ec"].CGColor;
    //下分割线
    UILabel *lblLine = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frameHeight, self.frameWidth, 0.5)];
    lblLine.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
    [self addSubview:lblLine];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithData:(NSString*)str{
    _lblName.text = str;
}
@end
