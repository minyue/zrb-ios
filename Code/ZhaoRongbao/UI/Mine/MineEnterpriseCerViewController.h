//
//  MineReEnterpriseViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  企业认证

#import "ZRB_ViewController.h"
typedef NS_ENUM(NSInteger, MineEnterpriceCerType){
    /**
     *  注册时填写
     */
    MineEnterpriceCerTypeRegister,
    /**
     *  从个人 - 修改机构认证信息
     */
    MineEnterpriceCerTypePersonal
    
};
@interface MineEnterpriseCerViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, assign) MineEnterpriceCerType thePageType;
@end
