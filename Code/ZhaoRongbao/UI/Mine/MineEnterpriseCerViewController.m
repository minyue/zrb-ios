//
//  MineReEnterpriseViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineEnterpriseCerViewController.h"
#import "TOTextInputChecker.h"
#import "UINavigationItem+CustomItem.h"
#import "MineReSuccessViewController.h"
#import "RegisterService.h"
#import "RegisterModel.h"
#import "MineEnterpriceCerInfoCell.h"
#import "CompanyInfoModel.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"

@interface MineEnterpriseCerViewController ()<ZRB_TextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{

    IBOutlet  ZRB_TextField  *_nameTextFiled;
    
    IBOutlet  UIButton       *_finishButton;      //下一步
    
    IBOutlet  UIView         *_lineView;        //线条
    
    RegisterService          *_service;
 
    IBOutlet UITableView    *_searchTableView;
    
    NSMutableArray          *_arrayInfo;
    
    MineService             *_myservice;
}

@property (nonatomic, copy)  CompanyInfoModel        *selectecCompanyInfo;

- (void)setup;


- (IBAction)finsish:(id)sender;
@end

@implementation MineEnterpriseCerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    if(self.thePageType == MineEnterpriceCerTypeRegister){
        [self setRightItem];
    }else{
        ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
        if(user.org)
            _nameTextFiled.text = user.org;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setup
{
    [_nameTextFiled initWithTitle:@"企业名" placeHolder:@"请输入您的企业名称"];
    _nameTextFiled.delegate = self;
    _finishButton.layer.cornerRadius = (65/2);
    _finishButton.layer.masksToBounds = YES;
    [_finishButton setTitle:@"完成" forState:UIControlStateNormal];
    [_finishButton.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [self nextButtonActive:NO];
    
    _service = [[RegisterService  alloc] init];
    _arrayInfo = [[NSMutableArray alloc] init];
    _searchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _searchTableView.layer.borderWidth = 0.5;//边框宽度
    _searchTableView.separatorInset = UIEdgeInsetsZero;
    _searchTableView.layer.borderColor = [UIColor colorWithHexString:@"4997ec"].CGColor;
    
    _myservice = [[MineService alloc] init];
}



- (void)setRightItem
{
    NSString *title = @"跳过";
    CustomBarItem *item =  [self.navigationItem setItemWithTitle:title textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [item setOffset:-2];
    [item addTarget:self selector:@selector(passEnterpriseCer:) event:UIControlEventTouchUpInside];

}


/**
 *  跳过企业认证
 *
 *  @param sender 企业认证
 */
- (void)passEnterpriseCer:(id)sender
{
    [self saveAppUser];
}


/**
 *  完成注册
 *
 *  @param sender
 */
- (IBAction)finsish:(id)sender
{
    [RegisterRequest shareInstance].org = _nameTextFiled.text;
    if(self.thePageType == MineEnterpriceCerTypeRegister){
        [self saveAppUser];
    }else{
        //更改企业认证信息
        if(_selectecCompanyInfo){
            [self updateUserEnterpriseInfo:_selectecCompanyInfo];
        }else{
            [self updateUserEnterpriseInfo2:_nameTextFiled.text];
        }
    }
}

- (void)gotoReSuccess
{

    MineReSuccessViewController *reSucVC = (MineReSuccessViewController *)[StoryBoardUtilities viewControllerForStoryboardName:kMineStoryboardName class:[MineReSuccessViewController class]];
    [self.navigationController pushViewController:reSucVC animated:YES];
}



/**
 *  设置完成按钮状态
 *
 *  @param isActive 
 */
- (void)nextButtonActive:(BOOL)isActive
{
    if (isActive)
    {
        _finishButton.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        _finishButton.enabled = YES;
        [_finishButton setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        
    }else
    {
        _finishButton.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
        _finishButton.enabled = NO;
        [_finishButton setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    }
}


//注册--保存用户信息
- (void)saveAppUser
{
    WS(bself);
    [_service saveAppUserWithRequest:[RegisterRequest shareInstance] success:^(id responseObject) {
        [SVProgressHUD dismiss];
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        
        if (model.res == ZRBHttpSuccssType) {
            [bself gotoReSuccess];
        }else
        {
            [bself showTipViewWithMsg:model.msg];
        }

    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];

        [bself showTipViewWithMsg:kHttpFailError];

    }];
    
    [SVProgressHUD show];
}

//修改用户企业信息 -- 情况1：数据库中有该企
-(void)updateUserEnterpriseInfo:(CompanyInfoModel*)model{
    
    NSDictionary *dic = @{@"org":[model name],
                          @"orgId":[model uid]};
    
    [self requestService:dic];
}

//修改用户企业信息 -- 情况2：数据库中无该企
-(void)updateUserEnterpriseInfo2:(NSString*) str{
    if(!str)
        return;
    NSDictionary *dic = @{@"org":str};
    
    [self requestService:dic];
}

- (void)requestService:(NSDictionary *)dic{
    WS(bself)
    [_myservice updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改企业信息失败"];
    }];
    
    [SVProgressHUD show];

}


/**
 *  拉取企业信息后更新列表
 *
 *  @param NSString 文本框内容
 *
 *  @return
 */
- (void)getCompanyInfo:(NSString*)str{
    WS(bself);
    [_service checkEnterpriseInfoWithKey:str success:^(NSMutableArray *infolist) {
        [SVProgressHUD dismiss];
        [_searchTableView setHidden:NO];
        if(!infolist || infolist.count == 0){
            [_arrayInfo removeAllObjects];
            _searchTableView.frameHeight = _arrayInfo.count*44;
            [_searchTableView reloadData];
            return ;
        }
        _arrayInfo = infolist;
        _searchTableView.frameHeight = _arrayInfo.count*44;
        [_searchTableView reloadData];
        
        
    } failure:^(NSError *error) {
     
        [SVProgressHUD dismiss];
        
        [bself showTipViewWithMsg:kHttpFailError];
        
    }];
    
    [SVProgressHUD show];
}

#pragma mark - UItextFieldDelegate
- (void)ZRBtextFieldDidBeginEditing:(UITextField *)textField{
    if(textField.text.length < 1){
        [self nextButtonActive:NO];
    }else{
        [self nextButtonActive:YES];
    }
}

- (BOOL)ZRBtextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
     long int length = textField.text.length - range.length + string.length;
    
    if(length > 0){
        [self nextButtonActive:YES];
        //得到输入框的内容
        NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        //请求查询
        [self getCompanyInfo:toBeString];
    }else{
        [self nextButtonActive:NO];
    }
    return YES;
}


#pragma mark -
#pragma mark - 列表方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrayInfo.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MineEnterpriceCerInfoCellIde";
    
    MineEnterpriceCerInfoCell *cell = (MineEnterpriceCerInfoCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil){
        cell = (MineEnterpriceCerInfoCell *)[[[NSBundle mainBundle] loadNibNamed:@"MineEnterpriceCerInfoCell" owner:self options:nil] lastObject];
    }
    CompanyInfoModel *comModel = (CompanyInfoModel*)[_arrayInfo objectAtIndex:indexPath.row];
    
    [cell initWithData:comModel.name];
    return cell;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectecCompanyInfo = (CompanyInfoModel*)[_arrayInfo objectAtIndex:indexPath.row];
    _nameTextFiled.text = _selectecCompanyInfo.name;
    
    [_arrayInfo removeAllObjects];
    [_searchTableView reloadData];
    [_searchTableView setHidden:YES];
    
    [self nextButtonActive:YES];
    NSLog(@"_selectecCompanyInfo:%@",_selectecCompanyInfo);
    
}

@end
