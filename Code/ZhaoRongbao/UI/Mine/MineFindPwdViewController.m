//
//  MineFindPwdViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/21.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineFindPwdViewController.h"
#import "TOTextInputChecker.h"
#import "RegisterService.h"
#import "UIButton+ZRB.h"
#import "RegisterModel.h"
#import "MineSetupPwdViewController.h"
#import "UINavigationItem+CustomItem.h"
#import "MineRegisterViewController.h"

@interface MineFindPwdViewController()
{
    IBOutlet  ZRB_TextField  *_phoneTextFiled;
    
    IBOutlet  UIButton       *_sendButton;
    
    TOTextInputChecker        *_phoneChecker;   //电话号码检查器
    
    RegisterService             *_service;

}

- (void)setup;

- (IBAction)send:(id)sender;

@end

@implementation MineFindPwdViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup ];
    [self setupRightItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)setup
{
    
    WS(bself);
    _phoneChecker = [TOTextInputChecker telChecker:YES];
    _phoneChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself checkState];
    };
    [_phoneTextFiled initWithTitle:@"手机号" placeHolder:@"请输入您的手机号"];
    _phoneTextFiled.delegate = _phoneChecker;
    
    [_sendButton configButtonWithType:ZRB_ButtonTypeOperation];
    [self sendButtonState:NO];
    
    _service = [[RegisterService alloc] init];

}



- (void)setupRightItem
{
    NSString *title = @"注册";
    CustomBarItem * _rightItem =  [self.navigationItem setItemWithTitle:title textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_rightItem setOffset:-2];
    [_rightItem addTarget:self selector:@selector(resister:) event:UIControlEventTouchUpInside];
}

- (void)resister:(id)sender
{
    MineRegisterViewController *reViewController = (MineRegisterViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterViewController class]];
    [self.navigationController pushViewController:reViewController animated:YES];
}


- (void)sendButtonState:(BOOL)state
{
    if (state) {
        
        _sendButton.enabled = YES;
        [_sendButton setBackgroundColor:[UIColor colorWithHexString:@"4997ec"] forState:UIControlStateNormal];
        [_sendButton setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
    }else
    {
        _sendButton.enabled = NO;
        [_sendButton setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"] forState:UIControlStateDisabled];
        [_sendButton setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
    }
}

- (void)checkState
{
    if ([_phoneChecker finalCheck] == InputCheckErrorNone )
    {
        [self sendButtonState:YES];
    }else
    {
        [self sendButtonState:NO];
    }
}



- (IBAction)send:(id)sender
{

    [self.view endEditing:YES];
    [_service sendSMSFindPWWithMobile:_phoneTextFiled.text success:^(id responseObject) {
        [SVProgressHUD dismiss];
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        if (model.res == ZRBHttpSuccssType)
        {
            //进入下一步
            [self gotoNextWithMobile:_phoneTextFiled.text];
            
        }else
        {
            [self showTipViewWithMsg:model.msg];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:kHttpFailError];

    }];
    
    [SVProgressHUD show];
}



/**
 *  进入下一步
 *
 *  @param mobile 手机号码
 */
- (void)gotoNextWithMobile:(NSString *)mobile
{
    MineSetupPwdViewController    *pwdVC = (MineSetupPwdViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineSetupPwdViewController class]];
    pwdVC.mobile = mobile;
    pwdVC.pageType = 0;
    [self.navigationController pushViewController:pwdVC animated:YES];

}
@end
