//
//  MineHomeViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/27.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineHomeViewController.h"
#import "MineHomeTopCell.h"
#import "MineHomeContentCell.h"
#import "MineNumStatisticCell.h"
#import "ZRB_UserManager.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"
#import "MineProjectsViewController.h"
#import "MineProspectsViewController.h"
#import "MineAllPublishViewController.h"
#import "MineAttentionHomeViewController.h"
#import "MineMoneyListViewController.h"
#import "ChatSystemNotificationViewController.h"
#import "MinePersonalCenterViewController.h"
#import "MineMembersServiceViewController.h"
#import "MineMemberOfCertificationViewController.h"
#define HEAD_HEIGHT             50
static NSString *const kHomeTopCellIdentifier = @"MineHomeTopCellIdentifier";
static NSString *const kHomeContentCellIdentifier = @"HomeContentCellIdentifier";
static NSString *const kHomePaddingCellIdentifier = @"HomePaddingCellIdentifier";
static NSInteger const kHomeTopHeight = 130;
static NSInteger const kHomeContentHeight = 44;

@interface MineHomeViewController ()<UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{

    IBOutlet   UITableView  *_tableView;
    
    NSArray    *_dataArray;
    
    MineService *_service;
}

- (void)getNumStatisticData;
@end

@implementation MineHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([ZRB_UserManager isLogin])
        [self getNumStatisticData];
    [self modifyUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)setup
{
//    _tableView.frame = CGRectMake(0, HEAD_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 64- 49 - HEAD_HEIGHT);
    _tableView.backgroundColor = [UIColor clearColor];
    
    _dataArray = @[[HomeContentModel modelWithType:MineHomeContentTypeOne],
                   [HomeContentModel modelWithType:MineHomeContentTypeSeven],
                   [HomeContentModel modelWithType:MineHomeContentTypeTwo],
                   [HomeContentModel modelWithType:MineHomeContentTypeThree],
                   [HomeContentModel modelWithType:MineHomeContentTypeFour],
                   [HomeContentModel modelWithType:MineHomeContentTypeFive],
                   [HomeContentModel modelWithType:MineHomeContentTypeSix]];
    
    
    _service = [[MineService alloc] init];
}

- (void)modifyUI{
    //调整TableView的Frame
    _tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 114);
    
}

- (void)getNumStatisticData{
    //调用关注次数方法
    [_service getCurrentUserNumStatisticWithsuccess:^(id responseObject) {
//        [SVProgressHUD dismiss];
        if(responseObject){
            ZRB_UserModel *mo = (ZRB_UserModel*)responseObject;
            [ZRB_UserManager shareUserManager].userData.collectedNumber = mo.collectedNumber;
            [ZRB_UserManager shareUserManager].userData.collectFund = mo.collectFund;
            [ZRB_UserManager shareUserManager].userData.collectProject = mo.collectProject;
            [_tableView reloadData];
        }
    } failure:^{
//        [SVProgressHUD dismiss];
    }];
    
//    [SVProgressHUD show];
}


- (void)takePictureClicked:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相机拍摄",@"相册库", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}


#pragma mark - UIActionSheet Delegate
/**
 *  跳出相机或相册
 *
 *  @param actionSheet 下标
 *  @param buttonIndex 按钮下标
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIImagePickerControllerSourceType sourceType;
    if (buttonIndex == 0) {
        //先设定sourceType为相机，然后判断相机是否可用，不可用将sourceType设定为相片库
        sourceType = UIImagePickerControllerSourceTypeCamera;
        if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        picker.navigationBar.barTintColor = [UIColor colorWithHexString:@"0077d9"];
        picker.navigationBar.translucent = NO;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else if (buttonIndex == 1){
        //相册
        sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        picker.navigationBar.barTintColor = [UIColor colorWithHexString:@"0077d9"];
        picker.navigationBar.translucent = NO;
        [self presentViewController:picker animated:YES completion:nil];
    }
}


#pragma mark - Camera View Delegate Methods
/**
 *  拍照完成或相册选择完成
 *
 *  @param picker 相机或相册
 *  @param info   照片
 */
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    NSString *path = [ZRBUtilities ZRBScaleAndSaveImage:image];
    NSLog(@"path:%@",path);
    if(path){
        [self upLoadUserHead:path];
    }else{
        [self showTipViewWithMsg:@"保存失败"];
    }
}

/**
 *  在相机或相册 点了取消
 *
 *  @param picker
 */
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

///**
// *  调整选择的照片质量，并写入临时目录下
// *
// *  @param image    选择的图片
// *  @param fullPath 写入的路径
// */
//- (void)scaleAndSaveImage:(UIImage *)image andFilePath:(NSString *)fullPath
//{
//    //调整图片方向
//    float quality = 0.5;
//    NSData *imageData = UIImageJPEGRepresentation(image, quality);
//    [imageData writeToFile:fullPath atomically:NO];//写入到缓存目录
//}


/**
 *  上传头像文件
 */
- (void)upLoadUserHead:(NSString*)path {
    // 上传用户头像
    WS(bself)
    [_service fileUpload:path success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        
        [bself uploadImageCallBackWithModel:(ZRB_NormalModel *)responseObject andPath:(NSString*)path];

    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
    }];
    
    [SVProgressHUD show];
}


- (void)uploadImageCallBackWithModel:(ZRB_NormalModel *)model andPath:(NSString*)path
{
    if (model.res == ZRBHttpSuccssType) {
        
        MineHomeTopCell *cell = (MineHomeTopCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell.iconView setImage:[UIImage imageWithContentsOfFile:path]];
    }
}


#pragma mark - 
#pragma mark - 列表方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 6;
//    return 5;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 3)
        return 5;
    if(section == 5)
        return 2;
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //头部
    if (indexPath.section == 0)
    {
        MineHomeTopCell *cell = [_tableView dequeueReusableCellWithIdentifier:kHomeTopCellIdentifier forIndexPath:indexPath];
        cell.uploadImageBlock = ^{
//           [self takePictureClicked:nil];
            //跳转个人中心
            MinePersonalCenterViewController *eVC = (MinePersonalCenterViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalCenterViewController class]];
            [self.navigationController pushViewController:eVC animated:YES];
        };
        [cell   initWihtModel:[ZRB_UserManager shareUserManager].userData];
        
        return cell;

    }else if (indexPath.section == 1)//关注次数
    {
        MineNumStatisticCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineNumStatisticCellIdentifier forIndexPath:indexPath];
        [cell initWithModel:[ZRB_UserManager shareUserManager].userData];
        return cell;
        
    }
    else if (indexPath.section == 3)
    {
        MineHomeContentCell *cell = [_tableView dequeueReusableCellWithIdentifier:kHomeContentCellIdentifier forIndexPath:indexPath];
        HomeContentModel   *model = [_dataArray objectAtIndex:indexPath.row];
        model.indexPath = indexPath;
        [cell initWithModel:model];
        return cell;
    
    }else if(indexPath.section == 5) //认证介绍，会员服务
    {
    
        MineHomeContentCell *cell = [_tableView dequeueReusableCellWithIdentifier:kHomeContentCellIdentifier forIndexPath:indexPath];
        HomeContentModel *model = [_dataArray  objectAtIndex:(indexPath.row+5)];
        model.indexPath = indexPath;
        [cell initWithModel:model];
        return cell;

    }else           //空隙
    {
       UITableViewCell *cell =  [_tableView dequeueReusableCellWithIdentifier:kHomePaddingCellIdentifier forIndexPath:indexPath];
        return cell;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return kHomeTopHeight;
        case 1:
            return 56;
        case 2:
            return 15;      //空隙
        case 3:
            return kHomeContentHeight;
        case 4:
            return 15;      //空隙
        case 5:
            return kHomeContentHeight;
        default:
            return 0;
            break;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3 )
    {
        HomeContentModel *model = [_dataArray objectAtIndex:indexPath.row];
        if(indexPath.row == 0){
            //我的关注
            MineAttentionHomeViewController *attentionViewController = (MineAttentionHomeViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAttentionHomeViewController class]];
            attentionViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:attentionViewController animated:YES];
        }else if(indexPath.row == 1){
            //我的发布
            MineAllPublishViewController *mineAllPublishViewController = (MineAllPublishViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAllPublishViewController class]];
            mineAllPublishViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:mineAllPublishViewController animated:YES];
        }else if(indexPath.row == 2){
            //我的项目
            MineProjectsViewController *mineProjectsViewController = (MineProjectsViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineProjectsViewController class]];
            mineProjectsViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:mineProjectsViewController animated:YES];
        }else if(indexPath.row == 3){
            //我的资金
            MineMoneyListViewController *mineMoneyListViewController = (MineMoneyListViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineMoneyListViewController class]];
            mineMoneyListViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:mineMoneyListViewController animated:YES];
        }else if(indexPath.row == 4){
            //我的消息
            ChatSystemNotificationViewController *chatSystemNotificationViewController = (ChatSystemNotificationViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Chat" class:[ChatSystemNotificationViewController class]];
            chatSystemNotificationViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:chatSystemNotificationViewController animated:YES];
        }
        else{
            //关注项目
//            MineAttentionProjectsViewController *attentionViewController = (MineAttentionProjectsViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAttentionProjectsViewController class]];
//            attentionViewController.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:attentionViewController animated:YES];
        }
        
        
    }else if (indexPath.section == 5)
    {
        if(indexPath.row ==0){
            //认证介绍
            MineMemberOfCertificationViewController *mineMemberOfCertificationViewController = (MineMemberOfCertificationViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineMemberOfCertificationViewController class]];
            mineMemberOfCertificationViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:mineMemberOfCertificationViewController animated:YES];
        }else{
           //会员服务
            MineMembersServiceViewController *mineMembersServiceViewController = (MineMembersServiceViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineMembersServiceViewController class]];
            mineMembersServiceViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:mineMembersServiceViewController animated:YES];
            
        }
    }
}


@end
