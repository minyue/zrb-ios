//
//  MineIdentityViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineIdentityViewController.h"
#import "MineRegisterViewController.h"
#import "RegisterService.h"
@interface MineIdentityViewController ()
{
    IBOutlet    UIButton   *_servantsButton;       //公务员
    
    IBOutlet    UIButton   *_investorButton;       //投资人

    IdentityType           _identiyType;           //当前选择的类型
    
    IBOutlet    UIView     *_topBackView;
}

- (void)setup;


- (IBAction)showServants:(id)sender;


- (IBAction)showInvestor:(id)sender;

@end

@implementation MineIdentityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    [self styleWithType:IdentityTypeServants];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup
{
    _servantsButton.layer.cornerRadius = 18.0f;
    _servantsButton.layer.masksToBounds = YES;
    _servantsButton.layer.borderColor = [UIColor colorWithHexString:@"4997ec"].CGColor;
    _servantsButton.layer.borderWidth = 1.0f;
    

    _investorButton.layer.cornerRadius = 18.0f;
    _investorButton.layer.masksToBounds = YES;
    _investorButton.layer.borderColor = [UIColor colorWithHexString:@"4997ec"].CGColor;
    _investorButton.layer.borderWidth = 1.0f;

    NSLayoutConstraint *con = [_topBackView findOwnConstraintForAttribute:NSLayoutAttributeWidth];
    con.constant = UIScreenWidth;
    
    
    if(SCREEN_HEIGHT < 667){
        //iphone6以下
        [_servantsButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(100);
        }];
    }else{
        [_servantsButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(145);
        }];
    }
}



- (void)styleWithType:(IdentityType)type
{

    _identiyType = type;
    
    switch (type) {
        case IdentityTypeServants:
        {
            [_servantsButton setBackgroundColor:[UIColor colorWithHexString:@"4997ec"]];
            [_servantsButton setTitleColor:[UIColor whiteColor]];
            
            [_investorButton setBackgroundColor:[UIColor whiteColor]];
            [_investorButton setTitleColor:[UIColor colorWithHexString:@"4997ec"]];
            break;

        }
        case IdentityTypeInvestor:
        {
            
            [_investorButton setBackgroundColor:[UIColor colorWithHexString:@"4997ec"]];
            [_investorButton setTitleColor:[UIColor whiteColor]];
            
            [_servantsButton setBackgroundColor:[UIColor whiteColor]];
            [_servantsButton setTitleColor:[UIColor colorWithHexString:@"4997ec"]];

            break;
        }
        default:
            break;
    }
}


/**
 *  政府入口
 *
 *  @param sender
 */
- (IBAction)showServants:(id)sender
{
    [RegisterRequest shareInstance].userType = IdentityTypeServants;
    [self styleWithType:IdentityTypeServants];
    MineRegisterViewController *reViewController = (MineRegisterViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterViewController class]];
    [self.navigationController pushViewController:reViewController animated:YES];
}

/**
 *  投资人入口
 *
 *  @param sender
 */
- (IBAction)showInvestor:(id)sender
{
    [RegisterRequest shareInstance].userType = IdentityTypeInvestor;

    [self styleWithType:IdentityTypeInvestor];
    
    MineRegisterViewController *reViewController = (MineRegisterViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterViewController class]];
    [self.navigationController pushViewController:reViewController animated:YES];
}



@end
