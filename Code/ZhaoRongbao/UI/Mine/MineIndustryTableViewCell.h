//
//  MineIndustryTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  注册- 行业Cell

#import <UIKit/UIKit.h>
#import "IndustryModel.h"

static NSString *const mineIndustryTableViewCellIdentifier = @"MineIndustryTableViewCellIdentifier";

@interface MineIndustryTableViewCell : UITableViewCell

- (void) initWithData:(IndustryModel*)model andIndex:(int)index andSelectId:(int)industryId;
@end
