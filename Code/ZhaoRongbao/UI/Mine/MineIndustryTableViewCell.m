//
//  MineIndustryTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineIndustryTableViewCell.h"

@interface MineIndustryTableViewCell(){
    IBOutlet UILabel    *_lblTitle;
    IBOutlet UILabel    *_lblIcon;
    IBOutlet UIView     *_iconView;
    IBOutlet UIImageView     *_imgSelected;;
}
@end

@implementation MineIndustryTableViewCell

- (void)awakeFromNib {
    [_iconView.layer setCornerRadius:4];
    [_iconView.layer setMasksToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

- (void) initWithData:(IndustryModel*)model andIndex:(int)index andSelectId:(int)industryId{
    if(industryId){
        if(model.iid == industryId){
//             _imgSelected.backgroundColor = [UIColor redColor];
            _imgSelected.image = [UIImage imageNamed:@"radio"];
        }else{
//            _imgSelected.backgroundColor = [UIColor clearColor];
            _imgSelected.image = nil;
        }
    }
    NSString *string = model.industryName;
    if(string){
        _lblTitle.text = string;
        _lblIcon.text = [string substringToIndex:1];
    }
    if(model.color)
        _iconView.backgroundColor = [UIColor colorWithHexString:model.color];
//    switch (index) {
//        case 0:
//        case 1:
//        case 2:
//        case 3:
//        case 4:
//        case 5:
//            _iconView.backgroundColor = RGB(78, 171, 226);
//            break;
//        case 6:
//        case 7:
//        case 8:
//        case 9:
//        case 10:
//        case 11:
//            _iconView.backgroundColor = RGB(145 , 145, 238);
//            break;
//        case 12:
//        case 13:
//        case 14:
//        case 15:
//            _iconView.backgroundColor = RGB(141 , 201, 139);
//            break;
//        default:
//            _iconView.backgroundColor = RGB(193 , 193, 193);
//            break;
//    }
}

@end
