//
//  MineIndustryViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  行业选择页面

#import "ZRB_ViewController.h"

@interface MineIndustryViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic,assign) int pageType;//0-注册中选择行业  1-个人中心中选择行业
@property (nonatomic,assign) int industryId;//pageType= 1的时候有值；
@end
