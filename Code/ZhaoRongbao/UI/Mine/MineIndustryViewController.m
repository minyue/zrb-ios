//
//  MineIndustryViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineIndustryViewController.h"
#import "MineIndustryTableViewCell.h"
#import "PublicService.h"
#import "ZRB_UserRegisterInfo.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"

@interface MineIndustryViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet    UITableView *_tableView;
    NSMutableArray  *_listArray;
    PublicService *_service;
    MineService *_myservice;
}
@end

@implementation MineIndustryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _listArray = [[NSMutableArray alloc] init];
    _service = [[PublicService alloc] init];
    _myservice = [[MineService alloc] init];
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getData{
    //获取数据
    [_service getAllIndustryRequestWithsuccess:^(id responseObject) {
        if(responseObject){
            _listArray = (NSMutableArray*)responseObject;
            [_tableView reloadData];
        }
    } failure:^(NSError *error) {
        [_listArray removeAllObjects];
        [self showTipViewWithMsg:@"请求行业列表失败，请稍后重试"];
    }];
}

#pragma mark -
#pragma mark - 列表相关方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineIndustryTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineIndustryTableViewCellIdentifier forIndexPath:indexPath];

    [cell initWithData:[_listArray objectAtIndex:indexPath.row] andIndex:(int)indexPath.row andSelectId:_industryId];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_pageType == 0){
        //选择行业
        [self selectRow:indexPath];
    }else if(_pageType == 1)
    {
        [self saveUser:indexPath];
    }
        
}

- (void)selectRow:(NSIndexPath *)index{
    //save
    [ZRB_UserRegisterInfo shareUserRegisterInfo].model.industry = (IndustryModel*)[_listArray objectAtIndex:index.row];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Data
/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser:(NSIndexPath *)index
{
    IndustryModel *model = (IndustryModel*)[_listArray objectAtIndex:index.row];
    
    if(!index || !_listArray || !model.iid)
        return;
    
    WS(bself)
    NSDictionary *dic = @{@"industryId":@(model.iid),@"industry":model.industryName};
    [_myservice updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"保存行业选择失败，请稍后重试"];
    }];
    
    [SVProgressHUD show];
    
}

@end
