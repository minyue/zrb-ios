//
//  MineLoginViewcController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/20.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ViewController.h"

@interface MineLoginViewcController : ZRB_ViewControllerWithBackButton


@property (nonatomic,assign) BOOL isFindHomeRefresh;        //判断登录后是否刷新首页
@property (nonatomic,copy) void (^loginSuccssBlock)();
@end
