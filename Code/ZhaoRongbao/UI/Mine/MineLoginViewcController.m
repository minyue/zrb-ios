//
//  MineLoginViewcController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/20.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineLoginViewcController.h"
#import "TOTextInputChecker.h"
#import "RegisterService.h"
#import "UserModel.h"
#import "UIButton+ZRB.h"
#import "MineContainerViewController.h"
#import "UINavigationItem+CustomItem.h"
#import "MineRegisterViewController.h"

@interface MineLoginViewcController()
{
    IBOutlet  ZRB_TextField  *_phoneTextFiled;
    
    IBOutlet  ZRB_TextField  *_passWordTextFiled;
    
    IBOutlet  UIButton       *_loginButton;      //下一步
    
    IBOutlet  UIButton       *_forgetPwdBtn;
    
    TOTextInputChecker        *_phoneChecker;   //电话号码检查器
    
    TOTextInputChecker        *_passWordChecker;    //验证码检查器
    
    IBOutlet      UIView      *_helpView;           //辅助线
    
    
    RegisterService             *_service;
}
- (void)setup;

- (void)setupRightItem;

-  (IBAction)login:(id)sender;

@end


@implementation MineLoginViewcController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
    [self setupRightItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setup
{
    NSLayoutConstraint *con  = [_helpView findOwnConstraintForAttribute:NSLayoutAttributeWidth];
    con.constant = UIScreenWidth;
    
    WS(bself);
    _phoneChecker = [TOTextInputChecker telChecker:YES];
    _phoneChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself checkState];
    };
    [_phoneTextFiled initWithTitle:@"手机号" placeHolder:@"请输入您的手机号码"];
    _phoneTextFiled.delegate = _phoneChecker;
    _passWordChecker = [TOTextInputChecker  passwordChecker];
    _passWordChecker.secureTextEntry = YES;
    _passWordChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself checkState];
    };
    [_passWordTextFiled initWithTitle:@"密码" placeHolder:@"请输入您的密码"];
    _passWordTextFiled.delegate = _passWordChecker;
    [_loginButton configButtonWithType:ZRB_ButtonTypeOperation];
    [self loginButtonState:NO];  //---1
    [_forgetPwdBtn configButtonWithType:ZRB_ButtonTypeUnderLine];
    _service = [[RegisterService alloc] init];
}


- (void)setupRightItem
{
    NSString *title = @"注册";
   CustomBarItem * _rightItem =  [self.navigationItem setItemWithTitle:title textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_rightItem setOffset:-2];
    [_rightItem addTarget:self selector:@selector(resister:) event:UIControlEventTouchUpInside];

}

- (void)resister:(id)sender
{
    MineRegisterViewController *reViewController = (MineRegisterViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterViewController class]];
    [self.navigationController pushViewController:reViewController animated:YES];

}

-  (IBAction)login:(id)sender
{
    [self.view endEditing:YES];
    //13554142989 程家兴 123456
    [_service loginWithName:_phoneTextFiled.text passWord:_passWordTextFiled.text success:^(id responseObject) { //---2
//    政府账号
//    [_service loginWithName:@"18164005008" passWord:@"123456" success:^(id responseObject) {
//    [_service loginWithName:_phoneTextFiled.text passWord:_passWordTextFiled.text success:^(id responseObject) { //---2
    //政府账号
//    [_service loginWithName:@"18164005008" passWord:@"123456" success:^(id responseObject) {
    //投资人账号
//    [_service loginWithName:@"13266669999" passWord:@"123456" success:^(id responseObject) {
//      [_service loginWithName:@"13554142989" passWord:@"123456" success:^(id responseObject) {
// [_service loginWithName:@"13555559999" passWord:@"123456" success:^(id responseObject) {
//    [_service loginWithName:@"13545173623" passWord:@"111111" success:^(id responseObject) {
// [_service loginWithName:@"13555559999" passWord:@"123456" success:^(id responseObject) {
//    [_service loginWithName:@"13545173623" passWord:@"111111" success:^(id responseObject) {
        LoginModel *model = responseObject;
        
        [SVProgressHUD dismiss];

        if (model.res == ZRBHttpSuccssType) {
            model.phoneNum = _phoneTextFiled.text;
            model.userPwd = _passWordTextFiled.text;
            //登录成功
            [self loginSuccssWithObj:model];
            
            //判断是否通知发现首页刷新
//            if(_isFindHomeRefresh){
                [[NSNotificationCenter defaultCenter] postNotificationName:FIND_HOME_REFRESH object:nil];
//            }
            
        }else
        {
            [self showTipViewWithMsg:model.msg];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:kHttpFailError];
    }];
    [SVProgressHUD show];
}




- (void)loginButtonState:(BOOL)state
{
    if (state)
    {
        _loginButton.enabled = YES;
        [_loginButton setBackgroundColor:[UIColor colorWithHexString:@"4997ec"] forState:UIControlStateNormal];
        [_loginButton setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
    }else
    {
        _loginButton.enabled = NO;
        [_loginButton setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"] forState:UIControlStateDisabled];
        [_loginButton setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];

    }
}



- (void)checkState
{
    if ([_phoneChecker finalCheck] == InputCheckErrorNone && [_passWordChecker finalCheck] == InputCheckErrorNone)
    {
        [self loginButtonState:YES];
    }else
    {
        [self loginButtonState:NO];

    }
}

/**
 *  登录成功
 *
 *  @param model 
 */
- (void)loginSuccssWithObj:(LoginModel *)model
{
    [ZRB_UserManager loginSuccssWithModel:model withBlock:^(bool data) {
        if(data){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"登录失败，请稍候再试" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    //可去掉
//    if (self.loginSuccssBlock)
//    {
//        self.loginSuccssBlock();
//    }

    
}


/**
 *  显示个人主页
 */
- (void)showHomeVC
{
    NSArray *VCs = self.navigationController.viewControllers;
    MineContainerViewController *homeVC = (MineContainerViewController *)[VCs firstObject];
    [homeVC showHomeViewController];
    
}
@end
