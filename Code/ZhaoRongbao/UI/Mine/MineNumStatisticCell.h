//
//  MineProjectNumStatisticCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的 - 各种次数统计

#import <UIKit/UIKit.h>
#import "ProjectListCell.h"

static NSString *const mineNumStatisticCellIdentifier = @"MineNumStatisticCellIdentifier";

@interface MineNumStatisticCell : UITableViewCell

-(void)initWithModel:(ZRB_UserModel*)model;

@end
