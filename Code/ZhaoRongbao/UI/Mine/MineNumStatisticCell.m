//
//  MineProjectNumStatisticCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineNumStatisticCell.h"

@interface MineNumStatisticCell(){
    IBOutlet UILabel    *_lbl1;
    IBOutlet UILabel    *_lbl2;
    IBOutlet UILabel    *_lbl3;
}
@end

@implementation MineNumStatisticCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initWithModel:(ZRB_UserModel*)model{
//    if (model.collectedNumber) {
         _lbl1.text = [NSString stringWithFormat:@"%d",model.collectedNumber];
//    }
//    if (model.intcollectProject) {
        _lbl2.text = [NSString stringWithFormat:@"%d",model.collectProject];
//    }
//    if (model.collectFund) {
        _lbl3.text = [NSString stringWithFormat:@"%d",model.collectFund];
//    }
    
}
@end
