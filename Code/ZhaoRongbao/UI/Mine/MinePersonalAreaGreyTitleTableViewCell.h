//
//  MinePersonalAreaGreyTitleTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心 - 地区 灰色文字描述 Cell

#import <UIKit/UIKit.h>

@interface MinePersonalAreaGreyTitleTableViewCell : UITableViewCell

- (void)initWihtString:(NSString *)string andHideUpline:(BOOL)hi;

@end
