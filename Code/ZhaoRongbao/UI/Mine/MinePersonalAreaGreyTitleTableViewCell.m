
//
//  MinePersonalAreaGreyTitleTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalAreaGreyTitleTableViewCell.h"

@interface MinePersonalAreaGreyTitleTableViewCell(){
    IBOutlet UILabel *_lblTitle;
    IBOutlet UIView  *_upLine;
}
@end

@implementation MinePersonalAreaGreyTitleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWihtString:(NSString *)string andHideUpline:(BOOL)hi{
    _lblTitle.text = string;
    _upLine.hidden = hi;
}
@end
