//
//  MinePersonalAreaStatueCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心 - 地区 - 定位状态

#import <UIKit/UIKit.h>

@interface MinePersonalAreaStatueCell : UITableViewCell
@property (nonatomic, strong)  IBOutlet UILabel *lblTitle;
@property (nonatomic, strong)  IBOutlet UIImageView *imgView;
- (void)initWihtString:(NSString *)string;

@end
