//
//  MinePersonalAreaStatueCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalAreaStatueCell.h"

@interface MinePersonalAreaStatueCell(){
   
}
@end

@implementation MinePersonalAreaStatueCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWihtString:(NSString *)string{
    _lblTitle.text = string;
}

@end
