//
//  MinePersonalAreaViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心 - 地区 - 一级页面 省份信息

#import "ZRB_ViewController.h"

@interface MinePersonalAreaViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, assign) NSInteger lastCtrlFlag;       //上一级页面名称标记
@property (nonatomic, strong) NSString *myAreaInfo;

@end
