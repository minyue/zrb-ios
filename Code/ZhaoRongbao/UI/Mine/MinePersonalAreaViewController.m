//
//  MinePersonalAreaViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalAreaViewController.h"
#import "MineService.h"
#import "MinePersonalAreaGreyTitleTableViewCell.h"
#import "MinePersonalAreaStatueCell.h"
#import "MinePersonalItemsCell.h"
#import "AreaModel.h"
#import "MinePersonalArea_SecondLevelViewController.h"
#import "ZRBLocationMe.h"
#import <CoreLocation/CoreLocation.h>
#import "ZRB_NormalModel.h"
#import "ZRB_UserRegisterInfo.h"

@interface MinePersonalAreaViewController ()<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate>
{
    IBOutlet  UITableView  *_tableView;
    NSArray   *_dataArray;
    MineService *_service;
    AreaModel   *_selectAreaModel;
    AreaModel   *_locationAreaModel;
}
@property (strong, nonatomic) CLLocationManager* locationManager;
@end

@implementation MinePersonalAreaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //定位
    [self startLocation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_locationManager stopUpdatingLocation];
}

- (void)setup{
    _dataArray = [[NSMutableArray alloc] init];
    _service = [[MineService alloc] init];
    _selectAreaModel = [[AreaModel alloc] init];
    _locationAreaModel = [[AreaModel alloc] init];
}

//开始定位
-(void)startLocation{
    
    if ([CLLocationManager locationServicesEnabled]) {
        
        self.locationManager = [[CLLocationManager alloc] init];
        
        self.locationManager.delegate = self;
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        self.locationManager.distanceFilter = 10.0f;
        
        [_locationManager startUpdatingLocation];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            
            [_locationManager requestWhenInUseAuthorization];  //调用了这句,就会弹出允许框了.
    }
}

- (void)changeAreaStatue{
    if(!_locationAreaModel)
        return;
    
    MinePersonalAreaStatueCell *cell = (MinePersonalAreaStatueCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    cell.lblTitle.text = _locationAreaModel.showName;
}

-(void)getData{
    WS(bself)

    [_service getAllProvinceInfoWithSuccess:^(id responseObject) {
         [SVProgressHUD dismiss];
        _dataArray = (NSMutableArray*)responseObject;
        
        [_tableView reloadData];
    } failure:^{
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:kHttpFailError];
    }];
    
    [SVProgressHUD show];
}

/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser
{
    
    if(_locationAreaModel.cityName.length < 1)
        return;
    
    WS(bself)
    
    NSDictionary *dic = @{@"location":_locationAreaModel.cityName};
    [_service updateAppUserWithLocationAtRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
            
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改地区失败"];
    }];
    
    [SVProgressHUD show];
    
}

/**
 *  注册模块
 *  注册用户信息保存到内存
 */
- (void)saveRegisterUserInfo{
    RegisterInfoModel *rmodel = (RegisterInfoModel*)[ZRB_UserRegisterInfo shareUserRegisterInfo].model;
    rmodel.city = _locationAreaModel.cityName;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 定位代理经纬度回调

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [_locationManager stopUpdatingLocation];
    
    NSLog(@"location ok");
    
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        for (CLPlacemark * placemark in placemarks) {
//            NSLog(@"wh:%@ ISOcountryCode:%@ subAdministrativeArea:%@ subLocality:%@ subThoroughfare:%@  supportsSecureCoding:%@",placemark.postalCode,placemark.ISOcountryCode,placemark.subAdministrativeArea,placemark.subLocality,placemark.subAdministrativeArea,placemark.subThoroughfare);
//            NSLog(@"wh:%@",placemark.locality);
//            
//            NSLog(@"wh2:%@",placemark);
            AreaModel *model = [[AreaModel alloc] init];
            model.postcode = placemark.postalCode;
            NSDictionary *dic =    [placemark addressDictionary];
            model.cityName = [dic objectForKey:@"City"];
            model.showName = [NSString stringWithFormat:@"%@ %@",[dic objectForKey:@"State"],[dic objectForKey:@"City"]];
            _locationAreaModel = model;
            [self changeAreaStatue];
        }
        
    }];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
     [_locationManager stopUpdatingLocation];
    MinePersonalAreaStatueCell *cell = (MinePersonalAreaStatueCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    if ([error code] == kCLErrorDenied)
    {
        //访问被拒绝
        cell.lblTitle.text = @"定位失败";
    }
    if ([error code] == kCLErrorLocationUnknown) {
        //无法获取位置信息
        cell.lblTitle.text = @"无法获取位置信息";
    }
}



#pragma mark -
#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 6){
        return  [_dataArray count];
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //头部
    if (indexPath.section == 0 || indexPath.section == 4)
    {
        static NSString *minePersonalAreaGreyTitleTableViewCellIdentifier = @"MinePersonalAreaGreyTitleTableViewCellIdentifier";
        MinePersonalAreaGreyTitleTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:minePersonalAreaGreyTitleTableViewCellIdentifier forIndexPath:indexPath];
        if(indexPath.section == 0){
            [cell initWihtString:@"定位到的位置" andHideUpline:YES];
        }else{
            [cell initWihtString:@"全部" andHideUpline:NO];
        }
        return cell;
        
    }else if (indexPath.section == 2)
    {
        static NSString *minePersonalAreaStatueCellIdentifier = @"MinePersonalAreaStatueCellIdentifier";
        MinePersonalAreaStatueCell *cell = [_tableView dequeueReusableCellWithIdentifier:minePersonalAreaStatueCellIdentifier forIndexPath:indexPath];
        [cell initWihtString:@"正在定位中……"];
//       cell.imgView.hidden = YES;
        return cell;
        
    }else if(indexPath.section == 6)
    {
        static NSString *minePersonalItemsCellIdentifier = @"MinePersonalItemsCellIdentifier";
        MinePersonalItemsCell *cell = [_tableView dequeueReusableCellWithIdentifier:minePersonalItemsCellIdentifier forIndexPath:indexPath];
        AreaModel   *model = [_dataArray objectAtIndex:indexPath.row];
        [cell initWithAreaModel:model andHideArrow:NO];
        return cell;
        
    }else
    {
        static NSString *minePersonalPaddingCellIdentifier = @"MinePersonalPaddingCellIdentifier";
        UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:minePersonalPaddingCellIdentifier forIndexPath:indexPath];
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.lastCtrlFlag == ZRBPersonalCenterViewController) {
        
        if (indexPath.section == 2)
        {
            //提交定位
            [self saveUser];
        }else if(indexPath.section == 6)
        {
            //跳转到 市
            _selectAreaModel = [_dataArray objectAtIndex:[indexPath row]];
            MinePersonalArea_SecondLevelViewController *eVC = (MinePersonalArea_SecondLevelViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalArea_SecondLevelViewController class]];
            
            eVC.areaFirstLevelModel = _selectAreaModel;
            [self.navigationController pushViewController:eVC animated:YES];
        }
        
    }else if(self.lastCtrlFlag == ZRBProjectHomeViewCtroller){
        
        LogInfo(@"%d  %d",indexPath.section,indexPath.row);
        
        if (indexPath.section == 2) {
            _selectAreaModel.cityName = _locationAreaModel.cityName;
            _selectAreaModel.postcode = _locationAreaModel.postcode;
        }else if(indexPath.section == 4){
            
            _selectAreaModel.cityName = @"全国市";
            _selectAreaModel.postcode = @"";
            
        }else if(indexPath.section == 6){
            _selectAreaModel = [_dataArray objectAtIndex:[indexPath row]];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:PROJECT_PROVINCE_CHANGE object:_selectAreaModel];
        [self.navigationController popViewControllerAnimated:YES];
    }if (self.lastCtrlFlag == ZRBRegisterUserInfoViewCtroller) {
        
        if (indexPath.section == 2)
        {
            //提交定位
            [self saveRegisterUserInfo];
        }else if(indexPath.section == 6)
        {
            //跳转到 市
            _selectAreaModel = [_dataArray objectAtIndex:[indexPath row]];
            MinePersonalArea_SecondLevelViewController *eVC = (MinePersonalArea_SecondLevelViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalArea_SecondLevelViewController class]];
            eVC.currentPageType = 1;
            eVC.areaFirstLevelModel = _selectAreaModel;
            [self.navigationController pushViewController:eVC animated:YES];
        }
        
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return 44;/*头部*/
        case 1:
            return 10;/*间隙*/
        case 2:
            return 44;/*每一项*/
        case 3:
            return 10;/*间隙*/
        case 4:
            return 44;/*每一项*/
        case 5:
            return 10;/*间隙*/
        case 6:
            return 44;/*每一项*/
        default:
            return 0;
            break;
    }
}


@end
