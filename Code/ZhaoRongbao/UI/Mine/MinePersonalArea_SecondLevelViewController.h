//
//  MinePersonalArea_SecondLevelViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心 - 地区 - 二级页面  市区信息

#import "ZRB_ViewController.h"
#import "AreaModel.h"

@interface MinePersonalArea_SecondLevelViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, strong) AreaModel *areaFirstLevelModel;
@property (nonatomic, assign) int currentPageType;
@end
