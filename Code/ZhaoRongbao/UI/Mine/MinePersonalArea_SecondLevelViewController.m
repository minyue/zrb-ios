//
//  MinePersonalArea_SecondLevelViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalArea_SecondLevelViewController.h"
#import "MineService.h"
#import "MinePersonalAreaGreyTitleTableViewCell.h"
#import "MinePersonalAreaStatueCell.h"
#import "MinePersonalItemsCell.h"
#import "AreaModel.h"
#import "ZRB_NormalModel.h"
#import "MinePersonalCenterViewController.h"
#import "ZRB_UserRegisterInfo.h"
#import "MineRegisterInfoViewController.h"

@interface MinePersonalArea_SecondLevelViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet  UITableView  *_tableView;
    NSArray   *_dataArray;
    MineService *_service;
}
@end

@implementation MinePersonalArea_SecondLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    _dataArray = [[NSMutableArray alloc] init];
    _service = [[MineService alloc] init];
}

-(void)getData{
    WS(bself)
    [_service getAllCitynfoWithProvinceCode:_areaFirstLevelModel.postcode success:^(id responseObject) {
        [SVProgressHUD dismiss];
        _dataArray = (NSMutableArray*)responseObject;
        
        [_tableView reloadData];
    } failure:^{
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:kHttpFailError];
    }];
    
    
    [SVProgressHUD show];
    
}

/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser:(NSString*)code
{
    if(code.length < 1)
        return;
    
    WS(bself)
    
    NSDictionary *dic = @{@"location":code};
    
    [_service updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            for (UIViewController *vCenter in self.navigationController.viewControllers) {
                if ([vCenter isKindOfClass:[MinePersonalCenterViewController class]]) {
                    [bself.navigationController popToViewController:vCenter animated:YES];
                }
            }
            
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改地区失败"];
    }];
    
    [SVProgressHUD show];
    
}

/**
 *  注册模块
 *  注册用户信息保存到内存
 */
- (void)saveRegisterUserInfo:(AreaModel*)area{
    RegisterInfoModel *rmodel = (RegisterInfoModel*)[ZRB_UserRegisterInfo shareUserRegisterInfo].model;
    rmodel.city = area.cityName;
    rmodel.cityID = area.postcode;
    WS(bself);
    for (UIViewController *vCenter in self.navigationController.viewControllers) {
        if ([vCenter isKindOfClass:[MineRegisterInfoViewController class]]) {
            [bself.navigationController popToViewController:vCenter animated:YES];
        }
    }
}

#pragma mark -
#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 2){
        return  [_dataArray count];
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //头部
    if (indexPath.section == 0)
    {
        static NSString *minePersonalAreaGreyTitleTableViewCellIdentifier = @"MinePersonalAreaGreyTitleTableViewCellIdentifier";
        MinePersonalAreaGreyTitleTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:minePersonalAreaGreyTitleTableViewCellIdentifier forIndexPath:indexPath];
        [cell initWihtString:@"全部" andHideUpline:YES];
        return cell;
        
    }else if(indexPath.section == 2)
    {
        static NSString *minePersonalItemsCellIdentifier = @"MinePersonalItemsCellIdentifier";
        MinePersonalItemsCell *cell = [_tableView dequeueReusableCellWithIdentifier:minePersonalItemsCellIdentifier forIndexPath:indexPath];
        AreaModel   *model = [_dataArray objectAtIndex:indexPath.row];
        [cell initWithAreaModel:model andHideArrow:YES];
        return cell;
        
    }else
    {
        static NSString *minePersonalPaddingCellIdentifier = @"MinePersonalPaddingCellIdentifier";
        UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:minePersonalPaddingCellIdentifier forIndexPath:indexPath];
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2)
    {
        //跳转回去
        AreaModel   *model = [_dataArray objectAtIndex:indexPath.row];
        if(_currentPageType != 1){
            [self saveUser:model.postcode];
        }else{
            [self saveRegisterUserInfo:model];
        }
        
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return 44;/*头部*/
        case 1:
            return 10;/*间隙*/
        case 2:
            return 44;/*每一项*/
        default:
            return 0;
            break;
    }
}

@end
