//
//  MinePersonalCenterViewController.m
//  ZhaoRongbao
//
//  Created by zouli on 15/8/11.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalCenterViewController.h"
#import "MinePersonalItemsCell.h"
#import "MinePersonalTopCell.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"
#import "MinePersonalZRBAlertView.h"
#import "AppDelegate.h"
#import "MineEnterpriseCerViewController.h"
#import "MinePersonalSetEmailViewController.h"
#import "MinePersonalSetUNameViewController.h"
#import "MinePersonalUpdatePhoneNumberViewController.h"
#import "MinePersonalRealNameViewController.h"
#import "MinePersonalIDCardViewController.h"
#import "MinePersonalInfoViewController.h"
#import "MinePersonalChangePasswordViewController.h"
#import "MineSetupPwdViewController.h"
#import "MinePersonalZRBTypePwdView.h"
#import "MinePersonalZRBPwdError.h"
#import "MinePersonalAreaViewController.h"
#import "MineIndustryViewController.h"
#import "MinePersonalCompanyNameViewController.h"
#import "MinePersonalCompanyDutyViewController.h"

static NSString *const kMinePersonalTopCellIdentifier = @"MinePersonalTopCellIdentifier";
static NSString *const kMinePersonalItemsCellIdentifier = @"MinePersonalItemsCellIdentifier";
static NSString *const kMinePersonalPaddingCellIdentifier = @"MinePersonalPaddingCellIdentifier";

@interface MinePersonalCenterViewController ()<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,MinePersonalZRBTypePwdViewDelegate,MinePersonalZRBPwdErrorViewDelegate>
{
    IBOutlet  UITableView  *_tableView;
    NSArray   *_dataArray;
    MineService *_service;
    MinePersonalZRBAlertView *_alertView;
    MinePersonalZRBTypePwdView *_alertViewTypePwd;
    MinePersonalZRBPwdError *_alertViewError;
}
@end

@implementation MinePersonalCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getData];
}

- (void)setup
{
    self.title = @"个人中心";
    
    _tableView.backgroundColor = [UIColor clearColor];
    
    _dataArray = @[[PersonalItem itemWithType:MinePersonalItemsTypeUTureName],
                   [PersonalItem itemWithType:MinePersonalItemsTypeUMobile],
                   [PersonalItem itemWithType:MinePersonalItemsTypeUPassword],
                   [PersonalItem itemWithType:MinePersonalItemsTypeCompanyName],
                   [PersonalItem itemWithType:MinePersonalItemsTypeCompanyDuty],
                   [PersonalItem itemWithType:MinePersonalItemsTypeIndustry],
                   [PersonalItem itemWithType:MinePersonalItemsTypeUArea],
                    [PersonalItem itemWithType:MinePersonalItemsTypeUEmail],
                   [PersonalItem itemWithType:MinePersonalItemsTypeUDetail]];
    
    _service = [[MineService alloc] init];
    
}

-(void)getData{
    WS(bself)
    [_service getCurrentUserInfoWithsuccess:^(id responseObject) {

        [ZRB_UserManager saveUserModel:responseObject];

        [_tableView reloadData];
        
    } failure:^{
        
        [bself showTipViewWithMsg:kHttpFailError];
    }];
    
    
}

#pragma mark -
#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 2){
        return  3;
    }else if(section == 4){
        return  4;
    }else if(section == 6){
        return  2;
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //头部
    if (indexPath.section == 0)
    {
        MinePersonalTopCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMinePersonalTopCellIdentifier forIndexPath:indexPath];
        [cell   initWithModel:[ZRB_UserManager shareUserManager].userData];
        return cell;
        
    }else if (indexPath.section == 2)
    {
        MinePersonalItemsCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMinePersonalItemsCellIdentifier forIndexPath:indexPath];
        PersonalItem   *model = [_dataArray objectAtIndex:indexPath.row];
        model.indexPath = indexPath;
        [cell initWithItem:model andData:[ZRB_UserManager shareUserManager].userData];
        return cell;
        
    }else if(indexPath.section == 4)
    {
        
        MinePersonalItemsCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMinePersonalItemsCellIdentifier forIndexPath:indexPath];
        PersonalItem   *model = [_dataArray objectAtIndex:indexPath.row + 3];
        model.indexPath = indexPath;
        [cell initWithItem:model andData:[ZRB_UserManager shareUserManager].userData];
        return cell;
        
    }else if(indexPath.section == 6)
    {
        MinePersonalItemsCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMinePersonalItemsCellIdentifier forIndexPath:indexPath];
        PersonalItem   *model = [_dataArray objectAtIndex:indexPath.row + 7];
        model.indexPath = indexPath;
        [cell initWithItem:model andData:[ZRB_UserManager shareUserManager].userData];
        return cell;
    }else
    {
        UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:kMinePersonalPaddingCellIdentifier forIndexPath:indexPath];
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0)
    {
        //修改头像
        [self takePictureClicked:nil];
        
    }else if (indexPath.section == 2)
    {
        switch (indexPath.row) {
            case 0:
//                //企业认证
//                [self clickEnterpriseCer];
                //真实姓名
                [self clickRealName];
                break;
            case 1:
                //绑定手机
                [self clickUmobile];
                break;
            case 2:
                //修改密码
                [self clickChangePwd];
                break;
            default:
                break;
        }
        
    }else if(indexPath.section == 4)
    {
        switch (indexPath.row) {
            case 0:
                //公司名称
                [self clickCompanyName];
                break;
            case 1:
                //担任职务
                [self clickCompanyDuty];
                break;
            case 2:
                //所属行业
                [self clickIndustry];
                break;
            case 3:
                //所在城市
                //地区
                [self clickAreaInfo];
                break;
            default:
                break;
        }
    }else if(indexPath.section == 6)
    {
        switch (indexPath.row) {
            case 0:
                //电子邮箱
                [self clickEmail];
                break;
            case 1:
                //个人说明
                [self clickUInfo];
                break;
            default:
                break;
        }
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return 60;/*头部*/
        case 1:
            return 10;/*间隙*/
        case 2:
            return 44;/*每一项*/
        case 3:
            return 10;/*间隙*/
        case 4:
            return 44;/*每一项*/
        case 5:
            return 10;/*间隙*/
        case 6:
            return 44;/*每一项*/
        default:
            return 0;
            break;
    }
}

#pragma mark -- IBAction
//点击 修改头像
- (IBAction)takePictureClicked:(id)sender {
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil
                                                            delegate:self
                                                   cancelButtonTitle:@"取消"
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"拍照",@"从手机相册选择", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
    
}

//点击 企业认证
- (IBAction)clickEnterpriseCer{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    
    NSMutableAttributedString *attString;
  /*  if(user.status == OrgStatus_ed){
        if(![user.org isEqualToString:@""]){
            attString = [[NSMutableAttributedString alloc] initWithString:@"您已认证\n\n若有疑问可联系我们人工客服\n"];
        }else{
            if(user.statusName!=nil && ![user.statusName isEqualToString:@""]){
                attString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"您已认证“%@”\n若有疑问可联系我们人工客服\n",user.statusName]];
                [attString addAttribute:NSForegroundColorAttributeName value:RGB(86, 171, 228) range:NSMakeRange(5, user.statusName.length)];
               
            }else{
                attString = [[NSMutableAttributedString alloc] initWithString:@"您已认证\n\n若有疑问可联系我们人工客服\n"];
            }
        }
    }else{*/
        if(user.userType == IdentityTypeServants){
            //机构认证
            attString = [[NSMutableAttributedString alloc] initWithString: @"您还未认证您的企业\n\n可联系我们人工客服进行认证\n"];
//             for 测试
            MineEnterpriseCerViewController *eVC = (MineEnterpriseCerViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineEnterpriseCerViewController class]];
            eVC.thePageType = MineEnterpriceCerTypePersonal;
            [self.navigationController pushViewController:eVC animated:YES];
            return;
        }else{
            //企业认证
            MineEnterpriseCerViewController *eVC = (MineEnterpriseCerViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineEnterpriseCerViewController class]];
            eVC.thePageType = MineEnterpriceCerTypePersonal;
            [self.navigationController pushViewController:eVC animated:YES];
            return;
        }
   // }
    
    if(_alertView != nil){
        [_alertView removeFromSuperview];
        _alertView = nil;
    }
    _alertView = [[MinePersonalZRBAlertView alloc] initWithText:attString];
    [_alertView showOnView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
        
}

//点击 用户名
- (IBAction)clickUname{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalSetUNameViewController *eVC = (MinePersonalSetUNameViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalSetUNameViewController class]];
    eVC.uname = user.uname;
    [self.navigationController pushViewController:eVC animated:YES];
    
}

//点击 绑定手机
- (IBAction)clickUmobile{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalUpdatePhoneNumberViewController *eVC = (MinePersonalUpdatePhoneNumberViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalUpdatePhoneNumberViewController class]];
    eVC.umobile = user.uMobile;
    [self.navigationController pushViewController:eVC animated:YES];
    
}

//点击 修改密码
- (IBAction)clickChangePwd{
    if(_alertViewTypePwd != nil){
        [_alertViewTypePwd removeFromSuperview];
        _alertViewTypePwd = nil;
    }
    _alertViewTypePwd = [[MinePersonalZRBTypePwdView alloc] initWithTextField];
    _alertViewTypePwd.delegate = self;
    [_alertViewTypePwd showOnView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
}

//提示密码验证错误
-(void)showValidPwdError{
    if(_alertViewError != nil){
        [_alertViewError removeFromSuperview];
        _alertViewError = nil;
    }
    _alertViewError = [[MinePersonalZRBPwdError alloc] initWithTextFieldError];
    _alertViewError.delegate = self;
    [_alertViewError showOnView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
}

// 找回密码
- (void)findPassword
{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MineSetupPwdViewController    *pwdVC = (MineSetupPwdViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineSetupPwdViewController class]];
    pwdVC.pageType = 1;
    pwdVC.mobile = user.uMobile;
    [self.navigationController pushViewController:pwdVC animated:YES];
    
}

//点击 电子邮箱
- (IBAction)clickEmail{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalSetEmailViewController *eVC = (MinePersonalSetEmailViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalSetEmailViewController class]];
    eVC.uemail = user.uEmail;
    [self.navigationController pushViewController:eVC animated:YES];
}

//点击 真实姓名
- (IBAction)clickRealName{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalRealNameViewController *eVC = (MinePersonalRealNameViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalRealNameViewController class]];
    eVC.urealName = user.realname;
    [self.navigationController pushViewController:eVC animated:YES];
    
}

//点击 身份证
- (IBAction)clickIdCard{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalIDCardViewController *eVC = (MinePersonalIDCardViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalIDCardViewController class]];
    eVC.uidCard = user.idCard;
    [self.navigationController pushViewController:eVC animated:YES];
}

//点击 公司名称
- (IBAction)clickCompanyName{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalCompanyNameViewController *cnameCtrl = (MinePersonalCompanyNameViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalCompanyNameViewController class]];
    cnameCtrl.companyName = user.org;
    [self.navigationController pushViewController:cnameCtrl animated:YES];
}

//点击 担任职位
- (IBAction)clickCompanyDuty{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalCompanyDutyViewController *cdutyCtrl = (MinePersonalCompanyDutyViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalCompanyDutyViewController class]];
    cdutyCtrl.duty = user.position;
    [self.navigationController pushViewController:cdutyCtrl animated:YES];
}

//点击 所属行业
- (IBAction)clickIndustry{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MineIndustryViewController *industryCtrl = (MineIndustryViewController *)[StoryBoardUtilities viewControllerForStoryboardName:kMineStoryboardName class:[MineIndustryViewController class]];
    industryCtrl.pageType = 1;
    industryCtrl.industryId = user.industryId;
    [self.navigationController pushViewController:industryCtrl animated:YES];
}

//点击 地区
- (IBAction)clickAreaInfo{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalAreaViewController *eVC = (MinePersonalAreaViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalAreaViewController class]];

    eVC.myAreaInfo = user.LOCATIONNAME;
    eVC.lastCtrlFlag = ZRBPersonalCenterViewController;
    [self.navigationController pushViewController:eVC animated:YES];
}

//点击 个人说明
- (IBAction)clickUInfo{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    MinePersonalInfoViewController *eVC = (MinePersonalInfoViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalInfoViewController class]];
    eVC.uinfo = user.INFO;
    [self.navigationController pushViewController:eVC animated:YES];
}

#pragma mark - MinePersonalZRBTypePwdViewDelegate

- (void)mZRB_AlertViewChangePwd:(NSString *)pwd{
    //验证密码
    WS(bself);
    [_service checkUserPWDWithRequest:pwd success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            //验证成功 -> 修改密码页面
            dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 1.0 *NSEC_PER_SEC);
            dispatch_after(afterTime, dispatch_get_main_queue(), ^{
                MinePersonalChangePasswordViewController *eVC = (MinePersonalChangePasswordViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalChangePasswordViewController class]];
                [self.navigationController pushViewController:eVC animated:YES];
            });
            
        }else{
            //验证失败
            [self showValidPwdError];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"请求验证失败"];
    }];
    
    [SVProgressHUD show];
    
}

#pragma mark - MinePersonalZRBPwdErrorViewDelegate
- (void)mZRB_AlertViewFindPwd{
    //找回密码
    dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 1.0 *NSEC_PER_SEC);
    dispatch_after(afterTime, dispatch_get_main_queue(), ^{
        [self findPassword];
    });
    
}

- (void)mZRB_AlertViewValidPwd{
    //重新弹出验证框
    dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 0.8 *NSEC_PER_SEC);
    dispatch_after(afterTime, dispatch_get_main_queue(), ^{
        [self clickChangePwd];
    });
    
}

#pragma mark - UIActionSheet Delegate
/**
 *  跳出相机或相册
 *
 *  @param actionSheet 下标
 *  @param buttonIndex 按钮下标
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIImagePickerControllerSourceType sourceType;
    if (buttonIndex == 0) {
        //先设定sourceType为相机，然后判断相机是否可用，不可用将sourceType设定为相片库
        sourceType = UIImagePickerControllerSourceTypeCamera;
        if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        picker.navigationBar.barTintColor = [UIColor colorWithHexString:@"0077d9"];
        picker.navigationBar.translucent = NO;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else if (buttonIndex == 1){
        //相册
        sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        picker.navigationBar.barTintColor = [UIColor colorWithHexString:@"0077d9"];
        picker.navigationBar.translucent = NO;
        [self presentViewController:picker animated:YES completion:nil];
    }
}


#pragma mark - Camera View Delegate Methods
/**
 *  拍照完成或相册选择完成
 *
 *  @param picker 相机或相册
 *  @param info   照片
 */
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    NSString *path = [ZRBUtilities ZRBScaleAndSaveImage:image];
    if(path){
        [self upLoadUserHead:path];
    }else{
        [self showTipViewWithMsg:@"保存失败"];
    }
}
/**
 *  在相机或相册 点了取消
 *
 *  @param picker
 */
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}



/**
 *  上传头像文件
 */
- (void)upLoadUserHead:(NSString*)path {
    // 上传用户头像
    WS(bself)
    [_service fileUpload:path success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        
        [bself uploadImageCallBackWithModel:(ZRB_NormalModel *)responseObject andPath:(NSString*)path];
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
    }];
    
    [SVProgressHUD show];
}


- (void)uploadImageCallBackWithModel:(ZRB_NormalModel *)model andPath:(NSString*)path
{
    if (model.res == ZRBHttpSuccssType) {
        
        MinePersonalTopCell *cell = (MinePersonalTopCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell.myIcon setImage:[UIImage imageWithContentsOfFile:path]];
    }
}
@end

