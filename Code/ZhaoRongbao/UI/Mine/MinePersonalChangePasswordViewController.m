//
//  MinePersonalChangePasswordViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/19.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalChangePasswordViewController.h"
#import "ZRB_TextField.h"
#import "TOTextInputChecker.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface MinePersonalChangePasswordViewController ()
{
    IBOutlet ZRB_TextField  *_txtPwd;
    IBOutlet ZRB_TextField  *_txtRePwd;
    IBOutlet UIButton *_btnFinish;
    TOTextInputChecker *_pwdChecker;

    MineService *_myservice;
}
@end

@implementation MinePersonalChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    WS(bself);
    _pwdChecker = [TOTextInputChecker passwordChecker];
    _pwdChecker.checkInputBlock = ^(BOOL flag)
    {
        [self finishBtnActive:flag];
    };
    
    [_txtPwd initWithTitle:@"新密码" placeHolder:@"请设置 6 ~16 位密码"];
    _txtPwd.delegate = _pwdChecker;
    _txtPwd.textFiled.clearButtonMode = UITextFieldViewModeNever;
    
    [_txtRePwd initWithTitle:@"确认密码" placeHolder:@"请再次确认您的密码"];
    _txtRePwd.delegate = _pwdChecker;
    _txtRePwd.textFiled.clearButtonMode = UITextFieldViewModeNever;
    
    [self finishBtnActive:NO];
    
    _myservice = [[MineService alloc] init];
    
}


- (void)finishBtnActive:(BOOL)isActive
{
    if (isActive)
    {
        _btnFinish.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        _btnFinish.enabled = YES;
        
    }else
    {
        _btnFinish.backgroundColor = RGB(187, 187, 187);
        _btnFinish.enabled = NO;
    }
}

/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (IBAction)saveUser:(id)sender
{
    if(_txtPwd.text.length < 1 || _txtRePwd.text.length < 1)
        return;
    if(![_txtPwd.text isEqualToString:_txtRePwd.text]){
        [self showTipViewWithMsg:@"两次密码输入不一致请重新输入"];
        return;
    }
    
    WS(bself)
    
    [_myservice changeUserPWDWithRequest:_txtPwd.text success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [MBProgressHUD showHUDTitle:model.msg onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
            dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 1.5 *NSEC_PER_SEC);
            dispatch_after(afterTime, dispatch_get_main_queue(), ^{
                [bself.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改密码失败"];
    }];
    
    [SVProgressHUD show];
    
}
//
////#pragma mark - UItextFieldDelegate
//- (void)ZRBtextFieldDidBeginEditing:(UITextField *)textField{
//    
//}
//
//- (BOOL)ZRBtextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    return YES;
//}
//  _txtPwd.text.length == _txtRePwd.text.length

@end
