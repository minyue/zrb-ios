//
//  MinePersonalCompanyDutyViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心 - 担任职务

#import "ZRB_ViewController.h"

@interface MinePersonalCompanyDutyViewController : ZRB_ViewControllerWithBackButton
@property (nonatomic, strong) NSString *duty;
@end
