//
//  MinePersonalCompanyDutyViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalCompanyDutyViewController.h"
#import "ZRB_TextField.h"
#import "TOTextInputChecker.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"

@interface MinePersonalCompanyDutyViewController ()<ZRB_TextFieldDelegate>
{
    IBOutlet ZRB_TextField  *_txtDuty;
//    TOTextInputChecker *_rNameChecker;
    CustomBarItem *_item;
    MineService *_myservice;
}
@end

@implementation MinePersonalCompanyDutyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setup{
    [self setRightItem];
    
    [_txtDuty initWithTitle:@"担任职务" placeHolder:@"请输入您的职务"];
    _txtDuty.delegate = self;
    _txtDuty.showIconWhenSuccess = YES;
    _txtDuty.textFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    _myservice = [[MineService alloc] init];
    
    if(_duty){
        _txtDuty.text = _duty;
    }
}

- (void)setRightItem
{
    NSString *title = @"保存";
    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_item setOffset:-2];
    [_item addTarget:self selector:@selector(saveUser:) event:UIControlEventTouchUpInside];
    
}

/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser:(id)sender
{
    if(_txtDuty.text.length < 1)
        return;
    
    WS(bself)
    
    NSDictionary *dic = @{@"position":_txtDuty.text};
    
    [_myservice updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改职务信息失败"];
    }];
    
    [SVProgressHUD show];
    
}

#pragma mark - UItextFieldDelegate
- (void)ZRBtextFieldDidBeginEditing:(UITextField *)textField{
    
}

- (BOOL)ZRBtextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    long int length = textField.text.length - range.length + string.length;
    
    if(length>0){
        _item.contentBarItem.enabled = YES;
    }else{
        _item.contentBarItem.enabled = NO;
    }
    return YES;
}

- (BOOL)ZRBtextFieldShouldClear:(UITextField *)textField{
    _item.contentBarItem.enabled = NO;
    return YES;
}
@end
