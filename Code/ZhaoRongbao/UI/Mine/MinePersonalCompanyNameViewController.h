//
//  MinePersonalCompanyNameViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"

@interface MinePersonalCompanyNameViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, strong) NSString *companyName;

@end
