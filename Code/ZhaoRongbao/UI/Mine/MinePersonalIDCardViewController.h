//
//  MinePersonalIDCardViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/19.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心-- 修改身份证

#import "ZRB_ViewController.h"

@interface MinePersonalIDCardViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, strong) NSString *uidCard;

@end
