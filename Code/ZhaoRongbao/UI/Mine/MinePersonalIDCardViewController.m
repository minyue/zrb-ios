//
//  MinePersonalIDCardViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/19.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalIDCardViewController.h"
#import "ZRB_TextField.h"
#import "TOTextInputChecker.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"

@interface MinePersonalIDCardViewController ()
{
    IBOutlet ZRB_TextField  *_txtIdCard;
    TOTextInputChecker *_idCardChecker;
    CustomBarItem *_item;
    MineService *_myservice;
}
@end

@implementation MinePersonalIDCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
//    WS(bself);
    [self setRightItem];
    _idCardChecker = [TOTextInputChecker idCarcChecker:YES];
    _idCardChecker.checkInputBlock = ^(BOOL flag)
    {
        [self userIconActive:flag];
        if(flag){
            _item.contentBarItem.enabled = YES;
        }else{
            _item.contentBarItem.enabled = NO;
        }
    };
    
    [_txtIdCard initWithTitle:@"身份证" placeHolder:@"请输入您的18位身份证号码"];
    _txtIdCard.delegate = _idCardChecker;
    _txtIdCard.showIconWhenSuccess = YES;
    _txtIdCard.textFiled.clearButtonMode = UITextFieldViewModeNever;
    
    _myservice = [[MineService alloc] init];
    
    if(_uidCard){
        _txtIdCard.text = _uidCard;
    }
}

- (void)setRightItem
{
    NSString *title = @"保存";
    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_item setOffset:-2];
    [_item addTarget:self selector:@selector(saveUser:) event:UIControlEventTouchUpInside];
    
}

- (void)userIconActive:(BOOL)isActive
{
    [_txtIdCard showIconView:isActive];
    
}


/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser:(id)sender
{
    if(_txtIdCard.text.length < 1)
        return;
    
    WS(bself)
    
    NSDictionary *dic = @{@"idcard":_txtIdCard.text};
    
    [_myservice updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改身份证失败"];
    }];
    
    [SVProgressHUD show];
    
}

#pragma mark - UItextFieldDelegate
- (void)ZRBtextFieldDidBeginEditing:(UITextField *)textField{
    
}

- (BOOL)ZRBtextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    long int length = textField.text.length - range.length + string.length;
    
    if(length>0){
        _item.contentBarItem.enabled = YES;
    }else{
        _item.contentBarItem.enabled = NO;
    }
    return YES;
}


@end
