//
//  MinePersonalInfoViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/19.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalInfoViewController.h"
#import "ZRB_TextField.h"
#import "TOTextInputChecker.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"
#import "RegexKitLite.h"

#define TextMaxLength 30

@interface MinePersonalInfoViewController ()<UITextViewDelegate>
{
    IBOutlet UITextView  *_txtUinfo;
    CustomBarItem *_item;
    MineService *_myservice;
    IBOutlet UIView *_countDownLabel;
    IBOutlet UILabel *_lblCountDown;
}
@end

@implementation MinePersonalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    WS(bself);
    [self setRightItem];
    
    
    _countDownLabel.layer.cornerRadius = 10;
    _countDownLabel.layer.masksToBounds = YES;
    
    _myservice = [[MineService alloc] init];
    
    if(_uinfo){
        _txtUinfo.text = _uinfo;
        [_countDownLabel setHidden:NO];
        _lblCountDown.text = [NSString stringWithFormat:@"%lu",(30 - _uinfo.length)];
    }else{
        [_countDownLabel setHidden:YES];
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textViewEditChanged:) name:@"UITextViewTextDidChangeNotification"
                                              object:_txtUinfo];
}

- (void)setRightItem
{
    NSString *title = @"保存";
    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_item setOffset:-2];
    [_item addTarget:self selector:@selector(saveUser:) event:UIControlEventTouchUpInside];
    _item.contentBarItem.enabled = NO;
    
}


/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser:(id)sender
{
    if(_txtUinfo.text.length < 1)
        return;
    
    WS(bself)
    
    NSDictionary *dic = @{@"info":_txtUinfo.text};
    
    [_myservice updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改个人信息失败"];
    }];
    
    [SVProgressHUD show];
    
}

#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    
    _lblCountDown.text = [NSString stringWithFormat:@"%u",(30 - [textView text].length)];
}

//- (BOOL)textView:(UITextView *)textView:(NSRange)range replacementText:(NSString *)text{
//    
//        if ([text length]==0) {
//            
//            return YES;
//        }
//        else {
//            NSLog(@"length:%lu : %lu  :%lu  last:%lu",(unsigned long)[textView text].length,(unsigned long)text.length, (unsigned long)range.length,[[textView text] length]+[text length]-range.length- TextMaxLength);
//            if ([[textView text] length]+[text length]-range.length < TextMaxLength){
//                
//                return YES;
//            }
//            else {
//                
//                return NO;
//            }
//        }
//}
//
//- (void)textViewDidChange:(UITextView *)textView{
//    if([textView text].length > 0){
//        _item.contentBarItem.enabled = YES;
//        [_countDownLabel setHidden:NO];
//    }else{
//        _item.contentBarItem.enabled = NO;
//        [_countDownLabel setHidden:YES];
//    }
//    if([textView text].length>=30)
//        return;
//    NSLog(@"length:%lu : %@",(unsigned long)[textView text].length,[textView text]);
//    _lblCountDown.text = [NSString stringWithFormat:@"%lu",(29 - [textView text].length)];
//}
//
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    NSString *new = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(new.length > TextMaxLength){
        
        if (![text isEqualToString:@""]) {
            
            return NO;
            
        }
        
    }
    
    return YES;
    
}

-(void)textViewEditChanged:(NSNotification *)obj{
    UITextField *textField = (UITextField *)obj.object;
    if([_txtUinfo text].length > 0){
        _item.contentBarItem.enabled = YES;
        [_countDownLabel setHidden:NO];
    }else{
        _item.contentBarItem.enabled = NO;
        [_countDownLabel setHidden:YES];
    }
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > TextMaxLength) {
                _txtUinfo.text = [toBeString substringToIndex:TextMaxLength-1];
                _lblCountDown.text = [NSString stringWithFormat:@"%u",(TextMaxLength - [_txtUinfo text].length)];
            }else{
                _lblCountDown.text = [NSString stringWithFormat:@"%u",(TextMaxLength - [_txtUinfo text].length)];
            }
        }
        else{
            if(toBeString.length <= TextMaxLength){
                _lblCountDown.text = [NSString stringWithFormat:@"%u",(TextMaxLength - [_txtUinfo text].length)];
            }else{
                _txtUinfo.text = [toBeString substringToIndex:TextMaxLength-1];
                _lblCountDown.text = @"0";
            }

        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > TextMaxLength) {
            _txtUinfo.text = [toBeString substringToIndex:TextMaxLength];
        }
        _lblCountDown.text = [NSString stringWithFormat:@"%u",(TextMaxLength - [_txtUinfo text].length)];
    }
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:@"UITextViewTextDidChangeNotification"
                                                 object:_txtUinfo];
}
@end
