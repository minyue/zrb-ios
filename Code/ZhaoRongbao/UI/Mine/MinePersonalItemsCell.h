//
//  MinePersonalItemsCell.h
//  ZhaoRongbao
//
//  Created by zouli on 15/8/11.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  个人中心 - 普通Cell

#import <UIKit/UIKit.h>
#import "AreaModel.h"
@class PersonalItem;

typedef NS_ENUM(NSInteger, MinePersonalItemsType){
    /**
     *  机构认证
     */
    MinePersonalItemsTypeStatue,
    /**
     *  用户名
     */
    MinePersonalItemsTypeUName,
    /**
     *  绑定手机
     */
    MinePersonalItemsTypeUMobile,
    /**
     *  修改密码
     */
    MinePersonalItemsTypeUPassword,
    
    /**
     *  电子邮箱
     */
    MinePersonalItemsTypeUEmail,
    
    /**
     *  真实姓名
     */
    MinePersonalItemsTypeUTureName,
    /**
     *  身份证
     */
    MinePersonalItemsTypeUCode,
    /**
     *  地区
     */
    MinePersonalItemsTypeUArea,
    /**
     *  个人说明
     */
    MinePersonalItemsTypeUDetail,
    /**
     *  公司名称
     */
    MinePersonalItemsTypeCompanyName,
    /**
     *  担任职务
     */
    MinePersonalItemsTypeCompanyDuty,
    /**
     *  所属行业
     */
    MinePersonalItemsTypeIndustry
    
};
@interface MinePersonalItemsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *imgArrow;

- (void) initWithItem:(PersonalItem*)item andData:(ZRB_UserModel*)user;

- (void) initWithAreaModel:(AreaModel*)model andHideArrow:(BOOL)hi;
@end



@interface PersonalItem : NSObject

@property (nonatomic,strong) NSString *value;

@property (nonatomic,copy) NSString *title;

@property (nonatomic,strong) NSIndexPath *indexPath;

@property (nonatomic,assign) MinePersonalItemsType type;


+ (PersonalItem *)itemWithType:(MinePersonalItemsType)type;

@end