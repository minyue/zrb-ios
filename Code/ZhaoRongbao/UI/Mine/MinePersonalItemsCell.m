//
//  MinePersonalItemsCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/11.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MinePersonalItemsCell.h"

@interface MinePersonalItemsCell()
{
    IBOutlet UILabel   *lblItem;
    IBOutlet UILabel   *lblValue;
    
}

@end

@implementation MinePersonalItemsCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) initWithItem:(PersonalItem*)item andData:(ZRB_UserModel*)user{
    lblItem.text = item.title;
    if(!user)
        return;
    lblValue.textColor = RGB(143, 143, 143);
    switch (item.type) {
        case MinePersonalItemsTypeStatue:
        {
            lblItem.text = user.userType == IdentityTypeServants ? @"机构认证" : @"企业认证" ;
            if(user.status == OrgStatus_ed){
                if(![user.org isEqualToString:@""]){
                    lblValue.text = user.org;
                }else{
                    lblValue.text = user.statusName;
                }
            }else{
                lblValue.text = @"未认证";
                lblValue.textColor = RGB(51, 176, 134);
            }
            break;
        }
        case MinePersonalItemsTypeUName:
        {
            item.title = @"用户名";
            lblValue.text = user.uname;
            break;
        }
        case MinePersonalItemsTypeUMobile:
        {
            item.title = @"绑定手机";
            lblValue.text = user.uMobile;
            break;
        }case MinePersonalItemsTypeUPassword:
        {
            item.title = @"修改密码";
            lblValue.text = @"";
            break;
        }
        case MinePersonalItemsTypeUEmail:
        {
            item.title = @"电子邮箱";
            if(![user.uEmail isEqualToString:@""]){
                lblValue.text = user.uEmail;
            }else{
                lblValue.text = @"未绑定";
            }
            break;
        }
        case MinePersonalItemsTypeUTureName:
        {
            item.title = @"真实姓名";
            if(![user.realname isEqualToString:@""]){
                lblValue.text = user.realname;
            }else{
                lblValue.text = @"未填写";
            }
            break;
        }
        case MinePersonalItemsTypeUCode:
        {
            item.title = @"身份证";
            if(![user.idCard isEqualToString:@""]){
                lblValue.text = user.idCard;
            }else{
                lblValue.text = @"未填写";
            }
            break;
        }
        case MinePersonalItemsTypeUArea:
        {
            item.title = @"所在城市";
            if(![user.LOCATIONNAME isEqualToString:@""]){
                lblValue.text = user.LOCATIONNAME;
            }else{
                lblValue.text = @"未填写";
            }
//            lblValue.text = [NSString stringWithFormat:@"%@ %@",user.IDCARD_PROVINCE,user.IDCARD_CITY];
            break;
        }
        case MinePersonalItemsTypeUDetail:
        {
            item.title = @"个人说明";
            if(![user.INFO isEqualToString:@""]){
                lblValue.text = user.INFO;
            }else{
                lblValue.text = @"未填写";
            }
            break;
        }
        case MinePersonalItemsTypeCompanyName:
        {
            item.title = @"公司名称";
            if(![user.org isEqualToString:@""]){
                lblValue.text = user.org;
            }else{
                lblValue.text = @"未填写";
            }
            break;
        }
        case MinePersonalItemsTypeCompanyDuty:
        {
            item.title = @"担任职务";
            if(![user.position isEqualToString:@""]){
                lblValue.text = user.position;
            }else{
                lblValue.text = @"未填写";
            }
            break;
        }
        case MinePersonalItemsTypeIndustry:
        {
            item.title = @"所属行业";
            if(![user.industry isEqualToString:@""]){
                lblValue.text = user.industry;
            }else{
                lblValue.text = @"未填写";
            }
            break;
        }
        default:
            break;
    }
}

- (void) initWithAreaModel:(AreaModel*)model andHideArrow:(BOOL)hi{
    lblItem.text = model.cityName;
    lblValue.hidden = YES;
    _imgArrow.hidden = hi;
}

@end

@implementation PersonalItem

+ (PersonalItem *)itemWithType:(MinePersonalItemsType)type
{
    
    PersonalItem *item = [[PersonalItem alloc] init];
    item.type = type;
    
    switch (type) {
        case MinePersonalItemsTypeStatue:
        {
            item.title = @"企业认证";
            break;
        }
        case MinePersonalItemsTypeUName:
        {
            item.title = @"用户名";
            break;
        }
        case MinePersonalItemsTypeUMobile:
        {
            item.title = @"绑定手机";
            item.indexPath = 0;
            break;
        }case MinePersonalItemsTypeUPassword:
        {
            item.title = @"修改密码";
            break;
        }
        case MinePersonalItemsTypeUEmail:
        {
            item.title = @"电子邮箱";
            break;
        }
        case MinePersonalItemsTypeUTureName:
        {
            item.title = @"真实姓名";
            break;
        }
        case MinePersonalItemsTypeUCode:
        {
            item.title = @"身份证";
            break;
        }
        case MinePersonalItemsTypeUArea:
        {
            item.title = @"所在城市";
            break;
        }
        case MinePersonalItemsTypeUDetail:
        {
            item.title = @"个人说明";
            break;
        }
        case MinePersonalItemsTypeCompanyName:
        {
            item.title = @"公司名称";
            break;
        }
        case MinePersonalItemsTypeCompanyDuty:
        {
            item.title = @"担任职务";
            break;
        }
        case MinePersonalItemsTypeIndustry:
        {
            item.title = @"所属行业";
            break;
        }
            
        default:
            break;
    }
    return item;
    
}

@end
