//
//  MinePersonalRealNameViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalRealNameViewController.h"
#import "ZRB_TextField.h"
#import "TOTextInputChecker.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"

@interface MinePersonalRealNameViewController ()
{
    IBOutlet ZRB_TextField  *_txtRealName;
    TOTextInputChecker *_rNameChecker;
    CustomBarItem *_item;
    MineService *_myservice;
}
@end

@implementation MinePersonalRealNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setup{
    WS(bself);
    [self setRightItem];
    _rNameChecker = [TOTextInputChecker userTrueNameChecker];
    _rNameChecker.checkInputBlock = ^(BOOL flag)
    {
        [self userIconActive:flag];
        if(flag){
            _item.contentBarItem.enabled = YES;
        }else{
            _item.contentBarItem.enabled = NO;
            //            [bself showTipViewWithMsg:@"邮箱格式不正确请重新输入"];
        }
    };
    
    [_txtRealName initWithTitle:@"姓 名" placeHolder:@"请输入您的真实姓名"];
    _txtRealName.delegate = _rNameChecker;
    _txtRealName.showIconWhenSuccess = YES;
    _txtRealName.textFiled.clearButtonMode = UITextFieldViewModeNever;
    
    _myservice = [[MineService alloc] init];
    
    if(_urealName){
        _txtRealName.text = _urealName;
    }
}

- (void)setRightItem
{
    NSString *title = @"保存";
    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_item setOffset:-2];
    [_item addTarget:self selector:@selector(saveUser:) event:UIControlEventTouchUpInside];
    
}

- (void)userIconActive:(BOOL)isActive
{
    [_txtRealName showIconView:isActive];
    
}


/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser:(id)sender
{
    if(_txtRealName.text.length < 1)
        return;
    
    WS(bself)
    
    NSDictionary *dic = @{@"realname":_txtRealName.text};
    
    [_myservice updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改姓名失败"];
    }];
    
    [SVProgressHUD show];
    
}

#pragma mark - UItextFieldDelegate
- (void)ZRBtextFieldDidBeginEditing:(UITextField *)textField{
    
}

- (BOOL)ZRBtextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    long int length = textField.text.length - range.length + string.length;
    
    if(length>0){
        _item.contentBarItem.enabled = YES;
    }else{
        _item.contentBarItem.enabled = NO;
    }
    return YES;
}

- (BOOL)ZRBtextFieldShouldClear:(UITextField *)textField{
    _item.contentBarItem.enabled = NO;
    return YES;
}
@end
