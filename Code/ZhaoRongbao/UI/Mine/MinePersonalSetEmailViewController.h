//
//  MinePersonalSetEmailViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心-设置邮箱

#import "ZRB_ViewController.h"

@interface MinePersonalSetEmailViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, strong) NSString *uemail;

@end
