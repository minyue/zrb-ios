//
//  MinePersonalSetEmailViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalSetEmailViewController.h"
#import "ZRB_TextField.h"
#import "TOTextInputChecker.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"

@interface MinePersonalSetEmailViewController ()
{
    IBOutlet ZRB_TextField  *_txtEmail;
    TOTextInputChecker *_emailChecker;
    CustomBarItem *_item;
    MineService *_myservice;
}
@end

@implementation MinePersonalSetEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    WS(bself);
    [self setRightItem];
    _emailChecker = [TOTextInputChecker mailChecker:YES];
    _emailChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself userIconActive:flag];
        if(flag){
            _item.contentBarItem.enabled = YES;
        }else{
            _item.contentBarItem.enabled = NO;
//            [bself showTipViewWithMsg:@"邮箱格式不正确请重新输入"];
        }
    };
    [_txtEmail initWithTitle:@"邮 箱" placeHolder:@"请输入您真实有效的电子邮箱"];
    
    _txtEmail.delegate = _emailChecker;
    _txtEmail.showIconWhenSuccess = YES;
    _txtEmail.textFiled.clearButtonMode = UITextFieldViewModeNever;
    
    _myservice = [[MineService alloc] init];
    
    if(_uemail){
        _txtEmail.text = _uemail;
    }
}

- (void)setRightItem
{
    NSString *title = @"保存";
    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_item setOffset:-2];
    [_item addTarget:self selector:@selector(saveUser:) event:UIControlEventTouchUpInside];
  
}

- (void)userIconActive:(BOOL)isActive
{
    [_txtEmail showIconView:isActive];
    
}

/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser:(id)sender
{
    if(_txtEmail.text.length < 1)
        return;
    
    WS(bself)
    
    NSDictionary *dic = @{@"uemail":_txtEmail.text};
    
    [_myservice updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
        }else if (model.res == -1){
            [bself showTipViewWithMsg:@"会话超时，正在重新登录，请稍后重试！"];
        }
        else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"修改邮箱失败"];
    }];
    
    [SVProgressHUD show];
    
}

#pragma mark - UItextFieldDelegate
- (void)ZRBtextFieldDidBeginEditing:(UITextField *)textField{

}

- (BOOL)ZRBtextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    long int length = textField.text.length - range.length + string.length;
    
    if(length>0){
        _item.contentBarItem.enabled = YES;
    }else{
        _item.contentBarItem.enabled = NO;
    }
    return YES;
}

@end
