//
//  MinePersonalSetUNameViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心--修改用户名

#import "ZRB_ViewController.h"

@interface MinePersonalSetUNameViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, strong) NSString *uname;

@end
