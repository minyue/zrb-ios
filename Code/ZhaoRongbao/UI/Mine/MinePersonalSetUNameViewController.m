//
//  MinePersonalSetUNameViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalSetUNameViewController.h"
#import "ZRB_TextField.h"
#import "TOTextInputChecker.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"

@interface MinePersonalSetUNameViewController ()
{
    IBOutlet ZRB_TextField  *_txtUname;
    TOTextInputChecker *_nameChecker;
    CustomBarItem *_item;
    MineService *_myservice;
}
@end

@implementation MinePersonalSetUNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- UI

- (void)setup{
    WS(bself);
    [self setRightItem];
    _nameChecker = [TOTextInputChecker userNameChecker];
    _nameChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself userIconActive:flag];
        
        if(flag){
            _item.contentBarItem.enabled = YES;
        }else{
            _item.contentBarItem.enabled = NO;
        }
    };
    NSString *holder = [self.uname isEqualToString:@""] ? @"":self.uname;
    [_txtUname initWithTitle:@"用户名" placeHolder:holder];

    _txtUname.delegate = _nameChecker;
    _txtUname.showIconWhenSuccess = YES;
    _txtUname.textFiled.clearButtonMode = UITextFieldViewModeNever;
    _myservice = [[MineService alloc] init];
   
    if(_uname){
        _txtUname.text = _uname;
    }
}

- (void)setRightItem
{
    NSString *title = @"保存";
    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_item setOffset:-2];
    [_item addTarget:self selector:@selector(saveUser:) event:UIControlEventTouchUpInside];
    
}

- (void)userIconActive:(BOOL)isActive
{
    [_txtUname showIconView:isActive];
    
}

#pragma mark -- Data
/**
 *  保存更新的用户信息
 *
 *  @param sender
 */
- (void)saveUser:(id)sender
{
    if(_txtUname.text.length < 1)
        return;
    
    WS(bself)
    NSDictionary *dic = @{@"uname":_txtUname.text};
    
    [_myservice updateAppUserWithRequest:dic success:^(id responseObject) {
        [SVProgressHUD dismiss];
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res == 1){
            [bself.navigationController popViewControllerAnimated:YES];
        }else if (model.res == -1){
            [bself showTipViewWithMsg:@"会话超时，正在重新登录，请稍后重试！"];
        }else{
            [bself showTipViewWithMsg:model.msg];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"用户名已存在"];
    }];
    
    [SVProgressHUD show];
    
}

#pragma mark - UItextFieldDelegate
- (void)ZRBtextFieldDidBeginEditing:(UITextField *)textField{
    
}

- (BOOL)ZRBtextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    long int length = textField.text.length - range.length + string.length;
    
    if(length>0){
        _item.contentBarItem.enabled = YES;
    }else{
        _item.contentBarItem.enabled = NO;
    }
    return YES;
}

- (BOOL)ZRBtextFieldShouldClear:(UITextField *)textField{
    return NO;
}

@end
