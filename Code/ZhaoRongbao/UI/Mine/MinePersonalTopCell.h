//
//  MinePersonalTopCell.h
//  ZhaoRongbao
//
//  Created by zouli on 15/8/11.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  个人中心 - 修改头像Cell

#import <UIKit/UIKit.h>

@interface MinePersonalTopCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView   *myIcon;
-(void)initWithModel:(ZRB_UserModel*)user;

@end
