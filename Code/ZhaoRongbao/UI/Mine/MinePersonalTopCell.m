//
//  MinePersonalTopCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/11.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MinePersonalTopCell.h"

@interface MinePersonalTopCell()
{
    
}

@end

@implementation MinePersonalTopCell

- (void)awakeFromNib {
    // Initialization code
    _myIcon.layer.cornerRadius = _myIcon.frameWidth/2;
    _myIcon.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initWithModel:(ZRB_UserModel*)user
{
//    if(user.avatar){
//        if([ZRBUtilities ZRB_ifCachedIcon:user.avatar]){
//            [_myIcon setImage:[ZRBUtilities ZRB_UserDefaultImage]];
//        }else{
//            [_myIcon sd_setImageWithURL:[ZRBUtilities urlWithIconPath:user.avatar] placeholderImage:[ZRBUtilities ZRB_UserDefaultImage] options:SDWebImageRetryFailed];
//        }
//    }
    
    UIImage *img = [ZRBUtilities ZRBUserIconFile];
    if(!img){
        img = [ZRBUtilities ZRB_UserDefaultImage];
    }
    
    if(user.avatar){
        [_myIcon sd_setImageWithURL:[ZRBUtilities urlWithIconPath:user.avatar] placeholderImage:img options:SDWebImageRetryFailed];
    }else{
        [_myIcon setImage:img];
    }
}

@end
