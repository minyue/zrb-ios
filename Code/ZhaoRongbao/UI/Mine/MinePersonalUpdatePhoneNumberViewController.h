//
//  MinePersonalUpdatePhoneNumberViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  个人中心--更换手机号

#import "ZRB_ViewController.h"

@interface MinePersonalUpdatePhoneNumberViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, strong) NSString *umobile;

@end
