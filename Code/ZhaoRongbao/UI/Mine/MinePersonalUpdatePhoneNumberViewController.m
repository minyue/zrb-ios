//
//  MinePersonalUpdatePhoneNumberViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/18.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalUpdatePhoneNumberViewController.h"
#import "TOTextInputChecker.h"
#import "MineEnterpriseCerViewController.h"
#import "RegisterService.h"
#import "MineService.h"
#import "JKCountDownButton.h"
#import "RegisterService.h"
#import "RegisterModel.h"
#import "MineService.h"
#import "UIButton+ZRB.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface MinePersonalUpdatePhoneNumberViewController ()
{
    IBOutlet  ZRB_TextField  *_phoneTextFiled;
    
    IBOutlet  ZRB_TextField  *_vercodeTextFiled;
    
    IBOutlet  UIButton       *_nextButton;      //完成
    
    IBOutlet  UILabel        *_lblPhone;
    
    IBOutlet  UIScrollView   *_scrollView;
    
    JKCountDownButton        *_countDownCode;   //计时器
    
    TOTextInputChecker        *_phoneChecker;   //电话号码检查器
    
    TOTextInputChecker        *_codeChecker;    //验证码检查器
    
    RegisterService           *_service;
    
    MineService               *_myservice;
}
@end

@implementation MinePersonalUpdatePhoneNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup
{
    WS(bself);
    _phoneChecker = [TOTextInputChecker telChecker:YES];
    _phoneChecker.checkInputBlock = ^(BOOL flag)
    {
        
        [bself countCodeActive:flag];
        
    };
    [_phoneTextFiled initWithTitle:@"+86" placeHolder:@"手机号"];
    
    _phoneTextFiled.delegate = _phoneChecker;
    
    _countDownCode = [JKCountDownButton buttonWithType:UIButtonTypeCustom];
    [_countDownCode setTitle:@"获取验证码" forState:UIControlStateNormal];
    _countDownCode.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [_countDownCode setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
    [_countDownCode.titleLabel setFont:[UIFont systemFontOfSize:11.0f]];
    
    _countDownCode.layer.cornerRadius = 15;
    _countDownCode.layer.masksToBounds = YES;
    _countDownCode.enabled = NO;
    [_countDownCode addToucheHandler:^(JKCountDownButton*sender, NSInteger tag) {
        sender.enabled = NO;
        [bself getSMSCode];
        [sender startWithSecond:60];
        [sender didChange:^NSString *(JKCountDownButton *countDownButton,int second) {
            //NSString *title = [NSString stringWithFormat:@"剩余%d秒",second];
            NSString *title = [NSString stringWithFormat:@"%ds",second];
            return title;
        }];
        [sender didFinished:^NSString *(JKCountDownButton *countDownButton, int second) {
            countDownButton.enabled = YES;
            return @"重新获取";
            
        }];
    }];
    
    _codeChecker = [TOTextInputChecker codeChecker];
    _codeChecker.checkInputBlock = ^(BOOL flag)
    {
        
        [bself nextButtonActive:flag];
    };
    
    [_vercodeTextFiled initWithTitle:@"验证码" placeHolder:@"请输入验证码" customView:_countDownCode];
    _vercodeTextFiled.delegate = _codeChecker;

    if(_umobile)
        _lblPhone.text = [NSString stringWithFormat:@"当前手机号: %@",_umobile];
    
    [_nextButton setBackgroundColor:[UIColor colorWithHexString:@"4997ec"] forState:UIControlStateNormal];
    [_nextButton setBackgroundColor:[UIColor colorWithHexString:@"BBBBBB"] forState:UIControlStateDisabled];
    [_nextButton setTitleColor:[UIColor whiteColor]];
    _nextButton.layer.cornerRadius = 2.0f;
    _nextButton.layer.masksToBounds = YES;
    [self nextButtonActive:NO];
    
    _service = [[RegisterService alloc] init];
    _myservice = [[MineService alloc] init];
}



/**
 *  获取验证码是否被激活
 *
 *  @param isActive YES:可用   NO:不可用
 */
- (void)countCodeActive:(BOOL)isActive
{
    if (isActive) {
        _countDownCode.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        _countDownCode.enabled = YES;
        [_countDownCode setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
    }else
    {
        _countDownCode.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        _countDownCode.enabled = NO;
        [_countDownCode setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
        
    }
    
}

/**
 *  控制下一步的状态
 *
 *  @param isActive 是否激活
 */
- (void)nextButtonActive:(BOOL)isActive
{
    if (isActive)
    {
        _nextButton.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        _nextButton.enabled = YES;
        [_nextButton setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
        
    }else
    {
        _nextButton.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
        _nextButton.enabled = NO;
        [_nextButton setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
    }
}


/**
 *  完成
 *
 *  @param sender
 */
- (IBAction)goNext:(id)sender
{
    if ([_phoneChecker finalCheck] != InputCheckErrorNone || [_codeChecker finalCheck] != InputCheckErrorNone){
        [self showTipViewWithMsg:@"您的手机号或验证码有误！"];
        return;
    }
    
    //校验手机号
    [self checkMobileRegister];
    [SVProgressHUD show];
}


#pragma mark -
#pragma mark - 网络请求

/**
 *  获取短信验证码
 */
- (void)getSMSCode
{
    [_myservice sendUpdateUmobileSMSRegisterWithTel:_phoneTextFiled.text success:^(id responseObject) {
        
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        if (model.res != ZRBHttpSuccssType)
        {
            [self showTipViewWithMsg:model.msg];
            [_countDownCode stop];
            
        }
        
    } failure:^(NSError *error) {
        [self showTipViewWithMsg:kHttpFailError];
        
    }];
}


/**
 *  检查手机号是否已经注册
 */
- (void)checkMobileRegister
{
    WS(bself)
    [_service checkMobileRegisterWithTel:_phoneTextFiled.text success:^(id responseObject) {
        
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        
        if (model.res == ZRBHttpSuccssType)
        {
//            [bself checkSMSRegister];
            [bself submitChangeMobile:nil];
        }else
        {
            [self showTipViewWithMsg:model.msg];
            
            [SVProgressHUD dismiss];
            
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:kHttpFailError];
        
    }];
    
}


/**
 *  检查验证码是否正确
 */
- (void)checkSMSRegister
{
    [_service checkSMSCodeWithTel:_phoneTextFiled.text code:_vercodeTextFiled.text success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        
        if (model.res == ZRBHttpSuccssType) {
            //进入下一步
            [self submitChangeMobile:(NSString*)model.data];
            
        }else
        {
            [self showTipViewWithMsg:model.msg];
            
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:kHttpFailError];
        
    }];
}


/**
 *  提交修改
 */
- (void)submitChangeMobile:(NSString*)code
{
    NSDictionary *dic = @{@"mobile":_phoneTextFiled.text,@"smsCode":_vercodeTextFiled.text};
    NSLog(@"mobile:%@ smsCode:%@",_phoneTextFiled.text,code);
    [_myservice changeUserMobileWithRequest:dic success:^(BOOL back) {
        [SVProgressHUD dismiss];
        if(back){
            [MBProgressHUD showHUDTitle:@"修改成功" onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
            dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 1.5 *NSEC_PER_SEC);
            dispatch_after(afterTime, dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [self showTipViewWithMsg:@"效验码过期或者不存在"];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showErrorWithStatus:kHttpFailError];
    }];
    [SVProgressHUD show];
    
}

- (void)dealloc
{
    //释放的时候，关闭计时器
    [_countDownCode stop];
}


@end
