//
//  MinePersonalZRBAlertView.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/14.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  弹出框——提示 三种样式

#import <UIKit/UIKit.h>
#import "UIAlertPopAnimationView.h"


@interface MinePersonalZRBAlertView : UIAlertPopAnimationView



-(id)initWithText:(NSMutableAttributedString*)attString;

@end

