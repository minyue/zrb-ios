//
//  MinePersonalZRBAlertView.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/14.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MinePersonalZRBAlertView.h"

@interface MinePersonalZRBAlertView()

@property (nonatomic, weak) IBOutlet UILabel *lblShow;

@end

@implementation MinePersonalZRBAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(id)initWithText:(NSMutableAttributedString*)attString{
    if(self == [super init]){
        self = [[[NSBundle mainBundle] loadNibNamed:@"MinePersonalZRBAlertView" owner:self options:nil]  lastObject];
//        self.alertViewEnterpriseView
//        [self initWithContentView:_alertViewEnterpriseView];
        self.lblShow.attributedText = attString;
    }
    return self;
}

-(IBAction)clickCall:(id)sender{
    [self hideAlertAction:sender];
    //呼叫
//    NSString *telUrl = @"tel://400-688-0101";
    NSString *telUrl = @"tel://400-027-5955";
    
//    NSString *telUrl = @"tel://400-2525-888";
    NSURL *url = [[NSURL alloc] initWithString:telUrl];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)clickCancel:(id)sender{
    [self hideAlertAction:sender];
}

@end
