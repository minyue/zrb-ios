//
//  MinePersonalZRBPwdError.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/19.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlertPopAnimationView.h"

@protocol MinePersonalZRBPwdErrorViewDelegate <NSObject>

- (void)mZRB_AlertViewFindPwd;
- (void)mZRB_AlertViewValidPwd;

@end

@interface MinePersonalZRBPwdError : UIAlertPopAnimationView<MinePersonalZRBPwdErrorViewDelegate>
@property (nonatomic, assign) id <MinePersonalZRBPwdErrorViewDelegate> delegate;

-(id)initWithTextFieldError;
@end
