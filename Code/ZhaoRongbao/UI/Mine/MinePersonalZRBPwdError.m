//
//  MinePersonalZRBPwdError.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/19.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalZRBPwdError.h"

@implementation MinePersonalZRBPwdError

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithTextFieldError{
    if(self == [super init]){
        self = [[[NSBundle mainBundle] loadNibNamed:@"MinePersonalZRBPwdError" owner:self options:nil]  lastObject];
    }
    return self;
}

-(IBAction)clickCancel:(id)sender{
    [self hideAlertAction:sender];
}

//找回密码
-(IBAction)clickFindPwd:(id)sender{
    [self clickCancel:nil];
    if([self.delegate respondsToSelector:@selector(mZRB_AlertViewFindPwd)]){
        [self.delegate mZRB_AlertViewFindPwd];
    }
}

//重新弹出验证框
- (IBAction)clickError_OK{
    [self clickCancel:nil];
    if([self.delegate respondsToSelector:@selector(mZRB_AlertViewValidPwd)]){
        [self.delegate mZRB_AlertViewValidPwd];
    }
}

@end
