//
//  MinePersonalZRBTypePwdView.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/19.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlertPopAnimationView.h"

@protocol MinePersonalZRBTypePwdViewDelegate <NSObject>

- (void)mZRB_AlertViewChangePwd:(NSString *)pwd;

@end
@interface MinePersonalZRBTypePwdView : UIAlertPopAnimationView<MinePersonalZRBTypePwdViewDelegate>
@property (nonatomic, assign) id <MinePersonalZRBTypePwdViewDelegate> delegate;

-(id)initWithTextField;
@end
