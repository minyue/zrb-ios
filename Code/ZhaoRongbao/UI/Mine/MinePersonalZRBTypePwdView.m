//
//  MinePersonalZRBTypePwdView.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/19.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MinePersonalZRBTypePwdView.h"
@interface MinePersonalZRBTypePwdView(){
    
   
}
@property (nonatomic, weak) IBOutlet UITextField *txtPwd;
@end

@implementation MinePersonalZRBTypePwdView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithTextField{
    if(self == [super init]){
        self = [[[NSBundle mainBundle] loadNibNamed:@"MinePersonalZRBTypePwdView" owner:self options:nil]  lastObject];
        //        self.alertViewEnterpriseView.hidden = YES;
        //        self.alertViewEnterpriseView = self.alertViewTypePwdView;
//        [self initWithContentView:_alertViewTypePwdView];
        //        [self.altView addSubview:_alertViewTypePwdView];
        //        [self addSubview:self.alertViewTypePwdView];
        //        [self.alertViewTypePwdView setCenter:self.alertViewEnterpriseView.center];
        
        
    }
    return self;
}

-(IBAction)clickCancel:(id)sender{
    [self hideAlertAction:sender];
}

//验证密码
-(IBAction)clickToValidPwd:(id)sender{
    [self clickCancel:nil];
    if([self.delegate respondsToSelector:@selector(mZRB_AlertViewChangePwd:)]){
        [self.delegate mZRB_AlertViewChangePwd:_txtPwd.text];
    }
}

- (void)drawRect:(CGRect)rect{
    if(SCREEN_HEIGHT == 480){
        [self.altView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.superview.mas_top).offset(70);
        }];
    }
}
@end
