//
//  MineProjectBehaviorChartView.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  用户行为图标

#import <UIKit/UIKit.h>

@interface MineProjectBehaviorChartView : UIView

-(void)initWithData:(NSArray*)array;

@end
