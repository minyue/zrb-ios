//
//  MineProjectBehaviorChartView.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProjectBehaviorChartView.h"
#import "UUChart.h"
#import "NSDate+Addition.h"

@interface MineProjectBehaviorChartView()<UUChartDataSource>
{
    UUChart *_chartView;
    int _minY;
    int _maxY;
}
@property (nonatomic,strong) NSMutableArray *arrayX;
@property (nonatomic,strong) NSMutableArray *arrayY;

@end
@implementation MineProjectBehaviorChartView

-(id)initWithFrame:(CGRect)frame{
    if(self == [super initWithFrame:frame]){
        
    }
    return self;
}

-(void)initWithData:(NSArray*)array{
    if(array){
        NSMutableArray *xArray = [[NSMutableArray alloc] init];
        NSMutableArray *yArray = [[NSMutableArray alloc] init];
        for(NSDictionary *dic in array){
//            NSString *dateStr = [NSDate couponDateFormatString:[dic valueForKey:@"date"] format:NSATE_FORMAT_COUPON_6];
            NSString *dateStr = [dic valueForKey:@"date"];
//            [xArray addObject:[NSDate dateWithString:[dic valueForKey:@"date"] format:NSATE_FORMAT_COUPON_6]];
            [xArray addObject:dateStr];
            [yArray addObject:[dic valueForKey:@"number"]];
        }
        _arrayX = xArray;
        _arrayY = yArray;
        
        //线性统计表
        _chartView = [[UUChart alloc]initwithUUChartDataFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 150) withSource:self withStyle:UUChartLineStyle];
        [_chartView showInView:self];
    }
}
#pragma mark - @required
//横坐标标题数组
- (NSArray *)UUChart_xLableArray:(UUChart *)chart
{
    return _arrayX;
}

//数值多重数组  数值
- (NSArray *)UUChart_yValueArray:(UUChart *)chart
{

    if(_arrayY){
        float maxy = 0.0;
        for(NSString* i in _arrayY){
            if([i floatValue] > maxy){
                maxy = [i floatValue];
                continue;
            }
        }
        _maxY = maxy;
    }
    return @[_arrayY];
}

#pragma mark - @optional
//颜色数组
- (NSArray *)UUChart_ColorArray:(UUChart *)chart
{
    return @[UUYellow,UUBlue,UURed];
}

//显示数值范围
- (CGRange)UUChartChooseRangeInLineChart:(UUChart *)chart
{
    if(_maxY <20){
        return CGRangeMake(20, 0);
    }else{
        return CGRangeMake(_maxY, 0);
    }
    
}

#pragma mark 折线图专享功能

//标记数值区域
- (CGRange)UUChartMarkRangeInLineChart:(UUChart *)chart
{
    return CGRangeZero;
}

//判断显示横线条
- (BOOL)UUChart:(UUChart *)chart ShowHorizonLineAtIndex:(NSInteger)index
{
    return YES;
}

//判断显示最大最小值
- (BOOL)UUChart:(UUChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return YES;
}

@end
