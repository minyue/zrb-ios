//
//  MineProjectCommentStatisticDetailCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的项目详情-回复统计 详情 Cell

#import <UIKit/UIKit.h>
static NSString *const mineProjectCommentStatisticDetailCellIdentifier = @"MineProjectCommentStatisticDetailCellIdentifier";

@interface MineProjectCommentStatisticDetailCell : UITableViewCell

- (void)initWithData:(NSDictionary*)dic;

@end
