//
//  MineProjectCommentStatisticDetailCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProjectCommentStatisticDetailCell.h"
#import <QuartzCore/QuartzCore.h>

@interface MineProjectCommentStatisticDetailCell()
{
    IBOutlet UILabel    *_lblName;
    IBOutlet UILabel    *_lblPercent;
    IBOutlet UIView     *_percentBGView;
}
@end

@implementation MineProjectCommentStatisticDetailCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithData:(NSDictionary*)dic{
    if(dic){
        _lblName.text = [dic objectForKey:@"name"];
       NSString *percent =  [dic objectForKey:@"percent"];
        _lblPercent.text = percent;
  
        CGRect frame = _percentBGView.bounds;
        float lineWidth = [percent floatValue]*(frame.size.width)*0.01;
        if(lineWidth > 0){
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointMake(frame.origin.x,frame.origin.y)];//startpoint
            [path addLineToPoint:CGPointMake(frame.origin.x + lineWidth,frame.origin.y)];//endpoint
            [path closePath];
            shapeLayer.path = path.CGPath;
            shapeLayer.strokeColor = RGB(41, 94, 214).CGColor;
            shapeLayer.fillColor = RGB(41, 94, 214).CGColor;
            shapeLayer.lineWidth = frame.size.height;
            [_percentBGView.layer addSublayer:shapeLayer];
        }
    }
}
@end
