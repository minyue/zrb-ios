//
//  MineProjectNumStatisticCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的项目详情 - 各种次数统计

#import <UIKit/UIKit.h>
#import "ProjectListCell.h"

static NSString *const mineProjectNumStatisticCellIdentifier = @"MineProjectNumStatisticCellIdentifier";

@interface MineProjectNumStatisticCell : UITableViewCell

-(void)initWithModel:(ProjectItemModel*)model;

@end
