//
//  MineProjectNumStatisticCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProjectNumStatisticCell.h"

@interface MineProjectNumStatisticCell(){
    IBOutlet UILabel    *_lblInviteNum;
    IBOutlet UILabel    *_lblScanNum;
    IBOutlet UILabel    *_lblInspectNum;
}
@end

@implementation MineProjectNumStatisticCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initWithModel:(ProjectItemModel*)model{
    _lblInviteNum.text = [NSString stringWithFormat:@"%d",model.inviteNum];
    _lblScanNum.text = [NSString stringWithFormat:@"%d",model.viewNum];
    _lblInspectNum.text = [NSString stringWithFormat:@"%d",model.inspectNum];
    
}
@end
