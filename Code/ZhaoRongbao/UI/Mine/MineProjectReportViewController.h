//
//  MineProjectReportViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/31.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的项目 详情

#import "ZRB_ViewController.h"

@interface MineProjectReportViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, strong) NSString *projectId;

@end
