//
//  MineProjectReportViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/31.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProjectReportViewController.h"
#import "ProjectService.h"
#import "ProjectListCell.h"
#import "MineProjectTopCell.h"
#import "MineProjectNumStatisticCell.h"
#import "MIneProjectCommentStatisticCell.h"
#import "MineProjectCommentStatisticDetailCell.h"
#import "MineProjectUserBehaviorAnalysisCell.h"
//#import "AllCommentViewController.h"

static NSString *const mineProjectDetailPaddingCellIdentifier = @"MineProjectDetailPaddingCellIdentifier";
static NSString *const mineProjectDetailWhitePaddingCellIdentifier = @"MineProjectDetailWhitePaddingCellIdentifier";


@interface MineProjectReportViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView    *_tableView;
    
    ProjectService          *_projectService;
    
    NSMutableArray          *_listArray;
    
    ProjectItemModel        *_currentModel;
}

@end

@implementation MineProjectReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
     self.navigationController.navigationBar.barTintColor = RGBA(2, 145, 247, 0.8);
    if(_tableView){
        [_tableView setContentOffset:CGPointZero];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%f",scrollView.contentOffset.y);
    if(scrollView.contentOffset.y > 0){
        self.navigationController.navigationBar.hidden = NO;
        self.navigationController.navigationBar.barTintColor = RGBA(2, 145, 247, 0.8);
    }else{
        self.navigationController.navigationBar.hidden = YES;
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"0077d9"];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)setup
{
    _projectService = [[ProjectService alloc] init];
    _listArray = [NSMutableArray new];
    [self requestData];
}

- (void)requestData{
    
    [_projectService myProjectDetailWithRequest:_projectId success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if(responseObject){
            _currentModel = (ProjectItemModel*)responseObject;
            [_tableView reloadData];
        }else{
            [self showTipViewWithMsg:@"暂无数据"];
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"请求失败，请稍后再试"];
    }];
    
    [SVProgressHUD show];
}

- (IBAction)backController:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - 列表相关方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 5){
        return _currentModel.commentStatistics.count+1;
    }else{
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        //头部
        MineProjectTopCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineProjectTopCellIdentifier forIndexPath:indexPath];
        [cell   initWithModel:_currentModel];
        return cell;
        
    }else if(indexPath.section == 1){
        //统计
        MineProjectNumStatisticCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineProjectNumStatisticCellIdentifier forIndexPath:indexPath];
        [cell   initWithModel:_currentModel];
        return cell;
    }else if (indexPath.section == 2 || indexPath.section == 4){
        //空隙
        UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:mineProjectDetailPaddingCellIdentifier forIndexPath:indexPath];
        return cell;
    }else if (indexPath.section == 3){
        //行为分析
        MineProjectUserBehaviorAnalysisCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineProjectUserBehaviorAnalysisCellIdentifier forIndexPath:indexPath];
        [cell   initWithModel:_currentModel];
        return cell;
    }else if(indexPath.section == 6){
        //底部空白空隙
        UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:mineProjectDetailWhitePaddingCellIdentifier forIndexPath:indexPath];
        return cell;
    }else{
        //回复统计
        if(indexPath.row == 0){
            MIneProjectCommentStatisticCell *cell = [_tableView dequeueReusableCellWithIdentifier:mIneProjectCommentStatisticCell forIndexPath:indexPath];
            [cell   initWithModel:_currentModel];
            return cell;
        }else{
            //回复统计TableView
            MineProjectCommentStatisticDetailCell *cell= [_tableView dequeueReusableCellWithIdentifier:mineProjectCommentStatisticDetailCellIdentifier forIndexPath:indexPath];
            [cell   initWithData:[_currentModel.commentStatistics objectAtIndex:indexPath.row-1]];
            return cell;
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return 186.0f;
            break;
        case 1:
            return 100.0f;
            break;
        case 2:
            return 10.0f;
            break;
        case 3:
            return 251.0f;
            break;
        case 4:
            return 10.0f;
            break;
        case 5:
            if(indexPath.row == 0){
                return 45.0f;
            }else{
                return 30.0f;
            }
            break;
        case 6:
            return 15.0f;
            break;
        default:
            break;
    }
    return 0;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 5){
//        AllCommentViewController *allCommentCtrl = [[AllCommentViewController alloc]init];
//        allCommentCtrl.projectID = self.projectId;
//        [self.navigationController pushViewController:allCommentCtrl animated:YES];
    }
    
}


@end
