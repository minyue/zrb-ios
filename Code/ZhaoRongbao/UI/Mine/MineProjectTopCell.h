//
//  MineProjectTopCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/31.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的项目详情 头部

#import <UIKit/UIKit.h>
#import "ProjectListCell.h"

static NSString *const mineProjectTopCellIdentifier = @"MineProjectTopCellIdentifier";

@interface MineProjectTopCell : UITableViewCell

-(void)initWithModel:(ProjectItemModel*)model;
@end
