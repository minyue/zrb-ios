//
//  MineProjectTopCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/31.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProjectTopCell.h"
#import "ProjectTypeModel.h"

@interface MineProjectTopCell (){
    IBOutlet UIImageView *_imgBG;
    IBOutlet UIImageView *_imgIcon;
    IBOutlet UILabel    *_lblTitle;
    IBOutlet UILabel    *_lblTakeTime;
    IBOutlet UILabel    *_lblPrjTotalMoney;
}
@end
@implementation MineProjectTopCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initWithModel:(ProjectItemModel*)model{
    NSString *imageUrlStr = [NSString stringWithFormat:@"%@/%@",IMAGE_SERVER_URL,model.pic];
    [_imgBG sd_setImageWithURL:[NSURL URLWithString:imageUrlStr] placeholderImage:nil];
    [_imgIcon sd_setImageWithURL:[NSURL URLWithString:imageUrlStr] placeholderImage:nil];
    _lblTitle.text = model.projectName;
    if(model.period)
        _lblTakeTime.text = [NSString stringWithFormat:@"招商时长:%@",model.period];
    if(model.investment)
        _lblPrjTotalMoney.text = [NSString stringWithFormat:@"项目总额:%@",model.investment];
    
}

@end
