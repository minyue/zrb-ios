//
//  MineProjectUserBehaviorAnalysisCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的项目详情 - 用户行为分析

#import <UIKit/UIKit.h>
#import "ProjectListCell.h"

static NSString *const mineProjectUserBehaviorAnalysisCellIdentifier = @"MineProjectUserBehaviorAnalysisCellIdentifier";

@interface MineProjectUserBehaviorAnalysisCell : UITableViewCell

-(void)initWithModel:(ProjectItemModel*)model;
@end
