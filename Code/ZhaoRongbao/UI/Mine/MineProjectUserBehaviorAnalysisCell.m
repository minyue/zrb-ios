//
//  MineProjectUserBehaviorAnalysisCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/1.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProjectUserBehaviorAnalysisCell.h"
#import "MineProjectBehaviorChartView.h"
//#import "NewsHomeHrizontalMenu.h"

@interface MineProjectUserBehaviorAnalysisCell(){

    IBOutlet   UIScrollView  *_scrollView;
    
    IBOutlet   UIView  *_scrollLine;
    
    IBOutlet    UIButton *_btn1;
    IBOutlet    UIButton *_btn2;
    IBOutlet    UIButton *_btn3;
}
-(IBAction)doSomethingValueChange:(id)sender;
@end
@implementation MineProjectUserBehaviorAnalysisCell

- (void)awakeFromNib {
    // Initialization code
//    [self setup];
}

//-(void)setup{
//    
//    
//    _segmentedControl.tintColor = RGB(41, 94, 214);
//    _segmentedControl.selectedSegmentIndex = 0;//默认选中的按钮索引
//
//    
//    //设置分段控件点击相应事件
//    [_segmentedControl addTarget:self action:@selector(doSomethingInSegment:)forControlEvents:UIControlEventValueChanged];
//    
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initWithModel:(ProjectItemModel*)model{
    if(model){
        for (int i = 0; i < 3; i ++)
        {
            MineProjectBehaviorChartView *view = [[MineProjectBehaviorChartView alloc] initWithFrame:self.bounds];
            NSArray *array;
            switch (i) {
                case 0:
                    array = model.collectionStatistics;//关注
                    break;
                case 1:
                    array = model.shareStatistic;//分享
                    break;
                case 2:
                    array = model.inspectStatistics;//勘察
                    break;
                default:
                    break;
            }
            [view initWithData:array];
            [_scrollView addSubview:view];
            
            view.frameSize = _scrollView.frameSize;
            view.frameX = _scrollView.frameWidth * i;
            view.frameY = 0;
            _scrollView.contentSize = CGSizeMake(_scrollView.frameWidth * (i + 1), _scrollView.contentSize.height);
        }

    }
}
//
//-(void)doSomethingInSegment:(id)sender{
//    UISegmentedControl *co = (UISegmentedControl*)sender;
//
//    [_scrollView setContentOffset:CGPointMake(_scrollView.frameWidth * co.selectedSegmentIndex, _scrollView.contentOffset.y) animated:YES];
//}

-(IBAction)doSomethingValueChange:(id)sender{
    UIButton *co = (UIButton*)sender;
 
    switch (co.tag) {
        case 0:
            _btn1.selected = YES;
            _btn2.selected = NO;
            _btn3.selected = NO;
            break;
        case 1:
            _btn1.selected = NO;
            _btn2.selected = YES;
            _btn3.selected = NO;
            break;
        case 2:
            _btn1.selected = NO;
            _btn2.selected = NO;
            _btn3.selected = YES;
            break;
        default:
            break;
    }
    
    [_scrollView setContentOffset:CGPointMake(_scrollView.frameWidth * co.tag, _scrollView.contentOffset.y) animated:YES];
    
    [UIView beginAnimations:nil context:nil];//动画开始
    [UIView setAnimationDuration:0.3];
    CGRect frame = co.frame;
    _scrollLine.frame = CGRectMake(frame.origin.x, _scrollLine.frame.origin.y, _scrollLine.frame.size.width, _scrollLine.frame.size.height);
    
    [UIView commitAnimations];
}
@end
