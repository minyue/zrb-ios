//
//  MineProjectsViewController.m
//  ZhaoRongbao
//
//  Created by  on 15/8/28.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProjectsViewController.h"
#import "MJRefresh.h"
#import "MineService.h"
#import "MineAttentionProjectsViewCell.h"
#import "MineProjectListModel.h"
#import "SearchProjectDetailViewController.h"

@interface MineProjectsViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *_tableView;
    
    MineService         *_projectService;
    
    NSMutableArray      *_listArray;
    
    BOOL       _isRequst;            //是否请求过
}
@end

@implementation MineProjectsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup
{
    _isRequst = NO;
    _projectService = [[MineService alloc] init];
    _listArray = [NSMutableArray new];
    [self addHeaderView];
    [self addFooterView];
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}


#pragma mark -
#pragma mark -网络请求



/**
 *  请求我关注的项目列表
 */
- (void)queryListData
{
    _isRequst = NO;
    
    MineProjectListRequest *request = [[MineProjectListRequest alloc]init];
    request.limit = 20;
    request.pushTime = @"";
    
    [self startWithReq:request];
    
}


/**
 *  加载更多
 */
- (void)loadMoreData
{
    if(_listArray.count > 0){
        
        ProjectItemModel *lastModel = [_listArray lastObject];
        LogInfo([NSString stringWithFormat:@"%@",lastModel.pushTime]);
        MineProjectListRequest *request = [[MineProjectListRequest alloc]init];
        request.limit = 20;
//        request.pushTime = [ZRBUtilities stringToData:@"yyyy-MM-dd HH:mm:ss" interval:lastModel.pushTime];
        request.pushTime = lastModel.pushTime;
        LogInfo(request.pushTime);
        
        [self loadMoreWithReq:request];
    }else{
        [_tableView.header endRefreshing];
        
        [_tableView.footer endRefreshing];
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
}


/**
 *  下拉加载
 *
 *  @param req 请求体
 */

- (void)startWithReq:(MineProjectListRequest *)req
{
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_projectService mineProjectQueryListWithRequest:req success:^(id responseObject) {
        [bself projectQueryListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(MineProjectListRequest   *)req
{
    
    WS(bself);
    [_projectService mineProjectQueryListWithRequest:req success:^(id responseObject) {
        [bself loadMoreListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
    
}




- (void)projectQueryListCallBackWithObject:(NSMutableArray *)modelArray
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    if(modelArray){
        _listArray = modelArray;
        [_tableView reloadData];
    }
    
    [_tableView.header endRefreshing];
    
}


- (void)loadMoreListCallBackWithObject:(NSMutableArray *)modelArray
{
    [_tableView.header endRefreshing];
    
    [_tableView.footer endRefreshing];
    if(modelArray){
        [_listArray addObjectsFromArray:modelArray];
        [_tableView reloadData];
    }else{
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
    
}

#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineAttentionProjectsViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kAttentionProjectViewCellIdetifier forIndexPath:indexPath];
    MineProjectListModel *model = _listArray[indexPath.row];
    [cell initMineProjectWithModel:model];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 110;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineProjectListModel *model = [[MineProjectListModel alloc]init];
    model = _listArray[indexPath.row];
    SearchProjectDetailViewController *detailCtrl = [[SearchProjectDetailViewController alloc]init];
    detailCtrl.projectId = model.pid;
    detailCtrl.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:detailCtrl animated:YES];
    
}

@end
