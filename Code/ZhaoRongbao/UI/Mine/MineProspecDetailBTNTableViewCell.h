//
//  MineProspecDetailBTNTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/7.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察详情 - 按钮栏

#import <UIKit/UIKit.h>
@protocol mineProspecDetailBTNTableViewCellDelegate <NSObject>
- (void)btnClick;
@end

static NSString *const mineProspecDetailBTNTableViewCellIdentifier = @"MineProspecDetailBTNTableViewCellIdentifier";

@interface MineProspecDetailBTNTableViewCell : UITableViewCell<mineProspecDetailBTNTableViewCellDelegate>
@property   (nonatomic,assign)  id <mineProspecDetailBTNTableViewCellDelegate> delegate;

- (void)initWithStatue:(int)statue;
@end
