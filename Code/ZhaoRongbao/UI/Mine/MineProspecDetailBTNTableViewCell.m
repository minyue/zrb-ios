
//
//  MineProspecDetailBTNTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/7.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspecDetailBTNTableViewCell.h"

@interface MineProspecDetailBTNTableViewCell(){
    IBOutlet    UIButton    *_btnShow;
}
- (IBAction)reP:(id)sender;
@end

@implementation MineProspecDetailBTNTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _btnShow.layer.cornerRadius = 2.0f;
    _btnShow.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithStatue:(int)statue{
    if(statue == 0){
        [_btnShow setTitle:@"重新预约" forState:UIControlStateNormal];
    }else{
        [_btnShow setTitle:@"取消预约" forState:UIControlStateNormal];
    }
}



//跳到预约项目勘察页面
- (IBAction)reP:(id)sender{
    if([self.delegate respondsToSelector:@selector(btnClick)]){
        [self.delegate btnClick];
    }
}
@end
