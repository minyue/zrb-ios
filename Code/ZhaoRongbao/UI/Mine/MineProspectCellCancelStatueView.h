//
//  MineProspectCellCancelStatueView.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/2.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察Cell中取消勘察的状态

#import <UIKit/UIKit.h>
@protocol MineProspectCellCancelStatueViewDeleteDelegate <NSObject>

- (void)deleteData:(int)index;
@end
@interface MineProspectCellCancelStatueView : UIView<MineProspectCellCancelStatueViewDeleteDelegate>
@property (nonatomic,assign) int selectIndex;
@property (nonatomic, assign) id <MineProspectCellCancelStatueViewDeleteDelegate> delegate;
@end
