//
//  MineProspectCellCancelStatueView.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/2.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspectCellCancelStatueView.h"

@interface MineProspectCellCancelStatueView()
{
    IBOutlet    UIImageView *_imgIcon;
    IBOutlet    UILabel *_lblTitle;
    
}
@property (nonatomic,assign) int cancelStatue;
@end

@implementation MineProspectCellCancelStatueView

-(id)initWithFrame:(CGRect)frame{
    if(self == [super initWithFrame:frame]){
        self = [[[UINib nibWithNibName:@"MineProspectCellCancelStatueView" bundle:[NSBundle mainBundle]] instantiateWithOwner:nil options:nil] lastObject];
        self.frame = CGRectMake(10, frame.size.height-13 -20, 77, 14);
    }
    return self;
}

-(void)awakeFromNib{
    self.cancelStatue = 0;
    [self.layer setCornerRadius:3];
    [self.layer setMasksToBounds:YES];
}

-(IBAction)clickChange{
    if(_cancelStatue == 0){
        self.cancelStatue = 1;
    }else{
//        self.cancelStatue = 0;
        //删除
        if([self.delegate respondsToSelector:@selector(deleteData:)])
        {
            [self.delegate deleteData:self.selectIndex];
        }
    }
}

-(void)setCancelStatue:(int)cancelStatue{
    if(cancelStatue == 0){
        _lblTitle.text = @"已取消勘察";
        _lblTitle.textColor = [UIColor colorWithHexString:@"999999"];
        _imgIcon.image = [UIImage imageNamed:@"delmyproject"];
        self.backgroundColor = [UIColor clearColor];
    }else{
        _lblTitle.text = @"删除勘察";
        _lblTitle.textColor = [UIColor whiteColor];
        _imgIcon.image = [UIImage imageNamed:@"delmyproject2"];
        self.backgroundColor = [UIColor colorWithHexString:@"999999"];
    }
    _cancelStatue = cancelStatue;
}

@end
