//
//  MineProspectCellStatueView.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/2.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察Cell中的状态

#import <UIKit/UIKit.h>

@interface MineProspectCellStatueView : UIView

-(void)initWithStatue:(int)statue;

@end
