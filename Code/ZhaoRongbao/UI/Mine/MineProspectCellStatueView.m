//
//  MineProspectCellStatueView.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/2.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspectCellStatueView.h"

@interface MineProspectCellStatueView(){
    IBOutlet UIView *_view1;
    IBOutlet UIView *_view2;
    IBOutlet UIView *_view3;
}
@end
@implementation MineProspectCellStatueView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithFrame:(CGRect)frame{
    if(self == [super initWithFrame:frame]){
        self = [[[UINib nibWithNibName:@"MineProspectCellStatueView" bundle:[NSBundle mainBundle]] instantiateWithOwner:self options:nil] lastObject];
        self.frame = CGRectMake(10, frame.size.height-10 -20, 175, 20);
    }
    return self;
}

-(void)awakeFromNib{
    
    [_view1.layer setCornerRadius:3];
    [_view1.layer setMasksToBounds:YES];
    [_view2.layer setCornerRadius:3];
    [_view2.layer setMasksToBounds:YES];
    [_view3.layer setCornerRadius:3];
    [_view3.layer setMasksToBounds:YES];
}

-(void)initWithStatue:(int)statue{
    switch (statue) {
        case 1:
            _view1.backgroundColor = [UIColor colorWithHexString:@"42c5a5"];
            _view2.backgroundColor = [UIColor colorWithHexString:@"cccccc"];
            _view3.backgroundColor = [UIColor colorWithHexString:@"cccccc"];
            break;
        case 2:
            _view1.backgroundColor = [UIColor colorWithHexString:@"42c5a5"];
            _view2.backgroundColor = [UIColor colorWithHexString:@"eea14b"];
            _view3.backgroundColor = [UIColor colorWithHexString:@"cccccc"];
            break;
        case 3:
            _view1.backgroundColor = [UIColor colorWithHexString:@"42c5a5"];
            _view2.backgroundColor = [UIColor colorWithHexString:@"eea14b"];
            _view3.backgroundColor = [UIColor colorWithHexString:@"5788d0"];
            break;
            
        default:
            break;
    }
}
@end
