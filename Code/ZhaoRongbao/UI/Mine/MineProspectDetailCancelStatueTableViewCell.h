//
//  MineProspectDetailCancelStatueTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察详情 - 取消勘察状态Cell

#import <UIKit/UIKit.h>

static NSString *const mineProspectDetailCancelStatueTableViewCellIdentifier = @"MineProspectDetailCancelStatueTableViewCellIdentifier";

@interface MineProspectDetailCancelStatueTableViewCell : UITableViewCell

- (void)initWithReason:(NSString*)resaon;

@end
