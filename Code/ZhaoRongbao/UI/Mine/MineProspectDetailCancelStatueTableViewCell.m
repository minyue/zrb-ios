//
//  MineProspectDetailCancelStatueTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspectDetailCancelStatueTableViewCell.h"
@interface MineProspectDetailCancelStatueTableViewCell()
{
    IBOutlet    UILabel *_lblCancelReason;
}
@end
@implementation MineProspectDetailCancelStatueTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithReason:(NSString*)resaon{
    if(resaon)
        _lblCancelReason.text = resaon;
}
@end
