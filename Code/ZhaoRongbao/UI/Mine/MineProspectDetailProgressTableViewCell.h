//
//  MineProspectDetailProgressTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察详细信息

#import <UIKit/UIKit.h>
#import "ProspectDetailModel.h"

static NSString *const mineProspectDetailProgressTableViewCellIdentifier = @"MineProspectDetailProgressTableViewCellIdentifier";

@interface MineProspectDetailProgressTableViewCell : UITableViewCell

- (void)initWithModel:(ProspectEventModel*)model andIndex:(NSInteger)index;

@end
