//
//  MineProspectDetailProgressTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspectDetailProgressTableViewCell.h"
#import "NSDate+Addition.h"

@interface MineProspectDetailProgressTableViewCell(){
    IBOutlet    UIImageView *_imgLineUp;
    IBOutlet    UIImageView *_imgCircle;
    IBOutlet    UIImageView *_imgLineDown;
    IBOutlet    UILabel *_lblTitle;
    IBOutlet    UILabel *_lblDetail;
    IBOutlet    UILabel *_lblTime;
}
@end

@implementation MineProspectDetailProgressTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _imgCircle.layer.cornerRadius = _imgCircle.frame.size.width/2;
    _imgCircle.layer.masksToBounds = YES;
    _imgCircle.backgroundColor = [UIColor lightGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithModel:(ProspectEventModel*)model andIndex:(NSInteger)index{
    if(!model)
        return;
    if(model.content)
        _lblDetail.text = model.content;
    if(model.ctime)
        _lblTime.text = [ZRBUtilities stringToData:NSDATE_FORMAT_COUPON_1 interval:[NSString stringWithFormat:@"%lld",model.ctime]];
    
    if(index == 0){
        _imgLineUp.hidden = YES;
        _imgCircle.backgroundColor = [UIColor colorWithHexString:@"0077d9"];
    }
}
@end
