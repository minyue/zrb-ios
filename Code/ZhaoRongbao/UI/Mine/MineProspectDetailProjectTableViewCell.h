//
//  MineProspectDetailProjectTableViewCell.h
//  ZhaoRongbao
//
//  Created by on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察详情 - 项目信息Cell

#import <UIKit/UIKit.h>
@protocol mineProspectDetailProjectDelegate <NSObject>
- (void)pushToProjectDetail;
- (void)pushToMap;
- (void)pushToCustomerService;
- (void)pushToProjectComment;
@end

static NSString *const mineProspectDetailProjectTableViewCellIdentifier = @"MineProspectDetailProjectTableViewCellIdentifier";

@interface MineProspectDetailProjectTableViewCell : UITableViewCell<mineProspectDetailProjectDelegate>
@property (nonatomic,assign) id <mineProspectDetailProjectDelegate> delegate;

- (void)initWithProjectName:(NSString*)name;
@end
