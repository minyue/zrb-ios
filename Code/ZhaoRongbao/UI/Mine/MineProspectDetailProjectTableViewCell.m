//
//  MineProspectDetailProjectTableViewCell.m
//  ZhaoRongbao
//
//  Created by on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspectDetailProjectTableViewCell.h"

@interface MineProspectDetailProjectTableViewCell(){
    IBOutlet    UIView  *_view1;
    IBOutlet    UIView  *_view2;
    IBOutlet    UIView  *_view3;
    IBOutlet    UILabel *_lblName;
}
@end

@implementation MineProspectDetailProjectTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _view1.layer.cornerRadius = 4.0f;
    _view1.layer.masksToBounds = YES;
    _view1.layer.borderWidth = 1;
    _view1.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
    _view2.layer.cornerRadius = 4.0f;
    _view2.layer.masksToBounds = YES;
    _view2.layer.borderWidth = 1;
    _view2.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
    _view3.layer.cornerRadius = 4.0f;
    _view3.layer.masksToBounds = YES;
    _view3.layer.borderWidth = 1;
    _view3.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithProjectName:(NSString*)name{
    if(name)
        _lblName.text = name;
}

- (IBAction)clickName:(id)sender{
    if([self.delegate respondsToSelector:@selector(pushToProjectDetail)]){
        [self.delegate pushToProjectDetail];
    }
}

- (IBAction)clickMap:(id)sender{
    if([self.delegate respondsToSelector:@selector(pushToMap)]){
        [self.delegate pushToMap];
    }
}

- (IBAction)clickCustomerService:(id)sender{
    if([self.delegate respondsToSelector:@selector(pushToCustomerService)]){
        [self.delegate pushToCustomerService];
    }
}

- (IBAction)clickProjectComment:(id)sender{
    if([self.delegate respondsToSelector:@selector(pushToProjectComment)]){
        [self.delegate pushToProjectComment];
    }
}

@end
