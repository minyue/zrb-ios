//
//  MineProspectDetailStatueTableViewCell.h
//  ZhaoRongbao
//
//  Created by zl on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察详情 - 勘察状态Cell

#import <UIKit/UIKit.h>

static NSString *const mineProspectDetailStatueTableViewCellIdentifier = @"MineProspectDetailStatueTableViewCellIdentifier";

@interface MineProspectDetailStatueTableViewCell : UITableViewCell

-(void)initWithStatue:(int)statue;

@end
