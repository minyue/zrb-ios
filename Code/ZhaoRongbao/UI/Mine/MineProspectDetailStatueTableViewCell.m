//
//  MineProspectDetailStatueTableViewCell.m
//  ZhaoRongbao
//
//  Created by zl on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspectDetailStatueTableViewCell.h"
@interface MineProspectDetailStatueTableViewCell()
{
    IBOutlet UIView  *_view1;
    IBOutlet UIView *_view2;
    IBOutlet UIView *_view3;
}
@end

@implementation MineProspectDetailStatueTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)awakeFromNib{
    
    [_view1.layer setCornerRadius:_view1.frame.size.width/2];
    [_view1.layer setMasksToBounds:YES];
    [_view2.layer setCornerRadius:_view2.frame.size.width/2];
    [_view2.layer setMasksToBounds:YES];
    [_view3.layer setCornerRadius:_view3.frame.size.width/2];
    [_view3.layer setMasksToBounds:YES];
}

-(void)initWithStatue:(int)statue{
    switch (statue) {
        case 1:
            _view1.backgroundColor = [UIColor colorWithHexString:@"42c5a5"];
            _view2.backgroundColor = [UIColor colorWithHexString:@"cccccc"];
            _view3.backgroundColor = [UIColor colorWithHexString:@"cccccc"];
            break;
        case 2:
            _view1.backgroundColor = [UIColor colorWithHexString:@"42c5a5"];
            _view2.backgroundColor = [UIColor colorWithHexString:@"eea14b"];
            _view3.backgroundColor = [UIColor colorWithHexString:@"cccccc"];
            break;
        case 3:
            _view1.backgroundColor = [UIColor colorWithHexString:@"42c5a5"];
            _view2.backgroundColor = [UIColor colorWithHexString:@"eea14b"];
            _view3.backgroundColor = [UIColor colorWithHexString:@"5788d0"];
            break;
            
        default:
            break;
    }
}

@end
