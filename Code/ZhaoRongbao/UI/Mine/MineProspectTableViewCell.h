//
//  MineProspectTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/2.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察列表 Cell

#import <UIKit/UIKit.h>
#import "MineProspectCellCancelStatueView.h"

static NSString *const mineProspectTableViewCellIdentifier = @"MineProspectTableViewCellIdentifier";

@interface MineProspectTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet    MineProspectCellCancelStatueView *cancelStatueView;

-(void)initWithModel:(id)model andIndex:(int)index;
@end
