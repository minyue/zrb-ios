//
//  MineProspectTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/2.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  

#import "MineProspectTableViewCell.h"
#import "MineProspectCellStatueView.h"
#import "ProspectModel.h"
#import "NSDate+Addition.h"

@interface MineProspectTableViewCell(){
    IBOutlet    UILabel *_lblTitle;
    IBOutlet    UILabel *_lblTime;
    
    IBOutlet    UIImageView *_imgIcon;
    ZRB_LineView            *_lineView;
}
@property (nonatomic, strong) IBOutlet    MineProspectCellStatueView *statueView;

@end

@implementation MineProspectTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    _lineView = [[ZRB_LineView alloc] init];
    [self.contentView addSubview:_lineView];
    
    UIView *superView = self.contentView;
    [_lineView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.leading.equalTo(superView.mas_leading).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.height.equalTo(@0.5);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initWithModel:(id)model andIndex:(int)index{
    ProspectModel *mo = (ProspectModel*)model;
    if(mo.inspectStatus == 0){
        //取消
        [_statueView removeFromSuperview];
        [_cancelStatueView removeFromSuperview];
        _statueView = nil;
        _cancelStatueView = nil;
        
        _cancelStatueView = [[MineProspectCellCancelStatueView alloc] initWithFrame:self.frame];
        _cancelStatueView.selectIndex = index;
        [self addSubview:_cancelStatueView];
    }else{
        [_statueView removeFromSuperview];
        [_cancelStatueView removeFromSuperview];
        _statueView = nil;
        _cancelStatueView = nil;
       
        _statueView = [[MineProspectCellStatueView alloc] initWithFrame:self.frame];
        [_statueView initWithStatue:mo.inspectStatus];
        [self addSubview:_statueView];
    }
    if(mo.projectName)
        _lblTitle.text = mo.projectName;
    if(mo.inspectTime)
        _lblTime.text = [ZRBUtilities stringToData:NSDATE_FORMAT_COUPON_1 interval:[NSString stringWithFormat:@"%lld",mo.inspectTime]];
    if(mo.projectPicUrl)
    {
        NSString *imageUrlStr = [NSString stringWithFormat:@"%@/%@",IMAGE_SERVER_URL,mo.projectPicUrl];
        [_imgIcon sd_setImageWithURL:[NSURL URLWithString:imageUrlStr] placeholderImage:[ZRBUtilities ZRB_DefaultImage]];
    }
}

@end
