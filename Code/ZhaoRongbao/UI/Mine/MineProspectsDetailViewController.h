//
//  MineProspectsDetailViewController.h
//  ZhaoRongbao
//
//  Created by on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的勘察 - 勘察详情页面

#import "ZRB_ViewController.h"
#import "ProspectModel.h"

@interface MineProspectsDetailViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic, strong) ProspectModel  *model;
@end
