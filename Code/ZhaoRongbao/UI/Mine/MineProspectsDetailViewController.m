//
//  MineProspectsDetailViewController.m
//  ZhaoRongbao
//
//  Created by on 15/9/6.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspectsDetailViewController.h"
#import "ProjectService.h"
#import "ProspectDetailModel.h"
#import "MineProspectDetailStatueTableViewCell.h"
#import "MineProspectDetailCancelStatueTableViewCell.h"
#import "MineProspectDetailProgressTableViewCell.h"
#import "MineProspectDetailProjectTableViewCell.h"
#import "MineProspecDetailBTNTableViewCell.h"
//#import "ProjectDetailViewController.h"
//#import "InvestigationViewController.h"
#import "MinePersonalZRBAlertView.h"
//#import "WriteCommentViewController.h"
#import "MineCancelProspectsViewController.h"
#import "NSDate+Addition.h"
#import "AppDelegate.h"


static NSString *const kMineProjectDetailPaddingCellIdentifier = @"MineProjectDetailPaddingCellIdentifier";
static NSString *const kMineProspectDetailProgressPaddingTableViewCellIdentifier = @"MineProspectDetailProgressPaddingTableViewCellIdentifier";


@interface MineProspectsDetailViewController ()<mineProspectDetailProjectDelegate,mineProspecDetailBTNTableViewCellDelegate,UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet    UITableView *_mytableView;
    NSMutableArray  *_listArray;
    ProjectService  *_projectService;
    ProspectDetailModel *_currentModel;
    MinePersonalZRBAlertView    *_alertView;
}
@end

@implementation MineProspectsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    _projectService = [[ProjectService alloc] init];
    _mytableView.dataSource = self;
    _mytableView.delegate = self;
}

- (void)getData{
    WS(bself);
    [_projectService getProspectDetailWithRequest:[NSString stringWithFormat:@"%d",_model.autoId] success:^(id responseObject) {
        if(responseObject){
            _currentModel = (ProspectDetailModel*)responseObject;
            [_mytableView reloadData];
        }else{
            [bself showTipViewWithMsg:@"无详细勘察信息，请稍后重试"];
        }
    } failure:^(NSError *error) {
        [bself showTipViewWithMsg:@"请求失败，请稍后重试"];
    }];
}

#pragma mark -
#pragma mark - 列表相关方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 8;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 6){
        return _currentModel.event.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //头部
    if (indexPath.section == 1)
    {
        if(_currentModel.inspectStatus == 0){
            //取消
            MineProspectDetailCancelStatueTableViewCell *cell = [_mytableView dequeueReusableCellWithIdentifier:mineProspectDetailCancelStatueTableViewCellIdentifier forIndexPath:indexPath];
            NSString *time = [ZRBUtilities stringToData:NSDATE_FORMAT_COUPON_1 interval:[NSString stringWithFormat:@"%lld",_currentModel.updateTime]];
            NSString *str = [NSString stringWithFormat:@"您于%@取消了对%@勘察的申请，因%@原因，勘察已成功取消。",time,_currentModel.projectName,_currentModel.reasonTypeZH];
            
            [cell  initWithReason:str];
            return cell;
        }else{
            //勘察状态
            MineProspectDetailStatueTableViewCell *cell = [_mytableView dequeueReusableCellWithIdentifier:mineProspectDetailStatueTableViewCellIdentifier forIndexPath:indexPath];
            [cell  initWithStatue:_currentModel.inspectStatus];
            return cell;
        }
    }else if (indexPath.section == 3)
    {
        MineProspectDetailProjectTableViewCell *cell = [_mytableView dequeueReusableCellWithIdentifier:mineProspectDetailProjectTableViewCellIdentifier forIndexPath:indexPath];
        [cell initWithProjectName:_currentModel.projectName];
        cell.delegate = self;
        return cell;
        
    }else if(indexPath.section == 5)
    {
        //勘察进度
        UITableViewCell  *cell = [_mytableView dequeueReusableCellWithIdentifier:kMineProspectDetailProgressPaddingTableViewCellIdentifier forIndexPath:indexPath];
        return cell;
        
    }
    else if(indexPath.section == 6)
    {
        //勘察进度
        MineProspectDetailProgressTableViewCell *cell = [_mytableView dequeueReusableCellWithIdentifier:mineProspectDetailProgressTableViewCellIdentifier forIndexPath:indexPath];
        [cell initWithModel:[_currentModel.event objectAtIndex:indexPath.row] andIndex:indexPath.row];
        return cell;
        
    }else if(indexPath.section == 7)
    {
        //重新预约 //取消预约
        MineProspecDetailBTNTableViewCell *cell = [_mytableView dequeueReusableCellWithIdentifier:mineProspecDetailBTNTableViewCellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        [cell initWithStatue:_currentModel.inspectStatus];
        
        return cell;
    }else
    {
        UITableViewCell  *cell = [_mytableView dequeueReusableCellWithIdentifier:kMineProjectDetailPaddingCellIdentifier forIndexPath:indexPath];
        return cell;
    }

}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //头部
    if (indexPath.section == 1)
    {
        if(_currentModel.inspectStatus == 0){
            //取消
            return 91;
            
        }else{
            //勘察状态
            return 121;
        }
        
    }else if (indexPath.section == 3)
    {
        return 97;
        
    }else if(indexPath.section == 5)
    {
        //勘察进度
        return 44;
        
    }
    else if(indexPath.section == 6)
    {
        //勘察进度
        return 96;
        
    }else if(indexPath.section == 7)
    {
        //重新预约 //取消预约
        return 93;
    }else
    {
        return 10;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    ProjectItemModel *model = [[ProjectItemModel alloc]init];
    //    model = _listArray[indexPath.row];
    //    MineProjectReportViewController *detailCtrl = (MineProjectReportViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineProjectReportViewController class]];;
    //    detailCtrl.projectId = model.projectId;
    //    //    detailCtrl.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:detailCtrl animated:YES];
    
    
}

#pragma mark - mineProspectDetailProjectDelegate
- (void)pushToProjectDetail{
    if(!_currentModel.projectId)
        return;
    //跳项目详情
//    ProjectDetailViewController *detailCtrl = [[ProjectDetailViewController alloc]init];
//    detailCtrl.projectID = [NSString stringWithFormat:@"%d",_currentModel.projectId];
//    detailCtrl.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:detailCtrl animated:YES];
}

- (void)pushToMap{
    //跳地图页面 geoCoor
    if(!_currentModel.geoCoor)
        return;
    NSArray *array = [_currentModel.geoCoor componentsSeparatedByString:@","];
    
//    ProjectLocationViewController *locationCtrl = [[ProjectLocationViewController alloc]init];
//    locationCtrl.longitude = [array[0] floatValue];
//    locationCtrl.latitude = [array[1] floatValue];
//    [self.navigationController pushViewController:locationCtrl animated:YES];
}

- (void)pushToCustomerService{
    //客户服务
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString: @"若有任何疑问\n\n可及时联系我们人工客服\n"];
    if(_alertView != nil){
        [_alertView removeFromSuperview];
        _alertView = nil;
    }
    _alertView = [[MinePersonalZRBAlertView alloc] initWithText:attString];
    [_alertView showOnView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
}

- (void)pushToProjectComment{
    if(!_currentModel.projectId)
        return;
    //跳评论页面
//    WriteCommentViewController *writeCommentCtrl = [[WriteCommentViewController alloc]init];
//    writeCommentCtrl.projectID = [NSString stringWithFormat:@"%d",_currentModel.projectId];
//    [self.navigationController pushViewController:writeCommentCtrl animated:YES];
}

#pragma mark - mineProspecDetailBTNTableViewCellDelegate
- (void)btnClick{
    if(_currentModel.inspectStatus == 0){
        //重新预约 跳预约勘察页面
        if(!_currentModel.projectId)
            return;
//        InvestigationViewController *investigationCtrl = [[InvestigationViewController alloc]init];
//        investigationCtrl.projectID = [NSString stringWithFormat:@"%d",_currentModel.projectId];
//        [self.navigationController pushViewController:investigationCtrl animated:YES];
    }else{
        //跳取消预约
        MineCancelProspectsViewController *mineCancelProspectsViewController = (MineCancelProspectsViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineCancelProspectsViewController class]];
        mineCancelProspectsViewController.prospectId = [NSString stringWithFormat:@"%d",_currentModel.autoId];
        [self.navigationController pushViewController:mineCancelProspectsViewController animated:YES];
    }
}   
@end
