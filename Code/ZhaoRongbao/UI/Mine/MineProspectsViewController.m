//
//  MineProspectsViewController.m
//  ZhaoRongbao
//
//  Created by on 15/9/2.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineProspectsViewController.h"
#import "MineProspectTableViewCell.h"
#import "ProjectService.h"
#import "MJRefresh.h"
#import "ProspectModel.h"
#import "ZRB_NormalModel.h"
#import "MineProspectsDetailViewController.h"

@interface MineProspectsViewController ()<UITableViewDataSource,UITableViewDelegate,MineProspectCellCancelStatueViewDeleteDelegate>
{
    IBOutlet    UITableView *_tableView;
    NSMutableArray  *_listArray;
    ProjectService  *_projectService;
    BOOL       _isRequst;            //是否请求过
}
@end

@implementation MineProspectsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)setup{
    _listArray = [NSMutableArray array];
    _projectService = [[ProjectService alloc] init];
    [self addHeaderView];
    [self addFooterView];
    [self refreshData];
}


/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}


#pragma mark -
#pragma mark -网络请求



/**
 *  请求我关注的项目列表
 */
- (void)queryListData
{
    _isRequst = NO;
    ProspectsRequest *request = [[ProspectsRequest alloc] init];
    request.pageSize = 10;
    
    [self startWithReq:request];
    
}

/**
 *  加载更多
 */
- (void)loadMoreData
{
    if(_listArray.count > 0){        
        ProspectModel *lastModel = [_listArray lastObject];
        LogInfo([NSString stringWithFormat:@"%d",lastModel.autoId]);
        ProspectsRequest *request = [[ProspectsRequest alloc]init];
        request.pageSize = 10;
        request.maxInspectId = lastModel.autoId;
     
        
        [self loadMoreWithReq:request];
    }
}


/**
 *  下拉加载
 *
 *  @param req 请求体
 */

- (void)startWithReq:(ProspectsRequest *)req
{
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_projectService requestProspectListWithRequest:req success:^(id responseObject) {
        [bself prospectQueryListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(ProspectsRequest   *)req
{
    
    WS(bself);
    [_projectService requestProspectListWithRequest:req success:^(id responseObject) {
        [bself loadMoreListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
    
}




- (void)prospectQueryListCallBackWithObject:(NSMutableArray *)modelArray
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    if(modelArray){
        _listArray = modelArray;
        [_tableView reloadData];
    }
    
    [_tableView.header endRefreshing];
    
}


- (void)loadMoreListCallBackWithObject:(NSMutableArray *)modelArray
{
    [_tableView.header endRefreshing];
    
    [_tableView.footer endRefreshing];
    if(modelArray){
        [_listArray addObjectsFromArray:modelArray];
        [_tableView reloadData];
    }else{
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
    
}

#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineProspectTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineProspectTableViewCellIdentifier forIndexPath:indexPath];
    ProspectModel *model = _listArray[indexPath.row];
    [cell initWithModel:model andIndex:(int)indexPath.row];
    cell.cancelStatueView.delegate = self;
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 94;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProspectModel *model = _listArray[indexPath.row];
    MineProspectsDetailViewController *detailCtrl = (MineProspectsDetailViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineProspectsDetailViewController class]];
    detailCtrl.model = model;
    [self.navigationController pushViewController:detailCtrl animated:YES];
    
    
}

#pragma mark - MineProspectCellCancelStatueViewDeleteDelegate 删除行
- (void)deleteData:(int)index{
    [self requestDeleteData:(ProspectModel *)[_listArray objectAtIndex:index] andindex:index];
}

-(void)requestDeleteData:(ProspectModel*)model andindex:(int)index{
    if(!model)
        return;
    
    //请求删除取消的勘察
    WS(bself);
    [_projectService cancelProspectDeleteWithRequest:[NSString stringWithFormat:@"%d",model.autoId] success:^(id responseObject) {
        ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
        if(model.res != 1){
            [bself showTipViewWithMsg:model.msg];
        }else{
            NSIndexPath *indexp = [NSIndexPath indexPathForRow:index inSection:0];
            [_listArray removeObjectAtIndex:index];
            [_tableView deleteRowsAtIndexPaths:@[indexp] withRowAnimation:UITableViewRowAnimationFade];

        }
        
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];

    
}
@end
