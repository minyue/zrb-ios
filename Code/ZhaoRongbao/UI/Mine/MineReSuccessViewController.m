//
//  MineReSuccessViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineReSuccessViewController.h"
#import "UIButton+ZRB.h"
@interface MineReSuccessViewController ()
{
    IBOutlet  UIButton   *_goButton;
}


- (void)setup;

//体验
- (IBAction)experience:(id)sender;
@end

@implementation MineReSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setup
{
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;

    [_goButton configButtonWithType:ZRB_ButtonTypeOperation];

}


/**
 *  体验
 *
 *  @param sender 
 */
- (IBAction)experience:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
