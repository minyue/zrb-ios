//
//  MineRegisterBTNTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  注册-基本信息 按钮栏

#import <UIKit/UIKit.h>

static NSString *const mineRegisterBTNShowTableViewCellIdentifier = @"MineRegisterBTNShowTableViewCellIdentifier";

@interface MineRegisterBTNTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIButton    *btnShow;

- (void)initwithStatue:(int)sta;
@end
