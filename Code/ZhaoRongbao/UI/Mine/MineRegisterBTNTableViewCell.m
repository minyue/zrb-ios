//
//  MineRegisterBTNTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineRegisterBTNTableViewCell.h"
#import "ZRB_UserRegisterInfo.h"
@interface MineRegisterBTNTableViewCell()
{
    int _currentSta;
}
@end

@implementation MineRegisterBTNTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initwithStatue:(int)sta{
    if(sta == 2){
        [_btnShow setTitle:@"立即体验" forState:UIControlStateNormal];
    }else{
        [_btnShow setTitle:@"下一步" forState:UIControlStateNormal];
    }
//    _currentSta = sta;
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNext:) name:REGISTER_INFO_CHANGE object:nil];
//    [self showNext:nil];
}


@end
