//
//  MineRegisterDidSelectTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  注册-基本信息 跳转页面

#import <UIKit/UIKit.h>

static NSString *const mineRegisterDidSelectTableViewCellIdentifier = @"MineRegisterDidSelectTableViewCellIdentifier";

@interface MineRegisterDidSelectTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel    *lblTitle;
@property (nonatomic,strong) IBOutlet UILabel    *lblContent;

- (void)initWithtitle:(NSString*)title andContent:(NSString*)str;
@end
