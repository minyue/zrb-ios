//
//  MineRegisterDidSelectTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineRegisterDidSelectTableViewCell.h"

@implementation MineRegisterDidSelectTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithtitle:(NSString*)title andContent:(NSString*)str{
    if(title)
        _lblTitle.text = title;
    if(str){
        _lblContent.text = str;
    }else{
        _lblContent.text = @"请选择";
    }
}
@end
