//
//  MineRegisterInfoViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  基本信息 3个页面

#import "ZRB_ViewController.h"

@interface MineRegisterInfoViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic,assign) int currentStep;//0 1 2
@property (nonatomic,copy) NSString   *phoneNum;

@end
