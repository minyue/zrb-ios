//
//  MineRegisterInfoViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineRegisterInfoViewController.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MinePersonalZRBAlertView.h"
#import "AppDelegate.h"
#import "MineRegisterTopCell.h"
#import "MineRegisterTypeinTableViewCell.h"
#import "MineRegisterDidSelectTableViewCell.h"
#import "MineRegisterBTNTableViewCell.h"
#import "ZRB_UserRegisterInfo.h"
#import "MinePersonalAreaViewController.h"
#import "MineIndustryViewController.h"
#import "RegisterService.h"
#import "MineService.h"

static NSString *const mineRegisterPaddingCellIdentifier = @"MineRegisterPaddingCellIdentifier";

@interface MineRegisterInfoViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    CustomBarItem *_item;
    MinePersonalZRBAlertView    *_alertView;
    IBOutlet UITableView *_tableView;
    IBOutlet UIButton *_btnShowNext;
    RegisterService *_service;
}
@end

@implementation MineRegisterInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
    
     
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//     [[NSNotificationCenter defaultCenter] postNotificationName:REGISTER_INFO_CHANGE object:@(_currentStep)];
    if(_tableView)
        [_tableView reloadData];
}

- (void)setup{
    switch (_currentStep) {
        case 0:
            self.navigationItem.title = @"基本信息";
            break;
        case 1:
            self.navigationItem.title = @"企业认证";
            break;
        case 2:
            self.navigationItem.title = @"注册成功";
            break;
        default:
            break;
    }
    [self setRightItem];
    
    _service = [[RegisterService alloc] init];
}

- (void)setRightItem
{
 
    _item =  [self.navigationItem setItemWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_call"]] itemType:right];
    [_item addTarget:self selector:@selector(clickCall:) event:UIControlEventTouchUpInside];
    [_item setOffset:-2];
    
//    NSString *title = @"咨询";
//    _item = [self.navigationItem setItemWithTitle:title textUnableColor:RGB(131, 185, 252)  textEnbaleColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
//    [_item setOffset:-2];
//    [_item addTarget:self selector:@selector(clickCall:) event:UIControlEventTouchUpInside];
}

- (void)click_popViewController
{
    //弹出提示
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"中断填写？\n您离招融宝只有一步之遥了，真的要放弃吗？" delegate:self cancelButtonTitle:nil otherButtonTitles:@"先不填了",@"继续填写", nil];
    [alert show];
    
}

#pragma mark delegate of UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        [ZRB_UserRegisterInfo shareUserRegisterInfo].model = [[RegisterInfoModel alloc] init];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else if (buttonIndex == 1){
        [alertView canResignFirstResponder];
    }
}

#pragma mark - action
//咨询
-(IBAction)clickCall:(id)sender{
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString: @"点击呼叫“400-2525-888”\n若有任何疑问可联系我们人工客服\n"];
    if(_alertView != nil){
        [_alertView removeFromSuperview];
        _alertView = nil;
    }
    _alertView = [[MinePersonalZRBAlertView alloc] initWithText:attString];
    [_alertView showOnView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
}

#pragma mark - UITextfield delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    switch (textField.tag) {
        case 1:
            [ZRB_UserRegisterInfo shareUserRegisterInfo].model.realName = textField.text;
            break;
        case 2:
            [ZRB_UserRegisterInfo shareUserRegisterInfo].model.enterpriseName = textField.text;
            break;
        case 3:
            [ZRB_UserRegisterInfo shareUserRegisterInfo].model.duty = textField.text;
            break;
        default:
            break;
    }
    [self showNext:_btnShowNext];
}

#pragma mark -
#pragma mark - 列表相关方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    int i =0;
    switch (_currentStep) {
        case 0:
            i = 6;
            break;
        case 1:
            i = 5;
            break;
        case 2:
            i = 2;
            break;
        default:
            break;
    }
    return i;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterInfoModel *info = (RegisterInfoModel*)[ZRB_UserRegisterInfo shareUserRegisterInfo].model;
    switch (_currentStep) {
        case 0:
        {
            if(indexPath.section == 0){
                
                MineRegisterTopCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterTopCellIdentifier forIndexPath:indexPath];
                [cell initWithStatue:_currentStep andTitile:nil];
                return cell;
            }else if (indexPath.section == 1){
                UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterPaddingCellIdentifier forIndexPath:indexPath];
                return cell;
            }
            else if(indexPath.section == 2){
                MineRegisterTypeinTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterTypeinTableViewCellIdentifier forIndexPath:indexPath];
                NSString* content = info.realName;
                [cell initWithtitle:@"真实姓名" andContentHolder:@"请输入您真实的姓名" andContent:content];
                cell.txtContent.tag = 1;
                cell.txtContent.delegate = self;
                return cell;
            }else if(indexPath.section == 3){
                MineRegisterDidSelectTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterDidSelectTableViewCellIdentifier forIndexPath:indexPath];
                NSString* content = info.city;
                [cell initWithtitle:@"所在城市" andContent:content];
                return cell;
            }else if(indexPath.section == 4){
                MineRegisterDidSelectTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterDidSelectTableViewCellIdentifier forIndexPath:indexPath];
                NSString* content = info.industry.industryName;
                [cell initWithtitle:@"所属行业" andContent:content];
                return cell;
            }else if(indexPath.section == 5){
                //下一步
                MineRegisterBTNTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterBTNShowTableViewCellIdentifier forIndexPath:indexPath];
                [cell initwithStatue:_currentStep];
                _btnShowNext = cell.btnShow;
                [self showNext:_btnShowNext];
                return cell;
            }
        }
            break;
        case 1:
        {
            if(indexPath.section == 0){
                
                MineRegisterTopCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterTopCellIdentifier forIndexPath:indexPath];
                [cell initWithStatue:_currentStep andTitile:nil];
                return cell;
            }else if (indexPath.section == 1){
                UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterPaddingCellIdentifier forIndexPath:indexPath];
                return cell;
            }
            else if(indexPath.section == 2){
                //                    MineRegisterDidSelectTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterDidSelectTableViewCellIdentifier forIndexPath:indexPath];
                //                    NSString* content = [ZRB_UserRegisterInfo shareUserRegisterInfo].enterpriseName;
                //                    [cell initWithtitle:@"企业名称" andContent:content];
                //                    return cell;
                MineRegisterTypeinTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterTypeinTableViewCellIdentifier forIndexPath:indexPath];
                NSString* content = info.enterpriseName;
                [cell initWithtitle:@"企业名称" andContentHolder:@"请填写" andContent:content];
                cell.txtContent.tag = 2;
                cell.txtContent.delegate = self;
                return cell;
            }else if(indexPath.section == 3){
                MineRegisterTypeinTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterTypeinTableViewCellIdentifier forIndexPath:indexPath];
                NSString* content = info.duty;
                [cell initWithtitle:@"担任职务" andContentHolder:@"请填写" andContent:content];
                cell.txtContent.tag = 3;
                cell.txtContent.delegate = self;
                return cell;
                
            }else if(indexPath.section == 4){
                //下一步
                MineRegisterBTNTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterBTNShowTableViewCellIdentifier forIndexPath:indexPath];
                [cell initwithStatue:_currentStep];
                _btnShowNext = cell.btnShow;
                [self showNext:_btnShowNext];
                return cell;
            }
            
        }
            break;
        case 2:
        {
            if(indexPath.section == 0){
                
                MineRegisterTopCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterTopCellIdentifier forIndexPath:indexPath];
                [cell initWithStatue:_currentStep andTitile:@"恭喜您，已注册成功，请等待招融宝团队为您申请认证"];
                return cell;
            }else if(indexPath.section == 1){
                //下一步
                MineRegisterBTNTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineRegisterBTNShowTableViewCellIdentifier forIndexPath:indexPath];
                [cell initwithStatue:_currentStep];
                _btnShowNext = cell.btnShow;
                [self showNext:_btnShowNext];
                return cell;
            }
        }
            break;
        default:
            break;
    }

    return nil;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
        return 115.0f;
    switch (_currentStep) {
        case 0:
        {
            if (indexPath.section == 1){
                return 10.0f;
            }
            else if(indexPath.section == 2){
                return 44.0f;
            }else if(indexPath.section == 3){
               return 44.0f;
            }else if(indexPath.section == 4){
                return 44.0f;
            }else if(indexPath.section == 5){
                return 93.0f;
            }
        }
            break;
        case 1:
        {
            if (indexPath.section == 1){
                return 10.0f;
            }
            else if(indexPath.section == 2){
                return 44.0f;
            }else if(indexPath.section == 3){
                return 44.0f;
            }else if(indexPath.section == 4){
                return 93.0f;
            }
            
        }
            break;
        case 2:
        {
            if(indexPath.section == 1){
                return 93.0f;
            }
        }
            break;
        default:
            break;
    }
    return 0;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (_currentStep) {
        case 0:
        {
            if(indexPath.section == 3){
                //所在城市
                MinePersonalAreaViewController *areaCtrl = (MinePersonalAreaViewController *)[StoryBoardUtilities viewControllerForStoryboardName:kPersonalStoryboardName class:[MinePersonalAreaViewController class]];
                areaCtrl.lastCtrlFlag = ZRBRegisterUserInfoViewCtroller;
                
                areaCtrl.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:areaCtrl animated:YES];
            }else if(indexPath.section == 4){
                //所属行业
                MineIndustryViewController *industryCtrl = (MineIndustryViewController *)[StoryBoardUtilities viewControllerForStoryboardName:kMineStoryboardName class:[MineIndustryViewController class]];
                industryCtrl.pageType = 0;
                [self.navigationController pushViewController:industryCtrl animated:YES];

            }
        }
            break;
        case 1:
        {
           if(indexPath.section == 2){
                //企业名称
            }
            
        }
            break;
        default:
            break;
    }
}

- (void)showNext:(id)sender{
    UIButton    *btn;
    if(sender){
        btn = (UIButton*)sender;
    }
    RegisterInfoModel *rmodel = (RegisterInfoModel*)[ZRB_UserRegisterInfo shareUserRegisterInfo].model;
    btn.enabled = NO;
    switch (_currentStep) {
        case 0:
        {
            if(rmodel.realName.length>0  && rmodel.industry.industryName && rmodel.industry.iid && (rmodel.city.length>0  || rmodel.cityID)){
                btn.enabled = YES;
            }
            [btn setTitle:@"下一步" forState:UIControlStateNormal];
        }
            break;
        case 1:
        {
            if(rmodel.enterpriseName.length>0  && rmodel.duty.length>0 ){
                btn.enabled = YES;
            }
            [btn setTitle:@"下一步" forState:UIControlStateNormal];
        }
            break;
        case 2:
        {
            btn.enabled = YES;
            [btn setTitle:@"立即体验" forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    [self nextButtonActive:btn.enabled andButton:btn];
}

- (void)nextButtonActive:(BOOL)isActive andButton:(UIButton*)btn
{
    if (isActive)
    {
        btn.backgroundColor = [UIColor colorWithHexString:@"4997ec"];//蓝色
        btn.enabled = YES;
        [btn setTitleColor:[UIColor colorWithHexString:@"ffffff"]];//白色
        
    }else
    {
        btn.backgroundColor = [UIColor colorWithHexString:@"AAAAAA"];//灰色
        btn.enabled = NO;
        [btn setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];//深灰色
    }
}

- (IBAction)next:(id)sender{
    if(!_phoneNum)
        return;
    RegisterInfoModel *info = (RegisterInfoModel*)[ZRB_UserRegisterInfo shareUserRegisterInfo].model;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:_phoneNum forKey:@"phone"];
    switch (_currentStep) {
        case 0:
        {
            [dic setObject:_phoneNum forKey:@"phone"];
            [dic setValue:info.realName forKey:@"realname"];
            if(info.cityID)
                [dic setValue:info.cityID forKey:@"locationId"];
            if(info.city)
                [dic setValue:info.city forKey:@"locationName"];
            [dic setValue:@(info.industry.iid) forKey:@"industryId"];
            [dic setValue:info.industry.industryName forKey:@"industry"];
        }
        break;
        case 1:
        {
            [dic setObject:_phoneNum forKey:@"phone"];
            [dic setValue:info.enterpriseName forKey:@"org"];
            [dic setValue:info.duty forKey:@"position"];
        }
        break;
        case 2:{
            //体验 //请求登录
            [self requestLogin];
            
            return;
        }
        break;
        default:
            break;
    }
    [_service saveAppUserInfoWithRequest:dic success:^(id responseObject) {
        ZRB_BaseModel *base = (ZRB_BaseModel*)responseObject;
        if(base.res == 1){
            if(_currentStep == 0){
                //
                MineRegisterInfoViewController *stepCtrl = (MineRegisterInfoViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterInfoViewController class]];;
                stepCtrl.currentStep = 1;
                stepCtrl.phoneNum = _phoneNum;
                [self.navigationController pushViewController:stepCtrl animated:YES];
            }else if(_currentStep == 1){
                //
                MineRegisterInfoViewController *stepCtrl = (MineRegisterInfoViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterInfoViewController class]];;
                stepCtrl.currentStep = 2;
                stepCtrl.phoneNum = _phoneNum;
                [self.navigationController pushViewController:stepCtrl animated:YES];
            }
        }else{
            [self showTipViewWithMsg:base.msg];
        }
    } failure:^(NSError *error) {
        [self showTipViewWithMsg:@"提交注册信息失败，请稍后再试"];
    }];
    
}

- (void)requestLogin{
    RegisterInfoModel *model = [ZRB_UserRegisterInfo shareUserRegisterInfo].model;
    if(!model.phone || !model.pwd){
        [ZRB_UserRegisterInfo shareUserRegisterInfo].model = [[RegisterInfoModel alloc] init];
        [SVProgressHUD dismiss];
        return;
    }
    
    [_service loginWithName:model.phone passWord:model.pwd success:^(id responseObject) {
        [ZRB_UserRegisterInfo shareUserRegisterInfo].model = [[RegisterInfoModel alloc] init];
        LoginModel *logmodel = responseObject;
        [SVProgressHUD dismiss];
        
        if (logmodel.res == ZRBHttpSuccssType) {
            logmodel.phoneNum = model.phone;
            logmodel.userPwd = model.pwd;
            //登录成功
            [ZRB_UserManager loginSuccssWithModel:logmodel withBlock:^(bool data) {
                if(data){
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"登录失败，请稍候再试" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
            
        }
        
    } failure:^(NSError *error) {
        [ZRB_UserRegisterInfo shareUserRegisterInfo].model = [[RegisterInfoModel alloc] init];
        [SVProgressHUD dismiss];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    [SVProgressHUD show];
}

@end
