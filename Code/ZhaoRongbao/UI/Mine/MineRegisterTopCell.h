//
//  MineRegisterTopCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  注册-基本信息 第一行

#import <UIKit/UIKit.h>

static NSString *const mineRegisterTopCellIdentifier = @"MineRegisterTopCellIdentifier";

@interface MineRegisterTopCell : UITableViewCell

-(void)initWithStatue:(int)statue andTitile:(NSString*)title;

@end
