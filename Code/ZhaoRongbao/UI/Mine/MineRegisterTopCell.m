//
//  MineRegisterTopCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineRegisterTopCell.h"
@interface MineRegisterTopCell()
{
    IBOutlet UIView  *_view1;
    IBOutlet UIView *_view2;
    IBOutlet UIView *_view3;
    IBOutlet UILabel  *_lbl1;
    IBOutlet UILabel *_lbl2;
    IBOutlet UILabel *_lbl3;
    IBOutlet UILabel  *_line1;
    IBOutlet UILabel *_line2;
    
    IBOutlet UILabel *_lblTitle;
}
@end

@implementation MineRegisterTopCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)awakeFromNib{
    
    [_view1.layer setCornerRadius:_view1.frame.size.width/2];
    [_view1.layer setMasksToBounds:YES];
    _view1.layer.borderWidth = 1;
    _view1.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
    [_view2.layer setCornerRadius:_view2.frame.size.width/2];
    [_view2.layer setMasksToBounds:YES];
    _view2.layer.borderWidth = 1;
    _view2.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
    [_view3.layer setCornerRadius:_view3.frame.size.width/2];
    [_view3.layer setMasksToBounds:YES];
    _view3.layer.borderWidth = 1;
    _view3.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
}

-(void)initWithStatue:(int)statue andTitile:(NSString*)title{
    if(title)
        _lblTitle.text = title;
    switch (statue) {
        case 0:
            _view1.layer.borderColor = [UIColor colorWithHexString:@"0077d9"].CGColor;
            _view2.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
            _view3.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
            _lbl1.textColor= [UIColor colorWithHexString:@"0077d9"];
            _lbl2.textColor = [UIColor colorWithHexString:@"CCCCCC"];
            _lbl3.textColor = [UIColor colorWithHexString:@"CCCCCC"];
            _line1.backgroundColor = [UIColor colorWithHexString:@"CCCCCC"];
            _line2.backgroundColor = [UIColor colorWithHexString:@"CCCCCC"];
            break;
        case 1:
            _view1.layer.borderColor = [UIColor colorWithHexString:@"0077d9"].CGColor;
            _view2.layer.borderColor = [UIColor colorWithHexString:@"0077d9"].CGColor;
            _view3.layer.borderColor = [UIColor colorWithHexString:@"CCCCCC"].CGColor;
            _lbl1.textColor= [UIColor colorWithHexString:@"0077d9"];
            _lbl2.textColor = [UIColor colorWithHexString:@"0077d9"];
            _lbl3.textColor = [UIColor colorWithHexString:@"CCCCCC"];
            _line1.backgroundColor = [UIColor colorWithHexString:@"0077d9"];
            _line2.backgroundColor = [UIColor colorWithHexString:@"CCCCCC"];
            break;
        case 2:
            _view1.layer.borderColor = [UIColor colorWithHexString:@"0077d9"].CGColor;
            _view2.layer.borderColor = [UIColor colorWithHexString:@"0077d9"].CGColor;
            _view3.layer.borderColor = [UIColor colorWithHexString:@"0077d9"].CGColor;
            _lbl1.textColor= [UIColor colorWithHexString:@"0077d9"];
            _lbl2.textColor = [UIColor colorWithHexString:@"0077d9"];
            _lbl3.textColor = [UIColor colorWithHexString:@"0077d9"];
            _line1.backgroundColor = [UIColor colorWithHexString:@"0077d9"];
            _line2.backgroundColor = [UIColor colorWithHexString:@"0077d9"];
            break;
            
        default:
            break;
    }
}

@end
