//
//  MineRegisterTypeinTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  注册-基本信息 填写行

#import <UIKit/UIKit.h>

static NSString *const mineRegisterTypeinTableViewCellIdentifier = @"MineRegisterTypeinTableViewCellIdentifier";

@interface MineRegisterTypeinTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UITextField    *txtContent;
@property (nonatomic,assign) int ctag;

- (void)initWithtitle:(NSString*)title andContentHolder:(NSString*)holder andContent:(NSString*)str;
@end
