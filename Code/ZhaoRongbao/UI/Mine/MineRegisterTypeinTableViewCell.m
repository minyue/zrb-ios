//
//  MineRegisterTypeinTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineRegisterTypeinTableViewCell.h"
#import "ZRB_UserRegisterInfo.h"

@interface MineRegisterTypeinTableViewCell(){
    
    IBOutlet UILabel        *_lblTitle;
}
@end

@implementation MineRegisterTypeinTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithtitle:(NSString*)title andContentHolder:(NSString*)holder andContent:(NSString*)str{
    if(holder)
        _txtContent.placeholder = holder;
    if(str)
        _txtContent.text = str;
    if(title)
        _lblTitle.text = title;
    
}
@end
