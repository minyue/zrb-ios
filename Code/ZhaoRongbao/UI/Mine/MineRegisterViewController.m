//
//  MineRePhoneViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineRegisterViewController.h"
#import "TOTextInputChecker.h"
#import "JKCountDownButton.h"
#import "RegisterService.h"
#import "RegisterModel.h"
#import "MineUserResisterViewController.h"
@interface MineRegisterViewController (){
    IBOutlet  ZRB_TextField  *_phoneTextFiled;
    
    IBOutlet  ZRB_TextField  *_vercodeTextFiled;
    
    IBOutlet  UIButton       *_nextButton;      //下一步
    
//    IBOutlet  UIView         *_lineView;        //线条
    
    JKCountDownButton        *_countDownCode;   //计时器
    
    TOTextInputChecker        *_phoneChecker;   //电话号码检查器
    
    TOTextInputChecker        *_codeChecker;    //验证码检查器
    
    
    RegisterService            *_service;
}

- (void)setup;

/**
 *  下一步
 *
 *  @param sender
 */
- (IBAction)goNext:(id)sender;

@end

@implementation MineRegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setup
{
    WS(bself);
    _phoneChecker = [TOTextInputChecker telChecker:YES];
    _phoneChecker.checkInputBlock = ^(BOOL flag)
    {
        
        [bself countCodeActive:flag];
        
    };
    [_phoneTextFiled initWithTitle:@"+86" placeHolder:@"手机号码"];

    _phoneTextFiled.delegate = _phoneChecker;

    _countDownCode = [JKCountDownButton buttonWithType:UIButtonTypeCustom];
    [_countDownCode setTitle:@"获取验证码" forState:UIControlStateNormal];
    _countDownCode.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [_countDownCode setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
    [_countDownCode.titleLabel setFont:[UIFont systemFontOfSize:11.0f]];
    
    _countDownCode.layer.cornerRadius = 15;
    _countDownCode.layer.masksToBounds = YES;
    _countDownCode.enabled = NO;
    [_countDownCode addToucheHandler:^(JKCountDownButton*sender, NSInteger tag) {
        sender.enabled = NO;
        [bself getSMSCode];
        [sender startWithSecond:60];
        [sender didChange:^NSString *(JKCountDownButton *countDownButton,int second) {
//            NSString *title = [NSString stringWithFormat:@"剩余%d秒",second];
            sender.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
            [sender setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
            NSString *title = [NSString stringWithFormat:@"%ds",second];
            return title;
        }];
        [sender didFinished:^NSString *(JKCountDownButton *countDownButton, int second) {
            countDownButton.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
            [countDownButton setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
            countDownButton.enabled = YES;
            return @"重新获取";
            
        }];
    }];
    
    _codeChecker = [TOTextInputChecker codeChecker];
    _codeChecker.checkInputBlock = ^(BOOL flag)
    {
        
        [bself nextButtonActive:flag];
    };

    [_vercodeTextFiled initWithTitle:@"验证码" placeHolder:@"请输入验证码" customView:_countDownCode];
    _vercodeTextFiled.delegate = _codeChecker;
//    _nextButton.layer.cornerRadius = (65/2);
//    _nextButton.layer.masksToBounds = YES;
    [_nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [_nextButton.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [self nextButtonActive:NO];
    
    _service = [[RegisterService alloc] init];

}



/**
 *  获取验证码是否被激活
 *
 *  @param isActive YES:可用   NO:不可用
 */
- (void)countCodeActive:(BOOL)isActive
{
    if (isActive) {
        _countDownCode.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        _countDownCode.enabled = YES;
        [_countDownCode setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
    }else
    {
        _countDownCode.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        _countDownCode.enabled = NO;
        [_countDownCode setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];

    }

}

/**
 *  控制下一步的状态
 *
 *  @param isActive 是否激活
 */
- (void)nextButtonActive:(BOOL)isActive
{
    if (isActive)
    {
        _nextButton.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        _nextButton.enabled = YES;
        [_nextButton setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
//        _lineView.backgroundColor = [UIColor colorWithHexString:@"4997ec"];

    }else
    {
        _nextButton.backgroundColor = [UIColor colorWithHexString:@"AAAAAA"];
        _nextButton.enabled = NO;
        [_nextButton setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
//        _lineView.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    }
}


/**
 *  下一步
 *
 *  @param sender
 */
- (IBAction)goNext:(id)sender
{
    //for test
//    [self setupUserNameAndPassWord];
    
    if ([_phoneChecker finalCheck] != InputCheckErrorNone){
        [self showTipViewWithMsg:@"请输入正确的手机号"];
        return;
    }
    
    if ([_codeChecker finalCheck] != InputCheckErrorNone)
    {
        [self showTipViewWithMsg:@"验证码输入有误！"];
        return;
    }
    
    //校验手机号
    [self checkMobileRegister];
    [SVProgressHUD show];
}


#pragma mark - 
#pragma mark - 网络请求

/**
 *  获取短信验证码
 */
- (void)getSMSCode
{

    [_service sendSMSRegisterWithTel:_phoneTextFiled.text success:^(id responseObject) {
        
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        if (model.res != ZRBHttpSuccssType)
        {
            [self showTipViewWithMsg:model.msg];
            [_countDownCode stop];

        }
        
    } failure:^(NSError *error) {
        [self showTipViewWithMsg:kHttpFailError];

    }];
}


/**
 *  检查手机号是否已经注册
 */
- (void)checkMobileRegister
{
    WS(bself)
    [_service checkMobileRegisterWithTel:_phoneTextFiled.text success:^(id responseObject) {
        
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        
        if (model.res == ZRBHttpSuccssType)
        {
            [bself checkSMSRegister];
        }else
        {
            [self showTipViewWithMsg:model.msg];

            [SVProgressHUD dismiss];

        }

    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:kHttpFailError];
        
    }];

}


/**
 *  检查验证码是否正确
 */
- (void)checkSMSRegister
{
    [_service checkSMSCodeWithTel:_phoneTextFiled.text code:_vercodeTextFiled.text success:^(id responseObject) {

        [SVProgressHUD dismiss];
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;

        if (model.res == ZRBHttpSuccssType) {
            //进入下一步
            [self setupUserNameAndPassWord];

        }else
        {
            [self showTipViewWithMsg:model.msg];

        }

    } failure:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:kHttpFailError];

    }];
}


/**
 *  设置用户名密码
 */
- (void)setupUserNameAndPassWord
{
    
    [RegisterRequest shareInstance].mobile = _phoneTextFiled.text;
    [RegisterRequest shareInstance].smsCode = _vercodeTextFiled.text;

    MineUserResisterViewController *userVC = (MineUserResisterViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineUserResisterViewController class]];
    [self.navigationController pushViewController:userVC animated:YES];

}




- (void)dealloc
{
    //释放的时候，关闭计时器
    [_countDownCode stop];
}
@end
