//
//  MineSettingAboutUsViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineSettingAboutUsViewController.h"

@interface MineSettingAboutUsViewController ()
{
    IBOutlet UIWebView *_webView;
}
@end

@implementation MineSettingAboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadWebPageWithString:HTTP_ABOUTZRB_URL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadWebPageWithString:(NSString*)urlString
{
    NSURL *url =[NSURL URLWithString:urlString];
    NSLog(urlString);
    NSURLRequest *request =[NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
