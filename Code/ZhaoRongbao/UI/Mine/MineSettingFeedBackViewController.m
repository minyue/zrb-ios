//
//  MineSettingFeedBackViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineSettingFeedBackViewController.h"
#import "MineService.h"
#import "ZRB_NormalModel.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "UIButton+ZRB.h"


#define TextMaxLength 200
#define textPlaceHodel @"请在这里输入..."

@interface MineSettingFeedBackViewController ()<UITextViewDelegate,UITextFieldDelegate>{
    IBOutlet UIView *_contactInfoView;//输入栏
    IBOutlet UIView *_countDownLabel;//倒计数栏
    IBOutlet UILabel *_lblCountDown;//到计数
    IBOutlet UITextView  *_txtFeed;//意见
    IBOutlet UITextField  *_txtContact;//联系方式
    IBOutlet  UIButton  *_btnSubmit;
    MineService *_service;
    
    IBOutlet UIView *_typeInFeedBackView;
    IBOutlet UIView *_submitView;
}
@end

@implementation MineSettingFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    if(user){
        _contactInfoView.hidden = YES;
        [_contactInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
        [_submitView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_typeInFeedBackView.mas_bottom).offset(10);
        }];
    }else{
        _contactInfoView.hidden = NO;
        _contactInfoView.frameHeight = 44;
    }
    
    _countDownLabel.layer.cornerRadius = 10;
    _countDownLabel.layer.masksToBounds = YES;
    _lblCountDown.text = @"200";
    
    _service = [[MineService alloc] init];
    
    [_btnSubmit configButtonWithType:ZRB_ButtonTypeOperation];
    [self feedButtonActive:NO];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textViewEditChanged:) name:@"UITextViewTextDidChangeNotification"
                                              object:_txtFeed];
    
    _txtFeed.text = textPlaceHodel;
}

- (void)feedButtonActive:(BOOL)isActive
{
    if (isActive)
    {
        _btnSubmit.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        _btnSubmit.enabled = YES;
        [_btnSubmit setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
        
    }else
    {
        _btnSubmit.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
        _btnSubmit.enabled = NO;
        [_btnSubmit setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
    }
}

#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if([[textView text] isEqualToString:textPlaceHodel]){
        textView.text = @"";
    }
    _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [textView text].length)];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        _txtFeed.text = textPlaceHodel;
        [self feedButtonActive:NO];
    }else{
        if(_contactInfoView.hidden == NO){
            if(_txtContact.text.length >0)
                [self feedButtonActive:YES];
        }else{
             [self feedButtonActive:YES];
        }
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    NSString *new = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(new.length > TextMaxLength){
        
        if (![text isEqualToString:@""]) {
            
            return NO;
            
        }
        
    }
    
    return YES;
    
}

-(void)textViewEditChanged:(NSNotification *)obj{
    
    UITextField *textField = (UITextField *)obj.object;
//    NSLog(@"test:%@",textField.text);
    if (textField.text.length == 0 || [textField.text isEqualToString:textPlaceHodel]) {
        [self feedButtonActive:NO];
    }else{
        if(_contactInfoView.hidden == NO){
            if(_txtContact.text.length >0)
                [self feedButtonActive:YES];
        }else{
            [self feedButtonActive:YES];
        }
    }
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > TextMaxLength) {
                _txtFeed.text = [toBeString substringToIndex:TextMaxLength-1];
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtFeed text].length)];
            }else{
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtFeed text].length)];
            }
        }
        else{
            if(toBeString.length <= TextMaxLength){
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtFeed text].length)];
            }else{
                _txtFeed.text = [toBeString substringToIndex:TextMaxLength-1];
                _lblCountDown.text = @"0";
            }
            
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > TextMaxLength) {
            _txtFeed.text = [toBeString substringToIndex:TextMaxLength];
        }
        _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtFeed text].length)];
    }
    
}

#pragma mark - UITextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(_txtContact.text.length >0){
        [self feedButtonActive:YES];
    }else{
        [self feedButtonActive:NO];
    }

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(_txtContact.text.length >0){
        [self feedButtonActive:YES];
    }else{
        [self feedButtonActive:NO];
    }
    return YES;
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:@"UITextViewTextDidChangeNotification"
                                                 object:_txtFeed];
}

-(BOOL)checkSubmit{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    BOOL back = NO;
    if(_txtFeed.text.length < 1 || [_txtFeed.text isEqualToString:textPlaceHodel]){
        [self showTipViewWithMsg:@"请先填写您的意见"];
        return back;
    }else if (_txtContact.text.length < 1 && !user){
        [self showTipViewWithMsg:@"请留下您的联系方式"];
        return back;
    }else{
        if(![ZRBUtilities isValidateEmail:_txtContact.text] && ![ZRBUtilities isMobileNumber:_txtContact.text] && !user){
            [self showTipViewWithMsg:@"请留下您正确的联系方式（手机号或者邮箱）"];
            return back;
        }
    }
    back = YES;
    return back;
}


#pragma mark - Action
- (IBAction)submitFeedback:(id)sender{
    if([self checkSubmit]){
        WS(bself);
        //提交
        [_service userFeedbackZRBRequest:_txtFeed.text andContact:_txtContact.text success:^(id responseObject) {
            [SVProgressHUD dismiss];
            
            ZRB_NormalModel *model = (ZRB_NormalModel*)responseObject;
            if(model.res == 1){
                [MBProgressHUD showHUDTitle:model.msg onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
                dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 1.5 *NSEC_PER_SEC);
                dispatch_after(afterTime, dispatch_get_main_queue(), ^{
                    [bself.navigationController popViewControllerAnimated:YES];
                });
            }else{
                [bself showTipViewWithMsg:model.msg];
            }
        } failure:^(NSError *error) {
            [SVProgressHUD dismiss];
            [bself showTipViewWithMsg:@"提交意见失败，请稍后提交"];
        }];
        
        [SVProgressHUD show];
    }
}
@end
