//
//  MineSettingViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  设置界面

#import "ZRB_ViewController.h"

@interface MineSettingViewController : ZRB_ViewControllerWithBackButton
@property (nonatomic,weak) void (^loginSuccssBlock)();
@property (nonatomic,copy) void (^logOutBlock)();
@end
