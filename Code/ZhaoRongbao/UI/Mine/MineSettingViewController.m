//
//  MineSettingViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineSettingViewController.h"
#import "MineSettingTopCell.h"
#import "MineSettingItemsCell.h"
#import "MineSettingBottomCell.h"
#import "MinePersonalCenterViewController.h"
#import "MineHomeViewController.h"
#import "MineLoginViewcController.h"
#import "MineViewController.h"
#import "MineRegisterViewController.h"
#import "MineSettingAboutUsViewController.h"
#import "MineContainerViewController.h"
#import "MinePersonalZRBAlertView.h"
#import "AppDelegate.h"
#import "MineSettingFeedBackViewController.h"
#import "UMSocial.h"
#import "MineShareViewController.h"
#import "SSGlobalConfig.h"

static NSString *const kMineSettingTopCellIdentifier = @"MineSettingTopCellIdentifier";
static NSString *const kMineSettingItemsCellIdentifier = @"MineSettingItemsCellIdentifier";
static NSString *const kMineSettingBottomCellIdentifier = @"MineSettingBottomCellIdentifier";
static NSString *const kMineSettingPaddingCellIdentifier = @"MineSettingPaddingCellIdentifier";

@interface MineSettingViewController ()<MineSettingTopCellDelegate,MineSettingBottomCellDelegate>
{
    IBOutlet  UITableView  *_tableView;
    
    NSArray   *_dataArray;
    
    MinePersonalZRBAlertView    *_alertView;
}
@end

@implementation MineSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup
{
    _tableView.backgroundColor = [UIColor clearColor];
    
    _dataArray = @[[SettingItem itemWithType:MineSettingItemsTypeAbout],
                   [SettingItem itemWithType:MineSettingItemsTypeIdea],
                   [SettingItem itemWithType:MineSettingItemsTypeHelp],
                   [SettingItem itemWithType:MineSettingItemsTypeRecommed]];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_tableView reloadData];
}

#pragma mark -
#pragma mark - 列表方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
//    return [ZRB_UserManager isLogin] ? 5 : 4;
    return [ZRB_UserManager isLogin] ? 6: 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 2 ? 3: 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //头部
    if (indexPath.section == 0)
    {
        MineSettingTopCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMineSettingTopCellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        [cell   initWihtModel:[ZRB_UserManager shareUserManager].userData];
        return cell;
        
    }else if (indexPath.section == 2)
    {
        MineSettingItemsCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMineSettingItemsCellIdentifier forIndexPath:indexPath];
        SettingItem   *model = [_dataArray objectAtIndex:indexPath.row];
        model.indexPath = indexPath;
        [cell initWithItem:model];
        return cell;
        
    }
    else if(indexPath.section == 4)
    {
        
        MineSettingItemsCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMineSettingItemsCellIdentifier forIndexPath:indexPath];
        SettingItem *model = [_dataArray lastObject];
        model.indexPath = indexPath;
        [cell initWithItem:model];
        return cell;
        
    }
    else if(indexPath.section == 5)
    {
        MineSettingBottomCell *cell =  [_tableView dequeueReusableCellWithIdentifier:kMineSettingBottomCellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        return cell;
    }else
    {
        UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:kMineSettingPaddingCellIdentifier forIndexPath:indexPath];
        return cell;
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return 60;/*头部*/
        case 1:
            return 10;/*间隙*/
        case 2:
            return 44;/*每一项*/
        case 3:
            return 10;/*间隙*/
        case 4:
            return 44;/*每一项*/
        case 5:
            return 80;/*退出登录*/
        default:
            return 0;
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0)
    {
        ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
        if(user){
            //跳转个人中心
            MinePersonalCenterViewController *eVC = (MinePersonalCenterViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MinePersonalCenterViewController class]];
            [self.navigationController pushViewController:eVC animated:YES];
        }
        
    }
    else if (indexPath.section == 2)
    {
        switch (indexPath.row) {
            case 0:
                //关于招融宝
                [self pushToAbout];
                break;
            case 1:
                //意见反馈
                [self pushFeedback];
                break;
            case 2:
                //使用帮助
                [self showAlertHelpView];
                break;

            default:
                break;
        }
        
    }
    else if (indexPath.section == 4){
        //分享
        
        MineShareViewController *sVC = (MineShareViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineShareViewController class]];
        [self.navigationController pushViewController:sVC animated:YES];
    }

}



#pragma mark - MineSettingTopCellDelegate
- (void)pushToLogin{
    
    //跳转登录页面
    //成功后跳转回本页
    MineLoginViewcController *loginVC = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
    loginVC.hidesBottomBarWhenPushed = YES;
    loginVC.loginSuccssBlock = ^{
        if (self.loginSuccssBlock)
        {
            self.loginSuccssBlock();
        }
    };
    [self.navigationController pushViewController:loginVC animated:YES];
    //在主页 设置成功之后的页面刷新loginSuccssBlock
    NSArray *VCs = self.navigationController.viewControllers;
    MineContainerViewController *controller = (MineContainerViewController *)[VCs firstObject];
    if([controller.currentVC isEqual:controller.mineVC]){
        loginVC.loginSuccssBlock = ^{
            if (controller.mineVC.loginSuccssBlock)
            {
                controller.mineVC.loginSuccssBlock();
            }
        };
    }
}


- (void)pushToRegister{
    //跳转注册页面
    MineRegisterViewController *reViewController = (MineRegisterViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterViewController class]];
    [self.navigationController pushViewController:reViewController animated:YES];
}

#pragma mark - MineSettingBottomCellDelegate

- (void)userLogout{
    //退出登录
    [ZRB_UserManager logOut];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

#pragma mark - action
- (void)pushToAbout{
    MineSettingAboutUsViewController *aboutCtrl = (MineSettingAboutUsViewController*)[StoryBoardUtilities viewControllerForStoryboardName:kPersonalStoryboardName class:[MineSettingAboutUsViewController class]];
    [self.navigationController pushViewController:aboutCtrl animated:YES];
}

//意见反馈
- (void)pushFeedback{
    MineSettingFeedBackViewController *feedbackController = (MineSettingFeedBackViewController*)[StoryBoardUtilities viewControllerForStoryboardName:kPersonalStoryboardName class:[MineSettingFeedBackViewController class]];
    [self.navigationController pushViewController:feedbackController animated:YES];
}

//使用帮助
-(void)showAlertHelpView{
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString: @"若有任何疑问\n\n可及时联系我们人工客服\n"];
    if(_alertView != nil){
        [_alertView removeFromSuperview];
        _alertView = nil;
    }
    _alertView = [[MinePersonalZRBAlertView alloc] initWithText:attString];
    [_alertView showOnView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
}


@end
