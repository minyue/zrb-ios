//
//  MineSetupPwdViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/21.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  找回密码

#import "ZRB_ViewController.h"

@interface MineSetupPwdViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic,assign) int pageType;//0是从注册入口进，1是从登陆后修改密码-找回密码入口进
@property (nonatomic,copy) NSString *mobile;
@end
