//
//  MineSetupPwdViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/21.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineSetupPwdViewController.h"
#import "JKCountDownButton.h"
#import "UINavigationItem+CustomItem.h"
#import "TOTextInputChecker.h"
#import "UIButton+ZRB.h"
#import "RegisterService.h"
#import "RegisterModel.h"
#import "MineLoginViewcController.h"
@interface MineSetupPwdViewController()
{
    IBOutlet  ZRB_TextField  *_newPwdTextField;
    
    IBOutlet  ZRB_TextField  *_oldPwdTextField;
    
    IBOutlet  ZRB_TextField  *_smsCodeTextField;
    
    TOTextInputChecker       *_newPwdChecker;
    
    TOTextInputChecker       *_oldPwdChecker;
    
    TOTextInputChecker       *_smsCodeChecker;
    
    IBOutlet  JKCountDownButton *_countDownButton;
    
    IBOutlet  UIView           *_helpView;
    
    CustomBarItem              *_rightItem;
    
    RegisterService             *_service;
}
- (void)setup;


- (void)createRightItem;
@end

@implementation MineSetupPwdViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup ];
    [self createRightItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setup
{
    WS(bself);
    
    _service = [[RegisterService alloc] init];
    
    NSLayoutConstraint *con  = [_helpView findOwnConstraintForAttribute:NSLayoutAttributeWidth];
    con.constant = UIScreenWidth;
    
    _newPwdChecker = [TOTextInputChecker passwordChecker];
    _newPwdChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself check];
    };
    [_newPwdTextField  initWithTitle:@"新密码" placeHolder:@"请设置6~16位新密码"];
    _newPwdTextField.delegate = _newPwdChecker;
    
    _oldPwdChecker = [TOTextInputChecker passwordChecker];
    _oldPwdChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself check];
    };
    [_oldPwdTextField initWithTitle:@"确认密码" placeHolder:@"确认您的密码"];
    _oldPwdTextField.delegate = _oldPwdChecker;
    
    _smsCodeChecker = [TOTextInputChecker codeChecker];
    _smsCodeChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself check];
    };
    [_smsCodeTextField initWithTitle:@"验证码" placeHolder:@"请输入短信中得6位验证码"];
    _smsCodeTextField.delegate = _smsCodeChecker;
    [_countDownButton configButtonWithType:ZRB_ButtonTypeOperation];
    [_countDownButton addToucheHandler:^(JKCountDownButton*sender, NSInteger tag) {
        [bself smsButtonState:NO];
        [bself getSMSCode];
        [sender startWithSecond:60];
        [sender didChange:^NSString *(JKCountDownButton *countDownButton,int second) {
//            NSString *title = [NSString stringWithFormat:@"剩余%d秒",second];
            NSString *title = [NSString stringWithFormat:@"%ds",second];
            return title;
        }];
        [sender didFinished:^NSString *(JKCountDownButton *countDownButton, int second) {
            [bself smsButtonState:YES];
            return @"重新获取 60秒";
            
        }];
    }];
    [self smsButtonState:YES];

}


/**
 *  设置完成按钮
 */
- (void)createRightItem
{
    NSString *title = @"完成";
    _rightItem =  [self.navigationItem setItemWithTitle:title textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_rightItem setOffset:-2];
    [_rightItem addTarget:self selector:@selector(finishModify:) event:UIControlEventTouchUpInside];
    _rightItem.contentBarItem.enabled = NO;
    [_rightItem.contentBarItem setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
}


/**
 *  设置倒计时按钮的状态
 *
 *  @param state 倒计时
 */
- (void)smsButtonState:(BOOL)state
{
    if (state)
    {
        _countDownButton.enabled = YES;
        [_countDownButton setBackgroundColor:[UIColor colorWithHexString:@"4997ec"] forState:UIControlStateNormal];
    }else
    {
        _countDownButton.enabled = NO;
        [_countDownButton setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"] forState:UIControlStateDisabled];
        
    }
}


/**
 *  获取短信验证码
 */
- (void)getSMSCode
{

    [self.view endEditing:YES];
    [_service sendSMSFindPWWithMobile:self.mobile success:^(id responseObject) {
        [SVProgressHUD dismiss];
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        if (model.res == ZRBHttpSuccssType)
        {
            //获取验证码成功
        }else
        {
            [self showTipViewWithMsg:model.msg];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:kHttpFailError];
        
    }];
    
    [SVProgressHUD show];

}

/**
 *  完成输入
 *
 *  @param sender
 */
- (void)finishModify:(id)sender
{
    
    if ([_newPwdTextField.text isEqualToString:_oldPwdTextField.text])
    {
        [_service changePWordWithMobile:self.mobile pwd:_newPwdTextField.text smsCode:_smsCodeTextField.text success:^(id responseObject) {
            [SVProgressHUD dismiss];
            RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
            if (model.res == ZRBHttpSuccssType)
            {
                [self showTipViewWithMsg:model.msg];
                dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 1.0 *NSEC_PER_SEC);
                dispatch_after(afterTime, dispatch_get_main_queue(), ^{
                    if(self.pageType != 1){
                        //返回到登陆页面
                        ZRB_ViewController    *temp = nil;
                        
                        for (ZRB_ViewController *VC in self.navigationController.viewControllers)
                        {
                            if ([VC isKindOfClass:[MineLoginViewcController class]]) {
                                temp = VC;
                                break;
                            }
                        }
                        
                        if (temp) {
                            [self.navigationController popToViewController:temp animated:YES];
                        }
                    }else{
                        //返回到个人中心
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                });
            }else
            {
                [self showTipViewWithMsg:model.msg];
            }
            
        } failure:^(NSError *error) {
            [SVProgressHUD dismiss];
            [self showTipViewWithMsg:kHttpFailError];
        }];
        
        [SVProgressHUD show];
    }else
    {
        [self showTipViewWithMsg:@"两次密码输入不一致，请重新输入。"];
    }

}



- (void)check
{
    if ([_newPwdChecker finalCheck] == InputCheckErrorNone
        &&[_oldPwdChecker finalCheck] == InputCheckErrorNone
        && [_smsCodeChecker finalCheck] == InputCheckErrorNone)
    {
        _rightItem.contentBarItem.enabled = YES;

    }else
    {
        _rightItem.contentBarItem.enabled = NO;

    }
}

@end
