//
//  MineShareTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  分享招融宝Cell

#import <UIKit/UIKit.h>

static NSString *const mineShareTableViewCellIdentifier = @"MineShareTableViewCellIdentifier";

@interface MineShareTableViewCell : UITableViewCell

- (void)initWithString:(NSString*)str;
@end
