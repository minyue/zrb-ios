//
//  MineShareTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineShareTableViewCell.h"
@interface MineShareTableViewCell(){
    IBOutlet  UIImageView *_iconView;
    IBOutlet  UILabel   *_lblTitle;
}
@property (nonatomic,copy) NSString *normalImgName;
@property (nonatomic,assign) ZRBSocialSnsType type;
@end

@implementation MineShareTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithString:(NSString*)str{
    [self initWithName:str];
    [self setItemButtonImageName];
}

- (void)initWithName:(NSString *)name
{
    if ([name isEqualToString:kShareToWechat]) {
        self.type = ZRBSocialSnsTypeWeChat;
    }
    else if ([name isEqualToString:kShareToWechatTimeline]){
        self.type = ZRBSocialSnsTypeWeChatTimeLine;
    }
    else if ([name isEqualToString:kShareToQQ]){
        self.type = ZRBSocialSnsTypeQQ;
    }
    else if ([name isEqualToString:kShareToQzone]){
        self.type = ZRBSocialSnsTypeQzone;
    }
    else if ([name isEqualToString:kShareToSinaBlog]){
        self.type = ZRBSocialSnsTypeSinaBlog;
    }
//    _lblTitle.text = name;
}

- (void)setItemButtonImageName
{
    
    switch (self.type) {
        case ZRBSocialSnsTypeWeChat:
        {
            self.normalImgName = @"news_control_center_wechat";
            _lblTitle.text = @"分享给微信好友";
            break;
        }
        case ZRBSocialSnsTypeWeChatTimeLine:
        {
            self.normalImgName = @"news_control_center_wechatQ";
            _lblTitle.text = @"分享到微信朋友圈";
            
            break;
        }
        case ZRBSocialSnsTypeQQ:
        {
            self.normalImgName = @"news_control_center_qq";
            _lblTitle.text = @"分享给QQ好友";
            break;
        }
        case ZRBSocialSnsTypeQzone:
        {
            self.normalImgName = @"news_control_center_zone";
            _lblTitle.text = @"分享到QQ空间";
            break;
        }
        case ZRBSocialSnsTypeSinaBlog:
        {
            self.normalImgName = @"news_control_center_weibo";
            _lblTitle.text = @"分享到新浪微博";
            break;
        }
    
        default:
            break;
    }
    
    [_iconView setImage:[UIImage imageNamed:self.normalImgName]];
}
@end
