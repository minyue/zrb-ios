
//
//  MineShareViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineShareViewController.h"
#import "MineShareTableViewCell.h"
#import "ZhaoRongbaoConst.h"
#import "ShareManager.h"

#define TextMaxLength 100
#define textPlaceHodel @"我发现了招融宝可以看很多资讯，你也来下载试试？"

@interface MineShareViewController ()<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet    UITableView *_tableView;
    IBOutlet UIView *_contactInfoView;//输入栏
    IBOutlet UIView *_countDownLabel;//倒计数栏
    IBOutlet UILabel *_lblCountDown;//到计数
    IBOutlet UITextView  *_txtView;//意见
    
    NSArray  *_itemsArray;
}
@end

@implementation MineShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textViewEditChanged:) name:@"UITextViewTextDidChangeNotification"
                                              object:_txtView];
    
    _txtView.text = textPlaceHodel;
    
    _itemsArray = @[kShareToWechat,kShareToWechatTimeline,kShareToQQ,kShareToQzone];
    
//    UIView *view = [[UIView alloc]init];
//    view.backgroundColor = [UIColor clearColor];
//    _tableView.tableFooterView = view;
    [_tableView reloadData];
    
}

#pragma mark -
#pragma mark - 列表方法
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineShareTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineShareTableViewCellIdentifier forIndexPath:indexPath];
  
    [cell  initWithString:[_itemsArray objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;/*每一项*/
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *shareTxt;
    if(_txtView.text.length<1){
        shareTxt = textPlaceHodel;
    }else{
        shareTxt = _txtView.text;
    }
    NSString *clickstr = [_itemsArray objectAtIndex:indexPath.row];
    if([clickstr isEqualToString:kShareToWechat]){
        [[ShareManager ShareManager] WXChatShareWithViewControll:shareTxt URLStr:ZRBINDEXSHAREURL];
    }else if([clickstr isEqualToString:kShareToQQ]){
        [[ShareManager ShareManager] QQShareWithViewControll:shareTxt URLStr:ZRBINDEXSHAREURL];
    }else if([clickstr isEqualToString:kShareToWechatTimeline]){
        [[ShareManager ShareManager] WXFriendShareWithViewControll:shareTxt URLStr:ZRBINDEXSHAREURL];
    }else if([clickstr isEqualToString:kShareToQzone]){
        [[ShareManager ShareManager] QQZoneShareWithViewControll:shareTxt URLStr:ZRBINDEXSHAREURL];
    }

}

#pragma mark - UITextView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if([[textView text] isEqualToString:textPlaceHodel]){
        textView.text = @"";
    }
    _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [textView text].length)];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        _txtView.text = textPlaceHodel;
    }
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    NSString *new = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if(new.length > TextMaxLength){
        
        if (![text isEqualToString:@""]) {
            return NO;
        }
    }
    return YES;
}

-(void)textViewEditChanged:(NSNotification *)obj{
    
    UITextField *textField = (UITextField *)obj.object;
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入模式
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > TextMaxLength) {
                _txtView.text = [toBeString substringToIndex:TextMaxLength-1];
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtView text].length)];
            }else{
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtView text].length)];
            }
        }
        else{
            if(toBeString.length <= TextMaxLength){
                _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtView text].length)];
            }else{
                _txtView.text = [toBeString substringToIndex:TextMaxLength-1];
                _lblCountDown.text = @"0";
            }
            
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > TextMaxLength) {
            _txtView.text = [toBeString substringToIndex:TextMaxLength];
        }
        _lblCountDown.text = [NSString stringWithFormat:@"%lu",(TextMaxLength - [_txtView text].length)];
    }
    
}



-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self
                                                   name:@"UITextViewTextDidChangeNotification"
                                                 object:_txtView];
}

@end
