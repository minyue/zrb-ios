//
//  MineUserResisterViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/17.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineUserResisterViewController.h"
#import "TOTextInputChecker.h"
#import "MineEnterpriseCerViewController.h"
#import "RegisterService.h"
#import "RegisterModel.h"
#import "MineRegisterInfoViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "ZRB_UserRegisterInfo.h"

@interface MineUserResisterViewController()
{
    
    IBOutlet   ZRB_TextField   *_passWordTextField;
    IBOutlet   ZRB_TextField   *_rePassWordTextField;
    
    IBOutlet  UIButton       *_nextButton;      //下一步
    
//    IBOutlet  UIView         *_lineView;        //线条
    
    
    TOTextInputChecker        *_rePassWordChecker;        //用户名检察器
    
    TOTextInputChecker        *_passWordChecker;        //用户密码检察器
    
    
    RegisterService           *_service;

}


- (IBAction)next:(id)sender;

@end

@implementation MineUserResisterViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setup
{
    _service = [[RegisterService alloc] init];
    
    WS(bself)
    _passWordChecker = [TOTextInputChecker passwordChecker];
    _passWordChecker.checkInputBlock = ^(BOOL flag)
    {
        [bself check];
        [bself pwdIconActive:flag];
    };

    _passWordTextField.showIconWhenSuccess = YES;
    _passWordTextField.textFiled.clearButtonMode = UITextFieldViewModeNever;
    _passWordTextField.delegate = _passWordChecker;
    [_passWordTextField initWithTitle:@"密码" placeHolder:@"请设置 6 ~16 位密码"];
    

    _rePassWordChecker = [TOTextInputChecker passwordChecker];
    _rePassWordChecker.checkInputBlock = ^(BOOL flag){
        [bself check];
        [bself repwdIconActive:flag];
    };
    _rePassWordTextField.showIconWhenSuccess = YES;
    _rePassWordTextField.textFiled.clearButtonMode = UITextFieldViewModeNever;
    _rePassWordTextField.delegate = _rePassWordChecker;
    [_rePassWordTextField initWithTitle:@"重复密码" placeHolder:@"请再次输入您的密码"];
    
//    _nextButton.layer.cornerRadius = (65/2);
//    _nextButton.layer.masksToBounds = YES;
    [_nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [_nextButton.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    [self nextButtonActive:NO];
    
    
}



- (void)nextButtonActive:(BOOL)isActive
{
    if (isActive)
    {
        _nextButton.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        _nextButton.enabled = YES;
        [_nextButton setTitleColor:[UIColor colorWithHexString:@"ffffff"]];
//        _lineView.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
        
    }else
    {
        _nextButton.backgroundColor = [UIColor colorWithHexString:@"AAAAAA"];
        _nextButton.enabled = NO;
        [_nextButton setTitleColor:[UIColor colorWithHexString:@"5f5f5f"]];
    }
}

- (void)check
{
    if ([_rePassWordChecker finalCheck] == InputCheckErrorNone && [_passWordChecker finalCheck] == InputCheckErrorNone) {
        [self nextButtonActive:YES];
    }else
    {
        [self nextButtonActive:NO];
    }
}



/**
 *  是否展示当用户名验证成功后的小图片
 *
 *  @param isActive YES：展示  NO：不展示
 */
- (void)pwdIconActive:(BOOL)isActive
{
    [_passWordTextField showIconView:isActive];

}


/**
 *  是否展示当密码验证成功后的小图片
 *
 *  @param isActive YES：展示  NO：不展示
 */
- (void)repwdIconActive:(BOOL)isActive
{
    [_rePassWordTextField showIconView:isActive];
    
}

/**
 *  下一步
 *
 *  @param sende
 */
- (IBAction)next:(id)sender
{
    [self checkPassword];
    //for test
//    [self gotoMineRegisterInfoViewController];
}

- (void)checkPassword{
    if([_passWordTextField.text isEqualToString:_rePassWordTextField.text]){
        WS(bself)
        [RegisterRequest shareInstance].passWord = _passWordTextField.text;
        [bself saveAppUser];
    }else{
        [self showTipViewWithMsg:@"两次输入的密码不一致"];
    }
}

/*
- (void)checkUnameRegister
{
    WS(bself)
    [_service checkUnameRegisterWithUserName:_userTextField.text success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        
        if (model.res == ZRBHttpSuccssType) {
            //缺一步- 保存用户信息 提交到服务器
            [RegisterRequest shareInstance].uname = _userTextField.text;
            [RegisterRequest shareInstance].passWord = _passWordTextField.text;
            [bself saveAppUser];
        }else
        {
            [bself showTipViewWithMsg:model.msg];
        }

    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
    
    [SVProgressHUD show];
}*/

//注册--保存用户信息
- (void)saveAppUser
{
    [_rePassWordTextField resignFirstResponder];
    [_passWordTextField resignFirstResponder];
    WS(bself);
    [_service saveAppUserWithRequest:[RegisterRequest shareInstance] success:^(id responseObject) {
        [SVProgressHUD dismiss];
        RegisterNormalModel *model = (RegisterNormalModel *)responseObject;
        
        if (model.res == ZRBHttpSuccssType) {
            [MBProgressHUD showHUDTitle:@"恭喜您注册成功" onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
            dispatch_time_t afterTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) 1.5 *NSEC_PER_SEC);
            dispatch_after(afterTime, dispatch_get_main_queue(), ^{
                [bself gotoMineRegisterInfoViewController];
            });
        }else
        {
            [bself showTipViewWithMsg:model.msg];
        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        
        [bself showTipViewWithMsg:kHttpFailError];
        
    }];
    
    [SVProgressHUD show];
}

- (void)gotoMineRegisterInfoViewController
{
    RegisterInfoModel *info = (RegisterInfoModel*)[ZRB_UserRegisterInfo shareUserRegisterInfo].model;
    info.phone = [RegisterRequest shareInstance].mobile;
    info.pwd = [RegisterRequest shareInstance].passWord;
    
    
    MineRegisterInfoViewController *eVC = (MineRegisterInfoViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterInfoViewController class]];
    eVC.currentStep = 0;
    eVC.phoneNum = info.phone;
    [self.navigationController pushViewController:eVC animated:YES];
    
}


//- (void)gotoEnterpriseViewController
//{
//    
//    [RegisterRequest shareInstance].uname = _userTextField.text;
//    [RegisterRequest shareInstance].passWord = _passWordTextField.text;
//    MineEnterpriseCerViewController *eVC = (MineEnterpriseCerViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineEnterpriseCerViewController class]];
//    [self.navigationController pushViewController:eVC animated:YES];
//
//}
@end
