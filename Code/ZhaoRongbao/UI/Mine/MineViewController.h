//
//  MineViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  登录注册开始界面

#import "ZRB_ViewController.h"

@interface MineViewController : ZRB_ViewController

@property (nonatomic,strong) NSString   *imgName;

@property (nonatomic,copy) void (^loginSuccssBlock)();

- (IBAction)pushToLogin;

- (IBAction)pushToRegister;
@end
