//
//  MineViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineViewController.h"
#import "UIButton+ZRB.h"
#import "UINavigationItem+CustomItem.h"
#import "MineLoginViewcController.h"
#import "MineRegisterViewController.h"
#import "MineRegisterInfoViewController.h"

static  NSString *const kLoginSegue = @"kLoginSegue";

static  NSString *const kRegisterSegue = @"kRegisterSegue";

@interface MineViewController ()
{

    IBOutlet  UIButton  *_loginButton;
    
    IBOutlet  UIButton  *_reButton;
    
    IBOutlet  UIView    *_btnViews;
    
}
@property  (nonatomic,weak) IBOutlet UIImageView    *imgBig;

- (void)setup;
@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(SCREEN_HEIGHT == 480){
        [_btnViews mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view.mas_bottom).offset(-111);
        }];
    }else if(SCREEN_HEIGHT == 568){
        [_btnViews mas_makeConstraints:^(MASConstraintMaker *make) {

                make.bottom.equalTo(self.view.mas_bottom).offset(-130);
        }];
    }else if(SCREEN_HEIGHT > 568){
        [_btnViews mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view.mas_bottom).offset(-180);
        }];
    }
}

- (void)setup
{
    [_loginButton configButtonWithType:ZRB_ButtonTypeOperation];
    [_reButton configButtonWithType:ZRB_ButtonTypeUnderLine];
    
    if(!_imgName){
        _imgBig.image = [UIImage imageNamed:@"bg_my_unlogin3"];
    }else{
        _imgBig.image = [UIImage imageNamed:_imgName];
    }
    
    if(SCREEN_HEIGHT > 568){
        //6 以及更长的屏
        [_imgBig makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_top).offset(80);
        }];
    }else if(SCREEN_HEIGHT == 480){
//        [_btnViews mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.equalTo(self.view.mas_bottom).offset(-45);
//        }];
    }
    
}



 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     
     WS(bself)
     if ([segue.identifier isEqualToString:kRegisterSegue]) {
         MineRegisterViewController *viewController = segue.destinationViewController;
         viewController.hidesBottomBarWhenPushed = YES;
         
//         //for test
//         MineRegisterInfoViewController *eVC = (MineRegisterInfoViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Mine" class:[MineRegisterInfoViewController class]];
//         eVC.currentStep = 0;
//         eVC.phoneNum = @"13545173623";
//         [self.navigationController pushViewController:eVC animated:YES];

     }else if ([segue.identifier isEqualToString:kLoginSegue])
     {
         
         MineLoginViewcController *loginVC = segue.destinationViewController;
         loginVC.hidesBottomBarWhenPushed = YES;
         loginVC.loginSuccssBlock = ^{
             if (bself.loginSuccssBlock)
             {
                 bself.loginSuccssBlock();
             }
         };

     }
}


- (IBAction)pushToLogin{
    [self performSegueWithIdentifier:@"kLoginSegue" sender:self];

}

- (IBAction)pushToRegister{
    //跳转注册页面
    [self performSegueWithIdentifier:@"kRegisterSegue" sender:self];
}

@end
