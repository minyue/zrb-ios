//
//  ProjectListCell.h
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/12.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectListModel.h"
#import "UIFont+GillSansFonts.h"
#import "BTLabel.h"

@interface ProjectListCell : UITableViewCell

- (void)initWithModel:(ProjectItemModel *)model;


@property (nonatomic,strong)UIImageView *bigPic;              //左边大图
@property (nonatomic,strong)UIImageView *typePic;             //资产类型图标
@property (nonatomic,strong)BTLabel *title;                   //项目标题
@property (nonatomic,strong)UIImageView *certificationPic;    //认证图标
@property (nonatomic,strong)UILabel *certificationLB;         //认证类型
@property (nonatomic,strong)UILabel *areaLB;                  //面积
@property (nonatomic,strong)UIView *line;
@property (nonatomic,strong)UILabel *price;                   //估值
@property (nonatomic,strong)UILabel *location;                //地址
@property (nonatomic,strong)UILabel *date;                    //时间

@property (nonatomic,strong)UIImageView *defaultImage;        //占位图
@end
