//
//  MineHomeContentCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/28.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HomeContentModel;
typedef NS_ENUM(NSInteger, MineHomeContentType){
    /**
     *  我的关注
     */
    MineHomeContentTypeOne,
    /**
     *  我的项目
     */
    MineHomeContentTypeTwo,
    /**
     *  我的资金
     */
    MineHomeContentTypeThree,
    /**
     *  我的消息
     */
    MineHomeContentTypeFour,
    /**
     *  认证介绍
     */
    MineHomeContentTypeFive,
    /**
     *  会员服务
     */
    MineHomeContentTypeSix,
    /**
     *  我的发布
     */
    MineHomeContentTypeSeven
};

@interface MineHomeContentCell : UITableViewCell



- (void)initWithModel:(HomeContentModel *)model;
@end



@interface HomeContentModel : NSObject

@property (nonatomic,strong) UIImage *image;


@property (nonatomic,copy) NSString *title;

@property (nonatomic,strong) NSIndexPath *indexPath;

@property (nonatomic,assign) MineHomeContentType type;


+ (HomeContentModel *)modelWithType:(MineHomeContentType)type;

@end