//
//  MineHomeContentCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/28.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineHomeContentCell.h"

@interface MineHomeContentCell()
{

    IBOutlet   UIImageView  *_imageView;
    
    IBOutlet   UILabel      *_titleLabel;
    
    ZRB_LineView            *_lineView;
}

@end

@implementation MineHomeContentCell

- (void)awakeFromNib {
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setup];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setup
{

    _lineView = [[ZRB_LineView alloc] init];
    [self.contentView addSubview:_lineView];
    [_lineView   makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.right.and.top.equalTo(self.contentView);
        make.height.equalTo(1);
    }];
}

- (void)initWithModel:(HomeContentModel *)model
{
    _imageView.image = model.image;
    
    _titleLabel.text = model.title;
    
    if (model.indexPath.row == 0) {
        _lineView.hidden = NO;
    }else
    {
        _lineView.hidden = YES;
    }

}
@end


@implementation HomeContentModel

+ (HomeContentModel *)modelWithType:(MineHomeContentType)type
{
    HomeContentModel *model = [[HomeContentModel alloc] init];
    model.type = type;
    switch (type)
    {
        case MineHomeContentTypeOne:
        {
            model.image = [UIImage imageNamed:@"ico_mylike"];
            model.title = @"我的关注";
            break;
        }
        case MineHomeContentTypeTwo:
        {
            model.image = [UIImage imageNamed:@"ico_myproj"];
            model.title = @"我的项目";
            break;
        }
        case MineHomeContentTypeThree:
        {
            model.image = [UIImage imageNamed:@"ico_mycash"];
            model.title = @"我的资金";
            break;
        }
        case MineHomeContentTypeFour:
        {
            model.image = [UIImage imageNamed:@"ico_mynotifc"];
            model.title = @"我的消息";
            break;
        }
        case MineHomeContentTypeFive:
        {
            model.image = [UIImage imageNamed:@"ico_verify"];
            model.title = @"认证介绍";
            break;
        }
        case MineHomeContentTypeSix:
        {
            model.image = [UIImage imageNamed:@"ico_vipservice"];
            model.title = @"会员服务";
            break;
        }
        case MineHomeContentTypeSeven:
        {
            model.image = [UIImage imageNamed:@"ico_mypublish"];
            model.title = @"我的发布";
            break;
        }
            
            
        default:
            break;
    }
    
    return model;
    
}

@end