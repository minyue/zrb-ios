//
//  MineHomeTopCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/28.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineHomeTopCell : UITableViewCell

@property (nonatomic,copy) void(^uploadImageBlock)();
@property (nonatomic, strong) IBOutlet   UIImageView   *iconView;

- (void)initWihtModel:(ZRB_UserModel *)model;
@end
