//
//  MineHomeTopCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/28.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineHomeTopCell.h"
#import "UIButton+ZRB.h"
#import "UIImageView+TapGesture.h"
@interface MineHomeTopCell()
{
    
    IBOutlet   UILabel       *_nameLabel;
    
    IBOutlet   UILabel       *_companyLabel;
    
    IBOutlet   UILabel       *_companyDutyLabel;
    
    IBOutlet   UIButton      *_modifyButton;
}

@end

@implementation MineHomeTopCell

- (void)awakeFromNib {
    // Initialization code
    [self setup ];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (void)setup
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _iconView.layer.cornerRadius = _iconView.frameWidth/2;
    _iconView.layer.masksToBounds = YES;
    
    _modifyButton.layer.cornerRadius = 10.0f;
    _modifyButton.layer.masksToBounds= YES;
    _modifyButton.layer.borderWidth = 1.0f;
    _modifyButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [_modifyButton setBackgroundColor:[UIColor colorWithHexString:@"0077d9"] forState:UIControlStateNormal alpa:0.3];
    [_modifyButton setBackgroundColor:[UIColor colorWithHexString:@"00275b"] forState:UIControlStateHighlighted alpa:0.3];
    [_modifyButton setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
    
    [_iconView addTapGestureWithTarget:self action:@selector(tapGesture)];
}



- (void)tapGesture
{
    if (self.uploadImageBlock) {
        self.uploadImageBlock();
    }
}


- (void)initWihtModel:(ZRB_UserModel *)model
{
    if(model.realname){
        _nameLabel.text = model.realname;
    }else{
        _nameLabel.text = @"匿名";
    }
    
    UIImage *img = [ZRBUtilities ZRBUserIconFile];
    if(!img){
        img = [ZRBUtilities ZRB_UserDefaultImage];
    }
    
    if(model.avatar){
        [self.iconView sd_setImageWithURL:[ZRBUtilities urlWithIconPath:model.avatar] placeholderImage:img options:SDWebImageRetryFailed];
    }else{
        [self.iconView setImage:img];
    }
    if(model.org){
        _companyLabel.text = model.org;
    }else{
        _companyLabel.text = @"";
    }
    
    //所在行业·担任职务
    NSMutableString *mstr = [[NSMutableString alloc] init];
    if(model.industry){
        [mstr appendString:model.industry];
        if(model.position){
            [mstr appendFormat:@"·%@",model.position];
        }
    }else{
        if(model.position){
            [mstr appendString:model.position];
        }
    }
    _companyDutyLabel.text = mstr;
    
    
}

@end
