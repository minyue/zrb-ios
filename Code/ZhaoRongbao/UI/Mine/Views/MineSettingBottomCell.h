//
//  MineSettingBottomCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MineSettingBottomCellDelegate <NSObject>

- (void)userLogout;
@end

@interface MineSettingBottomCell : UITableViewCell

@property (nonatomic, assign) id <MineSettingBottomCellDelegate> delegate;

@end
