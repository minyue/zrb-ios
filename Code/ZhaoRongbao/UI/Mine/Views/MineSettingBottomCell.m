//
//  MineSettingBottomCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineSettingBottomCell.h"
#import "UIButton+ZRB.h"
@interface MineSettingBottomCell()
{
    IBOutlet UIButton   *_loginOutButton;
}

- (IBAction)loginOut:(id)sender;
@end

@implementation MineSettingBottomCell

- (void)awakeFromNib {
    // Initialization code
    [self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setup
{
    [_loginOutButton configButtonWithType:ZRB_ButtonTypeOperation];
}

/**
 *  退出登录
 *
 *  @param sender
 */
- (IBAction)loginOut:(id)sender
{
    if([self.delegate respondsToSelector:@selector(userLogout)]){
        [self.delegate userLogout];
    }
}

@end
