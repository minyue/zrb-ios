//
//  MineSettingItemsCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SettingItem;
typedef NS_ENUM(NSInteger, MineSettingItemsType){
    /**
     *  关于招融宝
     */
    MineSettingItemsTypeAbout,
    /**
     *  意见反馈
     */
    MineSettingItemsTypeIdea,
    /**
     *  试用帮助
     */
    MineSettingItemsTypeHelp,
    /**
     *  检查更新
     */
    MineSettingItemsTypeUpdate,
    /**
     *  将招融宝推荐给朋友
     */
    MineSettingItemsTypeRecommed
};

@interface MineSettingItemsCell : UITableViewCell


- (void)initWithItem:(SettingItem *)item;
@end





@interface SettingItem : NSObject

@property (nonatomic,strong) UIImage *image;

@property (nonatomic,copy) NSString *title;

@property (nonatomic,strong) NSIndexPath *indexPath;

@property (nonatomic,assign)MineSettingItemsType type;


+ (SettingItem *)itemWithType:(MineSettingItemsType)type;

@end