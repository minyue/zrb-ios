//
//  MineSettingItemsCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineSettingItemsCell.h"

@interface MineSettingItemsCell()
{
    IBOutlet UIImageView  *_imageView;
    
    IBOutlet UILabel      *_titleLabel;
    
    ZRB_LineView    *_lineView;
}

@end

@implementation MineSettingItemsCell

- (void)awakeFromNib {
    // Initialization code
    [self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setup
{
    
    _lineView = [[ZRB_LineView alloc] init];
    [self.contentView addSubview:_lineView];
    [_lineView   makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.right.and.top.equalTo(self.contentView);
        make.height.equalTo(1);
    }];
}


- (void)initWithItem:(SettingItem *)item
{
    _imageView.image = item.image;
    _titleLabel.text = item.title;
    
    if (item.indexPath.row == 0) {
        _lineView.hidden = NO;
    }else
    {
        _lineView.hidden = YES;
    }

}



@end


@implementation SettingItem

+ (SettingItem *)itemWithType:(MineSettingItemsType)type
{

    SettingItem *item = [[SettingItem alloc] init];
    item.type = type;
    
    switch (type) {
        case MineSettingItemsTypeAbout:
        {
            item.image = [UIImage imageNamed:@"my_setup_aboutus"];
            item.title = @"关于招融宝";
            break;
        }
        case MineSettingItemsTypeIdea:
        {
            item.image = [UIImage imageNamed:@"my_setup_feedback"];
            item.title = @"意见反馈";
            break;
        }
        case MineSettingItemsTypeHelp:
        {
            item.image = [UIImage imageNamed:@"my_setup_help"];
            item.title = @"联系客服";
            break;
        }case MineSettingItemsTypeUpdate:
        {
            item.image = [UIImage imageNamed:@"my_setup_update"];
            item.title = @"检查更新";
            break;
        }
        case MineSettingItemsTypeRecommed:
        {
            item.image = [UIImage imageNamed:@"my_setup_share"];
            item.title = @"将招融宝推荐给朋友";
            break;
        }
        default:
            break;
    }
    return item;

}

@end