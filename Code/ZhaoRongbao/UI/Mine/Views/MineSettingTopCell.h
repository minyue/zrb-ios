//
//  MineSettingTopCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MineSettingTopCellDelegate <NSObject>

- (void)pushToLogin;

- (void)pushToRegister;
@end

@interface MineSettingTopCell : UITableViewCell<MineSettingTopCellDelegate>

@property (nonatomic, assign) id <MineSettingTopCellDelegate> delegate;

- (void)initWihtModel:(ZRB_UserModel *)model;

@end
