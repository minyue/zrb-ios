//
//  MineSettingTopCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "MineSettingTopCell.h"

@interface MineSettingTopCell()
{
    IBOutlet UIImageView  *_imageIcon;
    
    IBOutlet UIImageView  *_imageArrow;
    
    IBOutlet UILabel      *_lblName;
    
    IBOutlet UIButton      *_btnRegister;
    
    IBOutlet UIButton      *_btnLogin;
    
}

@end

@implementation MineSettingTopCell

- (void)awakeFromNib {
    // Initialization code
    [self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setup
{

    ZRB_LineView *lineView = [[ZRB_LineView alloc] init];
    [self.contentView addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.right.and.top.equalTo(self.contentView);
        make.height.equalTo(1);
    }];
    
    
    _imageIcon.layer.cornerRadius = _imageIcon.frameWidth/2;
    _imageIcon.layer.masksToBounds = YES;
    
}

- (void)initWihtModel:(ZRB_UserModel *)model
{
    
    
    if(model.uid != nil ){
        _lblName.hidden = NO;
        _lblName.text = model.realname;
        _btnLogin.hidden = YES;
        _btnRegister.hidden = YES;
        _imageArrow.hidden = NO;
        UIImage *img = [ZRBUtilities ZRBUserIconFile];
        if(!img){
            img = [ZRBUtilities ZRB_UserDefaultImage];
        }
        if(model.avatar){
            [_imageIcon sd_setImageWithURL:[ZRBUtilities urlWithIconPath:model.avatar] placeholderImage:img options:SDWebImageRetryFailed];
        }else{
            [_imageIcon setImage:img];
        }
    }else{
        _lblName.hidden = YES;
        _btnLogin.hidden = NO;
        _btnRegister.hidden = NO;
        _imageArrow.hidden = YES;
        [_imageIcon setImage:[ZRBUtilities ZRB_UserDefaultImage]];
        return;
    }
    
}

- (IBAction)loginClick:(id)sender{
    if([self.delegate respondsToSelector:@selector(pushToLogin)]){
        [self.delegate pushToLogin];
    }
}

- (IBAction)registerClick:(id)sender{
    if([self.delegate respondsToSelector:@selector(pushToRegister)]){
        [self.delegate pushToRegister];
    }
}
@end
