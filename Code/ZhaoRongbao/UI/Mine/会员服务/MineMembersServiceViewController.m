//
//  MineMembersServiceViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/10/9.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "MineMembersServiceViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "MinePersonalZRBAlertView.h"
#import "AppDelegate.h"

@interface MineMembersServiceViewController ()
{
    IBOutlet UIWebView *_webView;
    JSContext    *_content;
    MinePersonalZRBAlertView    *_alertView;
}
@end

@implementation MineMembersServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadWebPageWithString:HTTP_MEMBERSERVICEZRB_URL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadWebPageWithString:(NSString*)urlString
{
    NSURL *url =[NSURL URLWithString:urlString];
    NSLog(urlString);
    NSURLRequest *request =[NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
    _webView.delegate = self;
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self setupJsContent];
}

- (void)setupJsContent
{
    //获取当前JS环境
    _content = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    // 打印异常
    _content.exceptionHandler =
    ^(JSContext *context, JSValue *exceptionValue)
    {
        context.exception = exceptionValue;
        LogInfo(@"%@", exceptionValue);
    };
    //获取JS事件
    WS(bself);
    _content[@"showtag"] = ^(int num,NSString *tag){
        LogInfo(@"num = ");
        [bself showAlertHelpView];
    };
}


-(void)showAlertHelpView{
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString: @"致电招融宝服务中心\n\n了解会员详情\n"];
    if(_alertView != nil){
        [_alertView removeFromSuperview];
        _alertView = nil;
    }
    _alertView = [[MinePersonalZRBAlertView alloc] initWithText:attString];
    [_alertView showOnView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
