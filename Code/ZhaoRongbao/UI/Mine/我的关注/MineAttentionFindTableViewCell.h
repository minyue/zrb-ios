//
//  MineAttentionFindTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/23.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  关注发现列表- cell

#import <UIKit/UIKit.h>
#import "AttentionModel.h"

static NSString *const mineAttentionFindTableViewCell = @"MineAttentionFindTableViewCellIdentifier";

@interface MineAttentionFindTableViewCell : UITableViewCell

- (void)initWithModel:(AttentionModel*)model;

@end
