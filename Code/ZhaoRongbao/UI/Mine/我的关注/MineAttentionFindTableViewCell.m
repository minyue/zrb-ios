//
//  MineAttentionFindTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/23.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAttentionFindTableViewCell.h"

@interface MineAttentionFindTableViewCell (){
    IBOutlet    UILabel *_lblTitle;
    IBOutlet    UILabel *_lblLeft;
    IBOutlet    UILabel *_lblTime;
}
@end

@implementation MineAttentionFindTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithModel:(AttentionModel*)model{
    if(model.title){
        _lblTitle.text = model.title;
    }else{
        _lblTitle.text = @"";
    }
    NSString *t1 = model.typeName ? model.typeName :@"";
    NSString *t2 = model.userName ? model.userName:@"";
    _lblLeft.text = [NSString stringWithFormat:@"%@  %@",t1,t2];
    if(model.createTime){
        _lblTime.text = [ZRBUtilities stringToData:@"yyyy.MM.dd" interval:model.createTime];
    }else{
        _lblTime.text = @"";
    }
}

@end
