//
//  MineAttentionHomeViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/23.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAttentionHomeViewController.h"
#import "MineAttentionFindViewController.h"
#import "MineAttentionMoneyViewController.h"
#import "MineAttentionProjectViewController.h"

@interface MineAttentionHomeViewController ()<UIScrollViewDelegate>
{
    UIScrollView *_scrollView;
    IBOutlet   UIView  *_scrollLine;
    IBOutlet UIView *_scrollHeadView;
    
    IBOutlet    UIButton *_btn1;
    IBOutlet    UIButton *_btn2;
    IBOutlet    UIButton *_btn3;
    
    NSMutableArray  *_mainViewsArray;
}
@end

@implementation MineAttentionHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpNavigationBar{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(-50, 15, 320, 40)];
    view = [[[UINib nibWithNibName:@"MineAttentionsView" bundle:[NSBundle mainBundle]] instantiateWithOwner:self options:nil] lastObject];
    
//    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
//    
//    view.autoresizesSubviews = YES;
    [self.navigationItem setTitleView:view];
//    [self.navigationController.navigationBar addSubview:view];
}

- (void)setupScrollView{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
    _scrollView.delegate = self;
    _scrollView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
//    _scrollView.backgroundColor = [];
//    _scrollView.showsVerticalScrollIndicator =YES; //垂直方向的滚动指示
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.scrollEnabled = NO;
//    _scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;//滚动指示的风格
    _scrollView.bounces = NO;
    
    [self.view addSubview:_scrollView];
    
    
    NSLog(@"%@ ----- %.0f",_scrollView,UIScreenWidth);
    MineAttentionFindViewController *listVC1 = (MineAttentionFindViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAttentionFindViewController class]];
    [self addChildViewController:listVC1];
    [_scrollView addSubview:listVC1.view];
    [_mainViewsArray     addObject:listVC1];
    UIView  *view = listVC1.view;
    view.frameHeight = _scrollView.frameSize.height;
    view.frameWidth = UIScreenWidth;
    view.frameX = UIScreenWidth * 0;
    view.frameY = 0;
   _scrollView.contentSize = CGSizeMake(UIScreenWidth * 1, _scrollView.contentSize.height);
    
    
    MineAttentionProjectViewController *listVC2 = (MineAttentionProjectViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAttentionProjectViewController class]];
    [self addChildViewController:listVC2];
    [_scrollView addSubview:listVC2.view];
    [_mainViewsArray     addObject:listVC2];
    UIView  *view2 = listVC2.view;
    view.frameHeight = _scrollView.frameSize.height;
    view.frameWidth = UIScreenWidth;
    view2.frameX = UIScreenWidth * 1;
    view2.frameY = 0;
    _scrollView.contentSize = CGSizeMake(UIScreenWidth * 2, _scrollView.contentSize.height);
    
    MineAttentionMoneyViewController *listVC3 = (MineAttentionMoneyViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAttentionMoneyViewController class]];
    [self addChildViewController:listVC3];
    [_scrollView addSubview:listVC3.view];
    [_mainViewsArray     addObject:listVC3];
    UIView  *view3 = listVC3.view;
    view.frameHeight = _scrollView.frameSize.height;
    view.frameWidth = UIScreenWidth;
    view3.frameX = UIScreenWidth * 2;
    view3.frameY = 0;
    _scrollView.contentSize = CGSizeMake(UIScreenWidth * 3, _scrollView.contentSize.height);
}

- (void)setup
{
    

    _mainViewsArray = [NSMutableArray new];
    
    [self setupScrollView];
    [self setUpNavigationBar];
}

-(IBAction)doSomethingValueChange:(id)sender{
    UIButton *co = (UIButton*)sender;
    
    switch (co.tag) {
        case 0:
            _btn1.selected = YES;
            _btn2.selected = NO;
            _btn3.selected = NO;
            break;
        case 1:
            _btn1.selected = NO;
            _btn2.selected = YES;
            _btn3.selected = NO;
            break;
        case 2:
            _btn1.selected = NO;
            _btn2.selected = NO;
            _btn3.selected = YES;
            break;
        default:
            break;
    }
    
    [_scrollView setContentOffset:CGPointMake(UIScreenWidth * co.tag, _scrollView.contentOffset.y) animated:YES];
    
    [UIView beginAnimations:nil context:nil];//动画开始
    [UIView setAnimationDuration:0.3];
    CGRect frame = co.frame;
    _scrollLine.frame = CGRectMake(frame.origin.x, _scrollLine.frame.origin.y, _scrollLine.frame.size.width, _scrollLine.frame.size.height);
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self doSomethingValueChange:_btn1];
}
@end
