//
//  MineAttentionMoneyTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttentionModel.h"

static NSString *const mineAttentionMoneyTableViewCellIdentifier = @"MineAttentionMoneyTableViewCellIdentifier";

@interface MineAttentionMoneyTableViewCell : UITableViewCell
- (void)initWithModel:(AttentionModel*)model;
@end
