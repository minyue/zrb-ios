//
//  MineAttentionProjectTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的关注-关注项目Cell

#import <UIKit/UIKit.h>
#import "AttentionModel.h"

static NSString *const mineAttentionProjectTableViewCell = @"MineAttentionProjectTableViewCellIdentifier";

@interface MineAttentionProjectTableViewCell : UITableViewCell

- (void)initWithModel:(AttentionModel*)model;

@end
