//
//  MineAttentionProjectViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAttentionProjectViewController.h"
#import "MineAttentionProjectTableViewCell.h"
#import "SearchProjectDetailViewController.h"
#import "MineService.h"
#import "MJRefresh.h"
#import "AttentionModel.h"
#import "FcousModel.h"

@interface MineAttentionProjectViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *_tableView;
    
    MineService         *_myService;
    
    NSMutableArray      *_listArray;
    
    BOOL       _isRequst;            //是否请求过
}


@end

@implementation MineAttentionProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)setup
{
    _isRequst = NO;
    _myService = [[MineService alloc] init];
    _listArray = [NSMutableArray new];
    [self addHeaderView];
    [self addFooterView];
    [self refreshData];
}

//请求删除关注
- (void)cancelFcous:(NSIndexPath*)path{
    AttentionModel *itemModel = [_listArray objectAtIndex:path.row];
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousProject;
    request.fcousObjectId =  itemModel.aid;
    
    [SVProgressHUD show];
    AFBaseService *baseService = [[AFBaseService alloc]init];
    
    [baseService cancelFcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                [_listArray removeObjectAtIndex:path.row];
                [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:path] withRowAnimation:UITableViewRowAnimationLeft];
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"删除关注失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"删除关注失败"];
    }];
    
}


/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}

/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}

#pragma mark -
#pragma mark -网络请求

/**
 *  请求我关注的项目列表
 */
- (void)queryListData
{
    _isRequst = NO;
    
    AttentionProjectRequest *request = [[AttentionProjectRequest alloc]init];
    request.limit = 20;
    
    [self startWithReq:request];
    
}


/**
 *  加载更多
 */
- (void)loadMoreData
{
    if(_listArray.count > 0){
        
        AttentionModel *lastModel = (AttentionModel*)[_listArray lastObject];
        AttentionProjectRequest *request = [[AttentionProjectRequest alloc]init];
        request.limit = 20;
//        request.createTime = [ZRBUtilities stringToData:@"yyyy-MM-dd HH:mm:ss" interval:lastModel.createTime];
        request.createTime = lastModel.createTime;
        
        [self loadMoreWithReq:request];
    }else{
        [_tableView.header endRefreshing];
        
        [_tableView.footer endRefreshing];
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
}


/**
 *  下拉加载
 *
 *  @param req 请求体
 */
- (void)startWithReq:(AttentionProjectRequest *)req
{
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_myService mineAttentionProjectQueryWithRequest:req success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [bself attentionQueryListCallBackWithObject:(NSMutableArray*)responseObject];
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
    
    [SVProgressHUD show];
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(AttentionProjectRequest   *)req
{
    
    WS(bself);
    [_myService mineAttentionProjectQueryWithRequest:req success:^(id responseObject) {
        [bself loadMoreListCallBackWithObject:(NSMutableArray*)responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
}




- (void)attentionQueryListCallBackWithObject:(NSMutableArray *)modelArray
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    if(modelArray){
        _listArray = modelArray;
        [_tableView reloadData];
    }
    
    [_tableView.header endRefreshing];
    
}


- (void)loadMoreListCallBackWithObject:(NSMutableArray *)modelArray
{
    [_tableView.header endRefreshing];
    
    [_tableView.footer endRefreshing];
    if(modelArray){
        if(modelArray.count >0)
            [_listArray addObjectsFromArray:modelArray];
        [_tableView reloadData];
    }else{
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
    
}

#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineAttentionProjectTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineAttentionProjectTableViewCell forIndexPath:indexPath];
    AttentionModel *model = _listArray[indexPath.row];
    [cell initWithModel:model];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 65;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    ProjectItemModel *model = [[ProjectItemModel alloc]init];
    //    model = _listArray[indexPath.row];
    //    MineProjectReportViewController *detailCtrl = (MineProjectReportViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineProjectReportViewController class]];;
    //    detailCtrl.projectId = model.projectId;
    //    //    detailCtrl.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:detailCtrl animated:YES];
    
    AttentionModel *model = _listArray[indexPath.row];
    SearchProjectDetailViewController *detailCtrl = [[SearchProjectDetailViewController alloc]init];
    detailCtrl.projectId = model.aid;
    detailCtrl.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:detailCtrl animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCellEditingStyle result = UITableViewCellEditingStyleNone;//默认没有编辑风格
    if ([tableView isEqual:_tableView]) {
        result = UITableViewCellEditingStyleDelete;//设置编辑风格为删除风格
    }
    return result;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    [_tableView setEditing:editing animated:animated];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle ==UITableViewCellEditingStyleDelete) {
        if (indexPath.row < [_listArray count]) {
            [self cancelFcous:indexPath];
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}
@end
