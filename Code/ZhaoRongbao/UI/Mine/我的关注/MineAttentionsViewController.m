//
//  MineAttentionsViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/21.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAttentionsViewController.h"
#import "MineAttentionFindViewController.h"
#import "MineAttentionMoneyViewController.h"
#import "MineAttentionProjectViewController.h"

@interface MineAttentionsViewController ()
{
    UIScrollView *_scrollView;
    
    UIView      *_lineView;
    
    IBOutlet   UIView  *_scrollLine;
    IBOutlet UIView *_scrollHeadView;
    
    IBOutlet    UIButton *_btn1;
    IBOutlet    UIButton *_btn2;
    IBOutlet    UIButton *_btn3;
    
    NSMutableArray  *_mainViewsArray;
}
-(IBAction)doSomethingValueChange:(id)sender;
@end

@implementation MineAttentionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpNavigationBar{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 260, 44)];
    [self.navigationItem setTitleView:view];
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
}

- (void)setupScrollView{
    MineAttentionFindViewController *listVC1 = (MineAttentionFindViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAttentionFindViewController class]];
    [self addChildViewController:listVC1];
    [_scrollView addSubview:listVC1.view];
    [_mainViewsArray     addObject:listVC1];
    UIView  *view = listVC1.view;
    view.frameSize = _scrollView.frameSize;
    view.frameX = _scrollView.frameWidth * 0;
    view.frameY = 0;
    
    MineAttentionProjectViewController *listVC2 = (MineAttentionProjectViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAttentionProjectViewController class]];
    [self addChildViewController:listVC2];
    [_scrollView addSubview:listVC2.view];
    [_mainViewsArray     addObject:listVC2];
    UIView  *view2 = listVC2.view;
    view2.frameSize = _scrollView.frameSize;
    view2.frameX = _scrollView.frameWidth * 1;
    view2.frameY = 0;
    
    MineAttentionMoneyViewController *listVC3 = (MineAttentionMoneyViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Personal" class:[MineAttentionMoneyViewController class]];
    [self addChildViewController:listVC3];
    [_scrollView addSubview:listVC3.view];
    [_mainViewsArray     addObject:listVC3];
    UIView  *view3 = listVC3.view;
    view3.frameSize = _scrollView.frameSize;
    view3.frameX = _scrollView.frameWidth * 2;
    view3.frameY = 0;
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frameWidth * 3, _scrollView.contentSize.height);
    
}

- (void)setup
{
    
//    UIView  *downLine = [[UIView alloc] init];
//    downLine.backgroundColor = [UIColor colorWithHexString:@"e6e7ec"];
//    [self addSubview:downLine];
//    
//    [downLine makeConstraints:^(MASConstraintMaker *make) {
//        make.height.equalTo(@1);
//        make.leading.and.right.and.bottom.equalTo(self);
//    }];
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.backgroundColor =[UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_scrollView];
    
    [_scrollView makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.right.and.top.and.bottom.equalTo(self);
//        make.bottom.equalTo(downLine.mas_top);
    }];
    
    
//    _lineView = [[UIView alloc] init];
//    _lineView.backgroundColor = [UIColor colorWithHexString:@"eb4f38"];
//    [_scrollView addSubview:_lineView];
//    _lineView.hidden = YES;
    
    
    _mainViewsArray = [NSMutableArray new];
}

//
//-(void)doSomethingInSegment:(id)sender{
//    UISegmentedControl *co = (UISegmentedControl*)sender;
//
//    [_scrollView setContentOffset:CGPointMake(_scrollView.frameWidth * co.selectedSegmentIndex, _scrollView.contentOffset.y) animated:YES];
//}

-(IBAction)doSomethingValueChange:(id)sender{
    UIButton *co = (UIButton*)sender;
    
    switch (co.tag) {
        case 0:
            _btn1.selected = YES;
            _btn2.selected = NO;
            _btn3.selected = NO;
            break;
        case 1:
            _btn1.selected = NO;
            _btn2.selected = YES;
            _btn3.selected = NO;
            break;
        case 2:
            _btn1.selected = NO;
            _btn2.selected = NO;
            _btn3.selected = YES;
            break;
        default:
            break;
    }
    
    [_scrollView setContentOffset:CGPointMake(_scrollView.frameWidth * co.tag, _scrollView.contentOffset.y) animated:YES];
    
    [UIView beginAnimations:nil context:nil];//动画开始
    [UIView setAnimationDuration:0.3];
    CGRect frame = co.frame;
    _scrollLine.frame = CGRectMake(frame.origin.x, _scrollLine.frame.origin.y, _scrollLine.frame.size.width, _scrollLine.frame.size.height);
    
    [UIView commitAnimations];
}

//- (void)changeView:(CGFloat)x
//{
//    //取余
//    if (fmod(x, _scrollView.frameWidth) == 0)
//    {
//        //当前页面不处理
//        if (_currentPage == x / _scrollView.frameWidth) return;
//        
//        //获取新的页面
//        NSInteger  index = x / _scrollView.frameWidth;
//        
//        [_hrizontalMenu selectItemAtIndex:index];
//        
//        _currentPage = x / _scrollView.frameWidth ;
//        
//        //取出新的页面数据
//        if (!_mainViewsArray[index]) return;
//        
//        [self queryNewsListWithVC:_mainViewsArray[index]];
//    }
//}
@end
