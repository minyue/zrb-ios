//
//  MineAllPublishTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的发布Cell

#import <UIKit/UIKit.h>
#import "FindHomeModel.h"
@class MineAllPublishTableViewCell;
@protocol MineAllPublishTableViewCellDelegate <NSObject>

- (void)editCell:(MineAllPublishTableViewCell*)cell;

- (void)deleteCell:(MineAllPublishTableViewCell*)cell;

- (void)moveCell:(MineAllPublishTableViewCell*)cell isDelete:(BOOL)isDelete;

@end

static NSString *const mineAllPublishTableViewCellIdentifier = @"MineAllPublishTableViewCellIdentifier";

@interface MineAllPublishTableViewCell : UITableViewCell
@property (nonatomic,assign) id<MineAllPublishTableViewCellDelegate>   delegate;
@property (nonatomic,strong) FindHomeItemModel *currentModel;
@property (nonatomic,strong) NSIndexPath* index;
- (void)initWithModel:(FindHomeItemModel*)model;//我的发布

@end
