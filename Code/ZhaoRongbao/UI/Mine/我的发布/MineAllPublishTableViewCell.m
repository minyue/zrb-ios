//
//  MineAllPublishTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAllPublishTableViewCell.h"
#import "UIView+Additions.h"
#import "UIView+Extend.h"
@interface MineAllPublishTableViewCell (){
    IBOutlet    UILabel *_lblTitle;
    IBOutlet    UILabel *_lblLeft;
    IBOutlet    UILabel *_lblTime;
    IBOutlet    UIView  *_backView;
    IBOutlet    UIButton  *_editButton,*_deleteButton,*_cellButton;
}
@end

@implementation MineAllPublishTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    gesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [_backView addGestureRecognizer:gesture];
    
    [_cellButton makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.width.equalTo(self.mas_width);
        make.height.equalTo(self.mas_height);
    }];
    _cellButton.hidden = YES;
}

-(void)move:(UISwipeGestureRecognizer *)recognizer
{
        [self.delegate  moveCell:self isDelete:YES];
        [self   showDelete];

}

- (void)showDelete
{
    _deleteButton.hidden = NO;
    _cellButton.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        _backView.frame = CGRectMake(-180, 0, _backView.bounds.size.width, _backView.frame.size.height);
    }];
}

-(IBAction)resetCellFrame
{
    _cellButton.hidden = YES;
    [UIView animateWithDuration:0.3 animations:^{
        _backView.frame = CGRectMake(0, 0, _backView.bounds.size.width, _backView.frame.size.height);
        
    } completion:^(BOOL finished) {
        _deleteButton.hidden = YES;
        
    }];
}

- (IBAction)actionDelete:(id)sender
{
    [self.delegate  deleteCell:self];
    [self   resetCellFrame];
}

- (IBAction)actionEdit:(id)sender
{
    [self.delegate  editCell:self];
    [self   resetCellFrame];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initWithModel:(FindHomeItemModel*)model{
    self.currentModel = model;
    if(model.intro){
        _lblTitle.text = model.intro;
    }else{
        _lblTitle.text = @"";
    }
    int t1 = model.attentionTimes ? [model.attentionTimes intValue]:0;
    int t2 = model.messageAmount ? [model.messageAmount intValue]:0;
    _lblLeft.text = [NSString stringWithFormat:@"关注%d人 | 留言%d人",t1,t2];
    if(model.pushTime){
        _lblTime.text = [ZRBUtilities stringToData:@"yyyy.MM.dd" interval:model.pushTime];
    }else{
        _lblTime.text = @"";
    }
}
@end
