//
//  MineAllPublishViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineAllPublishViewController.h"
#import "MineAllPublishTableViewCell.h"
#import "MineService.h"
#import "FindService.h"
#import "MJRefresh.h"
#import "FindHomeModel.h"
#import "FindDetailViewController.h"
#import "MoneyDetailViewController.h"
#import "SearchProjectDetailViewController.h"
#import "PushViewController.h"
#import "PushModel.h"
#import "ZRB_NormalModel.h"

@interface MineAllPublishViewController ()<UITableViewDataSource,UITableViewDelegate,MineAllPublishTableViewCellDelegate>
{
    IBOutlet UITableView *_tableView;
    
    MineService         *_myService;
    
    FindService         *_findService;
    
    NSMutableArray      *_listArray;
    
    BOOL       _isRequst;            //是否请求过
}
@end

@implementation MineAllPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup
{
    _isRequst = NO;
    _myService = [[MineService alloc] init];
    _findService = [[FindService alloc] init];
    _listArray = [NSMutableArray new];
    [self addHeaderView];
    [self addFooterView];
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)requestDeleteCell:(MineAllPublishTableViewCell*)cell{
    [_findService deleteProjectOrSumWithId:cell.currentModel.uid success:^(id responseObject) {
        if(responseObject){
            PushModel *model = (PushModel*)responseObject;
            if(model.res == 1){
                [_listArray removeObjectAtIndex:cell.index.row];
                [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:cell.index, nil] withRowAnimation:UITableViewRowAnimationLeft];
                return ;
            }
        }
        [self showTipViewWithMsg:@"删除意向失败，请稍后再试！"];
    } failure:^(NSError *error) {
        [self showTipViewWithMsg:@"删除意向失败，请稍后再试！"];
    }];
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}

/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}


#pragma mark -
#pragma mark -网络请求



/**
 *  请求我关注的项目列表
 */
- (void)queryListData
{
    _isRequst = NO;
    
    PublishRequest *request = [[PublishRequest alloc]init];
    request.limit = 20;
    
    [self startWithReq:request];
    
}


/**
 *  加载更多
 */
- (void)loadMoreData
{
    if(_listArray.count > 0){
        
        FindHomeItemModel *lastModel = (FindHomeItemModel*)[_listArray lastObject];
        PublishRequest *request = [[PublishRequest alloc]init];
        request.limit = 20;
        request.maxId = [lastModel.uid longLongValue];
        
        [self loadMoreWithReq:request];
    }else{
        [_tableView.header endRefreshing];
        
        [_tableView.footer endRefreshing];
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
}


/**
 *  下拉加载
 *
 *  @param req 请求体
 */
- (void)startWithReq:(PublishRequest *)req
{
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_myService minePublishQueryWithRequest:req success:^(id responseObject) {
        [bself publishQueryListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
    }];
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(PublishRequest   *)req
{
    
    WS(bself);
    
    [_myService minePublishQueryWithRequest:req
                                    success:^(id responseObject) {
        [SVProgressHUD dismiss];
        [bself loadMoreListCallBackWithObject:responseObject];
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
    [SVProgressHUD show];
}


- (void)publishQueryListCallBackWithObject:(NSMutableArray *)modelArray
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    if(modelArray){
        _listArray = modelArray;
        [_tableView reloadData];
    }
    
    [_tableView.header endRefreshing];
    
}


- (void)loadMoreListCallBackWithObject:(NSMutableArray *)modelArray
{
    [_tableView.header endRefreshing];
    
    [_tableView.footer endRefreshing];
    if(modelArray){
        if(modelArray.count >0){
            [_listArray addObjectsFromArray:modelArray];
            [_tableView reloadData];
        }else{
             [_tableView.footer setState:MJRefreshStateNoMoreData];
        }
    }else{
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
    
}

#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineAllPublishTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineAllPublishTableViewCellIdentifier forIndexPath:indexPath];
    FindHomeItemModel *model = _listArray[indexPath.row];
    cell.delegate = self;
    cell.index = indexPath;
    [cell initWithModel:model];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //发布详情
    FindHomeItemModel *model = [_listArray objectAtIndex:indexPath.row];
    if(model.type == 101 && model.projectId != nil){
        //项目详情
        SearchProjectDetailViewController *detailCtrl = [[SearchProjectDetailViewController alloc]init];
        detailCtrl.projectId = model.projectId;
        detailCtrl.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:detailCtrl animated:YES];
    }else if (model.type == 102 && model.projectId != nil){
        //资金详情
        MoneyDetailViewController *detailCtrl = [[MoneyDetailViewController alloc]init];
        detailCtrl.moneyId = model.projectId;
        detailCtrl.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:detailCtrl animated:YES];
    }else{
        //发现详情
        FindDetailViewController *detailCtrl = [[FindDetailViewController alloc]init];
        detailCtrl.hidesBottomBarWhenPushed = YES;
        detailCtrl.model = model;
        
        [self.navigationController pushViewController:detailCtrl animated:YES];
    }
    
}

#pragma mark - 请求意向详情
#pragma mark 网络请求
- (void)requestFindDetail:(NSString*)fid{
    //封装参数
    FindDetailRequest *request = [[FindDetailRequest alloc]init];
    request.intentId = fid;
    
    [SVProgressHUD show];
    
    [_findService intentDetailDataWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FindHomeModel *model = (FindHomeModel *)responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.model) {
                    FindHomeItemModel *imodel = (FindHomeItemModel*)model.model;
                    //跳转编辑
                    PushViewController *editCtrl = [[PushViewController alloc]init];
                    editCtrl.isEdit = YES;
                    editCtrl.dataModel = imodel;
                    
                    if([imodel.typeName isEqualToString:@"资金方"]){
                        
                        editCtrl.navTitle = @"编辑资金";
                        editCtrl.intentType = 1;
                        
                    }else if([imodel.typeName isEqualToString:@"项目方"]){
                        
                        editCtrl.navTitle = @"编辑项目";
                        editCtrl.intentType = 0;
                    }
                    
                    [self.navigationController pushViewController:editCtrl animated:YES];
                }else{
                    [self showTipViewWithMsg:@"无法操作，信息已失效，请刷新再试"];
                }
                break;
            case ZRBHttpFailType:
                if(model.msg)
                    [self showTipViewWithMsg:model.msg];
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"请求意向信息失败，请稍后再试"];
    }];
}


#pragma mark - MineAllPublishTableViewCellDelegate
- (void)editCell:(MineAllPublishTableViewCell*)cell{
    if(!cell.currentModel)
        return;
    
    //请求意向详情
    [self requestFindDetail:cell.currentModel.uid];

}

- (void)deleteCell:(MineAllPublishTableViewCell*)cell{
    //删除意向
    [self requestDeleteCell:cell];
}

- (void)moveCell:(MineAllPublishTableViewCell*)cell isDelete:(BOOL)isDelete{

}

@end
