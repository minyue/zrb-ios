//
//  MineMoneyListTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  我的资金列表Cell

#import "MineMoneyListModel.h"

static NSString *const mineMoneyListTableViewCellIdentifier = @"MineMoneyListTableViewCellIdentifier";
@interface MineMoneyListTableViewCell : UITableViewCell

-(void)initWithModel:(MineMoneyListModel*)model;
@end
