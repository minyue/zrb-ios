//
//  MineMoneyListTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineMoneyListTableViewCell.h"
@interface MineMoneyListTableViewCell(){
    IBOutlet    UILabel *_lblTitle;
    IBOutlet    UILabel *_lblLeft;
    IBOutlet    UILabel *_lblTime;
}
@end
@implementation MineMoneyListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initWithModel:(MineMoneyListModel*)model{
    if(model.title){
        _lblTitle.text = model.title;
    }else{
        _lblTitle.text = @"";
    }
    int t1 = model.collectNum ? model.collectNum:0;
    int t2 = model.queryNum ? model.queryNum:0;
    _lblLeft.text = [NSString stringWithFormat:@"关注%d人 | 咨询%d人",t1,t2];
    if(model.pushTime){
        _lblTime.text = [ZRBUtilities stringToData:@"yyyy.MM.dd" interval:model.pushTime];
    }else{
        _lblTime.text = @"";
    }

}
@end
