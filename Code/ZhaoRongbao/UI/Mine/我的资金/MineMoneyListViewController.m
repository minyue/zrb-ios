//
//  MineMoneyListViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MineMoneyListViewController.h"
#import "MineMoneyListTableViewCell.h"
#import "MineService.h"
#import "MJRefresh.h"
#import "MineMoneyListModel.h"
#import "MoneyDetailViewController.h"

@interface MineMoneyListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *_tableView;
    
    MineService         *_myService;
    
    NSMutableArray      *_listArray;
    
    BOOL       _isRequst;            //是否请求过
}

@end

@implementation MineMoneyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)setup
{
    _isRequst = NO;
    _myService = [[MineService alloc] init];
    _listArray = [[NSMutableArray alloc] init];
    [self addHeaderView];
    [self addFooterView];
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];
        
    }
}


#pragma mark -
#pragma mark -网络请求



/**
 *  请求我关注的项目列表
 */
- (void)queryListData
{
    _isRequst = NO;
    
    MineMoneyListRequest *request = [[MineMoneyListRequest alloc]init];
    request.limit = 20;
    request.pushTime = @"";
    [self startWithReq:request];
    
}


/**
 *  加载更多
 */
- (void)loadMoreData
{
    if(_listArray.count > 0){
        
        MineMoneyListModel *lastModel = [_listArray lastObject];
        LogInfo([NSString stringWithFormat:@"%@",lastModel.pushTime]);
        MineMoneyListRequest *request = [[MineMoneyListRequest alloc]init];
        request.limit = 20;
//        request.pushTime = [ZRBUtilities stringToData:@"yyyy-MM-dd HH:mm:ss" interval:lastModel.pushTime];
        request.pushTime = lastModel.pushTime;
        LogInfo(request.pushTime);
        
        [self loadMoreWithReq:request];
    }else{
        [_tableView.header endRefreshing];
        
        [_tableView.footer endRefreshing];
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
}


/**
 *  下拉加载
 *
 *  @param req 请求体
 */
- (void)startWithReq:(MineMoneyListRequest *)req
{
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_myService mineMoneyQueryListWithRequest:req success:^(id responseObject) {
        [bself moneyQueryListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
        
    }];
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(MineMoneyListRequest   *)req
{
    
    WS(bself);
    
    [_myService mineMoneyQueryListWithRequest:req success:^(id responseObject) {
        
                                        [SVProgressHUD dismiss];
        
                                        [bself loadMoreListCallBackWithObject:responseObject];
        
                                        
                                    } failure:^(NSError *error) {
                                        [SVProgressHUD dismiss];
                                        [HUDManager showNonNetWorkHUDInView:self.view event:^{
                                            [bself queryListData];
                                        }];
                                        
                                    }];
    [SVProgressHUD show];
}


- (void)moneyQueryListCallBackWithObject:(NSMutableArray *)modelArray
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    if(modelArray){
        _listArray = modelArray;
        [_tableView reloadData];
    }
    
    [_tableView.header endRefreshing];
    
}


- (void)loadMoreListCallBackWithObject:(NSMutableArray *)modelArray
{
    [_tableView.header endRefreshing];
    
    [_tableView.footer endRefreshing];
    if(modelArray){
        if(modelArray.count >0)
            [_listArray addObjectsFromArray:modelArray];
        [_tableView reloadData];
    }else{
        [_tableView.footer setState:MJRefreshStateNoMoreData];
    }
    
}

#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineMoneyListTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:mineMoneyListTableViewCellIdentifier forIndexPath:indexPath];
    MineMoneyListModel *model = _listArray[indexPath.row];
    [cell initWithModel:model];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MineMoneyListModel *model = _listArray[indexPath.row];
    
    MoneyDetailViewController *detailCtrl = [[MoneyDetailViewController alloc]init];
    detailCtrl.moneyId = model.mid;
    detailCtrl.moneyTitle = model.title;
    detailCtrl.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:detailCtrl animated:YES];
}

@end
