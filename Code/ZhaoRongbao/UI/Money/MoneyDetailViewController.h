//
//  MoneyDetailViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"
#import "MoneyService.h"

#import "UMSocialShakeService.h"
#import "ShareMenuView.h"
#import "ShareManager.h"

@interface MoneyDetailViewController : ZRB_ViewControllerWithBackButton<UIWebViewDelegate, NJKWebViewProgressDelegate>
{
    UIWebView  *_webView;
    UIView *_rightBarView;
    UIButton  *_focusBtn;
    
    NJKWebViewProgressView *_progressView;
    NJKWebViewProgress *_progressProxy;
    
    MoneyService *_moneyService;
    
    ShareMenuView *_shareMenu;
}
@property (nonatomic,copy) ShareMenuView *shareMenu;
@property (nonatomic,strong) NSString *moneyId;
@property (nonatomic,strong) NSString *moneyTitle;

@property (nonatomic,copy) NSMutableString *shareContent;
@property (nonatomic,copy) NSString *shareUrl;

@end
