//
//  MoneyDetailViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MoneyDetailViewController.h"
#import "ZRB_NormalModel.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"
#import "MineLoginViewcController.h"
#import "FcousModel.h"
#import "IntentDetailMenu.h"

@interface MoneyDetailViewController ()

@end

@implementation MoneyDetailViewController

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setupNavBarViews];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    [self loadDetailWithId:self.moneyId];
}



- (void)setup{
    
//    self.title = @"资金详情";
    
    _moneyService = [[MoneyService alloc]init];
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -64)];
    _webView.autoresizesSubviews = YES;
    _webView.scalesPageToFit = YES;
    _webView.delegate = _progressProxy;
    _webView.detectsPhoneNumbers = NO;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_webView];
    
    IntentDetailMenu *bottomMenu = [[IntentDetailMenu alloc]init];
    [bottomMenu.personalInfo addTarget:self action:@selector(personalInfoCard:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bottomMenu];
    
    [bottomMenu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.equalTo(@49.5);
    }];

    
    [self initShareMenu];
}

//查看个人名片
- (void)personalInfoCard:(id)sender{
    
    LogInfo(@"跳转个人名片");
}

/**
 *  构造导航栏菜单
 */
- (void)setupNavBarViews
{
    _rightBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 2, 115, 44)];
    _rightBarView.backgroundColor = [UIColor clearColor];
    
    //分享
    UIButton  *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBtn setImage:[UIImage imageNamed:@"nav_share"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareMoney:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:shareBtn];
    [shareBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_rightBarView.mas_left).offset(5);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    UILabel  *shareLabel = [[UILabel alloc] init];
    [shareLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [shareLabel setBackgroundColor:[UIColor clearColor]];
    shareLabel.text = @"分享";;
    shareLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:shareLabel];
    
    [shareLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(shareBtn.mas_centerX);
        make.top.equalTo(shareBtn.mas_bottom).offset(2);
    }];
    
    
    //关注
    _focusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_focusBtn setImage:[UIImage imageNamed:@"nav_like_normal"] forState:UIControlStateNormal];
    [_focusBtn setImage:[UIImage imageNamed:@"nav_like_hl"] forState:UIControlStateSelected];
    [_focusBtn addTarget:self action:@selector(focusMoney:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:_focusBtn];
    [_focusBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shareBtn.mas_right).offset(20);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    [self retuqestIfAttention:_focusBtn];
    
    UILabel  *focusLabel = [[UILabel alloc] init];
    [focusLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [focusLabel setBackgroundColor:[UIColor clearColor]];
    focusLabel.text = @"关注";
    focusLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:focusLabel];
    
    [focusLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_focusBtn.mas_centerX);
        make.top.equalTo(_focusBtn.mas_bottom).offset(2);
    }];
    
    
    //评论
    UIButton  *commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentBtn setImage:[UIImage imageNamed:@"nav_call"] forState:UIControlStateNormal];
    [commentBtn addTarget:self action:@selector(callService:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:commentBtn];
    [commentBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_focusBtn.mas_right).offset(20);
        make.right.equalTo(_rightBarView.mas_right).offset(-5);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    
    UILabel  *commentLabel = [[UILabel alloc] init];
    [commentLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [commentLabel setBackgroundColor:[UIColor clearColor]];
    commentLabel.text = @"咨询";;
    commentLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:commentLabel];
    
    [commentLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(commentBtn.mas_centerX);
        make.top.equalTo(commentBtn.mas_bottom).offset(2);
    }];
    
    CustomBarItem *item =  [self.navigationItem setItemWithCustomView:_rightBarView itemType:right];
    
    [item setOffset:-5];
}

//关注资金
- (void)focusMoney:(id)sender{
    
    if([ZRB_UserManager isLogin]){
        
        if (_focusBtn.isSelected) {
            [self cancelFcousMoney:self.moneyId];
        }else{
            [self fcousMoney:self.moneyId];
        }
        
    }else{
        
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.isFindHomeRefresh = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
}

//分享资金
- (void)shareMoney:(id)sender{
 
    _shareUrl = [NSString stringWithFormat:@"%@/fund/detail?id=%@",SERVER_URL,self.moneyId];
//    NSString *str = @"汉海的招融宝APP新鲜出炉啦！";
   _shareContent = [[NSMutableString alloc]initWithString:@"向您推荐 "];
    if(_moneyTitle)
        [_shareContent appendString:_moneyTitle];
    
    LogInfo(_shareUrl);
    
    [_shareMenu show];
    
}

- (void)callService:(id)sender{
    
    LogInfo(@"客服电话");
    NSString *str = [NSString stringWithFormat:@"tel:%@",SERVICE_TEL];
    UIWebView *webView = [[UIWebView alloc]init];
    [webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:str]]];
    [self.view addSubview:webView];
    
}

/**
 * 初始化分享菜单
 */
- (void)initShareMenu{
    
    _shareMenu = [[ShareMenuView alloc]init];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_shareMenu];
    
    WS(bself);
    _shareMenu.shareButtonClickBlock = ^(NSInteger index){
        LogInfo(@"%ld",index);
        
        switch (index) {
            case ShareToQQ:
                [[ShareManager ShareManager] QQShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToQQZone:
                [[ShareManager ShareManager] QQZoneShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToWechat:
                [[ShareManager ShareManager] WXChatShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToWechatFriend:
                [[ShareManager ShareManager] WXFriendShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToCopy:
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = bself.shareUrl;
               
                [bself.shareMenu hide];
                break;
            }
            default:
                break;
        }
    };
}


/**
 *  加载详情页面
 *
 *  @param intentId 意向ID
 */
- (void)loadDetailWithId:(NSString *)intentId
{
    NSString  *url = [NSString stringWithFormat:@"%@/fund/detail?id=%@",SERVER_URL,intentId];
    LogInfo(url);
    NSURL *webUrl = [NSURL URLWithString:url];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:webUrl];
    [_webView loadRequest:req];
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
//    self.title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    
}

#pragma mark 网络请求
/**
 *  判断是否关注
 */
- (void)retuqestIfAttention:(UIButton*)btn{
    
    btn.selected = NO;
    [_moneyService checkIfAttentionNewOrProjectWithRequest:self.moneyId andType:FcousMoney success:^(id responseObject) {
        
        LogInfo(@"");
        if(responseObject){
            ZRB_NormalModel *model = responseObject;
            if(model.res == 1){
                btn.selected = [[model.data valueForKey:@"isCollected"] boolValue];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

/**
 *  意向关注
 */
- (void)fcousMoney:(NSString *) moneyId{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousMoney;
    request.fcousObjectId =  moneyId;
    
    [SVProgressHUD show];
    
    AFBaseService *baseService = [[AFBaseService alloc]init];
    [baseService fcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"关注成功" onView:self.view];
                    
                    _focusBtn.selected = YES;
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"已关注"];
                }
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"关注失败"];
                 _focusBtn.selected = NO;
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
        _focusBtn.selected = NO;
    }];
}

/**
 *  取消关注
 */
- (void)cancelFcousMoney:(NSString *) moneyId{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousMoney;
    request.fcousObjectId =  moneyId;
    
    [SVProgressHUD show];
    
    AFBaseService *baseService = [[AFBaseService alloc]init];
    [baseService cancelFcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"已取消关注" onView:self.view];
                    _focusBtn.selected = NO;
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"取消关注失败"];
                }
                
                break;
            case ZRBHttpFailType:
            {
                [self showTipViewWithMsg:@"取消关注失败"];
                _focusBtn.selected = NO;
            }
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
        _focusBtn.selected = NO;
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
