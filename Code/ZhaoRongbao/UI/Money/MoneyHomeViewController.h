//
//  MoneyHomeViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "MJRefresh.h"
#import "MoneyService.h"
#import "BlankRemindView.h"

@interface MoneyHomeViewController : ZRB_ViewController<UITableViewDelegate,UITableViewDataSource>
{
    UILabel *_areaLB;
    UITableView *_mTableView;
    MoneyService *_moneyService;
    
    BOOL  _isRequst;            //是否请求过
    NSMutableArray *_moneyItemArray;
}

@property (nonatomic,strong) NSString *filterIndustryStr;

@property (nonatomic,strong) NSString *filterAreaStr;

@property (nonatomic,copy) BlankRemindView *blankRemindView;

@end
