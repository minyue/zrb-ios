//
//  MoneyHomeViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MoneyHomeViewController.h"
#import "MoneyDetailViewController.h"
#import "MoneyHomeCell12.h"
#import "MoneyHomeModel.h"
#import "UINavigationItem+CustomItem.h"
#import "CustomBarItem.h"
#import "AreaSelectViewController.h"
#import "FilterViewController.h"
#import "AreaModel.h"
#import "FindIndustryModel.h"

#define CellIndentifier      @"MoneyHomeCell"

@interface MoneyHomeViewController ()

@end

@implementation MoneyHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    
    [self refreshData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterMoneyWithIndustry:) name:MONEY_FILTER_WITH_INDUSTRY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterMoneyWithArea:) name:MONEY_FILTER_WITH_AREA object:nil];
}


- (void)setup{
    
     self.title = @"找资金";
    
    _isRequst = NO;
    _filterIndustryStr = @"97"; //默认全国id = 97
    _filterAreaStr = @"";
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"筛选" style:UIBarButtonItemStylePlain target:self action:@selector(filterWithIndustry:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIButton *leftView = [[UIButton alloc]init];
    leftView.backgroundColor = [UIColor clearColor];
    leftView.frame = CGRectMake(0, 0, 100, 44);
    [leftView addTarget:self action:@selector(filterWithArea:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *icon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_project_location"]];
    [leftView addSubview:icon];
    
    _areaLB = [[UILabel alloc]init];
    _areaLB.backgroundColor = [UIColor clearColor];
    _areaLB.text = @"全国";
    _areaLB.textAlignment = NSTextAlignmentLeft;
    _areaLB.textColor = [UIColor whiteColor];
    _areaLB.font = [UIFont systemFontOfSize:ZRB_BACK_ITEM_SIZE];
    [leftView addSubview:_areaLB];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(@20);
        make.left.equalTo(leftView.mas_left);
        make.centerY.equalTo(leftView.mas_centerY);
    }];
    
    [_areaLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right);
        make.centerY.equalTo(leftView.mas_centerY);
        make.right.equalTo(leftView.mas_right);
    }];
    
    CustomBarItem *item =  [self.navigationItem setItemWithCustomView:leftView itemType:left];
    [item setOffset:-10];

    
    _moneyService = [[MoneyService alloc]init];
    _moneyItemArray = [[NSMutableArray alloc]init];
    
    _blankRemindView = [[BlankRemindView alloc]init];

    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64- 49) style:UITableViewStylePlain];
//    [_mTableView registerClass:[MoneyHomeCell class] forCellReuseIdentifier:CellIndentifier];
    _mTableView.backgroundColor = [UIColor clearColor];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    _mTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
    
    [self addFooterView];
    [self addHeaderView];
}

- (void)filterWithIndustry:(id)sender{
    
    FilterViewController *filterCtrl = [[FilterViewController alloc]init];
    filterCtrl.selectedIndustryId = _filterIndustryStr;
    filterCtrl.filterType = MoneyFilter;
    
    [self presentViewController:filterCtrl animated:YES completion:nil];
}

- (void)filterWithArea:(id)sender{
    
    AreaSelectViewController *eVC = (AreaSelectViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Projects" class:[AreaSelectViewController class]];
    eVC.filterType = MoneyFilter;
    eVC.selectedAreaPostcode = _filterAreaStr;
    
    [self presentViewController:eVC animated:YES completion:nil];
}

- (void)filterMoneyWithIndustry:(id)sender{
    
    NSNotification *notify = (NSNotification *)sender;
    FindIndustryItemModel *model = notify.object;
    
    _filterIndustryStr = [NSString stringWithFormat:@"%@",model.uid];
    LogInfo(_filterIndustryStr);
    
    [_mTableView.header beginRefreshing];

}

- (void)filterMoneyWithArea:(id)sender{
    
    LogInfo(@"根据地区筛选项目");
    NSNotification *notify = (NSNotification *)sender;
    
    AreaModel *model = (AreaModel *)notify.object;
    
    _filterAreaStr = [NSString stringWithFormat:@"%@",model.postcode];
    if (model.cityName.length > 2) {
        _areaLB.text = [model.cityName substringToIndex:model.cityName.length - 1];
    }else{
        _areaLB.text = model.cityName;
    }
    
    [_mTableView.header beginRefreshing];
}

/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        [_mTableView.header beginRefreshing];
    }
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _mTableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
    
}


/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _mTableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}


#pragma mark delegate of UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _moneyItemArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    MoneyHomeItemModel *itemModel = [_moneyItemArray objectAtIndex:indexPath.row];
//    
//    
//    if([itemModel.authName isEqualToString:@"资料认证"]){
//        return 210.0f;
//    }else{
//        return 220.0f + 10;
//    }
//    

    
     return 101.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    MoneyHomeCell12 *cell = [MoneyHomeCell12 cellForTabvleView:tableView];
    cell.model = [_moneyItemArray objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MoneyHomeItemModel *itemModel = [_moneyItemArray objectAtIndex:indexPath.row];
    
    MoneyDetailViewController *detailCtrl = [[MoneyDetailViewController alloc]init];
    detailCtrl.moneyId = itemModel.uid;
    detailCtrl.moneyTitle = itemModel.title;
    detailCtrl.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:detailCtrl animated:YES];
}

#pragma mark 网络请求

/**
 *  请求发现列表
 */
-(void)queryListData{
    
    _isRequst = NO;
    
    MoneyHomeListRequest *request = [[MoneyHomeListRequest alloc]init];
    request.pushTime = @"";
    request.limit = @"10";
    request.citys = _filterAreaStr;
    request.industrys = _filterIndustryStr;
    
    WS(bself);
    [_moneyService moneyHomeListWithRequest:request success:^(id responseObject) {
        
        MoneyHomeModel *model = responseObject;
        [bself moneyQueryListCallBackWithObject:model];
        
    } failure:^(NSError *error) {
        LogInfo(@"error:%@",error);
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData];
        }];
    }];
    
}

/**
 *  加载更多
 */
-(void)loadMoreData{
    
    MoneyHomeItemModel *lastModel = [_moneyItemArray lastObject];
    
    MoneyHomeListRequest *request = [[MoneyHomeListRequest alloc]init];
//    request.pushTime = [ZRBUtilities stringToData:@"yyyy-MM-dd HH:mm:ss" interval:lastModel.pushTime];
    request.pushTime = lastModel.pushTime;
    request.limit = @"10";
    request.citys = _filterAreaStr;
    request.industrys = _filterIndustryStr;
    
    WS(bself);
    [_moneyService moneyHomeListWithRequest:request success:^(id responseObject) {
        
        MoneyHomeModel *model = responseObject;
        [bself loadMoreListCallBackWithObject:model];
        
    } failure:^(NSError *error) {
        
    }];
}


/**
 * 下拉刷新请求回调
 */
- (void)moneyQueryListCallBackWithObject:(MoneyHomeModel *)model
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            _moneyItemArray = model.data;
            if (model.data.count < 10 && model.data > 0) {
                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                _mTableView.footer.hidden = YES;
                
            }else{
                _mTableView.footer.hidden = NO;
            }
            
            //数据为空提示
            if(model.data.count == 0){
                
                [_blankRemindView showInView:self.navigationController.view];
                self.view.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
                
            }else{
                if (_blankRemindView) {
                    [_blankRemindView removeFromSuperview];
                    self.view.backgroundColor = [UIColor colorWithHexString:ZRB_BACKGROUNDCORLOR];
                }
            }

            
            [_mTableView reloadData];
            [_mTableView.header endRefreshing];
            break;
        }
        case ZRBHttpFailType:
        {
            LogInfo(@"请求失败");
            [_mTableView.header endRefreshing];
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
        default:
            break;
    }
    
    [_mTableView.header endRefreshing];
    
}

/**
 *  加载更多请求回调
 */
- (void)loadMoreListCallBackWithObject:(MoneyHomeModel *)model
{
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            [_moneyItemArray addObjectsFromArray:model.data];
            
            [_mTableView reloadData];
            
            if (model.data.count < 10) {
                LogInfo(@"已经加载完毕");
                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                _mTableView.footer.hidden = YES;
            }else{
                _mTableView.footer.hidden = NO;
            }
            
            [_mTableView.footer endRefreshing];
            
            break;
        }
        case ZRBHttpFailType:
        {
            [_mTableView.header endRefreshing];
            
            break;
        }
        case ZRBHttpNoLoginType:
        {
            [_mTableView.header endRefreshing];
            
            break;
        }
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
