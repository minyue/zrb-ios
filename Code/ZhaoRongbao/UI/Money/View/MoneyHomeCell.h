//
//  MoneyHomeCell.h
//  ZhaoRongbao
//
//  根据App 1.1产品需求 改造
//  Created by songmk on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoneyHomeTagView.h"
#import "MoneyHomeModel.h"

@interface MoneyHomeCell : UITableViewCell


//- (void)setup:(MoneyHomeItemModel *)model;
//- (void)initWithModel:(MoneyHomeItemModel *)model;

@property (nonatomic,copy) UIView *superView;               //父视图（卡片样式）

@property (nonatomic,copy) UILabel *titleLB;                //标题

@property (nonatomic,copy) UIImageView *approveTypeIcon;    //认证类型图标
@property (nonatomic,copy) UILabel *approveTypeLB;          //认证类型名称

@property (nonatomic,copy) UIView *line;                    //分割线(位于内容之上)
@property (nonatomic,copy) UIView *line1;                   //分割线(位于内容之下)

@property (nonatomic,copy) UILabel *industryLB;             //投资行业
@property (nonatomic,copy) UILabel *areaLB;                 //投资区域
@property (nonatomic,copy) UILabel *investTypeLB;           //投资方式

@property (nonatomic,copy) UIButton * circle0;              //投资行业 圆点0
@property (nonatomic,copy) UIButton * circle1;              //投资区域 圆点1
@property (nonatomic,copy) UIButton * circle2;              //投资方式 圆点2


@property (nonatomic,copy) UIView *bottomLine;              //底部竖向分割线0
@property (nonatomic,copy) UIView *bottomLine1;             //底部竖向分割线1


@property (nonatomic,copy) MoneyHomeTagView *investSumView;     //投资金额
@property (nonatomic,copy) MoneyHomeTagView *hasPushTimeView;   //发布时间
@property (nonatomic,copy) MoneyHomeTagView *fcousCountView;    //关注人数

@property (nonatomic,retain) MoneyHomeItemModel *model;         //找资金数据模型


+ (instancetype)cellForTableView:(UITableView *)tableView;

@end
