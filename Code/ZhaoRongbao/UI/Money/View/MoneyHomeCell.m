//
//  MoneyHomeCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/22.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MoneyHomeCell.h"



#define CONTENT_TEXT_SIZE       14.0f

@implementation MoneyHomeCell


+ (instancetype)cellForTableView:(UITableView *)tableView{
    
    static NSString *CellId = @"MoneyHomeCell";
    MoneyHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    if (!cell) {
        
        cell = [[MoneyHomeCell alloc]init];

    }
    
    return cell;
    
}

- (instancetype)init{
    
    self = [super init];
    if (self) {
        [self initSubView];
    }
    
    return self;
}

/**
 *   初始化子视图
 */
- (void)initSubView{
    
    
    self.backgroundColor= [UIColor clearColor];
    
    //父视图
    _superView = [[UIView alloc]init];
    _superView.backgroundColor = [UIColor whiteColor];
    _superView.layer.masksToBounds = YES;
    _superView.layer.cornerRadius = 10.0f;
    
    
    [self.contentView addSubview:_superView];
    
    //标题
    _titleLB = [[UILabel alloc]init];
    _titleLB.backgroundColor = [UIColor clearColor];
    _titleLB.font = [UIFont boldSystemFontOfSize:15.0f];
    _titleLB.textColor = [UIColor colorWithHexString:@"656565"];
    [_superView addSubview:_titleLB];
    
    //认证图标
    _approveTypeIcon = [[UIImageView alloc]init];
    _approveTypeIcon.backgroundColor = [UIColor clearColor];
    [_superView addSubview:_approveTypeIcon];
        
    //认证类型名称
    _approveTypeLB = [[UILabel alloc]init];
    _approveTypeLB.backgroundColor = [UIColor clearColor];
    _approveTypeLB.font = [UIFont systemFontOfSize:14.0f];
    _approveTypeLB.textColor = [UIColor colorWithHexString:@"1b9bfb"];
    [_superView addSubview:_approveTypeLB];
    
    //分割线(位于内容之上)
    _line = [[UIView alloc]init];
    _line.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [_superView addSubview:_line];
    
     /*----------------------------- 内容 -----------------------------*/
    
    //圆点0
    _circle0 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _circle0.backgroundColor = [UIColor colorWithHexString:@"c8c8c8"];
    _circle0.layer.cornerRadius = 4;
    _circle0.layer.masksToBounds = YES;
    _circle0.layer.borderWidth = 1.0;
    _circle0.layer.borderColor = [UIColor colorWithHexString:@"c8c8c8"].CGColor;
    [_superView addSubview:_circle0];
    //投资行业
    _industryLB = [[UILabel alloc]init];
    _industryLB.backgroundColor = [UIColor clearColor];
    _industryLB.numberOfLines = 1;
    _industryLB.lineBreakMode = UILineBreakModeTailTruncation;
    _industryLB.font = [UIFont systemFontOfSize:CONTENT_TEXT_SIZE];
    [_superView addSubview:_industryLB];
    
    //圆点1
    _circle1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _circle1.backgroundColor = [UIColor colorWithHexString:@"c8c8c8"];
    _circle1.layer.cornerRadius = 4;
    _circle1.layer.masksToBounds = YES;
    _circle1.layer.borderWidth = 1.0;
    _circle1.layer.borderColor = [UIColor colorWithHexString:@"c8c8c8"].CGColor;
    [_superView addSubview:_circle1];
    
    //投资区域
    _areaLB = [[UILabel alloc]init];
    _areaLB.backgroundColor = [UIColor clearColor];
    _areaLB.numberOfLines = 1;
    _areaLB.lineBreakMode = UILineBreakModeTailTruncation;
    _areaLB.font = [UIFont systemFontOfSize:CONTENT_TEXT_SIZE];
    [_superView addSubview:_areaLB];
    
    //圆点3
    _circle2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _circle2.backgroundColor = [UIColor colorWithHexString:@"c8c8c8"];
    _circle2.layer.cornerRadius = 4;
    _circle2.layer.masksToBounds = YES;
    _circle2.layer.borderWidth = 1.0;
    _circle2.layer.borderColor = [UIColor colorWithHexString:@"c8c8c8"].CGColor;
    [_superView addSubview:_circle2];
    
    //投资方式
    _investTypeLB = [[UILabel alloc]init];
    _investTypeLB.backgroundColor = [UIColor clearColor];
    _investTypeLB.numberOfLines = 1;
    _investTypeLB.lineBreakMode = UILineBreakModeTailTruncation;
    _investTypeLB.font = [UIFont systemFontOfSize:CONTENT_TEXT_SIZE];
    [_superView addSubview:_investTypeLB];
    
    /** ----------------------------- 内容 ----------------------------- **/
    
    //分割线（位于内容之下）
    _line1 = [[UIView alloc]init];
    _line1.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [_superView addSubview:_line1];
    
    //投资金额
     _investSumView = [[MoneyHomeTagView alloc]init];
    [_superView addSubview:_investSumView];
    
    //底部竖向分割线0
    _bottomLine = [[UIView alloc]init];
    _bottomLine.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [_superView addSubview:_bottomLine];
    
    //发布时间
    _hasPushTimeView = [[MoneyHomeTagView alloc]init];
    [_superView addSubview:_hasPushTimeView];
    
    //底部竖向分割线1
    _bottomLine1 = [[UIView alloc]init];
    _bottomLine1.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [_superView addSubview:_bottomLine1];
    
    //关注人数
    _fcousCountView = [[MoneyHomeTagView alloc]init];
    [_superView addSubview:_fcousCountView];
    
}


/**
 *   根据数据模型动态约束视图
 */
- (void)setSubViewLayoutWithModel:(MoneyHomeItemModel *)model{
    
    [_superView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(5);
        make.left.equalTo(self.contentView.mas_left).offset(5);
        make.right.equalTo(self.contentView.mas_right).offset(-5);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-5);
    }];
    
    [_titleLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_superView.mas_left).offset(10);
        make.right.equalTo(_superView.mas_right).offset(-10);
        make.top.equalTo(_superView.mas_top).offset(10);
        make.height.equalTo(@30);
    }];
    
    if([model.authName isEqualToString:@"实地认证"]){
        [_approveTypeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_superView.mas_left).offset(10);
            make.top.equalTo(_titleLB.mas_bottom).offset(5);
            make.width.height.equalTo(@13);
        }];
        
        [_approveTypeLB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_approveTypeIcon.mas_right).offset(10);
            make.centerY.equalTo(_approveTypeIcon.mas_centerY);
            make.height.equalTo(@20);
        }];
        
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_superView);
            make.height.equalTo(@1);
            make.top.equalTo(_approveTypeLB.mas_bottom).offset(10);
        }];
    }else{
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_superView);
            make.height.equalTo(@1);
            make.top.equalTo(_titleLB.mas_bottom).offset(10);
        }];
        
    }
 
    [_circle0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@8);
        make.height.equalTo(@8);
        make.top.equalTo(_line.mas_bottom).offset(15);
        make.left.equalTo(_superView.mas_left).offset(10);
    }];
    
    [_industryLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_circle0.mas_right).offset(10);
        make.right.equalTo(_superView.mas_right).offset(-10);
        make.centerY.equalTo(_circle0.mas_centerY);
    }];
    
    [_circle1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@8);
        make.height.equalTo(@8);
        make.top.equalTo(_circle0.mas_bottom).offset(20);
        make.left.equalTo(_superView.mas_left).offset(10);
    }];
    
    [_areaLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_circle1.mas_right).offset(10);
        make.right.equalTo(_superView.mas_right).offset(-10);
        make.centerY.equalTo(_circle1.mas_centerY);
    }];
    
    [_circle2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@8);
        make.height.equalTo(@8);
        make.top.equalTo(_circle1.mas_bottom).offset(20);
        make.left.equalTo(_superView.mas_left).offset(10);
    }];
    
    [_investTypeLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_circle2.mas_right).offset(10);
        make.right.equalTo(_superView.mas_right).offset(-10);
        make.centerY.equalTo(_circle2.mas_centerY);
    }];
    
    [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_superView);
        make.top.equalTo(_investTypeLB.mas_bottom).offset(10);
        make.height.equalTo(@1);
    }];

    
    [_investSumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_superView.mas_left);
        make.top.equalTo(_line1.mas_bottom);
        make.bottom.equalTo(_superView.mas_bottom);
        make.width.equalTo(@(SCREEN_WIDTH*1/3));
    }];
    
    [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line1.mas_bottom).offset(10);
        make.bottom.equalTo(_superView.mas_bottom).offset(-10);
        make.left.equalTo(_investSumView.mas_right);
        make.width.equalTo(@1);
    }];
    
    [_hasPushTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bottomLine.mas_right);
        make.top.equalTo(_line1.mas_bottom);
        make.bottom.equalTo(_superView.mas_bottom);
        make.width.equalTo(@(SCREEN_WIDTH*1/3));
    }];
    
    [_bottomLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line1.mas_bottom).offset(10);
        make.bottom.equalTo(_superView.mas_bottom).offset(-10);
        make.left.equalTo(_hasPushTimeView.mas_right);
        make.width.equalTo(@1);
    }];
    
    [_fcousCountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bottomLine1.mas_right);
        make.top.equalTo(_line1.mas_bottom);
        make.bottom.equalTo(_superView.mas_bottom);
        make.right.equalTo(_superView.mas_right);
    }];

}


- (void)setModel:(MoneyHomeItemModel *)model{
    
    if(model){

        [self setSubViewLayoutWithModel:model];
        
        _titleLB.text = model.title;
        //删除资料认证显示
        if([model.authName isEqualToString:@"资料认证"]){
            _approveTypeIcon.hidden = YES;
            _approveTypeLB.hidden = YES;
        }else{
            _approveTypeIcon.hidden = NO;
            _approveTypeIcon.hidden = NO;
            _approveTypeIcon.image = [UIImage imageNamed:@"shidi_verify"];
            _approveTypeLB.text = model.authName;
        }
        
        _approveTypeLB.text = model.authName;
        
        _industryLB.text = [NSString stringWithFormat:@"投资行业：%@",model.industryName];
        
        _areaLB.text =  [NSString stringWithFormat:@"投资行业：%@",model.areas];
        
        if([model.investMode isEqualToString:@"不限"]){
            _investTypeLB.text = [NSString stringWithFormat:@"投资方式：%@",model.investMode];
        }else{
            _investTypeLB.text = [NSString stringWithFormat:@"投资方式：%@投资",model.investMode];
        }
        
        
        [_investSumView initWith:[NSString stringWithFormat:@"%@万",model.total] typeName:@"投资金额"];
        
        [_hasPushTimeView initWith:[NSString stringWithFormat:@"%@天",model.period] typeName:@"发布时间"];
        
        [_fcousCountView initWith:model.collectNum typeName:@"关注人数"];

    }
}

@end
