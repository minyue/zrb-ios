//
//  MoneyHomeTagView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/23.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoneyHomeTagView : UIView

@property (nonatomic,strong) UILabel *value;
@property (nonatomic,strong) UILabel *type;

- (void)initWith:(NSString *)value typeName:(NSString *)type;

@end
