//
//  MoneyHomeTagView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/23.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "MoneyHomeTagView.h"

@implementation MoneyHomeTagView

- (instancetype)init{
    
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    self.backgroundColor = [UIColor clearColor];
    
    _value = [[UILabel alloc]init];
    _value.backgroundColor = [UIColor clearColor];
    _value.font = [UIFont systemFontOfSize:16.0f];
    _value.textColor = [UIColor colorWithHexString:@"6a6a6a"];
    [self addSubview:_value];
    
    _type = [[UILabel alloc]init];
    _type.backgroundColor = [UIColor clearColor];
    _type.font = [UIFont systemFontOfSize:14.0f];
    _type.textColor = [UIColor colorWithHexString:@"b6b6b6"];
    [self addSubview:_type];
    
    [_value mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(5);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    [_type mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(_value.mas_bottom);
    }];
    
}

- (void)initWith:(NSString *)value typeName:(NSString *)type{
    
    _value.text = value;
    _type.text = type;
    
}

@end
