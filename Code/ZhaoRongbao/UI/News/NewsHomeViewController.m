//
//  NewsHomeViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsHomeViewController.h"
#import "NewsHomeViewCell.h"
#import "NewsHomeHrizontalMenu.h"
#import "NewsService.h"
#import "UIViewController+Child.h"
#import "NewsListViewController.h"
#import "ZRBUtilities.h"
#import "StoryBoardUtilities.h"
#import "UINavigationItem+CustomItem.h"
#import "NewsMainSearchViewController.h"

@interface NewsHomeViewController()
{
    IBOutlet   UIScrollView  *_scrollView;
    
    IBOutlet   NewsHomeHrizontalMenu  *_hrizontalMenu;

    NewsService         *_newsService;
    
    NSInteger     _currentPage;
    
    NSMutableArray  *_mainViewsArray;

}



//构建主视图
- (void)setupMainViewWithData:(NSMutableArray *)array;


- (void)setupRightItem;

@end

@implementation NewsHomeViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
     self.title = @"招融宝";

    [self setup];
    [self setupRightItem];
    [self queryCategoryRequest];
}


#pragma mark -
#pragma mark -私有方法


- (void)setup
{
    _newsService = [[NewsService alloc] init];
    
    _mainViewsArray = [NSMutableArray new];
    
    _currentPage = 0;       //默认为0
    
    WS(bself);
    _hrizontalMenu.changeSelectItemBlock = ^(CategoryItem *model,NSInteger index){
        [bself selectItem:model index:index];
    };
}




/**
 *  设置搜索按钮
 */
- (void)setupRightItem
{
   CustomBarItem *item =  [self.navigationItem setItemWithImage:@"search_bar" size:CGSizeMake(30, 30) itemType:right];
   [item setOffset:-5];
    [item addTarget:self selector:@selector(showSearchViewController:) event:UIControlEventTouchUpInside];

}



//构建主视图
- (void)setupMainViewWithData:(NSMutableArray *)array
{
    if (array.count == 0) return;
    
    [self createNewArrayWithArray:array];
    for (int i = 0; i < array.count; i ++)
    {
        NewsListViewController *listVC = (NewsListViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"News" class:[NewsListViewController class]];
        [self addChildViewController:listVC];
        [_scrollView addSubview:listVC.view];
        
        [_mainViewsArray     addObject:listVC];
        
        listVC.item = array[i];
        UIView  *view = listVC.view;
        view.frameSize = _scrollView.frameSize;
        view.frameX = _scrollView.frameWidth * i;
        view.frameY = 0;
        _scrollView.contentSize = CGSizeMake(_scrollView.frameWidth * (i + 1), _scrollView.contentSize.height);
    }
    
}

/**
 *  构建"全部新闻选项"
 *
 *  @param array 原始选项数组
 */
- (void)createNewArrayWithArray:(NSMutableArray *)array
{
    CategoryItem *model = [[CategoryItem alloc] init];
    model.type = @"全部新闻";
    [array insertObject:model atIndex:0];
}

/**
 *  选中某个选项
 *
 *  @param model 模型
 *  @param index 下标
 */
- (void)selectItem:(CategoryItem *)model  index:(NSInteger)index
{
    [_scrollView setContentOffset:CGPointMake(_scrollView.frameWidth * index, _scrollView.contentOffset.y) animated:NO];
}


/**
 *  展示搜索视图
 */
- (void)showSearchViewController:(id)sender
{
    NewsMainSearchViewController *mainVC = [[NewsMainSearchViewController alloc] init];
    mainVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mainVC animated:YES];
}


#pragma mark -
#pragma mark - 网络请求

//请求分类数据
- (void)queryCategoryRequest
{
    WS(bself);
    [_newsService newsQueryCategoryWithSuccess:^(id responseObject)
    {
        [bself queryCategoryRequestCallBackWithObject:(NewsCategory *)responseObject];
    } failure:^(NSError *error)
    {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryCategoryRequest];
        }];
    }];
}

//分类请求回调
- (void)queryCategoryRequestCallBackWithObject:(NewsCategory *)category
{
    [HUDManager removeHUDFromView:self.view];
    
    [_hrizontalMenu initWithData:category.data];
    //构建主视图
    [self setupMainViewWithData:category.data];
    
    [self firstNewsListRequest];
}

//首次列表请求
- (void)firstNewsListRequest
{
    NewsListViewController *vc =(NewsListViewController *) _mainViewsArray[0];
    [vc refreshData];
}


- (void)queryNewsListWithVC:(NewsListViewController *)vc
{
    [vc refreshData];
    
}

#pragma mark - 
#pragma mark -ScrollView 代理相关方法

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self changeView:scrollView.contentOffset.x];
}



- (void)changeView:(CGFloat)x
{
    //取余
    if (fmod(x, _scrollView.frameWidth) == 0)
    {
        //当前页面不处理
        if (_currentPage == x / _scrollView.frameWidth) return;
        
        //获取新的页面
        NSInteger  index = x / _scrollView.frameWidth;
        
        [_hrizontalMenu selectItemAtIndex:index];
        
        _currentPage = x / _scrollView.frameWidth ;

        //取出新的页面数据
        if (!_mainViewsArray[index]) return;
        
        [self queryNewsListWithVC:_mainViewsArray[index]];
    }
}

@end
