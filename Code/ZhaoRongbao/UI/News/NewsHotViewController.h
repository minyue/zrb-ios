//
//  NewsHotViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/3.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ViewController.h"
@class NewsTagModel;
@interface NewsHotViewController : ZRB_ViewController


@property (nonatomic,copy)void(^showSearchListBlock)(NewsTagModel *model);

@end
