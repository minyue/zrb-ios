//
//  NewsHotViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/3.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsHotViewController.h"
#import "NewsService.h"
#import "NewsListModel.h"
#import "NewsHomeTagView.h"
#import "NewsRandomCell.h"
#import "NewsWebViewController.h"


static NSString *const kRandomNewsCellIdentifier  = @"RandomNewsCellIdentifier";
@interface NewsHotViewController ()
{
    
    IBOutlet UITableView  *_tableView;
    
    IBOutlet   UIView   *_tagListView;
    
    IBOutlet   UIView   *_topView;
    

    NewsService *_newsService;  //网络请求服务
    
    NSMutableArray  *_dataArray;

    
    
}
- (void)setup;




/**
 *  刷新随机新闻
 *
 *  @param sender
 */
- (IBAction)refreshRandomNews:(id)sender;


@end

@implementation NewsHotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];

    [self querySearchLabel];
    [self queryRandomNewsList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


#pragma mark - 
#pragma mark - 私有方法


/**
 *  刷新随机新闻
 *
 *  @param sender
 */
- (IBAction)refreshRandomNews:(id)sender
{
    [self queryRandomNewsList];
}


- (void)setup
{
    _newsService = [[NewsService alloc] init];
    
    _dataArray = [NSMutableArray new ];
    
    [self.view layoutIfNeeded];
    
    UIView  *backGrondView = [[UIView alloc] init];
    backGrondView.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    [self.view addSubview:backGrondView];
    [self.view sendSubviewToBack:backGrondView];
    [backGrondView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tableView.mas_top);
        make.leading.and.right.and.bottom.equalTo(self.view);
    }];
}


/**
 *  构建常用标签视图
 *
 *  @param tagList 标签数据
 */
- (void)setupTagListViewWithData:(NewsTagList *)tagList
{
    
    WS(bself);
    TagItemView *lastView = nil;
    
    UIView *superView = _tagListView;
    
    __block NSInteger  lastWith = 0;
    for (int i = 0; i < tagList.data.count; i ++)
    {
        TagItemView *itemView = [[TagItemView alloc] init];
        NewsTagModel *model = (NewsTagModel *)tagList.data[i];
        itemView.tagItemClickBlock = ^(NewsTagModel *model){
            [bself showSearchViewWithTagModel:model];
        };
        [itemView   initWithModel:model ];
        [superView addSubview:itemView];
        
        NSDictionary *textAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:11.0f]};
      __block   NSInteger tagWith = [model.labelName sizeWithAttributes:textAttributes].width + 15/*字距离边界的距离*/ + 10/*标签间隔*/;
        
        [itemView makeConstraints:^(MASConstraintMaker *make) {
            
          
            if (!lastView)
            {
                /**
                 *  当第一个View不存在的时候按照常规布局
                 */
                make.leading.equalTo(superView.mas_leading).offset(10);
                make.top.equalTo(superView.mas_top).offset(10);
                
            }else
            {
                
                if ((lastWith + tagWith) > (UIScreenWidth - 10))
                {
                    /**
                     *  当需要换行的时候顶部需要连接到上一个View
                     */
                    make.leading.equalTo(superView.mas_leading).offset(10);
                    make.top.equalTo(lastView.mas_bottom).offset(5);

                }else
                {
                    
                    /**
                     *  多行的时候横向常规布局
                     *  竖排的时候不能连接到上个view的底部，需要垂直方向和横排的竖向中心对齐
                     */
                    make.leading.equalTo(lastView.mas_right).offset(10);
                
                    //这个地方花费的时间比较长（空间想象能力要加强咯）
                    make.centerY.equalTo(lastView.mas_centerY);
                }
            }

        }];
        
        lastView = itemView ;
        [itemView setNeedsLayout];
        [itemView  layoutIfNeeded];
        lastWith =(lastView.frameX+lastView.frameWidth);
        
        
        /**
         *  动态改变高度
         */
        if (i == tagList.data.count - 1)
        {
            
            NSInteger   tagHeight = lastView.frameY + lastView.frameHeight + 50;
            
            
            
            NSLayoutConstraint *con = [_topView findOwnConstraintForAttribute:NSLayoutAttributeHeight];
            con.constant = tagHeight;
            [_topView layoutIfNeeded];
            
        }
        
        
    }


}


/**
 *  进入搜索页面(标签)
 *
 *  @param model 标签数据
 */
- (void)showSearchViewWithTagModel:(NewsTagModel *)model
{
    if (self.showSearchListBlock) {
        self.showSearchListBlock(model);
    }

}




#pragma mark - 
#pragma mark - 网络请求

/**
 *  常用标签请求
 */
- (void)querySearchLabel
{
    WS(bself);
    [_newsService querySearchLabelWithSuccess:^(id responseObject)
     {
         [bself querySearchLabelCallBackWithObj:(NewsTagList *)responseObject];
        
    } failure:^(NSError *error) {
        
    }];
}

/**
 *  常用标签请求返回
 *
 *  @param tagList 常用标签数据
 */
- (void)querySearchLabelCallBackWithObj:(NewsTagList *)tagList
{
    if (tagList.res == ZRBHttpSuccssType) {
        [self setupTagListViewWithData:tagList];
    }
}


/**
 *  获取随机新闻
 */
- (void)queryRandomNewsList
{
    WS(bself);
    [_newsService querHotPoularAPIWithSuccess:^(id responseObject)
     {
         [bself queryRandomNewsListCallBackWithObj:(NewsRandomItemList *)responseObject];
         
     } failure:^(NSError *error) {
         
     }];
    
}

/**
 *  获取随机新闻返回
 *
 *  @param randomList 返回数据
 */
- (void)queryRandomNewsListCallBackWithObj:(NewsRandomItemList *)randomList
{
    if (randomList.res == ZRBHttpSuccssType) {
        _dataArray = randomList.data;
        [_tableView reloadData];
    }
}




#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsRandomCell *cell = [_tableView dequeueReusableCellWithIdentifier:kRandomNewsCellIdentifier forIndexPath:indexPath];
    
    NewsRandomItem  *model = _dataArray[indexPath.row];
    
    [cell initWithData:model indexPath:indexPath];
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 33;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsWebViewController *webViewController = (NewsWebViewController *)[StoryBoardUtilities  viewControllerForStoryboardName:@"News" class:[NewsWebViewController class]];
    NewsRandomItem *model = _dataArray[indexPath.row];
    webViewController.newsID = model.id ;
    [self.navigationController pushViewController:webViewController animated:YES];
    
    
}



- (void)dealloc
{
    NSLog(@"开始释放  NewsHotViewController");
}




@end
