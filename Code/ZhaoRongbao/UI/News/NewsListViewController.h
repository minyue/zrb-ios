//
//  NewsListViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  咨询列表页面

#import <UIKit/UIKit.h>
#import "NewsCategory.h"
@interface NewsListViewController : UIViewController


@property(nonatomic,strong) CategoryItem     *item;



- (void)refreshData;
@end
