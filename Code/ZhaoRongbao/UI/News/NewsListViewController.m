//
//  NewsListViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsListViewController.h"
#import "NewsService.h"
#import "NewsListModel.h"
#import "NewsHomeViewCell.h"
#import "NewsWebViewController.h"
#import "NewsSearchViewController.h"
#import "MJRefresh.h"
#import "NewsMainSearchViewController.h"
static NSString *const kNewsHomeViewCellIdetifier = @"NewsHomeViewCellIdetifier";

static NSString *const kShowWebViewSegue     = @"ShowWebViewSegue";
@interface NewsListViewController()
{

    IBOutlet   UITableView   *_tableView;
    
    BOOL       _isRequst;            //是否请求过
    
    NewsService         *_newsService;
    
    NSMutableArray      *_listArray;
}

- (void)addHeaderView;


- (void)addFooterView;
@end

@implementation NewsListViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
}

#pragma mark -
#pragma mark -私有方法

- (void)setup
{
    _isRequst = NO;

    _newsService = [[NewsService alloc] init];
    _listArray = [NSMutableArray new];
    [self addHeaderView];
    [self addFooterView];
}


/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _tableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
    
}


/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}



#pragma mark - 
#pragma mark -公有方法

/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        
        [_tableView.header beginRefreshing];

    }
}


#pragma mark - 
#pragma mark -网络请求



/**
 *  请求新闻列表
 */
- (void)queryListData
{
    _isRequst = NO;

    NewsQueryListRequest *req = [[NewsQueryListRequest alloc] init];
    req.forword = NO;
    req.limit = 10;
    req.category = self.item.id == 0 ? nil : @(self.item.id);
    [self startWithReq:req];


}


/**
 *  加载更多
 */
- (void)loadMoreData
{
    
    NewsItemModel *lastObj = [_listArray lastObject];
    NewsQueryListRequest *req = [[NewsQueryListRequest alloc] init];
    req.forword = NO;
    req.limit = 10;
    req.category = self.item.id == 0 ? nil : @(self.item.id);
    req.id   = [NSNumber numberWithInt:lastObj.id];
    [self loadMoreWithReq:req];
}



/**
 *  下拉加载
 *
 *  @param req 请求体
 */

- (void)startWithReq:(NewsQueryListRequest *)req
{
    
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_newsService newsQueryListWithRequest:req success:^(id responseObject) {
        
        [bself newsQueryListCallBackWithObject:(NewsListModel *)responseObject];
        
    } failure:^(NSError *error)
     {
         
     }];
    
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(NewsQueryListRequest   *)req
{
    
    WS(bself);
    [_newsService newsQueryListWithRequest:req success:^(id responseObject) {
        
        [bself loadMoreListCallBackWithObject:(NewsListModel *)responseObject];
        
    } failure:^(NSError *error)
     {
     }];
    
}




- (void)newsQueryListCallBackWithObject:(NewsListModel *)model
{
    //请求完毕的标志
    _isRequst = YES;
    
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            _listArray = model.data;
            [_tableView reloadData];
            break;
        }
        case ZRBHttpFailType:
        {
            
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
        default:
            break;
    }
    
    [_tableView.header endRefreshing];

    
}



- (void)loadMoreListCallBackWithObject:(NewsListModel *)model
{
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            [_listArray addObjectsFromArray:model.data];
            [_tableView reloadData];
            
            if (model.data.count == 0) {
                
//                _tableView.footer = nil;
                [_tableView.footer setState:MJRefreshStateNoMoreData];
            }else
            {
                [_tableView.footer endRefreshing];

            }
            break;
        }
        case ZRBHttpFailType:
        {
            [_tableView.header endRefreshing];

            break;
        }
        case ZRBHttpNoLoginType:
        {
            [_tableView.header endRefreshing];

            break;
        }
        default:
            break;
    }
}

#pragma mark -
#pragma mark - 列表相关方法


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WS(bself);
    NewsHomeViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kNewsHomeViewCellIdetifier forIndexPath:indexPath];
    
    NewsItemModel *model = _listArray[indexPath.row];
    cell.tagItemClickBlock = ^(NewsTagModel *model)
    {
        [bself showSearchViewWithTagModel:model];
    };
    [cell initWithModel:model andIfNeedBtn:YES];
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 110;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kShowWebViewSegue]) {
        NSIndexPath *path = [_tableView indexPathForSelectedRow];
        NewsItemModel *model = _listArray[path.row];
        NewsWebViewController *webViewController = segue.destinationViewController;
        webViewController.newsID = model.id ;
        webViewController.hidesBottomBarWhenPushed = YES;
    }
}



/**
 *  进入搜索页面(标签)
 *
 *  @param model 标签数据
 */
- (void)showSearchViewWithTagModel:(NewsTagModel *)model
{
    NewsMainSearchViewController *searchVc = [[NewsMainSearchViewController alloc] init];
    [searchVc setTagModel:model];
    searchVc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVc animated:YES];
}


@end
