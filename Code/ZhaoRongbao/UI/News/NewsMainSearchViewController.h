//
//  NewsMainSearchViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/9.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  搜索的主视图

#import "ZRB_ViewController.h"
#import "NewsListModel.h"
#import "NewsConst.h"
@interface NewsMainSearchViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic,assign) NewsSearchType  searchType;


- (void)setTagModel:(NewsTagModel *)model;


@end
