//
//  NewsMainSearchViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/9.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsMainSearchViewController.h"
#import "NewsHotViewController.h"
#import "NewsSearchViewController.h"
#import "NewsSearchListViewController.h"
#import "ZRB_SearchBar.h"
#import "UINavigationItem+CustomItem.h"
#import "NewsHomeTagView.h"
#import "SearchHistoryManager.h"


@interface NewsMainSearchViewController ()<ZRB_SearchBarDelegate>
{
    NewsHotViewController           *_hotViewController;        //热门
    
    NewsSearchViewController        *_searchViewController;     //搜索历史记录
    
    NewsSearchListViewController    *_searchListViewController; //搜索列表
    
    ZRB_ViewController              *_currentViewController;    //当前视图
    
    NSMutableArray                  *_tokens;
    
    ZRB_SearchBar           *_searchBar;
    

    NSString                  *_searchValue;
    
    NewsTagModel              *_tagModel;
    
    NewsSearchViewType        _currentViewType;   //当前视图的类型


}

/**
 *  设置导航栏
 */
- (void)setupNavigationBar;

/**
 *  设置子视图
 */
- (void)setupChildViewControllers;

/**
 *  设置第一次加载的View
 */
- (void)setupFirstSubView;

@end

@implementation NewsMainSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    [self setupChildViewControllers];
    [self setupFirstSubView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupNavigationBar];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeNavigationBar];
}

- (void)setup
{
    _tokens = [NSMutableArray new];
}

/**
 *  设置导航栏
 */
- (void)setupNavigationBar
{
    if (!_searchBar)
    {
        _searchBar = (ZRB_SearchBar *)[ZRBUtilities viewFromSourceName:@"ZRB_SearchBar"];
        _searchBar.delegate = self;
        [self.navigationController.navigationBar addSubview:_searchBar];
        
        switch (_currentViewType) {
            case NewsSearchViewTypeHot:
            {
                _searchBar.frame = CGRectMake(65, 9, UIScreenWidth-90, 26);
                _searchBar.text = nil;
                break;
            }
            case NewsSearchViewTypeHistory:
            {
                _searchBar.frame = CGRectMake(10, 9, UIScreenWidth-75, 26);
                
                break;
            }
            case NewsSearchViewTypeList:
            {
                _searchBar.frame = CGRectMake(10, 9, UIScreenWidth-75, 26);

                break;
            }
            default:
                break;
        }
    }
    
    [self changeSearchType:_searchType];

}



- (void)removeNavigationBar
{
    if (_searchBar)
    {
        [_searchBar removeFromSuperview];
        _searchBar = nil;
    }
}

/**
 *  设置子视图
 */
- (void)setupChildViewControllers
{
    WS(bself);
    
    _hotViewController = (NewsHotViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"News" class:[NewsHotViewController class]];
    _hotViewController.showSearchListBlock = ^(NewsTagModel *model){
        [bself showSearchListWithItem:model];
    };
    [self addChildViewController:_hotViewController];
    
    _searchViewController = (NewsSearchViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"News" class:[NewsSearchViewController class]];
    _searchViewController.searchListBlock = ^(HistoryItem *item)
    {
        [bself showSearchListWithItem:[bself modelWithItem:item]];
    };
    [self addChildViewController:_searchViewController];
    
    _searchListViewController = (NewsSearchListViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"News" class:[NewsSearchListViewController class]];
    _searchListViewController.hideKeyBoardBlock = ^{
        
        [bself hideKeyBoard];
        
    };
    [self addChildViewController:_searchListViewController];
    
    
}


/**
 *  设置第一次加载的View
 */
- (void)setupFirstSubView
{
    /**
     *  由于进到搜索页面有两种状态 一个是进入热门搜索，一个是进入搜索列表
     */
    if (_tagModel)
    {
        
        //搜索列表
        [self.view addSubview:_searchListViewController.view];
        
        [_searchListViewController didMoveToParentViewController:self];
        
        _currentViewController = _searchListViewController;
        
        _currentViewType = NewsSearchViewTypeList;
        
        
        [self searchBarStyleOther];
        
        [self showSearchListWithItem:_tagModel];
        
        
        
    }else
    {
        //热门搜索
        [self.view addSubview:_hotViewController.view];
        
        [_hotViewController didMoveToParentViewController:self];
        
        _currentViewController = _hotViewController;
        
    }

}


- (void)hideKeyBoard
{
    if ([_searchBar isFirstResponder]) {
        [_searchBar resignFirstResponder];
    }
}

/**
 *  改变视图
 *
 *  @param type 视图类型
 */
- (void)changeCurrentViewType:(NewsSearchViewType )type
{
    _currentViewType = type;
    
    switch (type) {
        case NewsSearchViewTypeHot:
        {
            [self showHotView];
            break;
        }
        case NewsSearchViewTypeHistory:
        {
            [self searchBarStyleOther];
            [self showHistoryView];
            break;
        }
        case NewsSearchViewTypeList:
        {
            [self searchBarStyleOther];
            [self showListView];
            break;
        }
            
        default:
            break;
    }
    
}


/**
 *  设置导航栏的样式
 */
- (void)searchBarStyleOther
{
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    
    
    _searchBar.frame = CGRectMake(10, 9, UIScreenWidth-75, 26);
    [_searchBar becomeFirstResponder];

  
    NSString *title = @"取消";
    CustomBarItem *item =  [self.navigationItem setItemWithTitle:title textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [item setOffset:-2];
    [item addTarget:self selector:@selector(cancelSelf:) event:UIControlEventTouchUpInside];
    
    
}


/**
 *  在历史记录和搜索列表中的取消
 *
 *  @param sender
 */
- (void)cancelSelf:(id)sender
{
//    if (_currentViewType == NewsSearchViewTypeList)
//    {
//        [self changeSearchType:NewsSearchTypeKeyValue];
//        [self changeCurrentViewType:NewsSearchViewTypeHistory];
//        _searchValue = nil;
//
//        
//    }else
//    {
//        [self.navigationController  popViewControllerAnimated:YES];
//    }

    [self.navigationController  popViewControllerAnimated:YES];

}



/**
 *  展示热门
 */
- (void)showHotView
{
    [self transitionFromViewController:_currentViewController toViewController:_hotViewController duration:0.4 options:UIViewAnimationOptionLayoutSubviews animations:^{
        
    } completion:^(BOOL finished) {
        _currentViewController = _hotViewController;
    }];
}

/**
 *  展示历史记录
 */
- (void)showHistoryView
{
    [self transitionFromViewController:_currentViewController toViewController:_searchViewController duration:0.4 options:UIViewAnimationOptionLayoutSubviews animations:^{
        
    } completion:^(BOOL finished) {
        _currentViewController = _searchViewController;
    }];

}

/**
 *  展示列表
 */
- (void)showListView
{
    [self transitionFromViewController:_currentViewController toViewController:_searchListViewController duration:0.4 options:UIViewAnimationOptionLayoutSubviews animations:^{
        
    } completion:^(BOOL finished) {
        _currentViewController = _searchListViewController;
    }];

}


/**
 *  第一次如果进入搜索列表界面的话需要设置TagModel ，后续根据这个model进行其他逻辑
 *
 *  @param model
 */
- (void)setTagModel:(NewsTagModel *)model
{
    [_tokens removeAllObjects];
    _tagModel = model;
    [_tokens addObject:model];
    [self changeSearchType:NewsSearchTypeTag];
    
}


/**
 *
 *  更改搜索类型
 *  @param type 标签或者关键字
 */
- (void)changeSearchType:(NewsSearchType)type
{
    self.searchType = type;
    
    if (type == NewsSearchTypeTag)
    {
        [_searchBar reloadData];
        _searchBar.text = @"";
        
    }else
    {
        _tagModel = nil;
        [_tokens removeAllObjects];
        [_searchBar reloadData];
       _searchBar.text  = _searchValue;
    
    }
}




/**
 *  添加历史记录
 *
 *  @param msg  消息
 *  @param type 消息类型（标签、关键字）
 */
- (void)insertHistory:(NSString *)msg   type:(NewsSearchType)type  tagId:(int)mid
{
    HistoryItem *item = [[HistoryItem  alloc] init];
    item.title = msg;
    if (type == NewsSearchTypeTag)
    {
        item.mid = mid;
    }
    [SearchHistoryManager insertObject:item];
    
}


/**
 *  进入搜索列表
 *
 *  @param item 搜索实体
 */
- (void)showSearchListWithItem:(NewsTagModel *)model
{

    if (_currentViewType!= NewsSearchViewTypeList) {
        [self changeCurrentViewType:NewsSearchViewTypeList];

    }
    
    //搜索关键字
    if (model.id == -1)
    {
        _searchBar.text = model.labelName;
        _searchValue = model.labelName;
        [self changeSearchType:NewsSearchTypeKeyValue];
        [_searchListViewController queryListWithMsg:model.labelName type:NewsSearchTypeKeyValue];
        
    }else
    {
        
        [_tokens removeAllObjects];
        
        _tagModel = model;
        [_tokens addObject:model];
        [self changeSearchType:NewsSearchTypeTag];

        [_searchListViewController queryListWithMsg:[NSString stringWithFormat:@"%d",model.id] type:NewsSearchTypeTag];
    }
    
    //保存
    [self insertHistory:_tagModel.labelName type:NewsSearchTypeTag tagId:_tagModel.id];

}


/**
 *  由历史记录组装成搜索
 *
 *  @param item
 *
 *  @return 
 */
- (NewsTagModel *)modelWithItem:(HistoryItem   *)item
{
    NewsTagModel *model = [[NewsTagModel alloc] init];
    model.id = item.mid;
    model.labelName = item.title;
    return model;
}


#pragma mark - UISearchBarDelegate 协议

- (CGFloat)lineHeightForTokenInField:(ZFTokenField *)tokenField
{
    return 25;
}

- (NSUInteger)numberOfTokenInField:(ZFTokenField *)tokenField
{
    return _tokens.count;
}

- (UIView *)tokenField:(ZFTokenField *)tokenField viewForTokenAtIndex:(NSUInteger)index
{
    NewsTagModel *model = _tokens[index];
    
    NSDictionary *textAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:11.0f]};
    NSInteger tagWith = [model.labelName sizeWithAttributes:textAttributes].width + 15/*字距离边界的距离*/ + 10/*标签间隔*/;
    
    
    TagItemView *item = [[TagItemView    alloc] initWithFrame:CGRectMake(0, 0, tagWith - 5, 20)];
    [item initWithModel:model];
    return item;
}

#pragma mark - ZFTokenField Delegate

- (CGFloat)tokenMarginInTokenInField:(ZFTokenField *)tokenField
{
    return 5;
}

- (void)tokenField:(ZFTokenField *)tokenField didReturnWithText:(NSString *)text
{

    //回车
    if (![ZRBUtilities isBlankString:text])
    {
        /**
         * 保存到本地
         */
        [self insertHistory:text type:NewsSearchTypeKeyValue tagId:-1];
        _searchValue = text;

        if (_currentViewType !=NewsSearchViewTypeList )
        {
            
            [self changeCurrentViewType:NewsSearchViewTypeList];
  
        }
        //搜索
        [_searchListViewController queryListWithMsg:text type:NewsSearchTypeKeyValue];
    }
}

- (void)tokenField:(ZFTokenField *)tokenField didRemoveTokenAtIndex:(NSUInteger)index
{
    [_tokens removeObjectAtIndex:index];
}


- (void)tokenFieldDidBeginEditing:(ZRB_SearchBar *)tokenField
{
    if (_currentViewType == NewsSearchViewTypeHot)
    {
        [self changeCurrentViewType:NewsSearchViewTypeHistory];

    }
}



- (BOOL)tokenFieldShouldEndEditing:(ZFTokenField *)textField
{
    return NO;
}


- (void)tokenField:(ZRB_SearchBar *)tokenField didTextChanged:(NSString *)text
{
    if (self.searchType == NewsSearchTypeTag)
    {
        [self changeSearchType:NewsSearchTypeKeyValue];
        [_searchBar becomeFirstResponder];
    }
    
}



- (void)dealloc
{
    NSLog(@"开始释放NewsMainSearchViewController");
}



@end
