//
//  NewsSearchListViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "NewsConst.h"
@interface NewsSearchListViewController : ZRB_ViewController

@property (nonatomic,copy) void(^hideKeyBoardBlock)();

/**
 *  搜索咨询列表
 *
 *  @param msg  搜索关键字或者标签ID
 *  @param type 搜索类型（关键字搜索、或者标签搜索）
 */
- (void)queryListWithMsg:(NSString *)msg  type:(NewsSearchType)type;
@end
