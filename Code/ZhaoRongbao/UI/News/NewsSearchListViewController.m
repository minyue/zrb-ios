//
//  NewsSearchListViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsSearchListViewController.h"
#import "NewsSearchListCell.h"
#import "NewsService.h"
#import "NewsConst.h"
#import "NewsSearchModel.h"
#import "NewsWebViewController.h"
#import "MJRefresh.h"

static NSString *const kNewsSearchListCellIdentifier = @"NewsSearchListCellIdentifier";
@interface NewsSearchListViewController ()<UIScrollViewDelegate>
{
    NSMutableArray  *_listArray;
    
    IBOutlet UITableView  *_tableView;
    
    NewsService     *_service;
    
    int  _currentPage;
    
    NSString *_keyValue;
    
    NewsSearchType _searchType;
}


- (void)setup;

@end

@implementation NewsSearchListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)setup
{
    _service = [[NewsService alloc] init];
    
}



/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}

- (void)loadMoreData
{
    NewsSearchListRequst *request = [[NewsSearchListRequst alloc] init];
    request.searchType = _searchType;
    request.searchStr = _keyValue;
    request.pageSize = 10;
    request.pageNum = ++_currentPage;
    WS(bself);
    [_service querySearchListWithRequest:request success:^(id responseObject)
     {
         [bself newsQueryListCallBackWithObject:(NewsSearchListModel *)responseObject];
         
     } failure:^(NSError *error)
     {
         
     }];

}



- (void)queryListWithMsg:(NSString *)msg  type:(NewsSearchType)type
{
    _keyValue = msg;
    
    _searchType = type;
    
    _currentPage = 1;
    
    [_listArray  removeAllObjects];
    [_tableView reloadData];

    NewsSearchListRequst *request = [[NewsSearchListRequst alloc] init];
    request.searchType = type;
    request.searchStr = msg;
    request.pageSize = 10;
    request.pageNum = 1;
    
    WS(bself);
    [_service querySearchListWithRequest:request success:^(id responseObject)
    {
        [bself newsQueryListCallBackWithObject:(NewsSearchListModel *)responseObject];
        
    } failure:^(NSError *error)
    {
        [HUDManager showNonNetWorkHUDInView:self.view event:nil];

    }];
    
    [HUDManager showLoadNetWorkHUDInView:self.view];

}



- (void)newsQueryListCallBackWithObject:(NewsSearchListModel *)model
{
    [HUDManager removeHUDFromView:self.view];
    
    //请求完毕的标志
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            if (_currentPage == 1)
            {
                _listArray = model.data.list;
                
                if (_listArray.count < model.data.total)
                {
                    [self addFooterView];
                }
                [_tableView reloadData];

                if (_listArray.count == 0)
                {
                    [HUDManager showNonDataHUDInView:self.view withMsg:kHttpNonData];
                }

            }else
            {
                if (model.data.list.count == 0)
                {
                    _tableView.footer = nil;
                }else
                {
                    [_listArray addObjectsFromArray:model.data.list];
                    [_tableView reloadData];
                    [_tableView.footer endRefreshing];
                }
            }
            
            break;
        }
        case ZRBHttpFailType:
        {
            if (_currentPage == 1)
            {
                [HUDManager showNonDataHUDInView:self.view withMsg:kHttpFailError];
            }
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
        default:
            break;
    }

}




#pragma mark -
#pragma mark - 列表相关方法

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsSearchListCell *cell = [_tableView dequeueReusableCellWithIdentifier:kNewsSearchListCellIdentifier forIndexPath:indexPath];
    
    NewsItemModel *model = _listArray[indexPath.row];
    
    [cell initWithModel:model];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsWebViewController *webViewController = (NewsWebViewController *)[StoryBoardUtilities  viewControllerForStoryboardName:@"News" class:[NewsWebViewController class]];
    NewsRandomItem *model = _listArray[indexPath.row];
    webViewController.newsID = model.id ;
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.hideKeyBoardBlock) {
        self.hideKeyBoardBlock();
    }
}


- (void)dealloc
{
    NSLog(@"开始释放NewsSearchListViewController");
}
@end
