//
//  NewsSearchViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/3.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "NewsConst.h"
#import "NewsListModel.h"
@class HistoryItem;
@interface NewsSearchViewController : ZRB_ViewController


@property (nonatomic,copy) void(^searchListBlock)(HistoryItem *item);

@property (nonatomic,assign) NewsSearchType  searchType;


@end




