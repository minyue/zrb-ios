//
//  NewsSearchViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/3.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsSearchViewController.h"
#import "UINavigationItem+CustomItem.h"
#import "ZRB_SearchBar.h"
#import "NewsHistroyCell.h"
#import "SearchHistoryManager.h"
#import "NewsHistoryDeleteCell.h"
#import "NewsHomeTagView.h"
static NSString *const kNewsHistroyCellIdentifier = @"NewsHistroyCellIdentifier";

static NSString *const kNewsDelCellIdentifier      = @"NewsDelCellIdentifier";


@interface NewsSearchViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet  UIView        *_backGroundView;
    
    IBOutlet  UITableView   *_tableView;
    
    NSMutableArray          *_dataArray;
    
}

@property (nonatomic,strong) NSMutableArray *dataArray;

- (void)setup;
@end

@implementation NewsSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    [self historyData];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)setup
{

    _dataArray = [NSMutableArray new];
}



- (void)removeAllHistory
{
    [SearchHistoryManager removeAll];
    [_tableView reloadData];
}


- (void)removeHitroyItem:(HistoryItem  *)item
{
    [SearchHistoryManager removeObject:item];
    [_tableView reloadData];
}

/**
 *  获取搜索历史记录
 */
- (void)historyData
{
   _dataArray = [SearchHistoryManager historyData];
  [_tableView reloadData];
}



#pragma mark -
#pragma mark - 列表相关方法



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0 ? _dataArray.count:1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NewsHistroyCell *cell = [_tableView dequeueReusableCellWithIdentifier:kNewsHistroyCellIdentifier forIndexPath:indexPath];
       [cell initWithItem:_dataArray[indexPath.row]];
        WS(bself);
        cell.removeHistoryBlock = ^{
            [bself removeHitroyItem:bself.dataArray[indexPath.row]];
        };
        return cell;

    }else
    {
        NewsHistoryDeleteCell *cell = [_tableView dequeueReusableCellWithIdentifier:kNewsDelCellIdentifier forIndexPath:indexPath];
        WS(bself);
        cell.removeAllBlock = ^{
            [bself removeAllHistory];
        };
        return cell;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (_dataArray.count == 0)
        {
            return 0;
        }else
        {
            return 38;
        }
    }else
    {
        return 33;
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  进入搜索到得内容界面
     */
    if (indexPath.section == 0)
    {
        if (self.searchListBlock) {
            self.searchListBlock(_dataArray[indexPath.row]);
        }
    }
}

- (void)dealloc
{
    NSLog(@"开始释放NewsSearchViewController");
}
@end



