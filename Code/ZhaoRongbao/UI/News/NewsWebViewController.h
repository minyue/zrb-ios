//
//  NewsWebViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/30.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  资讯详情

#import "ZRB_ViewController.h"
#import "ProjectHomeViewController.h"
@interface NewsWebViewController : ZRB_ViewControllerWithBackButton

@property (nonatomic,assign) int newsID;

- (void)loadDetailWithId:(int)mid;

@end
