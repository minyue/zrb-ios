//
//  NewsWebViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/30.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsWebViewController.h"
#import "UIButton+Addtions.h"
#import "ZRB_ShareView.h"
#import "ZRB_FontSheet.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "NewsMainSearchViewController.h"
#import "UINavigationItem+CustomItem.h"
#import "ProjectService.h"
#import "CollectionModel.h"
#import "MineLoginViewcController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "MineContainerViewController.h"
#import "ZRB_NormalModel.h"

@interface NewsWebViewController()<UIWebViewDelegate>
{
    IBOutlet  UIWebView   *_webView;
    
    UIView        *_rightBarView;
    
    JSContext    *_content;
    
    ProjectService *_projectService;
}

@property (nonatomic,assign) BOOL  on;

@property (nonatomic,assign)ChangeFontType type;

- (void)setup;

- (void)setupLeftItem;

- (void)setupJsContent;


@end

@implementation NewsWebViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
    [self setupLeftItem];
    

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupNavBarViews];
}

- (void)setup
{
    _webView.autoresizesSubviews = YES;
    _webView.scalesPageToFit = YES;
    _webView.delegate = self;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    //禁止数字链接
    _webView.dataDetectorTypes = UIDataDetectorTypeNone;
    [self loadDetailWithId:self.newsID];
    self.on = NO;
    self.type = FontTypeLarge;
    _projectService = [[ProjectService alloc] init];
}


- (void)setupLeftItem
{
    NSString *title = @"返回";
    CustomBarItem *backItem = [self.navigationItem setItemWithTitle:title   textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:left];
    [backItem addTarget:self selector:@selector(click_popViewController) event:UIControlEventTouchUpInside];
    [backItem setOffset:2];
}


- (void)click_popViewController
{
    if ([_webView canGoBack]) {
        [_webView goBack];
    }else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)retuqestIfAttention:(UIButton*)btn{
    btn.selected = NO;
    [_projectService checkIfAttentionNewOrProjectWithRequest:[NSString stringWithFormat:@"%d",self.newsID] andType:0 success:^(id responseObject) {
        if(responseObject){
            ZRB_NormalModel *model = responseObject;
            if(model.res == 1){
                btn.selected = [[model.data valueForKey:@"isCollected"] boolValue];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)setupNavBarViews
{
    _rightBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 2, 115, 44)];
    _rightBarView.backgroundColor = [UIColor clearColor];
    
    //分享
    UIButton  *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBtn setImage:[UIImage imageNamed:@"news_bar_Share"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareNews:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:shareBtn];
    [shareBtn makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_rightBarView.mas_leading).offset(5);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    UILabel  *shareLabel = [[UILabel alloc] init];
    [shareLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [shareLabel setBackgroundColor:[UIColor clearColor]];
    shareLabel.text = @"分享";;
    shareLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:shareLabel];
    
    [shareLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(shareBtn.mas_centerX);
        make.top.equalTo(shareBtn.mas_bottom).offset(2);
    }];
    
    
    //关注
    UIButton  *focusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [focusBtn setImage:[UIImage imageNamed:@"news_bar_follow_off"] forState:UIControlStateNormal];
    [focusBtn setImage:[UIImage imageNamed:@"news_bar_follow_on"] forState:UIControlStateSelected];
    [focusBtn addTarget:self action:@selector(focusNews:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:focusBtn];
    [focusBtn makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(shareBtn.mas_right).offset(20);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    [self retuqestIfAttention:focusBtn];
    
    UILabel  *focusLabel = [[UILabel alloc] init];
    [focusLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [focusLabel setBackgroundColor:[UIColor clearColor]];
    focusLabel.text = @"关注";;
    focusLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:focusLabel];
    
    [focusLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(focusBtn.mas_centerX);
        make.top.equalTo(focusBtn.mas_bottom).offset(2);
    }];

    
    //字体
    UIButton  *fontBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [fontBtn setImage:[UIImage imageNamed:@"news_bar_txt"] forState:UIControlStateNormal];
    [fontBtn addTarget:self action:@selector(updateFont:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:fontBtn];
    [fontBtn makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(focusBtn.mas_right).offset(20);
        make.right.equalTo(_rightBarView.mas_right).offset(-5);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    
    UILabel  *fontLabel = [[UILabel alloc] init];
    [fontLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [fontLabel setBackgroundColor:[UIColor clearColor]];
    fontLabel.text = @"字体";;
    fontLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:fontLabel];
    
    [fontLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(fontBtn.mas_centerX);
        make.top.equalTo(fontBtn.mas_bottom).offset(2);
    }];

   CustomBarItem *item =  [self.navigationItem setItemWithCustomView:_rightBarView itemType:right];

    [item setOffset:-5];

}

- (void)setupJsContent
{
    //获取当前JS环境
    _content = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    // 打印异常
    _content.exceptionHandler =
    ^(JSContext *context, JSValue *exceptionValue)
    {
        context.exception = exceptionValue;
        NSLog(@"%@", exceptionValue);
    };
    WS(bself)
    //获取JS事件
    _content[@"showtag"] = ^(int num,NSString *tag){
        NewsTagModel *model = [[NewsTagModel alloc] init];
        model.id =  num;
        model.labelName = tag;
        [bself showSearchViewWithTagModel:model];
    };
}


- (void)removeNavBarViews
{
    if (_rightBarView)
    {
        [_rightBarView removeFromSuperview];
        _rightBarView = nil;
    }
}



/**
 *  进入搜索页面(标签)
 *
 *  @param model 标签数据
 */
- (void)showSearchViewWithTagModel:(NewsTagModel *)model
{
    NewsMainSearchViewController *searchVc = [[NewsMainSearchViewController alloc] init];
    [searchVc setTagModel:model];
    [self.navigationController pushViewController:searchVc animated:YES];
}



//分享
- (void)shareNews:(id)sender
{
    WS(bself);
    ZRB_ShareView *shareView = [[ZRB_ShareView alloc] init];
    shareView.shareBlock = ^(ZRBSocialSnsType type)
    {
        [bself shareWithSocialType:type];
    };
    [shareView show];
}

//关注
- (void)focusNews:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSString *strId = [NSString stringWithFormat:@"%d",self.newsID];
    if (button.selected == NO) {
        //关注项目
        [self collectionProject:strId button:button];
        
    }else{
        //取消关注
        [self cancelCollectionProject:strId button:button];
        
    }

}

//更新字体
- (void)updateFont:(id)sender
{
    WS(bself);
    ZRB_FontSheet *sheet = [[ZRB_FontSheet alloc] init];
    
    sheet.nightBlock = ^(BOOL on){
        [bself openNightModel:on];
    };
    
    sheet.fontBlock = ^(ChangeFontType type)
    {
        [bself changeFontSize:type];
    };
    
    sheet.on = self.on;
    sheet.type = self.type;
    [sheet show];

}


/**
 *  是否开启夜间模式
 *
 *  @param on YES:开启   NO:关闭
 */
- (void)openNightModel:(BOOL)on
{
    if (on) {
        self.on = YES;
        [_webView stringByEvaluatingJavaScriptFromString:@"load_night()"];
    }else
    {
        self.on = NO;
        [_webView stringByEvaluatingJavaScriptFromString:@"load_day()"];

    }
}



- (void)changeFontSize:(ChangeFontType)type
{
    
    switch (type)
    {
        case FontTypeLarge:
        {
            [_webView stringByEvaluatingJavaScriptFromString:@"doZoom(15)"];
            
            self.type = FontTypeLarge;
            break;
        }
        case FontTypeMiddle:
        {
            [_webView stringByEvaluatingJavaScriptFromString:@"doZoom(12)"];
            self.type = FontTypeMiddle;
            break;
        }
        case FontTypeSmall:
        {
            [_webView stringByEvaluatingJavaScriptFromString:@"doZoom(9)"];
            
            self.type = FontTypeSmall;
            break;
        }
        default:
            break;
    }
}



/**
 *  分享到社交平台
 *
 *  @param type 平台类型
 */
- (void)shareWithSocialType:(ZRBSocialSnsType)type
{


}

/**
 *  加载详情页面
 *
 *  @param mid 新闻ID
 */
- (void)loadDetailWithId:(int)mid
{
    NSString  *url = [NSString stringWithFormat:@"%@/newsapp/newsDetails?id=%d",SERVER_URL,mid];
    NSURL *webUrl = [NSURL URLWithString:url];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:webUrl];
    [_webView loadRequest:req];
}



- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //设置JS回调
    [self setupJsContent];

}

#pragma mark - 网络请求

/**
 *  项目关注
 */
- (void)collectionProject:(NSString *)projectID button:(UIButton *)button{
    
    if ([ZRB_UserManager isLogin]) {
        CollectionProjectRequest *request = [[CollectionProjectRequest alloc]init];
        request.projectID = projectID;
        request.collectionType = 0;
        request.zrb_front_ut = [ZRB_UserManager shareUserManager].userData.zrb_front_ut;
        
        //指示器
        [SVProgressHUD show];
        //发起请求
        WS(bself);
        [_projectService collectionPorjectWithRequest:request success:^(id responseObject) {
            [SVProgressHUD dismiss];
            
            CollectionModel *model = responseObject;
            switch (model.res) {
                case ZRBHttpSuccssType:
                {
                    [MBProgressHUD showHUDTitle:@"关注成功" onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
                    button.selected = YES;
                    break;
                }
                case ZRBHttpFailType:
                    [bself showTipViewWithMsg:@"关注失败"];
                    button.selected = NO;
                    
                    break;
                case ZRBHttpNoLoginType:
                    button.selected = NO;
                    break;
                default:
                    break;
            }
            
        } failture:^(NSError *error) {
            [SVProgressHUD dismiss];
            button.selected = NO;
            [bself showTipViewWithMsg:@"关注失败"];
        }];
        
    }else{
        [self pushToLogin];
    }
}

/**
 *  取消关注
 */
- (void)cancelCollectionProject:(NSString *)projectID button:(UIButton *)button{
    if ([ZRB_UserManager isLogin]) {
        
        CancelCollectionProjectRequest *request = [[CancelCollectionProjectRequest alloc]init];
        request.projectID = [NSString stringWithFormat:@"%d",self.newsID];
        request.collectionType = 0;
        request.zrb_front_ut = [ZRB_UserManager shareUserManager].userData.zrb_front_ut;
        
        //指示器
        [SVProgressHUD show];
        //发起请求
        WS(bself);
        [_projectService cancelCollectionProjectWithRequest:request success:^(id responseObject) {
            
            [SVProgressHUD dismiss];
            
            CollectionModel *model = responseObject;
            
            switch (model.res) {
                case ZRBHttpSuccssType:
                {
                    [MBProgressHUD showHUDTitle:@"取消关注" onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
                    button.selected = NO;
                    break;
                }
                case ZRBHttpFailType:
                    [bself showTipViewWithMsg:@"取消失败"];
                    button.selected = YES;
                    
                    break;
                case ZRBHttpNoLoginType:
                    button.selected = YES;
                    break;
                default:
                    break;
            }
            
        } failture:^(NSError *error) {
            button.selected = YES;
            [SVProgressHUD dismiss];
            [bself showTipViewWithMsg:@"取消失败"];
        }];
        
    }else{
        [self pushToLogin];
    }
}

- (void)pushToLogin{
    MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
    [self.navigationController pushViewController:loginCtrl animated:YES];
  
}

@end
