//
//  NewsConst.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#ifndef ZhaoRongbao_NewsConst_h
#define ZhaoRongbao_NewsConst_h

/**
 *  历史记录搜索
 */
typedef NS_ENUM(NSInteger, NewsSearchType){
    /**
     *  标签搜索
     */
    NewsSearchTypeTag =1,
    /**
     *  关键字搜索
     */
    NewsSearchTypeKeyValue = 2
};




typedef NS_ENUM(NSInteger, NewsCategoryType)
{
    NewsHotType         = 20,   //新闻热点
    
    NewsViewPointType   = 32,   //观点解读
    
    NewsInvestmentType  = 28,   //投资动态
    
    NewsPoliciesType    = 14    //政策
};


typedef NS_ENUM(NSInteger, NewsSearchViewType){
    /**
     *  热门搜索
     */
    NewsSearchViewTypeHot,
    /**
     *  搜索历史记录
     */
    NewsSearchViewTypeHistory,
    /**
     *  搜索列表
     */
    NewsSearchViewTypeList
};


#endif
