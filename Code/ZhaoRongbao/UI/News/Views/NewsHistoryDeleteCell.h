//
//  NewsHistoryDeleteCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsHistoryDeleteCell : UITableViewCell

@property(nonatomic,copy) void(^removeAllBlock)();

- (IBAction)removeAll:(id)sender;

@end
