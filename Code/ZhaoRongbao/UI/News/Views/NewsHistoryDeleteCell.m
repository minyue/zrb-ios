//
//  NewsHistoryDeleteCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsHistoryDeleteCell.h"

@implementation NewsHistoryDeleteCell


- (IBAction)removeAll:(id)sender
{
    if (self.removeAllBlock) {
        self.removeAllBlock();
    }
}
@end
