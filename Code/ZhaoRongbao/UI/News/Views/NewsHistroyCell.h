//
//  NewsHistroyCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsConst.h"
@class HistoryItem;
@interface NewsHistroyCell : UITableViewCell

@property (nonatomic,copy) void(^removeHistoryBlock)();


- (void)initWithItem:(HistoryItem *)item;


- (IBAction)removeHistory:(id)sender;

@end
