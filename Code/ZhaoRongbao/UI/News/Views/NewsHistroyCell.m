//
//  NewsHistroyCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsHistroyCell.h"
#import "ZRB_LineView.h"
#import "SearchHistoryManager.h"
#import "NewsListModel.h"
#import "NewsHomeTagView.h"
@interface NewsHistroyCell()
{
    IBOutlet   UILabel   *_textLabel;
    
    ZRB_LineView        *_lineView;
}




@end

@implementation NewsHistroyCell


- (void)awakeFromNib
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setup];

}

- (void)setup
{

    _lineView = [[ZRB_LineView alloc] init];
    [self.contentView addSubview:_lineView];
    
    __block UIView *superView = self.contentView;
    [_lineView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.leading.equalTo(superView.mas_leading);
        make.right.equalTo(superView.mas_right);
        make.height.equalTo(@0.5);
    }];
    

}

- (void)initWithItem:(HistoryItem *)item
{
    if (item.mid == -1) {
        [self initWithKeyValue:item];

    }else
    {
        [self initWithTagValue:item];
    }
}


- (void)initWithKeyValue:(HistoryItem *)item
{
    _textLabel.text = item.title;

}

- (void)initWithTagValue:(HistoryItem *)item
{
    _textLabel.text = nil;
    
    [self removeTagView];
    
    TagItemView *itemView = [[TagItemView     alloc] init];
    itemView.tag = 1000;
    [itemView initWithModel:[self modelWithItem:item]];
    [self.contentView addSubview:itemView];
    __block UIView *superView = self.contentView;

    [itemView makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(superView.mas_centerY);
        make.leading.equalTo(superView.mas_leading).offset(20);
    }];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"热门标签";
    label.tag = 1001;
    label.font = [UIFont systemFontOfSize:11.0f];
    label.textColor = [UIColor colorWithHexString:@"969696"];
    [self.contentView addSubview:label];
    [label makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(itemView.mas_right).offset(10);
        make.centerY.equalTo(itemView.mas_centerY);
    }];
}



- (NewsTagModel *)modelWithItem:(HistoryItem   *)item
{
    NewsTagModel *model = [[NewsTagModel alloc] init];
    model.id = item.mid;
    model.labelName = item.title;
    return model;
}

- (void)removeTagView
{
    TagItemView *itemView = (TagItemView *)[self.contentView viewWithTag:1000];
    if (itemView)
    {
        [itemView removeFromSuperview   ];
        itemView = nil;
 
    }
    UILabel *label = (UILabel *)[self.contentView viewWithTag:1001];
    if (label)
    {
        [label removeFromSuperview];
        label = nil;

    }
    
}





- (IBAction)removeHistory:(id)sender
{

    if (self.removeHistoryBlock) {
        self.removeHistoryBlock();
    }
}
@end
