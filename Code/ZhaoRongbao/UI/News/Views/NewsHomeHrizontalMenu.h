//
//  NewsHomeHrizontalMenu.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsCategory.h"
@interface NewsHomeHrizontalMenu : UIView


//选中某个Item的block
@property (nonatomic,copy) void(^changeSelectItemBlock)(CategoryItem *model,NSInteger index);


- (void)initWithData:(NSMutableArray *)array;


- (void)selectItemAtIndex:(NSInteger)index;



@end





@interface HrizontalMenuButton : UIButton

@property(nonatomic,strong) CategoryItem    *model;

@end