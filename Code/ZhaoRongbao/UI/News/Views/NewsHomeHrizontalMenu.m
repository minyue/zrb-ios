//
//  NewsHomeHrizontalMenu.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsHomeHrizontalMenu.h"

#import "NSString+Extending.h"
@interface NewsHomeHrizontalMenu()
{
    UIScrollView *_scrollView;
    
    UIView      *_lineView;
    
    NSMutableArray    *_dataArray;
    
    
    NSMutableArray   *_btnArray;
    
    HrizontalMenuButton *selectButton;
}

@end

@implementation NewsHomeHrizontalMenu


- (instancetype)init
{

    if (self = [super init]) {
        [self setup];
    }
    
    return self;
}




- (void)awakeFromNib
{

    [super awakeFromNib];
    [self    setup];
}



- (void)setup
{
    
    UIView  *downLine = [[UIView alloc] init];
    downLine.backgroundColor = [UIColor colorWithHexString:@"e6e7ec"];
    [self addSubview:downLine];
    
    [downLine makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@1);
        make.leading.and.right.and.bottom.equalTo(self);
    }];
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.backgroundColor =[UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:_scrollView];
    
    [_scrollView makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.right.and.top.equalTo(self);
        make.bottom.equalTo(downLine.mas_top);
    }];
    
    
    _lineView = [[UIView alloc] init];
    _lineView.backgroundColor = [UIColor colorWithHexString:@"eb4f38"];
    [_scrollView addSubview:_lineView];
    _lineView.hidden = YES;

    
    _dataArray = [NSMutableArray new];
    _btnArray = [NSMutableArray new];
}


#pragma mark - 
#pragma mark -私有方法

- (void)initWithData:(NSMutableArray *)array
{
    
    [self removeAllSubButtons];
    
    [self createNewArrayWithArray:array];
    
    HrizontalMenuButton *lastView = nil;
    for (int i = 0; i <_dataArray.count; i ++)
    {
        CategoryItem *model = _dataArray[i];
        HrizontalMenuButton *headerButton = [HrizontalMenuButton buttonWithType:UIButtonTypeCustom];
        headerButton.model = model;
        headerButton.tag = i;
        headerButton.backgroundColor = [UIColor clearColor];
        [headerButton setTitle:model.type forState:UIControlStateNormal];
        [headerButton.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
        [headerButton setTitleColor:[UIColor colorWithHexString:@"000000"] forState:UIControlStateNormal];
        [headerButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollView addSubview:headerButton];
        float  x = lastView ? GetOriginX(lastView) + 20 : 10;
        
        float  y = 10;
        
        float  with = [model.type boundingRectWithSize:CGSizeMake(1000, 20) font:headerButton.titleLabel.font].width;
        
        float  height = 20;
        
        headerButton.frame = CGRectMake(x, y, with, height);
        
        lastView = headerButton;
        
        [_btnArray addObject:headerButton];
        
        if (i == 0) {
            _lineView.frame = CGRectMake(headerButton.frameX, 35, with, 2);

        }

    }

    [_scrollView setContentSize:CGSizeMake(GetOriginX(lastView) + 10, CGRectGetHeight(self.bounds) - 2)];
        
    [self selectAtIndex:0];

}



- (void)selectItemAtIndex:(NSInteger)index
{
    [self selectAtIndex:index];
    
}



- (void)removeAllSubButtons
{
    if (_dataArray.count > 0)
    {
        [_dataArray removeAllObjects];
    }
    
    if (_btnArray.count > 0) {
        
        for (HrizontalMenuButton *btn in _btnArray)
        {
            if (btn.superview) {
                [btn removeFromSuperview];
                
            }
        }
        
        [_btnArray removeAllObjects];;
    }

}

- (void)createNewArrayWithArray:(NSMutableArray *)array
{
    CategoryItem *model = [[CategoryItem alloc] init];
    model.type = @"全部新闻";
    model.id = 0;   //全部新闻
    [_dataArray addObject:model];
    [_dataArray addObjectsFromArray:array];
}



- (void)buttonClick:(id)sender
{
    HrizontalMenuButton *btn = (HrizontalMenuButton *)sender;
    [self animationButton:btn];
    
    /**
     *  上一级开始请求
     */
    if (self.changeSelectItemBlock)
    {
        self.changeSelectItemBlock(btn.model,btn.tag);
    }
}


- (void)selectAtIndex:(NSInteger)index
{
    if (_btnArray[index]) {
        [self animationButton:_btnArray[index]];
    }
    
}

- (void)animationButton:(HrizontalMenuButton *)btn
{
    [self changeButtonState:btn];
    [self bottomViewAnimationWithButton:btn];
    [self updateScrollViewOffsetWithButton:btn];
    
}


- (void)changeButtonState:(HrizontalMenuButton *)btn
{
    for (HrizontalMenuButton * temp in _btnArray)
    {
        if (btn.tag == temp.tag)
        {
            [temp.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
            [temp setTitleColor:[UIColor colorWithHexString:@"eb4f38"] forState:UIControlStateNormal];
        }else
        {
            [temp.titleLabel setFont:[UIFont systemFontOfSize:13.0f]];
            [temp setTitleColor:[UIColor colorWithHexString:@"5f5f5f"] forState:UIControlStateNormal];
        }
    }
    
}


- (void)bottomViewAnimationWithButton:(HrizontalMenuButton *)btn
{
    selectButton = btn;
    
    CGPoint   point = btn.frame.origin;
    
    float  with = [btn.titleLabel.text boundingRectWithSize:CGSizeMake(1000, 20) font:btn.titleLabel.font].width;
    
    _lineView.hidden = NO;
    [UIView animateWithDuration:.2 animations:^{
        _lineView.frame = CGRectMake(point.x, 35, with, 2);
        
    }];
}



- (void)updateScrollViewOffsetWithButton:(UIButton *)btn
{
    float xx = _scrollView.frameWidth * (btn.tag - 1) * (MENU_BUTTON_WIDTH / _scrollView.frameWidth) - MENU_BUTTON_WIDTH;
    [_scrollView scrollRectToVisible:CGRectMake(xx, 0, _scrollView.frameWidth, _scrollView.frameHeight) animated:YES];
}

@end




@implementation HrizontalMenuButton



@end