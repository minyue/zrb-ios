//
//  NewsHomeParamView.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/1.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListModel.h"
@interface NewsHomeParamView : UIView
@property (nonatomic,assign) BOOL needAddBtn;
- (void)initWithModel:(NewsItemModel *)model;
@end


@interface NewsHomeCollectionView : UIView


- (void)initWithModel:(NewsItemModel *)model;
@end
