//
//  NewsHomeParamView.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/1.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsHomeParamView.h"
#import "NSDate+Addition.h"
#import "NSString+WPAttributedMarkup.h"
@interface NewsHomeParamView()
{
    UILabel   *_timeLabel;
    
    UILabel   *_channelLabel;   //来源
    
    UIButton  *_operationButton;

    
    NewsHomeCollectionView *_collectionView;
}

@end

@implementation NewsHomeParamView

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
    
    
    _channelLabel = [[UILabel alloc] init];
    _channelLabel.backgroundColor = [UIColor clearColor];
    _channelLabel.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:_channelLabel];
    
    [_channelLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.leading.equalTo(self.mas_leading);
    }];

    
    
    
    _timeLabel = [[UILabel alloc] init];
    _timeLabel.backgroundColor = [UIColor clearColor];
    _timeLabel.textColor = [UIColor colorWithHexString:@"969696"];
    _timeLabel.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:_timeLabel ];
    
    [_timeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.leading.equalTo(_channelLabel.mas_right).offset(5);
    }];
    
    
//    if(self.needAddBtn){
        _operationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_operationButton setBackgroundImage:[UIImage imageNamed:@"news_list_follow_add"] forState:UIControlStateNormal];
        [_operationButton addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        _operationButton.enabled = YES;
        [self addSubview:_operationButton];
        
        [_operationButton makeConstraints:^(MASConstraintMaker *make) {
            // make.leading.equalTo(_timeLabel.mas_right).offset(5);
            make.centerY.equalTo(self);
            make.size.equalTo(CGSizeMake(8, 8));
        }];
        [_operationButton setEnlargeEdgeWithTop:20 right:20 bottom:10 left:20];
//    }

    
    
    _collectionView = [[NewsHomeCollectionView alloc] init];
    _collectionView.backgroundColor = [UIColor clearColor];
    [self addSubview:_collectionView];
    [_collectionView makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_operationButton.mas_right).offset(2);
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-2);
    }];
}

- (void)initWithModel:(NewsItemModel *)model
{
    NSString *timeStr = [NSDate timeFormatWithTimeStamp:model.createTime format:NSATE_FORMAT_COUPON_6];
    _timeLabel.text = timeStr;
    
    
    [self setChannelAndHotWithModel:model];
    [_collectionView initWithModel:model];
    
    _operationButton.hidden = !_needAddBtn;
}


- (void)setChannelAndHotWithModel:(NewsItemModel *)model
{
    if ([ZRBUtilities isBlankString:model.hotPointId]) {
        
        //判断是否是同一天
        NSDate *nowDate = [NSDate dateWithTimeIntervalSince1970:(model.createTime.doubleValue / 1000)];
        
        NSDate *createDate = [NSDate date];
        
        if ([nowDate isSameDay:createDate]) {
             _channelLabel.attributedText = [self contentWithChannel:model.channel hot:@"" new:@"最新"];
            
        }else
        {
            _channelLabel.text = model.channel;
            _channelLabel.textColor = [UIColor colorWithHexString:@"969696"];
        }
        
        
    }else
    {
        _channelLabel.attributedText = [self contentWithChannel:model.channel hot:@"热点" new:@""];
    }
}


- (NSAttributedString *)contentWithChannel:(NSString *)channel  hot:(NSString *)hot new:(NSString *)new
{
    channel = [channel stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (channel.length > 5) {
        channel = [channel substringToIndex:5];
    }
    
    NSAttributedString *string = nil;
    
    NSDictionary *style = @{@"channel":@[[UIColor colorWithHexString:@"969696"],[UIFont systemFontOfSize:11.0f]],
                            @"hot":@[[UIColor colorWithHexString:@"eb4f38"],[UIFont systemFontOfSize:11.0f]],
                            @"new":@[[UIColor colorWithHexString:@"56abe4"],[UIFont systemFontOfSize:11.0f]]};
    
    NSMutableString *mstring = [[NSMutableString alloc] init];
    
    NSString *contentStr = [NSString stringWithFormat:@"<new>%@</new><hot>%@ | </hot><channel>%@</channel>",new,hot,channel];
    
    [mstring appendString:contentStr];
    
    string = [mstring attributedStringWithStyleBook:style];
    return string;
    
}



- (void)test:(id)sender
{


}
@end



//首页Cell中得关注小视图
@interface NewsHomeCollectionView()
{
    
    UILabel   *_titleLabel;
}



@end


@implementation NewsHomeCollectionView


- (instancetype)init{

    if (self = [super init]) {
        [self setup];
    }
    return self;
}


- (void)setup
{
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor colorWithHexString:@"969696"];
    _titleLabel.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:_titleLabel];
    [_titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.mas_leading).offset(5);
        make.centerY.equalTo(self);
        make.right.equalTo(self.mas_right);
    }];
}

- (void)initWithModel:(NewsItemModel *)model
{
    _titleLabel.text = [NSString stringWithFormat:@"%d关注",model.collectionCount];

}

@end

