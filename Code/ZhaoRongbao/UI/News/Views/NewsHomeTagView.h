//
//  NewsHomeTagView.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/1.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListModel.h"
@interface NewsHomeTagView : UIView

@property (nonatomic,copy)void (^tagItemClickBlock)(NewsTagModel *model);;
- (void)initWithData:(NSMutableArray *)array;
@end



@interface TagItemView : UIView

@property (nonatomic,copy) void (^tagItemClickBlock)(NewsTagModel *model);

@property (nonatomic,assign)CGFloat labelWith;


- (void)initWithModel:(NewsTagModel *)model;

@end