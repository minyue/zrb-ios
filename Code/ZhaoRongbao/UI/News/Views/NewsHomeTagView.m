//
//  NewsHomeTagView.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/1.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsHomeTagView.h"
#import "NSString+Extending.h"
@implementation NewsHomeTagView

- (void)awakeFromNib
{

    [self setup];
}


- (instancetype)init{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}


- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
}



- (void)initWithData:(NSMutableArray *)array
{
    NSArray *countArray = array.count>3 ? [array subarrayWithRange:NSMakeRange(0, 3)]:array;

    TagItemView *lastView = nil;
    
    UIView *superView = self;
    
    [self clearLastSubViews];
    for (int i = 0; i < countArray.count; i ++)
    {
        WS(bself);
        TagItemView *itemView = [[TagItemView alloc] init];
        [itemView   initWithModel:countArray[i]];
        itemView.tagItemClickBlock = ^(NewsTagModel *model)
        {
            [bself tagClickWithModel:model];
        };
        [self addSubview:itemView];
        
        [itemView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(superView.mas_top);
            if (!lastView) {
                make.leading.equalTo(superView.mas_leading);

            }else
            {
                make.leading.equalTo(lastView.mas_right).offset(10);
            }
        }];
        
        lastView = itemView ;
    }
    
}


- (void)tagClickWithModel:(NewsTagModel *)model
{

    if (self.tagItemClickBlock) {
        self.tagItemClickBlock(model);
    }
}


- (void)clearLastSubViews
{
    for (TagItemView *view in self.subviews) {
        [view removeFromSuperview];
    }

}


@end



@interface TagItemView()
{
    UILabel *_titleLabel;
    
    UITapGestureRecognizer *_tapRecognizer;
    
    NewsTagModel        *_tagModel;
}


@end

@implementation TagItemView


- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
        [self addTapRecognizer];
    }
    return self;
}

- (instancetype)init{
    if (self = [super init]) {
        [self setup];
        [self addTapRecognizer];
    }
    return self;
}


- (void)setup
{
    self.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
    self.layer.cornerRadius = 10;
    self.layer.masksToBounds = YES;

    _titleLabel = [[UILabel alloc] init];
    _titleLabel.textColor = [UIColor colorWithHexString:@"5f5f5f"];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:_titleLabel];
    
    WS(bself);
    [_titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(bself.mas_leading).offset(8);
        make.top.equalTo(bself.mas_top).offset(3);
        make.right.equalTo(bself.mas_right).offset(-8);
        make.bottom.equalTo(bself.mas_bottom).offset(-3);
    }];
}



- (void)addTapRecognizer
{

    _tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchClick:)];
    [self addGestureRecognizer:_tapRecognizer];

}


- (void)removeTapRecognizer
{
    if (_tapRecognizer) {
        [self removeGestureRecognizer:_tapRecognizer];
    }
}


- (void)initWithModel:(NewsTagModel *)model
{
    _titleLabel.text = model.labelName;
    if (model) {
        _tagModel = model;
    }
}



- (void)touchClick:(id)sender
{
    
    if (self.tagItemClickBlock) {
        self.tagItemClickBlock(_tagModel);
    }
}


- (void)dealloc
{
    [self removeTapRecognizer];
}
@end