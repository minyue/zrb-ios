//
//  NewsHomeViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListModel.h"
#import "NewsHomeTagView.h"

@interface NewsHomeViewCell : UITableViewCell

@property (nonatomic,copy)void (^tagItemClickBlock)(NewsTagModel *model);
@property (nonatomic,strong) IBOutlet  NewsHomeTagView  *tagsView;


- (void)initWithModel:(NewsItemModel *)model andIfNeedBtn:(BOOL)need;
@end
