//
//  NewsHomeViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsHomeViewCell.h"
#import "NewsHomeParamView.h"
#import "NewsConst.h"

@interface NewsHomeViewCell()
{
    IBOutlet  UIImageView    *_imageView;
    
    IBOutlet  UILabel        *_titleLabel;
    
    IBOutlet  UILabel        *_contentLabel;
    
    IBOutlet  NewsHomeParamView *_paramView;
    
    UIImageView               *_iconView;
    
    UILabel                   *_iconLabel;
    
    ZRB_LineView             *_lineView;
    
    
}


- (void)setup;
@end

@implementation NewsHomeViewCell

- (void)awakeFromNib {
    [self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setup
{
    self.backgroundColor  = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _imageView.contentScaleFactor = [[UIScreen mainScreen] scale];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    _imageView.backgroundColor = [UIColor colorWithHexString:@"e4e4e4"];

    
    _iconView = [[UIImageView alloc] init];
    _iconView.backgroundColor = [UIColor clearColor];
    [_imageView addSubview:_iconView];
    [_iconView makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.top.equalTo(_imageView).offset(5);
        make.size.equalTo(CGSizeMake(14, 14));
    }];
    
    
    _iconLabel = [[UILabel alloc] init];
    _iconLabel.backgroundColor = [UIColor clearColor];
    _iconLabel.font = [UIFont systemFontOfSize:12.0f];
    _iconLabel.textColor = [UIColor whiteColor];
    [_imageView addSubview:_iconLabel];
    [_iconLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_iconView.mas_centerY);
        make.leading.equalTo(_iconView.mas_right).offset(5);
    }];
    WS(bself);
    _tagsView.tagItemClickBlock = ^(NewsTagModel *model)
    {
        if (bself.tagItemClickBlock) {
            bself.tagItemClickBlock(model);
        }
    };
        
    _lineView = [[ZRB_LineView alloc] init];
    [self.contentView addSubview:_lineView];
    
    UIView *superView = self.contentView;
    [_lineView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.leading.equalTo(superView.mas_leading).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.height.equalTo(@0.5);
    }];
    
    
    [superView bringSubviewToFront:_paramView];
}

- (void)initWithModel:(NewsItemModel *)model andIfNeedBtn:(BOOL)need
{
    [_imageView sd_setImageWithURL:[ZRBUtilities urlWithPath:model.imgPath] placeholderImage:[ZRBUtilities ZRB_DefaultImage] options:SDWebImageRetryFailed];
    _iconView.image = [self iconImageWithType:model.category];
    _iconLabel.text = [self iconTitleWithType:model.category];
    
    [self updateTitleAndSummaryWithObj:model];
    [_tagsView  initWithData:model.tagsInfoList];
    _paramView.needAddBtn = need;
    [_paramView initWithModel:model];
    
}


//更新标题和摘要
- (void)updateTitleAndSummaryWithObj:(NewsItemModel *)model
{
    //没有摘要显示大标题，并且显示两行
    if ([ZRBUtilities isBlankString:model.summary])
    {
        _titleLabel.numberOfLines = 2;
        _titleLabel.text = model.title;
        _contentLabel.text = nil;
    }else
    {
        _titleLabel.numberOfLines = 1;
        _titleLabel.text = model.shortTitle;
        _contentLabel.text = model.summary;
    }

}



- (UIImage *)iconImageWithType:(NewsCategoryType)type
{

    switch (type) {
        case NewsHotType:
        {
            return [UIImage imageNamed:@"news_photo_hotspot"];
            break;

        }
        case NewsViewPointType:
        {
            return [UIImage imageNamed:@"news_photo_View"];
            break;
        }
        case NewsPoliciesType:
        {
            return [UIImage imageNamed:@"news_photo_olicy"];
            break;
        }
        case NewsInvestmentType:
        {
            return [UIImage imageNamed:@"news_photo_invest"];
            break;
        }
            
        default:
            break;
    }
}



- (NSString *)iconTitleWithType:(NewsCategoryType)type
{
    switch (type)
    {
        case NewsHotType:
        {
            return @"新闻热点";
            break;
        }
        case NewsViewPointType:
        {
            return @"观点解读";
            break;
        }
        case NewsPoliciesType:
        {
            return @"政策法规";
            break;
        }
        case NewsInvestmentType:
        {
            return @"投资动态";
            break;
        }
        default:
            break;
    }
}


@end
