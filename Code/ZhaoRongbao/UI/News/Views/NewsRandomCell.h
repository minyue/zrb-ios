//
//  NewsRandomCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListModel.h"
@interface NewsRandomCell : UITableViewCell


- (void)initWithData:(NewsRandomItem *)randomItem indexPath:(NSIndexPath *)path;
@end
