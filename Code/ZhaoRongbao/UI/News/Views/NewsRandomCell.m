//
//  NewsRandomCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsRandomCell.h"

@interface NewsRandomCell()
{
   IBOutlet UILabel   *_titleLabel;
}

@end
@implementation NewsRandomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)initWithData:(NewsRandomItem *)randomItem indexPath:(NSIndexPath *)path
{
    _titleLabel.text = [NSString stringWithFormat:@"%ld.  %@",(long)path.row + 1,randomItem.title];
    
    if ((path.row % 2)== 0) {
        self.backgroundColor = [UIColor colorWithHexString:@"f9f9f9"];
    }else
    {
        self.backgroundColor = [UIColor colorWithHexString:@"ffffff"];

    }

}
@end
