//
//  NewsSearchListCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/7.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsListModel.h"
@interface NewsSearchListCell : UITableViewCell


- (void)initWithModel:(NewsItemModel *)model;

@end
