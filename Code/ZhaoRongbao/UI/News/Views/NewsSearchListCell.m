//
//  NewsSearchListCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/7.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "NewsSearchListCell.h"
#import "NewsHomeTagView.h"
#import "NewsHomeParamView.h"

#import "NewsConst.h"

@interface NewsSearchListCell()
{
    IBOutlet  UILabel        *_titleLabel;
    
    IBOutlet  NewsHomeTagView  *_tagsView;
    
    IBOutlet  NewsHomeParamView *_paramView;
    
    ZRB_LineView             *_lineView;
}


- (void)setup;
@end

@implementation NewsSearchListCell

- (void)awakeFromNib {
    [self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

- (void)setup
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _lineView = [[ZRB_LineView alloc] init];
    [self.contentView addSubview:_lineView];
    
    UIView *superView = self.contentView;
    [_lineView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.leading.equalTo(superView.mas_leading).offset(10);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.height.equalTo(@0.5);
    }];
    
    
    [superView bringSubviewToFront:_paramView];
}

- (void)initWithModel:(NewsItemModel *)model
{
    
    [self updateTitleAndSummaryWithObj:model];
    [_tagsView  initWithData:model.tagsInfoList];
    [_paramView initWithModel:model];
    
}


//更新标题和摘要
- (void)updateTitleAndSummaryWithObj:(NewsItemModel *)model
{
    //没有摘要显示大标题，并且显示两行
    if ([ZRBUtilities isBlankString:model.summary])
    {
        _titleLabel.numberOfLines = 2;
        _titleLabel.text = model.title;
    }else
    {
        _titleLabel.numberOfLines = 1;
        _titleLabel.text = model.shortTitle;
    }

}

@end
