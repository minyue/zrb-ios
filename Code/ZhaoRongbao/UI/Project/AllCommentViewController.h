//
//  AllCommentViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/18.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "MJRefresh.h"
#import "AllCommentCell.h"
#import "ProjectService.h"
#import "AllCommentModel.h"
#import "DeleteCommentModel.h"
#import "WriteCommentViewController.h"

@interface AllCommentViewController : ZRB_ViewControllerWithBackButton<UITableViewDataSource,UITableViewDelegate>
{
    NSIndexPath *_deleteIndexPath;                              //删除评论的位置
    BOOL  _isRequst;                                            //是否请求过
    
    ProjectService *_projectService;
    UITableView *_commentTableView;
}
@property (nonatomic,assign) int currentCommentId;
@property (nonatomic,strong)NSString *projectID;                //项目ID
@property (nonatomic,strong)NSMutableArray *commentArray;       //项目评论模型数组

@end
