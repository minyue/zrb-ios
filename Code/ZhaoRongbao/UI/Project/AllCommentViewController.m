//
//  AllCommentViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/18.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "AllCommentViewController.h"

@interface AllCommentViewController ()

@end

@implementation AllCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setup];
    
    [self refreshData];
}

- (void)setup{
    self.title = @"所有评论";
    
    _commentArray = [[NSMutableArray alloc]init];
    _projectService = [[ProjectService alloc]init];
   
     _commentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -64)];
     _commentTableView.delegate = self;
     _commentTableView.dataSource = self;
     _commentTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [_commentTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    [self addFooterView];
    [self addHeaderView];
    
    [self.view addSubview:_commentTableView];
}


- (void)refreshData{
    if (!_isRequst) {
        [_commentTableView.header beginRefreshing];
    }
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _commentTableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryProjectComment];
    }];
    
}


/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _commentTableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreComment];
    }];
}

#pragma delegate of UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _commentArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ProjectCell";
    AllCommentCell *cell = (AllCommentCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[AllCommentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    AllCommentItemModel *model = [_commentArray objectAtIndex:indexPath.row];
    [cell initWithModel:model];
    
    cell.deleteBtn.tag = indexPath.row;
    cell.replyBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(deleteComment:) forControlEvents:UIControlEventTouchUpInside];
    [cell.replyBtn addTarget:self action:@selector(replyComment:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}




/**
 *  删除评论
 */
- (void)deleteComment:(id)sender{
    UIButton *btn = (UIButton *)sender;
    _deleteIndexPath = [NSIndexPath indexPathForItem:btn.tag inSection:0];
    
    AllCommentItemModel *model = [_commentArray objectAtIndex:btn.tag];
    [self deleteCommentWithModel:model];
}

/**
 * 回复评论
 */
- (void)replyComment:(id)sender{
    UIButton *btn = (UIButton *)sender;
    AllCommentItemModel *model = [_commentArray objectAtIndex:btn.tag];
    
    WriteCommentViewController *writeCtrl = [[WriteCommentViewController alloc]init];
    writeCtrl.projectID = model.pid;
    writeCtrl.otherUsername = model.targetName;
    [self.navigationController pushViewController:writeCtrl animated:YES];
}

#pragma 网络请求

/**
 *  下拉刷新
 */
- (void)queryProjectComment{
    _isRequst = NO;
    
    ProjectAllCommentRequest *request = [[ProjectAllCommentRequest alloc]init];
    request.pid = self.projectID;
    request.limit = 10;
    request.maxCommentId = _currentCommentId?(_currentCommentId + 1): -1;
    
    WS(bself);
    [_projectService projectAllCommnetWithRequest:request success:^(id responseObject) {
        [bself queryProjectCommentCallBackWithObject:responseObject];
        
    } failture:^(NSError *error) {
        WS(bself);
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryProjectComment];
        }];
    }];
}

/**
 *  上拉加载更多
 */
- (void)loadMoreComment{
    AllCommentItemModel *model = [_commentArray lastObject];
    
    ProjectAllCommentRequest *request = [[ProjectAllCommentRequest alloc]init];
    request.pid = self.projectID;
    request.limit = 10;
    request.maxCommentId = model.cid;
    
    LogInfo(@"最大评论ID:%d",model.cid);
    LogInfo(@"项目ID:%d",self.projectID);
    
    WS(bself);
    [_projectService projectAllCommnetWithRequest:request success:^(id responseObject) {
        [bself loadMoreCallBackWithObject:responseObject];
        
    } failture:^(NSError *error) {
        
    }];
}
/**
 * 删除评论请求
 */
- (void)deleteCommentWithModel:(AllCommentItemModel *)model{
    
    DeleteProjectCommentRequest *request = [[DeleteProjectCommentRequest alloc]init];
    request.cid = model.cid;
    request.pid = model.pid;
    
    WS(bself);
    [_projectService deleteProjectCommentWithRequest:request success:^(id responseObject) {
        [bself commentDeleteCallBackWithObject:responseObject];
    } failture:^(NSError *error) {
        
    }];
    
}

#pragma 网络请求回调
/**
 *  下拉刷新回调
 */
- (void)queryProjectCommentCallBackWithObject:(AllCommentModel *)model{
    _isRequst = YES;
    LogInfo(@"请求结果：%d",model.res);
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            _commentArray = model.data;
            [_commentTableView reloadData];
            
            break;
        }
        case ZRBHttpFailType:
        {
            WS(bself);
            [HUDManager showNonNetWorkHUDInView:self.view event:^{
                [bself queryProjectComment];
            }];
            LogInfo(@"请求失败");
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
        default:
            break;
    }
    [_commentTableView.header endRefreshing];
}

/**
 *  上拉加载更多回调
 */
- (void)loadMoreCallBackWithObject:(AllCommentModel *)model{
    LogInfo(@"加载更多请求标志：%d",model.res);
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            [_commentArray addObjectsFromArray:model.data];
            [_commentTableView reloadData];
            
            if (model.data.count == 0) {
                LogInfo(@"已经加载完毕，需要处理");
            }else
            {
                
            }
           
            break;
        }
        case ZRBHttpFailType:
        {
            
            break;
        }
        case ZRBHttpNoLoginType:
        {
            
            break;
        }
        default:
            break;
    }

     [_commentTableView.footer endRefreshing];
}

/**
 *  删除评论回调
 */
- (void)commentDeleteCallBackWithObject:(DeleteCommentModel *)model{
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            [_commentArray removeObjectAtIndex:_deleteIndexPath.row];
            [_commentTableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:_deleteIndexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
            
            //刷新是因为cell数量改变，cell中按钮的tag变化
            [_commentTableView reloadData];
            break;
        }
        case ZRBHttpFailType:
            
            break;
        case ZRBHttpNoLoginType:
            
            break;
        default:
            break;
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
