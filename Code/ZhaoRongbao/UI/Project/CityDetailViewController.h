//
//  CityDetailViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/27.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "CityTagView.h"
#import "CityDetailView.h"
#import "PieChartView.h"
#import "UUChart.h"
#import "IndustryDetailViewController.h"
#import "ProjectService.h"
#import "CityDetailModel.h"
#import "CityEnomicModel.h"

@interface CityDetailViewController : ZRB_ViewControllerWithBackButton<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,PieChartViewDelegate,PieChartViewDataSource,UUChartDataSource>
{
    UITableView *_cityTableView;
    
    UIScrollView *_mScrollView;
    
    UIView *_navView;                       //自定义导航栏视图
    UIView *_topView;
    UIView *_cityDetailView;                //城市详情视图
    UIView *_industryDistributeView;        //产业分布视图
    UIView *_economyView;                   //经济发展视图
    
    UIView *_menuView;
    UIView *_menuBackGroundView;
    
    UISegmentedControl *_changeCtrl;
    UUChart *_chartView;
    
    ProjectService *_projectService;
    CityDetailModel *_model;
}

@property (nonatomic,strong)NSString *postCode;

@end
