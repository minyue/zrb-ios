//
//  CityDetailViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/27.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "CityDetailViewController.h"

@implementation CityDetailViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
     self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self setup];
    
    [self getCityDetail:self.postCode];
}

- (void)setup{
    
    self.postCode = @"421182";
    _projectService = [[ProjectService alloc]init];
    
    [self initNaviView];
    
    
    _cityTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64) style:UITableViewStylePlain];
    _cityTableView.delegate = self;
    _cityTableView.dataSource = self;
    _cityTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_cityTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
     [(UIScrollView *)[[_cityTableView subviews] objectAtIndex:0] setBounces:NO];
//    [self.view addSubview:_cityTableView];
//    [self.view insertSubview:_cityTableView belowSubview:_navView];
    
    
    _mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
    _mScrollView.delegate = self;
    _mScrollView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    _mScrollView.showsVerticalScrollIndicator =YES; //垂直方向的滚动指示
    _mScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;//滚动指示的风格
    _mScrollView.bounces = NO;
    
    [self.view addSubview:_mScrollView];
    
//    [self initTopView];
//    [self initCityDetailView];
//    [self initIndustryDistribute];
//    [self initEconomyView];
    
}

/**
 *  自定义导航栏
 */
- (void)initNaviView{
    
    _navView = [[UIView alloc]init];
    _navView.backgroundColor = [UIColor colorWithHexString:@"0077d9"];
    [self.view addSubview:_navView];
    
    //返回按钮
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setEnlargeEdgeWithTop:22 right:0 bottom:22 left:0];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"my_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(navBack:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];
    
    [_navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(@64);
    }];
    
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_navView.mas_left).offset(10);
        make.centerY.equalTo(_navView.mas_centerY).offset(10);
        make.width.equalTo(@25);
        make.height.equalTo(@10);
    }];
}

/**
 *  初始化顶部视图
 */
- (void)initTopView:(CityDetailModel *)model{
    
    //顶部视图
    _topView = [[UIView alloc]init];
    _topView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:_topView];
    
    
    UIView *cityView = [[UIView alloc]init];
    cityView.backgroundColor = [UIColor colorWithHexString:@"0077d9"];
    [_topView addSubview:cityView];
    
    //地区
    UILabel *cityLB = [[UILabel alloc]init];
    cityLB.backgroundColor = [UIColor clearColor];
    cityLB.text = [NSString stringWithFormat:@"%@·%@",model.province ,model.city];
    cityLB.font = [UIFont boldSystemFontOfSize:24.0f];
    cityLB.textColor = [UIColor whiteColor];
    [cityLB sizeToFit];
    [cityView addSubview:cityLB];
    
    //标签
    NSArray *titleArray = [model.keyWords componentsSeparatedByString:@"#"];
    CityTagView *tagView = [[CityTagView alloc]init];
    [tagView initWithData:titleArray];
    [cityView addSubview:tagView];
    
    //GDP总值
    UILabel *GDP = [[UILabel alloc]init];
    GDP.font = [UIFont systemFontOfSize:18.0f];
    //创建 NSMutableAttributedString
    NSString *titleStr = [NSString stringWithFormat:@"GDP总值%@亿",model.gdp];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
    //设置“选择项目类型”文字颜色
    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5f5f5f"] range: NSMakeRange(0,5)];
    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5f5f5f"] range: NSMakeRange(titleStr.length-1,1)];
    //设置“数字”文字颜色
    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"1579d6"] range: NSMakeRange(5,model.gdp.length)];
    GDP.attributedText = attributedStr;
    [_topView addSubview:GDP];
    
    //菜单按钮
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.backgroundColor = [UIColor clearColor];
    [menuBtn setBackgroundImage:[UIImage imageNamed:@"GDPmore"] forState:UIControlStateNormal];
    [menuBtn addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:menuBtn];
    
    //人口
    UILabel *peopleLB = [[UILabel alloc]init];
    peopleLB.backgroundColor = [UIColor clearColor];
    peopleLB.font = [UIFont systemFontOfSize:12.0f];
    [_topView addSubview:peopleLB];
    
    NSString *peopleStr = [NSString stringWithFormat:@"人口：%@万",model.pcdi];
    NSMutableAttributedString *peopleAttributedStr = [[NSMutableAttributedString alloc] initWithString:peopleStr];
    [peopleAttributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5f5f5f"] range: NSMakeRange(0,3)];
    [peopleAttributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5f5f5f"] range: NSMakeRange(peopleStr.length-1,1)];
    [peopleAttributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"1579d6"] range: NSMakeRange(3,model.pcdi.length)];
    peopleLB.attributedText = peopleAttributedStr;
    
    
    //人均年收入
    UILabel *incomeLB = [[UILabel alloc]init];
    incomeLB.backgroundColor = [UIColor clearColor];
    incomeLB.font = [UIFont systemFontOfSize:12.0f];
    [_topView addSubview:incomeLB];
    
    NSString *incomeStr = [NSString stringWithFormat:@"人均年收入：%@元",model.population];
    NSMutableAttributedString *incomeAttributedStr = [[NSMutableAttributedString alloc] initWithString:incomeStr];
    [incomeAttributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5f5f5f"] range: NSMakeRange(0,6)];
    [incomeAttributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5f5f5f"] range: NSMakeRange(incomeStr.length-1,1)];
    [incomeAttributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"1579d6"] range: NSMakeRange(6,model.population.length)];
    incomeLB.attributedText = incomeAttributedStr;
    
    
    //城镇化
    UILabel *cityAndTownLB = [[UILabel alloc]init];
    cityAndTownLB.backgroundColor = [UIColor clearColor];
    cityAndTownLB.font = [UIFont systemFontOfSize:12.0f];
    [_topView addSubview:cityAndTownLB];
    
    NSString *townStr = @"城镇化率：60%";
    NSMutableAttributedString *townAttributedStr = [[NSMutableAttributedString alloc] initWithString:townStr];
    [townAttributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"5f5f5f"] range: NSMakeRange(0,5)];
    [townAttributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"1579d6"] range: NSMakeRange(5,3)];
    cityAndTownLB.attributedText = townAttributedStr;
    
    [_topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_mScrollView.mas_left);
        make.top.equalTo(_mScrollView.mas_top);
        make.width.equalTo(_mScrollView.mas_width);
        make.height.equalTo(@150);
        
    }];
    
    [cityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_topView.mas_left);
        make.top.equalTo(_topView.mas_top);
        make.width.equalTo(_topView.mas_width);
        make.height.equalTo(@90);
    }];
    
    [cityLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(cityView.mas_centerX);
        make.centerY.equalTo(cityView.mas_centerY).offset(-32);
    }];
    
    [tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(cityView.mas_centerX);
        make.top.equalTo(cityLB.mas_bottom).offset(10);
    }];
    
    [GDP mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cityView.mas_bottom).offset(10);
        make.left.equalTo(_mScrollView.mas_left).offset(10);
    }];
    
    [menuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(GDP.mas_centerY);
        make.right.equalTo(_topView.mas_right).offset(-5);
        make.width.equalTo(@16);
        make.height.equalTo(@16);
    }];
    
    [peopleLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_topView.mas_left).offset(10);
        make.bottom.equalTo(_topView.mas_bottom).offset(-10);
    }];
    
    [incomeLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(peopleLB.mas_centerY);
        make.left.equalTo(peopleLB.mas_right).offset(15);
    }];
    
    [cityAndTownLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(incomeLB.mas_centerY);
        make.left.equalTo(incomeLB.mas_right).offset(15);
    }];
}

/**
 *  城市详情视图
 */
- (void)initCityDetailView:(CityDetailModel *)model{
    
    _cityDetailView = [[UIView alloc]init];
    _cityDetailView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:_cityDetailView];
    
    //图标
    UIImageView *icon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cityDetail"]];
    icon.backgroundColor = [UIColor clearColor];
    [_cityDetailView addSubview:icon];
    
    //标题
    UILabel *title = [[UILabel alloc]init];
    title.font = [UIFont systemFontOfSize:13.0f];
    title.textColor = [UIColor colorWithHexString:@"7f7f7f"];
    title.text = @"城市详情";
    [_cityDetailView addSubview:title];
    
    //分割线
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"f7f7f7"];
    [_cityDetailView addSubview:line];
    
    //详细内容
    NSString *detail = model.intro;
    CityDetailView *contentView = [[CityDetailView alloc]init];
    [contentView setCityDetail:detail];
    [_cityDetailView addSubview:contentView];
    
    
    [_cityDetailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_mScrollView.mas_width);
        make.left.equalTo(_mScrollView.mas_left);
        make.top.equalTo(_topView.mas_bottom).offset(10);
    }];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_cityDetailView.mas_left).offset(10);
        make.top.equalTo(_cityDetailView.mas_top).offset(10);
        make.width.equalTo(@20);
        make.height.equalTo(@20);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right).offset(10);
        make.centerY.equalTo(icon.mas_centerY);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_cityDetailView.mas_width);
        make.left.equalTo(_cityDetailView.mas_left);
        make.top.equalTo(icon.mas_bottom).offset(10);
        make.height.equalTo(@1);
    }];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_cityDetailView.mas_left).offset(10);
        make.right.equalTo(_cityDetailView.mas_right).offset(-10);
        make.top.equalTo(line.mas_bottom).offset(10);
        make.bottom.equalTo(_cityDetailView.mas_bottom).offset(-10);
        
    }];
}

/**
 *  初始化产业分布视图
 */
- (void)initIndustryDistribute{
    
    _industryDistributeView = [[UIView alloc]init];
    _industryDistributeView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:_industryDistributeView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    [btn addTarget:self action:@selector(industryDistribute:) forControlEvents:UIControlEventTouchUpInside];
    [_industryDistributeView addSubview:btn];
    
    //图标
    UIImageView *icon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"IndustryDistribute"]];
    icon.backgroundColor = [UIColor clearColor];
    [_industryDistributeView addSubview:icon];
    
    //标题
    UILabel *title = [[UILabel alloc]init];
    title.font = [UIFont systemFontOfSize:13.0f];
    title.textColor = [UIColor colorWithHexString:@"7f7f7f"];
    title.text = @"产业分布";
    [_industryDistributeView addSubview:title];
    
    //分割线
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
    [_industryDistributeView addSubview:line];
    
    //环状统计图
    PieChartView *mPieChartView = [[PieChartView alloc]init];
    mPieChartView.delegate = self;
    mPieChartView.datasource = self;
    [_industryDistributeView addSubview:mPieChartView];
    
    [_industryDistributeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_mScrollView.mas_width);
        make.left.equalTo(_mScrollView.mas_left);
        make.top.equalTo(_cityDetailView.mas_bottom).offset(10);
        make.height.equalTo(@210);
    }];
    
    UIView *valueView = [[UIView alloc]init];
    NSMutableArray *titleArray = [[NSMutableArray alloc]initWithObjects:@"第一产业",@"第二产业",@"第三产业", nil];
    NSMutableArray *colorArray = [[NSMutableArray alloc]initWithObjects:@"5fe18a",@"f8ba81",@"3698fb", nil];
    
    for (int i = 0; i < titleArray.count; i++) {
        UILabel *icon = [[UILabel alloc]init];
        icon.backgroundColor = [UIColor colorWithHexString:colorArray[i]];
        [valueView addSubview:icon];
        
        UILabel *title = [[UILabel alloc]init];
        title.backgroundColor = [UIColor clearColor];
        title.font = [UIFont systemFontOfSize:11.0f];
        title.text = titleArray[i];
        [valueView addSubview:title];
        
        [icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@15);
            make.height.equalTo(@15);
            make.left.equalTo(valueView.mas_left);
            make.top.equalTo(valueView.mas_top).offset(20*i);
            
            if (i == titleArray.count -1) {
                make.bottom.equalTo(valueView.mas_bottom);
            }
        }];
        
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(icon.mas_right).offset(5);
            make.centerY.equalTo(icon.mas_centerY);
             make.right.equalTo(valueView.mas_right);
        }];
    }
    [mPieChartView addSubview:valueView];
    
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_industryDistributeView.mas_left).offset(10);
        make.top.equalTo(_industryDistributeView.mas_top).offset(10);
        make.width.equalTo(@20);
        make.height.equalTo(@20);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right).offset(10);
        make.centerY.equalTo(icon.mas_centerY);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_industryDistributeView.mas_width);
        make.left.equalTo(_industryDistributeView.mas_left);
        make.top.equalTo(icon.mas_bottom).offset(10);
        make.height.equalTo(@1);
    }];
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_industryDistributeView.mas_width);
        make.top.equalTo(_industryDistributeView.mas_top);
        make.left.equalTo(_industryDistributeView.mas_left);
        make.bottom.equalTo(line.mas_bottom);
    }];
    
    [mPieChartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@150);
        make.height.equalTo(@150);
        make.centerX.equalTo(_industryDistributeView.mas_centerX);
        make.top.equalTo(line.mas_bottom).offset(10);
    }];

    [valueView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(mPieChartView.mas_centerX);
        make.centerY.equalTo(mPieChartView.mas_centerY);
    }];
    
}

/**
 *  初始化经济情况视图
 */
- (void)initEconomyView{
    
    _economyView = [[UIView alloc]init];
    _economyView.backgroundColor = [UIColor whiteColor];
    [_mScrollView addSubview:_economyView];
    
    //图标
    UIImageView *icon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ecoChart"]];
    icon.backgroundColor = [UIColor clearColor];
    [_economyView addSubview:icon];
    
    //标题
    UILabel *title = [[UILabel alloc]init];
    title.font = [UIFont systemFontOfSize:13.0f];
    title.textColor = [UIColor colorWithHexString:@"7f7f7f"];
    title.text = @"经济发展";
    [_economyView addSubview:title];
    
    //分割线
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"f7f7f7"];
    [_economyView addSubview:line];
    
    //分段按钮
    _changeCtrl = [[UISegmentedControl alloc]initWithItems:@[@"GDP总量",@"GDP增速"]];
    _changeCtrl.selectedSegmentIndex = 0;
    [_changeCtrl addTarget:self action:@selector(changeSelectedItem:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_changeCtrl];
    
    //线性统计表
    _chartView = [[UUChart alloc]initwithUUChartDataFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 150) withSource:self withStyle:UUChartLineStyle];
    [_chartView showInView:_economyView];
    
    [_economyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_mScrollView.mas_width);
        make.left.equalTo(_mScrollView.mas_left);
        make.top.equalTo(_industryDistributeView.mas_bottom).offset(10);
        make.height.equalTo(@250);
        make.bottom.equalTo(_mScrollView.mas_bottom);
    }];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_economyView.mas_left).offset(10);
        make.top.equalTo(_economyView.mas_top).offset(10);
        make.width.equalTo(@20);
        make.height.equalTo(@20);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right).offset(10);
        make.centerY.equalTo(icon.mas_centerY);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_economyView.mas_width);
        make.left.equalTo(_economyView.mas_left);
        make.top.equalTo(icon.mas_bottom).offset(10);
        make.height.equalTo(@1);
    }];
    
    [_changeCtrl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_economyView.mas_centerX);
        make.top.equalTo(line.mas_bottom).offset(10);
    }];
    
    [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_changeCtrl.mas_bottom).offset(10);
    }];
    
}


- (void)navBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showMenu:(id)sender{
    LogInfo(@"菜单");
//    [self showMenuWithModel];
    [self getEconomicInfo:self.postCode];
}

- (void)changeSelectedItem:(id)sender{
    
}

/**
 *  产业分布
 */
- (void)industryDistribute:(id)sender{
    LogInfo(@"");
    IndustryDetailViewController *industryDetailCtrl = [[IndustryDetailViewController alloc]init];
    [self.navigationController pushViewController:industryDetailCtrl animated:YES];
}

- (void)initPopAnimationMenu:(CityEnomicModel *)model{
    
    _menuBackGroundView = [[UIView alloc]init];
    _menuBackGroundView.backgroundColor = [UIColor blackColor];
    _menuBackGroundView.alpha = 0.5;
    [self.view addSubview:_menuBackGroundView];
    
    _menuView = [[UIView alloc]init];
    _menuView.backgroundColor = [UIColor whiteColor];
    _menuView.layer.masksToBounds = YES;
    _menuView.layer.cornerRadius = 5.0f;
    [self.view addSubview:_menuView];
    
    [_menuBackGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(self.view.mas_height);
    }];
    
    [_menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
        make.left.equalTo(self.view.mas_left).offset(10);
        make.right.equalTo(self.view.mas_right).offset(-10);
    }];
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"popupX"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(hideMenu:) forControlEvents:UIControlEventTouchUpInside];
    [_menuView addSubview:closeBtn];
    
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@12);
        make.height.equalTo(@12);
        make.right.equalTo(_menuView.mas_right).offset(-10);
        make.top.equalTo(_menuView.mas_top).offset(10);
    }];
    
    
    for(int i = 0;i < model.data.count;i++){
        
        CityEnomicItemModel *itemModel = [model.data objectAtIndex:i];
        
        UIView *itemView = [[UIView alloc]init];
        itemView.backgroundColor = [UIColor clearColor];
        [_menuView addSubview:itemView];
        
        UILabel *name = [[UILabel alloc]init];
        name.text = itemModel.name;
        name.backgroundColor  = [UIColor clearColor];
        name.textColor = [UIColor colorWithHexString:@"969696"];
        name.font = [UIFont systemFontOfSize:12.0f];
        [itemView addSubview:name];
        
        UILabel *value = [[UILabel alloc]init];
        value.text = itemModel.value;
        value.backgroundColor  = [UIColor clearColor];
        value.textColor = [UIColor colorWithHexString:@"3e83d8"];
        value.font = [UIFont systemFontOfSize:12.0f];
        [itemView addSubview:value];
        
        [itemView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i % 2 == 0) {
                make.left.equalTo(_menuView.mas_left);
                
            }else{
                make.right.equalTo(_menuView.mas_right);
            }
            
            make.width.equalTo(@(SCREEN_WIDTH*1/2-10));
            make.height.equalTo(@30);
            make.top.equalTo(closeBtn.mas_bottom).offset(30*(i/2));
            
            if(i == model.data.count - 1){
                make.bottom.equalTo(_menuView.mas_bottom);
            }
        }];
        
      [name mas_makeConstraints:^(MASConstraintMaker *make) {
          make.left.equalTo(itemView.mas_left).offset(10);
          make.top.equalTo(itemView.mas_top);
          make.height.equalTo(itemView.mas_height);
       }];
        
       [value mas_makeConstraints:^(MASConstraintMaker *make) {
           make.right.equalTo(itemView.mas_right).offset(-10);
           make.top.equalTo(itemView.mas_top);
           make.height.equalTo(itemView.mas_height);
       }];
        
    }
}

- (void)showMenuWithModel:(CityEnomicModel *)model{

    [self initPopAnimationMenu:model];
   
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_menuView.layer addAnimation:popAnimation forKey:nil];
}

- (void)hideMenu:(id)sender{
    CAKeyframeAnimation *hideAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    hideAnimation.duration = 0.4;
    hideAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                             [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0f, 1.0f, 1.0f)],
                             [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.00f, 0.00f, 0.00f)]];
    hideAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f];
    hideAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    hideAnimation.delegate = self;
    [_menuView.layer addAnimation:hideAnimation forKey:nil];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    [_menuView removeFromSuperview];
    [_menuBackGroundView removeFromSuperview];
}

#pragma delegate of UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 10.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 135.0f;
    }else{
        return 60.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *str = @"CityCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        UIView *topView = [[UIView alloc]init];
        topView.backgroundColor = [UIColor colorWithHexString:@"0077d9"];
        [cell.contentView addSubview:topView];
        
        [topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(cell.contentView.mas_left);
            make.top.equalTo(cell.contentView.mas_top);
            make.width.equalTo(cell.contentView.mas_width);
            make.height.equalTo(@85);
        }];
    }
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
 
    float offsetY = scrollView.contentOffset.y;
    if (offsetY < 64) {
//        _navView.alpha = offsetY / 64*6;
    }
    
}

#pragma mark -    PieChartViewDelegate
-(CGFloat)centerCircleRadius
{
    return 50.0f;
}
#pragma mark - PieChartViewDataSource
-(int)numberOfSlicesInPieChartView:(PieChartView *)pieChartView
{
    return 3;
}
-(UIColor *)pieChartView:(PieChartView *)pieChartView colorForSliceAtIndex:(NSUInteger)index
{
    UIColor *color = [[UIColor alloc]init];
    switch (index) {
        case 0:
            color = [UIColor colorWithHexString:@"5fe18a"];
            break;
            
        case 1:
            color = [UIColor colorWithHexString:@"f8ba81"];
            break;
            
        case 2:
            color = [UIColor colorWithHexString:@"3698fb"];
            break;
            
        default:
            break;
    }
    return color;
}

-(double)pieChartView:(PieChartView *)pieChartView valueForSliceAtIndex:(NSUInteger)index{
    
    double value = 0.00;
    switch (index) {
        case 0:
            value = [_model.primaryIndustry doubleValue];
            break;
            
        case 1:
            value = [_model.secondIndustry doubleValue];
            break;
            
        case 2:
            value = [_model.tertiaryIndustry doubleValue];
            break;
            
        default:
            break;
    }
    return value;
}


#pragma mark - @required
//横坐标标题数组
- (NSArray *)UUChart_xLableArray:(UUChart *)chart
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (CityStatisticsModel *model in _model.cityStatistics) {
        [array addObject:model.date];
    }
    
//    NSArray *array = @[@"1995", @"1996",@"1997",@"1998",@"1999",@"2000",@"2001"];
    return array;
}
//数值多重数组
- (NSArray *)UUChart_yValueArray:(UUChart *)chart
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    for (CityStatisticsModel *model in _model.cityStatistics) {
        if (_changeCtrl.selectedSegmentIndex == 0) {
            [array addObject:model.gdp];
        }else if(_changeCtrl.selectedSegmentIndex == 1){
           [array addObject:model.gdpRate];
        }
    }

    return @[array];
}

#pragma mark - @optional
//颜色数组
- (NSArray *)UUChart_ColorArray:(UUChart *)chart
{
    return @[UUGreen,UURed,UUBrown];
}
//显示数值范围
- (CGRange)UUChartChooseRangeInLineChart:(UUChart *)chart
{
    if (_changeCtrl.selectedSegmentIndex == 0) {
        return CGRangeMake(10*1000, 0);
    }else {
        return CGRangeMake(-10, -10);
    }
}

#pragma mark 折线图专享功能

//标记数值区域
- (CGRange)UUChartMarkRangeInLineChart:(UUChart *)chart
{
    return CGRangeZero;
}

//判断显示横线条
- (BOOL)UUChart:(UUChart *)chart ShowHorizonLineAtIndex:(NSInteger)index
{
    return YES;
}

//判断显示最大最小值
- (BOOL)UUChart:(UUChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return NO;
}

#pragma mark 网络请求

- (void)getCityDetail:(NSString *)postcode{
    
    //封装参数
    CityDetailInfoRequest *request = [[CityDetailInfoRequest alloc]init];
    request.postCode = postcode;
    //请求指示器
    [HUDManager showLoadNetWorkHUDInView:self.navigationController.view];
    
    [_projectService cityDetailInfoWithRequest:request success:^(id responseObject) {
       
        _model = responseObject;
        
        switch (_model.res) {
            case ZRBHttpSuccssType:
            {
                [HUDManager removeHUDFromView:self.navigationController.view];
                [self initTopView:_model];
                [self initCityDetailView:_model];
                [self initIndustryDistribute];
                [self initEconomyView];
                break;
            }
            case ZRBHttpFailType:
            {
                WS(bself);
                [HUDManager removeHUDFromView:self.navigationController.view];
                [HUDManager showNonNetWorkHUDInView:self.navigationController.view event:^{
                    [bself getCityDetail:self.postCode];
                }];
                break;
            }
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failture:^(NSError *error) {
        WS(bself);
        [HUDManager removeHUDFromView:self.navigationController.view];
        [HUDManager showNonNetWorkHUDInView:self.navigationController.view event:^{
            [bself getCityDetail:self.postCode];
        }];
        
    }];
}

- (void)getEconomicInfo:(NSString *)postCode{
    
    CityEconomicInfoRequest *request = [[CityEconomicInfoRequest alloc]init];
    request.postCode = postCode;
    
    [_projectService cityEconomicInfoWithRequest:request success:^(id responseObject) {
       
        CityEnomicModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                [self showMenuWithModel:model];
                break;
            case ZRBHttpFailType:
                
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failture:^(NSError *error) {
        
    }];
}

@end
