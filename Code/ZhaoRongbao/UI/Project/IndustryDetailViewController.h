//
//  IndustryDetailViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/31.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "IndustryCell.h"
#import "ProjectService.h"
#import "CityIndustryModel.h"

@interface IndustryDetailViewController : ZRB_ViewControllerWithBackButton<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_mTableView;
    ProjectService *_projectService;
}

@property (nonatomic,strong)NSMutableArray *colorStrArray;
@property (nonatomic,strong)NSMutableArray *titleArray;
@property (nonatomic,strong)NSMutableArray *valueArray;
@property (nonatomic,strong)NSMutableArray *cellContentArray;

@end
