//
//  IndustryDetailViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/31.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "IndustryDetailViewController.h"

@implementation IndustryDetailViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self setup];
    
    [self getIndustryDetail:@"421182"];
}

- (void)setup{
    self.title = @"产业详情";
    
    _projectService = [[ProjectService alloc]init];
    
    _colorStrArray = [[NSMutableArray alloc]initWithObjects:@"5fe18a",@"f5b880",@"2a75c2", nil];
    _titleArray = [[NSMutableArray alloc]init];
    _valueArray = [[NSMutableArray alloc]init];
    _cellContentArray = [[NSMutableArray alloc]init];
    
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 10,SCREEN_WIDTH, SCREEN_HEIGHT-10) style:UITableViewStylePlain];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
}

#pragma  delegate of UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _titleArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[_cellContentArray objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"fafafa"];
    
    //标签颜色
    UIView *leftFlagView = [[UIView alloc]init];
    leftFlagView.backgroundColor = [UIColor colorWithHexString:_colorStrArray[section]];
    [headerView addSubview:leftFlagView];
    
    //标题
    UILabel *title = [[UILabel alloc]init];
    title.backgroundColor = [UIColor clearColor];
    title.font = [UIFont systemFontOfSize:16.0f];
    title.text = _titleArray[section];
    title.textColor = [UIColor colorWithHexString:@"5e5e5e"];
    [headerView addSubview:title];
    
    //总产值
    UILabel *value = [[UILabel alloc]init];
    value.backgroundColor = [UIColor clearColor];
    value.font = [UIFont systemFontOfSize:16.0f];
    value.text = [NSString stringWithFormat:@"%@",_valueArray[section]];
    value.textColor = [UIColor colorWithHexString:@"5e5e5e"];
    [headerView addSubview:value];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"e6e7ec"];
    [headerView addSubview:line];
    
    [leftFlagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView.mas_left);
        make.top.equalTo(headerView.mas_top);
        make.height.equalTo(headerView.mas_height);
        make.width.equalTo(10);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftFlagView.mas_right).offset(10);
        make.height.equalTo(headerView.mas_height);
        make.top.equalTo(headerView.mas_top);
    }];
    
    [value mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headerView.mas_right).offset(-10);
        make.height.equalTo(headerView.mas_height);
        make.top.equalTo(headerView.mas_top);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(headerView.mas_width);
        make.bottom.equalTo(headerView.mas_bottom);
        make.left.equalTo(headerView.mas_left);
        make.height.equalTo(@1);
    }];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellStr = @"IndustryCell";
    IndustryCell *cell = (IndustryCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (cell == nil) {
        cell = [[IndustryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellStr];
    }
    
    cell.value.text = [[_cellContentArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark 网络请求

- (void)getIndustryDetail:(NSString *)postCode{
    
    CityIndustryInfoRequest *request = [[CityIndustryInfoRequest alloc]init];
    request.postCode = postCode;
    
    [HUDManager showLoadNetWorkHUDInView:self.navigationController.view];
    
    [_projectService cityIndustryInfoWithRequest:request success:^(id responseObject) {
     
        [HUDManager removeHUDFromView:self.navigationController.view];
        CityIndustryModel *model = responseObject;
        
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                for (CityIndustryItemModel *itemModel in model.data) {
                    
                    [_titleArray addObject:itemModel.name];
                    [_valueArray addObject:itemModel.value];
                    [_cellContentArray addObject:itemModel.company];
                }
                [_mTableView reloadData];
                
                break;
            }
            case ZRBHttpFailType:
                
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
        
        
    } failture:^(NSError *error) {
         [HUDManager removeHUDFromView:self.navigationController.view];
    }];
}


@end
