//
//  InvestigationViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "TOTextInputChecker.h"
#import "InvestigationCell.h"
#import "UIButton+ZRB.h"
#import "ProjectService.h"
#import "UUDatePicker.h"
#import "InvestigationApplyModel.h"

@interface InvestigationViewController : ZRB_ViewControllerWithBackButton<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UUDatePickerDelegate>
{
    ProjectService *_projectService;
    
    UIView *_inputView;
    UUDatePicker *_datePickerView;
    UITableView *_mTableView;
    UIButton *_finishBtn;
    TOTextInputChecker *_phoneChecker;      //电话号码检查器
}

@property (nonatomic,copy)NSString *projectID;

@property (nonatomic,copy)NSMutableArray *nameArray;
@property (nonatomic,copy)NSMutableArray *placeHolderArray;
@property (nonatomic,copy)NSMutableArray *flagArray;


@end
