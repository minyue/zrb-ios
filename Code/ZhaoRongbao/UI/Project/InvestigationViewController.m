//
//  InvestigationViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "InvestigationViewController.h"

@interface InvestigationViewController ()

@end

@implementation InvestigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
}

- (void)setup{
    self.title = @"预约项目勘察";
    _projectService = [[ProjectService alloc]init];
    
    _flagArray = [[NSMutableArray alloc]initWithObjects:@"0", @"0",@"0",@"0",nil];
    _nameArray = [[NSMutableArray alloc]initWithObjects:@"看地时间",@"看地人",@"联系电话",@"看地人数", nil];
    _placeHolderArray = [[NSMutableArray alloc]initWithObjects:@"选择时间",@"",@"",@"", nil];
    
    
    _inputView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 256)];
    _inputView.backgroundColor = [UIColor whiteColor];
    
    //取消时间选择按钮
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.tag = 0;
    cancelBtn.frame = CGRectMake(10, 3, 60, 50);
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:17.0f];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor colorWithHexString:@"838383"] forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [cancelBtn addTarget:self action:@selector(selectDate:) forControlEvents:UIControlEventTouchUpInside];
    [_inputView addSubview:cancelBtn];
    
    //确定时间选择按钮
    UIButton *okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.tag = 1;
    okBtn.frame = CGRectMake(SCREEN_WIDTH - 70, 3, 60, 50);
    okBtn.titleLabel.font = [UIFont systemFontOfSize:17.0f];
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor colorWithHexString:@"838383"] forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [okBtn addTarget:self action:@selector(selectDate:) forControlEvents:UIControlEventTouchUpInside];
    [_inputView addSubview:okBtn];

    
    //时间选择器
    _datePickerView= [[UUDatePicker alloc]initWithframe:CGRectMake(0,56, SCREEN_WIDTH, 200)
                                               Delegate:self
                                            PickerStyle:UUDateStyle_YearMonthDayHourMinute];
    _datePickerView.minLimitDate = [NSDate date];
    _datePickerView.ScrollToDate = [NSDate date];
    [_inputView addSubview:_datePickerView];
    
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 180) style:UITableViewStylePlain];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    _mTableView.scrollEnabled = NO;
    _mTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
    
    //完成按钮
     _finishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
     _finishBtn.frame = CGRectMake((SCREEN_WIDTH - 305)/2, 230, 305, 50);
     _finishBtn.layer.cornerRadius = 10.0f;
    [_finishBtn setTitle:@"完成" forState:UIControlStateNormal];
     _finishBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
     _finishBtn.enabled = NO;
    [_finishBtn setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"] forState:UIControlStateDisabled];
    [_finishBtn addTarget:self action:@selector(applyInvestigation:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_finishBtn];
}

- (void)selectDate:(id)sender{
    InvestigationCell *cell = (InvestigationCell *)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    UIButton *button = (UIButton *)sender;
    if (button.tag == 0) {
        //取消
//        cell.textField.text = @"";
//        [_flagArray replaceObjectAtIndex:0 withObject:@"0"];
        
    }else if(button.tag == 1){
        //确定
        if (cell.textField.text.length < 1) {
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            [formatter setDateFormat:@"yyyy-mm-dd HH:mm"];
            
            NSString *dateStr = [formatter stringFromDate:[NSDate date]];
            cell.textField.text = dateStr;
        }
         [_flagArray replaceObjectAtIndex:0 withObject:@"1"];
    }
    
    [cell.textField resignFirstResponder];
}

//完成按钮响应
- (void)applyInvestigation:(id)sender{
    
    InvestigationCell *dateCell = (InvestigationCell *)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    InvestigationCell *personCell = (InvestigationCell *)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    InvestigationCell *phoneCell = (InvestigationCell *)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    InvestigationCell *numCell = (InvestigationCell *)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    
    InvestigationApplyRequest *request = [[InvestigationApplyRequest alloc]init];
    request.projectID = self.projectID;
    request.contact = personCell.textField.text;
    request.telphone = phoneCell.textField.text;
    request.personNum = numCell.textField.text;
    request.companyName = [ZRB_UserManager shareUserManager].userData.org;
    request.inspectTime = [NSString stringWithFormat:@"%@:00",dateCell.textField.text];
    
    LogInfo(@"projectID:%@",request.projectID);
    LogInfo(@"contact:%@",request.contact);
    LogInfo(@"telphone:%@",request.telphone);
    LogInfo(@"personNum:%@",request.personNum);
    LogInfo(@"companyName:%@",request.companyName);
    LogInfo(@"inspectTime:%@",request.inspectTime);
    
    [self investigationApply:request];
    
}

#pragma mark - UUDatePicker's delegate
- (void)uuDatePicker:(UUDatePicker *)datePicker year:(NSString *)year month:(NSString *)month day:(NSString *)day hour:(NSString *)hour  minute:(NSString *)minute  weekDay:(NSString *)weekDay
{
    InvestigationCell *cell = (InvestigationCell *)[_mTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.textField.text = [NSString stringWithFormat:@"%@-%@-%@ %@:%@",year,month,day,hour,minute];
    
    [_flagArray replaceObjectAtIndex:0 withObject:@"1"];
}

#pragma delegate of UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _nameArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *indentifier = @"InvestigationCell";
    InvestigationCell *cell =(InvestigationCell *)[tableView dequeueReusableCellWithIdentifier:indentifier];
    if (cell == nil) {
        cell = [[InvestigationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
    }
    
    cell.name.text = [_nameArray objectAtIndex:indexPath.row];
    cell.textField.delegate = self;
    cell.textField.tag = indexPath.row;
    cell.textField.placeholder = [_placeHolderArray objectAtIndex:indexPath.row];
    [cell.textField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    
    if (indexPath.row == 0) {
        cell.textField.inputView = _inputView;
    }
    
    if(indexPath.row == 2){
        cell.textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    return cell;
}


#pragma delegate of UITextField
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldChanged:(id)sender{
    
    UITextField *textField = (UITextField *)sender;
    NSInteger i= textField.tag;
    NSString *text = textField.text;

    if (i == 2) {
        //手机号码长度限制 11 位
        if (text.length > 11) {
            textField.text = [ textField.text substringToIndex:11];
        }
        //验证手机号码格式
        if([self isMobileNumber:textField.text]){
            [_flagArray replaceObjectAtIndex:i withObject:@"1"];
        }else{
            [_flagArray replaceObjectAtIndex:i withObject:@"0"];
        }
    }else{
        if (text.length > 0) {
            [_flagArray replaceObjectAtIndex:i withObject:@"1"];
        }else{
            [_flagArray replaceObjectAtIndex:i withObject:@"0"];
        }
    }
    
    if([_flagArray containsObject:@"0"]){
        _finishBtn.enabled = NO;
        [_finishBtn setBackgroundColor:[UIColor colorWithHexString:@"c7c7c7"] forState:UIControlStateNormal];
    }else{
        _finishBtn.enabled = YES;
        [_finishBtn setBackgroundColor:[UIColor colorWithHexString:@"4997ec"] forState:UIControlStateNormal];
    }
}


//点击空白区域隐藏键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

#pragma 网络请求
- (void)investigationApply:(InvestigationApplyRequest *)request{
    [SVProgressHUD show];
    
    [_projectService investigationApplyWithRequest:request success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        InvestigationApplyModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
            }
            case ZRBHttpFailType:
                
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
        
    } failture:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}


/**
 *  判断手机号码格式
 */
- (BOOL)isMobileNumber:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
