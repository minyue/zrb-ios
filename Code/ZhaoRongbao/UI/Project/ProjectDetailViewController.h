//
//  ProjectDetailViewController.h
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/13.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZRB_ViewController.h"
#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"
#import "WriteCommentViewController.h"
#import "MineLoginViewcController.h"
#import "AllCommentViewController.h"
#import "ProjectLocationViewController.h"
#import "InvestigationViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "TOTextInputChecker.h"
#import "ProjectService.h"
#import "CollectionModel.h"
#import "CityDetailViewController.h"
#import "AskForMoreInfoModel.h"

@interface ProjectDetailViewController : ZRB_ViewControllerWithBackButton<UIWebViewDelegate,UIScrollViewDelegate, NJKWebViewProgressDelegate,UIAlertViewDelegate>
{
    ProjectService *_projectService;
    
    JSContext               *_content;
    UIWebView               *_webView;
    UIView                  *_rightBarView;
    UIImageView             *_bottomMenu;
    
    NJKWebViewProgressView *_progressView;
    NJKWebViewProgress *_progressProxy;
}

- (void)setupJsContent;

@property (nonatomic,copy) NSString *projectID;          //项目ID
@property (nonatomic,copy) NSString *projectName;        //项目名称
@property (nonatomic,assign)CGFloat oldOffset;          //滑动纵坐标

@end
