//
//  ProjectDetailViewController.m
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/13.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ProjectDetailViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "ZRB_NormalModel.h"


@implementation ProjectDetailViewController

#define BOTTOM_MENU_HEIGHT        49


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [self.navigationController.navigationBar addSubview:_progressView];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Remove progress view
    // because UINavigationBar is shared with other ViewControllers
    [_progressView removeFromSuperview];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self setup];
    [self loadDetailWithId:self.projectID];
    
    [self setupNavBarViews];
}


- (void)setup{
    
    _projectService = [[ProjectService alloc]init];

    _progressProxy = [[NJKWebViewProgress alloc] init];
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - BOTTOM_MENU_HEIGHT-64)];
    _webView.autoresizesSubviews = YES;
    _webView.scalesPageToFit = YES;
    _webView.delegate = _progressProxy;
    _webView.scrollView.delegate = self;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    //取消回弹
    [(UIScrollView *)[[_webView subviews] objectAtIndex:0] setBounces:NO];
    
    [self.view addSubview:_webView];
    
    UIView *bottomView = [[UIView alloc]init];
    bottomView.userInteractionEnabled = YES;
    bottomView.hidden = YES;
    bottomView.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:1.0];
    [self.view addSubview:bottomView];
    
    //横分割线
    UIView *horizontalLine = [[UIView alloc]init];
    horizontalLine.backgroundColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    [bottomView addSubview:horizontalLine];
    
    //消息按钮
    UIButton *messageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBtn addTarget:self action:@selector(allComment:) forControlEvents:UIControlEventTouchUpInside];
    [messageBtn setEnlargeEdgeWithTop:39 right:20 bottom:39 left:20];
    [messageBtn setBackgroundImage:[UIImage imageNamed:@"project_view_message"] forState:UIControlStateNormal];
    [bottomView addSubview:messageBtn];
    
    //竖分割线
    UIView *verticalLine = [[UIView alloc]init];
    verticalLine.backgroundColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    [bottomView addSubview:verticalLine];
    
    //输入框
    UIButton *textLB = [[UIButton alloc]init];
    textLB.layer.cornerRadius = 10;
    textLB.clipsToBounds =YES;
    [textLB addTarget:self action:@selector(writeComment:) forControlEvents:UIControlEventTouchUpInside];
    textLB.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
    [bottomView addSubview:textLB];
    
    //撰写评论图标
    UIImageView *icon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"project_comment"]];
    [textLB addSubview:icon];
    
    //撰写评论文字提示
    UILabel *remind = [[UILabel alloc]init];
    remind.text = @"发表评论";
    remind.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    remind.font = [UIFont systemFontOfSize:12.0f];
    [remind sizeToFit];
    [textLB addSubview:remind];
    
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom);
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(@55);
    }];
    
    [horizontalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(@0.5);
        make.top.equalTo(bottomView.mas_top);
    }];
    
    [messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@16);
        make.height.equalTo(@16);
        make.centerY.equalTo(bottomView.mas_centerY);
        make.right.equalTo(bottomView.mas_right).offset(-20);
    }];
    
    [verticalLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@0.5);
        make.height.equalTo(bottomView.mas_height);
        make.right.equalTo(messageBtn.mas_left).offset(-20);
        make.bottom.equalTo(bottomView.mas_bottom);
    }];
    
    [textLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.mas_left).offset(10);
        make.right.equalTo(verticalLine.mas_left).offset(-10);
        make.centerY.equalTo(bottomView.mas_centerY);
        make.height.equalTo(@30);
    }];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@16);
        make.height.equalTo(@16);
        make.centerY.equalTo(textLB.mas_centerY);
        make.left.equalTo(textLB.mas_left).offset(10);
    }];
    
    [remind mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(textLB.mas_centerY);
        make.left.equalTo(icon.mas_right).offset(10);
    }];
    
    
    
    //构造底部菜单
    _bottomMenu = [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, BOTTOM_MENU_HEIGHT)];
    _bottomMenu.image = [UIImage imageNamed:@"project_floating-box_bg"];
    _bottomMenu.backgroundColor = [UIColor clearColor];
    _bottomMenu.userInteractionEnabled = YES;
    [self.view addSubview:_bottomMenu];
    
    //客服
    UIButton *serviceView = [[UIButton alloc]init];
    serviceView.backgroundColor = [UIColor clearColor];
    [serviceView addTarget:self action:@selector(callService:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomMenu addSubview:serviceView];
    
    //客服头像
    UIImageView *serviceIcon = [[UIImageView alloc]init];
    serviceIcon.image = [UIImage imageNamed:@"my_project"];
    [serviceView addSubview:serviceIcon];
    
    //客服名称
    UILabel *serviceName = [[UILabel alloc]init];
    serviceName.text = @"USER\n招融宝客服";
    serviceName.numberOfLines = 2;
    serviceName.font = [UIFont systemFontOfSize:8.0f];
    [serviceView addSubview:serviceName];
    
    [_bottomMenu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(@(BOTTOM_MENU_HEIGHT));
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    [serviceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bottomMenu.mas_top);
        make.left.equalTo(_bottomMenu.mas_left);
        make.height.equalTo(_bottomMenu.mas_height);
        make.width.equalTo(@(SCREEN_WIDTH*3/7));
    }];
    
    [serviceIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@20);
        make.height.equalTo(@20);
        make.left.equalTo(serviceView.mas_left).offset(40);
        make.centerY.equalTo(serviceView.mas_centerY);
    }];
    
    [serviceName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(serviceView.mas_height);
        make.centerY.equalTo(serviceView.mas_centerY);
        make.left.equalTo(serviceIcon.mas_right).offset(10);
    }];
    
   
    
    NSMutableArray *iconArray = [[NSMutableArray alloc]initWithObjects:@"project_floating-box_consultation",@"project_floating-box_dialogue",@"project_floating-box_survey", nil];
    NSMutableArray *titleArray = [[NSMutableArray alloc]initWithObjects:@"咨询",@"对话",@"勘察", nil];

    for (int i = 0; i < iconArray.count; i ++) {
        UIButton *view = [[UIButton alloc]init];
        view.tag = i;
        [view addTarget:self action:@selector(bottomMenuCilck:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomMenu addSubview:view];
        
        //分割线
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = [UIColor colorWithHexString:@"b2b2b2"];
        [view addSubview:line];
        
        //图标
        UIImageView *icon = [[UIImageView alloc]init];
        icon.image = [UIImage imageNamed:iconArray[i]];
        [view addSubview:icon];
        
        //标题
        UILabel *title = [[UILabel alloc]init];
        title.textColor = [UIColor colorWithHexString:@"2982d8"];
        title.font = [UIFont systemFontOfSize:10.0f];
        title.text = titleArray[i];
        [view addSubview:title];
        
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(serviceView.mas_right).offset(SCREEN_WIDTH*4/21*i);
            make.top.equalTo(_bottomMenu.mas_top);
            make.height.equalTo(_bottomMenu.mas_height);
            make.width.equalTo(@(SCREEN_WIDTH*4/21));
        }];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view.mas_top);
            make.height.equalTo(view.mas_height);
            make.left.equalTo(view.mas_left);
            make.width.equalTo(@0.5);
        }];
        
        [icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@10);
            make.height.equalTo(@10);
            make.centerX.equalTo(view.mas_centerX);
            make.top.equalTo(_bottomMenu.mas_top).offset(10);
        }];
        
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(view.mas_centerX);
            make.top.equalTo(icon.mas_bottom).offset(5);
        }];
    }
}

//呼叫客服
- (void)callService:(id)sender{
    LogInfo(@"客服电话");
    NSString *str = [NSString stringWithFormat:@"tel:%@",@"15071235287"];
    UIWebView *webView = [[UIWebView alloc]init];
    [webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:str]]];
    [self.view addSubview:webView];
}

//咨询、对话、勘察
- (void)bottomMenuCilck:(id)sender{
    LogInfo(@"咨询、对话、勘察");
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case ConsultingMenuType:
            
            break;
        case ComunicationMenuType:
            
            break;
        case InvestigationMenuType:
        {
            if ([ZRB_UserManager isLogin]) {
                InvestigationViewController *investigationCtrl = [[InvestigationViewController alloc]init];
                investigationCtrl.projectID = self.projectID;
                [self.navigationController pushViewController:investigationCtrl animated:YES];
            }else{
                MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
                [self.navigationController pushViewController:loginCtrl animated:YES];
            }
            
            break;
        }
        default:
            break;
    }

}

/**
 *   显示底部菜单
 */
- (void)showBottomMenu{
    
   [UIView animateWithDuration:.3 animations:^{
       _bottomMenu.frame = CGRectMake(0, SCREEN_HEIGHT - BOTTOM_MENU_HEIGHT - 64, SCREEN_WIDTH, BOTTOM_MENU_HEIGHT);

   } completion:^(BOOL finished) {
       
   }];
}

/**
 *  隐藏底部菜单
 */
- (void)hideBottomMenu{
    [UIView animateWithDuration:.3 animations:^{
        _bottomMenu.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, BOTTOM_MENU_HEIGHT);

    } completion:^(BOOL finished) {
        
    }];
}

- (void)retuqestIfAttention:(UIButton*)btn{
    btn.selected = NO;
    [_projectService checkIfAttentionNewOrProjectWithRequest:self.projectID andType:1 success:^(id responseObject) {
        if(responseObject){
            ZRB_NormalModel *model = responseObject;
            if(model.res == 1){
                btn.selected = [[model.data valueForKey:@"isCollected"] boolValue];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}
/**
 *  构造导航栏菜单
 */
- (void)setupNavBarViews
{
    _rightBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 2, 115, 44)];
    _rightBarView.backgroundColor = [UIColor clearColor];
    
    //分享
    UIButton  *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBtn setImage:[UIImage imageNamed:@"news_bar_Share"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareNews:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:shareBtn];
    [shareBtn makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_rightBarView.mas_leading).offset(5);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    UILabel  *shareLabel = [[UILabel alloc] init];
    [shareLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [shareLabel setBackgroundColor:[UIColor clearColor]];
    shareLabel.text = @"分享";;
    shareLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:shareLabel];
    
    [shareLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(shareBtn.mas_centerX);
        make.top.equalTo(shareBtn.mas_bottom).offset(2);
    }];
    
    
    //关注
    UIButton  *focusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [focusBtn setImage:[UIImage imageNamed:@"news_bar_follow_off"] forState:UIControlStateNormal];
    [focusBtn setImage:[UIImage imageNamed:@"news_bar_follow_on"] forState:UIControlStateSelected];
    [focusBtn addTarget:self action:@selector(focusNews:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:focusBtn];
    [focusBtn makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(shareBtn.mas_right).offset(20);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    [self retuqestIfAttention:focusBtn];
    
    UILabel  *focusLabel = [[UILabel alloc] init];
    [focusLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [focusLabel setBackgroundColor:[UIColor clearColor]];
    focusLabel.text = @"关注";
    focusLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:focusLabel];
    
    [focusLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(focusBtn.mas_centerX);
        make.top.equalTo(focusBtn.mas_bottom).offset(2);
    }];
    
    
    //评论
    UIButton  *commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentBtn setImage:[UIImage imageNamed:@"project_bar_comment"] forState:UIControlStateNormal];
    [commentBtn addTarget:self action:@selector(commentProject:) forControlEvents:UIControlEventTouchUpInside];
    [_rightBarView addSubview:commentBtn];
    [commentBtn makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(focusBtn.mas_right).offset(20);
        make.right.equalTo(_rightBarView.mas_right).offset(-5);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    
    UILabel  *commentLabel = [[UILabel alloc] init];
    [commentLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [commentLabel setBackgroundColor:[UIColor clearColor]];
    commentLabel.text = @"评论";;
    commentLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:commentLabel];
    
    [commentLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(commentBtn.mas_centerX);
        make.top.equalTo(commentBtn.mas_bottom).offset(2);
    }];
    
    CustomBarItem *item =  [self.navigationItem setItemWithCustomView:_rightBarView itemType:right];
    
    [item setOffset:-5];
}

//分享
- (void)shareNews:(id)sender
{

}

//关注
- (void)focusNews:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if (button.selected == NO) {
        //关注项目
        [self collectionProject:self.projectID button:button];
    }else{
        //取消关注
        [self cancelCollectionProject:self.projectID button:button];
    }
    
}

//评论
- (void)commentProject:(id)sender
{
    if([ZRB_UserManager isLogin]){
        WriteCommentViewController *writeCommentCtrl = [[WriteCommentViewController alloc]init];
        writeCommentCtrl.projectID = self.projectID;
        [self.navigationController pushViewController:writeCommentCtrl animated:YES];
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
}


//查看所有评论
- (void)allComment:(id)sender{
    
    //需要登录
    if([ZRB_UserManager isLogin]){
        AllCommentViewController *allCommentCtrl = [[AllCommentViewController alloc]init];
        allCommentCtrl.projectID                 = self.projectID;
        [self.navigationController pushViewController:allCommentCtrl animated:YES];
    }else{
        MineLoginViewcController *loginCtrl      = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
}

//撰写评论
- (void)writeComment:(id)sender{
    if([ZRB_UserManager isLogin]){
        WriteCommentViewController *writeCommentCtrl = [[WriteCommentViewController alloc]init];
        writeCommentCtrl.projectID = self.projectID;
        [self.navigationController pushViewController:writeCommentCtrl animated:YES];
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
}


/**
 *  加载详情页面
 *
 *  @param projectID 项目ID
 */
- (void)loadDetailWithId:(NSString *)projectID
{
    NSString  *url = [NSString stringWithFormat:@"%@/pro/proDetails?id=%@",SERVER_URL,projectID];
    LogInfo(url);
    NSURL *webUrl = [NSURL URLWithString:url];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:webUrl];
    [_webView loadRequest:req];
}

- (void)setupJsContent
{
    //获取当前JS环境
    _content = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    // 打印异常
    _content.exceptionHandler =
    ^(JSContext *context, JSValue *exceptionValue)
    {
        context.exception = exceptionValue;
        LogInfo(@"%@", exceptionValue);
    };
    //获取JS事件
    WS(bself);
    _content[@"showtag"] = ^(int num,NSString *tag){
        LogInfo(@"num = %d   \n tag = %@",num,tag);
       
        if (num == 1) {        //查看项目所在地
            //获取经纬度
            NSArray *locationArray = [tag componentsSeparatedByString:@","];
            if (locationArray.count > 1) {
                
                ProjectLocationViewController *locationCtrl = [[ProjectLocationViewController alloc]init];
                locationCtrl.longitude = [locationArray[0] floatValue];
                locationCtrl.latitude = [locationArray[1] floatValue];
                [bself.navigationController pushViewController:locationCtrl animated:YES];
            }
            
        }else if(num == 2){
            UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"" message:@"我们将在3个工作日内\n将项目详细资料发送给你您" delegate:bself cancelButtonTitle:nil otherButtonTitles:@"取消",@"确定", nil];
            alter.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField *tf = [alter textFieldAtIndex:0];
            tf.placeholder = @"请输入邮箱";
            
            [alter show];
        }else if(num == 4){      //查看所有评论，需要登录
           
            if([ZRB_UserManager isLogin]){
                AllCommentViewController *allCommentCtrl = [[AllCommentViewController alloc]init];
                allCommentCtrl.projectID = bself.projectID;
                [bself.navigationController pushViewController:allCommentCtrl animated:YES];
            }else{
                MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
                [bself.navigationController pushViewController:loginCtrl animated:YES];
            }

        }else if(num == 6){       //查看项目所在城市详情
            if([ZRB_UserManager isLogin]){
                CityDetailViewController *cityDetailCtrl = [[CityDetailViewController alloc]init];
                [bself.navigationController pushViewController:cityDetailCtrl animated:YES];
            }else{
                MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
                [bself.navigationController pushViewController:loginCtrl animated:YES];
            }
        }
    };
}

/**
 *  验证邮箱格式
 */
-(BOOL)isValidateEmail:(NSString *)email {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


#pragma delegate of UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    UITextField *tf = (UITextField *)[alertView textFieldAtIndex:0];
    if (buttonIndex == 1) {
        if (![self isValidateEmail:tf.text]) {
            
            [self showTipViewWithMsg:@"邮箱格式错误"];
        }else{
            [self askForMoreInfo:tf.text];
        }
    }else if (buttonIndex == 0){
       
    }
}



#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
//    self.title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}



- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self setupJsContent];
}

#pragma delegate of UIScrollerView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y > _oldOffset) {
        //如果当前位移大于缓存位移，说明scrollView向上滑动
        [self hideBottomMenu];
        
    }else{
        [self showBottomMenu];
    }
  
    _oldOffset = scrollView.contentOffset.y;//将当前位移变成缓存位移
}


#pragma 网络请求

/**
 *  项目关注
 */
- (void)collectionProject:(NSString *)projectID button:(UIButton *)button{
    
    if ([ZRB_UserManager isLogin]) {
        CollectionProjectRequest *request = [[CollectionProjectRequest alloc]init];
        request.projectID = projectID;
        request.collectionType = 1;
        
        //指示器
        [SVProgressHUD show];
        //发起请求
        WS(bself);
        [_projectService collectionPorjectWithRequest:request success:^(id responseObject) {
            [SVProgressHUD dismiss];
            
            CollectionModel *model = responseObject;
            switch (model.res) {
                case ZRBHttpSuccssType:
                {
                    [MBProgressHUD showHUDTitle:@"关注成功" onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
                    button.selected = YES;
                    break;
                }
                case ZRBHttpFailType:
                    [bself showTipViewWithMsg:@"关注失败"];
                    button.selected = NO;
                    
                    break;
                case ZRBHttpNoLoginType:
                     button.selected = NO;
                    break;
                default:
                    break;
            }
            
        } failture:^(NSError *error) {
            [SVProgressHUD dismiss];
             button.selected = NO;
            [bself showTipViewWithMsg:@"关注失败"];
        }];

    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
}

/**
 *  取消关注
 */
- (void)cancelCollectionProject:(NSString *)projectID button:(UIButton *)button{
    if ([ZRB_UserManager isLogin]) {
        
        CancelCollectionProjectRequest *request = [[CancelCollectionProjectRequest alloc]init];
        request.projectID = self.projectID;
        request.collectionType = 1;
        
        //指示器
        [SVProgressHUD show];
        //发起请求
        WS(bself);
        [_projectService cancelCollectionProjectWithRequest:request success:^(id responseObject) {
            
            [SVProgressHUD dismiss];
            
            CollectionModel *model = responseObject;
            switch (model.res) {
                case ZRBHttpSuccssType:
                {
                    [MBProgressHUD showHUDTitle:@"取消关注" onView:[(AppDelegate *)([UIApplication sharedApplication].delegate)window]];
                    button.selected = NO;
                    break;
                }
                case ZRBHttpFailType:
                    [bself showTipViewWithMsg:@"取消失败"];
                    button.selected = YES;
                    
                    break;
                case ZRBHttpNoLoginType:
                    button.selected = YES;
                    break;
                default:
                    break;
            }

        } failture:^(NSError *error) {
            button.selected = YES;
            [SVProgressHUD dismiss];
            [bself showTipViewWithMsg:@"取消失败"];
        }];
        
    }else{
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
}

/**
 *  索要更多资料
 */
- (void)askForMoreInfo:(NSString *)email{
    
    //索取更多资料请求
    AskForMoreInfoRequest *request = [[AskForMoreInfoRequest alloc]init];
    request.projectID = self.projectID;
    request.projectName = self.projectName;
    request.email = email;
    
    [_projectService askForMoreInfoWithRequest:request success:^(id responseObject) {
        
        AskForMoreInfoModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                [MBProgressHUD showHUDTitle:@"申请成功" onView:self.view];
                break;
            case ZRBHttpFailType:
                
                [self showTipViewWithMsg:@"申请失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
                
            default:
                break;
        }
        
    } failture:^(NSError *error) {
        [self showTipViewWithMsg:@"申请失败"];
    }];
    
}


@end
