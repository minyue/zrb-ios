//
//  ProjectHomeViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/27.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "MJRefresh.h"
#import "ProjectService.h"
#import "ProjectListModel.h"
#import "ProjectListCell.h"
#import "HeadScrollView.h"
#import "ProjectDetailViewController.h"
#import "CollectionModel.h"
#import "MinePersonalAreaViewController.h"
#import "AreaModel.h"

@interface ProjectHomeViewController : ZRB_ViewController<UITableViewDataSource,UITableViewDelegate,HeadScrollDelegate>
{
    AreaModel      *_areaModel;
    
    ProjectService *_projectService;
    UIView         *_topView;                       //顶部视图
    UIButton       *_btnMenu;                       //顶部按钮菜单主按钮
    HeadScrollView *_headerView;                    //banner视图
    NSMutableArray *_bannerArray;                   //滚动banner数组
    NSMutableArray *_btnMenuArray;                  //顶部横向菜单按钮数组
    NSMutableArray *_projectArray;                  //项目列表数组
    UILabel        *_itemTitle;                      //导航栏右边Item自定义
    
    BOOL  _isRequst;            //是否请求过
}

- (void)refreshData;        //刷新数据

@property UITableView *projectTableView;
@end
