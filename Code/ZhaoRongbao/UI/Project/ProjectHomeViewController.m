//
//  ProjectHomeViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/27.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ProjectHomeViewController.h"

@interface ProjectHomeViewController ()


@end

@implementation ProjectHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"项目";
    
    [self initData];
    [self initView];
    
    [self refreshData];
    //    [self queryListData];
}

#pragma mark -
#pragma mark -公有方法

/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        [_projectTableView.header beginRefreshing];
    }
}


/**
 *  初始化数据
 */
- (void)initData{
    _isRequst = NO;
    
    _areaModel = [[AreaModel alloc]init];
    _projectService = [[ProjectService alloc]init];
    _bannerArray = [[NSMutableArray alloc]init];
    _projectArray = [[NSMutableArray alloc]init];
    _btnMenuArray = [[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getChangedAreaName:) name:PROJECT_PROVINCE_CHANGE object:nil];
}

/**
 *  初始化视图
 */
- (void)initView{
    
    UIView *itemView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 53, 44)];
    itemView.backgroundColor = [UIColor clearColor];
    itemView.userInteractionEnabled = NO;
    
    _itemTitle = [[UILabel alloc]init];
    _itemTitle.textColor = [UIColor whiteColor];
    _itemTitle.font = [UIFont systemFontOfSize:15.0f];
    _itemTitle.text = @"全国";
    [itemView addSubview:_itemTitle];
    
    UIImageView *icon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"project_location_top"]];
    [itemView addSubview:icon];
    
    
    [_itemTitle mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(icon.mas_right);
        make.centerY.equalTo(itemView.mas_centerY);
        make.right.equalTo(itemView.mas_right);
    }];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@9);
        make.height.equalTo(@14);
        make.right.equalTo(_itemTitle.mas_left).offset(-3);
        make.centerY.equalTo(itemView.mas_centerY);
    }];
    
    
    
    CustomBarItem * _rightItem =  [self.navigationItem setItemWithCustomView:itemView itemType:right];
    [_rightItem setOffset:-2];
    [_rightItem addTarget:self selector:@selector(changeArea:) event:UIControlEventTouchUpInside];

    
    _projectTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64 -49) style:UITableViewStylePlain];
    _projectTableView.delegate = self;
    _projectTableView.dataSource = self;
    [_projectTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    [self addHeaderView];
    [self addFooterView];
    
    [self.view addSubview:_projectTableView];
}

- (void)changeArea:(id)sender{
    LogInfo(@"");
    
    MinePersonalAreaViewController *areaCtrl = (MinePersonalAreaViewController *)[StoryBoardUtilities viewControllerForStoryboardName:kPersonalStoryboardName class:[MinePersonalAreaViewController class]];
    areaCtrl.lastCtrlFlag = ZRBProjectHomeViewCtroller;
    areaCtrl.myAreaInfo = [ZRB_UserManager shareUserManager].userData.LOCATIONNAME;
    areaCtrl.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:areaCtrl animated:YES];
}

//接收地区改变通知
- (void)getChangedAreaName:(id)sender{
   
    NSNotification *notify = (NSNotification *)sender;
    _areaModel = [notify object];
    
    LogInfo(@"%@   %@",_areaModel.cityName,_areaModel.postcode);
    _itemTitle.text = [_areaModel.cityName substringToIndex:_areaModel.cityName.length - 1];
    
    [_projectTableView.header beginRefreshing];
}

/**
 *   初始化广告滚动视图
 */

- (void)initBannerView:(ProjectItemModel *)model{
    
    _topView = [[UIView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 250.0f)];
    _topView.userInteractionEnabled = YES;
    
    //banner图片地址
    NSArray *modelUrlArray = [model.bigPic componentsSeparatedByString:@"#"];
    NSMutableArray *imageUrlArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < modelUrlArray.count; i ++) {
        [imageUrlArray addObject:[NSString stringWithFormat:@"%@%@",IMAGE_SERVER_URL,modelUrlArray[i]]];
    }
    LogInfo(@"滚动图片地址：%@",imageUrlArray);
    
    //首页滑动海报
    _headerView = [[HeadScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    _headerView.backgroundColor = [UIColor clearColor];
    _headerView.m_delegate = self;
    [_headerView setDataFromHome:imageUrlArray];
    [_topView addSubview:_headerView];
    
    //按钮菜单
    _btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnMenu.frame = CGRectMake(SCREEN_WIDTH - 50, 110, 30, 30);
    _btnMenu.selected = NO;
    [_btnMenu setBackgroundImage:[UIImage imageNamed:@"project_bar_Share1_black"] forState:UIControlStateNormal];
    [_btnMenu addTarget:self action:@selector(btnMenuClick:) forControlEvents:UIControlEventTouchUpInside];
    [_headerView addSubview:_btnMenu];
    
    _btnMenuArray = [self createButtonArray];
    
    //标示图标
    UIImageView *hotIcon = [[UIImageView alloc]init];
    hotIcon.image = [UIImage imageNamed:@"project_hot"];
    [_topView addSubview:hotIcon];
    
    //标题
    UILabel *title = [[UILabel alloc]init];
    title.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
    title.text = model.projectName;
    title.textColor = [UIColor colorWithRed:76.0/255.0 green:76.0/255.0 blue:76.0/255.0 alpha:1.0f];
    [_topView addSubview:title];
    
    //发布日期
    UILabel *dateLB = [[UILabel alloc]init];
    dateLB.textColor = [UIColor lightGrayColor];
    dateLB.textAlignment = NSTextAlignmentRight;
    dateLB.font = [UIFont systemFontOfSize:12.0f];
    dateLB.text = [NSString stringWithFormat:@"发布日期：%@",[ZRBUtilities stringToData:@"yyyy年MM月dd日" interval:[NSString stringWithFormat:@"%@",model.pushTime]]];
    [_topView addSubview:dateLB];
    
    //内容
    UILabel *contentLB = [[UILabel alloc]init];
    contentLB.textColor = [UIColor lightGrayColor];
    contentLB.font = [UIFont systemFontOfSize:12.0f];
    contentLB.numberOfLines = 2;
    contentLB.text = model.intro;
    [_topView addSubview:contentLB];
    
    //认证图标
    UIImageView *inventIcon = [[UIImageView alloc]init];
    [_topView addSubview:inventIcon];
    
    //认证类型
    UILabel *inventType = [[UILabel alloc]init];
    inventType.textAlignment = NSTextAlignmentCenter;
    inventType.font = [UIFont systemFontOfSize:12.0f];
    [_topView addSubview:inventType];
    
    inventIcon.image = [UIImage imageNamed:[self iconWithValidateType:model.isValidate label:inventType]];
    
    //估值
    UILabel *price = [[UILabel alloc]init];
    price.font = [UIFont systemFontOfSize:12.0f];
    price.textColor = [UIColor lightGrayColor];
    price.text = [NSString stringWithFormat:@"估值：%@万",model.investmentStr];
    price.backgroundColor = [UIColor clearColor];
    [_topView addSubview:price];
    
    //分割线
    UIView *line = [[UIImageView alloc]init];
    line.backgroundColor = [UIColor lightGrayColor];
    [_topView addSubview:line];
    
    //地址
    UILabel *locationLB = [[UILabel alloc]init];
    locationLB.font = [UIFont systemFontOfSize:12.0f];
    locationLB.textColor = [UIColor lightGrayColor];
    locationLB.text = model.city;
    locationLB.backgroundColor = [UIColor clearColor];
    [_topView addSubview:locationLB];
    
    
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topView.mas_top);
        make.left.equalTo(_topView.mas_left);
        make.right.equalTo(_topView.mas_right);
        make.height.equalTo(@150);
    }];
    
    [hotIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@20);
        make.height.equalTo(@32);
        make.left.equalTo(_topView.mas_left).offset(10);
        make.top.equalTo(_headerView.mas_bottom).offset(10);
    }];
    
    [dateLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headerView.mas_bottom).offset(10);
        make.right.equalTo(_topView.mas_right).offset(-10);
        //        make.width.equalTo(@30);
    }];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(hotIcon.mas_right).offset(10);
        make.right.equalTo(dateLB.mas_left).offset(-10);
        make.top.equalTo(_headerView.mas_bottom).offset(10);
        //        make.width.equalTo(@30);
    }];
    
    [contentLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(hotIcon.mas_right).offset(10);
        make.right.equalTo(_topView.mas_right).offset(-10);
        make.top.equalTo(title.mas_bottom).offset(5);
        //        make.height.equalTo(@60);
    }];
    
    [inventIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@14);
        make.height.equalTo(@16);
        make.left.equalTo(_topView.mas_left).offset(10);
        make.bottom.equalTo(_topView.mas_bottom).offset(-10);
    }];
    
    [inventType mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(inventIcon.mas_right).offset(5);
        make.top.equalTo(inventIcon.mas_top);
        make.bottom.equalTo(inventIcon.mas_bottom);
        //        make.width.equalTo(@60);
    }];
    
    [price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_topView.mas_right).offset(-10);
        make.top.equalTo(inventIcon.mas_top);
        make.bottom.equalTo(inventIcon.mas_bottom);
        //        make.width.equalTo(@60);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@1);
        make.top.equalTo(inventIcon.mas_top);
        make.bottom.equalTo(inventIcon.mas_bottom);
        make.right.equalTo(price.mas_left).offset(-5);
    }];
    
    [locationLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(line.mas_right).offset(-5);
        make.top.equalTo(inventIcon.mas_top);
        make.bottom.equalTo(inventIcon.mas_bottom);
        //        make.width.equalTo(@30);
    }];
}

/**
 *  初始化按钮菜单中的其他按钮
 */
- (NSMutableArray *)createButtonArray {
    NSMutableArray *buttonsMutable = [[NSMutableArray alloc] init];
    //正常状态下按钮图片
    NSMutableArray *imageArray = [[NSMutableArray alloc]initWithObjects:@"project_bar_Share_black",@"project_bar_follow_off_black",
                                @"project_bar_comment_black",nil];
    //按钮高亮图片
    NSMutableArray *imageHLArray = [[NSMutableArray alloc]initWithObjects:@"project_bar_Share",@"project_bar_follow_off",
                                    @"project_bar_comment",nil];
    
    for (int i = 0;i < imageArray.count;i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setBackgroundImage:[UIImage imageNamed:imageArray[i]] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:imageHLArray[i]] forState:UIControlStateHighlighted];
         button.frame = CGRectMake(SCREEN_WIDTH - 50, 110, 30, 30);
         button.tag = i;
        [button addTarget:self action:@selector(btnMenuOtherClick:) forControlEvents:UIControlEventTouchUpInside];
        [buttonsMutable addObject:button];
         button.hidden = YES;
        
        [_headerView addSubview:button];
    }
    return [buttonsMutable copy];
}

- (void)btnMenuOtherClick:(id)sender{
    [self showMenu:NO];
    
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case ProjectShareType:          //分享
            
            break;
        case ProjectCollectType:        //关注
        {
            if ([ZRB_UserManager isLogin]) {
                ProjectItemModel *model = [_bannerArray objectAtIndex:0];
                [self collectionProject:model.projectId];
            }else{
                MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
                [self.navigationController pushViewController:loginCtrl animated:YES];
            }
            break;
        }
        case ProjectCommentType:        //评论
        {
            if ([ZRB_UserManager isLogin]) {
                WriteCommentViewController *writeCommentCtrl = [[WriteCommentViewController alloc]init];
                [self.navigationController pushViewController:writeCommentCtrl animated:YES];
            }else{
                MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
                [self.navigationController pushViewController:loginCtrl animated:YES];
            }
           
            break;
        }
        default:
            break;
    }
}

/**
 *  显示或者隐藏按钮菜单
 *  
 *  @param isShow  显示和隐藏标志
 */
- (void)showMenu:(BOOL)isShow{
   
    for(int i= 0; i < _btnMenuArray.count; i++){
        UIButton *btn = [_btnMenuArray objectAtIndex:i];
        
        _btnMenu.selected = isShow;
         if (isShow) {
             //改变主按钮图片
            [_btnMenu setBackgroundImage:[UIImage imageNamed:@"project_bar_Share1_black_on"] forState:UIControlStateNormal];
            //改变子按钮位置
             [UIView animateWithDuration:.3 animations:^{
                 btn.hidden = NO;
                 btn.frame = CGRectMake(SCREEN_WIDTH - 90-i*40, 110, 30, 30);
             } completion:^(BOOL finished) {
                 
             }];
             
         }else{
             [_btnMenu setBackgroundImage:[UIImage imageNamed:@"project_bar_Share1_black"] forState:UIControlStateNormal];
             
             [UIView animateWithDuration:.3 animations:^{
                 btn.frame = CGRectMake(SCREEN_WIDTH - 50, 110, 30, 30);
             } completion:^(BOOL finished) {
                 btn.hidden = YES;
             }];
         }
    }
}

- (void)btnMenuClick:(id)sender{
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    if (btn.selected) {
        [btn setBackgroundImage:[UIImage imageNamed:@"project_bar_Share1_black_on"] forState:UIControlStateNormal];
    }else{
        [btn setBackgroundImage:[UIImage imageNamed:@"project_bar_Share1_black"] forState:UIControlStateNormal];
    }
    
    [self showMenu:btn.selected];
}


- (NSString *)iconWithValidateType:(int)validateType label:(UILabel *)label{
    switch (validateType) {
            
        case 701:       //未认证
            label.text = @"未认证";
            label.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
            return @"project_telephone_authentication";
            break;
        case 702:       //电话认证
            label.text = @"电话认证";
            label.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
            return @"project_telephone_authentication";
            break;
        case 703:       //现场认证
            label.text = @"现场认证";
            label.layer.borderColor = [UIColor colorWithRed:233.0/255.0 green:98.0/255.0 blue:52.0/255.0f alpha:1.0].CGColor;
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:98.0/255.0 blue:52.0/255.0f alpha:1.0];
            
            return @"project_field_certification";
            break;
        case 704:       //授权认证
            label.text = @"授权认证";
            label.layer.borderColor = [UIColor colorWithRed:57.0/255.0 green:187.0/255.0 blue:148.0/255.0f alpha:1.0].CGColor;
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = [UIColor colorWithRed:57.0/255.0 green:187.0/255.0 blue:148.0/255.0f alpha:1.0];
            return @"project_authorization_authentication";
            break;
        default:
            label.text = @"未认证";
            label.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
            return @"project_telephone_authentication";
            break;
    }
}

//广告点击响应事件
- (void)posterPressed:(NSInteger)index
{
    LogInfo(@"首页广告点击:%ld",(long)index);
}




/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _projectTableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData:_areaModel.postcode];
    }];
    
}


/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _projectTableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData:_areaModel.postcode];
    }];
}




#pragma delegate of UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return _bannerArray.count;
    }else{
        return _projectArray.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        return 250;
    }else{
        return 110;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ProjectCell";
    ProjectListCell *cell = (ProjectListCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[ProjectListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.section == 0) {
        for (UIView *vw in cell.contentView.subviews) {
            [vw removeFromSuperview];
        }
        ProjectItemModel *model = _bannerArray[indexPath.row];
        [self initBannerView:model];
        [cell.contentView addSubview:_topView];
        
    }else{
        ProjectItemModel *model = _projectArray[indexPath.row];
        [cell initWithModel:model];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProjectItemModel *model = [[ProjectItemModel alloc]init];
    if (indexPath.section == 0) {
        model = _bannerArray[0];
    }else if(indexPath.section == 1){
        model = _projectArray[indexPath.row];
    }
    ProjectDetailViewController *detailCtrl = [[ProjectDetailViewController alloc]init];
    detailCtrl.projectID = model.autoId;
    detailCtrl.projectName = model.projectName;
    detailCtrl.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailCtrl animated:YES];
}

#pragma 网络请求

/**
 *  项目关注
 */
- (void)collectionProject:(NSString *)projectID{
    CollectionProjectRequest *request = [[CollectionProjectRequest alloc]init];
    request.projectID = projectID;
    request.collectionType = 1;
    
    //指示器
    [SVProgressHUD show];
    //发起请求
    WS(bself);
    [_projectService collectionPorjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        CollectionModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
                [bself VCToast:@"关注成功"];
                break;
            }
            case ZRBHttpFailType:
                
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failture:^(NSError *error) {
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:@"关注失败"];
    }];
}


/**
 *  请求项目列表
 */
-(void)queryListData:(NSString *)postCode{
    
    _isRequst = NO;
    
    ProjectQueryListRequest *request = [[ProjectQueryListRequest alloc]init];
    request.limit = 10;
    request.pushTime = @"";
    request.postCode = postCode;
    
    [self startWithReq:request];
}

/**
 *  加载更多
 */
-(void)loadMoreData:(NSString *)postCode{
    
    ProjectItemModel *lastModel = [_projectArray lastObject];
    LogInfo([NSString stringWithFormat:@"%@",lastModel.pushTime]);
    ProjectQueryListRequest *request = [[ProjectQueryListRequest alloc]init];
    request.limit = 10;
    request.pushTime = [ZRBUtilities stringToData:@"yyyy-MM-dd HH:mm:ss" interval:lastModel.pushTime];
    request.postCode = postCode;
    LogInfo(request.pushTime);
    
    [self loadMoreWithReq:request];
}

/**
 *  下拉刷新
 *
 *  @param req 请求体
 */
- (void)startWithReq:(ProjectQueryListRequest *)req
{
    if (_isRequst) {
        return;
    }
    
    WS(bself);
    [_projectService projectQueryListWithRequest:req success:^(id responseObject) {
        
        [bself projectQueryListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            [bself queryListData:_areaModel.postcode];
        }];
    }];
}

/**
 *  上拉加载更多
 *
 *  @param req 请求体
 */
- (void)loadMoreWithReq:(ProjectQueryListRequest *)req
{
    WS(bself);
    [_projectService projectQueryListWithRequest:req success:^(id responseObject) {
        
        [bself loadMoreListCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        
    }];
}

/**
 * 下拉刷新请求回调
 */
- (void)projectQueryListCallBackWithObject:(ProjectListModel *)model
{
    [HUDManager removeHUDFromView:self.view];
    _isRequst = YES;
    
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            _bannerArray = model.bannerList;
            _projectArray = model.projectList;
            
            LogInfo(@"广告视图：%ld",_bannerArray.count);
            LogInfo(@"项目：%ld",_projectArray.count);
            if(_projectArray.count < 10){
                
                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                 _projectTableView.footer.hidden = YES;
            }else{
                 _projectTableView.footer.hidden = NO;
            }

            [_projectTableView reloadData];
            break;
        }
        case ZRBHttpFailType:
        {
            LogInfo(@"请求失败");
            [_projectTableView.header endRefreshing];
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
        default:
            break;
    }
    
    [_projectTableView.header endRefreshing];
    
}

/**
 *  加载更多请求回调
 */
- (void)loadMoreListCallBackWithObject:(ProjectListModel *)model
{
    switch (model.res)
    {
        case ZRBHttpSuccssType:
        {
            LogInfo(@"加载更多回调");
            [_projectArray addObjectsFromArray:model.projectList];
            [_projectTableView reloadData];
            
            if (model.projectList.count < 10) {
                LogInfo(@"已经加载完毕");
                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                _projectTableView.footer.hidden = YES;
            }else
            {
                
            }
            [_projectTableView.footer endRefreshing];
            break;
        }
        case ZRBHttpFailType:
        {
            [_projectTableView.header endRefreshing];
            
            break;
        }
        case ZRBHttpNoLoginType:
        {
            [_projectTableView.header endRefreshing];
            
            break;
        }
        default:
            break;
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
