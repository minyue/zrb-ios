//
//  AllCommentCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/18.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommentModel.h"
#import "UIButton+Addtions.h"
#import "NSDate+Addition.h"

@interface AllCommentCell : UITableViewCell

- (void)initWithModel:(AllCommentItemModel *)model;

@property (nonatomic,copy)UIImageView *icon;                //头像

@property (nonatomic,copy)UILabel *userameLB;               //用户名

@property (nonatomic,copy)UILabel *otherUsernameLB;         //@的用户名

@property (nonatomic,copy)UILabel *contentLB;               //评论内容

@property (nonatomic,copy)UILabel *dateLB;                  //评论时间

@property (nonatomic,copy)UILabel *replyLB;                 //回复

@property (nonatomic,copy)UIButton *replyBtn;               //回复按钮

@property (nonatomic,copy)UILabel *deleteLB;                //删除

@property (nonatomic,copy)UIButton *deleteBtn;              //删除按钮

@property (nonatomic,copy)UIView *cellLine;                 //自定义cell分割线
@end
