//
//  AllCommentCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/18.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "AllCommentCell.h"

@implementation AllCommentCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    UIView *view = [[UIView alloc]init];
    [self.contentView addSubview:view];
    
    //用户头像
    _icon = [[UIImageView alloc]init];
    [view addSubview:_icon];
    
    //用户名
    _userameLB = [[UILabel alloc]init];
    _userameLB.backgroundColor = [UIColor clearColor];
    _userameLB.font = [UIFont systemFontOfSize:14.0f];
    [_userameLB sizeToFit];
    [view addSubview:_userameLB];
    
    //@其他用户
    _otherUsernameLB = [[UILabel alloc]init];
    _otherUsernameLB.backgroundColor = [UIColor clearColor];
    _otherUsernameLB.font = [UIFont systemFontOfSize:14.0f];
    _otherUsernameLB.textColor = [UIColor colorWithHexString:@"e8802b"];
    [view addSubview:_otherUsernameLB];
    
    //评论内容
    _contentLB = [[UILabel alloc]init];
    _contentLB.backgroundColor = [UIColor clearColor];
    _contentLB.font = [UIFont systemFontOfSize:13.0f];
    _contentLB.textColor = [UIColor colorWithHexString:@"9c9c9c"];
    [_contentLB sizeToFit];
    [view addSubview:_contentLB];
    
    //评论日期
    _dateLB = [[UILabel alloc]init];
    _dateLB.backgroundColor = [UIColor clearColor];
    _dateLB.font = [UIFont systemFontOfSize:10.0f];
    _dateLB.textColor = [UIColor colorWithHexString:@"a8a8a8"];
    [view addSubview:_dateLB];
    
    //回复
    _replyLB = [[UILabel alloc]init];
    _replyLB.backgroundColor = [UIColor clearColor];
    _replyLB.text = @"回复";
    _replyLB.textColor = [UIColor colorWithHexString:@"b1b1b1"];
    _replyLB.font = [UIFont systemFontOfSize:12.0f];
    [view addSubview:_replyLB];
    
    //回复评论按钮
    _replyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _replyBtn.backgroundColor = [UIColor clearColor];
    [_replyBtn setEnlargeEdgeWithTop:0.0f right:0.0f bottom:0.0f left:20.0f];
    [_replyBtn setBackgroundImage:[UIImage imageNamed:@"project_reply_off"] forState:UIControlStateNormal];
    [_replyBtn setBackgroundImage:[UIImage imageNamed:@"project_reply_on"] forState:UIControlStateHighlighted];
    [view addSubview:_replyBtn];
    
    //删除
    _deleteLB = [[UILabel alloc]init];
    _deleteLB.backgroundColor = [UIColor clearColor];
    _deleteLB.text = @"删除";
    _deleteLB.textColor = [UIColor colorWithHexString:@"b1b1b1"];
    _deleteLB.font = [UIFont systemFontOfSize:12.0f];
    [view addSubview:_deleteLB];
    
    //删除按钮
    _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _deleteBtn.backgroundColor = [UIColor clearColor];
    [_deleteBtn setEnlargeEdgeWithTop:0.0f right:0.0f bottom:0.0f left:20.0f];
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"project_delet_comment"] forState:UIControlStateNormal];
    [_deleteBtn setBackgroundImage:[UIImage imageNamed:@"project_delet_comment"] forState:UIControlStateHighlighted];
    [view addSubview:_deleteBtn];
    
    //cell分割线
    _cellLine = [[UIView alloc]init];
    _cellLine.backgroundColor = [UIColor colorWithHexString:@"c3c2c7"];
    [view addSubview:_cellLine];
    
    /*--------------------------------------------------------------------------------*/
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.contentView.mas_width);
        make.height.equalTo(self.contentView.mas_height);
    }];
    
    [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@30);
        make.height.equalTo(@30);
        make.left.equalTo(self.mas_left).offset(10);
        make.top.equalTo(self.mas_top).offset(10);
    }];
    
    [_userameLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_icon.mas_right).offset(10);
        make.top.equalTo(_icon.mas_top).offset(5);
    }];
    
    [_otherUsernameLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_userameLB.mas_top);
        make.bottom.equalTo(_userameLB.mas_bottom);
        make.left.equalTo(_userameLB.mas_right).offset(10);
    }];
    
    [_contentLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_icon.mas_right).offset(10);
        make.top.equalTo(_userameLB.mas_bottom).offset(10);
    }];
    
    [_dateLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view.mas_bottom).offset(-10);
        make.left.equalTo(_icon.mas_right).offset(10);
    }];
    
    [_replyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).offset(-10);
        make.bottom.equalTo(view.mas_bottom).offset(-10);
    }];
    
    [_replyLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_replyBtn.mas_left).offset(-1);
        make.bottom.equalTo(view.mas_bottom).offset(-9);
    }];
    
    [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_replyLB.mas_left).offset(-10);
        make.bottom.equalTo(view.mas_bottom).offset(-13);
    }];
    
    [_deleteLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_deleteBtn.mas_left).offset(-1);
        make.bottom.equalTo(view.mas_bottom).offset(-9);
    }];

    [_cellLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(view.mas_width);
        make.bottom.equalTo(view.mas_bottom);
        make.height.equalTo(@0.5);
        make.left.equalTo(_icon.mas_right).offset(10);
    }];
}


- (void)initWithModel:(AllCommentItemModel *)model{
    
    if([model.uid isEqualToString:[ZRB_UserManager shareUserManager].userData.uid]){
        _deleteBtn.hidden = NO;
        _deleteLB.hidden = NO;
    }else{
        _deleteLB.hidden = YES;
        _deleteBtn.hidden = YES;
    }
    
    NSString *imageUrlStr = [NSString stringWithFormat:@"%@/%@",IMAGE_SERVER_URL,model.picUrl];
    [_icon sd_setImageWithURL:[NSURL URLWithString:imageUrlStr] placeholderImage:[UIImage imageNamed:@"my_project"]];
    
    //创建 NSMutableAttributedString
    NSString *titleStr =[NSString stringWithFormat:@"%@说：",model.uname];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
    //设置“xxx”(用户名)文字颜色
    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"e8802b"] range: NSMakeRange(0,model.uname.length)];
    //设置“说”文字颜色
    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithHexString:@"777777"] range: NSMakeRange(model.uname.length,1)];
    _userameLB.attributedText = attributedStr;
    
    _otherUsernameLB.text = model.targetName;
    
    _contentLB.text = model.content;
    
    _dateLB.text =  [self timeSinceNow:[ZRBUtilities stringToData:@"yyyy-MM-dd HH:mm" interval:model.ctime]];
}

/**
 *  目标时间距离当前时间多久
 *  @param dateStr  目标时间
 */
- (NSString *)timeSinceNow:(NSString *)dateStr{
    //创建日期格式化对象
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    //创建了两个日期对象 评论日期+当前日期
    NSDate *commentDate=[dateFormatter dateFromString:dateStr];
    NSDate *currentDate=[NSDate date];
    
    //取两个日期对象的时间间隔：
    //这里的NSTimeInterval 并不是对象，是基本型，其实是double类型，是由c定义的:typedef double NSTimeInterval;
    NSTimeInterval time=[currentDate timeIntervalSinceDate:commentDate];
    
    int days=((int)time)/(3600*24);
    int hours=((int)time)%(3600*24)/3600;
    
    if (days > 0) {
        if (hours > 0) {
            return [[NSString alloc] initWithFormat:@"%i天%i小时前",days,hours];
        }else{
            return [[NSString alloc] initWithFormat:@"%i天前",days];
        }
    }else{
        if (hours > 0) {
            return [[NSString alloc] initWithFormat:@"%i小时前",hours];
        }else{
            return [[NSString alloc] initWithFormat:@"刚刚"];
        }
    }
}

@end
