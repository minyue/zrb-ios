//
//  CityDetailView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/28.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityDetailView : UIView
{
    UILabel *_detailLB;
}

- (void)setCityDetail:(NSString *)detail;

@end
