//
//  CityDetailView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/28.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "CityDetailView.h"

@implementation CityDetailView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    
//    self.backgroundColor = [UIColor colorWithHexString:@"f4f4f4"];
    
    _detailLB = [[UILabel alloc]init];
    _detailLB.textColor = [UIColor colorWithHexString:@"6b6b6b"];
    _detailLB.font = [UIFont systemFontOfSize:12.0f];
    _detailLB.numberOfLines = 0;
    [self addSubview:_detailLB];
    
    WS(bself);
    [_detailLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bself.mas_left).offset(3);
        make.right.equalTo(bself.mas_right).offset(-3);
        make.top.equalTo(bself.mas_top).offset(3);
        make.bottom.equalTo(bself.mas_bottom).offset(-3);
    }];
}

- (void)setCityDetail:(NSString *)detail{
    _detailLB.text = detail;
}

@end
