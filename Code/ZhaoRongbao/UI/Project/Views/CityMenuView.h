//
//  CityMenuView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/31.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityMenuView : UIView

- (void)initMenuWithData;

@end
