//
//  CityTagView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/27.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityTagView : UIView

- (void)initWithData:(NSMutableArray *)array;

@end

@interface CityTagItem : UIView
{
     UILabel *_tagLabel;
}
- (void)setTagViewTitle:(NSString *)title;
@end