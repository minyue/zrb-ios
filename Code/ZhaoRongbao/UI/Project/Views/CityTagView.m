//
//  CityTagView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/27.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "CityTagView.h"


@implementation CityTagView

- (void)awakeFromNib
{
    
    [self setup];
}


- (instancetype)init{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}


- (void)setup
{
    self.backgroundColor = [UIColor yellowColor];
}



- (void)initWithData:(NSArray *)array
{
    
    CityTagItem *lastView = nil;
    UIView *superView = self;
    
    for (int i = 0; i < array.count; i ++){
        CityTagItem *itemView = [[CityTagItem alloc] init];
        [itemView  setTagViewTitle:array[i]];
        
        [self addSubview:itemView];
        
        [itemView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(superView.mas_top);
            if (!lastView) {
                make.left.equalTo(superView.mas_left);
                
            }else{
                make.left.equalTo(lastView.mas_right).offset(10);
                if (i == array.count - 1) {
                    make.right.equalTo(superView.mas_right);
                }
            }
        }];
        
        lastView = itemView ;
    }
}

@end


@implementation CityTagItem

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    return  self;
}


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    self.backgroundColor = [UIColor colorWithHexString:@"3994de"];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 10;
    
    _tagLabel = [[UILabel alloc]init];
    _tagLabel.backgroundColor = [UIColor clearColor];
    _tagLabel.textColor = [UIColor whiteColor];
    _tagLabel.font = [UIFont systemFontOfSize:10.0f];
    _tagLabel.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:_tagLabel];
    
    WS(bself);
    [_tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bself.mas_left).offset(8);
        make.right.equalTo(bself.mas_right).offset(-8);
        make.top.equalTo(bself.mas_top).offset(3);
        make.bottom.equalTo(bself.mas_bottom).offset(-3);
    }];
}

- (void)setTagViewTitle:(NSString *)title{
    _tagLabel.text = title;
}

@end
