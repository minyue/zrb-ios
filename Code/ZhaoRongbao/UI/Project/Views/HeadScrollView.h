/*!
 @header HeadScrollView
 @abstract 首页滑动海报
 @author 王松
 @version 1.0 2013/07/25建立
 */

#import <UIKit/UIKit.h>
@protocol HeadScrollDelegate <NSObject>
- (void)posterPressed:(NSInteger)index;
@end

@interface HeadScrollView : UIView <UIScrollViewDelegate>
{
    UIScrollView  *m_carScroll;
    UIPageControl *m_carPageControl;
    
    NSArray		*m_imagesAdresseAry;
    NSTimer     *m_viewSlideTimer;
    
    BOOL  didScroll;
    int   timeOut;
    int   sourceNums;
    id<HeadScrollDelegate>m_delegate;
}

@property id<HeadScrollDelegate>m_delegate;
@property (nonatomic ,strong) NSTimer        *m_viewSlideTimer;

- (void)setDataFromHome:(NSArray *)imgArr;

- (void)handleTimer:(NSTimer*)timer;
- (void)stopMove;
- (void)startMove;

@end
