////
////  HeadScrollView.m
////  DemoVod
////
////  Created by  on 12-6-26.
////  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
////
//


#import "HeadScrollView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#define VIEW_TAG_INDEX 10000

@implementation HeadScrollView

@synthesize m_viewSlideTimer;
@synthesize m_delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //UIScrollView
        m_carScroll=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [m_carScroll setBackgroundColor:[UIColor clearColor]];
        [m_carScroll setShowsHorizontalScrollIndicator:NO];
        [m_carScroll setShowsVerticalScrollIndicator:NO];
		[m_carScroll setPagingEnabled:YES];
		[m_carScroll setDelegate:self];
		[self addSubview:m_carScroll];
        
		//carPageControl
		m_carPageControl=[[UIPageControl alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-120, frame.size.height -20, 70, 20)];
        m_carPageControl.enabled = NO;
        m_carPageControl.hidden = YES;

        m_carPageControl.currentPage = 0;
        
        [self addSubview:m_carPageControl];
        
        [self initScrollView];
    }
    return self;
}



#pragma mark -
#pragma mark 自己定义方法

//定时器停止运行
- (void)stopMove 
{
    if ([m_viewSlideTimer isValid])
    {
        [m_viewSlideTimer invalidate];
        m_viewSlideTimer = nil;
    }
}

//定时器开始运行
- (void)startMove
{
    
    if ([m_imagesAdresseAry count]==0 || m_imagesAdresseAry == nil) 
    {
        return;
    }
    
    if ([m_viewSlideTimer isValid]) 
    {
        [m_viewSlideTimer invalidate];
        m_viewSlideTimer = nil;
    }
    
    m_viewSlideTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f 
                                                        target:self 
                                                      selector:@selector(handleTimer:) 
                                                      userInfo:nil 
                                                       repeats:YES];
}

//滑动视图界面图片初始化
- (void) initScrollView
{
    for (UIView *vw in m_carScroll.subviews)
    {
        if ([vw isKindOfClass:[UIImageView class]])
        {
            [vw removeFromSuperview];
        }
    }
    
    int nums = (int)[m_imagesAdresseAry count];
    sourceNums = nums;
    if (nums > 0)
    {
        m_carScroll.contentSize   = CGSizeMake((nums+1)*m_carScroll.frame.size.width, 0);
        
        for (int i = 0 ; i < nums+1; i++) 
        {

            UIImageView *scrollImg = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH)*i, 0, SCREEN_WIDTH, m_carScroll.frame.size.height)];
            NSString *urlStr = nil;
            if (i==nums)
            {
                urlStr = [m_imagesAdresseAry objectAtIndex:0];
            }
            else
            {
                urlStr = [m_imagesAdresseAry objectAtIndex:i];

            }
            
            NSLog(@"顶部图片地址：%@",urlStr);
            [scrollImg sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:@"default_icon"]];
            scrollImg.tag = VIEW_TAG_INDEX+i;
            scrollImg.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGestrue = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:@selector(tapAction:)];
            [scrollImg addGestureRecognizer:tapGestrue];
            [m_carScroll addSubview:scrollImg];
            
        } 
        
    }

    [self startMove];
}


//初始化首页数据源数组
- (void)setDataFromHome:(NSArray *)imgArr
{
    if (m_imagesAdresseAry) {
        m_imagesAdresseAry = nil;
    }

    m_imagesAdresseAry = [[NSArray alloc] initWithArray:imgArr];
    
    [m_carPageControl setNumberOfPages:m_imagesAdresseAry.count];
    
    [self initScrollView];
}

//定时器调用方法，海报每三秒滑动替换一次
- (void) handleTimer:(NSTimer*)timer
{
    
    if (didScroll)
    {
        timeOut=0;
        return;
    }
    
    if (timeOut<4)
    {
        timeOut+=1;
        return;
    }
    timeOut=0;
    
    
    CGPoint offset = m_carScroll.contentOffset;
    offset.x += m_carScroll.frame.size.width;
    
    float offset_x = ([m_imagesAdresseAry count]-1)*SCREEN_WIDTH;
    
    if (offset.x > offset_x )
    {
        offset.x = 0;
        m_carScroll.contentOffset = offset;
        m_carPageControl.currentPage = 0;
        
        [self performSelector:@selector(resetDidScroll) withObject:nil afterDelay:0.1];
        
        CATransition *animation = [CATransition animation];
        animation.duration  = 1.0f;
        animation.type = kCATransitionPush;
        animation.subtype = kCATransitionFromRight;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [m_carScroll.layer addAnimation:animation forKey:@"scrollAnimation"];
        
        return;
    }
    
    if (offset.x < 0 )
    {
        offset.x = offset_x;
        m_carScroll.contentOffset = offset;
        m_carPageControl.currentPage = [m_imagesAdresseAry count]-1;
        
        return;
    } 
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [self performSelector:@selector(resetDidScroll) withObject:nil afterDelay:0.1];
    
    m_carScroll.contentOffset = offset;
    m_carPageControl.currentPage = offset.x / m_carScroll.frame.size.width;

    [UIView commitAnimations];
    
}

-(void)resetDidScroll
{
    didScroll=NO;
}

#pragma mark - 
#pragma ScrollView Delegate


// 将要开始拖拽，手指已经放在view上并准备拖动的那一刻
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    didScroll = YES;
    timeOut=0;
}


-(void) scrollViewDidScroll:(UIScrollView *)scrollView 
{
    int offsetX = scrollView.contentOffset.x;
	if(offsetX < 0)
    {
		[scrollView setContentOffset:CGPointMake(SCREEN_WIDTH*sourceNums, 0)];
	}
	if(offsetX > SCREEN_WIDTH*sourceNums)
    {
		[scrollView setContentOffset:CGPointMake(0, 0)];
	}
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    didScroll = NO;
    
    int offsetX = scrollView.contentOffset.x;
    
	if(offsetX/SCREEN_WIDTH == sourceNums)
    {
		[scrollView setContentOffset:CGPointMake(0, 0)];
        m_carPageControl.currentPage = 0;
	}
    
	if((offsetX)%((int)SCREEN_WIDTH) == 0)
    {
		[m_carPageControl setCurrentPage:scrollView.contentOffset.x/SCREEN_WIDTH];
	}
}

- (void)tapAction:(UITapGestureRecognizer *)gesture
{
    int index = (int)(gesture.view.tag - VIEW_TAG_INDEX);
    if (index == sourceNums) 
    {
        return;
    }else
    {
        if (self.m_delegate && [self.m_delegate respondsToSelector:@selector(posterPressed:)])
        {
            [self.m_delegate posterPressed:index];
        }
    }
}

#pragma mark -
#pragma mark dealloc
- (void)dealloc
{
    if ([m_viewSlideTimer isValid]) 
    {
        [m_viewSlideTimer invalidate];
        m_viewSlideTimer = nil;
    }
}


@end
