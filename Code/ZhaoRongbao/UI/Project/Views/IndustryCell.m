//
//  IndustryCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/31.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "IndustryCell.h"

@implementation IndustryCell

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    _value = [[UILabel alloc]init];
    _value.backgroundColor = [UIColor clearColor];
    _value.textColor = [UIColor colorWithHexString:@"898989"];
    _value.font = [UIFont systemFontOfSize:13.0f];
    [self.contentView addSubview:_value];
    
    [_value mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
}

@end
