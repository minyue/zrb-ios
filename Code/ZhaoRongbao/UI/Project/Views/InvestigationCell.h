//
//  InvestigationCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/8/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZRB_TextField.h"

@interface InvestigationCell : UITableViewCell

@property (nonatomic,strong)UILabel *name;
@property (nonatomic,strong)UITextField *textField;

@end
