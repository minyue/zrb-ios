//
//  InvestigationCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/8/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "InvestigationCell.h"

@implementation InvestigationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    _name = [[UILabel alloc]init];
    _name.backgroundColor = [UIColor clearColor];
    _name.font = [UIFont systemFontOfSize:15.0f];
    _name.textColor = [UIColor colorWithHexString:@"6a6c72"];
    [_name sizeToFit];
    [self.contentView addSubview:_name];
    
    _textField = [[UITextField alloc]init];
    _textField.font = [UIFont systemFontOfSize:15.0f];
    _textField.textColor = [UIColor colorWithHexString:@"b5b5b5"];
    _textField.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_textField];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor colorWithHexString:@"c3c2c7"];
    [self.contentView addSubview:line];
    
    [_name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@70);
    }];
    
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_name.mas_right);
        make.right.equalTo(self.contentView.mas_right);
        make.top.equalTo(self.contentView.mas_top);
        make.height.equalTo(self.contentView.mas_height);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.contentView.mas_width);
        make.height.equalTo(@0.5);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}

@end
