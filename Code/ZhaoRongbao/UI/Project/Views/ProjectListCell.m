//
//  ProjectListCell.m
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/12.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ProjectListCell.h"

@implementation ProjectListCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}


- (void)setup
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:view];
    
    //左边大图
    _bigPic = [[UIImageView alloc]init];
    _bigPic.backgroundColor = [UIColor clearColor];
    _bigPic.contentScaleFactor = [[UIScreen mainScreen] scale];
    _bigPic.contentMode = UIViewContentModeScaleAspectFit;
    _bigPic.backgroundColor = [UIColor colorWithHexString:@"e4e4e4"];
    [view addSubview:_bigPic];
    
    //左边大图，中的类型图
    _typePic = [[UIImageView alloc]init];
    [_typePic setImage:[UIImage imageNamed:@""]];
    [_bigPic addSubview:_typePic];
    
    //项目标题
    _title = [[BTLabel alloc]init];
    _title.numberOfLines = 0;
    _title.textAlignment = NSTextAlignmentLeft;
    _title.verticalAlignment = BTVerticalAlignmentTop;
    [_title setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
    [view addSubview:_title];
    
    //认证图标
    _certificationPic = [[UIImageView alloc]init];
    [view addSubview:_certificationPic];
    
    //认证类型名称
    _certificationLB = [[UILabel alloc]init];
    _certificationLB.font = [UIFont systemFontOfSize:13.0f];
    _certificationLB.layer.borderWidth = 1.0;
    _certificationLB.textAlignment = NSTextAlignmentCenter;
    [view addSubview:_certificationLB];
    
    //面积
    _areaLB = [[UILabel alloc]init];
    _areaLB.font = [UIFont systemFontOfSize:12.0f];
    _areaLB.textColor = [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0];
    _areaLB.backgroundColor = [UIColor clearColor];
    [view addSubview:_areaLB];
    
    //分割线
    _line = [[UIView alloc]init];
    _line.backgroundColor = [UIColor lightGrayColor];
    [view addSubview:_line];
    
    //估值
    _price = [[UILabel alloc]init];
    _price.font = [UIFont systemFontOfSize:12.0f];
    _price.textColor = [UIColor colorWithRed:83.0/255.0 green:158.0/255.0 blue:223.0/255.0 alpha:1.0];
    _price.backgroundColor = [UIColor clearColor];
    [view addSubview:_price];
    
    //日期
    _date = [[UILabel alloc]init];
    _date.font = [UIFont systemFontOfSize:12.0f];
    _date.textColor = [UIColor lightGrayColor];
    [_date sizeToFit];
    _date.backgroundColor = [UIColor clearColor];
    [view addSubview:_date];
    
    //地址
    _location = [[UILabel alloc]init];
    _location.font = [UIFont systemFontOfSize:12.0f];
    _location.textColor = [UIColor lightGrayColor];
    [_location sizeToFit];
    _location.backgroundColor = [UIColor clearColor];
    [view addSubview:_location];
    
    
    
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.contentView.mas_width);
        make.height.equalTo(self.contentView.mas_height);
    }];
    
    [_bigPic makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(10);
        make.centerY.equalTo(view.mas_centerY);
        make.width.equalTo(@120);
        make.height.equalTo(@90);
    }];
    
    [_typePic makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@30);
        make.height.equalTo(@30);
        make.left.equalTo(_bigPic.mas_left).offset(10);
        make.bottom.equalTo(_bigPic.mas_bottom).offset(-10);
        
    }];
    
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bigPic.mas_top);
        make.left.equalTo(_bigPic.mas_right).offset(10);
        make.right.equalTo(view.mas_right).offset(-10);
    }];
    
    [_certificationPic mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bigPic.mas_right).offset(10);
        make.top.equalTo(_title.mas_bottom).offset(10);
        make.width.equalTo(@14);
        make.height.equalTo(@16);
        
    }];
    
    [_certificationLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_certificationPic.mas_top);
        make.bottom.equalTo(_certificationPic.mas_bottom);
        make.left.equalTo(_certificationPic.mas_right).offset(10);
        make.width.equalTo(@60);
    }];
    
    [_areaLB mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_certificationLB.mas_bottom).offset(10);
        make.left.equalTo(_bigPic.mas_right).offset(10);
        make.bottom.equalTo(view.mas_bottom).offset(-10);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_areaLB.mas_right).offset(5);
        make.top.equalTo(_areaLB.mas_top);
        make.bottom.equalTo(_areaLB.mas_bottom);
        make.width.equalTo(@1);
        
    }];
    
    [_price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_line.mas_right).offset(5);
        make.top.equalTo(_areaLB.mas_top);
        make.bottom.equalTo(_areaLB.mas_bottom);
        make.width.equalTo(@90);
    }];
    
    [_date mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).offset(-10);
        make.top.equalTo(_price.mas_top);
        make.bottom.equalTo(_price.mas_bottom);
//        make.width.equalTo(@60);
    }];
    
    [_location mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_date.mas_left).offset(-2);
        make.top.equalTo(_price.mas_top);
        make.bottom.equalTo(_price.mas_bottom);
//        make.width.equalTo(@40);
    }];
}


- (void)initWithModel:(ProjectItemModel *)model{
    //大图和资产类型图
    [_bigPic sd_setImageWithURL:[ZRBUtilities urlWithPath:model.pic] placeholderImage:[ZRBUtilities ZRB_DefaultImage] options:SDWebImageRetryFailed];
    _typePic.image = [UIImage imageNamed:[self iconWithProjectType:model.projectType]];
    
    //标题
    if([model.investmentStatus isEqualToString:@"1402"]){
        [_title setText:[NSString stringWithFormat:@"【公告期】%@",model.projectName]];
    }else{
        [_title setText:[NSString stringWithFormat:@"%@",model.projectName]];
    }
    
    
    
    //认证图标和类型
    _certificationPic.image = [UIImage imageNamed:[self iconWithValidateType:model.isValidate]];
    
    //m面积
    _areaLB.text = [NSString stringWithFormat:@"%d亩",model.area];
    
    //估值
    _price.text = [NSString stringWithFormat:@"估值:%@万",model.investmentStr];
    
    //日期
    _date.text = [ZRBUtilities stringToData:@"MM/dd" interval:[NSString stringWithFormat:@"%@",model.pushTime]];
    
    //地址
    _location.text = [model.city substringToIndex:model.city.length-1];
    
}

//资产类型图片
- (NSString *)iconWithProjectType:(int)projectType
{
    switch (projectType) {
        case 101:
            
            return @"project_place_plable";
            break;
        case 102:
            return @"project_ppp_plabel";
            break;
        case 103:
            return @"project_assets_plable";
            break;
        default:
            return nil;
            break;
    }
}

- (NSString *)iconWithValidateType:(int)validateType{
    switch (validateType) {
            
        case 701:       //未认证
            _certificationLB.hidden = YES;
            _certificationLB.text = @"未认证";
            _certificationLB.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
            _certificationLB.textColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
            return @"";
            break;
        case 702:       //电话认证
            _certificationLB.text = @"电话认证";
            _certificationLB.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
            _certificationLB.textColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
            return @"project_telephone_authentication";
            break;
        case 703:       //现场认证
            _certificationLB.text = @"现场认证";
            _certificationLB.layer.borderColor = [UIColor colorWithRed:233.0/255.0 green:98.0/255.0 blue:52.0/255.0f alpha:1.0].CGColor;
            _certificationLB.textColor = [UIColor colorWithRed:233.0/255.0 green:98.0/255.0 blue:52.0/255.0f alpha:1.0];
            
            return @"project_field_certification";
            break;
        case 704:       //授权认证
            _certificationLB.text = @"授权认证";
            _certificationLB.layer.borderColor = [UIColor colorWithRed:57.0/255.0 green:187.0/255.0 blue:148.0/255.0f alpha:1.0].CGColor;
            _certificationLB.textColor = [UIColor colorWithRed:57.0/255.0 green:187.0/255.0 blue:148.0/255.0f alpha:1.0];
            return @"project_authorization_authentication";
            break;
        default:
            _certificationLB.text = @"未认证";
            _certificationLB.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
            _certificationLB.textColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
            return @"project_telephone_authentication";
            break;
    }
}

@end
