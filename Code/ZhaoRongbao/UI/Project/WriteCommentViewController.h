//
//  WriteCommentViewController.h
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/14.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "QRadioButton.h"
#import "ProjectService.h"
#import "ProjectTypeModel.h"
#import "ProjectCommentModel.h"
#import "CustomBarItem.h"
#import "UINavigationItem+CustomItem.h"

@interface WriteCommentViewController : ZRB_ViewControllerWithBackButton<QRadioButtonDelegate,UITextViewDelegate>
{
    ProjectService *_projectService;
    UILabel *_statusLabel;          //显示评论文字字数限制
    UITextView *_commentTV;
}

@property (nonatomic,strong) NSMutableArray *projectTypeArray;   //问题类型数组

@property (nonatomic,strong) NSString *projectID;                //项目ID
@property (nonatomic,strong) NSString *questionType;             //提交评论的问题类型
@property (nonatomic,strong) NSString *otherUsername;            //被回复人的用户名

@end
