//
//  WriteCommentViewController.m
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/14.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "WriteCommentViewController.h"

@interface WriteCommentViewController ()

@end

@implementation WriteCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _projectService = [[ProjectService alloc]init];
    
    [self getProjectType];
    //    [self setup];
}

- (void)setup:(NSMutableArray *)projectTypeArray{
    
    self.otherUsername = @"我又不乱来";
    
    self.questionType = 0;
    
    CustomBarItem * _rightItem =  [self.navigationItem setItemWithTitle:@"发送" textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:right];
    [_rightItem setOffset:-2];
    [_rightItem addTarget:self selector:@selector(sendComment:) event:UIControlEventTouchUpInside];
    
    
    self.title = @"写评论";
    self.view.backgroundColor = [UIColor colorWithRed:240.0/255.0f green:240.0/255.0f blue:240.0/255.0f alpha:1.0];
    
    //富文本标题
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [titleLabel sizeToFit];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    
    //创建 NSMutableAttributedString
    NSString *titleStr = @"选择项目类型(必填*)";
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:titleStr];
    //设置“选择项目类型”文字颜色
    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithRed:38.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0] range: NSMakeRange(0,6)];
    //设置“(必填*)”文字颜色
    [attributedStr addAttribute: NSForegroundColorAttributeName value: [UIColor colorWithRed:114.0/255.0 green:155.0/255.0 blue:220.0/255.0 alpha:1.0] range: NSMakeRange(6,5)];
    
    titleLabel.attributedText = attributedStr;
    [self.view addSubview:titleLabel];
    
    
    //单选按钮父视图
    UIView *buttonGroup  = [[UIView alloc]init];
    buttonGroup.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:buttonGroup];
    
    //分割线
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = [UIColor lightGrayColor];
    [buttonGroup addSubview:line];
    
    //评论输入框
    _commentTV = [[UITextView alloc]init];
    _commentTV.delegate = self;
    _commentTV.backgroundColor = [UIColor whiteColor];
    _commentTV.font = [UIFont systemFontOfSize:14.0f];
    _commentTV.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:_commentTV];
    
    //被@的用户名，没有则不显示
    UILabel *otherUsernameLB = [[UILabel alloc]init];
    otherUsernameLB.font = [UIFont systemFontOfSize:14.0f];
    otherUsernameLB.backgroundColor = [UIColor clearColor];
    otherUsernameLB.textColor = [UIColor colorWithHexString:@"e8802b"];
    otherUsernameLB.text = [NSString stringWithFormat:@"@%@",self.otherUsername];
//    [self.view addSubview:otherUsernameLB];
    if (self.otherUsername.length < 1) {
        otherUsernameLB.hidden = YES;
    }else{
        _commentTV.text = [NSString stringWithFormat:@"@%@",self.otherUsername];
    }
    
    //字数限制显示
    _statusLabel = [[UILabel alloc]init];
    _statusLabel.text = @"0/240";
    _statusLabel.backgroundColor = [UIColor clearColor];
    _statusLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    _statusLabel.textColor = [UIColor colorWithRed:144.0/255.0 green:144.0/255.0 blue:144.0/255.0 alpha:1.0];
    [_statusLabel sizeToFit];
    [self.view addSubview:_statusLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.top.equalTo(self.view.mas_top).offset(10);
    }];
    
    [buttonGroup mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(@90);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(buttonGroup.mas_left).offset(10);
        make.right.equalTo(buttonGroup.mas_right);
        make.height.equalTo(@0.5);
        make.centerY.equalTo(buttonGroup.mas_centerY);
    }];
    
    
    for (int i = 0; i < projectTypeArray.count; i ++) {
        
        ProjectTypeItemModel *model = [projectTypeArray objectAtIndex:i];
        
        QRadioButton *radioBtn = [[QRadioButton alloc] initWithDelegate:self groupId:@"groupId"];
        radioBtn.tag = i;
        radioBtn.delegate = self;
        [radioBtn setTitleColor:[UIColor colorWithRed:38.0/255.0 green:38.0/255.0 blue:38.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [radioBtn setTitle:model.itemName forState:UIControlStateNormal];
        [radioBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0f]];
        [buttonGroup addSubview:radioBtn];
        
        if(i < 4){
            [radioBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(SCREEN_WIDTH*1/4));
                make.height.equalTo(@45);
                make.left.equalTo(buttonGroup.mas_left).offset(SCREEN_WIDTH*i/4+10);
                make.top.equalTo(buttonGroup.mas_top);
            }];
        }else{
            [radioBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(SCREEN_WIDTH*2/7));
                make.height.equalTo(@45);
                make.left.equalTo(buttonGroup.mas_left).offset(SCREEN_WIDTH*(i-4)/4+10);
                make.bottom.equalTo(buttonGroup.mas_bottom);
            }];
        }
    }
    
    [_commentTV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.equalTo(@200);
        make.top.equalTo(buttonGroup.mas_bottom).offset(10);
    }];
    
//    [otherUsernameLB mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_commentTV.mas_left).offset(5);
//        make.top.equalTo(_commentTV.mas_top).offset(10);
//    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_commentTV.mas_right);
        make.top.equalTo(_commentTV.mas_bottom).offset(10);
    }];
}


#pragma 网络请求和回调

//发送评论
- (void)sendComment:(id)sender{
    
    if (self.questionType != 0) {
        if ([ZRBUtilities isBlankString:_commentTV.text]) {
            [self showTipViewWithMsg:@"评论内容不能为空"];
        }
    }else{
        
        [self showTipViewWithMsg:@"请选择问题类型"];
    }
    
    if (self.questionType !=0 && ![ZRBUtilities isBlankString:_commentTV.text]) {
        //网络请求指示器
        [HUDManager showLoadNetWorkHUDInView:self.view];
        //初始化请求类
        ProjectCommentRequest *request = [[ProjectCommentRequest alloc]init];
        request.pid = self.projectID;
        request.questionType = self.questionType;
        request.content = _commentTV.text;
        
        WS(bself);
        [_projectService sendProjectCommentWithRequest:request success:^(id responseObject) {
            [HUDManager removeHUDFromView:self.view];
            [bself projectCommentCallBackWithObject:responseObject];
            
        } failure:^(NSError *error) {
            [HUDManager removeHUDFromView:self.view];
            
        }];
    }
}

/**
 *  获取项目类型
 */
- (void)getProjectType{
    //网络请求指示器
    [HUDManager showLoadNetWorkHUDInView:self.view];
    //开始iqingqiu
    ProjectTypeRequest *request = [[ProjectTypeRequest alloc]init];
    
    WS(bself);
    [_projectService projectTypeWithRequest:request success:^(id responseObject) {
        
        [HUDManager removeHUDFromView:self.view];
        [bself projectTypeCallBackWithObject:responseObject];
    } failure:^(NSError *error) {
        [HUDManager removeHUDFromView:self.view];
    }];
}

/**
 *   提交评论回调
 */
- (void)projectCommentCallBackWithObject:(ProjectCommentModel *)model{
    switch (model.res) {
        case ZRBHttpSuccssType:
            LogInfo(@"评论提交成功");
            
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case ZRBHttpFailType:
        {
            [self showTipViewWithMsg:@"评论提交失败"];
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
            
        default:
            break;
    }
}


/**
 *  项目类型请求回调
 */
- (void)projectTypeCallBackWithObject:(ProjectTypeModel *)model{
    _projectTypeArray = [[NSMutableArray alloc]init];
    
    switch (model.res) {
        case ZRBHttpSuccssType:
        {
            _projectTypeArray = model.data;
            [self setup:_projectTypeArray];
            break;
        }
        case ZRBHttpFailType:
        {
            LogInfo(@"请求失败");
            break;
        }
        case ZRBHttpNoLoginType:
        {
            break;
        }
        default:
            break;
            
    }
}

#pragma delegate of RadioButton
- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId{
    ProjectTypeItemModel *model = [_projectTypeArray objectAtIndex:radio.tag];
    self.questionType = model.itemCode;
}


#pragma delegate of UITextView
- (void)textViewDidChange:(UITextView *)textView {
    
    NSInteger number = [textView.text length];
    if (number > 240) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"字符个数不能超过240" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        textView.text = [textView.text substringToIndex:240];
        number = 240;
    }
    _statusLabel.text = [NSString stringWithFormat:@"%ld/240",(long)number];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_commentTV resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
