//
//  BlankRemindView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/9.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlankRemindView : UIView

@property (nonatomic,copy) UIImageView *remindImage;

@property (nonatomic,copy) UILabel *remindInfo;

- (void)showInView:(UIView *)superView;

@end
