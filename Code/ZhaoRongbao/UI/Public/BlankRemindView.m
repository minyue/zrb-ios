//
//  BlankRemindView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/9.
//  Copyright © 2015年 zouli. All rights reserved.
//  列表页面空白提醒视图

#import "BlankRemindView.h"

@implementation BlankRemindView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    
//    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
//    self.backgroundColor = [UIColor yellowColor];

//    UIView *view = [[UIView alloc]init];
//    view.backgroundColor = [UIColor clearColor];
//    [self addSubview:view];
    
    _remindImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"blank_remind"]];
    [self addSubview:_remindImage];
    
    _remindInfo = [[UILabel alloc]init];
    _remindInfo.backgroundColor = [UIColor clearColor];
    _remindInfo.textAlignment = NSTextAlignmentCenter;
    _remindInfo.textColor = [UIColor colorWithHexString:@"999999"];
    _remindInfo.font = [UIFont systemFontOfSize:17.0f];
    _remindInfo.text = @"暂无相关信息，敬请期待";
    [self addSubview:_remindInfo];
    
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.mas_centerX);
//        make.centerY.equalTo(self.mas_centerY);
//    }];
    
    [_remindImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@93);
        make.height.equalTo(@105);
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_top);
    }];
    
    [_remindInfo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_remindImage.mas_bottom).offset(30);
        make.bottom.equalTo(self.mas_bottom);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
}

- (void)showInView:(UIView *)superView{
    
    if (![superView.subviews containsObject:self]) {
        
        superView.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
        [superView addSubview:self];
        
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(superView.mas_centerX);
            make.centerY.equalTo(superView.mas_centerY);
        }];
    }
}


@end
