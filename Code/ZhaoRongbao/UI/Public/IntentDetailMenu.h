//
//  IntentDetailMenu.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/12.
//  Copyright © 2015年 songmk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntentDetailMenu : UIImageView

@property (nonatomic,copy) UIImageView *userIcon;

@property (nonatomic,copy) UILabel *job;
@property (nonatomic,copy) UILabel *company;

@property (nonatomic,copy) UIButton *personalInfo;

@end
