//
//  IntentDetailMenu.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/12.
//  Copyright © 2015年 songmk. All rights reserved.
//  项目和资金详情页面悬浮视图

#import "IntentDetailMenu.h"

@implementation IntentDetailMenu

- (instancetype)init{
    
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    
    self.image = [UIImage imageNamed:@"intent_detail_menu_bg"];
    self.userInteractionEnabled = YES;
    
    //用户头像
    _userIcon = [[UIImageView alloc]init];
    [self addSubview:_userIcon];
    
    //职位
    _job = [[UILabel alloc]init];
    _job.textColor = [UIColor colorWithHexString:@"3034a4"];
    _job.font = [UIFont boldSystemFontOfSize:12.0f];
    [self addSubview:_job];
    
    //行业、单位、职能
    _company = [[UILabel alloc]init];
    _company.textColor = [UIColor colorWithHexString:@"68686d"];
    _company.font = [UIFont systemFontOfSize:11.0f];
    [self addSubview:_company];
    
    //查看个人资料
    _personalInfo = [UIButton buttonWithType:UIButtonTypeCustom];
    _personalInfo.layer.masksToBounds = YES;
    _personalInfo.layer.cornerRadius = 11.0f;
    _personalInfo.layer.borderColor = [UIColor colorWithHexString:@"007de3"].CGColor;
    _personalInfo.layer.borderWidth = 1.0f;
    _personalInfo.titleLabel.font = [UIFont systemFontOfSize:11.0f];
    [_personalInfo setTitle:@"查看个人资料" forState:UIControlStateNormal];
    [_personalInfo setTitleColor:[UIColor colorWithHexString:@"007de3"] forState:UIControlStateNormal];
    [self addSubview:_personalInfo];
    
    [_userIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(@24);
        make.centerY.equalTo(self.mas_centerY);
        make.width.height.equalTo(@27.5);
    }];
    
    [_job mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_userIcon.mas_right).offset(7);
        make.top.equalTo(_userIcon.mas_top).offset(2);
    }];
    
    [_company mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_userIcon.mas_right).offset(7);
        make.top.equalTo(_userIcon.mas_bottom).offset(2);
    }];
    
    [_personalInfo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-12);
        make.height.equalTo(@22);
        make.width.equalTo(@90);
    }];
}

@end
