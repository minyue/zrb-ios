//
//  ShareCustomView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/10/12.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareCustomView : NSObject

@property(nonatomic,copy)void (^ shareButtonClickBlock)(NSInteger index);


- (void)initWithView:(UIView *)superView;

@end
