//
//  ShareCustomView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/10/12.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "ShareCustomView.h"

@implementation ShareCustomView


- (void)initWithView:(UIView *)superView{
    
    
    NSMutableArray *shareTitleArray = [[NSMutableArray alloc]initWithObjects:@"QQ",@"QQ空间",@"微信",@"朋友圈",@"复制链接", nil];
    NSMutableArray *shareIconArray = [[NSMutableArray alloc]initWithObjects:@"my_setup_share_QQ",@"my_setup_share_QQkongjian",@"my_setup_share_weixin",@"my_setup_share_friends",@"my_setup_share_weixin", nil];
    
    
    for (int i = 0; i < shareIconArray.count; i ++) {
        
        UIButton *itemView = [UIButton buttonWithType:UIButtonTypeCustom];
        itemView.backgroundColor = [UIColor clearColor];
        itemView.tag = i;
        [itemView addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
        [superView addSubview:itemView];
        
        //图标
        UIImageView *icon = [[UIImageView alloc]init];
        icon.backgroundColor = [UIColor clearColor];
        icon.image = [UIImage imageNamed:shareIconArray[i]];
        [itemView addSubview:icon];
        
        //标题
        UILabel *title = [[UILabel alloc]init];
        title.font = [UIFont systemFontOfSize:13.0f];
        title.backgroundColor = [UIColor clearColor];
        [title sizeToFit];
        title.text = shareTitleArray[i];
        [itemView addSubview:title];
        
        [itemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@50);
            make.width.equalTo(@(SCREEN_WIDTH*1/4));
            make.left.equalTo(superView.mas_left).offset((SCREEN_WIDTH*(i%4)/4));
            make.top.equalTo(superView.mas_top).offset(50*(i/4));
            if (i == shareIconArray.count - 1) {
                make.bottom.equalTo(superView.mas_bottom).offset(-10);
            }
        }];
        
        [icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.width.equalTo(@20);
            make.top.equalTo(itemView.mas_top).offset(10);
            make.centerX.equalTo(itemView.mas_centerX);
        }];
        
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(itemView.mas_centerX);
            make.top.equalTo(icon.mas_bottom);
            make.bottom.equalTo(itemView.mas_bottom);
        }];
        
    }
}

- (void)share:(id)sender{
    UIButton *button = (UIButton *)sender;
    if (self.shareButtonClickBlock) {
        self.shareButtonClickBlock(button.tag);
    }
}


@end
