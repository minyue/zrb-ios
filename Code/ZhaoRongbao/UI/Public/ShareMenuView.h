//
//  ShareMenuView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/10/12.
//  Copyright © 2015年 zouli. All rights reserved.
//  分享 弹出框

#import <UIKit/UIKit.h>

@interface ShareMenuView : UIView
{
    UIButton *_backView;
}

- (void)show;
- (void)hide;

@property(nonatomic,copy)void (^ shareButtonClickBlock)(NSInteger index);

@end
