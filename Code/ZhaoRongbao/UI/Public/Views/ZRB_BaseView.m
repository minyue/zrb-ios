//
//  ZRB_BaseView.m
//  ZhaoRongbao
//
//  Created by zouli on 15/10/22.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "ZRB_BaseView.h"

@implementation ZRB_BaseView
- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}


- (void)setup
{
    self.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    
}
@end
