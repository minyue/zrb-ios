//
//  ZRB_FontSheet.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/2.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ChangeFontType)
{
    /**
     *  大
     */
    FontTypeLarge = 0,
    
    /**
     *  中
     */
    FontTypeMiddle ,
    
    /**
     *  小
     */
    FontTypeSmall
};

/**
 *  是否开启夜间模式
 *
 *  @param on YES:开启   NO:关闭
 */
typedef void(^NightModeBlock)(BOOL on);


typedef void(^ChangeFontBlock)(ChangeFontType type) ;


@interface ZRB_FontSheet : UIView

//夜间模式
@property (nonatomic,copy) NightModeBlock  nightBlock;

//改变字体大小
@property (nonatomic,copy) ChangeFontBlock  fontBlock;

@property (nonatomic,assign) BOOL  on;

@property (nonatomic,assign)ChangeFontType type;


- (void)show;


- (void)hide;



@end


@interface FontSheetView : UIView
{
    IBOutlet  UIButton  *_finishButton;
    
    IBOutlet  UISegmentedControl  *_fontSizeSegement;
    
    IBOutlet  UISegmentedControl  *_paddingSegement;
    
    IBOutlet  UISwitch            *_switch;
}


@property (nonatomic,assign) BOOL  on;

@property (nonatomic,assign)ChangeFontType type;


//夜间模式
@property (nonatomic,copy) NightModeBlock  nightBlock;

//改变字体大小
@property (nonatomic,copy) ChangeFontBlock  fontBlock;

@property (nonatomic,copy) void(^closeBlock)();


@end