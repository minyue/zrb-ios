//
//  ZRB_FontSheet.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/2.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_FontSheet.h"
@interface ZRB_FontSheet()<UIGestureRecognizerDelegate>
{
    UIView          *_mainView;
    
    UITapGestureRecognizer  *_tapGesture;
    
    FontSheetView     *_fontSheet;

}

@end

@implementation ZRB_FontSheet

- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}



- (void)setup
{
    
    self.frame = CGRectMake(0, 0, UIScreenWidth, UIScreenHeight);
    [self setupMainView];
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCancel)];
    _tapGesture.delegate = self;
    [self addGestureRecognizer:_tapGesture];
    

}


- (void)setupMainView
{
    _mainView = [[UIView alloc] initWithFrame:CGRectMake(0, UIScreenHeight, UIScreenWidth, 160)];
    _mainView.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    [self addSubview:_mainView];
    
    _fontSheet = (FontSheetView *)[ZRBUtilities viewFromSourceName:@"FontSheetView"];
    [_mainView   addSubview:_fontSheet];
    [_fontSheet makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_mainView);
    }];
    
    WS(bself);
    _fontSheet.nightBlock = ^(BOOL on){
        bself.nightBlock(on);
    };
;
    
    _fontSheet.fontBlock = ^(ChangeFontType type){
        bself.fontBlock(type);
    };
    
    _fontSheet.closeBlock = ^{
    
        [bself hide];
    };
;
}


- (void)show
{
    
    _fontSheet.on = self.on;
    _fontSheet.type = self.type;
    
    [self animateBackGroundView:self show:YES complete:^{
        [self animationMainView];
    }];
    
    [[UIApplication sharedApplication].delegate.window.rootViewController.view addSubview:self];
    
}

- (void)hide
{
    
    [self animateBackGroundView:self show:NO complete:^{
        
        [UIView animateWithDuration:.25 animations:^{
            [_mainView setFrame:CGRectMake(0, UIScreenHeight,UIScreenWidth, 0)];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
    
}


- (void)animateBackGroundView:(UIView *)view show:(BOOL)show complete:(void(^)())complete
{
    if (show) {
        
        [UIView animateWithDuration:0.2 animations:^{
            view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
        }];
        
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        } completion:nil];
    }
    if (complete) {
        complete();
    }
}


- (void)animationMainView
{
    
    [UIView animateWithDuration:.25 animations:^{
        [UIView animateWithDuration:.25 animations:^{
            [_mainView setFrame:CGRectMake(_mainView.frameX, UIScreenHeight- _mainView.frameHeight, _mainView.frameWidth, _mainView.frameHeight)];
        }];
    } completion:^(BOOL finished) {
        
    }];
}





-(void)tappedCancel{
    
    [self animateBackGroundView:self show:NO complete:^{
        
        [UIView animateWithDuration:.25 animations:^{
            [_mainView setFrame:CGRectMake(0, UIScreenHeight,UIScreenWidth, 0)];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
    
}


//分享
- (void)socialItemSelected:(id)sender
{
    
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view isKindOfClass:[self class]]){
        return YES;
    }
    return NO;
}


@end




@implementation FontSheetView

- (void)awakeFromNib
{

    [self setup];
}


- (void)setup
{
    [_finishButton setTitleColor:[UIColor colorWithHexString:@"5f5f5f"] forState:UIControlStateNormal];
    [_finishButton setTitleColor:[UIColor colorWithHexString:@"5f5f5f"] forState:UIControlStateHighlighted];
    _finishButton.layer.borderWidth = 0.5;
    _finishButton.layer.borderColor = [UIColor colorWithHexString:@"969696"].CGColor;
    [_finishButton addTarget:self action:@selector(finsh:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *dic = @{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"5f5f5f"]};
    [_fontSizeSegement setTitleTextAttributes:dic forState:UIControlStateNormal];
    [_fontSizeSegement addTarget:self action:@selector(segementAction:) forControlEvents:UIControlEventValueChanged];
    [_paddingSegement setTitleTextAttributes:dic forState:UIControlStateNormal];
    _switch.transform = CGAffineTransformMakeScale(0.8, 0.8);
    _switch.on = NO;
    [_switch addTarget:self action:@selector(swithAction:) forControlEvents:UIControlEventValueChanged];
    
}



- (void)setOn:(BOOL)on
{
    _switch.on = on;
}


- (void)setType:(ChangeFontType)type
{
    _fontSizeSegement.selectedSegmentIndex = type;

}

- (void)segementAction:(UISegmentedControl *)segement
{
    
    NSInteger selectIndex = segement.selectedSegmentIndex;
    
    if (self.fontBlock) {
        self.fontBlock(selectIndex);
    }
    
    segement.selectedSegmentIndex = selectIndex;
}


- (void)swithAction:(UISwitch *)sender
{

    BOOL  on = sender.on;
    
    if (self.nightBlock) {
        self.nightBlock(on);
    }
    
    sender.on = on;
}


- (void)finsh:(id)sender
{
    if (self.closeBlock) {
        self.closeBlock();
    }
}



@end
