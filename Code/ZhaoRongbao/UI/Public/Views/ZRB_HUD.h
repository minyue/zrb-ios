//
//  ZRB_HUD.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/7.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZRB_HUD : UIView
{
    IBOutlet  UIButton   *_hudButton;
}

@property(nonatomic,copy)void (^hudClickBlock)();

- (IBAction)buttonClick:(id)sender;
@end
