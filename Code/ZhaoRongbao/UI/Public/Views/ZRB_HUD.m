//
//  ZRB_HUD.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/7.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_HUD.h"

@implementation ZRB_HUD

- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup
{
    _hudButton.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    _hudButton.layer.masksToBounds = YES;
    _hudButton.layer.borderWidth = .5;
    _hudButton.layer.borderColor = [UIColor colorWithHexString:@"a0a0a0"].CGColor;
    _hudButton.layer.cornerRadius = 3;
}

- (IBAction)buttonClick:(id)sender
{
    if (self.hudClickBlock) {
        self.hudClickBlock();
    }

}
@end
