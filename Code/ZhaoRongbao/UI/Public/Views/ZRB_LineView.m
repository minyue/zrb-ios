//
//  ZRB_LineView.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/30.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_LineView.h"

@implementation ZRB_LineView
- (void)awakeFromNib
{
    
    [super awakeFromNib];
    
}



- (id)init
{
    if (self = [super init])
    {
    }
    return self;
}



- (void)updateSelfHeight
{
    self.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    CGFloat scale = [UIScreen mainScreen].scale;
    NSLayoutConstraint *con = [self findOwnConstraintForAttribute:NSLayoutAttributeHeight];
    con.constant = 1.0/scale;
    [self layoutIfNeeded ];
}

- (void)layoutSubviews
{
    
    [super layoutSubviews];
    [self updateSelfHeight];
    
}
@end
