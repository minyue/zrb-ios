//
//  ZRB_SearchBar.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "NewsConst.h"
#import "ZFTokenField.h"
@protocol  ZRB_SearchBarDelegate;
@interface ZRB_SearchBar : UIView
{
    IBOutlet  UIImageView   *_iconView;
}

@property (nonatomic,copy) NSString *text;      //输入的字符
@property (nonatomic,strong) UIFont *font;      //字体
@property (nonatomic,copy) NSString *placeholder;   //默认字符串
@property (nonatomic, weak) id <ZRB_SearchBarDelegate> delegate;

@property (nonatomic,strong)  ZFTokenField  *tokenField;



- (void)reloadData;

@end



@protocol ZRB_SearchBarDelegate <NSObject>

@optional


#pragma mark - 
#pragma mark - 搜索框的相关协议

- (CGFloat)lineHeightForTokenInField:(ZRB_SearchBar *)tokenField;



- (NSUInteger)numberOfTokenInField:(ZRB_SearchBar *)tokenField;


- (UIView *)tokenField:(ZRB_SearchBar *)tokenField viewForTokenAtIndex:(NSUInteger)index;


- (CGFloat)tokenMarginInTokenInField:(ZRB_SearchBar *)tokenField;
- (void)tokenField:(ZRB_SearchBar *)tokenField didRemoveTokenAtIndex:(NSUInteger)index;
- (void)tokenField:(ZRB_SearchBar *)tokenField didReturnWithText:(NSString *)text;
- (void)tokenField:(ZRB_SearchBar *)tokenField didTextChanged:(NSString *)text;
- (void)tokenFieldDidBeginEditing:(ZRB_SearchBar *)tokenField;
- (BOOL)tokenFieldShouldEndEditing:(ZRB_SearchBar *)textField;
- (void)tokenFieldDidEndEditing:(ZRB_SearchBar *)tokenField;

@end


