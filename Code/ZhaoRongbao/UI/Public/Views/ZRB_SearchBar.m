//
//  ZRB_SearchBar.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_SearchBar.h"

@interface ZRB_SearchBar()<ZFTokenFieldDataSource,ZFTokenFieldDelegate>
{

}


- (void)setup;

@end

@implementation ZRB_SearchBar

- (instancetype)init{
    if (self = [super init])
    {
        [self setup];
    }
    return self;
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup
{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 13.0f;
    self.layer.masksToBounds = YES;
    
    self.tokenField = [[ZFTokenField alloc] init];
    self.tokenField.dataSource = self;
    self.tokenField.delegate = self;
    self.tokenField.textField.placeholder = @"请输入";
    [self addSubview:self.tokenField];
    
    [self.tokenField makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@25);
        make.leading.equalTo(_iconView.mas_right).offset(10);
        make.right.equalTo(self.mas_right).offset(10);
        make.centerY.equalTo(_iconView.mas_centerY);
    }];
    
}



- (void)reloadData
{
    [self.tokenField reloadData];
}

- (NSString *)text {
    return self.tokenField.textField.text;
}
- (void)setText:(NSString *)text {
    self.tokenField.textField.text = text;
}
- (NSString *)placeholder {
    return self.tokenField.textField.placeholder;
}
- (void)setPlaceholder:(NSString *)placeholder {
    self.tokenField.textField.placeholder = placeholder;
}

- (UIFont *)font {
    return self.tokenField.textField.font;
}
- (void)setFont:(UIFont *)font {
    self.tokenField.textField.font = font;
}


- (BOOL)isFirstResponder
{
    return [self.tokenField.textField isFirstResponder];
}

- (BOOL)becomeFirstResponder {
    return [self.tokenField.textField becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    [self.tokenField.textField resignFirstResponder];
    return YES;
}


- (CGFloat)lineHeightForTokenInField:(ZFTokenField *)tokenField
{
    if ([self.delegate respondsToSelector:@selector(lineHeightForTokenInField:)]) {
       return  [self.delegate lineHeightForTokenInField:self];
    }
    return 0;
}

- (NSUInteger)numberOfTokenInField:(ZFTokenField *)tokenField
{

    if ([self.delegate respondsToSelector:@selector(numberOfTokenInField:)]) {
       return  [self.delegate numberOfTokenInField:self];
    }
    return 0;
}

- (UIView *)tokenField:(ZFTokenField *)tokenField viewForTokenAtIndex:(NSUInteger)index
{
    if ([self.delegate respondsToSelector:@selector(tokenField:viewForTokenAtIndex:)]) {
        return [self.delegate tokenField:self viewForTokenAtIndex:index];
    }
    return nil;
}


- (CGFloat)tokenMarginInTokenInField:(ZRB_SearchBar *)tokenField
{
    if ([self.delegate respondsToSelector:@selector(tokenMarginInTokenInField:)]) {
        return [self.delegate tokenMarginInTokenInField:self];
    }
    return 0;
}

- (void)tokenField:(ZRB_SearchBar *)tokenField didRemoveTokenAtIndex:(NSUInteger)index
{
    if ([self.delegate respondsToSelector:@selector(tokenField:didRemoveTokenAtIndex:)]) {
        [self.delegate tokenField:self didRemoveTokenAtIndex:index];
    }
}
- (void)tokenField:(ZRB_SearchBar *)tokenField didReturnWithText:(NSString *)text
{
    if ([self.delegate respondsToSelector:@selector(tokenField:didReturnWithText:)]) {
        [self.delegate tokenField:self didReturnWithText:text];
    }
}
- (void)tokenField:(ZRB_SearchBar *)tokenField didTextChanged:(NSString *)text
{
    if ([self.delegate respondsToSelector:@selector(tokenField:didTextChanged:)]) {
        [self.delegate tokenField:self didTextChanged:text];
    }
}
- (void)tokenFieldDidBeginEditing:(ZRB_SearchBar *)tokenField
{
    if ([self.delegate respondsToSelector:@selector(tokenFieldDidBeginEditing:)]) {
        [self.delegate tokenFieldDidBeginEditing:self];
    }
}
- (BOOL)tokenFieldShouldEndEditing:(ZRB_SearchBar *)textField
{
    if ([self.delegate respondsToSelector:@selector(tokenFieldShouldEndEditing:)]) {
        [self.delegate tokenFieldShouldEndEditing:self];
    }
    return YES;
}
- (void)tokenFieldDidEndEditing:(ZRB_SearchBar *)tokenField
{
    if ([self.delegate respondsToSelector:@selector(tokenFieldDidEndEditing:)]) {
        [self.delegate tokenFieldDidEndEditing:self];
    }
}



@end
