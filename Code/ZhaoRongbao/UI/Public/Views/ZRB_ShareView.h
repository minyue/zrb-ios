//
//  ZRB_ShareView.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/2.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZRB_ShareView : UIView

@property (nonatomic,copy) void(^shareBlock)(ZRBSocialSnsType type);

- (void)show;


- (void)hide;
@end





@interface ZRB_ShareItem : UIButton

@property (nonatomic,strong)  UIImageView *iconView;      // 分享的社区图标
@property (nonatomic,strong)  UILabel     *nameLabel;     // 分享的社区名称
@property (nonatomic,assign)  ZRBSocialSnsType type;       //社区类型
@property (nonatomic,copy)    NSString    *normalImgName;  //普通状态下的图片名



- (void)initWithButtonName:(NSString *)name;

- (void)setItemButtonImageName;

@end
