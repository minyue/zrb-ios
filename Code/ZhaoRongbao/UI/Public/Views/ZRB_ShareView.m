//
//  ZRB_ShareView.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/2.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ShareView.h"


@interface ZRB_ShareView()<UIGestureRecognizerDelegate>
{
    UIView          *_mainView;
    
    UITapGestureRecognizer  *_tapGesture;
    
    NSArray   *_itemsArray;
    
}

- (void)setup;

- (void)setupMainView;

- (void)setupItems;
@end

@implementation ZRB_ShareView

- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
        [self setupItems];
    }
    return self;
}


- (void)setup
{

    self.frame = CGRectMake(0, 0, UIScreenWidth, UIScreenHeight);
    [self setupMainView];
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCancel)];
    _tapGesture.delegate = self;
    [self addGestureRecognizer:_tapGesture];
    
    
    UILabel  *titleLabel = [[UILabel alloc] init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    titleLabel.textColor = [UIColor colorWithHexString:@"5f5f5f"];
    titleLabel.text = @"分享";;
    [_mainView addSubview:titleLabel];
    [titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_mainView.mas_centerX);
        make.top.equalTo(_mainView.mas_top).offset(15);
    }];

}

- (void)setupMainView
{
    _mainView = [[UIView alloc] initWithFrame:CGRectMake(0, UIScreenHeight, UIScreenWidth, 208)];
    _mainView.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
    [self addSubview:_mainView];
}

- (void)setupItems
{
    _itemsArray = @[kShareToWechat,kShareToWechatTimeline,kShareToQQ,kShareToQzone,kShareToSinaBlog,kShareToTencentBlog,kShareToMail,kShareToCopy];
    
    
    int totalloc = 4;       //列数
    
    CGFloat itemWith = 45;
    
    CGFloat itemHeight = 54;
    
    CGFloat  leading = 34;
    
    CGFloat   top = 60;
    
    CGFloat  margin = (UIScreenWidth - totalloc*itemWith - 2*leading) / (totalloc - 1);
    
    for (int i = 0 ; i < _itemsArray.count; i ++) {
        
        int row=i/totalloc;//行号
        int loc=i%totalloc;//列号
        
        CGFloat x = leading + (margin + itemWith) * loc;
        CGFloat y = top +(20 +itemHeight) *row;
        
        ZRB_ShareItem *item = [ZRB_ShareItem buttonWithType:UIButtonTypeCustom];
        item.backgroundColor = [UIColor clearColor];
        item.tag = i +1;
        item.frame = CGRectMake(x, y, itemWith, itemHeight);
        item.nameLabel.text = _itemsArray[i];
        [item initWithButtonName:_itemsArray[i]];
        [item setItemButtonImageName];
        [item addTarget:self action:@selector(socialItemSelected:) forControlEvents:UIControlEventTouchUpInside];
        [_mainView addSubview:item];

    }
    
}

- (void)show
{

    [self animateBackGroundView:self show:YES complete:^{
        [self animationMainView];
    }];
    
    [[UIApplication sharedApplication].delegate.window.rootViewController.view addSubview:self];

}


- (void)hide
{

    [self animateBackGroundView:self show:NO complete:^{
        
        [UIView animateWithDuration:.25 animations:^{
            [_mainView setFrame:CGRectMake(0, UIScreenHeight,UIScreenWidth, 0)];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];

}


- (void)animateBackGroundView:(UIView *)view show:(BOOL)show complete:(void(^)())complete
{
    if (show) {
        
        [UIView animateWithDuration:0.2 animations:^{
            view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
        }];
        
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        } completion:nil];
    }
    if (complete) {
        complete();
    }
}


- (void)animationMainView
{
    
    [UIView animateWithDuration:.25 animations:^{
        [UIView animateWithDuration:.25 animations:^{
            [_mainView setFrame:CGRectMake(_mainView.frameX, UIScreenHeight- _mainView.frameHeight, _mainView.frameWidth, _mainView.frameHeight)];
        }];
    } completion:^(BOOL finished) {
        
    }];
}


-(void)tappedCancel{
    
    [self animateBackGroundView:self show:NO complete:^{
        
        [UIView animateWithDuration:.25 animations:^{
            [_mainView setFrame:CGRectMake(0, UIScreenHeight,UIScreenWidth, 0)];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
    
}


//分享
- (void)socialItemSelected:(id)sender
{
    ZRB_ShareItem *item = sender;
    if (self.shareBlock)
    {
        self.shareBlock(item.tag);
    }
    
    [self hide];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view isKindOfClass:[self class]]){
        return YES;
    }
    return NO;
}


@end


@interface ZRB_ShareItem()


- (void)setup;
@end


@implementation ZRB_ShareItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setup];
    }
    return self;
}


- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
    _iconView = [[UIImageView alloc] init];
    _iconView.backgroundColor = [UIColor clearColor];
    [self addSubview:_iconView];
    [_iconView makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_top);
        make.size.equalTo(CGSizeMake(40, 40));
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont systemFontOfSize:9.0f];
    self.nameLabel.textColor = [UIColor colorWithHexString:@"000000"];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.nameLabel];
    [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(_iconView.mas_bottom).offset(5);
    }];


}




- (void)initWithButtonName:(NSString *)name
{
    
    if ([name isEqualToString:kShareToWechat]) {
        self.type = ZRBSocialSnsTypeWeChat;
    }
    else if ([name isEqualToString:kShareToWechatTimeline]){
        self.type = ZRBSocialSnsTypeWeChatTimeLine;
    }
    else if ([name isEqualToString:kShareToQQ]){
        self.type = ZRBSocialSnsTypeQQ;
    }
    else if ([name isEqualToString:kShareToQzone]){
        self.type = ZRBSocialSnsTypeQzone;
    }
    else if ([name isEqualToString:kShareToSinaBlog]){
        self.type = ZRBSocialSnsTypeSinaBlog;
    }
    else if ([name isEqualToString:kShareToTencentBlog]){
        self.type = ZRBSocialSnsTypeTencentBlog;
    }
    else if ([name isEqualToString:kShareToMail]){
        self.type = ZRBSocialSnsTypeMail;
    }
    else if ([name isEqualToString:kShareToCopy]){
        self.type = ZRBSocialSnsTypeCopy;
    }
}

- (void)setItemButtonImageName
{
    
    switch (self.type) {
        case ZRBSocialSnsTypeWeChat:
        {
            self.normalImgName = @"news_control_center_wechat";
            break;
        }
        case ZRBSocialSnsTypeWeChatTimeLine:
        {
            self.normalImgName = @"news_control_center_wechatQ";

            break;
        }
        case ZRBSocialSnsTypeQQ:
        {
            self.normalImgName = @"news_control_center_qq";
            break;
        }
        case ZRBSocialSnsTypeQzone:
        {
            self.normalImgName = @"news_control_center_zone";

            break;
        }
        case ZRBSocialSnsTypeSinaBlog:
        {
            self.normalImgName = @"news_control_center_weibo";

            break;
        }
        case ZRBSocialSnsTypeTencentBlog:
        {
            self.normalImgName = @"news_control_center_qq_weibo";

            break;
        }
        case ZRBSocialSnsTypeMail:
        {
            self.normalImgName = @"news_control_center_mail";

            break;
        }
        case ZRBSocialSnsTypeCopy:
        {
            self.normalImgName = @"news_control_center_link";

            break;
        }
        default:
            break;
    }
    
    [_iconView setImage:[UIImage imageNamed:self.normalImgName]];
}

@end

