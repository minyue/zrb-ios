//
//  ZRB_TextField.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZRB_TextFieldDelegate <NSObject>

@optional

- (BOOL)ZRBtextFieldShouldBeginEditing:(UITextField *)textField;
- (void)ZRBtextFieldDidBeginEditing:(UITextField *)textField;
- (BOOL)ZRBtextFieldShouldEndEditing:(UITextField *)textField;
- (void)ZRBtextFieldDidEndEditing:(UITextField *)textField;
- (BOOL)ZRBtextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)ZRBtextFieldShouldClear:(UITextField *)textField;
- (BOOL)ZRBtextFieldShouldReturn:(UITextField *)textField;


@end

@interface ZRB_TextField : UIView

@property (nonatomic,strong) UITextField  *textFiled;

@property (nonatomic,assign) id<ZRB_TextFieldDelegate> delegate;

@property (nonatomic,strong) NSString *text;

/**
 *  验证正确后是否显示小图标
 */
@property (nonatomic,assign) BOOL    showIconWhenSuccess;


/**
 *  通用输入框
 *
 *  @param title  标题
 *  @param holder 默认填充描述
 */
- (void)initWithTitle:(NSString *)title   placeHolder:(NSString *)holder;

/**
 *  通用输入框
 *
 *  @param title      标题
 *  @param holder     默认填充描述
 *  @param customView 扩展View（eg：倒计时器）
 */
- (void)initWithTitle:(NSString *)title  placeHolder:(NSString *)holder customView:(UIView *)customView;


/**
 *  展示标示View
 *
 *  @param show YES:展示   NO:不展示
 */
- (void)showIconView:(BOOL)show;
@end
