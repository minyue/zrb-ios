//
//  ZRB_TextField.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_TextField.h"


@interface ZRB_TextField()<UITextFieldDelegate>
{
    UILabel   *_titleLabel;
    
    
    
    UIImageView  *_iconImageView;
}


- (void)setup;
@end


@implementation ZRB_TextField


- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}


- (void)setup
{
    self.layer.cornerRadius = 3.0f;
    self.layer.borderWidth = 0.5;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.backgroundColor = [UIColor whiteColor];
    
    //创建左边的View
    WS(bself)
    UIView  *leftView = [[UIView alloc] init];
    leftView.backgroundColor = [UIColor colorWithHexString:@"4997ec"];
    [self addSubview:leftView];
    
    [leftView makeConstraints:^(MASConstraintMaker *make) {
        make.leading.and.top.and.bottom.equalTo(bself);
        make.width.equalTo(@75);
    }];
    
    _titleLabel = [[UILabel   alloc] init];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.font = [UIFont systemFontOfSize:13.0f];
    _titleLabel.textColor = [UIColor whiteColor];
    [leftView addSubview:_titleLabel];
    [_titleLabel    makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(leftView.mas_centerX);
        make.centerY.equalTo(leftView.mas_centerY);
    }];
    
    _textFiled = [[UITextField alloc] init];
    _textFiled.backgroundColor = [UIColor clearColor];
    _textFiled.font = [UIFont systemFontOfSize:14.0f];
    _textFiled.textColor =[UIColor colorWithHexString:@"4997ec"];
    _textFiled.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _textFiled.enablesReturnKeyAutomatically = YES;
    _textFiled.autocorrectionType = UITextAutocorrectionTypeNo;
    _textFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
    _textFiled.delegate = self;
    _textFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self addSubview:_textFiled ];
    
    [_textFiled makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(leftView.mas_trailing).offset(10);
        make.centerY.equalTo(bself);
        make.right.equalTo(bself.mas_right).offset(-10);
        make.height.equalTo(@30);
        
    }];
    
}


- (void)setShowIconWhenSuccess:(BOOL)showIconWhenSuccess
{
    _showIconWhenSuccess    = showIconWhenSuccess;
    
    if (showIconWhenSuccess) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.backgroundColor = [UIColor clearColor];
        _iconImageView.image = [UIImage imageNamed:@"registered_bar_correct"];
        [_textFiled addSubview:_iconImageView];
        [_iconImageView makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(CGSizeMake(19, 19));
            make.centerY.equalTo(_textFiled.mas_centerY);
            make.right.equalTo(_textFiled.mas_right).offset(-10);
        }];
        _iconImageView.hidden = YES;
    }
    
}


- (void)initWithTitle:(NSString *)title   placeHolder:(NSString *)holder
{
    _titleLabel.text = title;
    _textFiled.placeholder = holder;
}


- (void)initWithTitle:(NSString *)title  placeHolder:(NSString *)holder customView:(UIView *)customView;
{
    _titleLabel.text = title;
    _textFiled.placeholder = holder;

    [self addSubview:customView];
    NSLayoutConstraint *con = [_textFiled findConstraintForAttribute:NSLayoutAttributeRight];
    if (con) [_textFiled.superview removeConstraint:con];

    [customView makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_textFiled.mas_trailing).offset(5);
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-5);
        make.width.equalTo(@70);
    }];
    [self layoutIfNeeded];
}


- (void)isActive:(BOOL)active
{
    if (active) {
        self.layer.borderColor = [UIColor colorWithHexString:@"4997ec"].CGColor;
    }else
    {
        self.layer.borderColor = [UIColor colorWithHexString:@"ffffff"].CGColor;

    }
    
    
    if (self.showIconWhenSuccess)
    {
        if (active) {
            _iconImageView.hidden = YES;
        }
    }
}

- (NSString *)text
{

    return _textFiled.text;
}

- (void)setText:(NSString *)text{
    _textFiled.text = text;
}


/**
 *  展示标示View
 *
 *  @param show YES:展示   NO:不展示
 */
- (void)showIconView:(BOOL)show
{
    if (self.showIconWhenSuccess)
    {
        _iconImageView.hidden = !show;
    }

}

#pragma mark - 
#pragma mark - 输入框代理协议
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self isActive:YES];

    if ([self.delegate respondsToSelector:@selector(ZRBtextFieldShouldBeginEditing:)])
    {
        return [self.delegate ZRBtextFieldShouldBeginEditing:textField];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(ZRBtextFieldDidBeginEditing:)]) {
        [self.delegate ZRBtextFieldDidBeginEditing:textField];
    }

}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self isActive:NO];

    if ([self.delegate respondsToSelector:@selector(ZRBtextFieldShouldEndEditing:)]) {
        [self.delegate ZRBtextFieldShouldEndEditing:textField];
    }
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(ZRBtextFieldDidEndEditing:)]) {
        [self.delegate ZRBtextFieldDidEndEditing:textField];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self.delegate respondsToSelector:@selector(ZRBtextField:shouldChangeCharactersInRange:replacementString:)]) {
      return   [self.delegate ZRBtextField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{

    if ([self.delegate respondsToSelector:@selector(ZRBtextFieldShouldClear:)]) {
        [self.delegate ZRBtextFieldShouldClear:textField];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(ZRBtextFieldShouldReturn:)]) {
        [self.delegate ZRBtextFieldShouldReturn:textField];
    }
    
    return YES;
}
@end
