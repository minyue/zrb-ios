//
//  ZRB_TipView.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/16.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_TipView.h"

@implementation ZRB_TipView


- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}



- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}



- (void)setup
{
    titleLabel = [[UILabel   alloc] init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:14.0f];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLabel];
    [titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    
    self.backgroundColor = [UIColor colorWithHexString:@"ec6753"];
    self.alpha = 0.9;
}

- (void)setTitle:(NSString *)title
{
    titleLabel.text = title;
}

@end
