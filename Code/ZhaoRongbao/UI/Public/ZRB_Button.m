//
//  ZRB_Button.m
//  ZhaoRongbao
//
//  Created by abel on 15/10/10.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "ZRB_Button.h"

@implementation ZRB_Button

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}


- (void)setup
{
    self.layer.cornerRadius = 3.0f;
    self.layer.masksToBounds = YES;
//    self.layer.borderWidth = 0.5f;
//    self.layer.borderColor = [UIColor whiteColor].CGColor;
    
}
@end
