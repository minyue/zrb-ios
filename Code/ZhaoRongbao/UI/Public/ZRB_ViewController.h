//
//  ZRB_ViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Log.h"


//控制器基类
@interface ZRB_ViewController : UIViewController

/**
 *  弹出提示框
 *
 *  @param message  提示信息
 */
- (void)VCToast:(NSString *)message;

- (void)showTipViewWithMsg:(NSString *)msg;



- (void)hideTipView;
@end


//带返回按钮的基类控制器
@interface ZRB_ViewControllerWithBackButton : ZRB_ViewController

@end


//基类导航控制器
@interface ZRB_UINavigationController : UINavigationController

@end