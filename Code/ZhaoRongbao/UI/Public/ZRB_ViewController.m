//
//  ZRB_ViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/26.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "UIColor+extend.h"
#import "UINavigationItem+CustomItem.h"
#import "ZRB_TipView.h"
@interface ZRB_ViewController()
{
    ZRB_TipView  *_tipView;
}

@end

@implementation ZRB_ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setCommonApperence];
    self.view.backgroundColor = [UIColor colorWithHexString:ZRB_BACKGROUNDCORLOR];
    [SVProgressHUD setBackgroundColor:[UIColor darkGrayColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];


}

- (void)setup
{
}


- (void)setCommonApperence
{
    //设置状态了颜色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

/**
 *  弹出提示框
 * 
 *  @param message  提示信息
 */
- (void)VCToast:(NSString *)message{
    UIView *view = [[UIApplication sharedApplication].delegate window].rootViewController.view;
    
    MBProgressHUD *hud = [[MBProgressHUD alloc]initWithView:view];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = message;
    [view addSubview:hud];
    
    [hud showAnimated:YES whileExecutingBlock:^{
        sleep(2);
    } completionBlock:^{
        [hud removeFromSuperview];
    }];
}

- (void)showTipViewWithMsg:(NSString *)msg
{
    
    if (_tipView) {
        return;
    }
    
    WS(bself);
    _tipView = [[ZRB_TipView alloc] initWithFrame:CGRectMake(0, -35, UIScreenWidth, 35)];
    [self.view addSubview:_tipView];
    [self.view bringSubviewToFront:_tipView];
    [_tipView setTitle:msg];
    [UIView animateWithDuration:.6 animations:^{
        _tipView.frame = CGRectMake(0, 0, UIScreenWidth, 35);
    } completion:^(BOOL finished) {
        [bself hideTipView];
    }];
}

- (void)hideTipView
{
    [UIView animateWithDuration:1.5 animations:^{
        _tipView.frame = CGRectMake(0, -35, UIScreenWidth, 35);

    } completion:^(BOOL finished) {
        _tipView.hidden = YES;
        [_tipView removeFromSuperview];
        _tipView = nil;
    }];


}
@end



@implementation ZRB_ViewControllerWithBackButton

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createBackItem];

}



- (void)createBackItem
{
    
//    NSString *title = @"返回";
//    CustomBarItem *backItem = [self.navigationItem setItemWithTitle:title  textColor:[UIColor whiteColor] fontSize:ZRB_BACK_ITEM_SIZE itemType:left];
    
   CustomBarItem *backItem =  [self.navigationItem setItemWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_back"]] itemType:left];
    [backItem addTarget:self selector:@selector(click_popViewController) event:UIControlEventTouchUpInside];
    [backItem setOffset:2];
    
//    UIButton    *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    button.frame = CGRectMake(0, 0, 44, 44);
//    [button addTarget:self action:@selector(click_popViewController) forControlEvents:UIControlEventTouchUpInside];
//    [button setImage:[UIImage imageNamed:@"my_back"] forState:UIControlStateNormal];
//    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
//    [self.navigationItem    setBackBarButtonItem:item];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
//    self.navigationController.navigationBar.translucent = NO;
}


- (void)click_popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end



@implementation ZRB_UINavigationController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationBar.barTintColor = [UIColor colorWithHexString:ZRB_NAVIGATIONBGCOLOR];
    self.navigationBar.translucent = NO;
   
}


- (void) pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [super pushViewController:viewController animated:animated];
    
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.interactivePopGestureRecognizer.delegate = nil;
        
    }
}


@end