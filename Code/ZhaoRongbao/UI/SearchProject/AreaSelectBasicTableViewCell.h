//
//  AreaSelectBasicTableViewCell.h
//  ZhaoRongbao
//
//  Created by abel on 15/11/3.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AreaModel.h"

@interface AreaSelectBasicTableViewCell : UITableViewCell
@property (nonatomic,strong)IBOutlet UIImageView *imgArrow;
- (void) initWithAreaModel:(AreaModel*)model;
@end
