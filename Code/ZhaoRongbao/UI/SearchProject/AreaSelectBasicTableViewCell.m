//
//  AreaSelectBasicTableViewCell.m
//  ZhaoRongbao
//
//  Created by abel on 15/11/3.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "AreaSelectBasicTableViewCell.h"
@interface AreaSelectBasicTableViewCell()
{
    IBOutlet UILabel   *lblItem;
    IBOutlet UILabel   *lblValue;
    
}

@end
@implementation AreaSelectBasicTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) initWithAreaModel:(AreaModel*)model{
    lblItem.text = model.cityName;
    lblValue.hidden = YES;
}
@end
