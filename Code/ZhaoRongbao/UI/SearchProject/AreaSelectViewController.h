//
//  AreaSelectViewController.h
//  ZhaoRongbao
//
//  Created by abel on 15/11/3.
//  Copyright © 2015年 zouli. All rights reserved.
//
#import "ZRB_ViewController.h"
#import <UIKit/UIKit.h>

@interface AreaSelectViewController : ZRB_ViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSString *selectedAreaPostcode;

@property (nonatomic,assign) NSInteger filterType;
@end
