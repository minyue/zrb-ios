//
//  AreaSelectViewController.m
//  ZhaoRongbao
//
//  Created by abel on 15/11/3.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "AreaSelectViewController.h"
#import "MinePersonalAreaViewController.h"
#import "MineService.h"
#import "MinePersonalAreaGreyTitleTableViewCell.h"
#import "MinePersonalAreaStatueCell.h"
#import "MinePersonalItemsCell.h"
#import "AreaModel.h"
#import "MinePersonalArea_SecondLevelViewController.h"
#import "ZRBLocationMe.h"
#import <CoreLocation/CoreLocation.h>
#import "ZRB_NormalModel.h"
#import "ZRB_UserRegisterInfo.h"
#import "AreaSelectBasicTableViewCell.h"

@interface AreaSelectViewController ()<CLLocationManagerDelegate>
{
    IBOutlet  UITableView  *_tableView;
    NSMutableArray   *_dataArray;
    MineService *_service;
    AreaModel   *_selectAreaModel;
    AreaModel   *_locationAreaModel;
}
@property (strong, nonatomic) CLLocationManager* locationManager;
@end

@implementation AreaSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    [self getData];
    
    // Do any additional setup after loading the view.
    
//    self.navigationItem.title = @"选择地区";
    _tableView.backgroundColor = [UIColor colorWithHexString:@"007de3"];
    self.view.backgroundColor = [UIColor colorWithHexString:@"007de3"];
    
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //定位
    [self startLocation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_locationManager stopUpdatingLocation];
}

- (void)setup{
    _dataArray = [[NSMutableArray alloc] init];
    _service = [[MineService alloc] init];
    _selectAreaModel = [[AreaModel alloc] init];
    _locationAreaModel = [[AreaModel alloc] init];
}

//开始定位
-(void)startLocation{
    
    if ([CLLocationManager locationServicesEnabled]) {
        
        self.locationManager = [[CLLocationManager alloc] init];
        
        self.locationManager.delegate = self;
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        self.locationManager.distanceFilter = 10.0f;
        
        [_locationManager startUpdatingLocation];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            
            [_locationManager requestWhenInUseAuthorization];  //调用了这句,就会弹出允许框了.
    }
}

- (void)changeAreaStatue{
    if(!_locationAreaModel)
        return;
    
    MinePersonalAreaStatueCell *cell = (MinePersonalAreaStatueCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.lblTitle.text = _locationAreaModel.showName;
}

-(void)getData{
    WS(bself)
    
    [_service getAllProvinceInfoWithSuccess:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        AreaModel *model = [[AreaModel alloc]init];
        model.cityName = @"全国";
        model.postcode = @"";
        
        [_dataArray addObject:model];
        [_dataArray addObjectsFromArray:(NSMutableArray*)responseObject];
        
        [_tableView reloadData];
    } failure:^{
        [SVProgressHUD dismiss];
        [bself showTipViewWithMsg:kHttpFailError];
    }];
    
    [SVProgressHUD show];
}

/**
 *  点击发出选择通知
 *
 *  @param sender
 */


- (IBAction)cancelSelectArea:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - 定位代理经纬度回调

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [_locationManager stopUpdatingLocation];
    
    NSLog(@"location ok");
    
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        for (CLPlacemark * placemark in placemarks) {
            AreaModel *model = [[AreaModel alloc] init];
            model.postcode = placemark.postalCode;
            NSDictionary *dic =    [placemark addressDictionary];
            model.cityName = [dic objectForKey:@"City"];
            model.showName = [NSString stringWithFormat:@"%@ %@",[dic objectForKey:@"State"],[dic objectForKey:@"City"]];
            _locationAreaModel = model;
            [self changeAreaStatue];
        }
        
    }];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    [_locationManager stopUpdatingLocation];
    MinePersonalAreaStatueCell *cell = (MinePersonalAreaStatueCell*)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if ([error code] == kCLErrorDenied)
    {
        //访问被拒绝
        cell.lblTitle.text = @"定位失败";
    }
    if ([error code] == kCLErrorLocationUnknown) {
        //无法获取位置信息
        cell.lblTitle.text = @"无法获取位置信息";
    }
}

#pragma mark -
#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 2){
        return  [_dataArray count];
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1)
    {
        static NSString *areaSelectPaddingCellIdentifier = @"AreaSelectPaddingCellIdentifier";
        UITableViewCell  *cell = [_tableView dequeueReusableCellWithIdentifier:areaSelectPaddingCellIdentifier forIndexPath:indexPath];
        return cell;
        
    }else if (indexPath.section == 0)
    {
        static NSString *areaSelectStatueCellIdentifier = @"AreaSelectStatueCellIdentifier";
        MinePersonalAreaStatueCell *cell = [_tableView dequeueReusableCellWithIdentifier:areaSelectStatueCellIdentifier forIndexPath:indexPath];
        return cell;
        
    }else
    {
        static NSString *minePersonalItemsCellIdentifier = @"AreaSelectItemsCellIdentifier";
        AreaSelectBasicTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:minePersonalItemsCellIdentifier forIndexPath:indexPath];
        AreaModel   *model = [_dataArray objectAtIndex:indexPath.row];
        [cell initWithAreaModel:model];
    
        if([self.selectedAreaPostcode isEqualToString:model.postcode]){
           
            cell.imgArrow.hidden = NO;
            cell.imgArrow.image = [UIImage imageNamed:@"icon_filter_choose"];
        }else{
            cell.imgArrow.hidden = YES;
        }

        
        return cell;
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    LogInfo(@"%d  %d",indexPath.section,indexPath.row);
    
    if (indexPath.section == 0) {
        _selectAreaModel.cityName = _locationAreaModel.cityName;
        _selectAreaModel.postcode = _locationAreaModel.postcode;
    }else if(indexPath.section == 2){
        _selectAreaModel = [_dataArray objectAtIndex:[indexPath row]];
    }
    
    if(indexPath.section != 1){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:^{
                //替换选择地区后通知事件
                if (self.filterType == ProjectFilter) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:PROJECT_FILTER_WITH_AREA object:_selectAreaModel];
                    
                }else if(self.filterType == MoneyFilter){
                    [[NSNotificationCenter defaultCenter] postNotificationName:MONEY_FILTER_WITH_AREA object:_selectAreaModel];
                }
            }];
            
        });
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return 44;
        case 1:
            return 52;
        case 2:
            return 44;
        default:
            return 0;
            break;
    }
}

@end
