//
//  FilterViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/10/30.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindService.h"
#import "FilterTagView.h"
#import "FilterTagButton.h"
#import "QRadioButton.h"

@interface FilterViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,FilterTagDelegate>
{
    FindService *_findService;
    
    UICollectionView *_collectionView;
}

@property (nonatomic,assign) NSString *selectedIndustryId;

@property (nonatomic,assign) NSInteger filterType;

@property (nonatomic,strong) NSMutableArray *industryArray;

@end
