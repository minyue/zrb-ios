//
//  FilterViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/10/30.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "FilterViewController.h"
#import "FindIndustryModel.h"
#import "FilterCell.h"

#define CellIdentify            @"Cell_Indentify"

@interface FilterViewController ()

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

   
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"1480df"];
    
    _findService = [[FindService alloc]init];
    _industryArray = [[NSMutableArray alloc]init];
    
    [self getAllIndustry];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}


- (void)setup{
    

    
    //导航栏标题
    UILabel *titleLB = [[UILabel alloc]init];
    titleLB.backgroundColor = [UIColor clearColor];
    titleLB.font = [UIFont systemFontOfSize:19.0f];
    titleLB.textColor = [UIColor colorWithHexString:@"ffffff"];
    titleLB.text = @"行业筛选";
    [self.view addSubview:titleLB];
    
    //副标题
    UILabel *subTitle = [[UILabel alloc]init];
    subTitle.backgroundColor = [UIColor clearColor];
    subTitle.font = [UIFont systemFontOfSize:16.0];
    subTitle.textColor = [UIColor colorWithHexString:@"94c1e5"];
    subTitle.text = @"选择行业";
    [self.view addSubview:subTitle];
    
    UICollectionViewFlowLayout *layout= [[UICollectionViewFlowLayout alloc]init];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, 200, 200) collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView registerClass :[FilterCell class] forCellWithReuseIdentifier:CellIdentify];
    
    [self.view addSubview:_collectionView];
    
//    UIView *collectionView = [[UIView alloc]init];
//    collectionView.userInteractionEnabled = YES;
//    collectionView.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:collectionView];
//    
//    for (int i = 0; i < _industryArray.count; i++) {
//        
//        FilterTagButton *tagBtn =[[FilterTagButton alloc] initWithDelegate:self groupId:@"FilterTagButton"];
//        [tagBtn setTitle:_industryArray[i] forState:UIControlStateNormal];
//        [tagBtn setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
//        [tagBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateSelected];
//        [tagBtn setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
//        [tagBtn setTitleColor:[UIColor colorWithHexString:@"007de3"] forState:UIControlStateSelected];
//                
//        
//        tagBtn.layer.masksToBounds = YES;
//        tagBtn.layer.cornerRadius = 10.0f;
//        tagBtn.layer.borderColor = [UIColor whiteColor].CGColor;
//        tagBtn.layer.borderWidth = 1.0f;
//        tagBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
//        [collectionView addSubview:tagBtn];
//        
//        
////        FilterTagView *tagView = [[FilterTagView alloc] initWithDelegate:self groupId:@"groupId"];;
////        tagView.titleLB.text = _industryArray[i];
////        [collectionView addSubview:tagView];
//        
//        [tagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.equalTo(@85);
//            make.height.equalTo(@30);
//            make.left.equalTo(collectionView.mas_left).offset((i%4)*90 + 15);
//            make.top.equalTo(collectionView.mas_top).offset((i/4)*35+10);
//        }];
//        
//    }
//    
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.backgroundColor = [UIColor clearColor];
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"icon_filter-_close"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeBtn];
    
    
    [titleLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.view.mas_top).offset(20);
        make.height.equalTo(@44);
    }];
    
    [subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(10);
        make.top.equalTo(titleLB.mas_bottom).offset(30);
    }];
    
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(subTitle.mas_bottom).offset(24);
        make.bottom.equalTo(closeBtn.mas_top).offset(-10);
    }];
    
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.width.height.equalTo(@29);
        make.bottom.equalTo(self.view.mas_bottom).offset(-40);
        make.centerX.equalTo(self.view.mas_centerX);
        
    }];
    
}

- (void)back:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}


#pragma delegate of UICollectionView

//section 的个数

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
    
}

//cell的个数

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   
    return _industryArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FilterCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentify forIndexPath:indexPath];
    
    FindIndustryItemModel *itemModel = _industryArray[indexPath.row];
    if ([itemModel.uid isEqual:_selectedIndustryId]) {
        cell.tagBtn.selected = YES;
    }else{
        cell.tagBtn.selected = NO;
    }
    [cell.tagBtn setTitle:itemModel.industryName forState:UIControlStateNormal];

    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FilterCell *cell = (FilterCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.tagBtn.selected = !cell.tagBtn.selected;
   
    LogInfo(@"%d",indexPath.row);
    FindIndustryItemModel *itemModel = [_industryArray objectAtIndex:indexPath.row];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        if (self.filterType == ProjectFilter) {
           [[NSNotificationCenter defaultCenter] postNotificationName:PROJECT_FILTER_WITH_INDUSTRY object:itemModel];
        }else if(self.filterType == MoneyFilter){
            [[NSNotificationCenter defaultCenter] postNotificationName:MONEY_FILTER_WITH_INDUSTRY object:itemModel];
        }

    }];
}


#pragma mark -- UICollectionViewDelegate

//设置每个 UICollectionView 的大小

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath

{
    
    return CGSizeMake(80, 35);
    
}

//定义每个UICollectionView 的间距

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, 10, 0,10);
    
}


//定义每个UICollectionView 的纵向间距

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 1;
    
}



-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}


#pragma mark 网络请求

//获取所有行业
- (void)getAllIndustry{
    
    //请求指示器
    [SVProgressHUD show];
    //开始请求
    IndustryRequest *request = [[IndustryRequest alloc]init];
    request.type = @"";
    
    [_findService industryWithRequest:request success:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        
        FindIndustryModel *model = responseObject;
        
        switch (model.res) {
            case ZRBHttpSuccssType:
            {
//                for (FindIndustryItemModel *itemModel in model.data) {
//                    
//                    [_industryArray addObject:itemModel.industryName];
//                }
                
                _industryArray = model.data;
                
                 [self setup];
                break;
            }
            case ZRBHttpFailType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        LogInfo(@"error:%@",error);
        [SVProgressHUD dismiss];
        [HUDManager showNonNetWorkHUDInView:self.view event:^{
            
        }];
        
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
