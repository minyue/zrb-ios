//
//  SearchProjectDetailViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"
#import "ProjectService.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "UMSocialControllerService.h"
#import "UMSocialShakeService.h"

#import "UMSocialShakeService.h"
#import "ShareMenuView.h"
#import "ShareManager.h"

@interface SearchProjectDetailViewController : ZRB_ViewControllerWithBackButton<UIWebViewDelegate, NJKWebViewProgressDelegate,UIActionSheetDelegate,UIActionSheetDelegate,
UMSocialUIDelegate,UMSocialShakeDelegate,UIAlertViewDelegate>
{
    UIWebView  *_webView;
    UIView *_rightBarView;
    UIButton  *_focusBtn;
    
    ProjectService *_projectService;
    NJKWebViewProgressView *_progressView;
    NJKWebViewProgress *_progressProxy;
    
    JSContext    *_content;
    
    ShareMenuView *_shareMenu;
}
@property (nonatomic,copy)ShareMenuView *shareMenu;

@property (nonatomic,strong) NSString *projectId;
@property (nonatomic,copy) NSString *projectName;
@property (nonatomic,copy) NSString *projectIntro;

@property (nonatomic,copy) NSMutableString *shareContent;
@property (nonatomic,copy) NSString *shareUrl;

@end
