//
//  SearchProjectDetailViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/25.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "SearchProjectDetailViewController.h"
#import "UINavigationItem+CustomItem.h"
#import "CustomBarItem.h"
#import "FcousModel.h"
#import "ZRB_NormalModel.h"
#import "MineLoginViewcController.h"
#import "UMSocialScreenShoter.h"
#import "SearchProjectLocationViewController.h"
#import "AskForMoreInfoModel.h"
#import "PhotoBroswerVC.h"
#import "ProjectImageModel.h"
#import "IntentDetailMenu.h"

@interface SearchProjectDetailViewController ()

@end

@implementation SearchProjectDetailViewController

@synthesize shareMenu = _shareMenu;

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setupNavBarViews];
    [self.navigationController.navigationBar addSubview:_progressView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    [self loadDetailWithId:self.projectId];
}

/**
 *  加载详情页面
 *
 *  @param projectId 项目ID
 */
- (void)loadDetailWithId:(NSString *)projectId
{
    NSString  *url = [NSString stringWithFormat:@"%@/projectApi/getProjectDetail?id=%@",SERVER_URL,projectId];
    LogInfo(url);
    NSURL *webUrl = [NSURL URLWithString:url];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:webUrl];
    [_webView loadRequest:req];
}


- (void)setup{
    
//    self.title = @"项目详情";
    
    _projectService = [[ProjectService alloc]init];
    
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -64)];
    _webView.autoresizesSubviews = YES;
    _webView.scalesPageToFit = YES;
    _webView.detectsPhoneNumbers = NO;
    _webView.delegate = _progressProxy;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    _webView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_webView];
    
    IntentDetailMenu *bottomMenu = [[IntentDetailMenu alloc]init];
    [bottomMenu.personalInfo addTarget:self action:@selector(personalInfoCard:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bottomMenu];
    
    [bottomMenu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.equalTo(@49.5);
    }];
    
    [self initShareMenu];
}

//跳转到个人名片
- (void)personalInfoCard:(id)sender{
    
    LogInfo(@"个人名片");
}

/**
 *  构造导航栏菜单
 */
- (void)setupNavBarViews
{
    _rightBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 2, 115, 44)];
    _rightBarView.backgroundColor = [UIColor clearColor];
    
    //分享
    UIButton  *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBtn setImage:[UIImage imageNamed:@"nav_share"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareMoney:) forControlEvents:UIControlEventTouchUpInside];
    [shareBtn setEnlargeEdgeWithTop:10.0f right:10.0f bottom:15.0f left:10.0f];
    [_rightBarView addSubview:shareBtn];
    [shareBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_rightBarView.mas_left).offset(5);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    UILabel  *shareLabel = [[UILabel alloc] init];
    [shareLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [shareLabel setBackgroundColor:[UIColor clearColor]];
    shareLabel.text = @"分享";;
    shareLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:shareLabel];
    
    [shareLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(shareBtn.mas_centerX);
        make.top.equalTo(shareBtn.mas_bottom).offset(2);
    }];
    
    
    //关注
    _focusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_focusBtn setImage:[UIImage imageNamed:@"nav_like_normal"] forState:UIControlStateNormal];
    [_focusBtn setImage:[UIImage imageNamed:@"nav_like_hl"] forState:UIControlStateSelected];
    [_focusBtn addTarget:self action:@selector(focusMoney:) forControlEvents:UIControlEventTouchUpInside];
    [_focusBtn setEnlargeEdgeWithTop:10.0f right:10.0f bottom:15.0f left:10.0f];
    [_rightBarView addSubview:_focusBtn];
    [_focusBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shareBtn.mas_right).offset(20);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    [self retuqestIfAttention:_focusBtn];
    
    UILabel  *focusLabel = [[UILabel alloc] init];
    [focusLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [focusLabel setBackgroundColor:[UIColor clearColor]];
    focusLabel.text = @"关注";
    focusLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:focusLabel];
    
    [focusLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_focusBtn.mas_centerX);
        make.top.equalTo(_focusBtn.mas_bottom).offset(2);
    }];
    
    //评论
    UIButton  *commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentBtn setImage:[UIImage imageNamed:@"nav_call"] forState:UIControlStateNormal];
    [commentBtn addTarget:self action:@selector(callService:) forControlEvents:UIControlEventTouchUpInside];
    [commentBtn setEnlargeEdgeWithTop:10.0f right:10.0f bottom:15.0f left:10.0f];
    [_rightBarView addSubview:commentBtn];
    [commentBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_focusBtn.mas_right).offset(20);
        make.right.equalTo(_rightBarView.mas_right).offset(-5);
        make.centerY.equalTo(_rightBarView.mas_centerY).offset(-10);
    }];
    
    UILabel  *commentLabel = [[UILabel alloc] init];
    [commentLabel setFont:[UIFont systemFontOfSize:11.0f]];
    [commentLabel setBackgroundColor:[UIColor clearColor]];
    commentLabel.text = @"咨询";;
    commentLabel.textColor = [UIColor whiteColor];
    [_rightBarView  addSubview:commentLabel];
    
    [commentLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(commentBtn.mas_centerX);
        make.top.equalTo(commentBtn.mas_bottom).offset(2);
    }];
    
    CustomBarItem *item =  [self.navigationItem setItemWithCustomView:_rightBarView itemType:right];
    
    [item setOffset:-5];
}


//关注资金
- (void)focusMoney:(id)sender{
    
    if([ZRB_UserManager isLogin]){
        
        if (_focusBtn.isSelected) {
            [self cancelFcousProject:self.projectId];
        }else{
            [self fcousProject:self.projectId];
        }
        
    }else{
        
        MineLoginViewcController *loginCtrl = (MineLoginViewcController *)[StoryBoardUtilities  viewControllerForStoryboardName:kMineStoryboardName class:[MineLoginViewcController class ]];
        loginCtrl.isFindHomeRefresh = YES;
        [self.navigationController pushViewController:loginCtrl animated:YES];
    }
    
}

//分享资金
- (void)shareMoney:(id)sender{

    _shareUrl = [NSString stringWithFormat:@"%@/projectApi/getProjectDetail?id=%@",SERVER_URL,self.projectId];
    _shareContent = [[NSMutableString alloc]initWithString:@"向您推荐 "];
    if(_projectName)
        [_shareContent appendString:_projectName];
   
    LogInfo(_shareUrl);
    
    [_shareMenu show];
    
    //Test for img Brower
//    PBViewController *broswerCtrl = [[PBViewController alloc] init];
//    [self.navigationController pushViewController:broswerCtrl animated:YES];
    
}

/*
 *  展示网络图片
 */
-(void)networkImageShow:(NSArray*)imgArray{
    WS(bself);
    [PhotoBroswerVC show:bself type:PhotoBroswerVCTypePush index:0 photoModelBlock:^NSArray *{
        
        NSMutableArray *modelsM = [NSMutableArray arrayWithCapacity:imgArray.count];
        for (NSUInteger i = 0; i< imgArray.count; i++) {
            ProjectImageModel *model = [imgArray objectAtIndex:i];
            PhotoModel *pbModel=[[PhotoModel alloc] init];
            pbModel.mid = i + 1;
//            pbModel.title = [NSString stringWithFormat:@"这是第%@页",@(i+1)];
            pbModel.desc = model.remark;
            pbModel.image_HD_U = [NSString stringWithFormat:@"%@/%@",IMAGE_SERVER_URL,model.url];
//            NSLog(@"pbModel.image_HD_U:%@",pbModel.image_HD_U);
            [modelsM addObject:pbModel];
        }
        return modelsM;
        
    }];
}


- (void)callService:(id)sender{
    
    LogInfo(@"客服电话");
    NSString *str = [NSString stringWithFormat:@"tel:%@",SERVICE_TEL];
    UIWebView *webView = [[UIWebView alloc]init];
    [webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:str]]];
    [self.view addSubview:webView];
    
}

- (void)setupJsContent
{
    //获取当前JS环境
    _content = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    // 打印异常
    _content.exceptionHandler =
    ^(JSContext *context, JSValue *exceptionValue)
    {
        context.exception = exceptionValue;
        LogInfo(@"%@", exceptionValue);
    };
    //获取JS事件
    WS(bself);
    
    _content[@"InitVar"] = ^(NSString *str){
        if(str){
            NSArray *array = [str componentsSeparatedByString:@","];
            if(array){
                if([array[0] isEqualToString:@"4"]){
                    //查看图片详情
                    [bself retuqestProjectImagesById];
                }
            }
        }
    };
    _content[@"showtag"] = ^(int num,NSString *tag){
        LogInfo(@"num = %d   tag:%@",num,tag);
        if(num == 1){
            //联系客服
            [bself callService:nil];
        }
        else if (num == 3) {        //查看项目所在地
            //获取经纬度
            NSArray *locationArray = [tag componentsSeparatedByString:@","];
            if (locationArray.count > 1) {
                
                SearchProjectLocationViewController *locationCtrl = [[SearchProjectLocationViewController alloc]init];
                locationCtrl.longitude = [locationArray[0] floatValue];
                locationCtrl.latitude = [locationArray[1] floatValue];
                [bself.navigationController pushViewController:locationCtrl animated:YES];
            }
            
        }else if(num == 2){
            UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"" message:@"我们将在3个工作日内\n将项目详细资料发送给您" delegate:bself cancelButtonTitle:nil otherButtonTitles:@"取消",@"确定", nil];
            alter.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField *tf = [alter textFieldAtIndex:0];
            tf.placeholder = @"请输入邮箱";
            if([ZRB_UserManager isLogin] && [self isValidateEmail:[ZRB_UserManager shareUserManager].userData.uEmail]){
                LogInfo([ZRB_UserManager shareUserManager].userData.uEmail);
                tf.text = [ZRB_UserManager shareUserManager].userData.uEmail;
            }
            
            [alter show];
        }else if (num == 4){
            
        }
        
    };
}

/**
 *  验证邮箱格式
 */
-(BOOL)isValidateEmail:(NSString *)email {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}

/**
 * 初始化分享菜单
 */
- (void)initShareMenu{
    
    _shareMenu = [[ShareMenuView alloc]init];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_shareMenu];
    
    WS(bself);
    _shareMenu.shareButtonClickBlock = ^(NSInteger index){
        LogInfo(@"%ld",index);
        
        switch (index) {
            case ShareToQQ:
                [[ShareManager ShareManager] QQShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToQQZone:
                [[ShareManager ShareManager] QQZoneShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                break;
            case ShareToWechat:
                [[ShareManager ShareManager] WXChatShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToWechatFriend:
                [[ShareManager ShareManager] WXFriendShareWithViewControll:bself.shareContent URLStr:bself.shareUrl];
                
                break;
            case ShareToCopy:
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = bself.shareUrl;
                
                [bself.shareMenu hide];
                break;
            }
            default:
                break;
        }
    };
}

#pragma delegate of UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    UITextField *tf = [alertView textFieldAtIndex:0];
    if (buttonIndex == 1) {
        if (![self isValidateEmail:tf.text]) {
            
            [self showTipViewWithMsg:@"邮箱格式错误"];
        }else{
            [self askForMoreInfo:tf.text];
        }
    }else if (buttonIndex == 0){
        
    }
}



#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [_progressView setProgress:progress animated:YES];
//    self.title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    LogInfo(@"网页加载完成");
    [self setupJsContent];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    [_progressView setProgress:1.1 animated:YES];
}

#pragma mark 网络请求
- (void)retuqestProjectImagesById{

    [_projectService myProjectImagesWithRequest:self.projectId success:^(id responseObject) {
        if(responseObject){
            [self networkImageShow:(NSMutableArray*)responseObject];
        }else{
            [self showTipViewWithMsg:@"项目暂无图片，请稍候再试！"];
        }
    } failure:^(NSError *error) {
        [self showTipViewWithMsg:@"项目图片请求失败，请稍候再试！"];
    }];
}

/**
 *  判断是否关注
 */
- (void)retuqestIfAttention:(UIButton*)btn{
    
    btn.selected = NO;
    [_projectService checkIfAttentionNewOrProjectWithRequest:self.projectId andType:FcousProject success:^(id responseObject) {
       
        if(responseObject){
            ZRB_NormalModel *model = responseObject;
            if(model.res == 1){
                btn.selected = [[model.data valueForKey:@"isCollected"] boolValue];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

/**
 *  项目关注
 */
- (void)fcousProject:(NSString *) projectId{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousProject;
    request.fcousObjectId =  projectId;
    
    [SVProgressHUD show];
    
    AFBaseService *baseService = [[AFBaseService alloc]init];
    [baseService fcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"关注成功" onView:self.view];
                    
                    _focusBtn.selected = YES;
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"已关注"];
                }
                
                break;
            case ZRBHttpFailType:
                [self showTipViewWithMsg:@"关注失败"];
                _focusBtn.selected = NO;
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
        _focusBtn.selected = NO;
    }];
}

/**
 *  取消关注
 */
- (void)cancelFcousProject:(NSString *) projectId{
    
    //封装参数
    FcousRequest *request = [[FcousRequest alloc]init];
    request.fcousType = FcousProject;
    request.fcousObjectId =  projectId;
    
    [SVProgressHUD show];
    
    AFBaseService *baseService = [[AFBaseService alloc]init];
    [baseService cancelFcousObjectWithRequest:request success:^(id responseObject) {
        [SVProgressHUD dismiss];
        
        FcousModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                if (model.data == YES) {
                    [MBProgressHUD showHUDTitle:@"已取消关注" onView:self.view];
                    _focusBtn.selected = NO;
                    
                }else if (model.data == NO){
                    [self showTipViewWithMsg:@"取消关注失败"];
                }
                
                break;
            case ZRBHttpFailType:
            {
                [self showTipViewWithMsg:@"取消关注失败"];
                _focusBtn.selected = NO;
            }
                break;
            case ZRBHttpNoLoginType:
                
                break;
            default:
                break;
        }
        
    } failure:^(NSError *error) {
        
        [SVProgressHUD dismiss];
        [self showTipViewWithMsg:@"关注失败"];
        _focusBtn.selected = NO;
    }];
}

/**
 *  索要更多资料
 */
- (void)askForMoreInfo:(NSString *)email{
    
    //索取更多资料请求
    AskForMoreInfoRequest *request = [[AskForMoreInfoRequest alloc]init];
    request.projectID = self.projectId;
    request.projectName = self.projectName;
    request.email = email;
    
    [_projectService askForMoreInfoWithRequest:request success:^(id responseObject) {
        
        AskForMoreInfoModel *model = responseObject;
        switch (model.res) {
            case ZRBHttpSuccssType:
                [MBProgressHUD showHUDTitle:@"申请成功" onView:self.view];
                break;
            case ZRBHttpFailType:
                
                [self showTipViewWithMsg:@"申请失败"];
                break;
            case ZRBHttpNoLoginType:
                
                break;
                
            default:
                break;
        }
        
    } failture:^(NSError *error) {
        [self showTipViewWithMsg:@"申请失败"];
    }];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
