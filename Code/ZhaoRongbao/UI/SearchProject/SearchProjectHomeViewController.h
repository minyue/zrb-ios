//
//  SearchProjectHomeViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_ViewController.h"
#import "MJRefresh.h"
#import "SearchProjectService.h"
#import "BlankRemindView.h"

@interface SearchProjectHomeViewController : ZRB_ViewController<UITableViewDelegate,UITableViewDataSource>
{
    UILabel *_areaLB;
    UITableView *_mTableView;
    BOOL _isRequst;
    
    SearchProjectService *_projectService;
}

@property (nonatomic,strong) NSMutableArray *projectModelArray;

@property (nonatomic,strong) NSString *filterAreaStr;           //筛选的地区

@property (nonatomic,strong) NSString *filterIndustryStr;       //筛选的行业

@property (nonatomic,strong) BlankRemindView *blankRemindView; //列表空白提示视图

@end
