//
//  SearchProjectHomeViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "SearchProjectHomeViewController.h"
#import "SearchProjectDetailViewController.h"
#import "SearchProjectHomeModel.h"
#import "SearchProjectHomeCell.h"
#import "FilterViewController.h"
#import "UINavigationItem+CustomItem.h"
#import "CustomBarItem.h"
#import "AreaSelectViewController.h"
#import "AreaModel.h"
#import "FindIndustryModel.h"

#define CellIndentifier         @"SearchProjectHomeCell"

@interface SearchProjectHomeViewController ()

@end

@implementation SearchProjectHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
    
    [self refreshData];
}


- (void)setup{
    
    _isRequst = NO;
    _filterAreaStr = @"";
    _filterIndustryStr = @"97";
    
    self.title = @"找项目";
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"筛选" style:UIBarButtonItemStylePlain target:self action:@selector(filter:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
    UIButton *leftView = [[UIButton alloc]init];
    leftView.backgroundColor = [UIColor clearColor];
    leftView.frame = CGRectMake(0, 0, 60, 44);
    [leftView addTarget:self action:@selector(selectCity:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *icon = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_project_location"]];
    [leftView addSubview:icon];
    
    _areaLB = [[UILabel alloc]init];
    _areaLB.backgroundColor = [UIColor clearColor];
    _areaLB.text = @"全国";
    _areaLB.textColor = [UIColor whiteColor];
    _areaLB.font = [UIFont systemFontOfSize:ZRB_BACK_ITEM_SIZE];
    [leftView addSubview:_areaLB];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(@20);
        make.left.equalTo(leftView.mas_left);
        make.centerY.equalTo(leftView.mas_centerY);
    }];
    
    [_areaLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right);
        make.centerY.equalTo(leftView.mas_centerY);
        make.right.equalTo(leftView.mas_right);
    }];
    
    CustomBarItem *item =  [self.navigationItem setItemWithCustomView:leftView itemType:left];
    [item setOffset:-10];
    
//    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftView];
//    self.navigationItem.leftBarButtonItem = leftItem;
//    
    _projectService = [[SearchProjectService alloc]init];
    _projectModelArray = [[NSMutableArray alloc]init];
    
     _blankRemindView = [[BlankRemindView alloc]init];
    
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64- 49) style:UITableViewStylePlain];
    [_mTableView registerClass:[SearchProjectHomeCell class] forCellReuseIdentifier:CellIndentifier];
    _mTableView.backgroundColor = [UIColor clearColor];
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    [_mTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.view addSubview:_mTableView];
    
    [self addFooterView];
    [self addHeaderView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterProjectWithIndustry:) name:PROJECT_FILTER_WITH_INDUSTRY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterProjectWithArea:) name:PROJECT_FILTER_WITH_AREA object:nil];
}

//城市切换
- (void)selectCity:(id)sender{
    
    AreaSelectViewController *eVC = (AreaSelectViewController *)[StoryBoardUtilities viewControllerForStoryboardName:@"Projects" class:[AreaSelectViewController class]];
    eVC.filterType = ProjectFilter;
    eVC.selectedAreaPostcode = _filterAreaStr;
    
    [self.navigationController presentViewController:eVC animated:YES completion:nil];
}

//行业筛选
- (void)filter:(id)sender{
    
    FilterViewController *filterCtrl = [[FilterViewController alloc]init];
    filterCtrl.selectedIndustryId = _filterIndustryStr;
    filterCtrl.filterType = ProjectFilter;
    
    [self presentViewController:filterCtrl animated:YES completion:nil];
    
}

/**
 *  根据行业筛选项目
 */
- (void)filterProjectWithIndustry:(id)sender{
    
    NSNotification *notify = (NSNotification *)sender;
    FindIndustryItemModel *model = notify.object;
    
    _filterIndustryStr = model.uid;
    LogInfo(_filterIndustryStr);
    
    [_mTableView.header beginRefreshing];
}

/**
 * 根据地区筛选项目
 */
- (void)filterProjectWithArea:(id)sender{
    
    LogInfo(@"根据地区筛选项目");
    NSNotification *notify = (NSNotification *)sender;
    
    AreaModel *model = (AreaModel *)notify.object;
    _filterAreaStr = [NSString stringWithFormat:@"%@",model.postcode];
    if (model.cityName.length > 2) {
        _areaLB.text = [model.cityName substringToIndex:model.cityName.length - 1];
    }else{
        _areaLB.text = model.cityName;
    }
    
    
    [_mTableView.header beginRefreshing];
}

/**
 *  刷新数据
 */
- (void)refreshData
{
    if (!_isRequst) {
        [_mTableView.header beginRefreshing];
    }
}

/**
 *  下拉刷新
 */
- (void)addHeaderView
{
    WS(bself);
    _mTableView.header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        [bself queryListData];
    }];
    
}

/**
 *  上拉加载更多
 */
- (void)addFooterView
{
    WS(bself);
    _mTableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [bself loadMoreData];
    }];
}

#pragma mark delegate of UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _projectModelArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  
    return 101.0f;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchProjectHomeCell *cell = [SearchProjectHomeCell cellForTabvleView:tableView];
    cell.model = [_projectModelArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SearchProjectHomeItemModel *model = [_projectModelArray objectAtIndex:indexPath.row];
    
    SearchProjectDetailViewController *detailCtrl = [[SearchProjectDetailViewController alloc]init];
    detailCtrl.projectId = model.uid;
    detailCtrl.projectName = model.projectName;
//    detailCtrl.projectIntro = model
    detailCtrl.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:detailCtrl animated:YES];
    
}


#pragma mark 网络请求

/**
 *  请求发现列表
 */
-(void)queryListData{
    
    //获取当前时间
    NSDate *date = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    _isRequst = NO;
    
    ProjectListRequest *request = [[ProjectListRequest alloc]init];
    request.lastPushTime = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
    request.rows = @"10";
    request.industrys = _filterIndustryStr;
    request.postcode = _filterAreaStr;
    
    WS(bself);
   [_projectService projectHomeListWithRequest:request success:^(id responseObject) {
       
        [HUDManager removeHUDFromView:self.view];
       
       SearchProjectHomeModel *model = responseObject;
       switch (model.res) {
           case ZRBHttpSuccssType:
           {
               _projectModelArray = model.data;
               [_mTableView reloadData];
               
               if(model.data.count < 10 && model.data > 0){
//                   [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                   _mTableView.footer.hidden = YES;
                   
               }else{
                   _mTableView.footer.hidden = NO;
               }
               
               LogInfo(@"model.data.count = %d",model.data.count);
               
               //数据为空提示
               if(model.data.count == 0){
                   
                   [_blankRemindView showInView:self.navigationController.view];
                   self.view.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
                   
               }else{
                   if (_blankRemindView) {
                       [_blankRemindView removeFromSuperview];
                       self.view.backgroundColor = [UIColor colorWithHexString:ZRB_BACKGROUNDCORLOR];
                   }
               }
               
               [_mTableView.header endRefreshing];
               break;
           }
           case ZRBHttpNoLoginType:
               
               [_mTableView.header endRefreshing];
               break;
           case ZRBHttpFailType:
           {
               [_mTableView.header endRefreshing];
               [self showTipViewWithMsg:@"获取项目列表失败"];
               break;
           }
           default:
               break;
       }
       
   } failure:^(NSError *error) {
       [_mTableView.header endRefreshing];
       LogInfo(@"error:%@",error);
       if(_projectModelArray.count < 1){
           [HUDManager showNonNetWorkHUDInView:self.view event:^{
               [bself queryListData];
           }];
       }       
   }];
    
}

/**
 *  加载更多
 */
-(void)loadMoreData{
    
    SearchProjectHomeItemModel *lastItemModel = [_projectModelArray lastObject];
    
    ProjectListRequest *request = [[ProjectListRequest alloc]init];
    request.lastPushTime = [ZRBUtilities stringToData:@"yyyy-MM-dd HH:mm:ss" interval:lastItemModel.pushtime];
    request.rows = @"10";
    request.industrys = _filterIndustryStr;
    request.postcode = _filterAreaStr;
    
    WS(bself);
    [_projectService projectHomeListWithRequest:request success:^(id responseObject) {
        
        SearchProjectHomeModel *model = responseObject;
        [bself loadMoreListCallBackWithObject:model];
        
    } failure:^(NSError *error) {
        LogInfo(@"error:%@",error);
        
        [_mTableView.footer endRefreshing];
    }];

}


/**
 *  加载更多请求回调
 */
- (void)loadMoreListCallBackWithObject:(SearchProjectHomeModel *)model
{
    switch (model.res) {
        case ZRBHttpSuccssType:
        {
            
            [_projectModelArray addObjectsFromArray:model.data];
            [_mTableView reloadData];
            
            if(model.data.count < 10){
                [MBProgressHUD showHUDTitle:@"已全部加载" onView:self.view];
                _mTableView.footer.hidden = YES;
            }else{
                _mTableView.footer.hidden = NO;
            }
            
            [_mTableView.footer endRefreshing];
            break;
        }
        case ZRBHttpNoLoginType:
            
            [_mTableView.footer endRefreshing];
            break;
        case ZRBHttpFailType:
        {
            [_mTableView.footer endRefreshing];
            [self showTipViewWithMsg:@"获取项目列表失败"];
            break;
        }
        default:
            break;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
