//
//  SearchProjectLocationViewController.h
//  ZhaoRongbao
//
//  Created by songmk on 15/10/9.
//  Copyright © 2015年 zouli. All rights reserved.
//


#import "ZRB_ViewController.h"
#import <BaiduMapAPI/BMapKit.h>
#import "BNRoutePlanModel.h"
#import "BNCoreServices.h"
#import <MapKit/MapKit.h>

#import <AMapNaviKit/MAMapKit.h>
#import <AMapNaviKit/AMapNaviKit.h>
#import "iflyMSC/IFlySpeechSynthesizer.h"
#import "iflyMSC/IFlySpeechSynthesizerDelegate.h"


@interface SearchProjectLocationViewController : ZRB_ViewControllerWithBackButton<BMKMapViewDelegate,BMKLocationServiceDelegate,BMKGeoCodeSearchDelegate,UIActionSheetDelegate>
{
    BMKLocationService *_locationService;
    BMKPlanNode *_startNode;                //当前位置，作为起点
    BMKPlanNode *_endNote;                  //项目位置，作为终点
    BMKMapView *_mapView;                       //地图视图
    BMKGeoCodeSearch* _geocodesearch;           //地址搜索，正向编码
    UIImageView *_paopaoView;
    
    MAMapView *_gaodeMapView;
    
    BMKGeoCodeSearch *_searcher;
}

@property (nonatomic, strong) IFlySpeechSynthesizer *iFlySpeechSynthesizer;
@property (nonatomic, strong) AMapNaviManager *naviManager;
@property (nonatomic, strong) AMapNaviViewController *naviViewController;
@property (nonatomic, strong) AMapNaviPoint* startPoint;
@property (nonatomic, strong) AMapNaviPoint* endPoint;
//@property (nonatomic, strong) MAMapView *gaodeMapView;

@property (nonatomic,assign)CGFloat longitude;      //项目所在位置经度
@property (nonatomic,assign)CGFloat latitude;       //项目所在位置纬度

@property (nonatomic,strong)NSMutableArray *availableMaps;

@end
