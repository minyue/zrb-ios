//
//  SearchProjectLocationViewController.m
//  ZhaoRongbao
//
//  Created by songmk on 15/10/9.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "SearchProjectLocationViewController.h"

@interface SearchProjectLocationViewController ()

@end

@implementation SearchProjectLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setup];
}

- (void)setup{
    
//    self.longitude = 114.329222;
//    self.latitude = 30.569079;
    
//    [self initCustomPaoPaoView];
    [self initBaiduMap];
}

/**
 *  初始化百度地图，添加大头针
 */
- (void)initBaiduMap{
    _startNode = [[BMKPlanNode alloc]init];
    
    //设置定位精确度，默认：kCLLocationAccuracyBest
    [BMKLocationService setLocationDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
    //指定最小距离更新(米)，默认：kCLDistanceFilterNone
    [BMKLocationService setLocationDistanceFilter:100.f];
    //初始化BMKLocationService
    _locationService = [[BMKLocationService alloc]init];
    _locationService.delegate = self;
    //启动LocationService
    [_locationService startUserLocationService];
    
    _mapView = [[BMKMapView alloc]initWithFrame:self.view.frame];
    _mapView.delegate = self;
    _mapView.zoomLevel = 15;
    [self.view addSubview:_mapView];
    
    //初始化检索对象
    _searcher =[[BMKGeoCodeSearch alloc]init];
    _searcher.delegate = self;
    //发起反向地理编码检索
    CLLocationCoordinate2D pt = (CLLocationCoordinate2D){self.latitude, self.longitude};
    BMKReverseGeoCodeOption *reverseGeoCodeSearchOption = [[
    BMKReverseGeoCodeOption alloc]init];
    reverseGeoCodeSearchOption.reverseGeoPoint = pt;
    BOOL flag = [_searcher reverseGeoCode:reverseGeoCodeSearchOption];
    
    
}


/**
 *  获取已安装的地图APP
 */

- (void)getAvailableMaps{
    _availableMaps = [[NSMutableArray alloc]init];
    
    //百度地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://map/"]]){
        NSDictionary *dic = @{@"name": @"百度地图"};
        [_availableMaps addObject:dic];
    }
}

/**
 *  自定义百度泡泡视图
 */
- (void)initCustomPaoPaoView:(NSString *)address{
    _paopaoView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    _paopaoView.backgroundColor = [UIColor whiteColor];
    _paopaoView.userInteractionEnabled = YES;
    
    UILabel *title = [[UILabel alloc]init];
    title.font = [UIFont systemFontOfSize:12.0f];
    title.textColor = [UIColor blackColor];
    title.text = address;
    [title sizeToFit];
    [_paopaoView addSubview:title];
    
    UIButton *navBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [navBtn setTitle:@"导航" forState:UIControlStateNormal];
    [navBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [navBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [navBtn addTarget:self action:@selector(nav:) forControlEvents:UIControlEventTouchUpInside];
    [_paopaoView addSubview:navBtn];
    
    
    
    [navBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_paopaoView.mas_right).offset(-5);
        make.centerY.equalTo(_paopaoView.mas_centerY);
        make.width.equalTo(@30);
        make.height.equalTo(@30);
    }];
    
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_paopaoView.mas_left).offset(5);
        make.centerY.equalTo(_paopaoView.mas_centerY);
        make.right.equalTo(navBtn.mas_left);
    }];
}

/**
 *  调用百度地图导航
 */
- (void)baiduMapNav{
    
    //初始化调启导航时的参数管理类
    BMKNaviPara* para = [[BMKNaviPara alloc]init];
    
    //指定起点名称
    _startNode.name = @"我的位置";
    //指定起点
    para.startPoint = _startNode;
    
    //初始化终点节点
    BMKPlanNode* end = [[BMKPlanNode alloc]init];
    //指定终点经纬度
    CLLocationCoordinate2D coor2;
    coor2.latitude = self.latitude;
    coor2.longitude = self.longitude;
    end.pt = coor2;
    //指定终点名称
    end.name = @"天安门";
    //指定终点
    para.endPoint = end;
    //指定返回自定义scheme
    para.appScheme = @"baidumapsdk://mapsdk.baidu.com";
    
    //调启百度地图客户端导航
    [BMKNavigation openBaiduMapNavigation:para];
}

/*
 *  调用自带地图导航
 */
- (void)iosNav{
    //获取当前位置
    MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
    //当前经维度
    float currentLatitude = currentLocation.placemark.location.coordinate.latitude;
    float currentLongitude = currentLocation.placemark.location.coordinate.longitude;
    
    //起点位置（当前位置）
    CLLocationCoordinate2D startCoords = CLLocationCoordinate2DMake(currentLatitude,currentLongitude);
    //目的地位置
    CLLocationCoordinate2D endCoords = CLLocationCoordinate2DMake(self.latitude,self.longitude);;
    
    // ios6以下，调用google map
    if (SYSTEM_VERSION_LESS_THAN(@"6.0")){
        
        NSString *urlString = [[NSString alloc] initWithFormat:@"http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f&dirfl=d", startCoords.latitude,startCoords.longitude,endCoords.latitude,endCoords.longitude];
        NSURL *aURL = [NSURL URLWithString:urlString];
        //打开网页google地图
        [[UIApplication sharedApplication] openURL:aURL];
    }else{
        // 直接调用ios自己带的apple map
        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:endCoords addressDictionary:nil]];
        toLocation.name = @"目的地";
        
        NSArray *items = [NSArray arrayWithObjects:currentLocation, toLocation, nil];
        NSDictionary *options = @{ MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsMapTypeKey: [NSNumber numberWithInteger:MKMapTypeStandard], MKLaunchOptionsShowsTrafficKey:@YES };
        
        //打开苹果自身地图应用，并呈现特定的item
        [MKMapItem openMapsWithItems:items launchOptions:options];
    }
}

/**
 *  调用高德地图导航
 */
- (void)GaodeMapNav{
    
    CLLocationCoordinate2D endCoordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    
    MANaviConfig * config = [[MANaviConfig alloc] init];
    config.destination = endCoordinate;
    config.appScheme = [self getApplicationScheme];
    config.appName = [self getApplicationName];
    config.strategy = MADrivingStrategyShortest;
    
    if(![MAMapURLSearch openAMapNavigation:config])
    {
        [MAMapURLSearch getLatestAMapApp];
    }
    
}

#pragma mark - Handle URL Scheme

- (NSString *)getApplicationName
{
    NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
    return [bundleInfo valueForKey:@"CFBundleDisplayName"];
}

- (NSString *)getApplicationScheme
{
    NSDictionary *bundleInfo    = [[NSBundle mainBundle] infoDictionary];
    NSString *bundleIdentifier  = [[NSBundle mainBundle] bundleIdentifier];
    NSArray *URLTypes           = [bundleInfo valueForKey:@"CFBundleURLTypes"];
    
    NSString *scheme;
    for (NSDictionary *dic in URLTypes)
    {
        NSString *URLName = [dic valueForKey:@"CFBundleURLName"];
        if ([URLName isEqualToString:bundleIdentifier])
        {
            scheme = [[dic valueForKey:@"CFBundleURLSchemes"] objectAtIndex:0];
            break;
        }
    }
    
    return scheme;
}


//点击导航按钮
- (void)nav:(id)sender{
    
    [self getAvailableMaps];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]init];
    actionSheet.delegate = self;
    [actionSheet addButtonWithTitle:@"苹果地图"];
    [actionSheet addButtonWithTitle:@"百度地图"];
    [actionSheet addButtonWithTitle:@"高德地图"];
    [actionSheet addButtonWithTitle:@"取消"];
    actionSheet.cancelButtonIndex = _availableMaps.count + 2;
    
    [actionSheet showInView:self.view];
}


#pragma delegate of UIActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case AppleMap:
            [self iosNav];
            break;
        case BaiduMap:
            [self baiduMapNav];
            
            break;
        case GaodeMap:
            [self GaodeMapNav];
            break;
            
        default:
            break;
    }
}

#pragma delegate of BMKMapView

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        
        
        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
        newAnnotationView.pinColor = BMKPinAnnotationColorPurple;
        newAnnotationView.animatesDrop = YES;// 设置该标注点动画显示
        newAnnotationView.paopaoView = [[BMKActionPaopaoView alloc]initWithCustomView:_paopaoView];
        
        return newAnnotationView;
    }
    return nil;
}

//接收反向地理编码结果
-(void) onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    
    if(error == BMK_SEARCH_NO_ERROR){
        LogInfo(@"检索成功");
        // 添加一个PointAnnotation
        BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc]init];
        annotation.coordinate = result.location;
        annotation.title = result.address;
        LogInfo(@"result.address:%@",result.address);
         [self initCustomPaoPaoView:annotation.title];
        //设置地址中心点
        _mapView.centerCoordinate = result.location;
        [_mapView addAnnotation:annotation];
        

    }else{
        LogInfo(@"抱歉，未找到结果");
    }
}

#pragma delegate of BMKLocationService
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    LogInfo(@"定位成功：%f,%f",userLocation.location.coordinate.longitude,userLocation.location.coordinate.latitude);
    _startNode.pt = userLocation.location.coordinate;
}

- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation{
    
}

- (void)didFailToLocateUserWithError:(NSError *)error{
    [self showTipViewWithMsg:@"获取当前位置失败"];
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
