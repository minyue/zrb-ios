//
//  BigPicture.h
//  ZhaoRongbao
//
//  Created by songmk on 15/10/8.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BigPicture : UIImageView

@property (nonatomic,copy) UIImageView *defaultImage;

@end
