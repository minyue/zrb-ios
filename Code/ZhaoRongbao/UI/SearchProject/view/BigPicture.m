//
//  BigPicture.m
//  ZhaoRongbao
//
//  Created by songmk on 15/10/8.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "BigPicture.h"

@implementation BigPicture

- (instancetype)init{
    
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    _defaultImage = [[UIImageView alloc]init];
    _defaultImage.backgroundColor = [UIColor clearColor];
    [self addSubview:_defaultImage];

    
    [_defaultImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@37);
        make.height.equalTo(@13);
        
    }];
    
}


@end
