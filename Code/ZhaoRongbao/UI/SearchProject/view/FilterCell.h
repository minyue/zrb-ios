//
//  FilterCell.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/2.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UICollectionViewCell

@property (nonatomic,strong) UIButton *tagBtn;

@end
