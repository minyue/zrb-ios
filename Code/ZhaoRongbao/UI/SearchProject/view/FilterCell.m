//
//  FilterCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/2.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell


- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup{
    
    
    self.tagBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.tagBtn.userInteractionEnabled = NO;
    [self.tagBtn setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [self.tagBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateSelected];
    
    [self.tagBtn setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [self.tagBtn setTitleColor:[UIColor colorWithHexString:@"007de3"] forState:UIControlStateSelected];
    
    self.tagBtn.layer.masksToBounds = YES;
    self.tagBtn.layer.cornerRadius = 8.0f;
    self.tagBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.tagBtn.layer.borderWidth = 1.0f;
    self.tagBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    
    [self addSubview:self.tagBtn];
    
    [self.tagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.width.equalTo(@80);
        make.height.equalTo(@30);

        
    }];
}

@end
