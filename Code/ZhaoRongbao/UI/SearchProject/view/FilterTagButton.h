//
//  FilterTagButton.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/2.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol FilterTagDelegate;

@interface FilterTagButton : UIButton {
    NSString                        *_groupId;
    BOOL                            _checked;
    id<FilterTagDelegate>           _delegate;
}

@property(nonatomic, assign)id<FilterTagDelegate>   delegate;
@property(nonatomic, copy, readonly)NSString            *groupId;
@property(nonatomic, assign)BOOL checked;

- (id)initWithDelegate:(id)delegate groupId:(NSString*)groupId;

@end

@protocol FilterTagDelegate <NSObject>

@optional

- (void)didSelectedRadioButton:(FilterTagButton *)radio groupId:(NSString *)groupId;

@end