//
//  FilterTagView.h
//  ZhaoRongbao
//
//  Created by songmk on 15/11/2.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterTagButton.h"

@interface FilterTagView : FilterTagButton<FilterTagDelegate>

@property (nonatomic,strong) UILabel *titleLB;

@end
