//
//  FilterTagView.m
//  ZhaoRongbao
//
//  Created by songmk on 15/11/2.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "FilterTagView.h"

@implementation FilterTagView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithDelegate:(id)delegate groupId:(NSString*)groupId {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

//- (instancetype)init{
//    
//    self = [super init];
//    if (self) {
//        [self setup];
//    }
//    
//    return self;
//}

- (void)setup{
    
    [self setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateSelected];
    
    [self setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorWithHexString:@"007de3"] forState:UIControlStateSelected];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 8.0f;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1.0f;
    
    _titleLB = [[UILabel alloc]init];
    _titleLB.userInteractionEnabled = NO;
    _titleLB.backgroundColor = [UIColor clearColor];
    _titleLB.textColor = [UIColor whiteColor];
    _titleLB.font = [UIFont systemFontOfSize:14.0f];
    
    [self addSubview:_titleLB];
    
    [_titleLB mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.mas_centerX);
//        make.centerY.equalTo(self.mas_centerY);
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.left.equalTo(self.mas_left).offset(5);
        make.right.equalTo(self.mas_right).offset(-5);
    }];
    
}

@end
