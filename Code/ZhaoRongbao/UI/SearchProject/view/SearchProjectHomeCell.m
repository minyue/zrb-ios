//
//  SearchProjectHomeCell.m
//  ZhaoRongbao
//
//  Created by songmk on 15/9/24.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "SearchProjectHomeCell.h"



@implementation SearchProjectHomeCell

+(instancetype) cellForTabvleView:(UITableView *)tableView{
    
    static NSString *ID = @"";
    
    SearchProjectHomeCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[SearchProjectHomeCell alloc]init];
    }
    
    return cell;
}

- (instancetype)init{
    
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}


- (void)setup
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:view];
    
    //左边大图
    _bigPic = [[UIImageView alloc]init];
    _bigPic.backgroundColor = [UIColor clearColor];
    _bigPic.contentScaleFactor = [[UIScreen mainScreen] scale];
    _bigPic.contentMode = UIViewContentModeScaleAspectFit;
    _bigPic.layer.borderWidth = 0.5f;
    _bigPic.layer.borderColor = [UIColor colorWithHexString:@"bdbdbd"].CGColor;
    _bigPic.backgroundColor = [UIColor colorWithHexString:@"e4e4e4"];
    [view addSubview:_bigPic];
    
    _defaultImage = [[UIImageView alloc]init];
    _defaultImage.image = [ZRBUtilities ZRB_DefaultImage];
    _defaultImage.backgroundColor = [UIColor clearColor];
    [_bigPic addSubview:_defaultImage];
    
    //认证图标
    _certificationPic = [[UIImageView alloc]init];
    [_bigPic addSubview:_certificationPic];
    
    //左边大图，中的类型图
//    _typePic = [[IndustryTagView alloc]init];
//    [_bigPic addSubview:_typePic];
    
    //项目标题
    _title = [[BTLabel alloc]init];
    _title.numberOfLines = 0;
    _title.textAlignment = NSTextAlignmentLeft;
    _title.verticalAlignment = BTVerticalAlignmentTop;
    [_title setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
    [view addSubview:_title];
    
    _industry = [[UILabel alloc]init];
    _industry.numberOfLines = 1;
    _industry.font = [UIFont systemFontOfSize:12.0f];
    _industry.textColor = [UIColor colorWithHexString:@"686d6b"];
    [view addSubview:_industry];
    
  
    
    //认证类型名称
    _certificationLB = [[UILabel alloc]init];
    _certificationLB.font = [UIFont systemFontOfSize:13.0f];
//    _certificationLB.layer.borderWidth = 1.0;
    _certificationLB.textAlignment = NSTextAlignmentCenter;
//    [view addSubview:_certificationLB];
    
    //面积
    _areaLB = [[UILabel alloc]init];
    _areaLB.font = [UIFont systemFontOfSize:12.0f];
    _areaLB.textColor = [UIColor colorWithHexString:@"b3b3b3"];
    _areaLB.backgroundColor = [UIColor clearColor];
    [view addSubview:_areaLB];
    
    //分割线
    _line = [[UIView alloc]init];
    _line.backgroundColor = [UIColor lightGrayColor];
    [view addSubview:_line];
    
    //估值
    _price = [[UILabel alloc]init];
    _price.font = [UIFont systemFontOfSize:12.0f];
    _price.textColor = [UIColor colorWithRed:83.0/255.0 green:158.0/255.0 blue:223.0/255.0 alpha:1.0];
    _price.backgroundColor = [UIColor clearColor];
    [view addSubview:_price];
    
    //日期
    _date = [[UILabel alloc]init];
    _date.font = [UIFont systemFontOfSize:12.0f];
    _date.textColor = [UIColor lightGrayColor];
    [_date sizeToFit];
    _date.backgroundColor = [UIColor clearColor];
    [view addSubview:_date];
    
    //地址
    _location = [[UILabel alloc]init];
    _location.font = [UIFont systemFontOfSize:12.0f];
    _location.textColor = [UIColor colorWithHexString:@"b3b3b3"];
    [_location sizeToFit];
    _location.backgroundColor = [UIColor clearColor];
    [view addSubview:_location];
    
    
    [view makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.left.equalTo(self.contentView.mas_left);
        make.width.equalTo(self.contentView.mas_width);
        make.height.equalTo(self.contentView.mas_height);
    }];
    
    [_bigPic makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(10);
        make.top.equalTo(view.mas_top).offset(10);
        make.width.equalTo(@111);
        make.height.equalTo(@81);
    }];
    
    [_defaultImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_bigPic.mas_centerX);
        make.centerY.equalTo(_bigPic.mas_centerY);
        make.width.equalTo(@37);
        make.height.equalTo(@13);
    }];
    
//    [_typePic makeConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(@20);
//        make.height.equalTo(@20);
//        make.left.equalTo(_bigPic.mas_left).offset(5);
//        make.top.equalTo(_bigPic.mas_top).offset(5);
//        
//    }];
    
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bigPic.mas_top);
        make.left.equalTo(_bigPic.mas_right).offset(10);
        make.right.equalTo(view.mas_right).offset(-10);
//        make.height.equalTo(@25);
    }];
    
    [_certificationPic mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bigPic.mas_left).offset(5);
        make.bottom.equalTo(_bigPic.mas_bottom).offset(-5);
        make.width.equalTo(@26);
        make.height.equalTo(@26);
        
    }];
    
//    [_certificationLB mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_certificationPic.mas_top);
//        make.bottom.equalTo(_certificationPic.mas_bottom);
//        make.left.equalTo(_certificationPic.mas_right);
//        make.width.equalTo(@60);
//    }];
    
    [_industry mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bigPic.mas_right).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.centerY.equalTo(_bigPic.mas_centerY).offset(5);
    }];
    
    [_areaLB mas_makeConstraints:^(MASConstraintMaker *make) {
        
//        make.top.equalTo(_certificationLB.mas_bottom).offset(10);
        make.left.equalTo(_bigPic.mas_right).offset(10);
        make.bottom.equalTo(view.mas_bottom).offset(-10);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_areaLB.mas_right).offset(5);
//        make.top.equalTo(_areaLB.mas_top);
//        make.bottom.equalTo(_areaLB.mas_bottom);
        make.centerY.equalTo(_areaLB.mas_centerY);
        make.height.equalTo(@10);
        make.width.equalTo(@1);
        
    }];
    
    [_price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_line.mas_right).offset(5);
        make.top.equalTo(_areaLB.mas_top);
        make.bottom.equalTo(_areaLB.mas_bottom);
        make.width.equalTo(@90);
    }];
    
    [_date mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view.mas_right).offset(-10);
        make.top.equalTo(_price.mas_top);
        make.bottom.equalTo(_price.mas_bottom);
        //        make.width.equalTo(@60);
    }];
    
    [_location mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_date.mas_left).offset(-7);
        make.top.equalTo(_price.mas_top);
        make.bottom.equalTo(_price.mas_bottom);
        make.left.equalTo(_price.mas_right).offset(5);
        //        make.width.equalTo(@40);
    }];
}


- (void)setModel:(SearchProjectHomeItemModel *)model{
    //大图和资产类型图
//    _bigPic.backgroundColor = [UIColor whiteColor];
//    [_bigPic sd_setImageWithURL:[ZRBUtilities urlWithPath:model.pic] placeholderImage:[ZRBUtilities ZRB_DefaultImage] options:SDWebImageRetryFailed];
//    _typePic.image = [UIImage imageNamed:[self iconWithProjectType:model.projectType]];
    
    
    [_bigPic sd_setImageWithURL:[ZRBUtilities urlWithPath:model.pic] placeholderImage:nil options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error) {
//            NSLog(@"图片加载失败：%@",error);
            [_bigPic addSubview:_defaultImage];
            
            [_defaultImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_bigPic.mas_centerX);
                make.centerY.equalTo(_bigPic.mas_centerY);
                make.width.equalTo(@37);
                make.height.equalTo(@13);
            }];
            
        }else{
            _bigPic.image = [ZRBUtilities imageWithImage:image scaledToSize:CGSizeMake(111, 81)];
            _bigPic.backgroundColor = [UIColor clearColor];
            [_defaultImage removeFromSuperview];

        }
    }];
    
    _industry.text = [NSString stringWithFormat:@"所属行业：%@",model.industryStr];
    
    if([model.authStr isEqualToString:@"实地认证"]){
        _certificationPic.image = [UIImage imageNamed:@"sdrz"];
//        self.certificationPic.hidden = NO;
//        self.certificationLB.text = model.authStr;
//        _certificationLB.textColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
        
    }else if([model.authStr isEqualToString:@"资料认证"]){
         _certificationPic.image = [UIImage imageNamed:@"zlrz"];
        
    }else{
        self.certificationPic.hidden = YES;
    }
    
    //标题
    [_title setText:[NSString stringWithFormat:@"%@",model.projectName]];
    
    //认证图标和类型
//    _certificationPic.image = [UIImage imageNamed:[self iconWithValidateType:[model.auth intValue]]];
    
    //m面积
//    if([model.modeStr isEqualToString:@"不限"]){
//        _areaLB.text = [NSString stringWithFormat:@"方式%@",model.modeStr];
//        
//    }else{
//        _areaLB.text = [NSString stringWithFormat:@"%@投资",model.modeStr];
//    }
    
     _areaLB.text = model.modeStr;
    
    //估值
    _price.text = [NSString stringWithFormat:@"%@万",model.total];
    
    //日期
    _date.text = [ZRBUtilities stringToData:@"MM月dd日" interval:[NSString stringWithFormat:@"%@",model.pushtime]];
    
    //地址
    
    NSString *locationStr = @"";
    if ([model.province containsString:@"省"]|| [model.province containsString:@"市"]) {
        locationStr = [model.province substringToIndex:model.province.length - 1];
    }else{
        locationStr = model.province;
    }
    
    _location.text = locationStr;
    
}

//资产类型图片
- (NSString *)iconWithProjectType:(int)projectType
{
    switch (projectType) {
        case 101:
            
            return @"project_place_plable";
            break;
        case 102:
            return @"project_ppp_plabel";
            break;
        case 103:
            return @"project_assets_plable";
            break;
        default:
            return nil;
            break;
    }
}

- (NSString *)iconWithValidateType:(int)validateType{
    switch (validateType) {
            
        case 500:       //未认证  不显示
//            _certificationLB.text = @"未认证";
//            _certificationLB.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
//            _certificationLB.textColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
//            return @"project_telephone_authentication";
            _certificationLB.text = @"";
            return @"";

            break;
        case 502:       //电话认证
            _certificationLB.text = @"实地认证";
            _certificationLB.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
            _certificationLB.textColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
            return @"shidi_verify";
            break;
        case 503:       //资料认证
            _certificationLB.text = @"资料认证";
            _certificationLB.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0].CGColor;
            _certificationLB.textColor = [UIColor colorWithRed:68.0/255.0 green:112.0/255.0 blue:200.0/255.0f alpha:1.0];
            
            return @"shidi_verify";
            break;
        default:
            _certificationLB.text = @"";
            return @"";
            break;
    }
}

@end
