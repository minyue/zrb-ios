//
//  NSDate+Addition.h
//  CenterMarket
//
//  Created by 周光 on 14-7-10.
//  Copyright (c) 2014年 yurun. All rights reserved.
//

#import <Foundation/Foundation.h>


// 日期字符串常用格式，NSDATE_FORMAT_NORMAL为默认格式。
#define NSDATE_FORMAT_NORMAL    @"yyyy-MM-dd HH:mm:ss"
#define NSDATE_FORMAT_NORMAL_1  @"yyyy/MM/dd HH:mm:ss"
#define NSDATE_FORMAT_DATE      @"yyyy-MM-dd"
#define NSDATE_FORMAT_DATE_1    @"yyyy/MM/dd"
#define NSDATE_FORMATE_DATE_2   @"yyyyMMddHHmmss"
#define NSDATE_FORMAT_TIME      @"HH:mm:ss"
#define NSDATE_FORMATE_TIME_1   @"HHmmss"


#define NSDATE_FORMAT_DATEANDTIME     @"yyyy-MM-dd HH:mm"


#define NSDATE_FORMAT_COUPON   @"yyyy年MM月dd日"


#define NSDATE_FORMAT_COUPON_1 @"yyyy.MM.dd"

#define NSDATE_FORMAT_COUPON_2 @"yyyy年MM月dd日 HH:mm:ss"


#define NSDATE_FORMAT_COUPON_3 @"yyyy-MM-dd"

#define NSDATE_FORMAT_COUPON_5 @"MM月dd日"

#define NSDATE_FORMAT_COUPON_4 @"HH:mm"


#define NSATE_FORMAT_COUPON_6 @"MM.dd"

// 常用地区
#define LOCALE_CHINA [[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans_CN"]
#define LOCALE_USA [[NSLocale alloc] initWithLocaleIdentifier:@"es_US"]

@interface NSDate (Addition)


#pragma mark
#pragma mark - ** 日期类方法 **


/**
 *  字符串格式化日期类
 *  @param string 普通字符串
 *
 *  @return 日期类
 */
+ (id)dateWithString:(NSString *)string;
+ (id)dateWithDateString:(NSString *)string;
+ (id)dateWithTimeString:(NSString *)string;
+ (id)dateWithString:(NSString *)string format:(NSString *)format;
+ (id)dateWithString:(NSString *)string locale:(NSLocale *)locale;
+ (id)dateWithString:(NSString *)string format:(NSString *)format locale:(NSLocale *)locale;




/**
 *  格式化日期字符串
 *
 *  @param string 日期字符串
 *
 *  @return 格式化的日期字符串
 */
+ (NSString *)dateFormatString:(NSString *)string;

/**
 *  返回当前时间 yyyyMMddHHmmss 格式
 *
 *
 *  @return 返回当前时间 yyyyMMddHHmmss NSString
 */
+ (NSString *)dateNowFormatString;
/**
 *  返回当前时间  任意格式
 *
 *
 *  @return 返回当前时间
 */
+ (NSString *)dateNowFormatString:(NSString*)format;
/**
 *  返回任意时间  任意格式
 *  时间戳
 *
 *  @return 返回传入时间
 */
+ (NSString *)dateNowFormatString:(NSString*)format andDate:(double)qtime;
/**
 *  优惠劵日期格式化 
 *
 *  @param string 输入字符串
 *
 *  @return 格式化的日期字符串
 */
+ (NSString *)couponDateFormatString:(NSString *)string  format:(NSString *)format;



+ (NSString *)couponDateFormatstring:(NSString *)string  local:(NSString *)local    fromat:(NSString *)format;



+ (NSString *)timeFormatWithTimeStamp:(NSString *)stamp format:(NSString *)format;

@end
