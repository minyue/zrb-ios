//
//  NSDate+Addition.m
//  CenterMarket
//
//  Created by 周光 on 14-7-10.
//  Copyright (c) 2014年 yurun. All rights reserved.
//

#import "NSDate+Addition.h"
@implementation NSDate (Addition)


#pragma mark
#pragma mark - ** 日期类方法 **
+ (id)dateWithString:(NSString *)string
{
    return [NSDate dateWithString:string format:NSDATE_FORMAT_NORMAL locale:LOCALE_CHINA];
}

+ (id)dateWithDateString:(NSString *)string
{
    return [NSDate dateWithString:string format:NSDATE_FORMAT_DATE];
}

+ (id)dateWithTimeString:(NSString *)string
{
    return [NSDate dateWithString:string format:NSDATE_FORMAT_TIME];
}
+ (id)dateWithDateAndTimeString:(NSString *)string
{
    return [NSDate dateWithString:string format:NSDATE_FORMAT_TIME];
}
+ (id)dateWithString:(NSString *)string format:(NSString *)format
{
    return [NSDate dateWithString:string format:format locale:LOCALE_CHINA];
}

+ (id)dateWithString:(NSString *)string locale:(NSLocale *)locale
{
    return [NSDate dateWithString:string format:NSDATE_FORMAT_NORMAL locale:LOCALE_CHINA];
}

+ (id)dateWithString:(NSString *)string format:(NSString *)format locale:(NSLocale *)locale
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:locale];
    NSDate *retDate = [dateFormatter dateFromString:string];
    return retDate;
}





+ (NSString *)dateFormatString:(NSString *)string
{

    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:LOCALE_CHINA];
    [inputFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSDate* inputDate = [inputFormatter dateFromString:string];


    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:NSDATE_FORMAT_DATEANDTIME];
    [dateFormatter setLocale:LOCALE_CHINA];
    return [dateFormatter stringFromDate:inputDate];
    
}


+ (NSString *)dateNowFormatString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:NSDATE_FORMATE_DATE_2];
    [dateFormatter setLocale:LOCALE_CHINA];
    return [dateFormatter stringFromDate:[NSDate date]];
}

+ (NSString *)dateNowFormatString:(NSString*)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:LOCALE_CHINA];
    return [dateFormatter stringFromDate:[NSDate date]];
}

+ (NSString *)dateNowFormatString:(NSString*)format andDate:(double)qtime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:LOCALE_CHINA];
    return [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:qtime]];
}


/**
 *  优惠劵相关阁书画
 *
 *  @param string 输入字符串
 *
 *  @return 格式化的日期字符串
 */
+ (NSString *)couponDateFormatString:(NSString *)string format:(NSString *)format
{
    
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:LOCALE_CHINA];
    [inputFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSDate* inputDate = [inputFormatter dateFromString:string];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:LOCALE_CHINA];
    return [dateFormatter stringFromDate:inputDate];
}

+ (NSString *)couponDateFormatstring:(NSString *)string  local:(NSString *)local    fromat:(NSString *)format
{
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:LOCALE_CHINA];
    [inputFormatter setDateFormat:local];
    NSDate* inputDate = [inputFormatter dateFromString:string];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:LOCALE_CHINA];
    return [dateFormatter stringFromDate:inputDate];

}

+ (NSString *)timeFormatWithTimeStamp:(NSString *)stamp format:(NSString *)format
{

    NSDate *date = [self dateWithTimeIntervalSince1970:(stamp.doubleValue / 1000)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:LOCALE_CHINA];
    return [dateFormatter stringFromDate:date];

}

@end
