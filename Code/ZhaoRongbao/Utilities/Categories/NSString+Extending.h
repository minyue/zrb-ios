//
//  NSString+Extending.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/29.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extending)


/**
 *  计算字符串根据字体属性所需要的具体大小
 *
 *  @param asize 容器大小 （eg:UILabel的大小）
 *  @param font  字符串的字体
 *
 *  @return 返回计算的大小
 */
- (CGSize)boundingRectWithSize:(CGSize)asize  font:(UIFont*)font;
@end
