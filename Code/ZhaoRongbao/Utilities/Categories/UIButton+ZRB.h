//
//  UIButton+ZRB.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ZRB_ButtonType){
    /**
     *  系统默认类型
     */
    ZRB_ButtonTypeNormal = 0,
    
    /**
     * 类似（登录、注册） 操作按钮
     */
    ZRB_ButtonTypeOperation,
    /**
     *  带下划线的按钮
     */
    ZRB_ButtonTypeUnderLine
    
    /**
     *  以下可以添加其他样式
     */
};

@interface UIButton (ZRB)


- (void)configButtonWithType:(ZRB_ButtonType)type;


- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;


- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state alpa:(CGFloat)alpa;

@end
