//
//  UIButton+ZRB.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/15.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "UIButton+ZRB.h"

@implementation UIButton (ZRB)

- (void)configButtonWithType:(ZRB_ButtonType)type
{

    switch (type) {
        case ZRB_ButtonTypeNormal:
        {
            [self configNormalButton];
            break;
        }
        case ZRB_ButtonTypeOperation:
        {
            [self configOperationButton];
            break;
        }
        case ZRB_ButtonTypeUnderLine:
        {
            [self configUnderLineButton];
            break;
        }
        default:
            break;
    }
}


- (void)configNormalButton
{
}


- (void)configOperationButton
{
    [self setBackgroundColor:[UIColor colorWithHexString:@"4997ec"] forState:UIControlStateNormal];
    
    self.layer.cornerRadius = 2.0f;
    self.layer.masksToBounds = YES;
    [self setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];

}

- (void)configUnderLineButton
{
    [self setTitleColor:[UIColor colorWithHexString:@"0077d9"]];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text];
    NSRange strRange = {0,[str length]};
    [str addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:strRange];
    [self setAttributedTitle:str forState:UIControlStateNormal];

}



- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state {
    [self setBackgroundImage:[UIButton imageWithColor:backgroundColor] forState:state];
}



- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state alpa:(CGFloat)alpa
{

    [self setBackgroundImage:[self imageByApplyingAlpha:alpa image:[UIButton imageWithColor:backgroundColor]] forState:state];
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}




- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    
    CGContextSetAlpha(ctx, alpha);
    
    CGContextDrawImage(ctx, area, image.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
@end
