//
//  UIImageView+TapGesture.h
//  CenterMarket
//
//  Created by zhoug on 14-8-20.
//  Copyright (c) 2014年 yurun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (TapGesture)


/**
 *  给UIImageView添加事件响应
 *
 *  @param target 响应者
 *  @param action 响应事件
 */
- (void)addTapGestureWithTarget:(id)target action:(SEL)action;


@end
