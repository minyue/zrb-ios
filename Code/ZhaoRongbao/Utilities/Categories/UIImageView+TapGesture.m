//
//  UIImageView+TapGesture.m
//  CenterMarket
//
//  Created by zhoug on 14-8-20.
//  Copyright (c) 2014年 yurun. All rights reserved.
//

#import "UIImageView+TapGesture.h"

@implementation UIImageView (TapGesture)

/**
 *  给UIImageView添加事件响应
 *
 *  @param target 响应者
 *  @param action 响应事件
 */
- (void)addTapGestureWithTarget:(id)target action:(SEL)action
{
    self.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
    singleTap.view.tag = self.tag;
    [self addGestureRecognizer:singleTap];
    
}



@end
