//
//  UILabel+VerticalUpAlignment.h
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/14.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (VerticalUpAlignment)

- (void)setTextTop;

@end
