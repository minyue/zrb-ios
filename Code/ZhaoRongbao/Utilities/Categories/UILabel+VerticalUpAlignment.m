//
//  UILabel+VerticalUpAlignment.m
//  ZhaoRongbao
//
//  Created by 宋明康 on 15/8/14.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "UILabel+VerticalUpAlignment.h"

//每行最多显示几个字符
#define kMaxCount  50

@implementation UILabel (VerticalUpAlignment)
- (void)setTextTop{
    //可变数组接收text
    NSMutableString *resultStr = [self.text mutableCopy];
    //以回车键分字符串数组
    NSArray  *array = [resultStr componentsSeparatedByString:@"\n"];
    NSInteger temp = 0;
    
    //取得label行数
    NSInteger  lines = self.lineBreakMode;
    
    //取得要增加或减少的行数
    for (int j = 0 ; j < array.count; j++) {
        NSString *str = array[j];
        //因为以回车键区分，故存在空的字符串，减掉
        if ([str  isEqual: @""]) {
            temp --;
            
        }
        //因为是自动换行的，故长度超过一行的要2行显示，加上一行
        if (str.length > kMaxCount) {
            temp ++;
        }
    }
    
    //不够的行数 用回车加空格补齐
    for (int i = 0; i < (lines - array.count - temp + 1); i++){
        [resultStr appendString:@"\n  "];
    }
    
    self.text = resultStr;
    
}

@end
