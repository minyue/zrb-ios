//
//  UIViewController+Child.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/30.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Child)

- (void)addToParentViewController:(UIViewController *)parentViewController callingAppearanceMethods:(BOOL)callAppearanceMethods;

- (void)removeFromParentViewControllerCallingAppearanceMethods:(BOOL)callAppearanceMethods;



@end
