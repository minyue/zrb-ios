//
//  HUDManager.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/7.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <Foundation/Foundation.h>
//hud视图上的事件
typedef void(^HUDEventBlock) ();

@interface HUDManager : NSObject

/**
 *  展示没有网络HUD
 *
 *  @param view  父视图
 *  @param event 相关事件
 */
+ (void)showNonNetWorkHUDInView:(UIView *)view  event:(HUDEventBlock)event ;


/**
 *  展示没有数据的HUD
 *
 *  @param view 父视图
 */
+ (void)showNonDataHUDInView:(UIView *)superView  withMsg:(NSString *)message;

/**
 *  网络加载中
 *
 *  @param view 父视图
 */
+ (void)showLoadNetWorkHUDInView:(UIView *)view;


/**
 *  从父视图移除HUD
 *
 *  @param view 父视图
 */
+ (void)removeHUDFromView:(UIView *)view;
@end
