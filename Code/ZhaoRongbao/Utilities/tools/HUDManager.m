//
//  HUDManager.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/7.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "HUDManager.h"
#import "ZRB_HUD.h"
#import "StoryBoardUtilities.h"

#define HUD_TAG_NONNETWORK  20151314        //没有网络

#define HUD_TAG_NODATA  20151315            //没有数据

#define HUD_TAG_LOAD    20151316            //网络加载


@implementation HUDManager


+ (void)showNonNetWorkHUDInView:(UIView *)view event:(HUDEventBlock)event
{
    
    [HUDManager removeNonNetWorkHudFormView:view];
    
    ZRB_HUD *hud = (ZRB_HUD *)[ZRBUtilities viewFromSourceName:@"ZRB_HUD"];
    hud.tag = HUD_TAG_NONNETWORK;
    hud.hudClickBlock = ^{
        if (event) {
            event();
        }
    };
    [view addSubview:hud];
    [view bringSubviewToFront:hud];
    [hud makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    
}

/**
 *  展示没有数据的HUD
 *
 *  @param view 父视图
 */
+ (void)showNonDataHUDInView:(UIView *)superView  withMsg:(NSString *)message
{
    [HUDManager removeNonDataHudFromView:superView];
    
    UIView   *returnView = [[UIView alloc] init];
    returnView.tag = HUD_TAG_NODATA;
    [superView addSubview:returnView];
    [superView bringSubviewToFront:returnView];

    [returnView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(superView);
    }];
    
    
    UIImageView* imgView = [[UIImageView alloc]init];
    imgView.image = [UIImage imageNamed:@"pic_sorry"];
    [returnView addSubview:imgView];
   [imgView makeConstraints:^(MASConstraintMaker *make) {
       make.centerX.equalTo(returnView.mas_centerX);
       make.centerY.equalTo(returnView.mas_centerY).offset(-50);
   }];
    
    
    UILabel* label = [[UILabel alloc]init];
    label.text = message;
    label.font = [UIFont systemFontOfSize:14.0f];
    label.textColor = [UIColor colorWithHexString:@"666d75"];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.adjustsFontSizeToFitWidth = YES;
    [returnView addSubview:label];
    [label makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(returnView.mas_centerX);
        make.top.equalTo(imgView.mas_bottom).offset(15);
    }];
    

}


/**
 *  网络加载中
 *
 *  @param view 父视图
 */
+ (void)showLoadNetWorkHUDInView:(UIView *)view
{
    [HUDManager removeLoadHudFromView:view];
    
    UIView *loading = [[UIView alloc] init];
    loading.backgroundColor = [UIColor whiteColor];
    loading.tag = HUD_TAG_LOAD;
    [view addSubview:loading];
    [loading makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
    
    UIActivityIndicatorView *wv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [wv setColor:[UIColor grayColor]];
    [wv  startAnimating];
    [loading addSubview:wv];
    [wv makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(loading.mas_centerX);
        make.centerY.equalTo(loading.mas_centerY);
    }];
}


+ (void)removeHUDFromView:(UIView *)view
{
    
    UIView  *hud1 = [view viewWithTag:HUD_TAG_NONNETWORK];
    if (hud1) {
        [hud1 removeFromSuperview];
        hud1 = nil;
    }
    
    UIView  *hud2 = [view viewWithTag:HUD_TAG_NODATA];
    
    if (hud2) {
        [hud2 removeFromSuperview];
        hud2 = nil;
    }
    
    UIView  *hud3 = [view viewWithTag:HUD_TAG_LOAD];
    
    if (hud3) {
        [hud3 removeFromSuperview];
        hud3 = nil;
    }

}


+ (void)removeNonNetWorkHudFormView:(UIView *)view
{
    
    UIView  *hud1 = [view viewWithTag:HUD_TAG_NONNETWORK];
    if (hud1) {
        [hud1 removeFromSuperview];
        hud1 = nil;
    }

}



+ (void)removeNonDataHudFromView:(UIView *)view
{

    UIView  *hud2 = [view viewWithTag:HUD_TAG_NODATA];
    
    if (hud2) {
        [hud2 removeFromSuperview];
        hud2 = nil;
    }

}


+ (void)removeLoadHudFromView:(UIView *)view
{
    
    UIView  *hud3 = [view viewWithTag:HUD_TAG_LOAD];
    
    if (hud3) {
        [hud3 removeFromSuperview];
        hud3 = nil;
    }

}
@end
