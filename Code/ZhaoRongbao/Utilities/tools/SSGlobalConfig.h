//
//  SSGlobalConfig.h
//  TChat
//
//  Created by weihuafeng on 15-3-21.
//  Copyright (c) Sinosun Technology Co., Ltd. All rights reserved..
//  全局配置类

#import <Foundation/Foundation.h>

@interface SSGlobalConfig : NSObject




+ (instancetype)defaultConfig;


/**
 *  初始化一个配置信息类
 *
 *  @param phoneNumber 手机号
 *
 *  @return SSGlobalConfig
 */
- (id)initWithPhoneNumer:(NSString*)phoneNumber;

/**
 *  设置参数值
 *
 *  @param paramName 参数名
 *  @param value     值
 */
- (BOOL)setParam:(NSString*)paramName value:(id)value;

/**
 *  获取参数值
 *
 *  @param paramName 参数名
 *
 *  @return 返回值
 */
- (id)getParam:(NSString*)paramName;

@end




