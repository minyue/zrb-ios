//
//  SSGlobalConfig.m
//  TChat
//
//  Created by weihuafeng on 15-3-21.
//  Copyright (c) Sinosun Technology Co., Ltd. All rights reserved..
//

#import "SSGlobalConfig.h"
#import "ZRBpath.h"

#define kDefaultConfigFile1 [[ZRBpath  dataFilePath] stringByAppendingString:@"defaultConfig.plist"]

@interface SSGlobalConfig ()
@property (nonatomic,strong) NSString * configFilePath;
@property (nonatomic,strong) NSMutableDictionary * configDic;

@end

@implementation SSGlobalConfig
+ (instancetype)defaultConfig
{
    __strong static SSGlobalConfig *_singleton = nil;
    static dispatch_once_t  pred;
    
    dispatch_once(&pred, ^{
        _singleton = [[self alloc] initDefaultConfig];
    });
    return _singleton;
}

- (id)init
{
    if (self = [super init]) {
        _configFilePath = nil;
        _configDic = nil;
    }
    return self;
}

- (id)initDefaultConfig
{
    self = [self init];
    if (self) {
        _configFilePath =  [ZRBpath plistPath];
        if ( ! [self readConfig]) {
            _configDic = [NSMutableDictionary dictionary];
        };
        
    }
    return self;
}


//- (id)initWithPhoneNumer:(NSString*)phoneNumber
//{
//    self = [self init];
//    if (self) {
//        
//        if (phoneNumber == nil || phoneNumber.length == 0) {
//            return nil;
//        }
//        _configFilePath =  kGlobalConfigFile(phoneNumber);
//        if ( ! [self readConfig]) {
//            NSLog(@"SSGlobalConfig: readConfig fail.create new NSMutableDictionary");
//            _configDic = [NSMutableDictionary dictionary];
//        };
//        
//    }
//    return self;
//}


- (BOOL)setParam:(NSString*)paramName value:(id)value
{
    if (paramName == nil || paramName.length == 0 || value == nil) {
        return NO;
    }
    [_configDic setObject:value forKey:paramName];
    return [self saveConfig];
}


- (id)getParam:(NSString*)paramName
{
    return [_configDic objectForKey:paramName];
}

- (BOOL)saveConfig
{
    BOOL returnBool = [_configDic writeToFile:_configFilePath atomically:YES];
    return returnBool;
}

- (BOOL)readConfig
{
    if (_configFilePath && _configFilePath.length > 0) {
         _configDic =  [[NSMutableDictionary alloc] initWithContentsOfFile:_configFilePath];
    }
    return _configDic != nil;
}

@end





