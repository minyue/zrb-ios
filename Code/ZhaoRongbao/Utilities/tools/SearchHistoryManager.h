//
//  SearchHistoryManager.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <Foundation/Foundation.h>

@class  HistoryItem;
@interface SearchHistoryManager : NSObject


/**
 *  查询搜索历史记录
 *
 *  @return 返回搜索历史
 */
+ (NSMutableArray *)historyData;


/**
 *  添加历史记录
 *
 *  @param obj 需要添加的记录
 */
+ (void)addHistroyObject:(NSString *)obj;


+ (void)insertObject:(HistoryItem *)item;



+ (void)removeObject:(HistoryItem *)item;


+ (void)removeAll;

/**
 *  修改历史记录
 *
 *  @param array 修改后的记录
 */
+ (void)modifyHistoryData:(NSMutableArray *)array;




@end




/**
 *  搜索实体
 */
@interface HistoryItem : NSObject

@property (nonatomic,copy) NSString  *title;

@property (nonatomic,assign) int     mid;

@end

