//
//  SearchHistoryManager.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/6.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "SearchHistoryManager.h"
#import "CoreArchive.h"
static NSString  *const   kHistoryPath =  @"searchHistory.plist";

static const int   kMaxNum  = 10;


@interface  SearchHistoryManager()

@property (nonatomic,strong) NSMutableArray  *histroyArray;
+ (instancetype)shareSearchManager;

@end

@implementation SearchHistoryManager

+ (instancetype)shareSearchManager
{
    static SearchHistoryManager  *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[SearchHistoryManager alloc] init];
    });
    return instance;
}


- (id)init
{
    if (self = [super init]) {
        self.histroyArray = [NSMutableArray new];
    }
    return self;
}



+ (NSString *)historyFilePath
{
    //获取document路径,括号中属性为当前应用程序独享
    NSArray *directoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentDirectory = [directoryPaths objectAtIndex:0];
    //定义记录文件全名以及路径的字符串filePath
    return  [documentDirectory stringByAppendingPathComponent:kHistoryPath];
}



/**
 *  查询搜索历史记录
 *
 *  @return 返回搜索历史
 */
+ (NSMutableArray *)historyData
{

    SearchHistoryManager  *instance = [SearchHistoryManager shareSearchManager];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:[SearchHistoryManager historyFilePath]])
    {
        NSDictionary  *dic = [CoreArchive unarchiveObjectWithFile:[SearchHistoryManager historyFilePath]];
        NSMutableArray *temp = [dic objectForKey:@"HISTORY"];
        instance.histroyArray = temp;
    }else
    {
        NSMutableArray  *temp = [[NSMutableArray alloc] init];
        instance.histroyArray = temp;
    }
    
    return instance.histroyArray;

}


/**
 *  添加历史记录
 *
 *  @param obj 需要添加的记录
 */
+ (void)addHistroyObject:(NSString *)obj
{
    SearchHistoryManager*instance = [SearchHistoryManager shareSearchManager];
    if (![ZRBUtilities  isBlankString:obj])
    {
        //去重
        BOOL flag = NO;
        for (int i=0; i<instance.histroyArray.count; i++)
        {
            //去重
            if ([obj isEqual:[instance.histroyArray objectAtIndex:i]])
            {
                flag = YES;
            }
        }
        
        
        if (flag == NO && ![ZRBUtilities isBlankString:obj])
        {
            //控制存放历史数量
            if (instance.histroyArray.count== kMaxNum )
            {
                [instance.histroyArray removeObjectAtIndex:0];
            }
            [instance.histroyArray addObject:obj];
            
            NSDictionary* dic1 = [NSDictionary dictionaryWithObject:instance.histroyArray forKey:@"HISTORY"];
            
            [CoreArchive     archiveRootObject:dic1 toFile:[SearchHistoryManager historyFilePath]];
        }
        
    }

}


+ (void)insertObject:(HistoryItem *)item
{
    SearchHistoryManager*instance = [SearchHistoryManager shareSearchManager];

    NSMutableArray  *itemArray = instance.histroyArray;

    //去掉重复
    
    BOOL  flag = NO;
    for (HistoryItem *tempItem in itemArray)
    {
        /**
         *  判断是否重复
         */
        if (item.mid == -1  && tempItem.mid == -1)
        {
            if ([tempItem.title isEqualToString:item.title])
            {
                flag = YES;
            }
        }else
        {
            if (tempItem.mid == item.mid)
            {
                flag = YES;
            }
        }
        
    }
    
    //如果是新添加的数据
    if (!flag) {
        //控制存放历史数量
        if (instance.histroyArray.count== kMaxNum )
        {
            [instance.histroyArray removeObjectAtIndex:0];
        }
        [instance.histroyArray addObject:item];
        
        NSDictionary* dic1 = [NSDictionary dictionaryWithObject:instance.histroyArray forKey:@"HISTORY"];
        
        [CoreArchive     archiveRootObject:dic1 toFile:[SearchHistoryManager historyFilePath]];


    }

}

+ (void)removeObject:(HistoryItem *)item
{
    SearchHistoryManager*instance = [SearchHistoryManager shareSearchManager];
    [instance.histroyArray removeObject:item];
    
    NSDictionary* dic1 = [NSDictionary dictionaryWithObject:instance.histroyArray forKey:@"HISTORY"];
    
    [CoreArchive     archiveRootObject:dic1 toFile:[SearchHistoryManager historyFilePath]];


}


+ (void)removeAll
{
    SearchHistoryManager*instance = [SearchHistoryManager shareSearchManager];
    [instance.histroyArray removeAllObjects];
    
    NSDictionary* dic1 = [NSDictionary dictionaryWithObject:instance.histroyArray forKey:@"HISTORY"];
    
    [CoreArchive     archiveRootObject:dic1 toFile:[SearchHistoryManager historyFilePath]];

}

/**
 *  修改历史记录
 *
 *  @param array 修改后的记录
 */
+ (void)modifyHistoryData:(NSMutableArray *)array
{
    NSDictionary* dic1 = [NSDictionary dictionaryWithObject:array forKey:@"HISTORY"];
    [CoreArchive     archiveRootObject:dic1 toFile:[SearchHistoryManager historyFilePath]];

}
@end




@implementation  HistoryItem

- (id)init
{
    if (self = [super init])
    {
        /**
         *  默认为-1
         */
        self.mid = -1;
    }
    return self;
}


MJCodingImplementation

@end

