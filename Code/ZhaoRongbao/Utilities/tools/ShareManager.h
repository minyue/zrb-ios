//
//  ShareManager.h
//  ZhaoRongbao
//
//  Created by songmk on 15/10/12.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialShakeService.h"
#import "UMSocialSinaHandler.h"

@interface ShareManager : NSObject<UMSocialUIDelegate>

+ (ShareManager *)ShareManager;

- (void)shareConfig;

//分享到QQ
- (void)QQShareWithViewControll:(NSString *)str URLStr:(NSString *)url;

//分享到QQ空间
- (void)QQZoneShareWithViewControll:(NSString *)str URLStr:(NSString *)url;

//分享到微信
- (void)WXChatShareWithViewControll:(NSString *)str URLStr:(NSString *)url;

//分享到微信朋友圈
- (void)WXFriendShareWithViewControll:(NSString *)str URLStr:(NSString *)url;

//分享到新浪微博
- (void)SinaWeiboShareWithViewControll:(UIViewController *)ctrl shareContent:(NSString *)str URLStr:(NSString *)url;

@end
