//
//  ShareManager.m
//  ZhaoRongbao
//
//  Created by songmk on 15/10/12.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "ShareManager.h"


@implementation ShareManager

static ShareManager *shareManager;

+ (ShareManager *)ShareManager{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareManager = [[ShareManager alloc] init];
    });
    return shareManager;
    
}

- (void)shareConfig{
    
    //设置微信AppId，设置分享url，默认使用友盟的网址
    [UMSocialWechatHandler setWXAppId:WXChatAppId appSecret:WXChatAppSecret url:nil];
}

/**
  *QQ分享
  **/
- (void)QQShareWithViewControll:(NSString *)str URLStr:(NSString *)url{
    
    //分享图标
    UIImage *image = [UIImage imageNamed:@"shareIcon"];
    //点击分享内容的链接
    [UMSocialData defaultData].extConfig.qqData.url = url;
    
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToQQ] content:str image:image location:nil urlResource:nil presentedController:nil completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
}

/**
 *  QQ空间分享
 */

- (void)QQZoneShareWithViewControll:(NSString *)str URLStr:(NSString *)url{
    
    //分享图标
    UIImage *image = [UIImage imageNamed:@"shareIcon"];
    //点击分享内容的链接
    [UMSocialData defaultData].extConfig.qqData.url = url;
    
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToQzone] content:str image:image location:nil urlResource:nil presentedController:nil completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
}

/**
 *  微信分享
 */
- (void)WXChatShareWithViewControll:(NSString *)str URLStr:(NSString *)url{
    
    //分享图标
    UIImage *image = [UIImage imageNamed:@"shareIcon"];
    //点击分享内容的链接
    [UMSocialData defaultData].extConfig.qqData.url = url;
    
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:str image:image location:nil urlResource:nil presentedController:nil completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
}

/**
 *  微信朋友圈分享
 */
- (void)WXFriendShareWithViewControll:(NSString *)str URLStr:(NSString *)url{
    
    //分享图标
    UIImage *image = [UIImage imageNamed:@"shareIcon"];
    //点击分享内容的链接
    [UMSocialData defaultData].extConfig.qqData.url = url;
    
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatTimeline] content:str image:image location:nil urlResource:nil presentedController:nil completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            NSLog(@"分享成功！");
        }
    }];
}

/**
 *  新浪微博分享
 */
- (void)SinaWeiboShareWithViewControll:(UIViewController *)ctrl shareContent:(NSString *)str URLStr:(NSString *)url{
    
    //分享图标
    UIImage *image = [UIImage imageNamed:@"shareIcon"];
    
    [[UMSocialControllerService defaultControllerService] setShareText:str shareImage:image socialUIDelegate:self];
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:@"sina"];
    snsPlatform.snsClickHandler(ctrl,[UMSocialControllerService defaultControllerService],YES);
    
}

//下面得到分享完成的回调
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    NSLog(@"didFinishGetUMSocialDataInViewController with response is %@",response);
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}


@end
