//
//  TOTextInput.h
//  FrameworkTest
//
//  Created by Ted on 13-11-13.
//  Copyright (c) 2013年 Tony. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputError.h"
#import "ZRB_TextField.h"
enum InputCheckType{
    //非兼容模式
    InputCheckTypeString = 0x00,
    InputCheckTypeFloat = 0x01,//未实现
    InputCheckTypeInt = 0x02,//未实现
    InputCheckTypeMoney = 0x04,
    
    
    /*兼容模式*/
    //字符模式下表示字符长度,数字长度下表示最大值
    InputCheckTypeMaxLength = 0x10,
    InputCheckTypeMinLength = 0x20,
    InputCheckTypeNotNull = 0x40,
    //输入字符集限制模式
    InputCheckTypeCharacters = 0x80,
    //文本合法性检测
    InputCheckTypeRegex = 0x100,
    
};




@class TOTextInputChecker;





@interface TOTextInputChecker : NSObject<ZRB_TextFieldDelegate>{
    enum InputCheckType type;
}


@property (nonatomic) enum InputCheckType type;
@property (nonatomic, strong)   NSString * characters;
@property (nonatomic, strong)   NSString * regex;
@property (nonatomic) UIKeyboardType keyboardType;

@property (nonatomic)   CGFloat  maxLen;
@property (nonatomic)   CGFloat  minLen;

@property (nonatomic)   NSString *  text;
@property (nonatomic)   BOOL secureTextEntry;



/**
 *  检查是否通过验证
 */
@property(nonatomic,copy) void (^checkInputBlock)(BOOL flag);

/**
 *  检查通过后是否展示自定义标示
 */
@property (nonatomic,copy) void (^showIconBlock)(BOOL flag);

+(TOTextInputChecker *)userNameChecker;
+(TOTextInputChecker *)userTrueNameChecker;
+(TOTextInputChecker *)passwordChecker;
+ (TOTextInputChecker *)codeChecker;
+(TOTextInputChecker *)telChecker:(BOOL)notNull;
+(TOTextInputChecker *)idCarcChecker:(BOOL) notNull;
+(TOTextInputChecker *)mailChecker:(BOOL)notNull;
+(TOTextInputChecker *)floatChecker:(float)min max:(float)max;
+(TOTextInputChecker *)intChecker:(float)min max:(float)max;
+(TOTextInputChecker *)moneyChecker:(float)min max:(float)max;


-(enum InputCheckError)finalCheck;


@end



