//
//  UIAlertPopAnimationView.h
//  ZhaoRongbao
//
//  Created by zouli on 15/8/14.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  个人中心-- 弹出框动画

#import <UIKit/UIKit.h>

@interface UIAlertPopAnimationView : UIView

@property (nonatomic, strong) IBOutlet UIView *altView;

-(void)initWithContentView:(UIView*)view;

- (IBAction)showOnView:(UIView*)view;


- (IBAction)hideAlertAction:(id)sender;
@end
