//
//  UIAlertPopAnimationView.m
//  ZhaoRongbao
//
//  Created by zouli on 15/8/14.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "UIAlertPopAnimationView.h"
#import <QuartzCore/QuartzCore.h>
@interface UIAlertPopAnimationView()


@end
@implementation UIAlertPopAnimationView

- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.backgroundColor = [UIColor clearColor];
    if(self.altView){
        self.altView.layer.cornerRadius = 8.0f;
    }
}

-(void)initWithContentView:(UIView*)view{
    CGRect bound = [self.altView frame];
    [self.altView removeFromSuperview];
    self.altView = nil;
    view.frameX = bound.origin.x;
    view.frameY = bound.origin.y;
    self.altView = view;
    
    [self addSubview:self.altView];
}

-(void)awakeFromNib{
    
}

- (IBAction)showOnView:(UIView*)view{
    if ([self superview] != nil)
        return;
    
    self.frame = view.bounds;
    [view addSubview:self];
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9f, 0.9f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.altView.layer addAnimation:popAnimation forKey:nil];
}


- (IBAction)hideAlertAction:(id)sender {
    CAKeyframeAnimation *hideAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    hideAnimation.duration = 0.4;
    hideAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1f, 1.1f, 1.0f)],
                             [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0f, 1.0f, 1.0f)],
                             [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.00f, 0.00f, 0.00f)]];
//    hideAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f];
    hideAnimation.keyTimes = @[@0.25f];
    hideAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    hideAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
//                                      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
//                                      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    hideAnimation.delegate = self;
    [self.altView.layer addAnimation:hideAnimation forKey:nil];
    
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    [self removeFromSuperview];
}

@end
