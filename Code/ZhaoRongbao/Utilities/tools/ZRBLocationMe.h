//
//  ZRBLocationMe.h
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  定位到当前城市

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ZRBLocationMe : NSObject<CLLocationManagerDelegate>
@property (nonatomic,copy) NSDictionary *(^codebackBlock)();
@property (strong, nonatomic) CLLocationManager* locationManager;
-(id)init;
@end
