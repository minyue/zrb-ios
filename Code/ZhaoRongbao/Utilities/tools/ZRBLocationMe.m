//
//  ZRBLocationMe.m
//  ZhaoRongbao
//
//  Created by abel on 15/8/20.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRBLocationMe.h"

@implementation ZRBLocationMe

-(id)init{
    if(self = [super init]){
        [self startLocation];
    }
    return self;
}

//开始定位

-(void)startLocation{
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    self.locationManager.distanceFilter = 10.0f;
    
    [self.locationManager startUpdatingLocation];
    
}


@end
