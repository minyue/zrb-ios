//
//  ZRBUtilities.h
//  ZhaoRongbao
//
//  Created by abel on 15/6/27.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatSystemNotyListModel.h"

@interface ZRBUtilities : NSObject

/**
 *  判断是否为空格或者空值
 *
 *  @param string 输入源
 *
 *  @return YES : 为真   NO:为假
 */
+ (BOOL)isBlankString:(NSString *)string;



/**
 *  从xib文件中读取view
 *
 *  @param fileName xib文件
 *
 *  @return
 */
+ (UIView *)viewFromSourceName:(NSString *)fileName;


//组装图像连接URL
+ (NSURL *)urlWithPath:(NSString *)imagePath;
//组装图像连接URL --临时 邹黎
+ (NSURL *)urlWithIconPath:(NSString *)imagePath;


/**
 *  判断奇偶性
 *
 *  @param i 输入
 *
 *  @return 返回  YES： 奇数。NO:偶数
 */
+ (BOOL)isOdd:(int)i;


+ (NSString *)stringToData:(NSString *)dateType interval:(NSString *)interval;


+ (UIImage *)ZRB_DefaultImage;
//返回用户默认头像
+ (UIImage *)ZRB_UserDefaultImage;

//生成上传头像名称
+ (NSString*)ZRB_CreateIconImageName;

//check服务器上的头像是否是存在本地
+ (BOOL)ZRB_ifCachedIcon:(NSString*)name;
/**
 *  保存用户头像 路径
 *
 *  @return 保存路径
 */
+ (NSString*)ZRB_UserIconLocalPath;
/**
 *  获取本地用户头像
 *
 *  @return 返回用户头像
 */
+ (UIImage*)ZRBUserIconFile;


/**
 *  保存用户头像到本地
 *
 *  @param image 用户头像
 *
 *  @return 生成文件的保存路径
 */
+ (NSString*)ZRBScaleAndSaveImage:(UIImage *)image;

//对图片尺寸进行压缩--
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;

/**
 *  判断文字是否为手机号
 */
+ (BOOL)isMobileNumber:(NSString *)mobileNum;

/**
 *  判断是否为邮箱
 */
+ (BOOL)isValidateEmail:(NSString *)Email;




///*
///**
// *  某一项目/资讯 保存关注
// *  @param sid //项目/资讯ID
// *  @param type //1是项目 0是资讯
// *
// *  @return <#return value description#>
// */
//
//+ (BOOL)saveAttenteionID:(NSString*)sid andType:(int)type;
///**
// *  某一项目/资讯 取消关注
// *   @param type //1是项目 0是资讯
// *  @return 取消成功 或失败
// */
//+ (BOOL)removeAttentionID:(NSString*)rid andType:(int)type;
///**
// *  判断某一项目/资讯是否已被关注
// *   @param type //1是项目 0是资讯
// *  @return 是否已关注
// */
//+ (BOOL)checkIdIfAttention:(NSString*)cid andType:(int)type;

/**
 *  根据当前系统通知类型，返回label应该显示的NSAttributeSting
 *  系统通知模块需要使用
 */
+ (void)setSysNotyShow:(UILabel*)content and:(UILabel*)title andModel: (ChatSystemNotyListModel*)model;
@end
