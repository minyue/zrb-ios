//
//  ZRBUtilities.m
//  ZhaoRongbao
//
//  Created by abel on 15/6/27.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRBUtilities.h"
#import "NSDate+Addition.h"


@implementation ZRBUtilities



/**
 *  判断是否为空格或者空值
 *
 *  @param string 输入源
 *
 *  @return YES : 为真   NO:为假
 */
+ (BOOL)isBlankString:(NSString *)string
{
    
    if (string == nil)
    {
        
        return YES;
        
    }
    
    if (string == NULL)
    {
        
        return YES;
        
    }
    
    if ([string isKindOfClass:[NSNull class]])
    {
        
        return YES;
        
    }
    
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
    {
        
        return YES;
        
    }
    
    return NO;
    
}



+ (UIView *)viewFromSourceName:(NSString *)fileName
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:fileName owner:nil options:nil];
    return [nibView objectAtIndex:0];
}


/**
 *  判断奇偶性
 *
 *  @param i 输入
 *
 *  @return 返回
 */
+ (BOOL)isOdd:(int)i
{
    
    return (i & 1)!=0;
}


//组装图像连接URL
+ (NSURL *)urlWithPath:(NSString *)imagePath
{
    
    if (imagePath) {
        NSString *imageUrl = [NSString stringWithFormat:@"%@/%@",IMAGE_SERVER_URL,imagePath];
        
        return [NSURL URLWithString:imageUrl];
  
    }else
    {
        return nil;
    }
    
}

//组装图像连接URL --临时 邹黎
+ (NSURL *)urlWithIconPath:(NSString *)imagePath
{
    
    if (imagePath) {
        NSString *imageUrl = [NSString stringWithFormat:@"%@/%@",IMAGE_SERVER_URL,imagePath];
        
        return [NSURL URLWithString:imageUrl];
        
    }else
    {
        return nil;
    }
    
}

//check服务器上的头像是否是存在本地
+ (BOOL)ZRB_ifCachedIcon:(NSString*)name{
    NSFileManager   *fileManager = [NSFileManager   defaultManager];
    NSArray *files = [fileManager subpathsAtPath: [ZRBpath iconCachePath] ];
    if([files count]>0){
        if([files[0] isEqualToString:name])
            return YES;
    }
    return NO;
}

/**
 *  时间戳转换成自定义时间格式
 *
 *  @param formatterType 时间格式：例如 YYYY-MM-DD
 *
 *  @return 返回
 */
+ (NSString *)stringToData:(NSString *)formatterType interval:(NSString *)interval
{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[interval longLongValue]/1000];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:formatterType];
    
    NSString *dateStr = [formatter stringFromDate:date];
    
    return dateStr;
}



+ (UIImage *)ZRB_DefaultImage
{

    return [UIImage imageNamed:@"default_icon"];
}

+ (UIImage *)ZRB_UserDefaultImage
{
    
    return [UIImage imageNamed:@"my_head"];
}

+ (NSString*)ZRB_CreateIconImageName{
    //当前时间 精确到秒
    return [NSDate dateNowFormatString];
}


/**
 *  保存用户头像本地路径
 *
 *  @return 保存路径
 */
+ (NSString*)ZRB_UserIconLocalPath{
    return [NSString stringWithFormat:@"%@%@.png",[ZRBpath iconCachePath],[ZRBUtilities ZRB_CreateIconImageName]];
}


/**
 *  获取本地用户头像
 *
 *  @return 返回用户头像
 */
+ (UIImage*)ZRBUserIconFile
{
    UIImage *image = nil;
    NSFileManager   *fileManager = [NSFileManager   defaultManager];
    NSArray *files = [fileManager subpathsAtPath: [ZRBpath iconCachePath] ];
    if([files count]>0){
//        NSData *data = [fm contentsAtPath:[self dataFilePath]];
       NSString *filePath = [NSString stringWithFormat:@"%@%@",[ZRBpath iconCachePath],files[0]];
        image = [UIImage imageWithContentsOfFile:filePath];
    }
    return image;
}

//保存用户头像到本地
+ (NSString*)ZRBScaleAndSaveImage:(UIImage *)image
{
    NSFileManager   *fileManager = [NSFileManager   defaultManager];
    NSArray *files = [fileManager subpathsAtPath: [ZRBpath iconCachePath]];
    if([files count]>0){
        //清空
        [fileManager removeItemAtPath:[ZRBpath iconCachePath] error:nil];
    }
    //调整图片方向
    float quality = 0.5;
    
    CGSize imagesize = image.size;
    imagesize.height = 80;
    imagesize.width = 80;
    //对图片大小进行压缩--
    image = [ZRBUtilities imageWithImage:image scaledToSize:imagesize];

    NSData *imageData = UIImageJPEGRepresentation(image, quality);
    NSString *path = [ZRBUtilities ZRB_UserIconLocalPath];
    [imageData writeToFile:path atomically:YES];//写入到目录
    return path;
}

//对图片尺寸进行压缩--
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}


+ (BOOL)isMobileNumber:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


+ (BOOL)isValidateEmail:(NSString *)Email

{
    
    NSString *emailCheck = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",emailCheck];
    
    return [emailTest evaluateWithObject:Email];
    
}

/*
//1是项目 0是资讯 用;分开
+ (BOOL)saveAttenteionID:(NSString*)sid andType:(int)type{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    if(!user.uid || !sid)
        return NO;
    NSString *fileName = [NSString stringWithFormat:@"%@_%d",user.uid,type];
    NSString *filePath = [ZRBpath attentionCachePath:fileName];
    NSLog(@"filePath:%@",filePath);
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSMutableArray *marray = [[NSMutableArray alloc] init];
    if (content) {
        marray = [NSMutableArray arrayWithArray:[content componentsSeparatedByString:@";"]];
    }
    [marray addObject:sid];
    //拼成字符串
    NSMutableString *mstring = [[NSMutableString alloc]init];
    for(NSString *str in marray){
        
        NSString *addString = [str stringByAppendingString:@";"];
        [mstring appendString:addString];
    }
    if( [[mstring dataUsingEncoding:NSUTF8StringEncoding] writeToFile:filePath atomically:YES]){
        return YES;
    }else{
        return NO;
    }
}

+ (BOOL)removeAttentionID:(NSString*)rid andType:(int)type{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    if(!user.uid || !rid)
        return NO;
    NSString *fileName = [NSString stringWithFormat:@"%@_%d",user.uid,type];
    NSString *filePath = [ZRBpath attentionCachePath:fileName];
    
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSMutableArray *marray = [[NSMutableArray alloc] init];
    if (content) {
        marray = [NSMutableArray arrayWithArray:[content componentsSeparatedByString:@";"]];
    }
    //移除关注ID
    for(int i =0;i <[marray count]; i++){
        NSString *str = [marray objectAtIndex:i];
        if([str isEqualToString:rid])
            [marray removeObject:str];
    }
    NSMutableString *mstring = [[NSMutableString alloc]init];
    //剩下的拼成字符串
    if([marray count]>0){
        for(NSString *str in marray){
            
            NSString *addString = [str stringByAppendingString:@";"];
            [mstring appendString:addString];
        }
    }
    if([[mstring dataUsingEncoding:NSUTF8StringEncoding] writeToFile:filePath atomically:YES]){
        return YES;
    }else{
        return NO;
    }
    return NO;
}

+ (BOOL)checkIdIfAttention:(NSString*)cid andType:(int)type{
    ZRB_UserModel *user = [ZRB_UserManager shareUserManager].userData;
    if(!user.uid || !cid)
        return NO;
    NSString *fileName = [NSString stringWithFormat:@"%@_%d",user.uid,type];
    NSString *filePath = [ZRBpath attentionCachePath:fileName];
    
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSMutableArray *marray = [[NSMutableArray alloc] init];
    if (content) {
        marray = [NSMutableArray arrayWithArray:[content componentsSeparatedByString:@";"]];
    }
    if([marray containsObject:cid])
        return YES;
  
    return NO;
}*/

/**
 *  根据当前系统通知类型，返回label应该显示的NSAttributeSting
 *  BizType type
 */
//+ (NSAttributedString*) txtWithType:(NSString*)type andContent:(NSString*)content{
//    if(!type || !content)
//        return nil;
//    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
//    NSDictionary *redColorDic = @{NSForegroundColorAttributeName:RGB(227, 108, 16)};//橘红色
//    NSDictionary *blueColorDic = @{NSForegroundColorAttributeName:RGB(59, 130, 232)};//蓝色
//    if([type isEqualToString:@"user_regiester_succes"]){
////        attString = [attString initWithString:@"恭喜您注册成功！"];
//        attString = [attString initWithString:content];
//    }else if([type isEqualToString:@"investor_regiester_succes_without_org"]){
//        attString = [attString initWithString:@"恭喜您注册成功！您可以点击立即认证"];
//        [attString setAttributes:redColorDic range:NSMakeRange(attString.length - 4,4)];
//    }else if([type isEqualToString:@"user_auth_succes"]){
////        attString = [attString initWithString:@"你的身份认证通过"];
//        attString = [attString initWithString:content];
//    }
//    else if([type isEqualToString:@"user_auth_modify"]){
////        attString = [attString initWithString:@"您的身份认证信息已经变更"];
//        attString = [attString initWithString:content];
//    }
//    else if([type isEqualToString:@"user_org_modify"]){
////        attString = [attString initWithString:@"你的所属机构已经变更"];
//        attString = [attString initWithString:content];
//    }
//    else if([type isEqualToString:@"proj_prog_change"]){
////        attString = [attString initWithString:@"您的项目进展有变化"];
//        attString = [attString initWithString:content];
//    }
//    else if([type isEqualToString:@"proj_prog_new"]){
//        attString = [attString initWithString:@"您有新项目发布,你可以点击立即查看"];
//        [attString setAttributes:blueColorDic range:NSMakeRange(attString.length - 4,4)];
//    }
//    else if([type isEqualToString:@"proj_cment_relay"]){
////        attString = [attString initWithString:@"你的评论有新的回复"];
//        attString = [attString initWithString:content];
//    }
//    else if([type isEqualToString:@"proj_cment_new"]){
//        //        attString = [attString initWithString:@"您的项目有新的评论"];
//        attString = [attString initWithString:content];
//    }
//    else if([type isEqualToString:@"user_apply_inspect"]){
//        attString = [attString initWithString:@"您的实地勘察申请已提交,点击查看勘察进度"];
//        [attString setAttributes:blueColorDic range:NSMakeRange(attString.length - 4,4)];
//    }
//    else if([type isEqualToString:@"user_confirm_inspect"]){
//        attString = [attString initWithString:content];
//    }
//    else if([type isEqualToString:@"user_complete_inspect"]){
//        attString = [attString initWithString:content];
//    }
//    return attString;
//}

//+ (void)setSysNotyShow:(UILabel*)content and:(UILabel*)title andModel: (ChatSystemNotyListModel*)model{
//    if(!model)
//        return;
//    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
//    NSDictionary *redColorDic = @{NSForegroundColorAttributeName:RGB(227, 108, 16)};//橘红色
////    NSDictionary *blueColorDic = @{NSForegroundColorAttributeName:RGB(59, 130, 232),NSUnderlineStyleAttributeName,NSUnderlineStyleSingle};//蓝色
//    NSDictionary *blueColorDic = [NSDictionary  dictionaryWithObjectsAndKeys:RGB(59, 130, 232),NSForegroundColorAttributeName,NSUnderlineStyleAttributeName,[NSString stringWithFormat:@"%ld",(long)NSUnderlineStyleDouble],nil];//蓝色+下划线
//    NSString* bizType = model.messageInfo.bizType;
//    NSString* con = model.messageInfo.content;
//    if([bizType isEqualToString:@"investor_regiester_succes_without_org"]){
//        attString = [attString initWithString:con];
//        [attString setAttributes:blueColorDic range:NSMakeRange(attString.length - 4,4)];
//        content.attributedText = attString;
//        title.text = @"身份认证";
//    }
//    else if([bizType isEqualToString:@"user_auth_succes"]){
//        BOOL ini = [con containsString:@"挂靠"];
//        if(ini){
//            NSRange range = [con rangeOfString:@"挂靠"];
//            attString = [attString initWithString:con];
//            [attString setAttributes:redColorDic range:NSMakeRange(range.location + range.length,con.length - range.location - range.length)];
//            content.attributedText = attString;
//        }
//        else{
//            content.text = con;
//        }
//        title.text = @"账户变更";
//    }else if([bizType isEqualToString:@"user_auth_modify"]){
//        title.text = @"账户变更";
//        if([con containsString:@"已由"])
//        {
//            attString = [attString initWithString:con];
//            NSArray *arr = [con componentsSeparatedByString:@"已由"];
//            NSArray *arr1 = [[arr objectAtIndex:1] componentsSeparatedByString:@"变更为"];
//            if(arr1){
//                NSRange range1 = [con rangeOfString:[arr1 objectAtIndex:0]];
//                NSRange range2 = [con rangeOfString:[arr1 objectAtIndex:1]];
//                [attString setAttributes:redColorDic range:range1];
//                [attString setAttributes:redColorDic range:range2];
//            }
//        }else{
//            content.text = con;
//        }
//    }
//    else if([bizType isEqualToString:@"proj_prog_new"]){
//        title.text = @"发布成功";
//        attString = [attString initWithString:con];
//        [attString setAttributes:blueColorDic range:NSMakeRange(attString.length - 4,4)];
//        content.attributedText = attString;
//    }
//    else if([bizType isEqualToString:@"proj_cment_relay"] || [bizType isEqualToString:@"proj_cment_new"]){
//        //pid(项目id):cid(评论id):uid(回复人id)：uname(评论人用户名)
//        NSArray *array = [model.messageInfo.bizKey componentsSeparatedByString:@":"];
//        
//        if(array.count==4){
//            NSString *str1 = @"";
//            if([(NSString*)array[2] containsString:@"1"]) {
//                str1 = @"投资人";
//            }
//            else if([(NSString*)array[2] containsString:@"2"]){
//                str1 = @"公务员";
//            }
//            title.text = [NSString stringWithFormat:@"%@ %@",str1,(NSString*)array[3]];
////          title.setText(UserUtils.getUserRole("_"+tmpArr[2])+" "+tmpArr[3]);
//        }
//        content.text = con;
//    }
//    else if([bizType isEqualToString:@"user_apply_inspect"]){
//        title.text = @"项目勘察";
//        attString = [attString initWithString:con];
//        [attString setAttributes:blueColorDic range:NSMakeRange(attString.length - 4,4)];
//        content.attributedText = attString;
//    }
//    else{
//        content.text = con;
//        title.text = @"系统通知";
//    }
//}

+ (void)setSysNotyShow:(UILabel*)content and:(UILabel*)title andModel: (ChatSystemNotyListModel*)model{
    if(!model)
        return;
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
//    NSDictionary *redColorDic = @{NSForegroundColorAttributeName:RGB(227, 108, 16)};//橘红色
        NSDictionary *blueColorDic = @{NSForegroundColorAttributeName:RGB(59, 130, 232)};//蓝色
//    NSDictionary *blueColorDic = [NSDictionary  dictionaryWithObjectsAndKeys:RGB(59, 130, 232),NSForegroundColorAttributeName,NSUnderlineStyleAttributeName,[NSString stringWithFormat:@"%ld",(long)NSUnderlineStyleDouble],nil];//蓝色+下划线
    NSString* bizType = model.messageInfo.bizType;
    NSString* con = model.messageInfo.content;
    if([bizType isEqualToString:@"pro_fund_pulished"]){
        attString = [attString initWithString:con];
        content.attributedText = attString;
        title.text = @"发布成功";
    }
    else if([bizType isEqualToString:@"pro_fund_changed"]){
        title.text = @"变更通知";
        if([con containsString:@"由"])
        {
            attString = [attString initWithString:con];
            NSArray *arr = [con componentsSeparatedByString:@"由"];
            NSArray *arr1 = [[arr objectAtIndex:1] componentsSeparatedByString:@"变更为"];
            if(arr1){
                NSRange range1 = [con rangeOfString:[arr1 objectAtIndex:0]];
                NSRange range2 = [con rangeOfString:[arr1 objectAtIndex:1]];
                [attString setAttributes:blueColorDic range:range1];
                [attString setAttributes:blueColorDic range:range2];
                content.attributedText = attString;
            }
        }else{
            content.text = con;
        }
    }else if([bizType isEqualToString:@"pro_fund_collected"]){
        title.text = @"关注通知";
        NSUInteger i = [con rangeOfString:@"关注了您的"].location;
        if(i)
        {
            attString = [attString initWithString:con];
             NSRange range1 = [con rangeOfString:[con substringToIndex:i]];
            [attString setAttributes:blueColorDic range:range1];
            content.attributedText = attString;
        }else{
            content.text = con;
        }
    }
    
    else{
        content.text = con;
        title.text = @"系统通知";
    }
}
@end
