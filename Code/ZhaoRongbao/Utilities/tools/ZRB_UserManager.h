//
//  ZRB_UserManager.h
//  ZhaoRongbao
//
//  Created by abel on 15/7/21.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//  个人信息管理类

#import <Foundation/Foundation.h>
#import "UserModel.h"
@interface ZRB_UserManager : NSObject

//用户信息
@property (nonatomic,strong) ZRB_UserModel *userData;


+ (instancetype)shareUserManager;

/**
 *  用户是否登录
 *
 *  @return YES:已登录   NO:未登录
 */
+ (BOOL)isLogin;


/**
 *  登录成功
 *
 *  @param model 登录返回的数据
 */
 + (void)loginSuccssWithModel:(LoginModel *)model withBlock:(void (^) (bool data))block;

/**
 *  保存当前用户信息
 */
+ (void)saveUserModel:(ZRB_UserModel *)model;

//退出登录
+ (void)logOut;
@end
