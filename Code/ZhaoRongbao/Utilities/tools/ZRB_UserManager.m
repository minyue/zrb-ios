//
//  ZRB_UserManager.m
//  ZhaoRongbao
//
//  Created by abel on 15/7/21.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRB_UserManager.h"
#import "SSGlobalConfig.h"

@implementation ZRB_UserManager

+ (instancetype)shareUserManager
{
    static ZRB_UserManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ZRB_UserManager alloc] init];
    });
    
    return manager;
}


-  (instancetype)init
{
    if (self = [super init]) {
        //这里可以注册登录、登出等通知
    }
    return self;
}


+ (BOOL)isLogin
{
    ZRB_UserManager *manager = [ZRB_UserManager  shareUserManager];
    if (manager.userData)
    {
        return YES;
    }else
    {
        return NO;
    }
}

/**
 *  登录成功
 *
 *  @param model 登录返回的数据
 */
+ (void)loginSuccssWithModel:(LoginModel *)model withBlock:(void (^) (bool data))block
{
    ZRB_UserManager *manager = [ZRB_UserManager  shareUserManager];
//    if(!model.model.zrb_front_ut){
//        manager.userData = nil;
//        manager.userData = model.model;
//    }else{
//        NSString *str = model.model.zrb_front_ut;
//        manager.userData = model.model;
//        manager.userData.zrb_front_ut = str;
//    }
    manager.userData = nil;
    manager.userData = model.model;
    
    //保存账号密码（base64），用于自动登录
    [[SSGlobalConfig defaultConfig] setParam:@"PHONENUMBER" value:model.phoneNum];
    [[SSGlobalConfig defaultConfig] setParam:@"PASSWORD" value:[model.userPwd encodeToBase64]];
    if (block) {
        block(YES);
    }
}

/**
 *  保存当前用户信息
 */
+ (void)saveUserModel:(ZRB_UserModel *)model
{
    //先取出登录后返回的重要信息，赋完对象后重新保存
    ZRB_UserManager *manager = [ZRB_UserManager  shareUserManager];
    NSString *strTemp = manager.userData.zrb_front_ut;
    manager.userData = model;
    manager.userData.zrb_front_ut = strTemp;
}

//退出登录
+ (void)logOut{
    ZRB_UserManager *manager = [ZRB_UserManager  shareUserManager];
    manager.userData = nil;
    //清空本地账号密码
    [[SSGlobalConfig defaultConfig] setParam:@"PHONENUMBER" value:@""];
    [[SSGlobalConfig defaultConfig] setParam:@"PASSWORD" value:@""];
}

@end
