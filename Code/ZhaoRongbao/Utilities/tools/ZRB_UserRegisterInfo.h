//
//  ZRB_UserRegisterInfo.h
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//  用户注册信息

#import <Foundation/Foundation.h>
#import "IndustryModel.h"
#import "AreaModel.h"

@class RegisterInfoModel;
@interface ZRB_UserRegisterInfo : NSObject

@property (nonatomic,strong) RegisterInfoModel *model;

+ (instancetype)shareUserRegisterInfo;
@end

@interface RegisterInfoModel : NSObject
@property (nonatomic,strong) NSString   *phone;
@property (nonatomic,strong) NSString   *pwd;

@property (nonatomic,strong) NSString   *realName;
@property (nonatomic,strong) NSString   *cityID;
@property (nonatomic,strong) NSString   *city;
//@property (nonatomic,strong) AreaModel  *area;
@property (nonatomic,strong) IndustryModel   *industry;//行业
@property (nonatomic,strong) NSString   *enterpriseName;
@property (nonatomic,strong) NSString   *duty;

@end
