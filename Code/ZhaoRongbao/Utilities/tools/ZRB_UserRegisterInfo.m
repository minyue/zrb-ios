//
//  ZRB_UserRegisterInfo.m
//  ZhaoRongbao
//
//  Created by abel on 15/9/17.
//  Copyright (c) 2015年 zouli. All rights reserved.
//

#import "ZRB_UserRegisterInfo.h"

@implementation ZRB_UserRegisterInfo

+ (instancetype)shareUserRegisterInfo
{
    static ZRB_UserRegisterInfo *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ZRB_UserRegisterInfo alloc] init];
    });
    
    return manager;
}


-  (instancetype)init
{
    if (self = [super init]) {
        //这里可以注册登录、登出等通知
        if(!_model)
            _model = [[RegisterInfoModel alloc] init];
    }
    return self;
}

@end

@implementation RegisterInfoModel


@end
