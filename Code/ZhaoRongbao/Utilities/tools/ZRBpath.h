//
//  ZRBpath.h
//  ZhaoRongbao
//
//  Created by zouli on 15/8/13.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZRBpath : NSObject
+ (NSString *)tempFilePath;     // .tem/
+ (NSString *)docFilePath;      // .documents/
+ (NSString *)iconCachePath;   ///Library/Data/myIcons/
+ (NSString *)attentionCachePath:(NSString*)str1; // .documents/myAttention 我的关注
+ (NSString *)plistPath;
@end
