//
//  ZRBpath.m
//  ZhaoRongbao
//
//  Created by zouli on 15/8/13.
//  Copyright (c) 2015年 zhoug. All rights reserved.
//

#import "ZRBpath.h"

@implementation ZRBpath

+ (NSString *)tempFilePath
{
    return [NSString stringWithFormat:@"%@/tmp/", NSHomeDirectory()];
}

+ (NSString *)docFilePath
{
    return [NSString stringWithFormat:@"%@/Documents/", NSHomeDirectory()];
}

+ (BOOL)ensureExistsOfDirectory:(NSString *)dirPath
{
    BOOL            isDir;
    NSFileManager   *fileMgr = [NSFileManager defaultManager];
    
    if (![fileMgr fileExistsAtPath:dirPath isDirectory:&isDir] || !isDir) {
        BOOL succeed = [fileMgr createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:[NSDictionary dictionaryWithObject:NSFileProtectionNone forKey:NSFileProtectionKey] error:nil];
        return succeed;
    }
    
    return YES;
}

+ (BOOL)ensureExistsOfFile:(NSString *)dirPath
{
    BOOL            isDir;
    NSFileManager   *fileMgr = [NSFileManager defaultManager];
    
    if (![fileMgr fileExistsAtPath:dirPath isDirectory:&isDir] || !isDir) {
        BOOL succeed = [fileMgr createFileAtPath:dirPath contents:nil attributes:nil];
        return succeed;
    }
    
    return YES;
}

+ (NSString *)dataFilePath
{
    //修改缓存文件的存储路径，确保在没有存储空间时，不会被删除
    NSString    *path = [NSString stringWithFormat:@"%@/Library/Data/", NSHomeDirectory()];
    [ZRBpath    ensureExistsOfDirectory:path];  //确保文件路径存在
    [[NSURL  fileURLWithPath:path] setResourceValue:[NSNumber numberWithBool: YES] forKey:NSURLIsExcludedFromBackupKey error:nil];    //设置该文件为不备份
    return path;
}

+ (NSString *)plistPath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString    *path = [documentsDirectory stringByAppendingPathComponent:@"myAttention"];
    
    NSString  *attPath = [[ZRBpath  dataFilePath]  stringByAppendingString:@"defaultConfig.plist"];
    [ZRBpath    ensureExistsOfFile:path];
    return attPath;
}

+ (NSString *)iconCachePath{
    NSString *cachePath = [[ZRBpath  dataFilePath] stringByAppendingString:@"myIcons"];
    [ZRBpath ensureExistsOfDirectory:cachePath];
    return [cachePath stringByAppendingString:@"/"];
}

+ (NSString *)attentionCachePath:(NSString*)str1{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString    *path = [documentsDirectory stringByAppendingPathComponent:@"myAttention"];
    
    NSString  *attPath = [path stringByAppendingString:[NSString stringWithFormat:@"%@.dat",str1]];
    [ZRBpath    ensureExistsOfFile:path];
    return attPath;
}
@end
