//
//  ZRRequestManager.m
//  ZhaoRongbao
//
//  Created by zouli on 15/10/20.
//  Copyright © 2015年 zouli. All rights reserved.
//

#import "ZRRequestManager.h"

@implementation ZRRequestManager

//判断请求Token
+ (NSDictionary*)reqestUrlAppentToken:(id)parm{
    ZRB_UserModel *model = [ZRB_UserManager shareUserManager].userData;
    if(model == nil || model.zrb_front_ut == nil)
        return parm;
    
    if(parm == nil){
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:[[ZRB_UserManager shareUserManager] userData].zrb_front_ut forKey:@"zrb_front_ut"];
        return dic;
    }
    else if([parm isKindOfClass:[NSDictionary class]]){
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary*)parm];
        [dic setObject:[[ZRB_UserManager shareUserManager] userData].zrb_front_ut forKey:@"zrb_front_ut"];
        return dic;
    }else{
        return (NSDictionary*)parm;
    }
}
@end
